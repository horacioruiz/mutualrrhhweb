GMaps = function (options) {
    this.div = $(options.div)[0];
    this.markers = [];
    this.polygon = null;
    this.infoWindow = null;
    this.map = new google.maps.Map(this.div, {
        zoom: 15,
        center: new google.maps.LatLng(options.lat, options.lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    google.maps.event.addListener(this.map, 'dragend', function (e) {
        if (options.dragend)
            options.dragend(e);
    });
    google.maps.event.addListener(this.map, 'click', function (e) {
        if (options.click)
            options.click(e);
    });
    google.maps.event.addListener(this.map, 'dblclick', function (e) {
        if (options.dblclick)
            options.dblclick(e);
    });
    google.maps.event.addListener(this.map, 'center_changed', function (e) {
        if (options.center_changed)
            options.center_changed(e);
    });

    this.geolocate = function (options) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                options.success(position);
                if (options.always)
                    options.always();
            }, function (error) {
                options.error(error);
                if (options.always)
                    options.always();
            }, options.options);
        } else {
            options.not_supported();
            if (options.always)
                options.always();
        }
    };

    // Map methods

    this.setCenter = function (lat, lng, callback) {
        this.map.setCenter(new google.maps.LatLng(lat, lng));
        if (callback)
            callback();
    };
    this.getCenter = function () {
        return this.map.getCenter();
    };
    this.getDiv = function () {
        return this.div;
    };
    this.zoomIn = function (value) {
        this.map.setZoom(this.map.getZoom() + 1);
    };
    this.zoomOut = function (value) {
        this.map.setZoom(this.map.getZoom() - 1);
    };

    // Markers

    this.addMarker = function (options) {
        if (options.lat && options.lng) {
            var self = this;
            var base_options = {
                position: new google.maps.LatLng(options.lat, options.lng),
                map: this.map
            };
            delete options.lat;
            delete options.lng;
            var marker_options = $.extend(base_options, options);

            var marker = new google.maps.Marker(marker_options);
            if (options.infoWindow)
                marker.infoWindow = new google.maps.InfoWindow(options.infoWindow);

            google.maps.event.addListener(marker, 'click', function (e) {
                if (options.click)
                    options.click(e);
                if (marker.infoWindow) {
                    self.hide_info_windows();
                    marker.infoWindow.open(self.map, marker);
                }
            });
            google.maps.event.addListener(marker, 'dragend', function (e) {
                if (options.dragend)
                    options.dragend(e);
            });

            this.markers.push(marker);
        } else {
            throw 'No latitude or longitude defined';
        }
    };
    this.addMarkers = function (array) {
        for (marker in array)
            this.addMarker(marker);
    };
    this.hideInfoWindows = function () {
        for (index in this.markers) {
            marker = this.markers[index];
            marker.infoWindow.close();
        }
    };
    this.removeMarkers = function () {
        for (index in this.markers) {
            marker = this.markers[index];
            marker.setMap(null);
        }
        this.markers = [];
    };

    // Overlays

    this.drawCircle = function () {
        if (this.polygon)
            this.polygon.setMap(null);
        this.polygon = new google.maps.Circle({
            map: this.map,
            center: this.get_center(),
            radius: 1500,
            strokeColor: '#7ba5e4',
            fillColor: '#d8e5f7',
            strokeWeight: 2
        });
    };
}