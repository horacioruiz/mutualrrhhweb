package py.mutualmsp.mutualweb;

import com.vaadin.annotations.PreserveOnRefresh;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import py.mutualmsp.mutualweb.vista.AboutView;
import py.mutualmsp.mutualweb.vista.LoginScreen;
import py.mutualmsp.mutualweb.vista.MainScreen;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@PreserveOnRefresh
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Responsive.makeResponsive(this);
        setLocale(vaadinRequest.getLocale());
        getPage().setTitle("Mutual Comercios");
       
        LoginScreen ls = new LoginScreen();
        ls.setLoginListener(MyUI.this::showMainView);     
        setContent(ls);
    }

    protected void showMainView(){
        addStyleName(ValoTheme.UI_WITH_MENU);
        setContent(new MainScreen());
        getNavigator().navigateTo("".equals(getNavigator().getState()) ? AboutView.VIEW_NAME : getNavigator().getState());
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}




