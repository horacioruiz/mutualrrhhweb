/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "tipooperacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipooperacion.findAll", query = "SELECT t FROM Tipooperacion t")
    , @NamedQuery(name = "Tipooperacion.findByIdtipooperacion", query = "SELECT t FROM Tipooperacion t WHERE t.idtipooperacion = :idtipooperacion")
    , @NamedQuery(name = "Tipooperacion.findByDescripcion", query = "SELECT t FROM Tipooperacion t WHERE t.descripcion = :descripcion")})
public class Tipooperacion implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipooperacion", fetch = FetchType.LAZY)
    private List<Verificacion> verificacionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipooperacion", fetch = FetchType.LAZY)
    private List<Requisitooperacion> requisitooperacionList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipooperacion")
    private Long idtipooperacion;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;

    public Tipooperacion() {
    }

    public Tipooperacion(Long idtipooperacion) {
        this.idtipooperacion = idtipooperacion;
    }

    public Long getIdtipooperacion() {
        return idtipooperacion;
    }

    public void setIdtipooperacion(Long idtipooperacion) {
        this.idtipooperacion = idtipooperacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipooperacion != null ? idtipooperacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipooperacion)) {
            return false;
        }
        Tipooperacion other = (Tipooperacion) object;
        if ((this.idtipooperacion == null && other.idtipooperacion != null) || (this.idtipooperacion != null && !this.idtipooperacion.equals(other.idtipooperacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Tipooperacion[ idtipooperacion=" + idtipooperacion + " ]";
    }

    @XmlTransient
    public List<Requisitooperacion> getRequisitooperacionList() {
        return requisitooperacionList;
    }

    public void setRequisitooperacionList(List<Requisitooperacion> requisitooperacionList) {
        this.requisitooperacionList = requisitooperacionList;
    }

    @XmlTransient
    public List<Verificacion> getVerificacionList() {
        return verificacionList;
    }

    public void setVerificacionList(List<Verificacion> verificacionList) {
        this.verificacionList = verificacionList;
    }
    
}
