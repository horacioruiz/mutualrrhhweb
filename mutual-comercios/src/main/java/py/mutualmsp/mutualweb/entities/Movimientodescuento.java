/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "movimientodescuento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movimientodescuento.findAll", query = "SELECT m FROM Movimientodescuento m")
    , @NamedQuery(name = "Movimientodescuento.findById", query = "SELECT m FROM Movimientodescuento m WHERE m.id = :id")
    , @NamedQuery(name = "Movimientodescuento.findByImporteactual", query = "SELECT m FROM Movimientodescuento m WHERE m.importeactual = :importeactual")
    , @NamedQuery(name = "Movimientodescuento.findByImportesiguiente", query = "SELECT m FROM Movimientodescuento m WHERE m.importesiguiente = :importesiguiente")})
public class Movimientodescuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "importeactual")
    private BigDecimal importeactual;
    @Column(name = "importesiguiente")
    private BigDecimal importesiguiente;
    @JoinColumn(name = "idanalisisdisponibilidad", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Analisis idanalisisdisponibilidad;
    @JoinColumn(name = "iddescuento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Descuento iddescuento;

    public Movimientodescuento() {
    }

    public Movimientodescuento(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getImporteactual() {
        return importeactual;
    }

    public void setImporteactual(BigDecimal importeactual) {
        this.importeactual = importeactual;
    }

    public BigDecimal getImportesiguiente() {
        return importesiguiente;
    }

    public void setImportesiguiente(BigDecimal importesiguiente) {
        this.importesiguiente = importesiguiente;
    }

    public Analisis getIdanalisisdisponibilidad() {
        return idanalisisdisponibilidad;
    }

    public void setIdanalisisdisponibilidad(Analisis idanalisisdisponibilidad) {
        this.idanalisisdisponibilidad = idanalisisdisponibilidad;
    }

    public Descuento getIddescuento() {
        return iddescuento;
    }

    public void setIddescuento(Descuento iddescuento) {
        this.iddescuento = iddescuento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimientodescuento)) {
            return false;
        }
        Movimientodescuento other = (Movimientodescuento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Movimientodescuento[ id=" + id + " ]";
    }
    
}
