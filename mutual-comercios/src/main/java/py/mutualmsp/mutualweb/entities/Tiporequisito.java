/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "tiporequisito")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tiporequisito.findAll", query = "SELECT t FROM Tiporequisito t")
    , @NamedQuery(name = "Tiporequisito.findByIdtiporequisito", query = "SELECT t FROM Tiporequisito t WHERE t.idtiporequisito = :idtiporequisito")
    , @NamedQuery(name = "Tiporequisito.findByDescripcion", query = "SELECT t FROM Tiporequisito t WHERE t.descripcion = :descripcion")})
public class Tiporequisito implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtiporequisito", fetch = FetchType.LAZY)
    private List<Requisito> requisitoList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtiporequisito")
    private Long idtiporequisito;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;

    public Tiporequisito() {
    }

    public Tiporequisito(Long idtiporequisito) {
        this.idtiporequisito = idtiporequisito;
    }

    public Long getIdtiporequisito() {
        return idtiporequisito;
    }

    public void setIdtiporequisito(Long idtiporequisito) {
        this.idtiporequisito = idtiporequisito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtiporequisito != null ? idtiporequisito.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tiporequisito)) {
            return false;
        }
        Tiporequisito other = (Tiporequisito) object;
        if ((this.idtiporequisito == null && other.idtiporequisito != null) || (this.idtiporequisito != null && !this.idtiporequisito.equals(other.idtiporequisito))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Tiporequisito[ idtiporequisito=" + idtiporequisito + " ]";
    }

    @XmlTransient
    public List<Requisito> getRequisitoList() {
        return requisitoList;
    }

    public void setRequisitoList(List<Requisito> requisitoList) {
        this.requisitoList = requisitoList;
    }
    
}
