/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "solicitud_admision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudAdmision.findAll", query = "SELECT s FROM SolicitudAdmision s")
    , @NamedQuery(name = "SolicitudAdmision.findById", query = "SELECT s FROM SolicitudAdmision s WHERE s.id = :id")
    , @NamedQuery(name = "SolicitudAdmision.findByNumerosolicitud", query = "SELECT s FROM SolicitudAdmision s WHERE s.numerosolicitud = :numerosolicitud")
    , @NamedQuery(name = "SolicitudAdmision.findByCedula", query = "SELECT s FROM SolicitudAdmision s WHERE s.cedula = :cedula")
    , @NamedQuery(name = "SolicitudAdmision.findByNombre", query = "SELECT s FROM SolicitudAdmision s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "SolicitudAdmision.findByApellido", query = "SELECT s FROM SolicitudAdmision s WHERE s.apellido = :apellido")
    , @NamedQuery(name = "SolicitudAdmision.findByFechanacimiento", query = "SELECT s FROM SolicitudAdmision s WHERE s.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "SolicitudAdmision.findByTelefonocelular", query = "SELECT s FROM SolicitudAdmision s WHERE s.telefonocelular = :telefonocelular")
    , @NamedQuery(name = "SolicitudAdmision.findByTelefonolineabaja", query = "SELECT s FROM SolicitudAdmision s WHERE s.telefonolineabaja = :telefonolineabaja")
    , @NamedQuery(name = "SolicitudAdmision.findByEmail", query = "SELECT s FROM SolicitudAdmision s WHERE s.email = :email")
    , @NamedQuery(name = "SolicitudAdmision.findByDireccion", query = "SELECT s FROM SolicitudAdmision s WHERE s.direccion = :direccion")
    , @NamedQuery(name = "SolicitudAdmision.findByNumerocasa", query = "SELECT s FROM SolicitudAdmision s WHERE s.numerocasa = :numerocasa")
    , @NamedQuery(name = "SolicitudAdmision.findByExsocio", query = "SELECT s FROM SolicitudAdmision s WHERE s.exsocio = :exsocio")
    , @NamedQuery(name = "SolicitudAdmision.findByFechadesvinculacion", query = "SELECT s FROM SolicitudAdmision s WHERE s.fechadesvinculacion = :fechadesvinculacion")
    , @NamedQuery(name = "SolicitudAdmision.findByFechaingreso", query = "SELECT s FROM SolicitudAdmision s WHERE s.fechaingreso = :fechaingreso")
    , @NamedQuery(name = "SolicitudAdmision.findBySeccion", query = "SELECT s FROM SolicitudAdmision s WHERE s.seccion = :seccion")
    , @NamedQuery(name = "SolicitudAdmision.findByReunerequisitos", query = "SELECT s FROM SolicitudAdmision s WHERE s.reunerequisitos = :reunerequisitos")
    , @NamedQuery(name = "SolicitudAdmision.findByObservaciones", query = "SELECT s FROM SolicitudAdmision s WHERE s.observaciones = :observaciones")
    , @NamedQuery(name = "SolicitudAdmision.findByNumeroacta", query = "SELECT s FROM SolicitudAdmision s WHERE s.numeroacta = :numeroacta")
    , @NamedQuery(name = "SolicitudAdmision.findByFechaacta", query = "SELECT s FROM SolicitudAdmision s WHERE s.fechaacta = :fechaacta")
    , @NamedQuery(name = "SolicitudAdmision.findByAprobado", query = "SELECT s FROM SolicitudAdmision s WHERE s.aprobado = :aprobado")
    , @NamedQuery(name = "SolicitudAdmision.findByObservacioncomite", query = "SELECT s FROM SolicitudAdmision s WHERE s.observacioncomite = :observacioncomite")
    , @NamedQuery(name = "SolicitudAdmision.findByNumeroresolucion", query = "SELECT s FROM SolicitudAdmision s WHERE s.numeroresolucion = :numeroresolucion")
    , @NamedQuery(name = "SolicitudAdmision.findByFecharesolucion", query = "SELECT s FROM SolicitudAdmision s WHERE s.fecharesolucion = :fecharesolucion")
    , @NamedQuery(name = "SolicitudAdmision.findByNumerosesion", query = "SELECT s FROM SolicitudAdmision s WHERE s.numerosesion = :numerosesion")
    , @NamedQuery(name = "SolicitudAdmision.findByApruebacomision", query = "SELECT s FROM SolicitudAdmision s WHERE s.apruebacomision = :apruebacomision")})
public class SolicitudAdmision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "numerosolicitud")
    private Long numerosolicitud;
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Column(name = "telefonocelular")
    private String telefonocelular;
    @Column(name = "telefonolineabaja")
    private String telefonolineabaja;
    @Column(name = "email")
    private String email;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "numerocasa")
    private Long numerocasa;
    @Column(name = "exsocio")
    private Boolean exsocio;
    @Column(name = "fechadesvinculacion")
    @Temporal(TemporalType.DATE)
    private Date fechadesvinculacion;
    @Column(name = "fechaingreso")
    @Temporal(TemporalType.DATE)
    private Date fechaingreso;
    @Column(name = "seccion")
    private String seccion;
    @Column(name = "reunerequisitos")
    private Boolean reunerequisitos;
    @Column(name = "observaciones")
    private String observaciones;
    @Column(name = "numeroacta")
    private Long numeroacta;
    @Column(name = "fechaacta")
    @Temporal(TemporalType.DATE)
    private Date fechaacta;
    @Column(name = "aprobado")
    private Boolean aprobado;
    @Column(name = "observacioncomite")
    private String observacioncomite;
    @Column(name = "numeroresolucion")
    private Long numeroresolucion;
    @Column(name = "fecharesolucion")
    @Temporal(TemporalType.DATE)
    private Date fecharesolucion;
    @Column(name = "numerosesion")
    private Long numerosesion;
    @Column(name = "apruebacomision")
    private Boolean apruebacomision;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitudAdmision", fetch = FetchType.LAZY)
    private List<ReferenciaPersonalAdmision> referenciaPersonalAdmisionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitudAdmision", fetch = FetchType.LAZY)
    private List<BeneficiarioAdmision> beneficiarioAdmisionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitudAdmision", fetch = FetchType.LAZY)
    private List<AdjuntosAdmision> adjuntosAdmisionList;
    @JoinColumn(name = "idformacion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FormacionAcademica idformacion;
    @JoinColumn(name = "idbarrio", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Barrio idbarrio;
    @JoinColumn(name = "idcargo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cargo idcargo;
    @JoinColumn(name = "idciudadresidencia", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad idciudadresidencia;
    @JoinColumn(name = "idpromotor", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Promotor idpromotor;
    @JoinColumn(name = "idlugarnacimiento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad idlugarnacimiento;
    @JoinColumn(name = "idestadosolicitud", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EstadoSolicitudAdmision idestadosolicitud;
    @JoinColumn(name = "idsexo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sexo idsexo;
    @JoinColumn(name = "idestadocivil", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Estadocivil idestadocivil;
    @JoinColumn(name = "idrubro", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Rubro idrubro;
    @JoinColumn(name = "idprofesion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profesion idprofesion;
    @JoinColumn(name = "idtipocasa", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipocasa idtipocasa;
    @JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario idfuncionario;
    @JoinColumn(name = "idgiraduria", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Giraduria idgiraduria;
    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Institucion idinstitucion;
    @JoinColumn(name = "idnacionalidad", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Pais idnacionalidad;

    public SolicitudAdmision() {
    }

    public SolicitudAdmision(Long id) {
        this.id = id;
    }

    public SolicitudAdmision(Long id, long numerosolicitud) {
        this.id = id;
        this.numerosolicitud = numerosolicitud;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumerosolicitud() {
        return numerosolicitud;
    }

    public void setNumerosolicitud(Long numerosolicitud) {
        this.numerosolicitud = numerosolicitud;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getTelefonocelular() {
        return telefonocelular;
    }

    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }

    public String getTelefonolineabaja() {
        return telefonolineabaja;
    }

    public void setTelefonolineabaja(String telefonolineabaja) {
        this.telefonolineabaja = telefonolineabaja;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getNumerocasa() {
        return numerocasa;
    }

    public void setNumerocasa(Long numerocasa) {
        this.numerocasa = numerocasa;
    }

    public Boolean getExsocio() {
        return exsocio;
    }

    public void setExsocio(Boolean exsocio) {
        this.exsocio = exsocio;
    }

    public Date getFechadesvinculacion() {
        return fechadesvinculacion;
    }

    public void setFechadesvinculacion(Date fechadesvinculacion) {
        this.fechadesvinculacion = fechadesvinculacion;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public Boolean getReunerequisitos() {
        return reunerequisitos;
    }

    public void setReunerequisitos(Boolean reunerequisitos) {
        this.reunerequisitos = reunerequisitos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getNumeroacta() {
        return numeroacta;
    }

    public void setNumeroacta(Long numeroacta) {
        this.numeroacta = numeroacta;
    }

    public Date getFechaacta() {
        return fechaacta;
    }

    public void setFechaacta(Date fechaacta) {
        this.fechaacta = fechaacta;
    }

    public Boolean getAprobado() {
        return aprobado;
    }

    public void setAprobado(Boolean aprobado) {
        this.aprobado = aprobado;
    }

    public String getObservacioncomite() {
        return observacioncomite;
    }

    public void setObservacioncomite(String observacioncomite) {
        this.observacioncomite = observacioncomite;
    }

    public Long getNumeroresolucion() {
        return numeroresolucion;
    }

    public void setNumeroresolucion(Long numeroresolucion) {
        this.numeroresolucion = numeroresolucion;
    }

    public Date getFecharesolucion() {
        return fecharesolucion;
    }

    public void setFecharesolucion(Date fecharesolucion) {
        this.fecharesolucion = fecharesolucion;
    }

    public Long getNumerosesion() {
        return numerosesion;
    }

    public void setNumerosesion(Long numerosesion) {
        this.numerosesion = numerosesion;
    }

    public Boolean getApruebacomision() {
        return apruebacomision;
    }

    public void setApruebacomision(Boolean apruebacomision) {
        this.apruebacomision = apruebacomision;
    }

    @XmlTransient
    public List<ReferenciaPersonalAdmision> getReferenciaPersonalAdmisionList() {
        return referenciaPersonalAdmisionList;
    }

    public void setReferenciaPersonalAdmisionList(List<ReferenciaPersonalAdmision> referenciaPersonalAdmisionList) {
        this.referenciaPersonalAdmisionList = referenciaPersonalAdmisionList;
    }

    @XmlTransient
    public List<BeneficiarioAdmision> getBeneficiarioAdmisionList() {
        return beneficiarioAdmisionList;
    }

    public void setBeneficiarioAdmisionList(List<BeneficiarioAdmision> beneficiarioAdmisionList) {
        this.beneficiarioAdmisionList = beneficiarioAdmisionList;
    }

    @XmlTransient
    public List<AdjuntosAdmision> getAdjuntosAdmisionList() {
        return adjuntosAdmisionList;
    }

    public void setAdjuntosAdmisionList(List<AdjuntosAdmision> adjuntosAdmisionList) {
        this.adjuntosAdmisionList = adjuntosAdmisionList;
    }

    public FormacionAcademica getIdformacion() {
        return idformacion;
    }

    public void setIdformacion(FormacionAcademica idformacion) {
        this.idformacion = idformacion;
    }

    public Barrio getIdbarrio() {
        return idbarrio;
    }

    public void setIdbarrio(Barrio idbarrio) {
        this.idbarrio = idbarrio;
    }

    public Cargo getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(Cargo idcargo) {
        this.idcargo = idcargo;
    }

    public Ciudad getIdciudadresidencia() {
        return idciudadresidencia;
    }

    public void setIdciudadresidencia(Ciudad idciudadresidencia) {
        this.idciudadresidencia = idciudadresidencia;
    }

    public Promotor getIdpromotor() {
        return idpromotor;
    }

    public void setIdpromotor(Promotor idpromotor) {
        this.idpromotor = idpromotor;
    }

    public Ciudad getIdlugarnacimiento() {
        return idlugarnacimiento;
    }

    public void setIdlugarnacimiento(Ciudad idlugarnacimiento) {
        this.idlugarnacimiento = idlugarnacimiento;
    }

    public EstadoSolicitudAdmision getIdestadosolicitud() {
        return idestadosolicitud;
    }

    public void setIdestadosolicitud(EstadoSolicitudAdmision idestadosolicitud) {
        this.idestadosolicitud = idestadosolicitud;
    }

    public Sexo getIdsexo() {
        return idsexo;
    }

    public void setIdsexo(Sexo idsexo) {
        this.idsexo = idsexo;
    }

    public Estadocivil getIdestadocivil() {
        return idestadocivil;
    }

    public void setIdestadocivil(Estadocivil idestadocivil) {
        this.idestadocivil = idestadocivil;
    }

    public Rubro getIdrubro() {
        return idrubro;
    }

    public void setIdrubro(Rubro idrubro) {
        this.idrubro = idrubro;
    }

    public Profesion getIdprofesion() {
        return idprofesion;
    }

    public void setIdprofesion(Profesion idprofesion) {
        this.idprofesion = idprofesion;
    }

    public Tipocasa getIdtipocasa() {
        return idtipocasa;
    }

    public void setIdtipocasa(Tipocasa idtipocasa) {
        this.idtipocasa = idtipocasa;
    }

    public Funcionario getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Giraduria getIdgiraduria() {
        return idgiraduria;
    }

    public void setIdgiraduria(Giraduria idgiraduria) {
        this.idgiraduria = idgiraduria;
    }

    public Institucion getIdinstitucion() {
        return idinstitucion;
    }

    public void setIdinstitucion(Institucion idinstitucion) {
        this.idinstitucion = idinstitucion;
    }

    public Pais getIdnacionalidad() {
        return idnacionalidad;
    }

    public void setIdnacionalidad(Pais idnacionalidad) {
        this.idnacionalidad = idnacionalidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudAdmision)) {
            return false;
        }
        SolicitudAdmision other = (SolicitudAdmision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.SolicitudAdmision[ id=" + id + " ]";
    }
    
}
