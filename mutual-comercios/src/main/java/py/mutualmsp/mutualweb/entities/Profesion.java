/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "profesion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesion.findAll", query = "SELECT p FROM Profesion p")
    , @NamedQuery(name = "Profesion.findById", query = "SELECT p FROM Profesion p WHERE p.id = :id")
    , @NamedQuery(name = "Profesion.findByDescripcion", query = "SELECT p FROM Profesion p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Profesion.findByActivo", query = "SELECT p FROM Profesion p WHERE p.activo = :activo")})
public class Profesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "activo")
    private Boolean activo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idprofesion", fetch = FetchType.LAZY)
    private List<BeneficiarioAdmision> beneficiarioAdmisionList;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idprofesion", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;

    public Profesion() {
    }

    public Profesion(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @XmlTransient
    public List<BeneficiarioAdmision> getBeneficiarioAdmisionList() {
        return beneficiarioAdmisionList;
    }

    public void setBeneficiarioAdmisionList(List<BeneficiarioAdmision> beneficiarioAdmisionList) {
        this.beneficiarioAdmisionList = beneficiarioAdmisionList;
    }

    /*@XmlTransient
    public List<SolicitudAdmision> getSolicitudAdmisionList() {
        return solicitudAdmisionList;
    }

    public void setSolicitudAdmisionList(List<SolicitudAdmision> solicitudAdmisionList) {
        this.solicitudAdmisionList = solicitudAdmisionList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesion)) {
            return false;
        }
        Profesion other = (Profesion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Profesion[ id=" + id + " ]";
    }
    
}
