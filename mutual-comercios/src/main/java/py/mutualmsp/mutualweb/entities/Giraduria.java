/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "giraduria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Giraduria.findAll", query = "SELECT g FROM Giraduria g")
    , @NamedQuery(name = "Giraduria.findById", query = "SELECT g FROM Giraduria g WHERE g.id = :id")
    , @NamedQuery(name = "Giraduria.findByDescripcion", query = "SELECT g FROM Giraduria g WHERE g.descripcion = :descripcion")
    , @NamedQuery(name = "Giraduria.findByDescripcionbreve", query = "SELECT g FROM Giraduria g WHERE g.descripcionbreve = :descripcionbreve")
    , @NamedQuery(name = "Giraduria.findByContacto", query = "SELECT g FROM Giraduria g WHERE g.contacto = :contacto")
    , @NamedQuery(name = "Giraduria.findByCargo", query = "SELECT g FROM Giraduria g WHERE g.cargo = :cargo")
    , @NamedQuery(name = "Giraduria.findByTratamiento", query = "SELECT g FROM Giraduria g WHERE g.tratamiento = :tratamiento")})
public class Giraduria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "descripcionbreve")
    private String descripcionbreve;
    @Column(name = "contacto")
    private String contacto;
    @Column(name = "cargo")
    private String cargo;
    @Column(name = "tratamiento")
    private String tratamiento;

    public Giraduria() {
    }

    public Giraduria(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionbreve() {
        return descripcionbreve;
    }

    public void setDescripcionbreve(String descripcionbreve) {
        this.descripcionbreve = descripcionbreve;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Giraduria)) {
            return false;
        }
        Giraduria other = (Giraduria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Giraduria[ id=" + id + " ]";
    }
    
}
