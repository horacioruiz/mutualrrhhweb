/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "solicitud", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Solicitud.findAll", query = "SELECT d FROM Solicitud d")
    , @NamedQuery(name = "Solicitud.findById", query = "SELECT d FROM Solicitud d WHERE d.id = :id")
})
public class Solicitud implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "cargofunc")
    private String cargofunc;

    @Column(name = "areafunc")
    private String areafunc;

    @Column(name = "rechazo")
    private String rechazo;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "periodovacas")
    private String periodovacas;

    @Column(name = "cantdia")
    private Long cantdia;

    @Column(name = "fechacreacion")
    private Date fechacreacion;

    @Column(name = "fechaini")
    private Date fechaini;

    @Column(name = "fechafin")
    private Date fechafin;

    @Column(name = "dependencia")
    private String dependencia;

    @Column(name = "aprobado")
    private Long aprobado;

    @Column(name = "horaini")
    private Timestamp horaini;

    @Column(name = "horafin")
    private Timestamp horafin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idfuncionario")
    private Funcionario funcionario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idformulario")
    private Formulario formulario;

    @JoinColumn(name = "idencargado", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario encargado;

    @JoinColumn(name = "idencargado2", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario encargado2;

    @JoinColumn(name = "idfuncrrhh", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcrrhh;

    public Solicitud() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechafin() {
        return fechafin;
    }

    public void setFechafin(Date fechafin) {
        this.fechafin = fechafin;
    }

    public String getRechazo() {
        return rechazo;
    }

    public Funcionario getEncargado2() {
        return encargado2;
    }

    public void setEncargado2(Funcionario encargado2) {
        this.encargado2 = encargado2;
    }

    public void setRechazo(String rechazo) {
        this.rechazo = rechazo;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public String getPeriodovacas() {
        return periodovacas;
    }

    public void setPeriodovacas(String periodovacas) {
        this.periodovacas = periodovacas;
    }

    public String getCargofunc() {
        return cargofunc;
    }

    public Timestamp getHoraini() {
        return horaini;
    }

    public void setHoraini(Timestamp horaini) {
        this.horaini = horaini;
    }

    public Timestamp getHorafin() {
        return horafin;
    }

    public void setHorafin(Timestamp horafin) {
        this.horafin = horafin;
    }

    public void setCargofunc(String cargofunc) {
        this.cargofunc = cargofunc;
    }

    public String getAreafunc() {
        return areafunc;
    }

    public void setAreafunc(String areafunc) {
        this.areafunc = areafunc;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getCantdia() {
        return cantdia;
    }

    public void setCantdia(Long cantdia) {
        this.cantdia = cantdia;
    }

    public Date getFechaini() {
        return fechaini;
    }

    public void setFechaini(Date fechaini) {
        this.fechaini = fechaini;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Formulario getFormulario() {
        return formulario;
    }

    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public Funcionario getEncargado() {
        return encargado;
    }

    public void setEncargado(Funcionario encargado) {
        this.encargado = encargado;
    }

    public Funcionario getFuncrrhh() {
        return funcrrhh;
    }

    public void setFuncrrhh(Funcionario funcrrhh) {
        this.funcrrhh = funcrrhh;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.aprobado == 1) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (this.aprobado == 2) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/pending.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    public Image getConfirmadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
//        if (this.confirmado != null && this.confirmado) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/goodConfirm.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } else {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/badConfirm.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        }
        return null;
    }

    public Long getAprobado() {
        return aprobado;
    }

    public void setAprobado(Long aprobado) {
        this.aprobado = aprobado;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitud)) {
            return false;
        }
        Solicitud other = (Solicitud) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Solicitud[ id=" + id + " ]";
    }

}
