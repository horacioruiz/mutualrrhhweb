/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "rubro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rubro.findAll", query = "SELECT r FROM Rubro r")
    , @NamedQuery(name = "Rubro.findById", query = "SELECT r FROM Rubro r WHERE r.id = :id")
    , @NamedQuery(name = "Rubro.findByDescripcion", query = "SELECT r FROM Rubro r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Rubro.findByJubilacion", query = "SELECT r FROM Rubro r WHERE r.jubilacion = :jubilacion")
    , @NamedQuery(name = "Rubro.findByIva", query = "SELECT r FROM Rubro r WHERE r.iva = :iva")
    , @NamedQuery(name = "Rubro.findByDescripcionBreve", query = "SELECT r FROM Rubro r WHERE r.descripcionBreve = :descripcionBreve")})
public class Rubro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "jubilacion")
    private Integer jubilacion;
    @Column(name = "iva")
    private Integer iva;
    @Column(name = "descripcion_breve")
    private String descripcionBreve;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrubro", fetch = FetchType.LAZY)
    private List<Analisis> analisisList;
    @OneToMany(mappedBy = "idrubro", fetch = FetchType.LAZY)
    private List<LugarLaboral> lugarLaboralList;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idrubro", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;

    public Rubro() {
    }

    public Rubro(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getJubilacion() {
        return jubilacion;
    }

    public void setJubilacion(Integer jubilacion) {
        this.jubilacion = jubilacion;
    }

    public Integer getIva() {
        return iva;
    }

    public void setIva(Integer iva) {
        this.iva = iva;
    }

    public String getDescripcionBreve() {
        return descripcionBreve;
    }

    public void setDescripcionBreve(String descripcionBreve) {
        this.descripcionBreve = descripcionBreve;
    }

    @XmlTransient
    public List<Analisis> getAnalisisList() {
        return analisisList;
    }

    public void setAnalisisList(List<Analisis> analisisList) {
        this.analisisList = analisisList;
    }

    @XmlTransient
    public List<LugarLaboral> getLugarLaboralList() {
        return lugarLaboralList;
    }

    public void setLugarLaboralList(List<LugarLaboral> lugarLaboralList) {
        this.lugarLaboralList = lugarLaboralList;
    }

    /*@XmlTransient
    public List<SolicitudAdmision> getSolicitudAdmisionList() {
        return solicitudAdmisionList;
    }

    public void setSolicitudAdmisionList(List<SolicitudAdmision> solicitudAdmisionList) {
        this.solicitudAdmisionList = solicitudAdmisionList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rubro)) {
            return false;
        }
        Rubro other = (Rubro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Rubro[ id=" + id + " ]";
    }
    
}
