/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "promotor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promotor.findAll", query = "SELECT p FROM Promotor p")
    , @NamedQuery(name = "Promotor.findById", query = "SELECT p FROM Promotor p WHERE p.id = :id")})
public class Promotor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionario idfuncionario;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idpromotor", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;

    @Transient
    private String nombreFuncionario;

    public String getNombreFuncionario() {
        return nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
            this.nombreFuncionario = nombreFuncionario;      
    }
    
    public Promotor() {
    }

    public Promotor(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    /*@XmlTransient
    public List<SolicitudAdmision> getSolicitudAdmisionList() {
        return solicitudAdmisionList;
    }

    public void setSolicitudAdmisionList(List<SolicitudAdmision> solicitudAdmisionList) {
        this.solicitudAdmisionList = solicitudAdmisionList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promotor)) {
            return false;
        }
        Promotor other = (Promotor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Promotor[ id=" + id + " ]";
    }
    
}
