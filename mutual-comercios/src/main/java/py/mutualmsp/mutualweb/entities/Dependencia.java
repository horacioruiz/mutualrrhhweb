/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "dependencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dependencia.findAll", query = "SELECT d FROM Dependencia d")
    , @NamedQuery(name = "Dependencia.findByIddependencia", query = "SELECT d FROM Dependencia d WHERE d.iddependencia = :iddependencia")
    , @NamedQuery(name = "Dependencia.findByDescripcion", query = "SELECT d FROM Dependencia d WHERE d.descripcion = :descripcion")})
public class Dependencia implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddependencia", fetch = FetchType.LAZY)
    private List<Verificacion> verificacionList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddependencia")
    private Long iddependencia;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "nivel")
    private String nivel;
    @Column(name = "iddependenciapadre")
    private Long iddependenciapadre;
    @Column(name = "idsubtipo")
    private Long idsubtipo;

    public Dependencia() {
    }

    public Long getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(Long iddependencia) {
        this.iddependencia = iddependencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Long getIddependenciapadre() {
        return iddependenciapadre;
    }

    public void setIddependenciapadre(Long iddependenciapadre) {
        this.iddependenciapadre = iddependenciapadre;
    }

    public Long getIdsubtipo() {
        return idsubtipo;
    }

    public void setIdsubtipo(Long idsubtipo) {
        this.idsubtipo = idsubtipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddependencia != null ? iddependencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dependencia)) {
            return false;
        }
        Dependencia other = (Dependencia) object;
        if ((this.iddependencia == null && other.iddependencia != null) || (this.iddependencia != null && !this.iddependencia.equals(other.iddependencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Dependencia[ iddependencia=" + iddependencia + " ]";
    }

    @XmlTransient
    public List<Verificacion> getVerificacionList() {
        return verificacionList;
    }

    public void setVerificacionList(List<Verificacion> verificacionList) {
        this.verificacionList = verificacionList;
    }

}
