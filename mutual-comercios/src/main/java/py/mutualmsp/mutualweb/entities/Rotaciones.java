/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "rotaciones", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rotaciones.findAll", query = "SELECT d FROM Rotaciones d")
    , @NamedQuery(name = "Rotaciones.findById", query = "SELECT d FROM Rotaciones d WHERE d.id = :id")})
public class Rotaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcionario;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "nombrefuncionario")
    private String nombrefuncionario;

    @Column(name = "cargofunc")
    private String cargofunc;

    @Column(name = "areafunc")
    private String areafunc;

    public Rotaciones() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombrefuncionario() {
        return nombrefuncionario;
    }

    public void setNombrefuncionario(String nombrefuncionario) {
        this.nombrefuncionario = nombrefuncionario;
    }

    public String getCargofunc() {
        return cargofunc;
    }

    public void setCargofunc(String cargofunc) {
        this.cargofunc = cargofunc;
    }

    public String getAreafunc() {
        return areafunc;
    }

    public void setAreafunc(String areafunc) {
        this.areafunc = areafunc;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rotaciones)) {
            return false;
        }
        Rotaciones other = (Rotaciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Rotaciones[ id=" + id + " ]";
    }

}
