/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "lugar_laboral")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LugarLaboral.findAll", query = "SELECT l FROM LugarLaboral l")
    , @NamedQuery(name = "LugarLaboral.findById", query = "SELECT l FROM LugarLaboral l WHERE l.id = :id")
    , @NamedQuery(name = "LugarLaboral.findByIdmoneda", query = "SELECT l FROM LugarLaboral l WHERE l.idmoneda = :idmoneda")
    , @NamedQuery(name = "LugarLaboral.findByNumeroBeneficiario", query = "SELECT l FROM LugarLaboral l WHERE l.numeroBeneficiario = :numeroBeneficiario")})
public class LugarLaboral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "idmoneda")
    private BigInteger idmoneda;
    @Column(name = "numero_beneficiario")
    private BigInteger numeroBeneficiario;
    @JoinColumn(name = "idgiraduria", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Giraduria idgiraduria;
    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institucion idinstitucion;
    @JoinColumn(name = "idregion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Region idregion;
    @JoinColumn(name = "idrubro", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Rubro idrubro;

    public LugarLaboral() {
    }

    public LugarLaboral(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(BigInteger idmoneda) {
        this.idmoneda = idmoneda;
    }

    public BigInteger getNumeroBeneficiario() {
        return numeroBeneficiario;
    }

    public void setNumeroBeneficiario(BigInteger numeroBeneficiario) {
        this.numeroBeneficiario = numeroBeneficiario;
    }

    public Giraduria getIdgiraduria() {
        return idgiraduria;
    }

    public void setIdgiraduria(Giraduria idgiraduria) {
        this.idgiraduria = idgiraduria;
    }

    public Institucion getIdinstitucion() {
        return idinstitucion;
    }

    public void setIdinstitucion(Institucion idinstitucion) {
        this.idinstitucion = idinstitucion;
    }

    public Region getIdregion() {
        return idregion;
    }

    public void setIdregion(Region idregion) {
        this.idregion = idregion;
    }

    public Rubro getIdrubro() {
        return idrubro;
    }

    public void setIdrubro(Rubro idrubro) {
        this.idrubro = idrubro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LugarLaboral)) {
            return false;
        }
        LugarLaboral other = (LugarLaboral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.LugarLaboral[ id=" + id + " ]";
    }
    
}
