/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "boleta_det", schema = "comercios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BoletaDet.findAll", query = "SELECT p FROM BoletaDet p")
})
public class BoletaDet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "idboletacab")
    private long idboletacab;

    @Column(name = "importe")
    private Long importe;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "nroboleta")
    private String nroboleta;

    @Column(name = "fechaemision")
    private Date fechaemision;

    @Column(name = "fechavto")
    private Date fechavto;

    @Column(name = "nrorden")
    private String nrorden;

    @Column(name = "idsocio")
    private Long idsocio;

    @Column(name = "idfuncionario")
    private Long idfuncionario;

    @Column(name = "montocuota")
    private Long montocuota;

    @Column(name = "cuota")
    private Long cuota;

    public BoletaDet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdboletacab() {
        return idboletacab;
    }

    public String getNroboleta() {
        return nroboleta;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public Date getFechavto() {
        return fechavto;
    }

    public void setFechavto(Date fechavto) {
        this.fechavto = fechavto;
    }

    public String getNrorden() {
        return nrorden;
    }

    public void setNrorden(String nrorden) {
        this.nrorden = nrorden;
    }

    public Long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(Long idsocio) {
        this.idsocio = idsocio;
    }

    public Long getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Long idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Long getMontocuota() {
        return montocuota;
    }

    public void setMontocuota(Long montocuota) {
        this.montocuota = montocuota;
    }

    public Long getCuota() {
        return cuota;
    }

    public void setCuota(Long cuota) {
        this.cuota = cuota;
    }

    public void setNroboleta(String nroboleta) {
        this.nroboleta = nroboleta;
    }

    public void setIdboletacab(long idboletacab) {
        this.idboletacab = idboletacab;
    }

    public Long getImporte() {
        return importe;
    }

    public void setImporte(Long importe) {
        this.importe = importe;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BoletaDet)) {
            return false;
        }
        BoletaDet other = (BoletaDet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.BoletaDet[ id=" + id + " ]";
    }

}
