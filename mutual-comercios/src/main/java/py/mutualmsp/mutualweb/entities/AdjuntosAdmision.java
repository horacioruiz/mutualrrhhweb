/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "adjuntos_admision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdjuntosAdmision.findAll", query = "SELECT a FROM AdjuntosAdmision a")
    , @NamedQuery(name = "AdjuntosAdmision.findById", query = "SELECT a FROM AdjuntosAdmision a WHERE a.id = :id")
    , @NamedQuery(name = "AdjuntosAdmision.findByRutaacceso", query = "SELECT a FROM AdjuntosAdmision a WHERE a.rutaacceso = :rutaacceso")})
public class AdjuntosAdmision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "rutaacceso")
    private String rutaacceso;
    @JoinColumn(name = "iddocumento", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Documento iddocumento;
    @JoinColumn(name = "idsolicitud_admision", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudAdmision idsolicitudAdmision;

    public AdjuntosAdmision() {
    }

    public AdjuntosAdmision(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRutaacceso() {
        return rutaacceso;
    }

    public void setRutaacceso(String rutaacceso) {
        this.rutaacceso = rutaacceso;
    }

    public Documento getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(Documento iddocumento) {
        this.iddocumento = iddocumento;
    }

    public SolicitudAdmision getIdsolicitudAdmision() {
        return idsolicitudAdmision;
    }

    public void setIdsolicitudAdmision(SolicitudAdmision idsolicitudAdmision) {
        this.idsolicitudAdmision = idsolicitudAdmision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdjuntosAdmision)) {
            return false;
        }
        AdjuntosAdmision other = (AdjuntosAdmision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.AdjuntosAdmision[ id=" + id + " ]";
    }
    
}
