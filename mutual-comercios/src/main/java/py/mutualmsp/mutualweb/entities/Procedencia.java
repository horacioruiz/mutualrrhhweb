/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "procedencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procedencia.findAll", query = "SELECT p FROM Procedencia p")
    , @NamedQuery(name = "Procedencia.findByIdmodalidad", query = "SELECT p FROM Procedencia p WHERE p.idprocedencia = :idprocedencia")
    , @NamedQuery(name = "Procedencia.findByDescripcion", query = "SELECT p FROM Procedencia p WHERE p.descripcion = :descripcion")})
public class Procedencia implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idprocedencia", fetch = FetchType.LAZY)
    private List<Verificacion> verificacionList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idprocedencia")
    private Long idprocedencia;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;

    public Procedencia() {
    }

    public Procedencia(Long idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    public Long getIdprocedencia() {
        return idprocedencia;
    }

    public void setIdprocedencia(Long idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprocedencia != null ? idprocedencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procedencia)) {
            return false;
        }
        Procedencia other = (Procedencia) object;
        if ((this.idprocedencia == null && other.idprocedencia != null) || (this.idprocedencia != null && !this.idprocedencia.equals(other.idprocedencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Procedencia[ idprocedencia=" + idprocedencia + " ]";
    }

    @XmlTransient
    public List<Verificacion> getVerificacionList() {
        return verificacionList;
    }

    public void setVerificacionList(List<Verificacion> verificacionList) {
        this.verificacionList = verificacionList;
    }
    
}
