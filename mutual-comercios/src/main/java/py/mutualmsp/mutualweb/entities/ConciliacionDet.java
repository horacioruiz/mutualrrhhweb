/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "conciliacion_det", schema = "comercios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConciliacionDet.findAll", query = "SELECT p FROM ConciliacionDet p")
})
public class ConciliacionDet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "idconciliacioncab")
    private long idconciliacioncab;

    @Column(name = "idcomercio")
    private long idcomercio;

    @Column(name = "comercio")
    private String comercio;

    @Column(name = "saldoant")
    private Long saldoant;

    @Column(name = "movmes")
    private Long movmes;

    @Column(name = "totalorden")
    private Long totalorden;

    @Column(name = "pagado")
    private Long pagado;

    @Column(name = "difep")
    private Long difep;

    @Column(name = "saldoapagar")
    private Long saldoapagar;

    @Column(name = "saldosinpagar")
    private Long saldosinpagar;

    @Column(name = "nopresentado")
    private Long nopresentado;

    @Column(name = "apagar")
    private Long apagar;

    @Column(name = "concilia")
    private Long concilia;

    @Column(name = "dif")
    private Long dif;

    @Column(name = "verificado")
    private Boolean verificado;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "fechamod")
    private String fechamod;

    @Column(name = "pagado2")
    private Long pagado2;

    public ConciliacionDet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdconciliacioncab() {
        return idconciliacioncab;
    }

    public void setIdconciliacioncab(long idconciliacioncab) {
        this.idconciliacioncab = idconciliacioncab;
    }

    public long getIdcomercio() {
        return idcomercio;
    }

    public void setIdcomercio(long idcomercio) {
        this.idcomercio = idcomercio;
    }

    public String getComercio() {
        return comercio;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public Long getSaldoant() {
        return saldoant;
    }

    public void setSaldoant(Long saldoant) {
        this.saldoant = saldoant;
    }

    public Long getMovmes() {
        return movmes;
    }

    public void setMovmes(Long movmes) {
        this.movmes = movmes;
    }

    public Long getTotalorden() {
        return totalorden;
    }

    public void setTotalorden(Long totalorden) {
        this.totalorden = totalorden;
    }

    public Long getPagado() {
        return pagado;
    }

    public Long getPagado2() {
        return pagado2;
    }

    public void setPagado2(Long pagado2) {
        this.pagado2 = pagado2;
    }

    public void setPagado(Long pagado) {
        this.pagado = pagado;
    }

    public Long getDifep() {
        return difep;
    }

    public void setDifep(Long difep) {
        this.difep = difep;
    }

    public Long getSaldoapagar() {
        return saldoapagar;
    }

    public void setSaldoapagar(Long saldoapagar) {
        this.saldoapagar = saldoapagar;
    }

    public Long getSaldosinpagar() {
        return saldosinpagar;
    }

    public void setSaldosinpagar(Long saldosinpagar) {
        this.saldosinpagar = saldosinpagar;
    }

    public Long getNopresentado() {
        return nopresentado;
    }

    public void setNopresentado(Long nopresentado) {
        this.nopresentado = nopresentado;
    }

    public Long getApagar() {
        return apagar;
    }

    public void setApagar(Long apagar) {
        this.apagar = apagar;
    }

    public Long getConcilia() {
        return concilia;
    }

    public void setConcilia(Long concilia) {
        this.concilia = concilia;
    }

    public Long getDif() {
        return dif;
    }

    public void setDif(Long dif) {
        this.dif = dif;
    }

    public Boolean getVerificado() {
        return verificado;
    }

    public void setVerificado(Boolean verificado) {
        this.verificado = verificado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFechamod() {
        return fechamod;
    }

    public void setFechamod(String fechamod) {
        this.fechamod = fechamod;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.verificado == true) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (this.verificado == false) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConciliacionDet)) {
            return false;
        }
        ConciliacionDet other = (ConciliacionDet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.ConciliacionDet[ id=" + id + " ]";
    }

}
