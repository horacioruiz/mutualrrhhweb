/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "control_det", schema = "comercios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ControlDet.findAll", query = "SELECT p FROM ControlDet p")
})
public class ControlDet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "idcontrolcab")
    private long idcontrolcab;

    @Column(name = "nroboleta")
    private String nroboleta;

    @Column(name = "monto")
    private Long monto;

    @Column(name = "cuota")
    private Long cuota;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "idcomercio")
    private long idcomercio;

    @Column(name = "comercio")
    private String comercio;

    @Column(name = "observacion")
    private String observacion;

    public ControlDet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdcontrolcab() {
        return idcontrolcab;
    }

    public void setIdcontrolcab(long idcontrolcab) {
        this.idcontrolcab = idcontrolcab;
    }

    public long getIdcomercio() {
        return idcomercio;
    }

    public void setIdcomercio(long idcomercio) {
        this.idcomercio = idcomercio;
    }

    public String getComercio() {
        return comercio;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public String getNroboleta() {
        return nroboleta;
    }

    public void setNroboleta(String nroboleta) {
        this.nroboleta = nroboleta;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public Long getCuota() {
        return cuota;
    }

    public void setCuota(Long cuota) {
        this.cuota = cuota;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ControlDet)) {
            return false;
        }
        ControlDet other = (ControlDet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.ControlDet[ id=" + id + " ]";
    }

}
