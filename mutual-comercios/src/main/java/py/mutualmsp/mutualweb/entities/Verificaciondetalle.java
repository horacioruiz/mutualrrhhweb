/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "verificaciondetalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verificaciondetalle.findAll", query = "SELECT v FROM Verificaciondetalle v")
    , @NamedQuery(name = "Verificaciondetalle.findByIdverificaciondetalle", query = "SELECT v FROM Verificaciondetalle v WHERE v.idverificaciondetalle = :idverificaciondetalle")
    , @NamedQuery(name = "Verificaciondetalle.findByVerificado", query = "SELECT v FROM Verificaciondetalle v WHERE v.verificado = :verificado")
    , @NamedQuery(name = "Verificaciondetalle.findByNoverificado", query = "SELECT v FROM Verificaciondetalle v WHERE v.noverificado = :noverificado")
    , @NamedQuery(name = "Verificaciondetalle.findByIndiferente", query = "SELECT v FROM Verificaciondetalle v WHERE v.indiferente = :indiferente")
    , @NamedQuery(name = "Verificaciondetalle.findByObservacion", query = "SELECT v FROM Verificaciondetalle v WHERE v.observacion = :observacion")})
public class Verificaciondetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idverificaciondetalle")
    private Long idverificaciondetalle;
    @Column(name = "verificado")
    private Boolean verificado;
    @Column(name = "noverificado")
    private Boolean noverificado;
    @Column(name = "indiferente")
    private Boolean indiferente;
    @Size(max = 150)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "idrequisito", referencedColumnName = "idrequisito")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Requisito idrequisito;
    @JoinColumn(name = "idverificacion", referencedColumnName = "idverificacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Verificacion idverificacion;

    public Verificaciondetalle() {
    }

    public Verificaciondetalle(Long idverificaciondetalle) {
        this.idverificaciondetalle = idverificaciondetalle;
    }

    public Long getIdverificaciondetalle() {
        return idverificaciondetalle;
    }

    public void setIdverificaciondetalle(Long idverificaciondetalle) {
        this.idverificaciondetalle = idverificaciondetalle;
    }

    public Boolean getVerificado() {
        return verificado;
    }

    public void setVerificado(Boolean verificado) {
        this.verificado = verificado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Requisito getIdrequisito() {
        return idrequisito;
    }

    public void setNoverificado(Boolean noverificado) {
        this.noverificado = noverificado;
    }

    public void setIndiferente(Boolean indiferente) {
        this.indiferente = indiferente;
    }

    public Boolean getNoverificado() {
        return noverificado;
    }

    public Boolean getIndiferente() {
        return indiferente;
    }

    public void setIdrequisito(Requisito idrequisito) {
        this.idrequisito = idrequisito;
    }

    public Verificacion getIdverificacion() {
        return idverificacion;
    }

    public void setIdverificacion(Verificacion idverificacion) {
        this.idverificacion = idverificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idverificaciondetalle != null ? idverificaciondetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verificaciondetalle)) {
            return false;
        }
        Verificaciondetalle other = (Verificaciondetalle) object;
        if ((this.idverificaciondetalle == null && other.idverificaciondetalle != null) || (this.idverificaciondetalle != null && !this.idverificaciondetalle.equals(other.idverificaciondetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Verificaciondetalle[ idverificaciondetalle=" + idverificaciondetalle + " ]";
    }
    
}
