/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "regional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regional.findAll", query = "SELECT r FROM Regional r")
    , @NamedQuery(name = "Regional.findById", query = "SELECT r FROM Regional r WHERE r.id = :id")
    , @NamedQuery(name = "Regional.findByDescripcion", query = "SELECT r FROM Regional r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Regional.findByIdprocedencia", query = "SELECT r FROM Regional r WHERE r.idprocedencia = :idprocedencia")
    , @NamedQuery(name = "Regional.findByIddepartamento", query = "SELECT r FROM Regional r WHERE r.iddepartamento = :iddepartamento")})
public class Regional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "idprocedencia")
    private BigInteger idprocedencia;
    @Basic(optional = false)
    @Column(name = "iddepartamento")
    private long iddepartamento;

    public Regional() {
    }

    public Regional(Long id) {
        this.id = id;
    }

    public Regional(Long id, long iddepartamento) {
        this.id = id;
        this.iddepartamento = iddepartamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getIdprocedencia() {
        return idprocedencia;
    }

    public void setIdprocedencia(BigInteger idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    public long getIddepartamento() {
        return iddepartamento;
    }

    public void setIddepartamento(long iddepartamento) {
        this.iddepartamento = iddepartamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regional)) {
            return false;
        }
        Regional other = (Regional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Regional[ id=" + id + " ]";
    }
    
}
