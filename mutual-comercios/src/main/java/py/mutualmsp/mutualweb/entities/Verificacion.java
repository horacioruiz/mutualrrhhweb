/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "verificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verificacion.findAll", query = "SELECT v FROM Verificacion v")
    , @NamedQuery(name = "Verificacion.findByIdverificacion", query = "SELECT v FROM Verificacion v WHERE v.idverificacion = :idverificacion")
    , @NamedQuery(name = "Verificacion.findByFechaverificacion", query = "SELECT v FROM Verificacion v WHERE v.fechaverificacion = :fechaverificacion")
    , @NamedQuery(name = "Verificacion.findByHorarecepcion", query = "SELECT v FROM Verificacion v WHERE v.horarecepcion = :horarecepcion")
    , @NamedQuery(name = "Verificacion.findByHoraremision", query = "SELECT v FROM Verificacion v WHERE v.horaremision = :horaremision")
    , @NamedQuery(name = "Verificacion.findByIdtipooperacion", query = "SELECT v FROM Verificacion v WHERE v.idtipooperacion = :idtipooperacion")
    , @NamedQuery(name = "Verificacion.findByIdmodalidad", query = "SELECT v FROM Verificacion v WHERE v.idmodalidad = :idmodalidad")
    , @NamedQuery(name = "Verificacion.findByCedula", query = "SELECT v FROM Verificacion v WHERE v.cedula = :cedula")
    , @NamedQuery(name = "Verificacion.findByNumerosolicitud", query = "SELECT v FROM Verificacion v WHERE v.numerosolicitud = :numerosolicitud")
    , @NamedQuery(name = "Verificacion.findByNumerooperacion", query = "SELECT v FROM Verificacion v WHERE v.numerooperacion = :numerooperacion")
    , @NamedQuery(name = "Verificacion.findByFechaoperacion", query = "SELECT v FROM Verificacion v WHERE v.fechaoperacion = :fechaoperacion")
    , @NamedQuery(name = "Verificacion.findByNumerocontrato", query = "SELECT v FROM Verificacion v WHERE v.numerocontrato = :numerocontrato")
    , @NamedQuery(name = "Verificacion.findByNumeroboleta", query = "SELECT v FROM Verificacion v WHERE v.numeroboleta = :numeroboleta")
    , @NamedQuery(name = "Verificacion.findByNumeroordencredito", query = "SELECT v FROM Verificacion v WHERE v.numeroordencredito = :numeroordencredito")
    , @NamedQuery(name = "Verificacion.findByNumerofactura", query = "SELECT v FROM Verificacion v WHERE v.numerofactura = :numerofactura")
    , @NamedQuery(name = "Verificacion.findByNumeronotacredito", query = "SELECT v FROM Verificacion v WHERE v.numeronotacredito = :numeronotacredito")
    , @NamedQuery(name = "Verificacion.findByNumeronotapedido", query = "SELECT v FROM Verificacion v WHERE v.numeronotapedido = :numeronotapedido")
    , @NamedQuery(name = "Verificacion.findByNumeroordenpago", query = "SELECT v FROM Verificacion v WHERE v.numeroordenpago = :numeroordenpago")
    , @NamedQuery(name = "Verificacion.findByNumerocheque", query = "SELECT v FROM Verificacion v WHERE v.numerocheque = :numerocheque")
    , @NamedQuery(name = "Verificacion.findByNumeroplanilla", query = "SELECT v FROM Verificacion v WHERE v.numeroplanilla = :numeroplanilla")
    , @NamedQuery(name = "Verificacion.findByIdprocedencia", query = "SELECT v FROM Verificacion v WHERE v.idprocedencia = :idprocedencia")
    , @NamedQuery(name = "Verificacion.findByNombrecomercio", query = "SELECT v FROM Verificacion v WHERE v.nombrecomercio = :nombrecomercio")
    , @NamedQuery(name = "Verificacion.findByMes", query = "SELECT v FROM Verificacion v WHERE v.mes = :mes")
    , @NamedQuery(name = "Verificacion.findByPeriodo", query = "SELECT v FROM Verificacion v WHERE v.periodo = :periodo")
    , @NamedQuery(name = "Verificacion.findByObservaciones", query = "SELECT v FROM Verificacion v WHERE v.observaciones = :observaciones")
    , @NamedQuery(name = "Verificacion.findByIdverificador", query = "SELECT v FROM Verificacion v WHERE v.idverificador = :idverificador")
    , @NamedQuery(name = "Verificacion.findByFechacorreccion", query = "SELECT v FROM Verificacion v WHERE v.fechacorreccion = :fechacorreccion")
    , @NamedQuery(name = "Verificacion.findByCorreccion", query = "SELECT v FROM Verificacion v WHERE v.correccion = :correccion")
    , @NamedQuery(name = "Verificacion.findByIddependencia", query = "SELECT v FROM Verificacion v WHERE v.iddependencia = :iddependencia")
    , @NamedQuery(name = "Verificacion.findByIdregional", query = "SELECT v FROM Verificacion v WHERE v.idregional = :idregional")
    , @NamedQuery(name = "Verificacion.findByIdresponsable", query = "SELECT v FROM Verificacion v WHERE v.idresponsable = :idresponsable")
    , @NamedQuery(name = "Verificacion.findByIdresponsable2", query = "SELECT v FROM Verificacion v WHERE v.idresponsable2 = :idresponsable2")
    , @NamedQuery(name = "Verificacion.findByIdaprueba", query = "SELECT v FROM Verificacion v WHERE v.idaprueba = :idaprueba")
    , @NamedQuery(name = "Verificacion.findByIdautorizador", query = "SELECT v FROM Verificacion v WHERE v.idautorizador = :idautorizador")
    , @NamedQuery(name = "Verificacion.findByIdestadoverificacion", query = "SELECT v FROM Verificacion v WHERE v.idestadoverificacion = :idestadoverificacion")
    , @NamedQuery(name = "Verificacion.findByFecharevision", query = "SELECT v FROM Verificacion v WHERE v.fecharevision = :fecharevision")
    , @NamedQuery(name = "Verificacion.findByRevisionfinal", query = "SELECT v FROM Verificacion v WHERE v.revisionfinal = :revisionfinal")
    , @NamedQuery(name = "Verificacion.findByIdrevisador", query = "SELECT v FROM Verificacion v WHERE v.idrevisador = :idrevisador")
    , @NamedQuery(name = "Verificacion.findByFechaanulado", query = "SELECT v FROM Verificacion v WHERE v.fechaanulado = :fechaanulado")
    , @NamedQuery(name = "Verificacion.findByObservacionanulado", query = "SELECT v FROM Verificacion v WHERE v.observacionanulado = :observacionanulado")})
public class Verificacion implements Serializable {

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idverificacion")
    private Long idverificacion;
    @Column(name = "fechaverificacion")
    @Temporal(TemporalType.DATE)
    private Date fechaverificacion;
    @Size(max = 12)
    @Column(name = "horarecepcion")
    private String horarecepcion;
    @Size(max = 12)
    @Column(name = "horaremision")
    private String horaremision;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "idtipooperacion", referencedColumnName = "idtipooperacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipooperacion idtipooperacion;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "idmodalidad", referencedColumnName = "idmodalidad")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Modalidad idmodalidad;
    @Size(max = 15)
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "numerosolicitud")
    private Long numerosolicitud;
    @Column(name = "numerooperacion")
    private Long numerooperacion;
    @Column(name = "fechaoperacion")
    @Temporal(TemporalType.DATE)
    private Date fechaoperacion;
    @Column(name = "numerocontrato")
    private Long numerocontrato;
    @Column(name = "numeroboleta")
    private Long numeroboleta;
    @Column(name = "numeroordencredito")
    private Long numeroordencredito;
    @Size(max = 3)
    @Column(name = "local")
    private String local;
    @Size(max = 3)
    @Column(name = "puntoexpedicion")
    private String puntoexpedicion;
    @Column(name = "numerofactura")
    private Long numerofactura;
    @Column(name = "numeronotacredito")
    private Long numeronotacredito;
    @Column(name = "numeronotapedido")
    private Long numeronotapedido;
    @Column(name = "numeroordenpago")
    private Long numeroordenpago;
    @Column(name = "numerocheque")
    private Long numerocheque;
    @Column(name = "numeroplanilla")
    private Long numeroplanilla;
    @JoinColumn(name = "idprocedencia", referencedColumnName = "idprocedencia")
    @ManyToOne(fetch = FetchType.LAZY)
    private Procedencia idprocedencia;
    @Size(max = 40)
    @Column(name = "nombrecomercio")
    private String nombrecomercio;
    @Size(max = 30)
    @Column(name = "mes")
    private String mes;
    @Size(max = 30)
    @Column(name = "periodo")
    private String periodo;
    @Size(max = 30)
    @Column(name = "numerocuenta")
    private String numerocuenta;
    @JoinColumn(name = "idregional", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Regional idregional;
    @Size(max = 200)
    @Column(name = "observaciones")
    private String observaciones;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "idverificador", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario idverificador;
    @Column(name = "fechacorreccion")
    @Temporal(TemporalType.DATE)
    private Date fechacorreccion;
    @Size(max = 200)
    @Column(name = "correccion")
    private String correccion;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "iddependencia", referencedColumnName = "iddependencia")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Dependencia iddependencia;
    @JoinColumn(name = "idresponsable", referencedColumnName = "idfuncionario")
    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionario idresponsable;
    @JoinColumn(name = "idresponsable2", referencedColumnName = "idfuncionario")
    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionario idresponsable2;
    @JoinColumn(name = "idaprueba", referencedColumnName = "idfuncionario")
    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionario idaprueba;
    @JoinColumn(name = "idautorizador", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Autorizador idautorizador;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "idestadoverificacion", referencedColumnName = "idestadoverificacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Estadoverificacion idestadoverificacion;
    @Column(name = "fecharevision")
    @Temporal(TemporalType.DATE)
    private Date fecharevision;
    @Size(max = 200)
    @Column(name = "revisionfinal")
    private String revisionfinal;
    //@Basic(optional = false)
    //@NotNull
    @JoinColumn(name = "idrevisador", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario idrevisador;
    @Column(name = "fechaanulado")
    @Temporal(TemporalType.DATE)
    private Date fechaanulado;
    @Size(max = 100)
    @Column(name = "observacionanulado")
    private String observacionanulado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idverificacion", fetch = FetchType.LAZY)
    private List<Verificaciondetalle> verificaciondetalleList;

    public Verificacion() {
    }

    public Verificacion(Long idverificacion) {
        this.idverificacion = idverificacion;
    }

    public Verificacion(Long idverificacion, Tipooperacion idtipooperacion, Modalidad idmodalidad, Long numerooperacion, Procedencia idprocedencia, Regional idregional, Funcionario idverificador, Dependencia iddependencia, Funcionario idresponsable, Funcionario idresponsable2, Funcionario idaprueba, Estadoverificacion idestadoverificacion, Funcionario idrevisador) {
        this.idverificacion = idverificacion;
        this.idtipooperacion = idtipooperacion;
        this.idmodalidad = idmodalidad;
        this.numerooperacion = numerooperacion;
        this.idprocedencia = idprocedencia;
        this.idregional = idregional;
        this.idverificador = idverificador;
        this.iddependencia = iddependencia;
        this.idresponsable = idresponsable;
        this.idresponsable2 = idresponsable2;
        this.idaprueba = idaprueba;
        this.idestadoverificacion = idestadoverificacion;
        this.idrevisador = idrevisador;
    }

    public Long getIdverificacion() {
        return idverificacion;
    }

    public void setIdverificacion(Long idverificacion) {
        this.idverificacion = idverificacion;
    }

    public Date getFechaverificacion() {
        return fechaverificacion;
    }

    public void setFechaverificacion(Date fechaverificacion) {
        this.fechaverificacion = fechaverificacion;
    }

    public String getHorarecepcion() {
        return horarecepcion;
    }

    public void setHorarecepcion(String horarecepcion) {
        this.horarecepcion = horarecepcion;
    }

    public String getHoraremision() {
        return horaremision;
    }

    public void setHoraremision(String horaremision) {
        this.horaremision = horaremision;
    }

    public Tipooperacion getIdtipooperacion() {
        return idtipooperacion;
    }

    public void setIdtipooperacion(Tipooperacion idtipooperacion) {
        this.idtipooperacion = idtipooperacion;
    }

    public Modalidad getIdmodalidad() {
        return idmodalidad;
    }

    public void setIdmodalidad(Modalidad idmodalidad) {
        this.idmodalidad = idmodalidad;
    }

    public Long getNumerooperacion() {
        return numerooperacion;
    }

    public String getLocal() {
        return local;
    }

    public String getPuntoexpedicion() {
        return puntoexpedicion;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setPuntoexpedicion(String puntoexpedicion) {
        this.puntoexpedicion = puntoexpedicion;
    }

    public void setNumerooperacion(Long numerooperacion) {
        this.numerooperacion = numerooperacion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Funcionario getIdverificador() {
        return idverificador;
    }

    public void setIdverificador(Funcionario idverificador) {
        this.idverificador = idverificador;
    }

    public Date getFechacorreccion() {
        return fechacorreccion;
    }

    public void setFechacorreccion(Date fechacorreccion) {
        this.fechacorreccion = fechacorreccion;
    }

    public String getCorreccion() {
        return correccion;
    }

    public void setCorreccion(String correccion) {
        this.correccion = correccion;
    }

    public Dependencia getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(Dependencia iddependencia) {
        this.iddependencia = iddependencia;
    }

    public Estadoverificacion getIdestadoverificacion() {
        return idestadoverificacion;
    }

    public void setIdestadoverificacion(Estadoverificacion idestadoverificacion) {
        this.idestadoverificacion = idestadoverificacion;
    }

    public Date getFecharevision() {
        return fecharevision;
    }

    public void setFecharevision(Date fecharevision) {
        this.fecharevision = fecharevision;
    }

    public String getRevisionfinal() {
        return revisionfinal;
    }

    public void setRevisionfinal(String revisionfinal) {
        this.revisionfinal = revisionfinal;
    }

    public Funcionario getIdrevisador() {
        return idrevisador;
    }

    public void setIdrevisador(Funcionario idrevisador) {
        this.idrevisador = idrevisador;
    }

    public Date getFechaanulado() {
        return fechaanulado;
    }

    public void setFechaanulado(Date fechaanulado) {
        this.fechaanulado = fechaanulado;
    }

    public String getObservacionanulado() {
        return observacionanulado;
    }

    public void setObservacionanulado(String observacionanulado) {
        this.observacionanulado = observacionanulado;
    }

    @XmlTransient
    public List<Verificaciondetalle> getVerificaciondetalleList() {
        return verificaciondetalleList;
    }

    public void setVerificaciondetalleList(List<Verificaciondetalle> verificaciondetalleList) {
        this.verificaciondetalleList = verificaciondetalleList;
    }
    
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setNumerosolicitud(Long numerosolicitud) {
        this.numerosolicitud = numerosolicitud;
    }

    public void setFechaoperacion(Date fechaoperacion) {
        this.fechaoperacion = fechaoperacion;
    }

    public void setNumerocontrato(Long numerocontrato) {
        this.numerocontrato = numerocontrato;
    }

    public void setNumeroboleta(Long numeroboleta) {
        this.numeroboleta = numeroboleta;
    }

    public void setNumeroordencredito(Long numeroordencredito) {
        this.numeroordencredito = numeroordencredito;
    }

    public void setNumerofactura(Long numerofactura) {
        this.numerofactura = numerofactura;
    }

    public void setNumeronotacredito(Long numeronotacredito) {
        this.numeronotacredito = numeronotacredito;
    }

    public void setNumeronotapedido(Long numeronotapedido) {
        this.numeronotapedido = numeronotapedido;
    }

    public void setNumeroordenpago(Long numeroordenpago) {
        this.numeroordenpago = numeroordenpago;
    }

    public void setNumerocheque(Long numerocheque) {
        this.numerocheque = numerocheque;
    }

    public void setNumeroplanilla(Long numeroplanilla) {
        this.numeroplanilla = numeroplanilla;
    }

    public void setIdprocedencia(Procedencia idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    public void setNombrecomercio(String nombrecomercio) {
        this.nombrecomercio = nombrecomercio;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public void setIdregional(Regional idregional) {
        this.idregional = idregional;
    }

    public void setIdresponsable(Funcionario idresponsable) {
        this.idresponsable = idresponsable;
    }

    public Funcionario getIdresponsable2() {
        return idresponsable2;
    }

    public void setIdresponsable2(Funcionario idresponsable2) {
        this.idresponsable2 = idresponsable2;
    }

    public void setIdaprueba(Funcionario idaprueba) {
        this.idaprueba = idaprueba;
    }

    public Autorizador getIdautorizador() {
        return idautorizador;
    }

    public String getCedula() {
        return cedula;
    }

    public Long getNumerosolicitud() {
        return numerosolicitud;
    }

    public Date getFechaoperacion() {
        return fechaoperacion;
    }

    public Long getNumerocontrato() {
        return numerocontrato;
    }

    public Long getNumeroboleta() {
        return numeroboleta;
    }

    public Long getNumeroordencredito() {
        return numeroordencredito;
    }

    public Long getNumerofactura() {
        return numerofactura;
    }

    public Long getNumeronotacredito() {
        return numeronotacredito;
    }

    public Long getNumeronotapedido() {
        return numeronotapedido;
    }

    public Long getNumeroordenpago() {
        return numeroordenpago;
    }

    public Long getNumerocheque() {
        return numerocheque;
    }

    public Long getNumeroplanilla() {
        return numeroplanilla;
    }

    public Procedencia getIdprocedencia() {
        return idprocedencia;
    }

    public String getNombrecomercio() {
        return nombrecomercio;
    }

    public String getMes() {
        return mes;
    }

    public String getPeriodo() {
        return periodo;
    }

    public Regional getIdregional() {
        return idregional;
    }

    public Funcionario getIdresponsable() {
        return idresponsable;
    }

    public Funcionario getIdaprueba() {
        return idaprueba;
    }

    public void setIdautorizador(Autorizador idautorizador) {
        this.idautorizador = idautorizador;
    }

    public String getNumerocuenta() {
        return numerocuenta;
    }

    public void setNumerocuenta(String numerocuenta) {
        this.numerocuenta = numerocuenta;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idverificacion != null ? idverificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verificacion)) {
            return false;
        }
        Verificacion other = (Verificacion) object;
        if ((this.idverificacion == null && other.idverificacion != null) || (this.idverificacion != null && !this.idverificacion.equals(other.idverificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Verificacion[ idverificacion=" + idverificacion + " ]";
    }
    
}
