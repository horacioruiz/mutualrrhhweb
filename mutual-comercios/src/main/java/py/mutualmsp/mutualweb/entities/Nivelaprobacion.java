/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "nivelaprobacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nivelaprobacion.findAll", query = "SELECT n FROM Nivelaprobacion n")
    , @NamedQuery(name = "Nivelaprobacion.findById", query = "SELECT n FROM Nivelaprobacion n WHERE n.id = :id")
    , @NamedQuery(name = "Nivelaprobacion.findByDescripcion", query = "SELECT n FROM Nivelaprobacion n WHERE n.descripcion = :descripcion")})
public class Nivelaprobacion implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idnivelaprobacion", fetch = FetchType.LAZY)
    private List<Autorizador> autorizadorList;
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 150)
    @Column(name = "descripcion")
    private String descripcion;

    public Nivelaprobacion() {
    }

    public Nivelaprobacion(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nivelaprobacion)) {
            return false;
        }
        Nivelaprobacion other = (Nivelaprobacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Nivelaprobacion[ id=" + id + " ]";
    }

    @XmlTransient
    public List<Autorizador> getAutorizadorList() {
        return autorizadorList;
    }

    public void setAutorizadorList(List<Autorizador> autorizadorList) {
        this.autorizadorList = autorizadorList;
    }
    
}
