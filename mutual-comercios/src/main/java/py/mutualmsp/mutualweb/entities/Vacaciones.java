/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "vacaciones", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacaciones.findAll", query = "SELECT d FROM Vacaciones d")
    , @NamedQuery(name = "Vacaciones.findById", query = "SELECT d FROM Vacaciones d WHERE d.id = :id")
})
public class Vacaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "periodo")
    private String periodo;

    @Column(name = "cantdiavaca")
    private Long cantdiavaca;

    @Column(name = "cantdiatomada")
    private Long cantdiatomada;

    @Column(name = "cantdiarestante")
    private Long cantdiarestante;

    @Column(name = "adelantado")
    private Boolean adelantado;

    @JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcionario;

//    @JoinColumn(name = "idsolicitud", referencedColumnName = "id")
//    @ManyToOne(optional = false, fetch = FetchType.LAZY)
//    private Solicitud solicitud;
    public Vacaciones() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public Boolean getAdelantado() {
        return adelantado;
    }

    public void setAdelantado(Boolean adelantado) {
        this.adelantado = adelantado;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Long getCantdiavaca() {
        return cantdiavaca;
    }

    public void setCantdiavaca(Long cantdiavaca) {
        this.cantdiavaca = cantdiavaca;
    }

    public Long getCantdiatomada() {
        return cantdiatomada;
    }

    public void setCantdiatomada(Long cantdiatomada) {
        this.cantdiatomada = cantdiatomada;
    }

    public Long getCantdiarestante() {
        return cantdiarestante;
    }

    public void setCantdiarestante(Long cantdiarestante) {
        this.cantdiarestante = cantdiarestante;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

//    public Solicitud getSolicitud() {
//        return solicitud;
//    }
//
//    public void setSolicitud(Solicitud solicitud) {
//        this.solicitud = solicitud;
//    }
    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.adelantado == null) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (!this.adelantado) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacaciones)) {
            return false;
        }
        Vacaciones other = (Vacaciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

//    public Image getEstadoIcon() {
//        String basepath = VaadinService.getCurrent()
//                .getBaseDirectory().getAbsolutePath();
//        if (this.solicitud != null) {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/good.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        } else {
//            FileResource resource = new FileResource(new File(basepath
//                    + "/WEB-INF/images/wrong.png"));
//            Image imagen = new Image("Image from file", resource);
//            imagen.setWidth(2, Sizeable.Unit.EM);
//            return imagen;
//        }
//    }
    public static Comparator<Vacaciones> idComparator = new Comparator<Vacaciones>() {

        public int compare(Vacaciones s1, Vacaciones s2) {

            int rollno1 = s1.getId().intValue();
            int rollno2 = s2.getId().intValue();

            /*For ascending order*/
            return rollno1 - rollno2;

            /*For descending order*/
            //rollno2-rollno1;
        }
    };

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Vacaciones[ id=" + id + " ]";
    }

}
