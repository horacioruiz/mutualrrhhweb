/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "estadoverificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadoverificacion.findAll", query = "SELECT e FROM Estadoverificacion e")
    , @NamedQuery(name = "Estadoverificacion.findByIdestadoverificacion", query = "SELECT e FROM Estadoverificacion e WHERE e.idestadoverificacion = :idestadoverificacion")
    , @NamedQuery(name = "Estadoverificacion.findByDescripcion", query = "SELECT e FROM Estadoverificacion e WHERE e.descripcion = :descripcion")})
public class Estadoverificacion implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idestadoverificacion")
    private Long idestadoverificacion;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idestadoverificacion", fetch = FetchType.LAZY)
    private List<Verificacion> verificacionList;

    public Estadoverificacion() {
    }

    public Estadoverificacion(Long idestadoverificacion) {
        this.idestadoverificacion = idestadoverificacion;
    }

    public Long getIdestadoverificacion() {
        return idestadoverificacion;
    }

    public void setIdestadoverificacion(Long idestadoverificacion) {
        this.idestadoverificacion = idestadoverificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestadoverificacion != null ? idestadoverificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadoverificacion)) {
            return false;
        }
        Estadoverificacion other = (Estadoverificacion) object;
        if ((this.idestadoverificacion == null && other.idestadoverificacion != null) || (this.idestadoverificacion != null && !this.idestadoverificacion.equals(other.idestadoverificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Estadoverificacion[ idestadoverificacion=" + idestadoverificacion + " ]";
    }

    @XmlTransient
    public List<Verificacion> getVerificacionList() {
        return verificacionList;
    }

    public void setVerificacionList(List<Verificacion> verificacionList) {
        this.verificacionList = verificacionList;
    }
    
}
