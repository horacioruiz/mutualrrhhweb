/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "conciliacion_cab", schema = "comercios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConciliacionCab.findAll", query = "SELECT p FROM ConciliacionCab p")
})
public class ConciliacionCab implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

//    @Column(name = "idcomercio")
//    private long idcomercio;
//    @Column(name = "descripcion")
//    private String descripcion;
    @Column(name = "periodo")
    private String periodo;

    @Column(name = "anho")
    private String anho;

    @Column(name = "fechapertura")
    private Date fechapertura;

    @Column(name = "fechaultimamod")
    private Date fechaultimamod;

    @Column(name = "saldoant")
    private Long saldoant;

    @Column(name = "movmes")
    private Long movmes;

    @Column(name = "totalorden")
    private Long totalorden;

    @Column(name = "pagado")
    private Long pagado;

    public ConciliacionCab() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechapertura() {
        return fechapertura;
    }

    public void setFechapertura(Date fechapertura) {
        this.fechapertura = fechapertura;
    }

    public Date getFechaultimamod() {
        return fechaultimamod;
    }

    public void setFechaultimamod(Date fechaultimamod) {
        this.fechaultimamod = fechaultimamod;
    }

    public Long getSaldoant() {
        return saldoant;
    }

    public void setSaldoant(Long saldoant) {
        this.saldoant = saldoant;
    }

    public Long getMovmes() {
        return movmes;
    }

    public void setMovmes(Long movmes) {
        this.movmes = movmes;
    }

    public String getAnho() {
        return anho;
    }

    public void setAnho(String anho) {
        this.anho = anho;
    }

    public Long getTotalorden() {
        return totalorden;
    }

    public void setTotalorden(Long totalorden) {
        this.totalorden = totalorden;
    }

    public Long getPagado() {
        return pagado;
    }

    public void setPagado(Long pagado) {
        this.pagado = pagado;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConciliacionCab)) {
            return false;
        }
        ConciliacionCab other = (ConciliacionCab) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.ConciliacionCab[ id=" + id + " ]";
    }

}
