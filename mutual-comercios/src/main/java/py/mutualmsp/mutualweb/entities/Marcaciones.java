/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import py.mutualmsp.mutualweb.util.DateUtils;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "marcaciones", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Marcaciones.findAll", query = "SELECT d FROM Marcaciones d join fetch d.horarioFuncionario hf ORDER BY hf.id, d.fecha")
    , @NamedQuery(name = "Marcaciones.findById", query = "SELECT d FROM Marcaciones d WHERE d.id = :id")})
public class Marcaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "inasignada")
    private Timestamp inasignada;

    @Column(name = "outasignada")
    private Timestamp outasignada;

    @Column(name = "nombrefunc")
    private String nombrefunc;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "inmarcada")
    private Timestamp inmarcada;

    @Column(name = "outmarcada")
    private Timestamp outmarcada;

    @Column(name = "mintrabajada")
    private Long mintrabajada;

    @Column(name = "alta")
    private String alta;

    @Column(name = "minllegadatardia")
    private Long minllegadatardia;

    @Column(name = "minllegadatemprana")
    private Long minllegadatemprana;

    @JoinColumn(name = "idreloj", referencedColumnName = "idreloj")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private HorarioFuncionario horarioFuncionario;

    @JoinColumn(name = "idsolicitud", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Solicitud solicitud;

    @JoinColumn(name = "idsolicitudproducciondetalle", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudProduccionDetalle solicitudProduccionDetalle;

    @Column(name = "hsextra")
    public long hsextra = 0;

    @Column(name = "mintrabajadaasignada")
    private long mintrabajadaasignada = 0;

    public Marcaciones() {
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.getMintrabajada() < 0 || (this.getAlta() != null && this.getAlta().equalsIgnoreCase("MANUAL"))) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/pending2.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (this.getMinllegadatemprana() > 0) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (this.getMinllegadatardia() > 0) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/pending.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    public String getAlta() {
        return alta;
    }

    public void setAlta(String alta) {
        this.alta = alta;
    }

    public Image getEstadoHsExtra() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.getHsextra() == 1) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        } catch (Exception e) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } finally {
        }
    }

    public String getEstadoHsExtraSiNO() {
        try {
            if (this.getHsextra() == 1) {
                return "SI";
            } else {
                return "NO";
            }
        } catch (Exception e) {
            return "NO";
        } finally {
        }
    }

    public Image getEstadoHsExtra2() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        try {
            if (this.getHsextra() == 1) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public String getAplicaHsExtra() {
        try {
            if (this.getHsextra() == 1) {
                return "SI";
            } else {
                return "NO";
            }
        } catch (Exception e) {
            return "NO";
        } finally {
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public Long getHsExtra() {
        long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(getOutasignada().getTime() - getInasignada().getTime());
        if (!isWeekendSaturday(DateUtils.asLocalDate(getFecha())) && minutesEntSal > 0) {
            minutesEntSal -= 40;
        }
        long minExtra = (getMintrabajada() - minutesEntSal);
        if (minExtra < 0) {
            return 0l;
        } else {
            return minExtra;
        }
    }

    public long getMintrabajadaasignada() {
        return mintrabajadaasignada;
    }

    public void setMintrabajadaasignada(long mintrabajadaasignada) {
        this.mintrabajadaasignada = mintrabajadaasignada;
    }

    public SolicitudProduccionDetalle getSolicitudProduccionDetalle() {
        return solicitudProduccionDetalle;
    }

    public void setSolicitudProduccionDetalle(SolicitudProduccionDetalle solicitudProduccionDetalle) {
        this.solicitudProduccionDetalle = solicitudProduccionDetalle;
    }

    public long getHsextra() {
        return Objects.isNull(hsextra) ? 0 : hsextra;
    }

    public void setHsextra(long hsextra) {
        this.hsextra = Objects.isNull(hsextra) ? 0 : hsextra;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Timestamp getInasignada() {
        return inasignada;
    }

    public void setInasignada(Timestamp inasignada) {
        this.inasignada = inasignada;
    }

    public Timestamp getOutasignada() {
        return outasignada;
    }

    public void setOutasignada(Timestamp outasignada) {
        this.outasignada = outasignada;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public String getNombrefunc() {
        return nombrefunc;
    }

    public void setNombrefunc(String nombrefunc) {
        this.nombrefunc = nombrefunc;
    }

    public Timestamp getInmarcada() {
        return inmarcada;
    }

    public void setInmarcada(Timestamp inmarcada) {
        this.inmarcada = inmarcada;
    }

    public Timestamp getOutmarcada() {
        return outmarcada;
    }

    public void setOutmarcada(Timestamp outmarcada) {
        this.outmarcada = outmarcada;
    }

    public Long getMintrabajada() {
        return mintrabajada;
    }

    public void setMintrabajada(Long mintrabajada) {
        this.mintrabajada = mintrabajada;
    }

    public Long getMinllegadatardia() {
        return minllegadatardia;
    }

    public void setMinllegadatardia(Long minllegadatardia) {
        this.minllegadatardia = minllegadatardia;
    }

    public Long getMinllegadatemprana() {
        return minllegadatemprana;
    }

    public void setMinllegadatemprana(Long minllegadatemprana) {
        this.minllegadatemprana = minllegadatemprana;
    }

    public HorarioFuncionario getHorarioFuncionario() {
        return horarioFuncionario;
    }

    public void setHorarioFuncionario(HorarioFuncionario horarioFuncionario) {
        this.horarioFuncionario = horarioFuncionario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Marcaciones)) {
            return false;
        }
        Marcaciones other = (Marcaciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Marcaciones[ id=" + id + " ]";
    }

    public String getSalidaAntesHora(long hs) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        if ((getInmarcada().getHours() * 60 + getInmarcada().getMinutes()) == (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes())) {
            return "--";
        } else {
            Timestamp tsOutMarcada = new Timestamp(getFecha().getTime());
            tsOutMarcada.setHours(getOutmarcada().getHours());
            tsOutMarcada.setMinutes(getOutmarcada().getMinutes());
            tsOutMarcada.setSeconds(getOutmarcada().getSeconds());
            if (hs >= getOutmarcada().getTime()) {
                long dif = TimeUnit.MILLISECONDS.toMinutes(hs - tsOutMarcada.getTime());
                if (dif > 0) {
                    try {
                        return sdfHM.format(sdf.parse(String.valueOf(dif)));
                    } catch (ParseException ex) {
                        System.out.println("-> " + ex.getLocalizedMessage());
                        System.out.println("-> " + ex.getMessage());
                        return "--";
                    } finally {
                    }
                } else {
                    return "--";
                }
            } else {
                Timestamp tsOut = new Timestamp(getOutmarcada().getTime());

                tsOut.setDate(getOutasignada().getDate());
                tsOut.setMonth(getOutasignada().getMonth());
                tsOut.setYear(getOutasignada().getYear());

                tsOut.setHours(getOutmarcada().getHours());
                tsOut.setMinutes(getOutmarcada().getMinutes());
                tsOut.setSeconds(getOutmarcada().getSeconds());
                long dif = TimeUnit.MILLISECONDS.toMinutes(getOutasignada().getTime() - tsOut.getTime());
                if (dif > 0) {
                    try {
                        return sdfHM.format(sdf.parse(String.valueOf(dif)));
                    } catch (ParseException ex) {
                        System.out.println("-> " + ex.getLocalizedMessage());
                        System.out.println("-> " + ex.getMessage());
                        return "--";
                    } finally {
                    }
                } else {
                    return "--";
                }
            }
        }
    }

    public String getLlegadaDespuesHora(Long parametroLlegadaTardia) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        if (getMinllegadatardia() > 0 && getMinllegadatardia() > parametroLlegadaTardia) {
            try {
                return sdfHM.format(sdf.parse(getMinllegadatardia() + ""));
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.getMessage());
                return "--";
            } finally {
            }
        } else {
            return "--";
        }
    }

    public String getLlegadaTardia(Long parametroLlegadaTardia) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        if (getMinllegadatardia() > 0 && getMinllegadatardia() <= parametroLlegadaTardia) {
            try {
                return sdfHM.format(sdf.parse(getMinllegadatardia() + ""));
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.getMessage());
                return "--";
            } finally {
            }
        } else {
            return "--";
        }
    }

    public String getHsNocturna(Long parametro, Long param2) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");

        if (getHsExtra() > 0 && this.hsextra == 1) {
            if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) > (Integer.parseInt(param2 + "") * 60)) {
                int minDif = (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) - (Integer.parseInt(param2 + "") * 60);
                try {
                    return sdfHM.format(sdf.parse(minDif + ""));
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            } else {
                return "--";
            }
        } else {
            return "--";
        }
    }

    public String getHsNocturna2(Long parametro, Long param2, SolicitudProduccionDetalle soli) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");

        if (getHsExtra() > 0 && this.hsextra == 1) {

            int horarioSolicitud = 0;
            try {
                horarioSolicitud = soli.getHora().getHours() * 60 + soli.getHora().getMinutes();
            } catch (Exception e) {
            } finally {
            }

            if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) >= horarioSolicitud && horarioSolicitud != 0) {

                if (horarioSolicitud > (Integer.parseInt(param2 + "") * 60)) {
                    int minDif = horarioSolicitud - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                    try {
                        String hsnoc = getHsNocturna(parametro, param2);
                        if (hsnoc.equalsIgnoreCase("--")) {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        } else {
                            if (minDif > 150) {
                                return sdfHM.format(sdf.parse(150 + ""));
                            } else {
                                return sdfHM.format(sdf.parse(minDif + ""));
                            }
                        }
                    } catch (ParseException ex) {
                        return "--";
                    } finally {
                    }
                } else {
                    return "--";
                }

            } else if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) > (Integer.parseInt(param2 + "") * 60)) {
                int minDif = (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) - (Integer.parseInt(param2 + "") * 60);
                try {
                    return sdfHM.format(sdf.parse(minDif + ""));
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            } else {
                return "--";
            }
        } else {
            return "--";
        }
    }

    public String getHsDiurna(Long parametro, Long param2) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");

        if (getHsExtra() > 0 && this.hsextra == 1) {
            if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) <= (Integer.parseInt(param2 + "") * 60)) {
                int minDif = (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                try {
                    String hsnoc = getHsNocturna(parametro, param2);
                    if (hsnoc.equalsIgnoreCase("--")) {
                        return sdfHM.format(sdf.parse(minDif + ""));
                    } else {
                        if (minDif > 150) {
                            return sdfHM.format(sdf.parse(150 + ""));
                        } else {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        }
                    }
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            } else {
                int minDif = (Integer.parseInt(param2 + "") * 60) - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                try {
                    String hsnoc = getHsNocturna(parametro, param2);
                    if (hsnoc.equalsIgnoreCase("--")) {
                        return sdfHM.format(sdf.parse(minDif + ""));
                    } else {
                        if (minDif > 150) {
                            return sdfHM.format(sdf.parse(150 + ""));
                        } else {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        }
                    }
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            }
        } else {
            return "--";
        }
    }

    public String getHsDiurna2(Long parametro, Long param2, SolicitudProduccionDetalle soli) {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");

        if (getHsExtra() > 0 && this.hsextra == 1) {
            int horarioSolicitud = 0;

            try {
                horarioSolicitud = soli.getHora().getHours() * 60 + soli.getHora().getMinutes();
            } catch (Exception e) {
            } finally {
            }

            if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) >= horarioSolicitud && horarioSolicitud != 0) {

                int minDif = horarioSolicitud - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                try {
                    String hsnoc = getHsNocturna2(parametro, param2, soli);
                    if (hsnoc.equalsIgnoreCase("--")) {
                        return sdfHM.format(sdf.parse(minDif + ""));
                    } else {
                        if (minDif > 150) {
                            return sdfHM.format(sdf.parse(150 + ""));
                        } else {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        }
                    }
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }

            } else if ((getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) <= (Integer.parseInt(param2 + "") * 60)) {
                int minDif = (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes()) - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                try {
                    String hsnoc = getHsNocturna2(parametro, param2, soli);
                    if (hsnoc.equalsIgnoreCase("--")) {
                        return sdfHM.format(sdf.parse(minDif + ""));
                    } else {
                        if (minDif > 150) {
                            return sdfHM.format(sdf.parse(150 + ""));
                        } else {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        }
                    }
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            } else {
                int minDif = (Integer.parseInt(param2 + "") * 60) - (getOutasignada().getHours() * 60 + getOutasignada().getMinutes());
                try {
                    String hsnoc = getHsNocturna2(parametro, param2, soli);
                    if (hsnoc.equalsIgnoreCase("--")) {
                        return sdfHM.format(sdf.parse(minDif + ""));
                    } else {
                        if (minDif > 150) {
                            return sdfHM.format(sdf.parse(150 + ""));
                        } else {
                            return sdfHM.format(sdf.parse(minDif + ""));
                        }
                    }
                } catch (ParseException ex) {
                    return "--";
                } finally {
                }
            }
        } else {
            return "--";
        }
    }

    public String getHoraTrabajada() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        if ((getInmarcada().getHours() * 60 + getInmarcada().getMinutes()) == (getOutmarcada().getHours() * 60 + getOutmarcada().getMinutes())) {
            try {
                return sdfHM.format(sdf.parse("0"));
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.getMessage());
                return "--";
            } finally {
            }
        } else {
            if (getMintrabajada() > 0) {
                try {
                    return sdfHM.format(sdf.parse(getMintrabajada() + ""));
                } catch (ParseException ex) {
                    System.out.println("-> " + ex.getLocalizedMessage());
                    System.out.println("-> " + ex.getMessage());
                    return "--";
                } finally {
                }
            } else {
                try {
                    Long numhere = getMintrabajada() * -1;
                    return sdfHM.format(sdf.parse(numhere + ""));
                } catch (ParseException ex) {
                    System.out.println("-> " + ex.getLocalizedMessage());
                    System.out.println("-> " + ex.getMessage());
                    return "--";
                } finally {
                }
            }
        }
    }

    public String getHsAsignada() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        if ((getInasignada().getHours() * 60 + getInasignada().getMinutes()) == (getOutasignada().getHours() * 60 + getOutasignada().getMinutes())) {
            try {
                return sdfHM.format(sdf.parse("0"));
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.getMessage());
                return "--";
            } finally {
            }
        } else {
            try {
                int minAsig = (getOutasignada().getHours() * 60 + getOutasignada().getMinutes()) - (getInasignada().getHours() * 60 + getInasignada().getMinutes());
                if (!isWeekendSaturday(DateUtils.asLocalDate(getFecha()))) {
                    minAsig -= 40;
                }
                return sdfHM.format(sdf.parse(minAsig + ""));
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.getMessage());
                return "--";
            } finally {
            }
        }
    }

}
