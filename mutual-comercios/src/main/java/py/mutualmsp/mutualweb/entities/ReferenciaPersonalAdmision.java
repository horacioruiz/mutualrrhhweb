/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "referencia_personal_admision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReferenciaPersonalAdmision.findAll", query = "SELECT r FROM ReferenciaPersonalAdmision r")
    , @NamedQuery(name = "ReferenciaPersonalAdmision.findById", query = "SELECT r FROM ReferenciaPersonalAdmision r WHERE r.id = :id")
    , @NamedQuery(name = "ReferenciaPersonalAdmision.findByNombre", query = "SELECT r FROM ReferenciaPersonalAdmision r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "ReferenciaPersonalAdmision.findByApellido", query = "SELECT r FROM ReferenciaPersonalAdmision r WHERE r.apellido = :apellido")
    , @NamedQuery(name = "ReferenciaPersonalAdmision.findByTelefono", query = "SELECT r FROM ReferenciaPersonalAdmision r WHERE r.telefono = :telefono")})
public class ReferenciaPersonalAdmision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "telefono")
    private String telefono;
    @JoinColumn(name = "idparentesco", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Parentesco idparentesco;
    @JoinColumn(name = "idsolicitud_admision", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudAdmision idsolicitudAdmision;

    public ReferenciaPersonalAdmision() {
    }

    public ReferenciaPersonalAdmision(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Parentesco getIdparentesco() {
        return idparentesco;
    }

    public void setIdparentesco(Parentesco idparentesco) {
        this.idparentesco = idparentesco;
    }

    public SolicitudAdmision getIdsolicitudAdmision() {
        return idsolicitudAdmision;
    }

    public void setIdsolicitudAdmision(SolicitudAdmision idsolicitudAdmision) {
        this.idsolicitudAdmision = idsolicitudAdmision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReferenciaPersonalAdmision)) {
            return false;
        }
        ReferenciaPersonalAdmision other = (ReferenciaPersonalAdmision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.ReferenciaPersonalAdmision[ id=" + id + " ]";
    }
    
}
