/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "autorizador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autorizador.findAll", query = "SELECT a FROM Autorizador a")
    , @NamedQuery(name = "Autorizador.findById", query = "SELECT a FROM Autorizador a WHERE a.id = :id")
    , @NamedQuery(name = "Autorizador.findByIdfuncionario", query = "SELECT a FROM Autorizador a WHERE a.idfuncionario = :idfuncionario")
    , @NamedQuery(name = "Autorizador.findByIdnivelaprobacion", query = "SELECT a FROM Autorizador a WHERE a.idnivelaprobacion = :idnivelaprobacion")})
public class Autorizador implements Serializable {

    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idautorizador", fetch = FetchType.LAZY)
    //private List<Verificacion> verificacionList;

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "idfuncionario", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario idfuncionario;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "idnivelaprobacion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Nivelaprobacion idnivelaprobacion;
    
    
    @Transient
    private String nombreFuncionario;

    public String getNombreFuncionario() {
        return nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
            this.nombreFuncionario = nombreFuncionario;      
    }
    
    public Autorizador() {
    }

    public Autorizador(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Funcionario getIdfuncionario() {
        return idfuncionario;
    }

    public Nivelaprobacion getIdnivelaprobacion() {
        return idnivelaprobacion;
    }

    public void setIdfuncionario(Funcionario idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public void setIdnivelaprobacion(Nivelaprobacion idnivelaprobacion) {
        this.idnivelaprobacion = idnivelaprobacion;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autorizador)) {
            return false;
        }
        Autorizador other = (Autorizador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Autorizador[ id=" + id + " ]";
    }

    /*@XmlTransient
    public List<Verificacion> getVerificacionList() {
        return verificacionList;
    }

    public void setVerificacionList(List<Verificacion> verificacionList) {
        this.verificacionList = verificacionList;
    }*/
    
}
