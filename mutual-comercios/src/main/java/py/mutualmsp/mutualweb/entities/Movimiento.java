/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "movimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movimiento.findAll", query = "SELECT m FROM Movimiento m")
    ,
    @NamedQuery(name = "Movimiento.findById", query = "SELECT m FROM Movimiento m WHERE m.id = :id")
    ,
    @NamedQuery(name = "Movimiento.findByIdsocio", query = "SELECT m FROM Movimiento m WHERE m.idsocio = :idsocio")
    ,
    @NamedQuery(name = "Movimiento.findByIdmoneda", query = "SELECT m FROM Movimiento m WHERE m.idmoneda = :idmoneda")
    ,
    @NamedQuery(name = "Movimiento.findByNumerosolicitud", query = "SELECT m FROM Movimiento m WHERE m.numerosolicitud = :numerosolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByFechasolicitud", query = "SELECT m FROM Movimiento m WHERE m.fechasolicitud = :fechasolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByMontosolicitud", query = "SELECT m FROM Movimiento m WHERE m.montosolicitud = :montosolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByPlazosolicitud", query = "SELECT m FROM Movimiento m WHERE m.plazosolicitud = :plazosolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByObservacionsolicitud", query = "SELECT m FROM Movimiento m WHERE m.observacionsolicitud = :observacionsolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByIdentidad", query = "SELECT m FROM Movimiento m WHERE m.identidad = :identidad")
    ,
    @NamedQuery(name = "Movimiento.findByNumeroboleta", query = "SELECT m FROM Movimiento m WHERE m.idcuenta=105 AND m.numeroboleta = :numeroboleta")
    ,
    @NamedQuery(name = "Movimiento.findByNumeroacta", query = "SELECT m FROM Movimiento m WHERE m.numeroacta = :numeroacta")
    ,
    @NamedQuery(name = "Movimiento.findByFechaacta", query = "SELECT m FROM Movimiento m WHERE m.fechaacta = :fechaacta")
    ,
    @NamedQuery(name = "Movimiento.findByMontoaprobado", query = "SELECT m FROM Movimiento m WHERE m.montoaprobado = :montoaprobado")
    ,
    @NamedQuery(name = "Movimiento.findByPlazoaprobado", query = "SELECT m FROM Movimiento m WHERE m.plazoaprobado = :plazoaprobado")
    ,
    @NamedQuery(name = "Movimiento.findByTasainteres", query = "SELECT m FROM Movimiento m WHERE m.tasainteres = :tasainteres")
    ,
    @NamedQuery(name = "Movimiento.findByFechaaprobado", query = "SELECT m FROM Movimiento m WHERE m.fechaaprobado = :fechaaprobado")
    ,
    @NamedQuery(name = "Movimiento.findByObservacionaprobado", query = "SELECT m FROM Movimiento m WHERE m.observacionaprobado = :observacionaprobado")
    ,
    @NamedQuery(name = "Movimiento.findByFechaprimervencimiento", query = "SELECT m FROM Movimiento m WHERE m.fechaprimervencimiento = :fechaprimervencimiento")
    ,
    @NamedQuery(name = "Movimiento.findByFechavencimiento", query = "SELECT m FROM Movimiento m WHERE m.fechavencimiento = :fechavencimiento")
    ,
    @NamedQuery(name = "Movimiento.findByIdestado", query = "SELECT m FROM Movimiento m WHERE m.idestado = :idestado")
    ,
    @NamedQuery(name = "Movimiento.findByIdautorizador", query = "SELECT m FROM Movimiento m WHERE m.idautorizador = :idautorizador")
    ,
    @NamedQuery(name = "Movimiento.findByTasaretencion", query = "SELECT m FROM Movimiento m WHERE m.tasaretencion = :tasaretencion")
    ,
    @NamedQuery(name = "Movimiento.findByIdpromotor", query = "SELECT m FROM Movimiento m WHERE m.idpromotor = :idpromotor")
    ,
    @NamedQuery(name = "Movimiento.findByIdcuenta", query = "SELECT m FROM Movimiento m WHERE m.idcuenta = :idcuenta")
    ,
    @NamedQuery(name = "Movimiento.findByFechaanulado", query = "SELECT m FROM Movimiento m WHERE m.fechaanulado = :fechaanulado")
    ,
    @NamedQuery(name = "Movimiento.findByObservacionanulado", query = "SELECT m FROM Movimiento m WHERE m.observacionanulado = :observacionanulado")
    ,
    @NamedQuery(name = "Movimiento.findByFecharechazado", query = "SELECT m FROM Movimiento m WHERE m.fecharechazado = :fecharechazado")
    ,
    @NamedQuery(name = "Movimiento.findByObservacionrechazado", query = "SELECT m FROM Movimiento m WHERE m.observacionrechazado = :observacionrechazado")
    ,
    @NamedQuery(name = "Movimiento.findByFechareconsiderado", query = "SELECT m FROM Movimiento m WHERE m.fechareconsiderado = :fechareconsiderado")
    ,
    @NamedQuery(name = "Movimiento.findByObservacionreconsiderado", query = "SELECT m FROM Movimiento m WHERE m.observacionreconsiderado = :observacionreconsiderado")
    ,
    @NamedQuery(name = "Movimiento.findByFechageneracion", query = "SELECT m FROM Movimiento m WHERE m.fechageneracion = :fechageneracion")
    ,
    @NamedQuery(name = "Movimiento.findByObservaciongeneracion", query = "SELECT m FROM Movimiento m WHERE m.observaciongeneracion = :observaciongeneracion")
    ,
    @NamedQuery(name = "Movimiento.findByNumerooperacion", query = "SELECT m FROM Movimiento m WHERE m.numerooperacion = :numerooperacion")
    ,
    @NamedQuery(name = "Movimiento.findByIdregional", query = "SELECT m FROM Movimiento m WHERE m.idregional = :idregional")
    ,
    @NamedQuery(name = "Movimiento.findByIdtipocredito", query = "SELECT m FROM Movimiento m WHERE m.idtipocredito = :idtipocredito")
    ,
    @NamedQuery(name = "Movimiento.findByMontointeres", query = "SELECT m FROM Movimiento m WHERE m.montointeres = :montointeres")
    ,
    @NamedQuery(name = "Movimiento.findByRetencion", query = "SELECT m FROM Movimiento m WHERE m.retencion = :retencion")
    ,
    @NamedQuery(name = "Movimiento.findByCancelacion", query = "SELECT m FROM Movimiento m WHERE m.cancelacion = :cancelacion")
    ,
    @NamedQuery(name = "Movimiento.findByMontocancelacion", query = "SELECT m FROM Movimiento m WHERE m.montocancelacion = :montocancelacion")
    ,
    @NamedQuery(name = "Movimiento.findByMontosaldo", query = "SELECT m FROM Movimiento m WHERE m.montosaldo = :montosaldo")
    ,
    @NamedQuery(name = "Movimiento.findByMontoimpuesto", query = "SELECT m FROM Movimiento m WHERE m.montoimpuesto = :montoimpuesto")
    ,
    @NamedQuery(name = "Movimiento.findByTasaimpuesto", query = "SELECT m FROM Movimiento m WHERE m.tasaimpuesto = :tasaimpuesto")
    ,
    @NamedQuery(name = "Movimiento.findByIddestino", query = "SELECT m FROM Movimiento m WHERE m.iddestino = :iddestino")
    ,
    @NamedQuery(name = "Movimiento.findByCancelado", query = "SELECT m FROM Movimiento m WHERE m.cancelado = :cancelado")
    ,
    @NamedQuery(name = "Movimiento.findByIdoficial", query = "SELECT m FROM Movimiento m WHERE m.idoficial = :idoficial")
    ,
    @NamedQuery(name = "Movimiento.findByIdanalista", query = "SELECT m FROM Movimiento m WHERE m.idanalista = :idanalista")
    ,
    @NamedQuery(name = "Movimiento.findByIdremision", query = "SELECT m FROM Movimiento m WHERE m.idremision = :idremision")
    ,
    @NamedQuery(name = "Movimiento.findByHorasolicitud", query = "SELECT m FROM Movimiento m WHERE m.horasolicitud = :horasolicitud")
    ,
    @NamedQuery(name = "Movimiento.findByHoraaprobado", query = "SELECT m FROM Movimiento m WHERE m.horaaprobado = :horaaprobado")
    ,
    @NamedQuery(name = "Movimiento.findByIdcambiorubro", query = "SELECT m FROM Movimiento m WHERE m.idcambiorubro = :idcambiorubro")
    ,
    @NamedQuery(name = "Movimiento.findByIdliquidador", query = "SELECT m FROM Movimiento m WHERE m.idliquidador = :idliquidador")
    ,
    @NamedQuery(name = "Movimiento.findByCompromisopago", query = "SELECT m FROM Movimiento m WHERE m.compromisopago = :compromisopago")})
public class Movimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsocio")
    private long idsocio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idfuncionario")
    private long idfuncionario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmoneda")
    private long idmoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerosolicitud")
    private long numerosolicitud;
    @Column(name = "fechasolicitud")
    @Temporal(TemporalType.DATE)
    private Date fechasolicitud;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "montosolicitud")
    private Long montosolicitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "plazosolicitud")
    private short plazosolicitud;
    @Size(max = 254)
    @Column(name = "observacionsolicitud")
    private String observacionsolicitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "identidad")
    private long identidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numeroboleta")
    private long numeroboleta;
    @Column(name = "numeroacta")
    private Long numeroacta;
    @Column(name = "fechaacta")
    @Temporal(TemporalType.DATE)
    private Date fechaacta;
    @Column(name = "montoaprobado")
    private Long montoaprobado;
    @Column(name = "plazoaprobado")
    private Short plazoaprobado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tasainteres")
    private Long tasainteres;
    @Column(name = "fechaaprobado")
    @Temporal(TemporalType.DATE)
    private Date fechaaprobado;
    @Size(max = 254)
    @Column(name = "observacionaprobado")
    private String observacionaprobado;
    @Column(name = "fechaprimervencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaprimervencimiento;
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechavencimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestado")
    private long idestado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idautorizador")
    private long idautorizador;
    @Column(name = "tasaretencion")
    private Long tasaretencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idpromotor")
    private long idpromotor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuenta")
    private long idcuenta;
    @Column(name = "fechaanulado")
    @Temporal(TemporalType.DATE)
    private Date fechaanulado;
    @Size(max = 254)
    @Column(name = "observacionanulado")
    private String observacionanulado;
    @Column(name = "fecharechazado")
    @Temporal(TemporalType.DATE)
    private Date fecharechazado;
    @Size(max = 254)
    @Column(name = "observacionrechazado")
    private String observacionrechazado;
    @Column(name = "fechareconsiderado")
    @Temporal(TemporalType.DATE)
    private Date fechareconsiderado;
    @Size(max = 254)
    @Column(name = "observacionreconsiderado")
    private String observacionreconsiderado;
    @Column(name = "fechageneracion")
    @Temporal(TemporalType.DATE)
    private Date fechageneracion;
    @Size(max = 254)
    @Column(name = "observaciongeneracion")
    private String observaciongeneracion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerooperacion")
    private long numerooperacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idregional")
    private long idregional;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipocredito")
    private long idtipocredito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montointeres")
    private Long montointeres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "retencion")
    private Long retencion;
    @Column(name = "cancelacion")
    private Boolean cancelacion;
    @Column(name = "montocancelacion")
    private Long montocancelacion;
    @Column(name = "montosaldo")
    private Long montosaldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoimpuesto")
    private Long montoimpuesto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tasaimpuesto")
    private Long tasaimpuesto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "iddestino")
    private long iddestino;
    @Column(name = "cancelado")
    private Boolean cancelado;
    @Column(name = "idoficial")
    private Long idoficial;
    @Column(name = "idanalista")
    private Long idanalista;
    @Column(name = "idremision")
    private Long idremision;
    @Size(max = 8)
    @Column(name = "horasolicitud")
    private String horasolicitud;
    @Size(max = 8)
    @Column(name = "horaaprobado")
    private String horaaprobado;
    @Column(name = "idcambiorubro")
    private Long idcambiorubro;
    @Column(name = "idliquidador")
    private Long idliquidador;
    @Column(name = "compromisopago")
    private Boolean compromisopago;

    public Movimiento() {
    }

    public Movimiento(Long id) {
        this.id = id;
    }

    public Movimiento(Long id, long idsocio, long idmoneda, long numerosolicitud, short plazosolicitud, long identidad, long numeroboleta, Long tasainteres, long idestado, long idautorizador, long idpromotor, long idcuenta, long numerooperacion, long idregional, long idtipocredito, Long montointeres, Long retencion, Long montoimpuesto, Long tasaimpuesto, long iddestino) {
        this.id = id;
        this.idsocio = idsocio;
        this.idmoneda = idmoneda;
        this.numerosolicitud = numerosolicitud;
        this.plazosolicitud = plazosolicitud;
        this.identidad = identidad;
        this.numeroboleta = numeroboleta;
        this.tasainteres = tasainteres;
        this.idestado = idestado;
        this.idautorizador = idautorizador;
        this.idpromotor = idpromotor;
        this.idcuenta = idcuenta;
        this.numerooperacion = numerooperacion;
        this.idregional = idregional;
        this.idtipocredito = idtipocredito;
        this.montointeres = montointeres;
        this.retencion = retencion;
        this.montoimpuesto = montoimpuesto;
        this.tasaimpuesto = tasaimpuesto;
        this.iddestino = iddestino;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(long idsocio) {
        this.idsocio = idsocio;
    }

    public long getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(long idmoneda) {
        this.idmoneda = idmoneda;
    }

    public long getNumerosolicitud() {
        return numerosolicitud;
    }

    public void setNumerosolicitud(long numerosolicitud) {
        this.numerosolicitud = numerosolicitud;
    }

    public Date getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(Date fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public Long getMontosolicitud() {
        return montosolicitud;
    }

    public void setMontosolicitud(Long montosolicitud) {
        this.montosolicitud = montosolicitud;
    }

    public short getPlazosolicitud() {
        return plazosolicitud;
    }

    public void setPlazosolicitud(short plazosolicitud) {
        this.plazosolicitud = plazosolicitud;
    }

    public String getObservacionsolicitud() {
        return observacionsolicitud;
    }

    public void setObservacionsolicitud(String observacionsolicitud) {
        this.observacionsolicitud = observacionsolicitud;
    }

    public long getIdentidad() {
        return identidad;
    }

    public void setIdentidad(long identidad) {
        this.identidad = identidad;
    }

    public long getNumeroboleta() {
        return numeroboleta;
    }

    public void setNumeroboleta(long numeroboleta) {
        this.numeroboleta = numeroboleta;
    }

    public Long getNumeroacta() {
        return numeroacta;
    }

    public void setNumeroacta(Long numeroacta) {
        this.numeroacta = numeroacta;
    }

    public Date getFechaacta() {
        return fechaacta;
    }

    public void setFechaacta(Date fechaacta) {
        this.fechaacta = fechaacta;
    }

    public Long getMontoaprobado() {
        return montoaprobado;
    }

    public void setMontoaprobado(Long montoaprobado) {
        this.montoaprobado = montoaprobado;
    }

    public Short getPlazoaprobado() {
        return plazoaprobado;
    }

    public void setPlazoaprobado(Short plazoaprobado) {
        this.plazoaprobado = plazoaprobado;
    }

    public Long getTasainteres() {
        return tasainteres;
    }

    public void setTasainteres(Long tasainteres) {
        this.tasainteres = tasainteres;
    }

    public Date getFechaaprobado() {
        return fechaaprobado;
    }

    public void setFechaaprobado(Date fechaaprobado) {
        this.fechaaprobado = fechaaprobado;
    }

    public String getObservacionaprobado() {
        return observacionaprobado;
    }

    public void setObservacionaprobado(String observacionaprobado) {
        this.observacionaprobado = observacionaprobado;
    }

    public Date getFechaprimervencimiento() {
        return fechaprimervencimiento;
    }

    public void setFechaprimervencimiento(Date fechaprimervencimiento) {
        this.fechaprimervencimiento = fechaprimervencimiento;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public long getIdestado() {
        return idestado;
    }

    public void setIdestado(long idestado) {
        this.idestado = idestado;
    }

    public long getIdautorizador() {
        return idautorizador;
    }

    public void setIdautorizador(long idautorizador) {
        this.idautorizador = idautorizador;
    }

    public Long getTasaretencion() {
        return tasaretencion;
    }

    public void setTasaretencion(Long tasaretencion) {
        this.tasaretencion = tasaretencion;
    }

    public long getIdpromotor() {
        return idpromotor;
    }

    public void setIdpromotor(long idpromotor) {
        this.idpromotor = idpromotor;
    }

    public long getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(long idcuenta) {
        this.idcuenta = idcuenta;
    }

    public Date getFechaanulado() {
        return fechaanulado;
    }

    public void setFechaanulado(Date fechaanulado) {
        this.fechaanulado = fechaanulado;
    }

    public String getObservacionanulado() {
        return observacionanulado;
    }

    public void setObservacionanulado(String observacionanulado) {
        this.observacionanulado = observacionanulado;
    }

    public Date getFecharechazado() {
        return fecharechazado;
    }

    public void setFecharechazado(Date fecharechazado) {
        this.fecharechazado = fecharechazado;
    }

    public String getObservacionrechazado() {
        return observacionrechazado;
    }

    public void setObservacionrechazado(String observacionrechazado) {
        this.observacionrechazado = observacionrechazado;
    }

    public Date getFechareconsiderado() {
        return fechareconsiderado;
    }

    public void setFechareconsiderado(Date fechareconsiderado) {
        this.fechareconsiderado = fechareconsiderado;
    }

    public String getObservacionreconsiderado() {
        return observacionreconsiderado;
    }

    public void setObservacionreconsiderado(String observacionreconsiderado) {
        this.observacionreconsiderado = observacionreconsiderado;
    }

    public Date getFechageneracion() {
        return fechageneracion;
    }

    public void setFechageneracion(Date fechageneracion) {
        this.fechageneracion = fechageneracion;
    }

    public String getObservaciongeneracion() {
        return observaciongeneracion;
    }

    public void setObservaciongeneracion(String observaciongeneracion) {
        this.observaciongeneracion = observaciongeneracion;
    }

    public long getNumerooperacion() {
        return numerooperacion;
    }

    public void setNumerooperacion(long numerooperacion) {
        this.numerooperacion = numerooperacion;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }

    public long getIdtipocredito() {
        return idtipocredito;
    }

    public void setIdtipocredito(long idtipocredito) {
        this.idtipocredito = idtipocredito;
    }

    public Long getMontointeres() {
        return montointeres;
    }

    public void setMontointeres(Long montointeres) {
        this.montointeres = montointeres;
    }

    public Long getRetencion() {
        return retencion;
    }

    public void setRetencion(Long retencion) {
        this.retencion = retencion;
    }

    public Boolean getCancelacion() {
        return cancelacion;
    }

    public void setCancelacion(Boolean cancelacion) {
        this.cancelacion = cancelacion;
    }

    public Long getMontocancelacion() {
        return montocancelacion;
    }

    public void setMontocancelacion(Long montocancelacion) {
        this.montocancelacion = montocancelacion;
    }

    public Long getMontosaldo() {
        return montosaldo;
    }

    public void setMontosaldo(Long montosaldo) {
        this.montosaldo = montosaldo;
    }

    public Long getMontoimpuesto() {
        return montoimpuesto;
    }

    public void setMontoimpuesto(Long montoimpuesto) {
        this.montoimpuesto = montoimpuesto;
    }

    public Long getTasaimpuesto() {
        return tasaimpuesto;
    }

    public void setTasaimpuesto(Long tasaimpuesto) {
        this.tasaimpuesto = tasaimpuesto;
    }

    public long getIddestino() {
        return iddestino;
    }

    public void setIddestino(long iddestino) {
        this.iddestino = iddestino;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public Long getIdoficial() {
        return idoficial;
    }

    public void setIdoficial(Long idoficial) {
        this.idoficial = idoficial;
    }

    public Long getIdanalista() {
        return idanalista;
    }

    public void setIdanalista(Long idanalista) {
        this.idanalista = idanalista;
    }

    public Long getIdremision() {
        return idremision;
    }

    public void setIdremision(Long idremision) {
        this.idremision = idremision;
    }

    public String getHorasolicitud() {
        return horasolicitud;
    }

    public void setHorasolicitud(String horasolicitud) {
        this.horasolicitud = horasolicitud;
    }

    public String getHoraaprobado() {
        return horaaprobado;
    }

    public void setHoraaprobado(String horaaprobado) {
        this.horaaprobado = horaaprobado;
    }

    public Long getIdcambiorubro() {
        return idcambiorubro;
    }

    public void setIdcambiorubro(Long idcambiorubro) {
        this.idcambiorubro = idcambiorubro;
    }

    public Long getIdliquidador() {
        return idliquidador;
    }

    public void setIdliquidador(Long idliquidador) {
        this.idliquidador = idliquidador;
    }

    public Boolean getCompromisopago() {
        return compromisopago;
    }

    public void setCompromisopago(Boolean compromisopago) {
        this.compromisopago = compromisopago;
    }

    public long getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(long idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimiento)) {
            return false;
        }
        Movimiento other = (Movimiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Movimiento[ id=" + id + " ]";
    }

}
