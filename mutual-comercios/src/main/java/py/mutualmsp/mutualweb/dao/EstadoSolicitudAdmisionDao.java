/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.EstadoSolicitudAdmision;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class EstadoSolicitudAdmisionDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<EstadoSolicitudAdmision> listaEstadoSolicitudAdmision() {
        List<EstadoSolicitudAdmision> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("EstadoSolicitudAdmision.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(EstadoSolicitudAdmision estadoSolicitudAdmision) {
        try {
            em.merge(estadoSolicitudAdmision);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(EstadoSolicitudAdmision estadoSolicitudAdmision) {
        try {
            em.remove(em.contains(estadoSolicitudAdmision) ? estadoSolicitudAdmision : em.merge(estadoSolicitudAdmision));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<EstadoSolicitudAdmision> getListEstadoSolicitudAdmision(String valor) {
        List<EstadoSolicitudAdmision> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")){
                lista = em.createNativeQuery("SELECT * FROM public.estado_solicitud_admision " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", EstadoSolicitudAdmision.class)
                   .getResultList();
            } else {
                lista = em.createQuery("select e from EstadoSolicitudAdmision e")
                   .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public EstadoSolicitudAdmision getPendiente() {
        EstadoSolicitudAdmision pendiente = null;
        try {
            pendiente = (EstadoSolicitudAdmision) em.createNativeQuery("SELECT * FROM public.estado_solicitud_admision " +
            "               where  lower(descripcion) like lower('%PENDIENTE%');", EstadoSolicitudAdmision.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            pendiente = (EstadoSolicitudAdmision) em.createQuery("select e from EstadoSolicitudAdmision e")
                .getResultList();
        }
        return pendiente;
    }
}
