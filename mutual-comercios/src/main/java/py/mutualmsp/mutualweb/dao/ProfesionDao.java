/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Profesion;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class ProfesionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Profesion> listaProfesion() {
        List<Profesion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Profesion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Profesion profesion){
        try {
            em.merge(profesion);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Profesion profesion) {
        try {
            em.remove(em.contains(profesion) ? profesion : em.merge(profesion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Profesion> getListProfesion(String valor) {
        List<Profesion> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.profesion " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Profesion.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select p from Profesion p")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Profesion getNinguno() {
        Profesion ninguno = null;
        try {
            ninguno = (Profesion) em.createNativeQuery("SELECT * FROM public.profesion " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Profesion.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Profesion) em.createQuery("select p from Profesion p")
                .getResultList();
        }
        return ninguno;
    }
}
