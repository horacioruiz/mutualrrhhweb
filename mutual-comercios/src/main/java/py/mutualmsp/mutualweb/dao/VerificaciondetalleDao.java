/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class VerificaciondetalleDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<Verificaciondetalle> listaVerificaciondetalle() {
        List<Verificaciondetalle> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Verificaciondetalle.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Verificaciondetalle verificaciondetalle) {
        try {
            em.merge(verificaciondetalle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(Verificaciondetalle verificaciondetalle) {
        try {
            em.remove(em.contains(verificaciondetalle) ? verificaciondetalle : em.merge(verificaciondetalle));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Verificaciondetalle> getListVerificaciondetalle(Long idverificacion) {
        List<Verificaciondetalle> lista = new ArrayList<>();
        try {
            lista = em.createNativeQuery("SELECT * FROM public.verificaciondetalle " +
"                   where  idverificacion ="+idverificacion+" order by idverificaciondetalle;", Verificaciondetalle.class)
                   .getResultList();            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    
}
