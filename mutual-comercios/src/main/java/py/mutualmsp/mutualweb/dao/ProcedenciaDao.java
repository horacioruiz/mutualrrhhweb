/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Procedencia;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class ProcedenciaDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Procedencia> listaProcedencia() {
        List<Procedencia> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Procedencia.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Procedencia procedencia){
        try {
            em.merge(procedencia);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Procedencia procedencia) {
        try {
            em.remove(em.contains(procedencia) ? procedencia : em.merge(procedencia));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Procedencia> getListProcedencia(String valor) {
        List<Procedencia> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.procedencia " +
               " where  lower(descripcion) like lower('%"+valor+"%');", Procedencia.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select p from Procedencia p")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Procedencia getNinguno() {
        Procedencia ninguno = null;
        try {
            ninguno = (Procedencia) em.createNativeQuery("SELECT * FROM public.procedencia " +
            " where  lower(descripcion) like lower('%NINGUNO%');", Procedencia.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Procedencia) em.createQuery("select p from Procedencia p")
                .getResultList();
        }
        return ninguno;
    }
}
