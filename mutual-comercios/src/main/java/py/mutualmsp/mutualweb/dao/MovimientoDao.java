/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Entidad;
import py.mutualmsp.mutualweb.entities.Movimiento;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class MovimientoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Movimiento> listaMovimientos() {
        List<Movimiento> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Movimiento.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Movimiento pais) {
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Movimiento guardarRetornarData(Movimiento pais) {
        try {
            em.merge(pais);
            em.flush();
            return pais;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void borrar(Movimiento pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Movimiento> getListMovimiento(String valor) {
        List<Movimiento> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.boleta_cab "
                        + "                   where  id=" + valor + ";", Movimiento.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select p from Movimiento p")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Movimiento buscarPorNroBoleta(String value) {
        Movimiento mov = new Movimiento();

//        try {
//            String sql = "SELECT * FROM public.movimiento WHERE idcuenta=105 "
//                    + "AND numeroboleta=" + value;
//            System.out.println("SQL -> " + sql);
//            mov = (Movimiento) em.createNativeQuery(sql, Movimiento.class)
//                    .getSingleResult();
//            return mov;
//        } catch (Exception e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//            System.out.println("-> " + e.getStackTrace());
//            System.out.println("-> " + e.getMessage());
//            return null;
//        }
        List<Movimiento> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Movimiento.findByNumeroboleta")
                    .setParameter("numeroboleta", Long.parseLong(value))
                    .getResultList();
            return lista.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public Movimiento buscarPorNroBoleta(String boleta, Entidad comercio, String periodo, String ano) {
        List<Movimiento> lista = new ArrayList<>();

        try {
            lista = em.createQuery("SELECT * FROM public.movimiento WHERE idcuenta=105 AND identidad=" + comercio.getId() + " "
                    + "AND (TO_CHAR(fecha, 'YYYY-MM')>='" + ano + "-" + periodo + "' AND TO_CHAR(fecha, 'YYYY-MM')<='" + ano + "-" + periodo + "') ")
                    .getResultList();
            return lista.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
