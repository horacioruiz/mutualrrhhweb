/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Requisitooperacion;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class RequisitooperacionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Requisitooperacion> listaRequisitooperacion() {
        List<Requisitooperacion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Requisitooperacion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Requisitooperacion requisito){
        try {
            em.merge(requisito);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Requisitooperacion requisitooperacion) {
        try {
            em.remove(em.contains(requisitooperacion) ? requisitooperacion : em.merge(requisitooperacion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Requisitooperacion> getListRequisito(String valor) {
        List<Requisitooperacion> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.requisitooperacion " +
"                   where  idtipooperacion="+valor+";", Requisitooperacion.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select ro from Requisitooperacion ro")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Requisitooperacion> getListRequisitoExistente(Long idrequisito, Long idtipooperacion) {
        List<Requisitooperacion> lista = new ArrayList<>();
       try {
           lista = em.createNativeQuery("SELECT * FROM public.requisitooperacion " +
            "where  idrequisito="+idrequisito+" and idtipooperacion="+idtipooperacion+";", Requisitooperacion.class)
                   .getResultList();
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Requisitooperacion> getRequisitoOperacionByTipoOperacion(Long id){
        List<Requisitooperacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select r from Requisitooperacion r where r.idtipooperacion.idtipooperacion = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }
}
