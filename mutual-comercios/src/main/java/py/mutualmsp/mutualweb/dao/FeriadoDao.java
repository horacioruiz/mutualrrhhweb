/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.mutualmsp.mutualweb.entities.Feriado;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class FeriadoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Feriado> listaFeriado() {
        List<Feriado> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Feriado.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Feriado feriado) {
        try {
            em.merge(feriado);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Feriado feriado) {
        try {
            em.remove(em.contains(feriado) ? feriado : em.merge(feriado));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Feriado> getListFeriado(String valor) {
        List<Feriado> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.feriado "
                        + "                   where  id=" + valor + ";", Feriado.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Feriado c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Feriado> getFeriadoByDescripcion(String valor) {
        List<Feriado> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.feriado "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Feriado.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Feriado c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public List<Feriado> getFeriadoByPeriodo(String valor) {
        List<Feriado> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.feriado "
                        + "                   where lower(periodo) LIKE '" + valor + "%';", Feriado.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Feriado c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Feriado getNinguno() {
        Feriado ninguno = null;
        try {
            ninguno = (Feriado) em.createNativeQuery("SELECT * FROM rrhh.feriado "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Feriado.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Feriado) em.createQuery("select c from Feriado c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Feriado> listarFeriadoPorPeriodo(Date inicio, Date fin) {
        try {
            String sql = "SELECT * FROM rrhh.feriado WHERE fecha BETWEEN '" + inicio + "' AND '" + fin + "';";
            Query query = em.createNativeQuery(sql, Feriado.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
