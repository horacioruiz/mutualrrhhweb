/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Regional;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class RegionalDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Regional> listaRegional() {
        List<Regional> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Regional.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Regional region){
        try {
            em.merge(region);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Regional region) {
        try {
            em.remove(em.contains(region) ? region : em.merge(region));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Regional> getListRegional(String value) {
        List<Regional> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.regional " +
"                   where  lower(descripcion) like lower('%"+value+"%');", Regional.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select r from Regional r")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Regional> getListRegionalExistente(String value) {
        List<Regional> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.regional " +
                "where  lower(descripcion) like lower('%"+value+"%');", Regional.class)
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
}
