/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Vacaciones;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class VacacionesDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Vacaciones> listaVacaciones() {
        List<Vacaciones> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from Vacaciones c").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Vacaciones listaVacacionesPorId(long idVaca) {
        Vacaciones vacas = new Vacaciones();
        try {
            vacas = (Vacaciones) em.createQuery("select c from Vacaciones c WHERE c.id=:idVaca")
                    .setParameter("idVaca", idVaca).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vacas;
    }

    public Vacaciones guardar(Vacaciones cargo) {
        if (cargo.getId() == null) {
            em.persist(cargo);
        } else {
            em.merge(cargo);
        }
        em.flush();
        return cargo;

    }

    public void borrar(Vacaciones cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Vacaciones> getListVacaciones(String valor) {
        List<Vacaciones> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones "
                        + "where id=" + valor + " ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Vacaciones> getVacacionesByDescripcion(String valor) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.idfuncionario=:idFunc";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", Long.parseLong(valor));
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vacaciones getNinguno() {
        Vacaciones ninguno = null;
        try {
            ninguno = (Vacaciones) em.createNativeQuery("SELECT * FROM rrhh.formulario "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Vacaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Vacaciones) em.createQuery("select c from Vacaciones c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Vacaciones> listadeVacaciones() {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p ORDER BY p.id";
            Query query = em.createQuery(sql, Vacaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacaciones(long idFuncionario) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.idfuncionario=:idFunc ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", idFuncionario);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vacaciones listadeVacacionesPorPeriodo(long idFuncionario, String periodo) {
        try {
            String sql = "select * ";
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                    + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE p.periodo='" + periodo + "' and "
                    + " f.idfuncionario=" + idFuncionario + " ORDER BY p.id ASC";
            Query query = em.createNativeQuery(sql, Vacaciones.class);
//            query.setParameter("idFunc", idFuncionario);
//            query.setParameter("periodo", periodo);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results.get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesON(long idFuncionario) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.idfuncionario=:idFunc AND p.cantdiarestante>0 ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", idFuncionario);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesPorCiFuncionario(String value) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.cedula=:ci ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("ci", "'" + value + "'");
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listarPorVacacionesSolicitud(String periodo) {
//        try {
//            String sql = "select p ";
//            sql += "from Vacaciones p WHERE p.periodo=:periodo AND p.solicitud is not null ORDER BY p.id ASC";
//            Query query = em.createQuery(sql, Vacaciones.class);
//            query.setParameter("periodo", "'" + periodo + "'");
//            int i = query.getResultList().size();
//            System.out.print(i);
//            List<Vacaciones> results = query.getResultList();
//            return results;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
        List<Vacaciones> lista = new ArrayList<>();
        try {
            if (periodo != null && !periodo.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones "
                        + "where periodo='" + periodo + "' AND idsolicitud is not null ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Vacaciones> listadeVacaciones(Long idfuncionario, Dependencia depen, String periodo) {
        try {
            String sql = "select * ";
            Query query = null;
            if (idfuncionario == 0) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE p.periodo='" + periodo + "' and "
                        + " depen.iddependenciapadre=" + depen.getIddependenciapadre() + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            } else {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE p.periodo='" + periodo + "' and "
                        + " depen.iddependenciapadre=" + depen.getIddependenciapadre() + " AND "
                        + " f.idfuncionario=" + idfuncionario + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            }

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacaciones(Long idfuncionario, String periodo) {
        try {
            String sql = "select * ";
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                    + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE p.periodo='" + periodo + "' and "
                    + " f.idfuncionario=" + idfuncionario + " ORDER BY p.id ASC";
            Query query = em.createNativeQuery(sql, Vacaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesCargo(Long idfuncionario, Dependencia depen) {
        try {
            String sql = "select * ";
            Query query = null;
            if (idfuncionario == 0 && depen != null) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE depen.iddependencia=" + depen.getIddependencia() + " "
                        + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            } else if (idfuncionario != 0 && depen != null) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE depen.iddependencia=" + depen.getIddependencia() + " and "
                        + " f.idfuncionario=" + idfuncionario + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            } else if (idfuncionario != 0 && depen == null) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE depen.iddependencia=" + depen.getIddependencia() + " and "
                        + " f.idfuncionario=" + idfuncionario + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            } else {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                        + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE depen.iddependencia=" + depen.getIddependencia() + " "
                        + " ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class);
            }

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesCargoData(Long idfuncionario, Dependencia depen, String periodo) {
        String periodoData = "";
        try {
            if (!periodo.equals("")) {
                periodoData = " AND p.periodo='" + periodo + "'";
            }
            String sql = "select * ";
            Query query = null;
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                    + " left join public.dependencia depen on depen.iddependencia=f.iddependencia WHERE depen.iddependenciapadre=" + depen.getIddependencia() + " "
                    + periodoData + " ORDER BY p.id ASC";
            query = em.createNativeQuery(sql, Vacaciones.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            if (!periodo.equals("")) {
                periodoData = " WHERE p.periodo='" + periodo + "'";
            }
            String sql = "select * ";
            Query query = null;
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.idfuncionario = p.idfuncionario "
                    + " left join public.dependencia depen on depen.iddependencia=f.iddependencia "
                    + periodoData + " ORDER BY p.id ASC";
            query = em.createNativeQuery(sql, Vacaciones.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;
        }
    }

}
