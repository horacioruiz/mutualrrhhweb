package py.mutualmsp.mutualweb.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Parametro;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class ParametroDao {

    @PersistenceContext
    private EntityManager em;

    public Parametro guardarParametro(Parametro usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Parametro> listadeParametro(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Parametro p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Parametro.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Parametro> listadeParametro() {
        try {
            String sql = "select u from Parametro u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Parametro> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Parametro p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, Parametro.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Parametro getByIdUsuario(Long idusuario) {
        return (Parametro) em.find(Parametro.class, idusuario);
    }

    public List<Parametro> listadeParametroByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from Parametro p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Parametro.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Parametro cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Parametro> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Parametro p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, Parametro.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Parametro listarPorIdFuncionario(Long idfuncionario) {
        try {
            return (Parametro) em.createQuery("select hf from Parametro hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", idfuncionario)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public Parametro getById(Long id) {
        Parametro marcacion = null;
        try {
            marcacion = (Parametro) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", Parametro.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new Parametro();
        }
        return marcacion;
    }

    public Parametro listarPorIdReloj(Long idReloj) {
        try {
            return (Parametro) em.createQuery("select hf from Parametro hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<Parametro> listarPorTipoCodigo(String tolerancia) {
        try {
            String sql = "select p ";
            sql += "from Parametro p JOIN FETCH p.tipo f WHERE f.codigo like :cod";
            Query query = em.createQuery(sql, Parametro.class).setParameter("cod", tolerancia + "%");
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Parametro> listarPorCodigo(String param) {
        try {
            String sql = "select p ";
            sql += "from Parametro p JOIN FETCH p.tipo f WHERE p.codigo like :cod";
            Query query = em.createQuery(sql, Parametro.class).setParameter("cod", param + "%");
            int i = query.getResultList().size();
            System.out.print(i);
            List<Parametro> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
