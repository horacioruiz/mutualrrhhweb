/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Barrio;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class BarrioDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Barrio> listaBarrio() {
        List<Barrio> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Barrio.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Barrio barrio){
        try {
            em.merge(barrio);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Barrio barrio) {
        try {
            em.remove(em.contains(barrio) ? barrio : em.merge(barrio));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Barrio> getListBarrio(String valor) {
        List<Barrio> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.barrio " +
"                   where  id="+valor+";", Barrio.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select b from Barrio b")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
