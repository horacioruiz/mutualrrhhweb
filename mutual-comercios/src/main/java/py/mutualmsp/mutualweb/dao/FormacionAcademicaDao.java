/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.FormacionAcademica;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class FormacionAcademicaDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<FormacionAcademica> listaFormacionAcademica() {
        List<FormacionAcademica> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("FormacionAcademica.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(FormacionAcademica formacionAcademica){
        try {
            em.merge(formacionAcademica);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(FormacionAcademica formacionAcademica) {
        try {
            em.remove(em.contains(formacionAcademica) ? formacionAcademica : em.merge(formacionAcademica));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<FormacionAcademica> getListFormacionAcademica(String valor) {
        List<FormacionAcademica> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.formacion_academica " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", FormacionAcademica.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select d from FormacionAcademica d")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public FormacionAcademica getNinguno() {
        FormacionAcademica ninguno = null;
        try {
            ninguno = (FormacionAcademica) em.createNativeQuery("SELECT * FROM public.formacion_academica " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", FormacionAcademica.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (FormacionAcademica) em.createQuery("select d from FormacionAcademica d")
                .getResultList();
        }
        return ninguno;
    }
}
