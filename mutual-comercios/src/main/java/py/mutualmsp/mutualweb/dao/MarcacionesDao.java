/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.Marcaciones;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class MarcacionesDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Marcaciones> listaMarcaciones() {
        List<Marcaciones> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Marcaciones.findAll").getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public void guardar(Marcaciones cargo) {
        cargo.setHsextra(Objects.isNull(cargo.getHsextra()) ? 0 : cargo.getHsextra());

        long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(cargo.getOutasignada().getTime() - cargo.getInasignada().getTime());
        minutesEntSal -= 40;
        cargo.setMintrabajadaasignada(minutesEntSal);

        try {
            if (cargo.getObservacion() == null) {
                cargo.setObservacion("");
            }
        } catch (Exception e) {
            cargo.setObservacion("");
        } finally {
        }
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }

    public void borrar(Marcaciones cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public List<Marcaciones> getListMarcaciones(String valor) {
        List<Marcaciones> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.marcaciones "
                        + "                   where  id=" + valor + ";", Marcaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Marcaciones c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public Marcaciones getNinguno() {
        Marcaciones ninguno = null;
        try {
            ninguno = (Marcaciones) em.createNativeQuery("SELECT * FROM rrhh.marcaciones "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Marcaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            ninguno = (Marcaciones) em.createQuery("select c from Marcaciones c")
                    .getResultList();
        }
        return ninguno;
    }

    public Marcaciones getMarcacionById(Long id) {
        Marcaciones marcacion = null;
        try {
            marcacion = (Marcaciones) em.createNativeQuery("SELECT * FROM rrhh.marcaciones "
                    + " where id=" + id + ";", Marcaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            marcacion = new Marcaciones();
        }
        return marcacion;
    }

    public Marcaciones getByIdRelojFecha(HorarioFuncionario horarioFuncionario, Date fecha) {
        Marcaciones ninguno = null;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String text = formatter.format(fecha);
        try {
            String sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf "
                    + "on hf.idreloj=m.idreloj where hf.idreloj='" + horarioFuncionario.getIdreloj() + "' and (m.fecha>='" + text + " 00:00:00' AND m.fecha<='" + text + " 23:59:59');";
            ninguno = (Marcaciones) em.createNativeQuery(sql, Marcaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            ninguno = null;
        }
        return ninguno;
    }

    public List<Marcaciones> getByIdReloj(String valor) {
        List<Marcaciones> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj where hf.idreloj=" + valor + " ORDER BY hf.idreloj, m.fecha DESC", Marcaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Marcaciones c ORDER BY c.fecha DESC")
                        .getResultList();
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public List<Marcaciones> getByFuncionario(Funcionario valor) {
        List<Marcaciones> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where f.idfuncionario=" + valor.getIdfuncionario() + " ORDER BY hf.idreloj, m.fecha DESC", Marcaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Marcaciones c ORDER BY c.fecha DESC")
                        .getResultList();
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public List<Marcaciones> listarPorFechas(Date fechaDesde, Date fechaHasta) {
        List<Marcaciones> lista = new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
        String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";
        try {
            if (fechaDesde != null && !fechaDesde.equals("") && fechaHasta != null && !fechaHasta.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "' ORDER BY hf.idreloj, m.fecha DESC", Marcaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Marcaciones c ORDER BY c.fecha DESC")
                        .getResultList();
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public List<Marcaciones> getByFilter(Funcionario value, Date fechaDesde, Date fechaHasta, String estado, long idReloj) {
        List<Marcaciones> lista = new ArrayList<>();
        String sql = "";
        String filterEstado = "";
        String filterReloj = "";
        if (estado != null) {
            if (estado.trim().equalsIgnoreCase("TEMPRANO")) {
                filterEstado = "AND m.minllegadatemprana>=0 AND m.minllegadatardia=0";
            } else if (estado.trim().equalsIgnoreCase("TARDE")) {
                filterEstado = "AND m.minllegadatardia>0";
            } else if (estado.trim().equalsIgnoreCase("OBSERVACION")) {
                filterEstado = "AND m.mintrabajada<0";
            }
        }
        if (idReloj > 0) {
            filterReloj = " AND hf.idreloj=" + idReloj;
        }
        if (value != null && fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";

            if (fechaDesdeText.equalsIgnoreCase(fechaHastaText)) {
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha >= '" + fechaDesdeText + "' and m.fecha <= '" + fechaDesdeText + "') and hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            } else {
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') and hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            }
        } else if (value != null) {
            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else if (fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";
            if (fechaDesdeText.equalsIgnoreCase(fechaHastaText)) {
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha >= '" + fechaDesdeText + "' and m.fecha <= '" + fechaDesdeText + "') " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            } else {
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            }
        } else {
            if (filterEstado.trim().equalsIgnoreCase("")) {
                filterReloj = filterReloj.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario" + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            } else {
                filterEstado = filterEstado.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            }
        }

        try {
            lista = em.createNativeQuery(sql, Marcaciones.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public Marcaciones getById(Long id) {
        Marcaciones ninguno = null;
        try {
            ninguno = (Marcaciones) em.createQuery("select c from Marcaciones c JOIN FETCH c.solicitud s WHERE c.id=:idMarcacion")
                    .setParameter("idMarcacion", id)
                    .getResultList().get(0);
        } catch (Exception e) {
        } finally {
        }
        return ninguno;
    }

    public Marcaciones getByIdSolicitudProduccion(Long id) {
        Marcaciones ninguno = null;
        try {
            ninguno = (Marcaciones) em.createQuery("select c from Marcaciones c JOIN FETCH c.solicitudProduccionDetalle spd WHERE c.id=:idMarcacion")
                    .setParameter("idMarcacion", id)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("->" + e.getLocalizedMessage());
        } finally {
        }
        return ninguno;
    }

    public List<Marcaciones> getBySolicitudProduccion(Long idSoliProd) {
        List<Marcaciones> ninguno = new ArrayList<>();
        try {
            ninguno = em.createQuery("select c from Marcaciones c JOIN FETCH c.solicitudProduccionDetalle spd WHERE spd.id=:idMarcacion")
                    .setParameter("idMarcacion", idSoliProd)
                    .getResultList();
        } catch (Exception e) {
            System.out.println("->" + e.getLocalizedMessage());
        } finally {
        }
        return ninguno;
    }

    public List<Marcaciones> getBySolicitud(Long idSoli) {
        List<Marcaciones> ninguno = new ArrayList<>();
        try {
            ninguno = em.createQuery("select c from Marcaciones c JOIN FETCH c.solicitud spd WHERE spd.id=:idMarcacion")
                    .setParameter("idMarcacion", idSoli)
                    .getResultList();
        } catch (Exception e) {
            System.out.println("->" + e.getLocalizedMessage());
        } finally {
        }
        return ninguno;
    }

    public List<Marcaciones> getByFilterHoraExtra(Funcionario value, Date fechaDesde, Date fechaHasta, String estado, long idReloj) {
        List<Marcaciones> lista = new ArrayList<>();
        String sql = "";
        String filterEstado = "";
        String filterReloj = "";
        if (estado != null) {
            if (estado.trim().equalsIgnoreCase("SI")) {
                filterEstado = "AND m.hsextra>0 AND m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0";
            } else if (estado.trim().equalsIgnoreCase("NO")) {
                filterEstado = "AND m.hsextra=0 AND m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0";
            }
        }
        if (idReloj > 0) {
            filterReloj = " AND hf.idreloj=" + idReloj;
        }
        if (value != null && fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";

            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0 AND (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') and hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else if (value != null) {
            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0 AND hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else if (fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";

            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0 AND (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else {
            if (filterEstado.trim().equalsIgnoreCase("")) {
                filterReloj = filterReloj.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario" + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            } else {
                filterEstado = filterEstado.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            }
        }

        try {
            lista = em.createNativeQuery(sql, Marcaciones.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }

    public List<Marcaciones> getByFilterHoraExtraHere(Funcionario value, Date fechaDesde, Date fechaHasta, String estado, long idReloj) {
        List<Marcaciones> lista = new ArrayList<>();
        String sql = "";
        String filterEstado = "";
        String filterReloj = "";
        if (estado != null) {
            if (estado.trim().equalsIgnoreCase("SI")) {
                filterEstado = "AND m.hsextra>0 AND m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0";
            } else if (estado.trim().equalsIgnoreCase("NO")) {
                filterEstado = "AND m.hsextra=0 AND m.mintrabajadaasignada<m.mintrabajada AND m.mintrabajada>0";
            }
        }
        if (idReloj > 0) {
            filterReloj = " AND hf.idreloj=" + idReloj;
        }
        if (value != null && fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";

            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') and hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else if (value != null) {
            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where hf.idfuncionario=" + value.getIdfuncionario() + " " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else if (fechaDesde != null && fechaHasta != null) {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaDesdeText = formatter.format(fechaDesde) + " 00:00:00";
            String fechaHastaText = formatter.format(fechaHasta) + " 23:59:59";

            sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario where (m.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "') " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
        } else {
            if (filterEstado.trim().equalsIgnoreCase("")) {
                filterReloj = filterReloj.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario" + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            } else {
                filterEstado = filterEstado.replaceFirst("AND", "WHERE");
                sql = "SELECT * FROM rrhh.marcaciones m left join rrhh.horario_funcionario hf on hf.idreloj=m.idreloj left join funcionario f on f.idfuncionario = hf.idfuncionario " + filterEstado + filterReloj + " ORDER BY hf.idreloj, m.fecha DESC";
            }
        }

        try {
            lista = em.createNativeQuery(sql, Marcaciones.class)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return lista;
    }
}
