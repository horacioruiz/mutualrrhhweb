/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tiporequisito;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TiporequisitoDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<Tiporequisito> listaTiporequisito() {
        List<Tiporequisito> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Tiporequisito.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Tiporequisito tiporequisito) {
        try {
            em.merge(tiporequisito);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(Tiporequisito tiporequisito) {
        try {
            em.remove(em.contains(tiporequisito) ? tiporequisito : em.merge(tiporequisito));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Tiporequisito> getListTiporequisito(String valor) {
        List<Tiporequisito> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")){
                lista = em.createNativeQuery("SELECT * FROM public.tiporequisito " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Tiporequisito.class)
                   .getResultList();
            } else {
                lista = em.createQuery("select t from Tiporequisito t")
                   .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public Tiporequisito getNinguno() {
        Tiporequisito ninguno = null;
        try {
            ninguno = (Tiporequisito) em.createNativeQuery("SELECT * FROM public.tiporequisito " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Tiporequisito.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tiporequisito) em.createQuery("select t from Tiporequisito t")
                .getResultList();
        }
        return ninguno;
    }
    
    
}
