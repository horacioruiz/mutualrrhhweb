package py.mutualmsp.mutualweb.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class HorarioFuncionarioDao {

    @PersistenceContext
    private EntityManager em;

    public HorarioFuncionario guardarHorarioFuncionario(HorarioFuncionario usuarios) {
        if (usuarios.getIdreloj() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionario(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionario() {
        try {
            String sql = "select u from HorarioFuncionario u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionarioTrue() {
        try {
            String sql = "select u from HorarioFuncionario u JOIN FETCH u.funcionario f WHERE f.idestado=13";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioFuncionario getByIdUsuario(Long idusuario) {
        return (HorarioFuncionario) em.find(HorarioFuncionario.class, idusuario);
    }

    public List<HorarioFuncionario> listadeHorarioFuncionarioByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public void borrar(HorarioFuncionario cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public List<HorarioFuncionario> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, HorarioFuncionario.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioFuncionario listarPorIdFuncionario(Long idfuncionario) {
        try {
            return (HorarioFuncionario) em.createQuery("select hf from HorarioFuncionario hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", idfuncionario)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public HorarioFuncionario getById(Long id) {
        HorarioFuncionario marcacion = null;
        try {
            marcacion = (HorarioFuncionario) em.createNativeQuery("SELECT * FROM rrhh.horario_funcionario "
                    + " where idreloj=" + id + ";", HorarioFuncionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            marcacion = new HorarioFuncionario();
        } finally {
        }
        return marcacion;
    }

    public HorarioFuncionario listarPorIdReloj(Long idReloj) {
        try {
            return (HorarioFuncionario) em.createQuery("select hf from HorarioFuncionario hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }
}
