/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.ControlDet;
import py.mutualmsp.mutualweb.entities.Pais;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class ControlDetDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<ControlDet> listaPaises() {
        List<ControlDet> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("ControlDet.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(ControlDet pais) {
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ControlDet guardarRetornarData(ControlDet pais) {
        try {
            em.merge(pais);
            em.refresh(pais);
            return pais;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void borrar(ControlDet pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ControlDet> getListControlDet(String valor) {
        List<ControlDet> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM comercios.control_cab "
                        + "                   where  id=" + valor + ";", Pais.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select p from ControlDet p")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<ControlDet> listarPorIdCab(String valor) {
        List<ControlDet> lista = new ArrayList<>();
        try {
//            if (valor != null && !valor.equals("")) {
            lista = em.createNativeQuery("SELECT * FROM comercios.control_det "
                    + " WHERE idboletacab=" + valor + ";", ControlDet.class)
                    .getResultList();
//            } else {
//                lista = em.createQuery("select p from ControlDet p")
//                        .getResultList();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
