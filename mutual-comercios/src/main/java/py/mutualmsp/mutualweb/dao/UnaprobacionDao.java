/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Unaprobacion;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class UnaprobacionDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Unaprobacion> listaUnaprobacion() {
        List<Unaprobacion> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Unaprobacion.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Unaprobacion cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Unaprobacion cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Unaprobacion> getListUnaprobacion(String valor) {
        List<Unaprobacion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.unaprobacion "
                        + "                   where  id=" + valor + ";", Unaprobacion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Unaprobacion c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Unaprobacion> getUnaprobacionByDescripcion(String valor) {
        List<Unaprobacion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.unaprobacion "
                        + " ;", Unaprobacion.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Unaprobacion c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Unaprobacion getNinguno() {
        Unaprobacion ninguno = null;
        try {
            ninguno = (Unaprobacion) em.createNativeQuery("SELECT * FROM rrhh.unaprobacion "
                    + " ", Unaprobacion.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Unaprobacion) em.createQuery("select c from Unaprobacion c")
                    .getResultList();
        }
        return ninguno;
    }

    public Unaprobacion getOne(Long idFuncionario) {
        Unaprobacion ninguno = null;
        try {
            ninguno = (Unaprobacion) em.createNativeQuery("SELECT * FROM rrhh.unaprobacion "
                    + " where estado=TRUE AND idfuncionario=" + idFuncionario + ";", Unaprobacion.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
//            ninguno = (Unaprobacion) em.createQuery("select c from Unaprobacion c")
//                    .getResultList();
        }
        return ninguno;
    }
}
