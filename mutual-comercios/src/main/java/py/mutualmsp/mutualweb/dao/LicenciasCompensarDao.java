package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class LicenciasCompensarDao {

    @PersistenceContext
    private EntityManager em;

    public LicenciasCompensar guardarLicenciasCompensar(LicenciasCompensar usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<LicenciasCompensar> listadeLicenciasCompensar(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from LicenciasCompensar p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, LicenciasCompensar.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<LicenciasCompensar> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<LicenciasCompensar> listadeLicenciasCompensar() {
        try {
            String sql = "select u from LicenciasCompensar u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<LicenciasCompensar> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from LicenciasCompensar p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, LicenciasCompensar.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<LicenciasCompensar> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LicenciasCompensar getByIdUsuario(Long idusuario) {
        return (LicenciasCompensar) em.find(LicenciasCompensar.class, idusuario);
    }

    public List<LicenciasCompensar> listadeLicenciasCompensarByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from LicenciasCompensar p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, LicenciasCompensar.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<LicenciasCompensar> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(LicenciasCompensar cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<LicenciasCompensar> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from LicenciasCompensar p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, LicenciasCompensar.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<LicenciasCompensar> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LicenciasCompensar listarPorIdFuncionario(Long idfuncionario) {
        try {
            return (LicenciasCompensar) em.createQuery("select hf from LicenciasCompensar hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", idfuncionario)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public LicenciasCompensar getById(Long id) {
        LicenciasCompensar marcacion = null;
        try {
            marcacion = (LicenciasCompensar) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", LicenciasCompensar.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new LicenciasCompensar();
        }
        return marcacion;
    }

    public LicenciasCompensar listarPorIdReloj(Long idReloj) {
        try {
            return (LicenciasCompensar) em.createQuery("select hf from LicenciasCompensar hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<LicenciasCompensar> listarPorFechas(Date fechaDesde, Date fechaHasta, Long idfuncionario) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (idfuncionario != null) {
            filterFuncionario = "AND f.idfuncionario=" + idfuncionario;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.licencia_compensacion p LEFT join funcionario f on p.idfuncionario=f.idfuncionario WHERE p.fecha BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, LicenciasCompensar.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<LicenciasCompensar> listarPorFechasHere(Date fechaDesde, Date fechaHasta, Long idfuncionario, String cod, String parametro) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (cod != null) {
            try {
                String sql = "select * FROM rrhh.licencia_compensacion p LEFT join rrhh.licencias lic on p.idlicencia=lic.id";
                sql += " LEFT join rrhh.parametro param on lic.idparametro=param.id ";
                sql += " LEFT join funcionario f on lic.idfuncionario =f.idfuncionario where "
                        + " param.codigo='" + parametro + "' AND "
                        + " lic.id =" + cod + " ORDER BY p.id DESC";
                Query query = em.createNativeQuery(sql, LicenciasCompensar.class);
                return query.getResultList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (idfuncionario != null) {
                filterFuncionario = " AND f.idfuncionario=" + idfuncionario;
            }
            try {
                String sql = "select * FROM rrhh.licencia_compensacion p LEFT join rrhh.licencias lic on p.idlicencia=lic.id";
                sql += " LEFT join rrhh.parametro param on lic.idparametro=param.id ";
                sql += " LEFT join funcionario f on lic.idfuncionario =f.idfuncionario where "
                        + " param.codigo='" + parametro + "' AND "
                        + " p.fechacompensar >='" + fechaDesdeText + " 00:00:00' AND p.fechacompensar<='" + fechaHastaText + " 23:59:59' " + filterFuncionario + "  ORDER BY p.id DESC";
                Query query = em.createNativeQuery(sql, LicenciasCompensar.class);
                return query.getResultList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new ArrayList<>();
    }

    public List<LicenciasCompensar> listarPorIdLicencia(Long id) {
        try {
            return em.createQuery("select sd from LicenciasCompensar sd join fetch sd.licencia s where s.id = :id ORDER BY sd.id")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.fillInStackTrace());
            System.out.println("-->> " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public List<LicenciasCompensar> listarPorFechaUsuario(Date targetDay, Long idfuncionario) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(targetDay);
        String fechaHastaText = formatter.format(targetDay);
        if (idfuncionario != null) {
            filterFuncionario = "AND lic.idfuncionario=" + idfuncionario;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.licencia_compensacion p LEFT join rrhh.licencias lic on lic.id=p.idlicencia left join funcionario f on lic.idfuncionario=f.idfuncionario "
                    + " WHERE (date(p.fechacompensar) BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') " + filterFuncionario + " AND lic.aprobado=1 ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, LicenciasCompensar.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
