/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Descuento;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class DescuentoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Descuento> listaDescuento() {
        List<Descuento> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Descuento.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Descuento descuento){
        try {
            em.merge(descuento);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Descuento descuento) {
        try {
            em.remove(em.contains(descuento) ? descuento : em.merge(descuento));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Descuento> getListDescuento(String valor) {
        List<Descuento> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.descuento " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Descuento.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select d from Descuento d")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Descuento getNinguno() {
        Descuento ninguno = null;
        try {
            ninguno = (Descuento) em.createNativeQuery("SELECT * FROM public.descuento " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Descuento.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Descuento) em.createQuery("select d from Descuento d")
                .getResultList();
        }
        return ninguno;
    }
}
