/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipocasa;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipocasaDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Tipocasa> listaTipocasa() {
        List<Tipocasa> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Tipocasa.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Tipocasa tipocasa){
        try {
            em.merge(tipocasa);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Tipocasa tipocasa) {
        try {
            em.remove(em.contains(tipocasa) ? tipocasa : em.merge(tipocasa));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tipocasa> getListTipocasa(String valor) {
        List<Tipocasa> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.tipocasa " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Tipocasa.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select t from Tipocasa t")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }

    
    public Tipocasa getNinguno() {
        Tipocasa ninguno = null;
        try {
            ninguno = (Tipocasa) em.createNativeQuery("SELECT * FROM public.tipocasa " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Tipocasa.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tipocasa) em.createQuery("select t from Tipocasa t")
                .getResultList();
        }
        return ninguno;
    }
}
