/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Entidad;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class EntidadDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Entidad> listaEntidad() {
        List<Entidad> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Entidad.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Entidad barrio){
        try {
            em.merge(barrio);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Entidad barrio) {
        try {
            em.remove(em.contains(barrio) ? barrio : em.merge(barrio));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Entidad> getListEntidad(String valor) {
        List<Entidad> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.entidad " +
"                   where  id="+valor+";", Entidad.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select b from Entidad b")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
