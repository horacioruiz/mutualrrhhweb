/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Rubro;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class RubroDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Rubro> listaRubros() {
        List<Rubro> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Rubro.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Rubro rubro){
        try {
            em.merge(rubro);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Rubro rubro) {
        try {
            em.remove(em.contains(rubro) ? rubro : em.merge(rubro));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Rubro> getListRubro(String valor) {
        List<Rubro> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.rubro " +
"                   where  id="+valor+";", Rubro.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select r from Rubro r")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
