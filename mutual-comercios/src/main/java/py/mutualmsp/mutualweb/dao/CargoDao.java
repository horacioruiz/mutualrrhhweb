/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Cargo;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class CargoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Cargo> listaCargo() {
        List<Cargo> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Cargo.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Cargo cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Cargo cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Cargo> getListCargo(String valor) {
        List<Cargo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where  id=" + valor + ";", Cargo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Cargo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Cargo> getCargoByDescripcion(String valor) {
        List<Cargo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Cargo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Cargo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Cargo getNinguno() {
        Cargo ninguno = null;
        try {
            ninguno = (Cargo) em.createNativeQuery("SELECT * FROM public.cargo "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Cargo.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Cargo) em.createQuery("select c from Cargo c")
                    .getResultList();
        }
        return ninguno;
    }
}
