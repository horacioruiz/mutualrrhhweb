package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Suspenciones;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SuspencionesDao {

    @PersistenceContext
    private EntityManager em;

    public Suspenciones guardarSuspenciones(Suspenciones usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Suspenciones> listadeSuspenciones(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Suspenciones p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SuspencionesDao.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Suspenciones> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Suspenciones> listadeSuspenciones() {
        try {
            String sql = "select u from Suspenciones u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Suspenciones> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Suspenciones p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, SuspencionesDao.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Suspenciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Suspenciones getByIdUsuario(Long idusuario) {
        return (Suspenciones) em.find(Suspenciones.class, idusuario);
    }

    public List<Suspenciones> listadeSuspencionesByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from Suspenciones p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SuspencionesDao.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Suspenciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Suspenciones cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Suspenciones> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Suspenciones p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, SuspencionesDao.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Suspenciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Suspenciones listarPorIdFuncionario(Long idfuncionario) {
        try {
            return (Suspenciones) em.createQuery("select hf from Suspenciones hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", idfuncionario)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public Suspenciones getById(Long id) {
        Suspenciones marcacion = null;
        try {
            marcacion = (Suspenciones) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", Suspenciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new Suspenciones();
        }
        return marcacion;
    }

    public Suspenciones listarPorIdReloj(Long idReloj) {
        try {
            return (Suspenciones) em.createQuery("select hf from Suspenciones hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<Suspenciones> listarPorFechas(Date fechaDesde, Date fechaHasta, Long idfuncionario) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (idfuncionario != null) {
            filterFuncionario = "AND f.idfuncionario=" + idfuncionario;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.suspenciones p LEFT join funcionario f on p.idfuncionario=f.idfuncionario WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Suspenciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<Suspenciones> listarPorPeriodoFuncionario(Date desde, Date hasta, Long idfuncionario) {
        try {
            String sql = "SELECT * FROM rrhh.suspenciones WHERE (fecha BETWEEN '" + desde + "' AND '" + hasta + "') AND idfuncionario=" + idfuncionario;
            Query query = em.createNativeQuery(sql, Suspenciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
