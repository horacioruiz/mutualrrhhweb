/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Institucion;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class InstitucionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Institucion> listaInstitucion() {
        List<Institucion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Institucion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Institucion institucion){
        try {
            em.merge(institucion);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Institucion institucion) {
        try {
            em.remove(em.contains(institucion) ? institucion : em.merge(institucion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Institucion> getListInstitucion(String valor) {
        List<Institucion> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.institucion " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Institucion.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select i from Institucion i")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Institucion getNinguno() {
        Institucion ninguno = null;
        try {
            ninguno = (Institucion) em.createNativeQuery("SELECT * FROM public.institucion " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Institucion.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Institucion) em.createQuery("select i from Institucion i")
                .getResultList();
        }
        return ninguno;
    }
}
