/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Estadocivil;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class EstadocivilDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<Estadocivil> listaEstadocivil() {
        List<Estadocivil> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Estadocivil.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Estadocivil estadocivil) {
        try {
            em.merge(estadocivil);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(Estadocivil estadocivil) {
        try {
            em.remove(em.contains(estadocivil) ? estadocivil : em.merge(estadocivil));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Estadocivil> getListEstadocivil(String valor) {
        List<Estadocivil> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")){
                lista = em.createNativeQuery("SELECT * FROM public.estadocivil " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Estadocivil.class)
                   .getResultList();
            } else {
                lista = em.createQuery("select e from Estadocivil e")
                   .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    
}
