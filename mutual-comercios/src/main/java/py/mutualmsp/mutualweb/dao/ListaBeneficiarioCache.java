/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import py.mutualmsp.mutualweb.entities.BeneficiarioAdmision;

/**
 *
 * @author Dbarreto
 */

@Singleton
public class ListaBeneficiarioCache {
    
    
    List<BeneficiarioAdmision> lista = new ArrayList();

    public List<BeneficiarioAdmision> getLista() {
        return lista;
    }
    public void setLista(List<BeneficiarioAdmision> lista) {
        this.lista = lista;
    }
    
    public void cargarDetalle(BeneficiarioAdmision beneficiarioAdmision){
        List<BeneficiarioAdmision> listaFinal = new ArrayList();
        try {
            for (BeneficiarioAdmision param : lista) {
                if (param.getId()== beneficiarioAdmision.getId()) {
                    listaFinal.add(beneficiarioAdmision);
                }else{
                    listaFinal.add(param);
                }
            }
            lista = listaFinal;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
