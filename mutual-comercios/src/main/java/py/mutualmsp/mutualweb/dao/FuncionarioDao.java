/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class FuncionarioDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Funcionario> listaFuncionario() {
        List<Funcionario> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Funcionario.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM public.funcionario "
                    + "                   where  idestado=13 ORDER BY nombre", Funcionario.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Funcionario listarFuncionarioPorId(String idfuncionario) {
        Funcionario lista = new Funcionario();
        try {
            //lista = em.createNamedQuery("Funcionario.findAll").getResultList();  
            lista = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario "
                    + " where  idfuncionario=" + idfuncionario + " ORDER BY nombre", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            lista = null;
        }
        return lista;
    }

    public List<Funcionario> listaVerificador() {
        List<Funcionario> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Funcionario.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM public.funcionario "
                    + "                   where  idestado=13 and iddependencia=13;", Funcionario.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Funcionario guardar(Funcionario funcionario) {
        if (funcionario.getIdfuncionario() == null) {
            em.persist(funcionario);
        } else {
            em.merge(funcionario);
        }
        em.flush();
        return funcionario;
    }

    public void borrar(Funcionario funcionario) {
        try {
            em.remove(em.contains(funcionario) ? funcionario : em.merge(funcionario));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Funcionario> getListFuncionario(String value) {
        List<Funcionario> lista = new ArrayList<>();
        try {
            if (value != null && !value.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.funcionario "
                        + "                   where  lower(cedula || nombre || apellido) like lower('%" + value + "%') AND idestado=13;", Funcionario.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select f from Funcionario f WHERE f.idestado=13")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Funcionario getNinguno() {
        Funcionario ninguno = null;
        try {
            ninguno = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario "
                    + " where  lower(nombre) like lower('%NINGUNO%') AND idestado=13;", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Funcionario) em.createQuery("select f from Funcionario f WHERE f.idestado=13")
                    .getResultList();
        }
        return ninguno;
    }

    public Funcionario getFuncionario(String valor) {
        Funcionario funcionario = null;
        try {
            funcionario = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario f "
                    + "left join usuario u ON  f.idfuncionario = u.idfuncionario "
                    + "where  lower(u.usuario) like lower('%" + valor + "%') AND f.idestado=13;", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            funcionario = (Funcionario) em.createQuery("select f from Funcionario f WHERE f.idestado=13")
                    .getResultList();
        }
        return funcionario;
    }

    public Funcionario listarFuncionarioPorCI(String value) {
        Funcionario lista = new Funcionario();
        try {
            lista = (Funcionario) em.createQuery("select c from Funcionario c JOIN FETCH c.cargo car WHERE c.cedula=:ci AND c.idestado=13")
                    .setParameter("ci", value).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public List<Funcionario> ListarPorDependencia(Long iddependencia) {
        List<Funcionario> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE (dep.iddependencia=:idDep OR dep.iddependenciapadre=:idDep) AND c.idestado=13")
                    .setParameter("idDep", iddependencia).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Funcionario listarPorNombreApellido(String nombreApellido) {
        String[] parts = nombreApellido.split(" ");
        Funcionario lista = new Funcionario();
        try {
            List<Funcionario> listaFunc = new ArrayList<>();
            listaFunc = em.createQuery("select func from Funcionario func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[1] + "%")
                    .setParameter("apellido", "%" + parts[0] + "%")
                    .getResultList();
            if (!listaFunc.isEmpty()) {
                lista = listaFunc.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Funcionario listarPorApellidoNombre(String apellidoNombre) {
        String[] parts = apellidoNombre.split(" ");
        Funcionario lista = new Funcionario();
        try {
            lista = (Funcionario) em.createQuery("select func from Funcionario func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[1] + "%")
                    .setParameter("apellido", "%" + parts[0] + "%")
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public List<Funcionario> listarFuncionarioParametro(Dependencia padre, Dependencia hijo, Funcionario funcionario) {
        List<Funcionario> lista = new ArrayList<>();
        if (padre != null && hijo != null && funcionario != null) {
            try {
                lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE dep.iddependencia=:idDep AND dep.iddependenciapadre=:idDepPadre AND upper(c.nombre) =:nombre AND upper(c.apellido) =:apellido  AND c.idestado=13")
                        .setParameter("idDep", hijo.getIddependencia())
                        .setParameter("idDepPadre", padre.getIddependencia())
                        .setParameter("nombre", funcionario.getNombre().toUpperCase())
                        .setParameter("apellido", funcionario.getApellido().toUpperCase())
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("--> " + e.getLocalizedMessage());
                System.out.println("-> " + e.fillInStackTrace());
            }
        } else if (padre != null && hijo != null && funcionario == null) {
            try {
                lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE dep.iddependencia=:idDep AND dep.iddependenciapadre=:idDepPadre  AND c.idestado=13")
                        .setParameter("idDep", hijo.getIddependencia())
                        .setParameter("idDepPadre", padre.getIddependencia())
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("--> " + e.getLocalizedMessage());
                System.out.println("-> " + e.fillInStackTrace());
            }
        } else if (padre != null && hijo == null && funcionario == null) {
            try {
                lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE dep.iddependenciapadre=:idDepPadre  AND c.idestado=13")
                        .setParameter("idDepPadre", padre.getIddependencia())
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("--> " + e.getLocalizedMessage());
                System.out.println("-> " + e.fillInStackTrace());
            }
        } else if (padre == null && funcionario != null) {
            try {
                lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE upper(c.nombre) =:nombre AND upper(c.apellido) =:apellido  AND c.idestado=13")
                        .setParameter("nombre", funcionario.getNombre().toUpperCase())
                        .setParameter("apellido", funcionario.getApellido().toUpperCase())
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("--> " + e.getLocalizedMessage());
                System.out.println("-> " + e.fillInStackTrace());
            }
        } else {
            try {
                lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE c.idestado=13")
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("--> " + e.getLocalizedMessage());
                System.out.println("-> " + e.fillInStackTrace());
            }
        }

        return lista;
    }
}
