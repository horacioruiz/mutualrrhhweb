/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.ReferenciaPersonalAdmision;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class ReferenciaPersonalAdmisionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<ReferenciaPersonalAdmision> listaReferenciaPersonalAdmision() {
        List<ReferenciaPersonalAdmision> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("ReferenciaPersonalAdmision.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(ReferenciaPersonalAdmision referenciaPersonalAdmision){
        try {
            em.merge(referenciaPersonalAdmision);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(ReferenciaPersonalAdmision referenciaPersonalAdmision) {
        try {
            em.remove(em.contains(referenciaPersonalAdmision) ? referenciaPersonalAdmision : em.merge(referenciaPersonalAdmision));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ReferenciaPersonalAdmision> getListReferenciaPersonalAdmision(String valor) {
        List<ReferenciaPersonalAdmision> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.referencia_personal_admision " +
"                   where  id="+valor+";", ReferenciaPersonalAdmision.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select r from ReferenciaPersonalAdmision r")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
