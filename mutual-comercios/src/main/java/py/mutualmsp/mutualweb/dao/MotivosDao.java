package py.mutualmsp.mutualweb.dao;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Motivos;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class MotivosDao {

    @PersistenceContext
    private EntityManager em;

    public Motivos guardarMotivos(Motivos usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Motivos> listadeMotivos(int firstRow, int pageSize, String filter) {

        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from Motivos p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Motivos.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Motivos> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Motivos> listadeMotivos() {
        try {
            String sql = "select u from Motivos u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Motivos> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Motivos p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, Motivos.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Motivos> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Motivos getByIdUsuario(Long idusuario) {
        return (Motivos) em.find(Motivos.class, idusuario);
    }

    public List<Motivos> listadeMotivosByFormulario(int init, int finish, String filter) {
        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from Motivos p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Motivos.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Motivos> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Motivos cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Motivos> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Motivos p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, Motivos.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Motivos> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
