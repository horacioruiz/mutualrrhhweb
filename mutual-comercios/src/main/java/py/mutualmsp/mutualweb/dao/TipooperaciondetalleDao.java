/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipooperaciondetalle;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipooperaciondetalleDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Tipooperaciondetalle> listaTipooperaciondetalle() {
        List<Tipooperaciondetalle> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Tipooperaciondetalle.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Tipooperaciondetalle tipooperaciondetalle){
        try {
            em.merge(tipooperaciondetalle);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Tipooperaciondetalle tipooperaciondetalle) {
        try {
            em.remove(em.contains(tipooperaciondetalle) ? tipooperaciondetalle : em.merge(tipooperaciondetalle));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tipooperaciondetalle> getListTipooperaciondetalle(String valor) {
        List<Tipooperaciondetalle> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.tipooperaciondetalle " +
"                   where  idtipooperacion="+valor+";", Tipooperaciondetalle.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select tod from Tipooperaciondetalle tod")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Tipooperaciondetalle> getListTipooperaciondetalleExistente(Long idtipooperacion, Long idmodalidad) {
        List<Tipooperaciondetalle> lista = new ArrayList<>();
       try {
           System.out.println("SELECT * FROM public.tipooperaciondetalle " +
            " where  idtipooperacion="+idtipooperacion+" and idmodalidad="+idmodalidad+";");
           lista = em.createNativeQuery("SELECT * FROM public.tipooperaciondetalle " +
            " where  idtipooperacion="+idtipooperacion+" and idmodalidad="+idmodalidad+";", Tipooperaciondetalle.class)
                   .getResultList();
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Tipooperaciondetalle> getTipooperaciondetalleByTipoOperacion(Long id){
        List<Tipooperaciondetalle> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select tod from Tipooperaciondetalle tod where tod.idtipooperacion.idtipooperacion = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }
}
