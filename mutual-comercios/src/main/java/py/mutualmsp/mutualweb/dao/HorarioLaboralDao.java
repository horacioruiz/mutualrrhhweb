/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class HorarioLaboralDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<HorarioLaboral> listaHorarioLaboral() {
        List<HorarioLaboral> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("HorarioLaboral.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(HorarioLaboral cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(HorarioLaboral cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<HorarioLaboral> getListHorarioLaboral(String valor) {
        List<HorarioLaboral> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.horario_laboral "
                        + "                   where  id=" + valor + ";", HorarioLaboral.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from HorarioLaboral c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<HorarioLaboral> getListHorarioLaboralByEstado(Boolean valor) {
        List<HorarioLaboral> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.horario_laboral "
                        + "                   where  estado=" + valor + " ORDER BY id DESC;", HorarioLaboral.class)
                        .getResultList();
            } else {
                lista = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public HorarioLaboral getNinguno() {
        HorarioLaboral ninguno = null;
        try {
            ninguno = (HorarioLaboral) em.createNativeQuery("SELECT * FROM rrhh.horario_laboral "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", HorarioLaboral.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (HorarioLaboral) em.createQuery("select c from HorarioLaboral c")
                    .getResultList();
        }
        return ninguno;
    }

    public HorarioLaboral getByValue(String sabado) {
        HorarioLaboral ninguno = null;
        try {
            ninguno = (HorarioLaboral) em.createNativeQuery("SELECT * FROM rrhh.horario_laboral "
                    + "               where lower(descripcion) like lower('" + sabado + "%');", HorarioLaboral.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = null;
        }
        return ninguno;
    }
}
