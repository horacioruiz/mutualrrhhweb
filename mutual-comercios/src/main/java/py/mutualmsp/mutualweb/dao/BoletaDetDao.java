/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.BoletaDet;
import py.mutualmsp.mutualweb.entities.Pais;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class BoletaDetDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<BoletaDet> listaPaises() {
        List<BoletaDet> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("BoletaDet.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(BoletaDet pais) {
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BoletaDet guardarRetornarData(BoletaDet pais) {
        try {
            em.merge(pais);
            em.refresh(pais);
            return pais;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void borrar(BoletaDet pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<BoletaDet> getListBoletaDet(String valor) {
        List<BoletaDet> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM comercios.boleta_cab "
                        + "                   where  id=" + valor + ";", Pais.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select p from BoletaDet p")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<BoletaDet> listarPorIdCab(String valor) {
        List<BoletaDet> lista = new ArrayList<>();
        try {
//            if (valor != null && !valor.equals("")) {
            lista = em.createNativeQuery("SELECT * FROM comercios.boleta_det "
                    + " WHERE idboletacab=" + valor + ";", BoletaDet.class)
                    .getResultList();
//            } else {
//                lista = em.createQuery("select p from BoletaDet p")
//                        .getResultList();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
