/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.ConciliacionDet;
import py.mutualmsp.mutualweb.entities.ConciliacionDet;
import py.mutualmsp.mutualweb.entities.Pais;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class ConciliacionDetDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<ConciliacionDet> listaPaises() {
        List<ConciliacionDet> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("ConciliacionDet.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(ConciliacionDet pais) {
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConciliacionDet guardarRetornarData(ConciliacionDet pais) {
        try {
            em.merge(pais);
            em.refresh(pais);
            return pais;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void borrar(ConciliacionDet pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ConciliacionDet> getListConciliacionDet(String valor) {
        List<ConciliacionDet> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM comercios.conciliacion_cab "
                        + "                   where  id=" + valor + ";", Pais.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select p from ConciliacionDet p")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<ConciliacionDet> listarPorIdCab(String valor) {
        List<ConciliacionDet> lista = new ArrayList<>();
        try {
//            if (valor != null && !valor.equals("")) {
            lista = em.createNativeQuery("SELECT * FROM comercios.conciliacion_det "
                    + " WHERE idboletacab=" + valor + ";", ConciliacionDet.class)
                    .getResultList();
//            } else {
//                lista = em.createQuery("select p from ConciliacionDet p")
//                        .getResultList();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
