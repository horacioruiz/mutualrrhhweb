/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.ControlCab;
import py.mutualmsp.mutualweb.entities.Entidad;
import py.mutualmsp.mutualweb.entities.Pais;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class ControlCabDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<ControlCab> listaPaises() {
        List<ControlCab> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("ControlCab.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(ControlCab pais) {
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ControlCab guardarRetornarData(ControlCab pais) {
        try {
            em.merge(pais);
            em.flush();
            return pais;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void borrar(ControlCab pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ControlCab> getListControlCab(String valor) {
        List<ControlCab> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM comercios.control_cab "
                        + "                   where  id=" + valor + ";", Pais.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select p from ControlCab p")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public long getLastId() {
        Long idData = 0l;
        try {
            ControlCab bc = (ControlCab) em.createNativeQuery("SELECT * FROM comercios.control_cab ORDER BY id desc limit 1", ControlCab.class)
                    .getSingleResult();
            idData = bc.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idData;
    }

    public List<ControlCab> buscarFiltros(Entidad entidad, String valor1, String valor2) {
        List<ControlCab> lista = new ArrayList<>();
        String entidadi = "";
        String valoruno = "";
        if (entidad != null) {
            entidadi = " AND UPPER(comercio)  LIKE '" + entidad.getDescripcion().toUpperCase() + "%'";
        }
        if (valor1 != null && valor2.equalsIgnoreCase("")) {
            valoruno = " AND UPPER(periodo)  LIKE '" + valor1.toUpperCase() + "/" + valor2.toUpperCase() + "%'";
        } else if (valor1 != null) {
            valoruno = " AND UPPER(periodo)  LIKE '%" + valor1.toUpperCase() + "%'";
        } else if (!valor2.equalsIgnoreCase("")) {
            valoruno = " AND UPPER(periodo)  LIKE '%" + valor2.toUpperCase() + "%'";
        }

        try {
            String sql = "SELECT * FROM comercios.boleta_cab "
                    + " where" + entidadi + valoruno + ";";
            sql = sql.replace("where AND", "where");

            lista = em.createNativeQuery(sql, ControlCab.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public ControlCab listarPorId(Long id) {
        ControlCab boleta = new ControlCab();
        try {
            ControlCab bc = (ControlCab) em.createNativeQuery("SELECT * FROM comercios.boleta_cab WHERE id=" + id, ControlCab.class)
                    .getSingleResult();
            boleta = bc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return boleta;
    }

}
