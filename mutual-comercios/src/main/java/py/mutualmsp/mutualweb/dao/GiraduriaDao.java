/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Giraduria;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class GiraduriaDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Giraduria> listaGiraduria() {
        List<Giraduria> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Giraduria.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Giraduria giraduria){
        try {
            em.merge(giraduria);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Giraduria giraduria) {
        try {
            em.remove(em.contains(giraduria) ? giraduria : em.merge(giraduria));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Giraduria> getListGiraduria(String valor) {
        List<Giraduria> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.giraduria " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Giraduria.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select g from Giraduria g")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }

    
    public Giraduria getNinguno() {
        Giraduria ninguno = null;
        try {
            ninguno = (Giraduria) em.createNativeQuery("SELECT * FROM public.giraduria " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Giraduria.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Giraduria) em.createQuery("select g from Giraduria g")
                .getResultList();
        }
        return ninguno;
    }
}
