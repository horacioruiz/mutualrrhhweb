/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Movimientodescuento;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class MovimientodescuentoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Movimientodescuento> listaMovimientodescuento() {
        List<Movimientodescuento> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Movimientodescuento.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Movimientodescuento movimientodescuento){
        try {
            em.merge(movimientodescuento);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Movimientodescuento movimientodescuento) {
        try {
            em.remove(em.contains(movimientodescuento) ? movimientodescuento : em.merge(movimientodescuento));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Movimientodescuento> getListAdjuntosAdmision(String valor) {
        List<Movimientodescuento> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.movimientodescuento " +
"                   where  id="+valor+";", Movimientodescuento.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select m from Movimientodescuento m")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
