package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSaturday;

/**
 * Created by Alfre on 28/6/2016.
 */
public class NuevaMarcacionForm extends Window {

    private TextField txtCedulaFuncionario = new TextField("Ingrese cédula (*)");
    private TextField txtNombreFuncionario = new TextField("Funcionario");
    private TextField txtArea = new TextField("Area");
    private TextField txtCargo = new TextField("Cargo");
    private TextArea txtObservacion = new TextArea("Observación");
    Grid<LicenciasCompensar> gridLicenciaCompensar = new Grid<>(LicenciasCompensar.class);
    private TextField txtCanhs = new TextField("Cantidad hs");
    private TextField txtCantDiaForm = new TextField("Cant dia");
    private DateField fechaInicio = new DateField("Fecha Inicio (*)");
    private DateField fechaForm = new DateField("Fecha (*)");
    private DateField fechaAsignada = new DateField("Fecha asignada");
    private DateField fechaFin = new DateField("Fecha Fin (*)");

    DateTimeField fechaHoraForm = new DateTimeField("Fecha/Hora entrada/salida");
    private ComboBox<String> prioridad = new ComboBox<>("Prioridad");
    private int editar = 0;

    private ComboBox<Dependencia> cbDpto = new ComboBox<>("Departamento (*)");
    private ComboBox<Dependencia> cbCargo = new ComboBox<>("Sección (*)");

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private TextField costo = new TextField("Costo");
    private TextField horas = new TextField("Horas Trabajadas");
    private TextField tiempoRespuesta = new TextField("Tiempo de Respuesta");
    private TextField txtDependencia = new TextField("Dependencia");
    private ComboBox<Parametro> formulario = new ComboBox<>("Parametro (*)");
    LicenciasCompensar licCompensarSeleccionado = new LicenciasCompensar();
    List<LicenciasCompensar> listProduccionDetalle = new ArrayList<>();

    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    UsuarioDao usuDao = ResourceLocator.locate(UsuarioDao.class);

    //Grid<LicenciasProduccionDetalle> gridLicenciaCompensar = new Grid<>(LicenciasProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    ComboBox<Funcionario> cbFuncionario = new ComboBox<>("Funcionario (*)");
    TextField txtCargoForm = new TextField("Cargo");
    DateTimeField fechaHoraFinalForm = new DateTimeField("Fecha/Hora final (*)");
    DateTimeField txtCantMinCompensar = new DateTimeField("Fecha/Hora final (*)");
    private Button btnAgregar = new Button("Agregar");

    private ComboBox<Motivos> motivo = new ComboBox<>("Motivos (*)");
    private ComboBox<Funcionario> encargado = new ComboBox<>("Encargado (*)");
//    private ComboBox<Funcionario> rrhh = new ComboBox<>("Funcionario RRHH");
//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    private TextArea txtOtroMotivo = new TextArea("Observación");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<Marcaciones> saveListener;
    private Consumer<Marcaciones> deleteListener;
    private Consumer<Marcaciones> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
//    LicenciasProduccionDetalle spd = new LicenciasProduccionDetalle();
    int numRowSelected = 0;
    LicenciasCompensar spd = new LicenciasCompensar();

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    MarcacionesDao marcacionesDao = ResourceLocator.locate(MarcacionesDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    ParametroDao formularioDao = ResourceLocator.locate(ParametroDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    //DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    LicenciasDao solicitudDao = ResourceLocator.locate(LicenciasDao.class);
    LicenciasCompensarDao licenciaCompensarDao = ResourceLocator.locate(LicenciasCompensarDao.class);
    // LicenciasProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(LicenciasProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private Licencias solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();
//    InlineDateTimeField sample = new InlineDateTimeField();
    DateTimeField fechaHoraInicio = new DateTimeField("Fecha/hora Inicio (*)");
    DateTimeField fechaHoraFin = new DateTimeField("Fecha/hora Fin (*)");
    DateTimeField fechaHoraInicioForm = new DateTimeField("Fecha/hora Inicio (*)");
    DateTimeField fechaHoraFinForm = new DateTimeField("Fecha/hora Fin (*)");
    private TextField txtTareaRealizar = new TextField("Tarea a realizar (*)");
    private TextField txtCanthsForm = new TextField("Cant hs (*)");
    Map<String, LicenciasCompensar> mapLicenciaCompensar = new HashMap<>();

    //List<LicenciasProduccionDetalle> listProduccionDetalle = new ArrayList<>();
    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload

    public NuevaMarcacionForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Marcación");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            txtCanthsForm.setEnabled(false);

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHoraini());
            }).setCaption("Desde hs");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHorafin());
            }).setCaption("Hasta hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getCantHsmin();
            }).setCaption("Cant hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Tarea a realizar");

//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreFuncionario.setEnabled(false);
            txtArea.setEnabled(false);
            txtCanhs.setEnabled(false);
            txtCargo.setEnabled(false);
            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);
            txtCargoForm.setEnabled(false);

            mainLayout.setVisible(false);

            txtCedulaFuncionario.addBlurListener(e -> {
                validate();
            });

            formulario.setItems(formularioDao.listarPorTipoCodigo("licencias"));
            formulario.setItemCaptionGenerator(Parametro::getDescripcion);
            cargarPorFormulario(formularioDao.listarPorTipoCodigo("licencias").get(0));

//            rrhh.setItems(funcionarioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setItems(funcionarioDao.listaFuncionario());
            encargado.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setVisible(false);

            txtCedulaFuncionario.focus();

            txtCedulaFuncionario.addValueChangeListener(e -> updateList(e.getValue()));
            formulario.addValueChangeListener(e -> cargarPorFormulario(e.getValue()));
            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));

            gridLicenciaCompensar.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (numRowSelected >= 0) {
                        listProduccionDetalle.remove(numRowSelected);
                        gridLicenciaCompensar.clearSortOrder();
                        gridLicenciaCompensar.setItems(listProduccionDetalle);
                        mapLicenciaCompensar = new HashMap<>();
                        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                        listProduccionDetalle.forEach((component) -> {
                            mapLicenciaCompensar.put(formatFecs.format(component.getFechacompensar()), component);
                        });
                        numRowSelected = -1;
                        spd = new LicenciasCompensar();
                    }
                }
            });

            gridLicenciaCompensar.addItemClickListener(e -> {
                if (e.getItem() != null) {
                    spd = e.getItem();
                    numRowSelected = e.getRowIndex();
                }
            });

            txtCedulaFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                        try {
                            calcularCantDiaFuncionario();
                            if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                                List<Rotaciones> listRotacion = rotacionDao.listarPorIdFuncionarioFecha(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
                                if (listRotacion.size() > 0) {
                                    fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
                                }
                            }
                        } catch (Exception e) {
                        } finally {
                        }

//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }

                private void calcularCantDiaFuncionario() {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCanhs.setValue(dif + "");
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            fechaHoraInicio.addValueChangeListener(e -> updateFechaHora());
            fechaHoraFin.addValueChangeListener(e -> updateFechaHora());
            fechaHoraInicioForm.addValueChangeListener(e -> updateFechaHoraForm());
            fechaHoraFinForm.addValueChangeListener(e -> updateFechaHoraForm());
            fechaInicio.addValueChangeListener(e -> updateFecha());
            fechaFin.addValueChangeListener(e -> updateFecha());
            cbFuncionario.addValueChangeListener(e -> updateFormulario());

            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                long min = 0l;
                try {
                    int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
                    int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
                    String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
                    String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));
                    if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                        min = (minFin - minInicio);
                    }
                } catch (Exception e) {
                } finally {
                }
                try {
                    if (min > 0) {
                        if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                            if (txtCanhs.getValue().trim().equalsIgnoreCase("0")) {
                                Notification.show("La cantidad hs compensada debe ser mayor a cero.",
                                        Notification.Type.ERROR_MESSAGE);
                            } else {
                                int minDif = 0;
                                for (LicenciasCompensar licenciasCompensar : listProduccionDetalle) {
                                    int minFin = ((licenciasCompensar.getHorafin().getHours() * 60) + licenciasCompensar.getHorafin().getMinutes());
                                    int minInicio = ((licenciasCompensar.getHoraini().getHours() * 60) + licenciasCompensar.getHoraini().getMinutes());
                                    if (minFin > minInicio) {
                                        minDif += (minFin - minInicio);
                                    }
                                }
                                int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
                                int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
                                int minDifMax = minFin - minInicio;
                                if (minDifMax > minDif && minDifMax >= (minDif + min)) {
                                    cargarGrilla();
                                } else {
                                    Notification.show("Las horas asignada al detalle no debe superar el maximo requerido.",
                                            Notification.Type.ERROR_MESSAGE);
                                }
                            }
                        } else {
                            if (txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE")) {
                                cargarGrillaSinCompensar();
                            } else if (Long.parseLong(txtCantDiaForm.getValue().trim()) == listProduccionDetalle.size()) {
                                Notification.show("Ya completo el detalle maximo de dias requerido.",
                                        Notification.Type.ERROR_MESSAGE);
                            } else {
                                cargarGrillaSinCompensar();
                            }
                        }
                    } else {
                        if (!formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                            if (txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE")) {
                                cargarGrillaSinCompensar();
                            } else if (Long.parseLong(txtCantDiaForm.getValue().trim()) == listProduccionDetalle.size()) {
                                Notification.show("Ya completo el detalle maximo de dias requerido.",
                                        Notification.Type.ERROR_MESSAGE);
                            } else {
                                cargarGrillaSinCompensar();
                            }
                        } else {
                            Notification.show("Las fechas deben ser iguales y la hora fin debe ser mayor a la hora inicio.",
                                    Notification.Type.ERROR_MESSAGE);
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                    System.out.println(e.fillInStackTrace());
                }
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Datos")) {
                        vl.addComponent(createForm());
                    } else {
//                        vl.addComponent(createFormMotivoSolicitud());
                    }

                    tab.addComponent(vl);
                }

                private VerticalLayout createForm() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtCedulaFuncionario);
                    horizontal.addComponent(txtNombreFuncionario);
                    horizontal.addComponent(txtArea);
                    horizontal.addComponent(txtCargo);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(fechaHoraForm);
//                    fechaHoraForm.setWidth("100%");

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createFormMotivoSolicitud() {
                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    //horizontal.addComponent(motivo);

//        horizontal.addComponent(rrhh);
                    horizontal.addComponent(encargado);
                    //horizontal.addComponent(upload);
                    //horizontal.addComponent(labelUrl);

                    image.setVisible(false);
                    upload.setButtonCaption("Seleccionar");
                    upload.addSucceededListener(receiver);

                    // Prevent too big downloads 0981752315
                    final long UPLOAD_LIMIT = 1000000l;
                    upload.addStartedListener(new Upload.StartedListener() {
                        private static final long serialVersionUID = 4728847902678459488L;

                        @Override
                        public void uploadStarted(Upload.StartedEvent event) {
                            if (event.getContentLength() > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });

                    // Check the size also during progress
                    upload.addProgressListener(new Upload.ProgressListener() {
                        private static final long serialVersionUID = 8587352676703174995L;

                        @Override
                        public void updateProgress(long readBytes, long contentLength) {
                            if (readBytes > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });
                    // Create uploads directory
                    File uploads = new File(Constants.UPLOAD_DIR_TEMP);
                    if (!uploads.exists() && !uploads.mkdir()) {
                        horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
                    }

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(txtOtroMotivo);
                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtDependencia);
//        horizontalSegundo.addComponent(txtOtroMotivo);
                    txtOtroMotivo.setWidth("100%");

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");
                    txtDependencia.setWidth("100%");

                    horizontalTercero.setWidth("100%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);

                    form.addComponent(fechaAsignada);
                    form.addComponent(fechaForm);
                    form.addComponent(txtCantDiaForm);
                    form.addComponent(fechaHoraInicioForm);
                    form.addComponent(fechaHoraFinForm);
                    form.addComponent(txtTareaRealizar);
                    form.addComponent(txtCanthsForm);
                    form.addComponent(txtTareaRealizar);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(gridLicenciaCompensar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);

//        FormLayout columnLayout = new FormLayout();
//// Setting the desired responsive steps for the columns in the layout
//columnLayout.setResponsiveSteps(
//           new ResponsiveStep("25em", 1),
//           new ResponsiveStep("32em", 2),
//           new ResponsiveStep("40em", 3));
//   TextField firstName = new TextField();
//   firstName.setPlaceholder("First Name");
//   TextField lastName = new TextField();
//   lastName.setPlaceholder("Last Name");
//   TextField email = new TextField();
//   email.setPlaceholder("Email");
//   TextField nickname = new TextField();
//   nickname.setPlaceholder("Username");
//   TextField website = new TextField();
//   website.setPlaceholder("Link to personal website");
//   TextField description = new TextField();
//   description.setPlaceholder("Enter a short description about yourself");
//   columnLayout.add(firstName, lastName,  nickname, email, website); 
//   // You can set the desired column span for the components individually.
//   columnLayout.setColspan(website, 2);
//   // Or just set it as you add them.
//   columnLayout.add(description, 3);
                    return layout;
                }
            });

// Have some tabs
            String[] tabs = {"Datos"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void editarSolicitudProduccion(Licencias solicitud) {
        List<LicenciasCompensar> listSolicitudDetalle = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        try {
            txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getNombrefuncionario().toUpperCase());
        } catch (Exception e) {
            txtNombreFuncionario.setValue("");
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
        } catch (Exception e) {
            txtArea.setValue("");
        } finally {
        }
        try {
            formulario.setValue(solicitud.getParametro());
            cargarPorFormulario(solicitud.getParametro());
        } catch (Exception e) {
            formulario.setValue(null);
        } finally {
        }
        /*try {
            if (solicitud.getCantdia() == null) {
                txtCantdias.setValue("0");
            } else {
                txtCantdias.setValue(solicitud.getCantdia() + "");
            }
        } catch (Exception e) {
            txtCantdias.setValue("0");
        } finally {
        }*/
        try {
            txtObservacion.setValue(solicitud.getMotivo());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }
        try {
            encargado.setValue(solicitud.getEncargado());
        } catch (Exception e) {
            encargado.setValue(null);
        } finally {
        }
        labelUrl.setValue("");
        motivo.setValue(null);
        txtDependencia.setVisible(false);
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);
        fechaHoraInicio.setVisible(false);

        fechaInicio.setValue(null);
        fechaFin.setValue(null);
        fechaHoraInicio.setValue(null);
        fechaHoraFin.setValue(null);

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);

        fechaHoraInicio.setVisible(false);
        fechaHoraFin.setVisible(false);
        txtDependencia.setVisible(false);

        fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
        upload.setVisible(false);
        txtOtroMotivo.setVisible(false);

        txtCedulaFuncionario.setVisible(false);
        txtNombreFuncionario.setVisible(false);
        txtArea.setVisible(false);
        txtCargo.setVisible(false);
        txtObservacion.setVisible(false);
        motivo.setVisible(false);
        gridLicenciaCompensar.clearSortOrder();
        mainLayout.setVisible(true);

        cbCargo.setItems(new ArrayList<>());
        cbCargo.setVisible(true);

        //cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        //cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));

        mainLayout.setVisible(true);
        cargarPorFormulario(formulario.getValue());
        cargarDetalleProduccion(solicitud);
        try {
            String fecha = formatFec.format(solicitud.getFechafin());
            String hora = formatHor.format(solicitud.getHorafin());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraFin.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraFin.setValue(null);
        } finally {
        }
    }

    private void cargarDetalleProduccion(Licencias solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<LicenciasCompensar> spd = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        listProduccionDetalle = spd;
        mapLicenciaCompensar = new HashMap<>();
        listProduccionDetalle.forEach((component) -> {
            mapLicenciaCompensar.put(formatFec.format(component.getFechacompensar()), component);
        });
        gridLicenciaCompensar.clearSortOrder();
        gridLicenciaCompensar.setItems(listProduccionDetalle);
    }

    public void editarRegistro(Licencias solicitud) {
        setCaption("Editar Solicitud");
        this.solicitud = solicitud;
        editar = 1;
        editarSolicitudProduccion(solicitud);
        /* if (solicitud.getParametro().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            editar = 1;
            editarSolicitudProduccion(solicitud);
        } else {
            List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
            try {
                txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
            } catch (Exception e) {
                txtCedulaFuncionario.setValue(null);
            } finally {
            }
            try {
                txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
            } catch (Exception e) {
                txtDependencia.setValue("");
            } finally {
            }
            try {
                txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
            } catch (Exception e) {
                txtNombreFuncionario.setValue(null);
            } finally {
            }
            try {
                txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
            } catch (Exception e) {
                txtCargo.setValue(null);
            } finally {
            }
            try {
                txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
            } catch (Exception e) {
                txtArea.setValue(null);
            } finally {
            }
            try {
                formulario.setValue(solicitud.getFormulario());
                cargarPorFormulario(solicitud.getFormulario());
            } catch (Exception e) {
                formulario.setValue(null);
            } finally {
            }
            try {
                if (solicitud.getCantdia() == null) {
                    txtCantdias.setValue("0");
                } else {
                    txtCantdias.setValue(solicitud.getCantdia() + "");
                }
            } catch (Exception e) {
                txtCantdias.setValue("0");
            } finally {
            }
            try {
                txtObservacion.setValue(solicitud.getObservacion());
            } catch (Exception e) {
                txtObservacion.setValue(null);
            } finally {
            }
            try {
                encargado.setValue(solicitud.getEncargado());
            } catch (Exception e) {
                encargado.setValue(null);
            } finally {
            }
            try {
                txtOtroMotivo.setValue(listSolicitudDetalle.get(0).getDescripcion());
            } catch (Exception e) {
                txtOtroMotivo.setValue("");
            } finally {
            }
            try {
                labelUrl.setValue(listSolicitudDetalle.get(0).getUrlimagen());
            } catch (Exception e) {
                labelUrl.setValue("");
            } finally {
            }
            try {
                motivo.setValue(listSolicitudDetalle.get(0).getMotivo());
            } catch (Exception e) {
                motivo.setValue(null);
            } finally {
            }
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                txtDependencia.setVisible(true);
            } else {
                txtDependencia.setVisible(false);
            }
            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                fechaHoraInicio.setVisible(true);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);

                mainLayout.setVisible(false);
                try {
                    String fecha = formatFec.format(solicitud.getFechaini());
                    String hora = formatHor.format(solicitud.getHoraini());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtInicio = LocalDateTime.parse(fec);
                    fechaHoraInicio.setValue(ldtInicio);
                } catch (Exception e) {
                    fechaHoraInicio.setValue(null);
                } finally {
                }
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);
                txtDependencia.setVisible(false);
                fechaHoraInicio.setValue(null);

                mainLayout.setVisible(true);
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(false);
                fechaInicio.setVisible(true);
                fechaFin.setVisible(true);

                mainLayout.setVisible(false);
                try {
                    fechaInicio.setValue(DateUtils.asLocalDate(solicitud.getFechaini()));
                } catch (Exception e) {
                    fechaInicio.setValue(null);
                } finally {
                }
                try {
                    fechaFin.setValue(DateUtils.asLocalDate(solicitud.getFechafin()));
                } catch (Exception e) {
                    fechaFin.setValue(null);
                } finally {
                }
            }
        }*/
    }

    private boolean validate() {
        boolean savedEnabled = true;
        try {
            if (txtCedulaFuncionario == null || txtCedulaFuncionario.isEmpty()) {
                savedEnabled = false;
            }
            if (txtNombreFuncionario == null || txtNombreFuncionario.isEmpty()) {
                savedEnabled = false;
            }
        } catch (Exception e) {
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    private void updateParametro() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void cargarGrilla() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        if (mapLicenciaCompensar.containsKey(formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue())))) {
            Notification.show("Ya se ha agregado al detalle la fecha seleccionada.",
                    Notification.Type.ERROR_MESSAGE);
        } else {
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));

            long min = 0l;
            try {
                int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
                int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
                if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                    min = (minFin - minInicio);
                }
            } catch (Exception e) {
            } finally {
            }
            if (min == 0) {
                Notification.show("Las fechas deben ser iguales y la hora fin debe ser mayor a la hora inicio.",
                        Notification.Type.ERROR_MESSAGE);
            } else if (validateForm() && inic.equalsIgnoreCase(finc)) {
                LicenciasCompensar spd = new LicenciasCompensar();
                spd.setFechacompensar(DateUtils.asDate(fechaHoraFinForm.getValue()));
                try {
                    spd.setHoraini(Timestamp.valueOf(fechaHoraInicioForm.getValue()));
                } catch (Exception e) {
                    spd.setHoraini(null);
                } finally {
                }
                try {
                    spd.setHorafin(Timestamp.valueOf(fechaHoraFinForm.getValue()));
                } catch (Exception e) {
                    spd.setHorafin(null);
                } finally {
                }

                spd.setCantidad(min);
                spd.setObservacion(txtTareaRealizar.getValue());
                spd.setLicencia(null);

                mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
                listProduccionDetalle.add(spd);
                gridLicenciaCompensar.clearSortOrder();
                gridLicenciaCompensar.setItems(listProduccionDetalle);
                SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                try {
                    String fecha = formatFec.format(new Date());
                    String fec = fecha + "T16:00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraInicioForm.setValue(ldtFin);
                    fechaHoraFinForm.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraInicioForm.setValue(null);
                    fechaHoraFinForm.setValue(null);
                } finally {
                }
                txtTareaRealizar.setValue("");
//            gridFuncionario.addComponentColumn(this::buildConfirmButton).setCaption(" X ");
            } else {
                Notification.show("Todos los campos son obligatorios",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void cargarGrillaSinCompensar() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        boolean val = false;
        if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaAsignada.getValue()), DateUtils.asDate(fechaAsignada.getValue()), func.getIdfuncionario());
            if (listRotacion.size() > 0) {
                fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
                val = true;
            } else {
                Notification.show("No existen rotaciones disponibles para cambiarlas",
                        Notification.Type.ERROR_MESSAGE);
            }
            if (isWeekendSaturday(fechaForm.getValue())) {
                val = true;
            } else {
                Notification.show("La fecha debe ser de un dia sabado para cambiarla",
                        Notification.Type.ERROR_MESSAGE);
                val = false;
            }
        } else {
            val = true;
        }

        if (mapLicenciaCompensar.containsKey(formatFecs.format(DateUtils.asDate(fechaForm.getValue())))) {
            Notification.show("Ya se ha agregado al detalle la fecha seleccionada.",
                    Notification.Type.ERROR_MESSAGE);
        } else {
            if (val) {
                if (validateFormSinCompensar()) {
                    LicenciasCompensar spd = new LicenciasCompensar();
                    spd.setFechacompensar(DateUtils.asDate(fechaForm.getValue()));
                    spd.setObservacion(formulario.getValue().getDescripcion().toUpperCase());
                    listProduccionDetalle.add(spd);
                    gridLicenciaCompensar.clearSortOrder();
                    gridLicenciaCompensar.setItems(listProduccionDetalle);
                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                    try {
                        String fecha = formatFec.format(new Date());
                        String fec = fecha + "T16:00";
                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                        fechaHoraInicioForm.setValue(ldtFin);
                        fechaHoraFinForm.setValue(ldtFin);
                    } catch (Exception e) {
                        fechaHoraInicioForm.setValue(null);
                        fechaHoraFinForm.setValue(null);
                    } finally {
                    }
                    txtTareaRealizar.setValue("");
                    mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
//            gridFuncionario.addComponentColumn(this::buildConfirmButton).setCaption(" X ");
                } else {
                    Notification.show("Todos los campos son obligatorios",
                            Notification.Type.ERROR_MESSAGE);
                }
            }
        }
    }

    private boolean validateForm() {
        boolean savedEnabled = true;
        if (fechaHoraFinForm == null || fechaHoraFinForm.isEmpty()) {
            savedEnabled = false;
        }
        if (fechaHoraInicioForm == null || fechaHoraInicioForm.getValue() == null) {
            savedEnabled = false;
        }
        if (txtCanthsForm == null || txtCanthsForm.getValue() == null) {
            savedEnabled = false;
        }
        if (txtTareaRealizar == null || txtTareaRealizar.getValue() == null || txtTareaRealizar.getValue().equals("")) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private boolean validateFormSinCompensar() {
        boolean savedEnabled = true;
        if (fechaForm == null || fechaForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtCantDiaForm == null || txtCantDiaForm.getValue() == null) {
            savedEnabled = false;
        }

        return savedEnabled;
    }

    public void setSaveListener(Consumer<Marcaciones> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Marcaciones> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Marcaciones> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void nuevoRegistro() {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        try {
            String fecha = formatFec.format(new Date());
            String fec = fecha + "T07:20";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraForm.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraForm.setValue(null);
        } finally {
        }
    }

    private void guardarDetalleProduccion(Licencias solicitud) {
        if (editar == 0) {
            if (listProduccionDetalle.size() > 0) {
                for (LicenciasCompensar solicitudProduccionDetalle : listProduccionDetalle) {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                }
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            if (listProduccionDetalle.size() > 0) {
                licenciaCompensarDao.listarPorIdLicencia(solicitud.getId()).forEach((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.borrar(solicitudProduccionDetalle);
                });
                listProduccionDetalle.stream().map((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    return solicitudProduccionDetalle;
                }).forEachOrdered((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                });
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void cargarSeccion(Dependencia dependencia) {
        if (dependencia != null && cbDpto.getValue() != null) {
            cbCargo.clear();
            cbCargo.setItems(new ArrayList<>());
//            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbCargo.setVisible(true);

            //Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
            // List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            //listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            // //for (Dependencia dependencia1 : listDependencia) {
            //   listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            //}
            //cbFuncionario.setItems(listFunc);
            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        }
    }

    private void editarLicenciasProduccion(Licencias solicitud) {
        /*List<LicenciasProduccionDetalle> listLicenciasDetalle = solicitudProduccionDetalleDao.listarPorIdLicencias(solicitud.getId());
        try {
            txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
        } catch (Exception e) {
            txtDependencia.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
        } catch (Exception e) {
            txtNombreFuncionario.setValue("");
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
        } catch (Exception e) {
            txtArea.setValue("");
        } finally {
        }
        try {
            formulario.setValue(solicitud.getParametro());
            cargarPorParametro(solicitud.getParametro());
        } catch (Exception e) {
            formulario.setValue(null);
        } finally {
        }
        try {
            if (solicitud.getCantdia() == null) {
                txtCanhs.setValue("0");
            } else {
                txtCanhs.setValue(solicitud.getCantdia() + "");
            }
        } catch (Exception e) {
            txtCanhs.setValue("0");
        } finally {
        }
        try {
            txtObservacion.setValue(solicitud.getObservacion());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }
        try {
            encargado.setValue(solicitud.getEncargado());
        } catch (Exception e) {
            encargado.setValue(null);
        } finally {
        }
        try {
            txtOtroMotivo.setValue(listLicenciasDetalle.get(0).getDescripcion());
        } catch (Exception e) {
            txtOtroMotivo.setValue("");
        } finally {
        }
        labelUrl.setValue("");
        motivo.setValue(null);
        txtDependencia.setVisible(false);
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);
        fechaHoraInicio.setVisible(false);

        fechaInicio.setValue(null);
        fechaFin.setValue(null);
        fechaHoraInicio.setValue(null);
        fechaHoraFin.setValue(null);

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);

        fechaHoraInicio.setVisible(false);
        fechaHoraFin.setVisible(false);
        txtDependencia.setVisible(false);

        fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
        upload.setVisible(false);
        txtOtroMotivo.setVisible(false);

        txtCedulaFuncionario.setVisible(false);
        txtNombreFuncionario.setVisible(false);
        txtArea.setVisible(false);
        txtCargo.setVisible(false);
        txtObservacion.setVisible(false);
        motivo.setVisible(false);
        gridLicenciaCompensar.clearSortOrder();
        mainLayout.setVisible(true);
        txtCanhs.setVisible(false);

        cbCargo.setItems(new ArrayList<>());
        cbCargo.setVisible(true);

        cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);
        cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));

        mainLayout.setVisible(true);
        cargarDetalleProduccion(solicitud);
        try {
            String fecha = formatFec.format(solicitud.getFechafin());
            String hora = formatHor.format(solicitud.getHorafin());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraFin.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraFin.setValue(null);
        } finally {
        }*/
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    private long calcularCantDias() {
        try {
            long substract = calcWeekDays(convertToDateViaInstant(fechaInicio.getValue()), convertToDateViaInstant(fechaFin.getValue())) + 1;
            if (isWeekendSunday(fechaFin.getValue())) {
                substract -= 1l;
            }

            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()));
            long cantFeriado = 0;
            for (Feriado feriado : listFeriado) {
                if (!isWeekendSunday(DateUtils.asLocalDate(feriado.getFecha()))) {
                    cantFeriado++;
                }
            }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
            return (substract - cantFeriado);
        } catch (Exception e) {
            return 0;
        } finally {
        }
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreFuncionario.setValue("");
                txtArea.setValue("");
                txtCargo.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateFecha() {
        if (fechaInicio.getValue() != null && fechaFin.getValue() != null) {
            if (fechaInicio.getValue().isBefore(fechaFin.getValue()) || fechaInicio.getValue().isEqual(fechaFin.getValue())) {
                if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCanhs.setValue(dif + "");
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                } else {
                    txtCanhs.setValue(calcularCantDias() + "");
                }
            }
        }
    }

    private void updateFormulario() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void save() {
        long min = 0l;
        Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(func.getIdfuncionario());
        List<Marcaciones> listMarcacion = marcacionesDao.getByFilter(func, DateUtils.asDate(fechaHoraForm.getValue()), DateUtils.asDate(fechaHoraForm.getValue()), null, 0);
        if (listMarcacion.size() == 0) {
            if (validate()) {
                Marcaciones marcacion = new Marcaciones();

                marcacion.setHorarioFuncionario(hf);
                marcacion.setAlta("MANUAL");
                marcacion.setFecha(DateUtils.asDate(fechaHoraForm.getValue()));
                Timestamp tsEntrada = Timestamp.valueOf(hf.getHorarioLaboral().getEntrada() + "".replace(".0", ""));
                Timestamp tsSalida = Timestamp.valueOf(hf.getHorarioLaboral().getSalida() + "".replace(".0", ""));

                marcacion.setInasignada(tsEntrada);
                marcacion.setOutasignada(tsSalida);

                marcacion.setNombrefunc(func.getNombreCompleto());
                try {
                    marcacion.setInmarcada(Timestamp.valueOf(fechaHoraForm.getValue()));
                } catch (Exception e) {
                    marcacion.setInmarcada(null);
                } finally {
                }
                try {
                    marcacion.setOutmarcada(Timestamp.valueOf(fechaHoraForm.getValue()));
                } catch (Exception e) {
                    marcacion.setOutmarcada(null);
                } finally {
                }

                long minutesEntSal = 0;
                try {
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal =(marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } catch (Exception ec) {
                    marcacion.setOutmarcada(marcacion.getInmarcada());
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal =(marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } finally {
                }

                marcacion.setMintrabajada(minutesEntSal - 40);

                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    marcacion.setMinllegadatardia(0L);
                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(0L);
                    minutesEntSal =(marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                    int numDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                    marcacion.setMinllegadatardia(Long.parseLong(numDif+""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                }
                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                }
                marcacionesDao.guardar(marcacion);
                setVisible(false);
                saveListener.accept(marcacion);
                Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                Notification.show("Todos los campos son obligatorios.",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            Notification.show("Ya se registró la marcación para esa fecha asignada.",
                    Notification.Type.HUMANIZED_MESSAGE);
        }

    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    private void cargarPorFormulario(Parametro value) {
        mapLicenciaCompensar = new HashMap<>();
        if (value.getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
            fechaInicio.setVisible(true);
            fechaFin.setVisible(false);

            fechaInicio.setValue(null);
            fechaFin.setValue(null);

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            try {
                String fecha = formatFecs.format(new Date());
                String fec = fecha + "T" + "16:00:00";
                LocalDateTime ldtFins = LocalDateTime.parse(fec);
                fechaHoraInicio.setValue(ldtFins);
                fechaHoraFin.setValue(ldtFins);
                txtCanhs.setValue("0");
            } catch (Exception e) {
                fechaInicio.setValue(null);
                fechaHoraFin.setValue(null);
                txtCanhs.setValue("0");
            } finally {
            }

            try {
                String fecha = formatFecs.format(new Date());
                String fec = fecha + "T16:00";
                LocalDateTime ldtFin = LocalDateTime.parse(fec);
                fechaHoraInicioForm.setValue(ldtFin);
                fechaHoraFinForm.setValue(ldtFin);
            } catch (Exception e) {
                fechaHoraInicioForm.setValue(null);
                fechaHoraFinForm.setValue(null);
            } finally {
            }

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(true);
            fechaHoraFin.setVisible(true);
            txtDependencia.setVisible(false);

            fechaHoraFin.setCaption("Fecha Fin (*)");
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);
            fechaAsignada.setVisible(false);

            txtCedulaFuncionario.setVisible(true);
            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);
            txtCanhs.setVisible(true);
            fechaHoraInicioForm.setVisible(true);
            fechaHoraFinForm.setVisible(true);
            txtCanthsForm.setVisible(true);
            txtTareaRealizar.setVisible(true);

//            cbDpto.setItems(dptoDao.listarDepartamentosPadres());
            cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbDpto.setVisible(false);

            cbCargo.setItems(new ArrayList<>());
            cbCargo.setVisible(false);

            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            try {
                String fecha = formatFec.format(new Date());
                String fec = fecha + "T" + "07:20:00";
                LocalDateTime ldtFin = LocalDateTime.parse(fec);
                fechaHoraForm.setValue(ldtFin);
            } catch (Exception e) {
                fechaHoraForm.setValue(null);
            } finally {
            }

            if (value.getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                fechaHoraForm.setVisible(true);
                fechaInicio.setVisible(false);
            } else {
                fechaHoraForm.setVisible(false);
                fechaInicio.setVisible(true);
            }
            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHoraini());
            }).setCaption("Desde hs");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHorafin());
            }).setCaption("Hasta hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getCantHsmin();
            }).setCaption("Cant hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Tarea a realizar");

            fechaForm.setVisible(false);
            txtCantDiaForm.setVisible(false);
            fechaForm.setValue(null);
            txtCantDiaForm.setValue("");
        } else if (value.getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
            fechaInicio.setValue(null);
            fechaFin.setValue(null);
            fechaHoraInicio.setValue(null);
            fechaHoraFin.setValue(null);

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);
            txtCanhs.setValue("");
            txtCanhs.setVisible(false);
            txtDependencia.setVisible(false);
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);

            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);
            fechaAsignada.setVisible(true);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            fechaForm.setVisible(true);
            txtCantDiaForm.setVisible(true);
            fechaForm.setValue(DateUtils.asLocalDate(new Date()));
            txtCantDiaForm.setValue("1");
            fechaHoraInicioForm.setVisible(false);
            fechaHoraFinForm.setVisible(false);
            txtTareaRealizar.setVisible(false);
            txtCanthsForm.setVisible(false);

            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Licencia");
            List<Rotaciones> listRotacion = rotacionDao.listarPorIdFuncionarioFecha(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
            if (listRotacion.size() > 0) {
                fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
            } else {
                Notification.show("No existen rotaciones disponibles para cambiarlas",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            fechaInicio.setValue(null);
            fechaFin.setValue(null);
            fechaHoraInicio.setValue(null);
            fechaHoraFin.setValue(null);

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);
            txtCanhs.setValue("");
            txtCanhs.setVisible(false);
            txtDependencia.setVisible(false);
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);
            fechaAsignada.setVisible(false);

            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            fechaForm.setVisible(true);
            txtCantDiaForm.setVisible(true);
            fechaForm.setValue(DateUtils.asLocalDate(new Date()));
            try {
                if (!formulario.getValue().getValor().equalsIgnoreCase("")) {
                    txtCantDiaForm.setValue(formulario.getValue().getValor());
                } else {
                    txtCantDiaForm.setValue("SIN LIMITE");
                }
            } catch (Exception e) {
                txtCantDiaForm.setValue("SIN LIMITE");
            } finally {
            }
            fechaHoraInicioForm.setVisible(false);
            fechaHoraFinForm.setVisible(false);
            txtTareaRealizar.setVisible(false);
            txtCanthsForm.setVisible(false);

            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Licencia");
        }
    }

    private void guardarDetalle(Licencias solicitud) {
        /*LicenciasCompensar detalle = new LicenciasCompensar();
        List<LicenciasCompensar> listSolicitudDetalle = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        if (listSolicitudDetalle.size() > 0) {
            detalle = listSolicitudDetalle.get(0);
        }
        try {
            detalle.setObservacion(txtOtroMotivo.getValue());
        } catch (Exception e) {
            detalle.setObservacion(null);
        } finally {
        }
        try {
            detalle.setLicencia(solicitud);
        } catch (Exception e) {
            detalle.setLicencia(null);
        } finally {
        }

        licenciaCompensarDao.guardarLicenciasCompensar(detalle);*/
        if (editar == 0) {
            if (listProduccionDetalle.size() > 0) {
                for (LicenciasCompensar solicitudProduccionDetalle : listProduccionDetalle) {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                }
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            if (listProduccionDetalle.size() > 0) {
                licenciaCompensarDao.listarPorIdLicencia(solicitud.getId()).forEach((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.borrar(solicitudProduccionDetalle);
                });
                listProduccionDetalle.stream().map((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    return solicitudProduccionDetalle;
                }).forEachOrdered((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                });
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void updateFechaHora() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
            int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicio.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFin.getValue()));
            if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                long millis = (minFin - minInicio);
                try {
                    txtCanhs.setValue(sdfHM.format(sdf.parse(millis + "")));
                } catch (ParseException ex) {
                    txtCanhs.setValue(0 + "");
                }
            } else {
                txtCanhs.setValue(0 + "");
            }
        } catch (Exception e) {
            txtCanhs.setValue(0 + "");
        } finally {
        }

    }

    private void updateFechaHoraForm() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
            int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));
            if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                long millis = (minFin - minInicio);
                try {
                    txtCanthsForm.setValue(sdfHM.format(sdf.parse(millis + "")));
                } catch (ParseException ex) {
                    txtCanthsForm.setValue(0 + "");
                }
            } else {
                txtCanthsForm.setValue(0 + "");
            }
        } catch (Exception e) {
            txtCanthsForm.setValue(0 + "");
        } finally {
        }

    }

    private long verificarHsmin() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        long min = 0l;
        try {
            int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
            int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicio.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFin.getValue()));
            if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                min = (minFin - minInicio);
            }
        } catch (Exception e) {
        } finally {
        }
        return min;
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web-rrhh/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(NuevaMarcacionForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };

}
