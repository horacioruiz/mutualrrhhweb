/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.ValoTheme;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.ModalidadDao;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperaciondetalleDao;
import py.mutualmsp.mutualweb.entities.Modalidad;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.entities.Tipooperaciondetalle;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class TipooperaciondetalleForm extends FormLayout{
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    ComboBox<Modalidad> cmbModalidad = new ComboBox<>("Modalidad");
    
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    TipooperaciondetalleDao tipooperaciondetalleDao = ResourceLocator.locate(TipooperaciondetalleDao.class);
    Binder<Tipooperaciondetalle> binder = new Binder<>(Tipooperaciondetalle.class);
    
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    ModalidadDao modalidadDao = ResourceLocator.locate(ModalidadDao.class);
    
    Tipooperaciondetalle tipooperaciondetalle;
    
    private Consumer<Tipooperaciondetalle> guardarListener;
    private Consumer<Tipooperaciondetalle> borrarListener;
    private Consumer<Tipooperaciondetalle> cancelarListener;
            
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "tipooperaciondetalle";
            
    public TipooperaciondetalleForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            List<Tipooperacion> listaTipooperaciones = tipooperacionDao.listaTipooperacion();
            cmbTipooperacion.setItems(listaTipooperaciones);
            cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
            
            binder.bind(cmbTipooperacion, 
                    (Tipooperaciondetalle source) -> source.getIdtipooperacion(),
                    (Tipooperaciondetalle bean, Tipooperacion fieldvalue) -> {
                    bean.setIdtipooperacion(fieldvalue);});
            
            List<Modalidad> listaModalidads = modalidadDao.listaModalidad();
            cmbModalidad.setItems(listaModalidads);
            cmbModalidad.setItemCaptionGenerator(Modalidad::getDescripcion);
            
            binder.bind(cmbModalidad, 
                    (Tipooperaciondetalle source) -> source.getIdmodalidad(),
                    (Tipooperaciondetalle bean, Modalidad fieldvalue) -> {
                    bean.setIdmodalidad(fieldvalue);});
            
            btnGuardar.addStyleName(ValoTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(ValoTheme.BUTTON_DANGER);
            
            addComponents(cmbTipooperacion, cmbModalidad, botones);
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(tipooperaciondetalle);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            List<Tipooperaciondetalle> lista = new ArrayList<>();
            lista = tipooperaciondetalleDao.getListTipooperaciondetalleExistente(cmbTipooperacion.getValue().getIdtipooperacion(), 
                    cmbModalidad.getValue().getIdmodalidad());
            if (lista.isEmpty()) {
                tipooperaciondetalleDao.guardar(tipooperaciondetalle);
                setVisible(false);
                guardarListener.accept(tipooperaciondetalle);
                Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                if(viejo.equals("")) operacion="I";
                else operacion="M";
                this.setNuevo();
                AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } else {
                Notification.show("Advertencia", "Esta modalidad ya fue asignada a la Operación. No se pudo guardar", Notification.Type.ERROR_MESSAGE);
                guardarListener.accept(tipooperaciondetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(tipooperaciondetalle);
        }
    }
    
    private void borrar() {
        try {
            tipooperaciondetalleDao.borrar(tipooperaciondetalle);
            setVisible(false);
            borrarListener.accept(tipooperaciondetalle);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(tipooperaciondetalle);
        }
    }
    
    public void setGuardarListener(Consumer<Tipooperaciondetalle> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Tipooperaciondetalle> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Tipooperaciondetalle> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(tipooperaciondetalle.getIdtipooperacion()!=null) this.viejo = "("+tipooperaciondetalle.getIdtipooperaciondetalle()+","+tipooperaciondetalle.getIdtipooperacion().getIdtipooperacion()+","+tipooperaciondetalle.getIdmodalidad().getIdmodalidad()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+tipooperaciondetalle.getIdtipooperaciondetalle()+","+tipooperaciondetalle.getIdtipooperacion().getIdtipooperacion()+","+tipooperaciondetalle.getIdmodalidad().getIdmodalidad()+")";
    }

    public void setRequisitooperacion(Tipooperaciondetalle tod) {
        try {
            this.tipooperaciondetalle = tod;
            binder.setBean(tod);
            btnBorrar.setVisible((tipooperaciondetalle.getIdtipooperaciondetalle()!=null));
            setVisible(true);
            this.viejo="";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
