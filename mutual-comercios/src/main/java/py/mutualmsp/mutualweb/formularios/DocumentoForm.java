/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.DocumentoDao;
import py.mutualmsp.mutualweb.entities.Documento;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class DocumentoForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    DocumentoDao documentoDao = ResourceLocator.locate(DocumentoDao.class);
    Binder<Documento> binder = new Binder<>(Documento.class);
    
    Documento documento;
    
    private Consumer<Documento> guardarListener;
    private Consumer<Documento> borrarListener;
    private Consumer<Documento> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "documento";
            
    public DocumentoForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(txtfDescripcion, botones);
            
            binder.bind(txtfDescripcion, Documento::getDescripcion, Documento::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Documento::getDescripcion, Documento::setDescripcion);
            binder.bindInstanceFields(this);
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(documento);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            documentoDao.guardar(documento);
            setVisible(false);
            guardarListener.accept(documento);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(documento);
        }
    }
    
    private void borrar() {
        try {
            documentoDao.borrar(documento);
            setVisible(false);
            borrarListener.accept(documento);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(documento);
        }
    }
    
    public void setGuardarListener(Consumer<Documento> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Documento> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Documento> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }
    
    public void setViejo(){
        if(documento.getId()!=null) this.viejo = "("+documento.getId()+","+documento.getDescripcion()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+documento.getId()+","+documento.getDescripcion()+")";
    }

    public void setDocumento(Documento d) {
        try {
            this.documento = d;
            binder.setBean(d);
            btnBorrar.setVisible((documento.getId()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
