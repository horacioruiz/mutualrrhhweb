/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioExporadicoDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */
public class CompensacionesForm extends FormLayout {

    TextField txtfCedula = new TextField("Cédula", "Ingrese cédula");
    Label txtfCedulaIdentidad = new Label("Cédula");
    Label txtfNombreApellido = new Label("Nombre y Apellido");
    TextField txtfNombre = new TextField("Nombre", "Ingrese Nombre");
    TextField txtfApellido = new TextField("Fecha", "Ingrese Apellido");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");

    LicenciasCompensar lc = new LicenciasCompensar();

    Button btnGuardarExporadicos = new Button("Guardar");
    Button btnVerExporadico = new Button("");
    Button btnCancelarExporadicos = new Button("Cancelar");

    Button btnBorrar = new Button("Borrar");
    Button btnHorarioExporadico = new Button();
    CheckBox habilitado = new CheckBox("Habilitado");

    VerticalLayout formularioPrimero = new VerticalLayout();
    VerticalLayout formularioSegundo = new VerticalLayout();

    CheckBox chkLunes = new CheckBox("Lun");
    CheckBox chkMartes = new CheckBox("Mar");
    CheckBox chkMiercoles = new CheckBox("Miér");
    CheckBox chkJueves = new CheckBox("Jue");
    CheckBox chkViernes = new CheckBox("Vier");
    CheckBox chkSabado = new CheckBox("Sáb");

    ComboBox<String> horaEntrada = new ComboBox<>("Hs. Inicio");
    ComboBox<String> minutoEntrada = new ComboBox<>("Minuto");
    ComboBox<String> horaSalida = new ComboBox<>("Hs Fin");
    ComboBox<String> minutoSalida = new ComboBox<>("Minuto");

    DateField fecha = new DateField("Fecha");
    private DateField fechaHasta = new DateField("Fecha Hasta");

    TextField txtIdReloj = new TextField("Código", "");
    ComboBox<String> cbHorarioLaboral = new ComboBox<>("Horario");
    ComboBox<String> cbHorarioLaboralExporadico = new ComboBox<>("Horario");
    HashMap<String, HorarioLaboral> mapHL = new HashMap<>();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    LicenciasCompensarDao licenciaCompesarDao = ResourceLocator.locate(LicenciasCompensarDao.class);
    HorarioLaboralDao horarioLaboralDao = ResourceLocator.locate(HorarioLaboralDao.class);
    HorarioFuncionarioDao horarioFuncionarioDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioExporadicoDao horarioExporadicoDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    Binder<Funcionario> binder = new Binder<>(Funcionario.class);

    HorarioExporadicoForm horarioExporadicoForm = new HorarioExporadicoForm();

    Funcionario funcionario;

    private Consumer<LicenciasCompensar> guardarListener;
    private Consumer<LicenciasCompensar> borrarListener;
    private Consumer<LicenciasCompensar> cancelarListener;

    public CompensacionesForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnCancelar);

            HorizontalLayout botonesExporadicos = new HorizontalLayout();
            botonesExporadicos.addComponents(btnVerExporadico, btnGuardarExporadicos, btnCancelarExporadicos);

            HorizontalLayout checkSemanas = new HorizontalLayout();
            checkSemanas.addComponents(chkLunes, chkMartes, chkMiercoles);

            HorizontalLayout checkSemanas2 = new HorizontalLayout();
            checkSemanas2.addComponents(chkJueves, chkViernes, chkSabado);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnHorarioExporadico.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnHorarioExporadico.setIcon(VaadinIcons.PLUS);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnVerExporadico.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnVerExporadico.setIcon(VaadinIcons.LIST);

            btnGuardarExporadicos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
//            btnGuardarExporadicos.setIcon(VaadinIcons.ADD_DOCK);

            btnCancelarExporadicos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
//            btnCancelarExporadicos.setIcon(VaadinIcons.);

            Label divider = new Label("<hr></hr>", ContentMode.HTML);
//            Label htmlLabel = new Label("<b>Horarios Exporádicos</b>",
//                    ContentMode.HTML);

//            formularioPrimero.addComponent(txtfCedula);
//            formularioPrimero.addComponent(txtfNombre);
//            formularioPrimero.addComponent(txtfApellido);
            HorizontalLayout entradaIn = new HorizontalLayout();
            entradaIn.addComponent(horaEntrada);
            entradaIn.addComponent(new Label(":"));
            entradaIn.addComponent(minutoEntrada);

            HorizontalLayout salidaOut = new HorizontalLayout();
            salidaOut.addComponent(horaSalida);
            salidaOut.addComponent(new Label(":"));
            salidaOut.addComponent(minutoSalida);

            formularioPrimero.addComponent(txtIdReloj);
            formularioPrimero.addComponent(txtfNombre);
            formularioPrimero.addComponent(fecha);
            formularioPrimero.addComponent(entradaIn);
            formularioPrimero.addComponent(salidaOut);
            formularioPrimero.addComponent(botones);

//            formularioSegundo.addComponent(htmlLabel);
//            formularioSegundo.addComponent(txtfCedula);
//            formularioSegundo.addComponent(txtfNombre);
//            formularioSegundo.addComponent(txtfApellido);
//            HorizontalLayout horizontalForm = new HorizontalLayout();
//            horizontalForm.addComponents(txtfCedulaIdentidad, btnVerExporadico);
            formularioSegundo.addComponent(txtfCedulaIdentidad);
            formularioSegundo.addComponent(txtfNombreApellido);
            formularioSegundo.addComponent(checkSemanas);
            formularioSegundo.addComponent(checkSemanas2);
//            formularioSegundo.addComponent(fechaDesde);
            formularioSegundo.addComponent(fechaHasta);
            formularioSegundo.addComponent(cbHorarioLaboralExporadico);
            formularioSegundo.addComponent(botonesExporadicos);

            formularioSegundo.setVisible(false);

            addComponents(formularioPrimero, formularioSegundo);

//            binder.bind(txtfCedula, Funcionario::getCedula, Funcionario::setCedula);
//            binder.forField(txtfCedula).withNullRepresentation("")
//                    .bind(Funcionario::getCedula, Funcionario::setCedula);
            binder.bind(txtfNombre, Funcionario::getNombreCompleto, Funcionario::setNombreCompleto);
            binder.forField(txtfNombre).withNullRepresentation("")
                    .bind(Funcionario::getNombre, Funcionario::setNombre);

//            binder.bind(txtfApellido, Funcionario::getApellido, Funcionario::setApellido);
//            binder.forField(txtfApellido).withNullRepresentation("")
//                    .bind(Funcionario::getApellido, Funcionario::setApellido);
            binder.bindInstanceFields(this);

            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });
            btnVerExporadico.addClickListener(e -> {
                verExporadicos();
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnHorarioExporadico.addClickListener(e -> {
                borrar();
            });
            btnGuardarExporadicos.addClickListener(e -> {
                saveExporadicos();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(lc);
            });

            ArrayList<String> myHourIn = new ArrayList<String>();
            for (int i = 0; i < 24; i++) {
                myHourIn.add("" + i);
            }
            horaEntrada.setItems(myHourIn);
            horaEntrada.setWidth(5f, ComboBox.UNITS_EM);
            horaSalida.setItems(myHourIn);
            horaSalida.setWidth(5f, ComboBox.UNITS_EM);
            ArrayList<String> myHourOut = new ArrayList<String>();
            for (int i = 0; i < 60; i++) {
                if ((String.valueOf(i)).length() == 1) {
                    myHourOut.add("0" + i);
                } else {
                    myHourOut.add(i + "");
                }
            }

            minutoEntrada.setItems(myHourOut);
            minutoEntrada.setWidth(5f, ComboBox.UNITS_EM);
            minutoSalida.setItems(myHourOut);
            minutoSalida.setWidth(5f, ComboBox.UNITS_EM);

            btnCancelarExporadicos.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(lc);
            });
            List<HorarioLaboral> listHorarioLaboral = horarioLaboralDao.getListHorarioLaboralByEstado(true);
            List<String> listString = new ArrayList<>();
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            for (HorarioLaboral listHorario : listHorarioLaboral) {
                mapHL.put(formatHor.format(listHorario.getEntrada()) + " a " + formatHor.format(listHorario.getSalida()), listHorario);
                listString.add(formatHor.format(listHorario.getEntrada()) + " a " + formatHor.format(listHorario.getSalida()));
            }
            cbHorarioLaboral.setItems(listString);
            cbHorarioLaboralExporadico.setItems(listString);

            btnBorrar.setVisible(false);
            txtfNombre.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            Date fechaOld = lc.getFechacompensar();
            LocalDate ld = fecha.getValue();
            Date fecMarcada = DateUtils.asDate(ld);
            fecMarcada.setHours(0);
            fecMarcada.setMinutes(0);
            fecMarcada.setSeconds(0);

            java.sql.Date sqlDateMarcada = java.sql.Date.valueOf(ld);

            Date fechaHere = fecMarcada;

            Date fecha = new Date();
            fecha.setHours(0);
            fecha.setMinutes(0);
            fecha.setSeconds(0);
            java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fecha));

            boolean val = false;
            if (sqlDateMarcada.compareTo(sqlDateHoy) == 0) {
                val = true;
            } else if (sqlDateMarcada.before(sqlDateHoy)) {
                val = false;
            } else if (sqlDateMarcada.after(sqlDateHoy)) {
                val = true;
            }
            if (val) {
                List<LicenciasCompensar> listLic = licenciaCompesarDao.listarPorFechasHere(fecMarcada, fecMarcada, lc.getLicencia().getFuncionario().getIdfuncionario(), null, "licencia_compensar");
                java.sql.Date fechaOldSql = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaOld));
                java.sql.Date fechaNewSql = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHere));
                if (fechaOldSql.compareTo(fechaNewSql) == 0) {
                    lc.setFechacompensar(fechaHere);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String vHoraEntrada = horaEntrada.getValue().length() == 1 ? "0" + horaEntrada.getValue() : horaEntrada.getValue();
                    String vMinutoEntrada = minutoEntrada.getValue().length() == 1 ? "0" + minutoEntrada.getValue() : minutoEntrada.getValue();
                    String vHoraSalida = horaSalida.getValue().length() == 1 ? "0" + horaSalida.getValue() : horaSalida.getValue();
                    String vMinutoSalida = minutoSalida.getValue().length() == 1 ? "0" + minutoSalida.getValue() : minutoSalida.getValue();
                    Date parsedDateIn = dateFormat.parse("2020-03-12 " + vHoraEntrada + ":" + vMinutoEntrada + ":00");
                    if (vHoraSalida.trim().equalsIgnoreCase("12")) {
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                        Date parsedDateOut = dateFormat2.parse("2020-03-12 12:" + vMinutoSalida + ":00 PM");
//                    cargo.setSalida(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                        lc.setHorafin(new Timestamp(parsedDateOut.getTime()));
                    } else {
                        Date parsedDateOut = dateFormat.parse("2020-03-12 " + vHoraSalida + ":" + vMinutoSalida + ":00");
                        lc.setHorafin(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                    }

                    lc.setHoraini(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateIn)));

                    setVisible(false);
                    licenciaCompesarDao.guardarLicenciasCompensar(lc);
//            guardarHorarioFuncionario(funcionario);
                    guardarListener.accept(lc);
                    Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);

                } else if (!listLic.isEmpty()) {
                    Notification.show("Mensaje del Sistema", "Ya existe Licencia a compesar en la fecha seleccionada.", Notification.Type.ERROR_MESSAGE);
                } else {
                    lc.setFechacompensar(fechaHere);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String vHoraEntrada = horaEntrada.getValue().length() == 1 ? "0" + horaEntrada.getValue() : horaEntrada.getValue();
                    String vMinutoEntrada = minutoEntrada.getValue().length() == 1 ? "0" + minutoEntrada.getValue() : minutoEntrada.getValue();
                    String vHoraSalida = horaSalida.getValue().length() == 1 ? "0" + horaSalida.getValue() : horaSalida.getValue();
                    String vMinutoSalida = minutoSalida.getValue().length() == 1 ? "0" + minutoSalida.getValue() : minutoSalida.getValue();
                    Date parsedDateIn = dateFormat.parse("2020-03-12 " + vHoraEntrada + ":" + vMinutoEntrada + ":00");
                    if (vHoraSalida.trim().equalsIgnoreCase("12")) {
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                        Date parsedDateOut = dateFormat2.parse("2020-03-12 12:" + vMinutoSalida + ":00 PM");
//                    cargo.setSalida(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                        lc.setHorafin(new Timestamp(parsedDateOut.getTime()));
                    } else {
                        Date parsedDateOut = dateFormat.parse("2020-03-12 " + vHoraSalida + ":" + vMinutoSalida + ":00");
                        lc.setHorafin(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                    }

                    lc.setHoraini(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateIn)));

                    setVisible(false);
                    licenciaCompesarDao.guardarLicenciasCompensar(lc);
//            guardarHorarioFuncionario(funcionario);
                    guardarListener.accept(lc);
                    Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                }

            } else {
                Notification.show("Advertencia", "La fecha a modificar debe ser mayor o igual a la fecha actual", Notification.Type.ERROR_MESSAGE);
            }
//            funcionario.setIdestado(habilitado.getValue() == null || habilitado.getValue() == false ? 262L : 13L);
//            funcionario = funcionarioDao.guardar(funcionario);

        } catch (Exception e) {
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(lc);
        }
    }

    private void guardarHorarioFuncionario(Funcionario funcionario) {
        try {
            HorarioFuncionario hf = horarioFuncionarioDao.listarPorIdFuncionario(funcionario.getIdfuncionario());
            if (hf == null) {
                hf = new HorarioFuncionario();
                hf.setFuncionario(funcionario);
                hf.setHorarioLaboral(mapHL.get(cbHorarioLaboral.getValue()));
                hf.setIdreloj(Long.parseLong(txtIdReloj.getValue()));
            } else {
                hf.setFuncionario(funcionario);
                hf.setHorarioLaboral(mapHL.get(cbHorarioLaboral.getValue()));
                hf.setIdreloj(Long.parseLong(txtIdReloj.getValue()));
            }
            horarioFuncionarioDao.guardarHorarioFuncionario(hf);
        } catch (Exception e) {
        } finally {
        }
    }

    private void borrar() {
        try {
            funcionarioDao.borrar(funcionario);
            setVisible(false);
//            borrarListener.accept(funcionario);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
//            borrarListener.accept(funcionario);
        }
    }

    public void setGuardarListener(Consumer<LicenciasCompensar> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<LicenciasCompensar> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<LicenciasCompensar> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setFuncionario(LicenciasCompensar f) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        lc = f;
//        if (valor) {
        txtIdReloj.setValue(f.getLicencia().getId() + "");
        txtfNombre.setValue(f.getLicencia().getFuncionario().getNombreCompleto());
//        txtfApellido.setValue(simpleDateFormat.format(f.getFechacompensar()));
        fecha.setValue(DateUtils.asLocalDate(f.getFechacompensar()));

        long hsInicio = f.getHoraini().getHours();
        long minInicio = f.getHoraini().getMinutes();

        long hsFin = f.getHorafin().getHours();
        long minFin = f.getHorafin().getMinutes();

        String horaInicio = hsInicio < 10 ? "0" + hsInicio : (hsInicio + "");
        String minutoInicio = minInicio < 10 ? "0" + minInicio : (minInicio + "");

        String horaFin = hsFin < 10 ? "0" + hsFin : (hsFin + "");
        String minutoFin = minFin < 10 ? "0" + minFin : (minFin + "");

        horaEntrada.setValue(horaInicio);
        minutoEntrada.setValue(minutoInicio);

        horaSalida.setValue(horaFin);
        minutoSalida.setValue(minutoFin);

        txtIdReloj.setEnabled(false);
        txtfNombre.setEnabled(false);
        txtfApellido.setEnabled(false);

//        formularioSegundo.setVisible(true);
        formularioPrimero.setVisible(true);

//        fecha.setValue(null);
//        fechaHasta.setValue(null);
        cbHorarioLaboralExporadico.setValue(null);

        fecha.setEnabled(true);
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
//        try {
//            this.funcionario = f.getLicencia().getFuncionario();
//            if (this.funcionario.getIdfuncionario() != null) {
//                if (this.funcionario.getIdestado() == 13) {
//                    habilitado.setValue(true);
//                } else {
//                    habilitado.setValue(false);
//                }
//                HorarioFuncionario hf = new HorarioFuncionario();
//                try {
//                    hf = horarioFuncionarioDao.listarPorIdFuncionario(funcionario.getIdfuncionario());
//                    txtIdReloj.setValue(String.valueOf(hf.getIdreloj()));
//                } catch (Exception e) {
//                    txtIdReloj.setValue("");
//                } finally {
//                }
//                try {
//                    cbHorarioLaboral.setValue(formatHor.format(hf.getHorarioLaboral().getEntrada()) + " a " + formatHor.format(hf.getHorarioLaboral().getSalida()));
//                } catch (Exception e) {
//                    cbHorarioLaboral.setValue("");
//                } finally {
//                }
//            } else {
//                txtIdReloj.setValue("");
//                cbHorarioLaboral.setValue("");
//            }
//            binder.setBean(f.getLicencia().getFuncionario());
//            btnBorrar.setVisible((funcionario.getIdfuncionario() != null));
//            setVisible(true);
//            txtfCedula.selectAll();
//            txtfCedulaIdentidad.setValue(f.getLicencia().getFuncionario().getCedula());
//            txtfNombreApellido.setValue(f.getLicencia().getFuncionario().getNombreCompleto());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
//        if (txtfCedula == null || txtfCedula.isEmpty()) {
//            savedEnabled = false;
//        }
//        if (txtfNombre == null || txtfNombre.isEmpty()) {
//            savedEnabled = false;
//        }
        if (fecha == null || fecha.isEmpty()) {
            savedEnabled = false;
        }
        if (horaEntrada == null || horaEntrada.isEmpty()) {
            savedEnabled = false;
        }
        if (minutoEntrada == null || minutoEntrada.isEmpty()) {
            savedEnabled = false;
        }
        if (horaSalida == null || horaSalida.isEmpty()) {
            savedEnabled = false;
        }
        if (minutoSalida == null || minutoSalida.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void saveExporadicos() {
        boolean lunes = chkLunes.getValue();
        boolean martes = chkMartes.getValue();
        boolean miercoles = chkMiercoles.getValue();
        boolean jueves = chkJueves.getValue();
        boolean viernes = chkViernes.getValue();
        boolean sabado = chkSabado.getValue();

        Date desde = null;
        try {
            desde = DateUtils.asDate(fecha.getValue());
        } catch (Exception e) {
        } finally {
        }
        Date hasta = null;
        try {
            hasta = DateUtils.asDate(fechaHasta.getValue());
        } catch (Exception e) {
        } finally {
        }

        boolean val = false;
        if (!lunes && !martes && !miercoles && !jueves && !viernes && !sabado && desde == null && hasta == null) {
            Notification.show("Advertencia", "Debes seleccionar días y rango de fecha para registrar un nuevo horario exporádico.", Notification.Type.ERROR_MESSAGE);
        } else if (cbHorarioLaboralExporadico.getValue() == null || cbHorarioLaboralExporadico.getValue().equalsIgnoreCase("")) {
            Notification.show("Advertencia", "Debes seleccionar el horario.", Notification.Type.ERROR_MESSAGE);
        } else if ((lunes || martes || miercoles || jueves || viernes || sabado) && !cbHorarioLaboralExporadico.getValue().equalsIgnoreCase("")) {
            if (desde != null && hasta != null && !cbHorarioLaboralExporadico.getValue().equalsIgnoreCase("")) {
                val = true;
            } else {
                Notification.show("Advertencia", "Debes seleccionar fecha desde y fecha hasta para registrar los datos.", Notification.Type.ERROR_MESSAGE);
            }
        } else {
            Notification.show("Advertencia", "Debes seleccionar al menos un día de la semana para registrar los datos.", Notification.Type.ERROR_MESSAGE);
        }

        if (val) {
            try {
                Funcionario funcionarioF = funcionarioDao.listarFuncionarioPorCI(txtfCedulaIdentidad.getValue());
                HorarioExporadico he = new HorarioExporadico();
                he.setFechadesde(desde);
                he.setFechahasta(hasta);
                he.setFuncionario(funcionarioF);
                String horarioLaboralHere = cbHorarioLaboralExporadico.getValue();
                he.setHorarioLaboral(mapHL.get(horarioLaboralHere));
                he.setLunes(lunes);
                he.setMartes(martes);
                he.setMiercoles(miercoles);
                he.setJueves(jueves);
                he.setViernes(viernes);
                he.setSabado(sabado);

                List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.listarExistenciaDatos(he);
                if (listHorarioExporadico.isEmpty()) {
                    horarioExporadicoDao.guardarHorarioExporadico(he);
                    setVisible(false);
                    Notification.show("Mensaje del Sistema", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    Notification.show("Información", "Verificar el detalle existen datos que generan solapamiento.", Notification.Type.ERROR_MESSAGE);
                }
            } catch (Exception e) {
            } finally {
            }
        }
    }

    private void verExporadicos() {
        Funcionario funcionarioF = funcionarioDao.listarFuncionarioPorCI(txtfCedulaIdentidad.getValue());
        List<HorarioExporadico> listHE = horarioExporadicoDao.listarPorIdFuncionarioF(funcionarioF.getIdfuncionario());

        if (listHE.isEmpty()) {
            Notification.show("Mensaje del Sistema", "No existen detalles asociados.", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            HorarioExporadicoForm horarioExporadicoForm = new HorarioExporadicoForm();
            UI.getCurrent().addWindow(horarioExporadicoForm);

            horarioExporadicoForm.nuevoRegistro(funcionarioF, listHE);
            horarioExporadicoForm.setVisible(true);
        }
    }

}
