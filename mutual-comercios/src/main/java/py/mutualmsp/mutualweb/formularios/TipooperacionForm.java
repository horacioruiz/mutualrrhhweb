/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */
public class TipooperacionForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    Binder<Tipooperacion> binder = new Binder<>(Tipooperacion.class);
    
    Tipooperacion tipooperacion;
    
    private Consumer<Tipooperacion> guardarListener;
    private Consumer<Tipooperacion> borrarListener;
    private Consumer<Tipooperacion> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "tipooperacion";
            
    public TipooperacionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(txtfDescripcion, botones);
            
            binder.bind(txtfDescripcion, Tipooperacion::getDescripcion, Tipooperacion::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Tipooperacion::getDescripcion, Tipooperacion::setDescripcion);
            binder.bindInstanceFields(this);
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
                borrar();
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(tipooperacion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            tipooperacionDao.guardar(tipooperacion);
            setVisible(false);
            guardarListener.accept(tipooperacion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar.", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(tipooperacion);
        }
    }

    private void borrar() {
        try {
            tipooperacionDao.borrar(tipooperacion);
            setVisible(false);
            borrarListener.accept(tipooperacion);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(tipooperacion);
        }
    }

    public void setGuardarListener(Consumer<Tipooperacion> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Tipooperacion> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Tipooperacion> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(tipooperacion.getIdtipooperacion()!=null) this.viejo = "("+tipooperacion.getIdtipooperacion()+","+tipooperacion.getDescripcion()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+tipooperacion.getIdtipooperacion()+","+tipooperacion.getDescripcion()+")";
    }

    public void setTipooperacion(Tipooperacion t) {
        try {
            this.tipooperacion = t;
            binder.setBean(t);
            btnBorrar.setVisible((tipooperacion.getIdtipooperacion()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo="";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
}
