package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ClassResource;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DocumentoLicenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.DocumentoLicencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import static py.mutualmsp.mutualweb.formularios.SolicitudForm.isWeekend;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 28/6/2016.
 */
public class LicenciasForm extends Window {

    private TextField txtCedulaFuncionario = new TextField("Ingrese cédula (*)");
    private TextField txtNombreFuncionario = new TextField("Funcionario");
    private TextField txtArea = new TextField("Area");
    private TextField txtCargo = new TextField("Cargo");
    private TextArea txtObservacion = new TextArea("Observación");
    Grid<LicenciasCompensar> gridLicenciaCompensar = new Grid<>(LicenciasCompensar.class);
    Grid<DocumentoLicencia> gridDocumentacionLicencia = new Grid<>(DocumentoLicencia.class);
    private TextField txtCanhs = new TextField("Cantidad hs");
    private TextField txtCantDiaForm = new TextField("Cant dia");
    private DateField fechaInicio = new DateField("Fecha Inicio (*)");
    private DateField fechaForm = new DateField("Fecha (*)");
    private DateField fechaAsignada = new DateField("Fecha asignada");
    private DateField fechaFin = new DateField("Fecha Fin (*)");
    private ComboBox<String> prioridad = new ComboBox<>("Prioridad");
    private int editar = 0;

    private ComboBox<Dependencia> cbDpto = new ComboBox<>("Departamento (*)");
    private ComboBox<Dependencia> cbCargo = new ComboBox<>("Sección (*)");

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private TextField costo = new TextField("Costo");
    private TextField horas = new TextField("Horas Trabajadas");
    private TextField tiempoRespuesta = new TextField("Tiempo de Respuesta");
    private TextField txtDependencia = new TextField("Dependencia");
    private ComboBox<Parametro> formulario = new ComboBox<>("Parametro (*)");
    LicenciasCompensar licCompensarSeleccionado = new LicenciasCompensar();
    List<LicenciasCompensar> listProduccionDetalle = new ArrayList<>();
    List<DocumentoLicencia> listDocumentacion = new ArrayList<>();

    //Grid<LicenciasProduccionDetalle> gridLicenciaCompensar = new Grid<>(LicenciasProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    HorizontalLayout mainLayoutArchivo = new HorizontalLayout();
    ComboBox<Funcionario> cbFuncionario = new ComboBox<>("Funcionario (*)");
    TextField txtCargoForm = new TextField("Cargo");
    DateTimeField fechaHoraForm = new DateTimeField("Fecha/Hora estipulada (*)");
    DateTimeField fechaHoraFinalForm = new DateTimeField("Fecha/Hora final (*)");
    DateTimeField txtCantMinCompensar = new DateTimeField("Fecha/Hora final (*)");
    private Button btnAgregar = new Button("Agregar");

    private ComboBox<Motivos> motivo = new ComboBox<>("Motivos (*)");
    private ComboBox<Funcionario> encargado = new ComboBox<>("Encargado (*)");
//    private ComboBox<Funcionario> rrhh = new ComboBox<>("Funcionario RRHH");
//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    private TextArea txtOtroMotivo = new TextArea("Observación");
    ImageReceiver receiver = new ImageReceiver();
    ImageReceiver receiverArchivo = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    Upload uploadArchivo = new Upload("Subir archivo", receiverArchivo);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    final Image imageArchivo = new Image("Imagen");
    Label labelUrlArchivo = new Label();
    private Button btnAgregarDocumento = new Button();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<Licencias> saveListener;
    private Consumer<Licencias> deleteListener;
    private Consumer<Licencias> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
    HashMap<String, Feriado> mapeoFeriados = new HashMap<>();
    ParametroDao parametroController = ResourceLocator.locate(ParametroDao.class);
    boolean mostrarFechaAnterior = false;
//    LicenciasProduccionDetalle spd = new LicenciasProduccionDetalle();
    int numRowSelected = 0;
    int numRowSelectedArchivo = 0;
    LicenciasCompensar spd = new LicenciasCompensar();
    DocumentoLicencia spdArchivo = new DocumentoLicencia();

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    ParametroDao formularioDao = ResourceLocator.locate(ParametroDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    //DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    LicenciasDao solicitudDao = ResourceLocator.locate(LicenciasDao.class);
    LicenciasCompensarDao licenciaCompensarDao = ResourceLocator.locate(LicenciasCompensarDao.class);
    DocumentoLicenciaDao documentoLicenciaDao = ResourceLocator.locate(DocumentoLicenciaDao.class);
    // LicenciasProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(LicenciasProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private Licencias solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();
//    InlineDateTimeField sample = new InlineDateTimeField();
    DateTimeField fechaHoraInicio = new DateTimeField("Fecha/hora Inicio (*)");
    DateTimeField fechaHoraFin = new DateTimeField("Fecha/hora Fin (*)");
    DateTimeField fechaHoraInicioForm = new DateTimeField("Fecha/hora Inicio (*)");
    DateTimeField fechaHoraFinForm = new DateTimeField("Fecha/hora Fin (*)");
    long minutoDiferenciado = 0;
    private TextField txtTareaRealizar = new TextField("Tarea a realizar (*)");
    private TextField txtCanthsForm = new TextField("Cant hs (*)");
    Map<String, LicenciasCompensar> mapLicenciaCompensar = new HashMap<>();

    //List<LicenciasProduccionDetalle> listProduccionDetalle = new ArrayList<>();
    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    StreamResource myResource;
    // Implement both receiver that saves upload in a file and
    // listener for successful upload

    public LicenciasForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Licencias");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();
            mapeoFeriados = new HashMap<>();
            txtCanthsForm.setEnabled(false);
            txtCantDiaForm.setEnabled(false);

            gridLicenciaCompensar.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    try {
                        if (!txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE") && !formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                            listProduccionDetalle = new ArrayList<>();
                            gridLicenciaCompensar.clearSortOrder();
                            gridLicenciaCompensar.setItems(listProduccionDetalle);
                            mapLicenciaCompensar = new HashMap<>();

                            numRowSelected = 0;
                            spd = new LicenciasCompensar();
                        } else {
                            if (numRowSelected >= 0) {
                                listProduccionDetalle.remove(numRowSelected);
                                gridLicenciaCompensar.clearSortOrder();
                                gridLicenciaCompensar.setItems(listProduccionDetalle);
                                mapLicenciaCompensar = new HashMap<>();
                                SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                                listProduccionDetalle.forEach((component) -> {
                                    mapLicenciaCompensar.put(formatFecs.format(component.getFechacompensar()), component);
                                });
                                numRowSelected = -1;
                                spd = new LicenciasCompensar();
                            }
                        }
                    } catch (Exception e) {
                        if (numRowSelected >= 0) {
                            listProduccionDetalle.remove(numRowSelected);
                            gridLicenciaCompensar.clearSortOrder();
                            gridLicenciaCompensar.setItems(listProduccionDetalle);
                            mapLicenciaCompensar = new HashMap<>();
                            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                            listProduccionDetalle.forEach((component) -> {
                                mapLicenciaCompensar.put(formatFecs.format(component.getFechacompensar()), component);
                            });
                            numRowSelected = -1;
                            spd = new LicenciasCompensar();
                        }
                    } finally {
                    }

                }
            });
            gridDocumentacionLicencia.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    try {
                        if (numRowSelectedArchivo >= 0) {
                            listDocumentacion.remove(numRowSelectedArchivo);
                            gridDocumentacionLicencia.clearSortOrder();
                            gridDocumentacionLicencia.setItems(listDocumentacion);
//                            mapLicenciaCompensar = new HashMap<>();

                            numRowSelectedArchivo = -1;
                            spdArchivo = new DocumentoLicencia();
                        }
                    } catch (Exception e) {
                    } finally {
                    }

                }
            });

            List<Parametro> listParame = parametroController.listarPorCodigo("cargar_fecha_anterior_solicitud");
            if (listParame.get(0).getValor().equalsIgnoreCase("SI")) {
                mostrarFechaAnterior = true;
            } else {
                mostrarFechaAnterior = false;
            }

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHoraini());
            }).setCaption("Desde hs");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHorafin());
            }).setCaption("Hasta hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getCantHsmin();
            }).setCaption("Cant hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Tarea a realizar");
            gridLicenciaCompensar.addComponentColumn(this::buildRemoveData).setCaption("");
            //            sample.setValue(LocalDateTime.now());
            //            sample.setLocale(Locale.US);
            //            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreFuncionario.setEnabled(false);
            txtArea.setEnabled(false);
            txtCanhs.setEnabled(false);
            txtCargo.setEnabled(false);
            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);
            txtCargoForm.setEnabled(false);

            mainLayout.setVisible(false);

            gridDocumentacionLicencia.clearSortOrder();
            gridDocumentacionLicencia.setCaption("Documentos adjuntos");
            gridDocumentacionLicencia.setItems(listDocumentacion);
            gridDocumentacionLicencia.removeAllColumns();
            gridDocumentacionLicencia.addColumn(e -> {
                return e.getUrl();
            }).setCaption("Documento");
            gridDocumentacionLicencia.addComponentColumn(this::buildConfirmButton).setCaption("");
            gridDocumentacionLicencia.addComponentColumn(this::buildRemove).setCaption("");

            txtCedulaFuncionario.addBlurListener(e -> {
                validate();
            });

            formulario.setItems(formularioDao.listarPorTipoCodigo("licencias"));
            formulario.setItemCaptionGenerator(Parametro::getDescripcion);
            cargarPorFormulario(formularioDao.listarPorTipoCodigo("licencias").get(0));

//            rrhh.setItems(funcionarioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setItems(funcionarioDao.listaFuncionario());
            encargado.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setVisible(false);

            txtCedulaFuncionario.focus();

            txtCedulaFuncionario.addValueChangeListener(e -> updateList(e.getValue()));
            formulario.addValueChangeListener(e -> cargarPorFormulario(e.getValue()));
            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));

            gridLicenciaCompensar.addItemClickListener(e -> {
                if (e.getItem() != null) {
                    spd = e.getItem();
                    numRowSelected = e.getRowIndex();
                }
            });
            gridDocumentacionLicencia.addItemClickListener(e -> {
                if (e.getItem() != null) {
                    spdArchivo = e.getItem();
                    numRowSelectedArchivo = e.getRowIndex();
                }
            });

            txtCedulaFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                        try {
                            calcularCantDiaFuncionario();
                            if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                                List<Rotaciones> listRotacion = rotacionDao.listarPorIdFuncionarioFecha(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
                                if (listRotacion.size() > 0) {
                                    fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
                                }
                            }
                            btnAgregar.setEnabled(true);
                            guardar.setEnabled(true);
                            verificarLicenciaAnual();
                        } catch (Exception e) {
                        } finally {
                        }

//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }

                private void calcularCantDiaFuncionario() {
                    SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                    SimpleDateFormat sdf = new SimpleDateFormat("mm");

                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    //long dif = calcularCantDias();
                    long dif = calcularCantDiasDif();
                    long minutePorDia = minutoDiferenciado / dif;
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    dif = dif * minutePorDia;
                    try {
                        txtCanhs.setValue(sdfHM.format(sdf.parse(dif + "")));
                    } catch (ParseException ex) {
                        System.out.println("-> " + ex.getLocalizedMessage());
                    } finally {
                    }
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            fechaHoraInicio.addValueChangeListener(e -> updateFechaHora());
            fechaHoraFin.addValueChangeListener(e -> updateFechaHora());
            fechaHoraInicioForm.addValueChangeListener(e -> updateFechaHoraForm());
            fechaHoraFinForm.addValueChangeListener(e -> updateFechaHoraForm());
            fechaInicio.addValueChangeListener(e -> updateFecha());
            fechaFin.addValueChangeListener(e -> updateFecha());
            cbFuncionario.addValueChangeListener(e -> updateFormulario());

            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                long min = 0l;
                boolean val = true;
                guardar.setVisible(true);
                try {
                    if (!mostrarFechaAnterior) {
                        Date fechaHoy = new Date();
                        fechaHoy.setHours(0);
                        fechaHoy.setMinutes(0);
                        fechaHoy.setSeconds(0);
                        java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                        Date fechaSolicitud = null;
                        if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                            try {
                                fechaSolicitud = DateUtils.asDate(fechaHoraInicioForm.getValue());
                                fechaSolicitud.setHours(0);
                                fechaSolicitud.setMinutes(0);
                                fechaSolicitud.setSeconds(0);
                            } catch (Exception e) {
                            } finally {
                            }
                            if (fechaSolicitud == null) {
                                try {
                                    fechaSolicitud = DateUtils.asDate(fechaHoraInicio.getValue());
                                    fechaSolicitud.setHours(0);
                                    fechaSolicitud.setMinutes(0);
                                    fechaSolicitud.setSeconds(0);
                                } catch (Exception e) {
                                } finally {
                                }
                            }
                        }

                        if (fechaSolicitud == null) {
                            try {
                                fechaSolicitud = DateUtils.asDate(fechaForm.getValue());
                                fechaSolicitud.setHours(0);
                                fechaSolicitud.setMinutes(0);
                                fechaSolicitud.setSeconds(0);
                            } catch (Exception e) {
                            } finally {
                            }
                        }

                        java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));

                        if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                            guardar.setVisible(false);
                            Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                                    Notification.Type.ERROR_MESSAGE);
                            val = false;
                        } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                            Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                                    Notification.Type.ERROR_MESSAGE);
                            guardar.setVisible(false);
                            val = false;
                        } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                            val = true;
                        }
                    }
                } catch (Exception e) {
                } finally {
                }

                if (val) {
                    try {
                        Date date1 = DateUtils.asDate(fechaHoraInicioForm.getValue());
                        Date date2 = DateUtils.asDate(fechaHoraFinForm.getValue());
                        date1.setHours(0);
                        date1.setMinutes(0);
                        date1.setSeconds(0);

                        date2.setHours(0);
                        date2.setMinutes(0);
                        date2.setSeconds(0);
                        long cantDias = (calcularCantDias(date1, date2));

                        int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
                        int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
                        String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
                        String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));
                        if (minFin > minInicio) {
                            min = (minFin - minInicio) * cantDias;
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        long cantDias = 1;
                        try {
                            Date date1 = DateUtils.asDate(fechaHoraInicio.getValue());
                            Date date2 = DateUtils.asDate(fechaHoraFin.getValue());
                            date1.setHours(0);
                            date1.setMinutes(0);
                            date1.setSeconds(0);

                            date2.setHours(0);
                            date2.setMinutes(0);
                            date2.setSeconds(0);
                            cantDias = (calcularCantDias(date1, date2));
                        } catch (Exception e) {
                        } finally {
                        }

                        if (min > 0) {
                            if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                                if (txtCanhs.getValue().trim().equalsIgnoreCase("0")) {
                                    Notification.show("La cantidad hs compensada debe ser mayor a cero.",
                                            Notification.Type.ERROR_MESSAGE);
                                } else {
                                    boolean estado = validarHorarios();
                                    if (estado) {
                                        int minDif = 0;
                                        for (LicenciasCompensar licenciasCompensar : listProduccionDetalle) {
                                            int minFin = ((licenciasCompensar.getHorafin().getHours() * 60) + licenciasCompensar.getHorafin().getMinutes());
                                            int minInicio = ((licenciasCompensar.getHoraini().getHours() * 60) + licenciasCompensar.getHoraini().getMinutes());
                                            if (minFin > minInicio) {
                                                minDif += (minFin - minInicio);
                                            }
                                        }
                                        int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
                                        int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
                                        long minDifMax = (minFin - minInicio) * cantDias;
                                        if (minDifMax > minDif && minDifMax >= (minDif + min)) {
                                            cargarGrilla();
                                        } else {
                                            Notification.show("Las horas asignadas al detalle no deben superar el máximo requerido.",
                                                    Notification.Type.ERROR_MESSAGE);
                                        }
                                    }
                                }
                            } else {
                                if (txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE")) {
                                    cargarGrillaSinCompensar();
                                } else if (Long.parseLong(txtCantDiaForm.getValue().trim()) == listProduccionDetalle.size()) {
                                    Notification.show("Ya completó el detalle máximo de días requerido.",
                                            Notification.Type.ERROR_MESSAGE);
                                } else {
                                    cargarGrillaSinCompensar();
                                }
                            }
                        } else {
                            if (!formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                                if (txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE")) {
                                    cargarGrillaSinCompensar();
                                } else if (Long.parseLong(txtCantDiaForm.getValue().trim()) == listProduccionDetalle.size()) {
                                    Notification.show("Ya completo el detalle maximo de dias requerido.",
                                            Notification.Type.ERROR_MESSAGE);
                                } else {
                                    cargarGrillaSinCompensar();
                                }
                            } else {
                                Notification.show("La hora fin debe ser mayor a la hora inicio y verificar que el/los dia/s no sean feriados o domigos.",
                                        Notification.Type.ERROR_MESSAGE);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                        System.out.println(e.fillInStackTrace());
                    }
                }
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Licencias")) {
                        vl.addComponent(createForm());
                    } else if (caption.equalsIgnoreCase("Documentos")) {
                        vl.addComponent(createDocumentForm());
                    } else {
                        vl.addComponent(createFormMotivoSolicitud());
                    }

                    tab.addComponent(vl);
                }

                private VerticalLayout createForm() {
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    horizontal.addComponent(txtCedulaFuncionario);
                    horizontal.addComponent(txtNombreFuncionario);
                    horizontal.addComponent(txtArea);
                    horizontal.addComponent(txtCargo);

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(formulario);
                    //horizontalSegundo.addComponent(fechaInicio);
                    horizontalSegundo.addComponent(fechaFin);
                    horizontalSegundo.addComponent(fechaHoraInicio);
                    horizontalSegundo.addComponent(fechaHoraFin);
                    horizontalSegundo.addComponent(txtCanhs);
                    horizontalSegundo.addComponent(cbDpto);
                    horizontalSegundo.addComponent(cbCargo);

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(txtObservacion);
                    txtObservacion.setWidth("100%");

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }

                private VerticalLayout createFormMotivoSolicitud() {
//                    guardar.setVisible(true);
                    VerticalLayout layout = new VerticalLayout();
                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    HorizontalLayout horizontal = new HorizontalLayout();
                    //horizontal.addComponent(motivo);

//        horizontal.addComponent(rrhh);
                    horizontal.addComponent(encargado);
                    //horizontal.addComponent(upload);
                    //horizontal.addComponent(labelUrl);

                    image.setVisible(false);
                    upload.setButtonCaption("Seleccionar");
                    upload.addSucceededListener(receiver);

                    // Prevent too big downloads 0981752315
                    final long UPLOAD_LIMIT = 104857600l;
                    upload.addStartedListener(new Upload.StartedListener() {
                        private static final long serialVersionUID = 4728847902678459488L;

                        @Override
                        public void uploadStarted(Upload.StartedEvent event) {
                            if (event.getContentLength() > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });

                    // Check the size also during progress
                    upload.addProgressListener(new Upload.ProgressListener() {
                        private static final long serialVersionUID = 8587352676703174995L;

                        @Override
                        public void updateProgress(long readBytes, long contentLength) {
                            if (readBytes > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                upload.interruptUpload();
                            }
                        }
                    });
                    // Create uploads directory
                    File uploads = new File(Constants.UPLOAD_DIR_TEMP);
                    if (!uploads.exists() && !uploads.mkdir()) {
                        horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
                    }

                    horizontal.setWidth("100%");
                    horizontal.setSpacing(true);
                    horizontal.setStyleName("top-bar");

                    HorizontalLayout horizontalSegundo = new HorizontalLayout();
                    horizontalSegundo.addComponent(txtOtroMotivo);
                    HorizontalLayout horizontalTercero = new HorizontalLayout();
                    horizontalTercero.addComponent(txtDependencia);
//        horizontalSegundo.addComponent(txtOtroMotivo);
                    txtOtroMotivo.setWidth("100%");

                    horizontalSegundo.setWidth("100%");
                    horizontalSegundo.setSpacing(true);
                    horizontalSegundo.setStyleName("top-bar");
                    txtDependencia.setWidth("100%");

                    horizontalTercero.setWidth("100%");
                    horizontalTercero.setSpacing(true);
                    horizontalTercero.setStyleName("top-bar");

                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);

                    form.addComponent(fechaAsignada);
                    form.addComponent(fechaForm);
                    form.addComponent(txtCantDiaForm);
                    form.addComponent(fechaHoraInicioForm);
                    form.addComponent(fechaHoraFinForm);
                    form.addComponent(txtTareaRealizar);
                    form.addComponent(txtCanthsForm);
                    form.addComponent(txtTareaRealizar);
                    form.addComponent(btnAgregar);
                    mainLayout.addComponent(gridLicenciaCompensar);
                    mainLayout.addComponent(form);
                    mainLayout.setWidth("100%");

                    layout.addComponent(mainLayout);

                    layout.addComponent(horizontal);
                    layout.addComponent(horizontalSegundo);
                    layout.addComponent(horizontalTercero);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);

//        FormLayout columnLayout = new FormLayout();
//// Setting the desired responsive steps for the columns in the layout
//columnLayout.setResponsiveSteps(
//           new ResponsiveStep("25em", 1),
//           new ResponsiveStep("32em", 2),
//           new ResponsiveStep("40em", 3));
//   TextField firstName = new TextField();
//   firstName.setPlaceholder("First Name");
//   TextField lastName = new TextField();
//   lastName.setPlaceholder("Last Name");
//   TextField email = new TextField();
//   email.setPlaceholder("Email");
//   TextField nickname = new TextField();
//   nickname.setPlaceholder("Username");
//   TextField website = new TextField();
//   website.setPlaceholder("Link to personal website");
//   TextField description = new TextField();
//   description.setPlaceholder("Enter a short description about yourself");
//   columnLayout.add(firstName, lastName,  nickname, email, website); 
//   // You can set the desired column span for the components individually.
//   columnLayout.setColspan(website, 2);
//   // Or just set it as you add them.
//   columnLayout.add(description, 3);
                    return layout;
                }

                private VerticalLayout createDocumentForm() {
                    mainLayoutArchivo = new HorizontalLayout();
                    guardar.setVisible(false);
                    VerticalLayout layout = new VerticalLayout();

                    layout.addStyleName("crud-view");
                    layout.setMargin(true);
                    layout.setSpacing(true);

                    VerticalLayout layoutData = new VerticalLayout();

                    layoutData.addStyleName("crud-view");
                    layoutData.setMargin(true);
                    layoutData.setSpacing(true);

                    imageArchivo.setVisible(false);
                    uploadArchivo.setButtonCaption("Seleccionar");
                    uploadArchivo.addSucceededListener(receiverArchivo);

                    final FormLayout formArchivo = new FormLayout();

                    // Prevent too big downloads 0981752315
                    final long UPLOAD_LIMIT = 104857600l;
                    uploadArchivo.addStartedListener(new Upload.StartedListener() {
                        private static final long serialVersionUID = 4728847902678459488L;

                        @Override
                        public void uploadStarted(Upload.StartedEvent event) {
                            if (event.getContentLength() > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                uploadArchivo.interruptUpload();
                            }
                        }
                    });

                    // Check the size also during progress
                    uploadArchivo.addProgressListener(new Upload.ProgressListener() {
                        private static final long serialVersionUID = 8587352676703174995L;

                        @Override
                        public void updateProgress(long readBytes, long contentLength) {
                            if (readBytes > UPLOAD_LIMIT) {
                                Notification.show("El archivo es muy grande",
                                        Notification.Type.ERROR_MESSAGE);
                                uploadArchivo.interruptUpload();
                            }
                        }
                    });

                    btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    //form.addComponent(cbFuncionario);

//                    form.addComponent(fechaAsignada);
//                    form.addComponent(fechaForm);
//                    form.addComponent(txtCantDiaForm);
//                    form.addComponent(fechaHoraInicioForm);
//                    form.addComponent(fechaHoraFinForm);
//                    form.addComponent(txtTareaRealizar);
//                    form.addComponent(txtCanthsForm);
                    layoutData.addComponent(uploadArchivo);
                    layoutData.addComponent(labelUrlArchivo);
//                    layoutData.addComponent(btnAgregarDocumento);

                    btnAgregarDocumento.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    btnAgregarDocumento.setIcon(VaadinIcons.PLUS);

//                    formArchivo.addComponent(labelUrlArchivo);
                    mainLayoutArchivo.addComponent(gridDocumentacionLicencia);
//                    mainLayoutArchivo.addComponent(formArchivo);
                    mainLayoutArchivo.addComponent(layoutData);
//                    mainLayoutArchivo.setWidth("100%");

                    layout.addComponent(mainLayoutArchivo);

                    HorizontalLayout horizontalButton = new HorizontalLayout();
                    guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
                    horizontalButton.addComponent(guardar);
                    cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
                    horizontalButton.addComponent(cancelar);
                    horizontalButton.setSpacing(true);
                    layout.addComponent(horizontalButton);
                    return layout;
                }
            });

// Have some tabs
            String[] tabs = {"Licencias", "Documentos", "Detalles"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void editarSolicitudProduccion(Licencias solicitud) {
        List<LicenciasCompensar> listSolicitudDetalle = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        try {
            txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getNombrefuncionario().toUpperCase());
        } catch (Exception e) {
            txtNombreFuncionario.setValue("");
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
        } catch (Exception e) {
            txtArea.setValue("");
        } finally {
        }
        try {
            formulario.setValue(solicitud.getParametro());
            cargarPorFormulario(solicitud.getParametro());
        } catch (Exception e) {
            formulario.setValue(null);
        } finally {
        }
        try {
            listDocumentacion = documentoLicenciaDao.getListDocumentoLicenciaByLicencia(solicitud.getId());
            numRowSelectedArchivo = listDocumentacion.size();
            gridDocumentacionLicencia.clearSortOrder();
            gridDocumentacionLicencia.setItems(listDocumentacion);
        } catch (Exception e) {
        } finally {
        }

        /*try {
            if (solicitud.getCantdia() == null) {
                txtCantdias.setValue("0");
            } else {
                txtCantdias.setValue(solicitud.getCantdia() + "");
            }
        } catch (Exception e) {
            txtCantdias.setValue("0");
        } finally {
        }*/
        try {
            txtObservacion.setValue(solicitud.getMotivo());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }
        try {
            encargado.setValue(solicitud.getEncargado());
        } catch (Exception e) {
            encargado.setValue(null);
        } finally {
        }
        labelUrl.setValue("");
        motivo.setValue(null);
        txtDependencia.setVisible(false);
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);
        fechaHoraInicio.setVisible(false);

        fechaInicio.setValue(null);
        fechaFin.setValue(null);
        fechaHoraInicio.setValue(null);
        fechaHoraFin.setValue(null);

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);

        fechaHoraInicio.setVisible(false);
        fechaHoraFin.setVisible(false);
        txtDependencia.setVisible(false);

        fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
        upload.setVisible(false);
        txtOtroMotivo.setVisible(false);

        txtCedulaFuncionario.setVisible(false);
        txtNombreFuncionario.setVisible(false);
        txtArea.setVisible(false);
        txtCargo.setVisible(false);
        txtObservacion.setVisible(false);
        motivo.setVisible(false);
        gridLicenciaCompensar.clearSortOrder();
        mainLayout.setVisible(true);

        cbCargo.setItems(new ArrayList<>());
        cbCargo.setVisible(true);

        //cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        //cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));

        mainLayout.setVisible(true);
        cargarPorFormulario(formulario.getValue());
        cargarDetalleProduccion(solicitud);
        try {
            String fecha = formatFec.format(solicitud.getFechaini());
            String hora = formatHor.format(solicitud.getHoraini());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraInicio.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraInicio.setValue(null);
        } finally {
        }
        try {
            String fecha = formatFec.format(solicitud.getFechafin());
            String hora = formatHor.format(solicitud.getHorafin());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraFin.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraFin.setValue(null);
        } finally {
        }
    }

    private void cargarDetalleProduccion(Licencias solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<LicenciasCompensar> spd = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        listProduccionDetalle = spd;
        mapLicenciaCompensar = new HashMap<>();
        listProduccionDetalle.forEach((component) -> {
            mapLicenciaCompensar.put(formatFec.format(component.getFechacompensar()), component);
        });
        gridLicenciaCompensar.clearSortOrder();
        gridLicenciaCompensar.setItems(listProduccionDetalle);
    }

    public void editarRegistro(Licencias solicitud) {
        setCaption("Editar Licencia");
        this.solicitud = solicitud;
        editar = 1;
        editarSolicitudProduccion(solicitud);
        /* if (solicitud.getParametro().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            editar = 1;
            editarSolicitudProduccion(solicitud);
        } else {
            List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
            try {
                txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
            } catch (Exception e) {
                txtCedulaFuncionario.setValue(null);
            } finally {
            }
            try {
                txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
            } catch (Exception e) {
                txtDependencia.setValue("");
            } finally {
            }
            try {
                txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
            } catch (Exception e) {
                txtNombreFuncionario.setValue(null);
            } finally {
            }
            try {
                txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
            } catch (Exception e) {
                txtCargo.setValue(null);
            } finally {
            }
            try {
                txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
            } catch (Exception e) {
                txtArea.setValue(null);
            } finally {
            }
            try {
                formulario.setValue(solicitud.getFormulario());
                cargarPorFormulario(solicitud.getFormulario());
            } catch (Exception e) {
                formulario.setValue(null);
            } finally {
            }
            try {
                if (solicitud.getCantdia() == null) {
                    txtCantdias.setValue("0");
                } else {
                    txtCantdias.setValue(solicitud.getCantdia() + "");
                }
            } catch (Exception e) {
                txtCantdias.setValue("0");
            } finally {
            }
            try {
                txtObservacion.setValue(solicitud.getObservacion());
            } catch (Exception e) {
                txtObservacion.setValue(null);
            } finally {
            }
            try {
                encargado.setValue(solicitud.getEncargado());
            } catch (Exception e) {
                encargado.setValue(null);
            } finally {
            }
            try {
                txtOtroMotivo.setValue(listSolicitudDetalle.get(0).getDescripcion());
            } catch (Exception e) {
                txtOtroMotivo.setValue("");
            } finally {
            }
            try {
                labelUrl.setValue(listSolicitudDetalle.get(0).getUrlimagen());
            } catch (Exception e) {
                labelUrl.setValue("");
            } finally {
            }
            try {
                motivo.setValue(listSolicitudDetalle.get(0).getMotivo());
            } catch (Exception e) {
                motivo.setValue(null);
            } finally {
            }
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                txtDependencia.setVisible(true);
            } else {
                txtDependencia.setVisible(false);
            }
            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                fechaHoraInicio.setVisible(true);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);

                mainLayout.setVisible(false);
                try {
                    String fecha = formatFec.format(solicitud.getFechaini());
                    String hora = formatHor.format(solicitud.getHoraini());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtInicio = LocalDateTime.parse(fec);
                    fechaHoraInicio.setValue(ldtInicio);
                } catch (Exception e) {
                    fechaHoraInicio.setValue(null);
                } finally {
                }
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);
                txtDependencia.setVisible(false);
                fechaHoraInicio.setValue(null);

                mainLayout.setVisible(true);
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(false);
                fechaInicio.setVisible(true);
                fechaFin.setVisible(true);

                mainLayout.setVisible(false);
                try {
                    fechaInicio.setValue(DateUtils.asLocalDate(solicitud.getFechaini()));
                } catch (Exception e) {
                    fechaInicio.setValue(null);
                } finally {
                }
                try {
                    fechaFin.setValue(DateUtils.asLocalDate(solicitud.getFechafin()));
                } catch (Exception e) {
                    fechaFin.setValue(null);
                } finally {
                }
            }
        }*/
    }

    private boolean validate() {
        boolean savedEnabled = true;
        try {
            if (!formulario.getValue().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                if (txtCedulaFuncionario == null || txtCedulaFuncionario.isEmpty()) {
                    savedEnabled = false;
                }
                if (txtNombreFuncionario == null || txtNombreFuncionario.isEmpty()) {
                    savedEnabled = false;
                }
                if (formulario == null || formulario.getValue() == null) {
                    savedEnabled = false;
                }

            } else {
                if (txtCedulaFuncionario == null || txtCedulaFuncionario.isEmpty()) {
                    savedEnabled = false;
                }
                if (txtNombreFuncionario == null || txtNombreFuncionario.isEmpty()) {
                    savedEnabled = false;
                }
                if (formulario == null || formulario.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaHoraInicio == null || fechaHoraInicio.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaHoraFin == null || fechaHoraFin.getValue() == null) {
                    savedEnabled = false;
                }
                try {

                } catch (Exception e) {
                    if (fechaInicio == null || fechaInicio.getValue() == null) {
                        savedEnabled = false;
                    }
                    if (fechaFin == null || fechaFin.getValue() == null) {
                        savedEnabled = false;
                    }
                } finally {
                }
            }
        } catch (Exception e) {
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    private void updateParametro() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void cargarGrilla() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        if (mapLicenciaCompensar.containsKey(formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue())))) {
            Notification.show("Ya se ha agregado al detalle la fecha seleccionada.",
                    Notification.Type.ERROR_MESSAGE);
        } else {
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));

            long min = 0l;
            try {
                int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
                int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
                if (minFin > minInicio && inic.equalsIgnoreCase(finc)) {
                    min = (minFin - minInicio);
                } else if (!inic.equalsIgnoreCase(finc)) {
                    min = (minFin - minInicio);
                }
            } catch (Exception e) {
            } finally {
            }
            if (min == 0) {
                Notification.show("La hora fin debe ser mayor a la hora inicio y verificar que el/los dia/s no sean feriados o domigos.",
                        Notification.Type.ERROR_MESSAGE);
            } else if (validateForm() && inic.equalsIgnoreCase(finc)) {
                Date date1 = DateUtils.asDate(fechaHoraInicioForm.getValue());
                Date date2 = DateUtils.asDate(fechaHoraFinForm.getValue());
                date1.setHours(0);
                date1.setMinutes(0);
                date1.setSeconds(0);

                date2.setHours(0);
                date2.setMinutes(0);
                date2.setSeconds(0);
                long cantDias = (calcularCantDias(date1, date2));

                if (cantDias == 0) {
                    Notification.show("Verificar que el/los dia/s no sean feriados ni domingos",
                            Notification.Type.ERROR_MESSAGE);
                } else {
                    LicenciasCompensar spd = new LicenciasCompensar();
                    spd.setFechacompensar(DateUtils.asDate(fechaHoraFinForm.getValue()));
                    try {
                        spd.setHoraini(Timestamp.valueOf(fechaHoraInicioForm.getValue()));
                    } catch (Exception e) {
                        spd.setHoraini(null);
                    } finally {
                    }
                    try {
                        spd.setHorafin(Timestamp.valueOf(fechaHoraFinForm.getValue()));
                    } catch (Exception e) {
                        spd.setHorafin(null);
                    } finally {
                    }

                    spd.setCantidad(min);
                    spd.setObservacion(txtTareaRealizar.getValue());
                    spd.setLicencia(null);

                    mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
                    listProduccionDetalle.add(spd);
                    gridLicenciaCompensar.clearSortOrder();
                    gridLicenciaCompensar.setItems(listProduccionDetalle);
                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                    try {
                        Date date = new Date();
                        date.setDate(date.getDate() + 1);
                        String fecha = formatFec.format(date);
                        String fec = fecha + "T16:00";
                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                        fechaHoraInicioForm.setValue(ldtFin);
                        fechaHoraFinForm.setValue(ldtFin);
                    } catch (Exception e) {
                        fechaHoraInicioForm.setValue(null);
                        fechaHoraFinForm.setValue(null);
                    } finally {
                    }
                    txtTareaRealizar.setValue("");
                }
//            gridFuncionario.addComponentColumn(this::buildConfirmButton).setCaption(" X ");
            } else if (validateForm() && !inic.equalsIgnoreCase(finc)) {
                Calendar start = Calendar.getInstance();
                start.setTime(DateUtils.asDate(fechaHoraInicioForm.getValue()));

                Calendar end = Calendar.getInstance();
                end.setTime(DateUtils.asDate(fechaHoraFinForm.getValue()));
                while (!start.after(end)) {
                    boolean cumple = true;
                    Date targetDay = start.getTime();
                    if (isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                        cumple = false;
                    }
                    List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(targetDay, targetDay);
                    for (Feriado feriado : listFeriado) {
                        cumple = false;
                    }
                    if (cumple) {
                        LicenciasCompensar spd = new LicenciasCompensar();
                        spd.setFechacompensar(targetDay);
                        if (!mapLicenciaCompensar.containsKey(formatFecs.format(spd.getFechacompensar()))) {
                            try {
                                spd.setHoraini(Timestamp.valueOf(fechaHoraInicioForm.getValue()));
                            } catch (Exception e) {
                                spd.setHoraini(null);
                            } finally {
                            }
                            try {
                                spd.setHorafin(Timestamp.valueOf(fechaHoraFinForm.getValue()));
                            } catch (Exception e) {
                                spd.setHorafin(null);
                            } finally {
                            }

                            spd.setCantidad(min);
                            spd.setObservacion(txtTareaRealizar.getValue());
                            spd.setLicencia(null);

                            mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
                            listProduccionDetalle.add(spd);
                            gridLicenciaCompensar.clearSortOrder();
                            gridLicenciaCompensar.setItems(listProduccionDetalle);

                        }
                    }
                    start.add(Calendar.DATE, 1);
                }
                SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                try {
                    Date date = new Date();
                    date.setDate(date.getDate() + 1);
                    String fecha = formatFec.format(date);
                    String fec = fecha + "T16:00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraInicioForm.setValue(ldtFin);
                    fechaHoraFinForm.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraInicioForm.setValue(null);
                    fechaHoraFinForm.setValue(null);
                } finally {
                }
                txtTareaRealizar.setValue("");
            } else {
                Notification.show("Todos los campos son obligatorios",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void cargarFeriados(Date fecha) {
        try {
            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(fecha, fecha);
            for (Feriado feriado : listFeriado) {
                SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                mapeoFeriados.put(formatFecs.format(feriado.getFecha()), feriado);
            }
        } catch (Exception e) {
        } finally {
        }
    }

    private void cargarGrillaSinCompensar() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
        boolean val = false;

        try {
            try {
                Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaAsignada.getValue()), DateUtils.asDate(fechaAsignada.getValue()), func.getIdfuncionario());
                if (listRotacion.size() > 0) {
                    fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
                    val = true;

                    if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("cambio_rotacion") && val) {

                        if (isWeekendSaturday(fechaForm.getValue())) {
                            val = true;
                        } else {
                            Notification.show("La fecha debe ser de un dia sabado para cambiarla",
                                    Notification.Type.ERROR_MESSAGE);
                            val = false;
                        }
                    } else {
                        val = true;
                    }
                } else {
                    Notification.show("No existen rotaciones disponibles para cambiarlas",
                            Notification.Type.ERROR_MESSAGE);
                    val = false;
                }
            } catch (Exception ex) {
                val = true;
            } finally {
            }

            if (mapLicenciaCompensar.containsKey(formatFecs.format(DateUtils.asDate(fechaForm.getValue())))) {
                Notification.show("Ya se ha agregado al detalle la fecha seleccionada.",
                        Notification.Type.ERROR_MESSAGE);
            } else {
                if (val) {
                    if (validateFormSinCompensar()) {
                        String valData = "";
                        try {
                            valData = formulario.getValue().getLicenciadiacorrido().toUpperCase();
                        } catch (Exception e) {
                            valData = "";
                        } finally {
                        }

                        if (valData.equalsIgnoreCase("")) {
                            LicenciasCompensar spd = new LicenciasCompensar();
                            spd.setFechacompensar(DateUtils.asDate(fechaForm.getValue()));
                            spd.setObservacion(formulario.getValue().getDescripcion().toUpperCase());
                            listProduccionDetalle.add(spd);
                            gridLicenciaCompensar.clearSortOrder();
                            gridLicenciaCompensar.setItems(listProduccionDetalle);
                            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date date = new Date();
                                date.setDate(date.getDate() + 1);
                                String fecha = formatFec.format(date);
                                String fec = fecha + "T16:00";
                                LocalDateTime ldtFin = LocalDateTime.parse(fec);
                                fechaHoraInicioForm.setValue(ldtFin);
                                fechaHoraFinForm.setValue(ldtFin);
                            } catch (Exception e) {
                                fechaHoraInicioForm.setValue(null);
                                fechaHoraFinForm.setValue(null);
                            } finally {
                            }
                            txtTareaRealizar.setValue("");
                            mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
                        } else if (valData.equalsIgnoreCase("SI")) {//Dias corridos
                            try {
                                for (int i = 0; i < Integer.parseInt(formulario.getValue().getValor()); i++) {
                                    Date fechaComp = DateUtils.asDate(fechaForm.getValue());
                                    Date nuevaFecha = fechaComp;
                                    nuevaFecha = new Date(nuevaFecha.getTime() + TimeUnit.DAYS.toMillis(i));
                                    cargarFeriados(nuevaFecha);
                                    LicenciasCompensar spd = new LicenciasCompensar();
                                    spd.setFechacompensar(nuevaFecha);
                                    spd.setObservacion(formulario.getValue().getDescripcion().toUpperCase());
                                    listProduccionDetalle.add(spd);
                                    gridLicenciaCompensar.clearSortOrder();
                                    gridLicenciaCompensar.setItems(listProduccionDetalle);
                                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                                    try {
                                        Date date = new Date();
                                        date.setDate(date.getDate() + 1);
                                        String fecha = formatFec.format(date);
                                        String fec = fecha + "T16:00";
                                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                                        fechaHoraInicioForm.setValue(ldtFin);
                                        fechaHoraFinForm.setValue(ldtFin);
                                    } catch (Exception e) {
                                        fechaHoraInicioForm.setValue(null);
                                        fechaHoraFinForm.setValue(null);
                                    } finally {
                                    }
                                    txtTareaRealizar.setValue("");
                                    mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);

                                }
                            } catch (Exception e) {
                            } finally {
                            }
                        } else {
                            try {
                                int i = 0;
                                do {
                                    Date fechaComp = DateUtils.asDate(fechaForm.getValue());
                                    Date nuevaFecha = fechaComp;
                                    nuevaFecha = new Date(nuevaFecha.getTime() + TimeUnit.DAYS.toMillis(i));
                                    cargarFeriados(nuevaFecha);

                                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                                    i++;
                                    if (!isWeekendSunday(DateUtils.asLocalDate(nuevaFecha)) && !mapeoFeriados.containsKey(formatFec.format(nuevaFecha))) {
                                        LicenciasCompensar spd = new LicenciasCompensar();
                                        spd.setFechacompensar(nuevaFecha);
                                        spd.setObservacion(formulario.getValue().getDescripcion().toUpperCase());
                                        listProduccionDetalle.add(spd);
                                        gridLicenciaCompensar.clearSortOrder();
                                        gridLicenciaCompensar.setItems(listProduccionDetalle);

                                        try {
                                            Date date = new Date();
                                            date.setDate(date.getDate() + 1);
                                            String fecha = formatFec.format(date);
                                            String fec = fecha + "T16:00";
                                            LocalDateTime ldtFin = LocalDateTime.parse(fec);
                                            fechaHoraInicioForm.setValue(ldtFin);
                                            fechaHoraFinForm.setValue(ldtFin);
                                        } catch (Exception e) {
                                            fechaHoraInicioForm.setValue(null);
                                            fechaHoraFinForm.setValue(null);
                                        } finally {
                                        }
                                        txtTareaRealizar.setValue("");
                                        mapLicenciaCompensar.put(formatFecs.format(spd.getFechacompensar()), spd);
                                    }
                                } while (Integer.parseInt(formulario.getValue().getValor()) != mapLicenciaCompensar.size());
                            } catch (Exception e) {
                            } finally {
                            }
                        }

                    } else {
                        Notification.show("Todos los campos son obligatorios",
                                Notification.Type.ERROR_MESSAGE);
                    }
                }
            }
        } catch (Exception e) {
        } finally {
        }
    }

    private boolean validateForm() {
        boolean savedEnabled = true;
        if (fechaHoraFinForm == null || fechaHoraFinForm.isEmpty()) {
            savedEnabled = false;
        }
        if (fechaHoraInicioForm == null || fechaHoraInicioForm.getValue() == null) {
            savedEnabled = false;
        }
        if (txtCanthsForm == null || txtCanthsForm.getValue() == null) {
            savedEnabled = false;
        }
        if (txtTareaRealizar == null || txtTareaRealizar.getValue() == null || txtTareaRealizar.getValue().equals("")) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private boolean validateFormSinCompensar() {
        boolean savedEnabled = true;
        if (fechaForm == null || fechaForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtCantDiaForm == null || txtCantDiaForm.getValue() == null) {
            savedEnabled = false;
        }

        return savedEnabled;
    }

    public void setSaveListener(Consumer<Licencias> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Licencias> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Licencias> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void nuevoRegistro() {
        solicitud = new Licencias();
        solicitud.setFechacreacion(new Date());
    }

    private void guardarDetalleProduccion(Licencias solicitud) {
        if (editar == 0) {
            if (listProduccionDetalle.size() > 0) {
                for (LicenciasCompensar solicitudProduccionDetalle : listProduccionDetalle) {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                }
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            if (listProduccionDetalle.size() > 0) {
                licenciaCompensarDao.listarPorIdLicencia(solicitud.getId()).forEach((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.borrar(solicitudProduccionDetalle);
                });
                listProduccionDetalle.stream().map((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    return solicitudProduccionDetalle;
                }).forEachOrdered((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                });
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void cargarSeccion(Dependencia dependencia) {
        if (dependencia != null && cbDpto.getValue() != null) {
            cbCargo.clear();
            cbCargo.setItems(new ArrayList<>());
//            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbCargo.setVisible(true);

            //Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
            // List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            //listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            // //for (Dependencia dependencia1 : listDependencia) {
            //   listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            //}
            //cbFuncionario.setItems(listFunc);
            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        }
    }

    private void editarLicenciasProduccion(Licencias solicitud) {
        /*List<LicenciasProduccionDetalle> listLicenciasDetalle = solicitudProduccionDetalleDao.listarPorIdLicencias(solicitud.getId());
        try {
            txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
        } catch (Exception e) {
            txtDependencia.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
        } catch (Exception e) {
            txtNombreFuncionario.setValue("");
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
        } catch (Exception e) {
            txtArea.setValue("");
        } finally {
        }
        try {
            formulario.setValue(solicitud.getParametro());
            cargarPorParametro(solicitud.getParametro());
        } catch (Exception e) {
            formulario.setValue(null);
        } finally {
        }
        try {
            if (solicitud.getCantdia() == null) {
                txtCanhs.setValue("0");
            } else {
                txtCanhs.setValue(solicitud.getCantdia() + "");
            }
        } catch (Exception e) {
            txtCanhs.setValue("0");
        } finally {
        }
        try {
            txtObservacion.setValue(solicitud.getObservacion());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }
        try {
            encargado.setValue(solicitud.getEncargado());
        } catch (Exception e) {
            encargado.setValue(null);
        } finally {
        }
        try {
            txtOtroMotivo.setValue(listLicenciasDetalle.get(0).getDescripcion());
        } catch (Exception e) {
            txtOtroMotivo.setValue("");
        } finally {
        }
        labelUrl.setValue("");
        motivo.setValue(null);
        txtDependencia.setVisible(false);
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);
        fechaHoraInicio.setVisible(false);

        fechaInicio.setValue(null);
        fechaFin.setValue(null);
        fechaHoraInicio.setValue(null);
        fechaHoraFin.setValue(null);

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);

        fechaHoraInicio.setVisible(false);
        fechaHoraFin.setVisible(false);
        txtDependencia.setVisible(false);

        fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
        upload.setVisible(false);
        txtOtroMotivo.setVisible(false);

        txtCedulaFuncionario.setVisible(false);
        txtNombreFuncionario.setVisible(false);
        txtArea.setVisible(false);
        txtCargo.setVisible(false);
        txtObservacion.setVisible(false);
        motivo.setVisible(false);
        gridLicenciaCompensar.clearSortOrder();
        mainLayout.setVisible(true);
        txtCanhs.setVisible(false);

        cbCargo.setItems(new ArrayList<>());
        cbCargo.setVisible(true);

        cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);
        cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        cbCargo.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getCargofunc().toLowerCase()));

        mainLayout.setVisible(true);
        cargarDetalleProduccion(solicitud);
        try {
            String fecha = formatFec.format(solicitud.getFechafin());
            String hora = formatHor.format(solicitud.getHorafin());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraFin.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraFin.setValue(null);
        } finally {
        }*/
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    private long calcularCantDias() {
        try {
            long substract = calcWeekDays(convertToDateViaInstant(fechaInicio.getValue()), convertToDateViaInstant(fechaFin.getValue())) + 1;
            if (isWeekendSunday(fechaFin.getValue())) {
                substract -= 1l;
            }

            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()));
            long cantFeriado = 0;
            for (Feriado feriado : listFeriado) {
                if (!isWeekendSunday(DateUtils.asLocalDate(feriado.getFecha()))) {
                    cantFeriado++;
                }
            }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
            return (substract - cantFeriado);
        } catch (Exception e) {
            return 0;
        } finally {
        }
    }

    private long calcularCantDiasDif() {
        try {
            long substract = calcWeekDays(convertToDateViaInstant(DateUtils.asLocalDate(DateUtils.asDate(fechaHoraInicio.getValue()))), convertToDateViaInstant(DateUtils.asLocalDate(DateUtils.asDate(fechaHoraFin.getValue())))) + 1;
            if (isWeekendSunday(DateUtils.asLocalDate(DateUtils.asDate(fechaHoraFin.getValue())))) {
                substract -= 1l;
            }

            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fechaHoraInicio.getValue()), DateUtils.asDate(fechaHoraFin.getValue()));
            long cantFeriado = 0;
            for (Feriado feriado : listFeriado) {
                if (!isWeekendSunday(DateUtils.asLocalDate(feriado.getFecha()))) {
                    cantFeriado++;
                }
            }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
            return (substract - cantFeriado);
        } catch (Exception e) {
            return 0;
        } finally {
        }
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreFuncionario.setValue("");
                txtArea.setValue("");
                txtCargo.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateFecha() {
        if (fechaInicio.getValue() != null && fechaFin.getValue() != null) {
            if (fechaInicio.getValue().isBefore(fechaFin.getValue()) || fechaInicio.getValue().isEqual(fechaFin.getValue())) {
                if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCanhs.setValue(dif + "");
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                } else {
                    txtCanhs.setValue(calcularCantDias() + "");
                }
            }
        }
    }

    private void updateFormulario() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void save() {
        long min = 0l;
        if (validate()) {
            if (formulario.getValue().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                solicitud.setEncargado(null);
                try {
                    solicitud.setParametro(formulario.getValue());
                    solicitud.setDescripcion(formulario.getValue().getDescripcion());
                } catch (Exception e) {
                    solicitud.setParametro(null);
                    solicitud.setDescripcion("");
                } finally {
                }
                try {
                    solicitud.setCargofunc(txtCargo.getValue());
                } catch (Exception e) {
                    solicitud.setCargofunc("");
                } finally {
                }
                try {
                    solicitud.setAreafunc(txtArea.getValue());
                } catch (Exception e) {
                    solicitud.setAreafunc("");
                } finally {
                }
                try {
                    solicitud.setFuncionario(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()));
                    solicitud.setNombrefuncionario(txtNombreFuncionario.getValue());
                } catch (Exception e) {
                    solicitud.setAreafunc("");
                } finally {
                }

                try {

                    Date date1 = DateUtils.asDate(fechaHoraInicio.getValue());
                    Date date2 = DateUtils.asDate(fechaHoraFin.getValue());
                    date1.setHours(0);
                    date1.setMinutes(0);
                    date1.setSeconds(0);

                    date2.setHours(0);
                    date2.setMinutes(0);
                    date2.setSeconds(0);
                    long cantDias = (calcularCantDias(date1, date2));

                    int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
                    int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
                    if (minFin > minInicio) {
                        min = (minFin - minInicio) * cantDias;
                    }
                } catch (Exception e) {
                } finally {
                }
                solicitud.setCantmin(min);
                try {
                    solicitud.setHoraini(Timestamp.valueOf(fechaHoraInicio.getValue()));
                } catch (Exception e) {
                    solicitud.setHoraini(null);
                } finally {
                }
                try {
                    solicitud.setHorafin(Timestamp.valueOf(fechaHoraFin.getValue()));
                } catch (Exception e) {
                    solicitud.setHorafin(null);
                } finally {
                }
                try {
                    solicitud.setMotivo(txtObservacion.getValue());
                } catch (Exception e) {
                    solicitud.setMotivo("");
                } finally {
                }

                if (verificarHsmin() > 0) {
                    if (listProduccionDetalle.size() > 0) {
                        int minHere = 0;
                        for (LicenciasCompensar licenciasCompensar : listProduccionDetalle) {
                            try {
                                int minInicio = ((licenciasCompensar.getHoraini().toLocalDateTime().getHour() * 60) + licenciasCompensar.getHoraini().toLocalDateTime().getMinute());
                                int minFin = ((licenciasCompensar.getHorafin().toLocalDateTime().getHour() * 60) + licenciasCompensar.getHorafin().toLocalDateTime().getMinute());
                                minHere += (minFin - minInicio);
                            } catch (Exception e) {
                            } finally {
                            }
                        }
                        if (min == minHere) {
                            solicitud.setAprobado(0L);
                            if (formulario.getValue().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                                solicitud.setFechaini(Timestamp.valueOf(fechaHoraInicio.getValue()));
                                solicitud.setFechafin(Timestamp.valueOf(fechaHoraFin.getValue()));
                            } else {
                                solicitud.setFechaini(listProduccionDetalle.get(0).getFechacompensar());
                                solicitud.setFechafin(listProduccionDetalle.get(0).getFechacompensar());
                            }
                            solicitud = solicitudDao.guardarLicencias(solicitud);
                            guardarDetalleProduccion(solicitud);
                            guardarDocumentosAdjuntos(solicitud);
                            setVisible(false);
                            saveListener.accept(solicitud);
                            Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            Notification.show("Completar la hora a compensar, debe ser igual a lo especificado.",
                                    Notification.Type.ERROR_MESSAGE);
                        }
                    } else {
                        Notification.show("Debe agregar detalles a la licencia.",
                                Notification.Type.ERROR_MESSAGE);
                    }
                } else {
                    Notification.show("Cantidad hs a compensar debe ser mayor a cero",
                            Notification.Type.ERROR_MESSAGE);
                }
            } else {
                try {
                    try {
                        solicitud.setParametro(formulario.getValue());
                        solicitud.setDescripcion(formulario.getValue().getDescripcion());
                    } catch (Exception e) {
                        solicitud.setParametro(null);
                        solicitud.setDescripcion("");
                    } finally {
                    }
                    try {
                        solicitud.setCargofunc(txtCargo.getValue());
                    } catch (Exception e) {
                        solicitud.setCargofunc("");
                    } finally {
                    }
                    try {
                        solicitud.setAreafunc(txtArea.getValue());
                    } catch (Exception e) {
                        solicitud.setAreafunc("");
                    } finally {
                    }
                    try {
                        solicitud.setFuncionario(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()));
                        solicitud.setNombrefuncionario(txtNombreFuncionario.getValue());
                    } catch (Exception e) {
                        solicitud.setAreafunc("");
                    } finally {
                    }
                    try {
                        solicitud.setEncargado(encargado.getValue());
                    } catch (Exception e) {
                        solicitud.setEncargado(null);
                    } finally {
                    }
                    try {
                        if (solicitud.getId() == null) {
                            solicitud.setAprobado(0l);
                        }
//                solicitud.setFuncrrhh(rrhh.getValue());
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        solicitud.setMotivo(txtObservacion.getValue());
                    } catch (Exception e) {
                        solicitud.setMotivo("");
                    } finally {
                    }
                    if (listProduccionDetalle.size() > 0) {
                        Date minFecha = null;
                        Date maxFecha = null;
                        for (LicenciasCompensar licenciasCompensar : listProduccionDetalle) {
                            if (minFecha == null) {
                                minFecha = licenciasCompensar.getFechacompensar();
                                maxFecha = licenciasCompensar.getFechacompensar();
                            } else {
                                if (!minFecha.before(licenciasCompensar.getFechacompensar())) {
                                    minFecha = licenciasCompensar.getFechacompensar();
                                }
                                if (!maxFecha.after(licenciasCompensar.getFechacompensar())) {
                                    maxFecha = licenciasCompensar.getFechacompensar();
                                }
                            }
                        }
                        if (txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE") || Long.parseLong(txtCantDiaForm.getValue().trim()) == listProduccionDetalle.size()) {
                            solicitud.setFechaini(minFecha);
                            solicitud.setFechafin(maxFecha);
                            if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                                solicitud.setFechaini(DateUtils.asDate(fechaAsignada.getValue()));
                            }
                            solicitud = solicitudDao.guardarLicencias(solicitud);
                            guardarDetalle(solicitud);
                            guardarDocumentosAdjuntos(solicitud);
                            setVisible(false);
                            saveListener.accept(solicitud);
                            Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            Notification.show("La cantidad de dias debes ser igual a lo asignado en el detalle.",
                                    Notification.Type.ERROR_MESSAGE);
                        }
                    } else {
                        Notification.show("Debe agregar detalles a la licencia.",
                                Notification.Type.ERROR_MESSAGE);
                    }
                } catch (Exception ex) {
                    Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
                    saveListener.accept(solicitud);
                    Logger.getLogger(FuncionarioForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            Notification.show("Atención", "Completar los campos obligatorios", Notification.Type.ERROR_MESSAGE);
        }
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    private void verificarLicenciaAnual() {
        //Faltaria estudio del prostata
        try {
            if (formulario.getValue().getCodigo().trim().equalsIgnoreCase("estudio_pap") && this.solicitud.getId() == null) {
                if (!txtNombreFuncionario.getValue().equals("")) {
                    Calendar cal = Calendar.getInstance();

                    cal.set(Calendar.DAY_OF_YEAR, 1);
                    Date yearStartDate = cal.getTime();

                    cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
                    Date yearEndDate = cal.getTime();
                    List<Licencias> listLicencias = solicitudDao.listarPorFechasUsoAnual(yearStartDate, yearEndDate, funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
                    if (listLicencias.isEmpty() || listLicencias.size() < 2) {
                        btnAgregar.setEnabled(true);
                        guardar.setEnabled(true);
                    } else {
                        Notification.show("Atención", "El/la funcionario/a seleccionado ya utilizó la licencia disponible para este año, consulte al administrador.", Notification.Type.ERROR_MESSAGE);
                        btnAgregar.setEnabled(false);
                        guardar.setEnabled(false);
                    }
                }
            } else {
                btnAgregar.setEnabled(true);
                guardar.setEnabled(true);
            }
        } catch (Exception e) {
            btnAgregar.setEnabled(true);
            guardar.setEnabled(true);
        } finally {
        }
    }

    private void cargarPorFormulario(Parametro value) {
        mapLicenciaCompensar = new HashMap<>();
        btnAgregar.setEnabled(true);
        guardar.setEnabled(true);
        if (value.getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
            fechaInicio.setVisible(true);
            fechaFin.setVisible(false);

            fechaInicio.setValue(null);
            fechaFin.setValue(null);

            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = new Date();
                date.setDate(date.getDate() + 1);
                String fecha = formatFecs.format(date);
                String fec = fecha + "T" + "16:00:00";
                LocalDateTime ldtFins = LocalDateTime.parse(fec);
                fechaHoraInicio.setValue(ldtFins);
                fechaHoraFin.setValue(ldtFins);
                txtCanhs.setValue("0");
            } catch (Exception e) {
                fechaInicio.setValue(null);
                fechaHoraFin.setValue(null);
                txtCanhs.setValue("0");
            } finally {
            }

            try {
                Date date = new Date();
                date.setDate(date.getDate() + 1);
                String fecha = formatFecs.format(date);
                String fec = fecha + "T16:00";
                LocalDateTime ldtFin = LocalDateTime.parse(fec);
                fechaHoraInicioForm.setValue(ldtFin);
                fechaHoraFinForm.setValue(ldtFin);
            } catch (Exception e) {
                fechaHoraInicioForm.setValue(null);
                fechaHoraFinForm.setValue(null);
            } finally {
            }

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(true);
            fechaHoraFin.setVisible(true);
            txtDependencia.setVisible(false);

            fechaHoraFin.setCaption("Fecha Fin (*)");
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);
            fechaAsignada.setVisible(false);

            txtCedulaFuncionario.setVisible(true);
            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);
            txtCanhs.setVisible(true);
            fechaHoraInicioForm.setVisible(true);
            fechaHoraFinForm.setVisible(true);
            txtCanthsForm.setVisible(true);
            txtTareaRealizar.setVisible(true);

//            cbDpto.setItems(dptoDao.listarDepartamentosPadres());
            cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbDpto.setVisible(false);

            cbCargo.setItems(new ArrayList<>());
            cbCargo.setVisible(false);

            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            try {
                Date date = new Date();
                date.setDate(date.getDate() + 1);
                String fecha = formatFec.format(date);
                String fec = fecha + "T" + "19:00:00";
                LocalDateTime ldtFin = LocalDateTime.parse(fec);
                fechaHoraForm.setValue(ldtFin);
            } catch (Exception e) {
                fechaHoraForm.setValue(null);
            } finally {
            }

            if (value.getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                fechaHoraForm.setVisible(true);
                fechaInicio.setVisible(false);
            } else {
                fechaHoraForm.setVisible(false);
                fechaInicio.setVisible(true);
            }
            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHoraini());
            }).setCaption("Desde hs");
            gridLicenciaCompensar.addColumn(e -> {
                return formatHora.format(e.getHorafin());
            }).setCaption("Hasta hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getCantHsmin();
            }).setCaption("Cant hs");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Tarea a realizar");
            gridLicenciaCompensar.addComponentColumn(this::buildRemoveData).setCaption("");

            fechaForm.setVisible(false);
            txtCantDiaForm.setVisible(false);
            fechaForm.setValue(null);
            txtCantDiaForm.setValue("");
        } else if (value.getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
            fechaInicio.setValue(null);
            fechaFin.setValue(null);
            fechaHoraInicio.setValue(null);
            fechaHoraFin.setValue(null);

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);
            txtCanhs.setValue("");
            txtCanhs.setVisible(false);
            txtDependencia.setVisible(false);
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);

            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);
            fechaAsignada.setVisible(true);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            fechaForm.setVisible(true);
            txtCantDiaForm.setVisible(true);
            Date date = new Date();
            date.setDate(date.getDate() + 1);
            fechaForm.setValue(DateUtils.asLocalDate(date));
            txtCantDiaForm.setValue("1");
            fechaHoraInicioForm.setVisible(false);
            fechaHoraFinForm.setVisible(false);
            txtTareaRealizar.setVisible(false);
            txtCanthsForm.setVisible(false);

            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Licencia");
            gridLicenciaCompensar.addComponentColumn(this::buildRemoveData).setCaption("");
            List<Rotaciones> listRotacion = rotacionDao.listarPorIdFuncionarioFecha(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
            if (listRotacion.size() > 0) {
                fechaAsignada.setValue(DateUtils.asLocalDate(listRotacion.get(0).getFecha()));
            } else {
                Notification.show("No existen rotaciones disponibles para cambiarlas",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            verificarLicenciaAnual();
            fechaInicio.setValue(null);
            fechaFin.setValue(null);
            fechaHoraInicio.setValue(null);
            fechaHoraFin.setValue(null);

            fechaInicio.setVisible(false);
            fechaFin.setVisible(false);

            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);
            txtCanhs.setValue("");
            txtCanhs.setVisible(false);
            txtDependencia.setVisible(false);
            upload.setVisible(false);
            txtOtroMotivo.setVisible(false);
            fechaAsignada.setVisible(false);

            txtNombreFuncionario.setVisible(true);
            txtArea.setVisible(true);
            txtCargo.setVisible(true);
            txtObservacion.setVisible(true);
            motivo.setVisible(false);
            gridLicenciaCompensar.clearSortOrder();
            mainLayout.setVisible(true);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            fechaForm.setVisible(true);
            txtCantDiaForm.setVisible(true);
            Date date = new Date();
            date.setDate(date.getDate() + 1);
            fechaForm.setValue(DateUtils.asLocalDate(date));
            try {
                if (!formulario.getValue().getValor().equalsIgnoreCase("")) {
                    txtCantDiaForm.setValue(formulario.getValue().getValor());
                } else {
                    txtCantDiaForm.setValue("SIN LIMITE");
                }
            } catch (Exception e) {
                txtCantDiaForm.setValue("SIN LIMITE");
            } finally {
            }
            fechaHoraInicioForm.setVisible(false);
            fechaHoraFinForm.setVisible(false);
            txtTareaRealizar.setVisible(false);
            txtCanthsForm.setVisible(false);

            listProduccionDetalle = new ArrayList<>();
            SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
            gridLicenciaCompensar.clearSortOrder();
            gridLicenciaCompensar.setCaption("Hora a compensar");
            gridLicenciaCompensar.setItems(listProduccionDetalle);
            gridLicenciaCompensar.removeAllColumns();
            gridLicenciaCompensar.addColumn(e -> {
                return formatFecs.format(e.getFechacompensar());
            }).setCaption("Fecha");
            gridLicenciaCompensar.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Licencia");
            gridLicenciaCompensar.addComponentColumn(this::buildRemoveData).setCaption("");
        }
    }

    private void guardarDetalle(Licencias solicitud) {
        /*LicenciasCompensar detalle = new LicenciasCompensar();
        List<LicenciasCompensar> listSolicitudDetalle = licenciaCompensarDao.listarPorIdLicencia(solicitud.getId());
        if (listSolicitudDetalle.size() > 0) {
            detalle = listSolicitudDetalle.get(0);
        }
        try {
            detalle.setObservacion(txtOtroMotivo.getValue());
        } catch (Exception e) {
            detalle.setObservacion(null);
        } finally {
        }
        try {
            detalle.setLicencia(solicitud);
        } catch (Exception e) {
            detalle.setLicencia(null);
        } finally {
        }

        licenciaCompensarDao.guardarLicenciasCompensar(detalle);*/
        if (editar == 0) {
            if (listProduccionDetalle.size() > 0) {
                for (LicenciasCompensar solicitudProduccionDetalle : listProduccionDetalle) {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                }
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            if (listProduccionDetalle.size() > 0) {
                licenciaCompensarDao.listarPorIdLicencia(solicitud.getId()).forEach((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.borrar(solicitudProduccionDetalle);
                });
                listProduccionDetalle.stream().map((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalle.setLicencia(solicitud);
                    return solicitudProduccionDetalle;
                }).forEachOrdered((solicitudProduccionDetalle) -> {
                    licenciaCompensarDao.guardarLicenciasCompensar(solicitudProduccionDetalle);
                });
            } else {
                Notification.show("Debe agregar detalles a la licencia",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        int workDays = 0;
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }

        do {
            //excluding start date
            startCal.add(Calendar.DAY_OF_MONTH, 1);
            ++workDays;
        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }

    private long calcularCantDias(Date d1, Date d2) {
        long substract = 0;
        Calendar start = Calendar.getInstance();
        start.setTime(d1);

        Calendar end = Calendar.getInstance();
        end.setTime(d2);
        long diaDomingo = 0;
        long diaWork = 0;
        while (!start.after(end)) {
            Date targetDay = start.getTime();
            if (isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                diaDomingo++;
            }
            diaWork++;
            start.add(Calendar.DATE, 1);
        }
        substract = diaWork - diaDomingo;

        List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(d1, d2);
        long cantFeriado = 0;
        for (Feriado feriado : listFeriado) {
            if (!isWeekend(DateUtils.asLocalDate(feriado.getFecha()))) {
                cantFeriado++;
            }
        }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
        return (substract - cantFeriado) > 0 ? (substract - cantFeriado) : 0;
    }

    private void updateFechaHora() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        minutoDiferenciado = 0;
        guardar.setVisible(true);

        boolean val = true;
        try {
            if (!mostrarFechaAnterior) {
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                Date fechaSolicitud = DateUtils.asDate(fechaHoraInicio.getValue());
                fechaSolicitud.setHours(0);
                fechaSolicitud.setMinutes(0);
                fechaSolicitud.setSeconds(0);
                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));

                if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                    val = false;
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    guardar.setVisible(false);
                } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                    guardar.setVisible(false);
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    val = false;
                } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                    val = true;
                }
            }
        } catch (Exception e) {
        } finally {
        }

        if (val) {
            try {
                int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
                int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());

                Date date1 = DateUtils.asDate(fechaHoraInicio.getValue());
                Date date2 = DateUtils.asDate(fechaHoraFin.getValue());
                date1.setHours(0);
                date1.setMinutes(0);
                date1.setSeconds(0);

                date2.setHours(0);
                date2.setMinutes(0);
                date2.setSeconds(0);
                long cantDias = (calcularCantDias(date1, date2));

                String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicio.getValue()));
                String finc = formatFecs.format(DateUtils.asDate(fechaHoraFin.getValue()));
                if (minFin > minInicio) {
                    long millis = (minFin - minInicio);
                    millis *= cantDias;
                    try {
                        minutoDiferenciado = millis;
                        txtCanhs.setValue(sdfHM.format(sdf.parse(millis + "")));
                    } catch (ParseException ex) {
                        txtCanhs.setValue(0 + "");
                    } finally {
                    }
                } else {
                    txtCanhs.setValue(0 + "");
                }
            } catch (Exception e) {
                txtCanhs.setValue(0 + "");
            } finally {
            }
        }
    }

    private void updateFechaHoraForm() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        try {

            Date date1 = DateUtils.asDate(fechaHoraInicioForm.getValue());
            Date date2 = DateUtils.asDate(fechaHoraFinForm.getValue());
            date1.setHours(0);
            date1.setMinutes(0);
            date1.setSeconds(0);

            date2.setHours(0);
            date2.setMinutes(0);
            date2.setSeconds(0);
            long cantDias = (calcularCantDias(date1, date2));

            int minFin = ((fechaHoraFinForm.getValue().getHour() * 60) + fechaHoraFinForm.getValue().getMinute());
            int minInicio = ((fechaHoraInicioForm.getValue().getHour() * 60) + fechaHoraInicioForm.getValue().getMinute());
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicioForm.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFinForm.getValue()));
            if (minFin > minInicio) {
                long millis = (minFin - minInicio) * cantDias;
                try {
                    txtCanthsForm.setValue(sdfHM.format(sdf.parse(millis + "")));
                } catch (ParseException ex) {
                    txtCanthsForm.setValue(0 + "");
                }
            } else {
                txtCanthsForm.setValue(0 + "");
            }
        } catch (Exception e) {
            txtCanthsForm.setValue(0 + "");
        } finally {
        }

    }

    private void downloadDocument(DocumentoLicencia p, Button button) {
        FileDownloader fld = new FileDownloader(new ClassResource(p.getUrl()));
        fld.extend(button);
    }

    private long verificarHsmin() {
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        long min = 0l;
        try {
            Date date1 = DateUtils.asDate(fechaHoraInicio.getValue());
            Date date2 = DateUtils.asDate(fechaHoraFin.getValue());
            date1.setHours(0);
            date1.setMinutes(0);
            date1.setSeconds(0);

            date2.setHours(0);
            date2.setMinutes(0);
            date2.setSeconds(0);
            long cantDias = (calcularCantDias(date1, date2));

            int minFin = ((fechaHoraFin.getValue().getHour() * 60) + fechaHoraFin.getValue().getMinute());
            int minInicio = ((fechaHoraInicio.getValue().getHour() * 60) + fechaHoraInicio.getValue().getMinute());
            String inic = formatFecs.format(DateUtils.asDate(fechaHoraInicio.getValue()));
            String finc = formatFecs.format(DateUtils.asDate(fechaHoraFin.getValue()));
            if (minFin > minInicio) {
                min = (minFin - minInicio) * cantDias;
            }
        } catch (Exception e) {
        } finally {
        }
        return min;
    }

    private Button buildConfirmButton(DocumentoLicencia p) {
        Button button = new Button(VaadinIcons.DOWNLOAD);
        if (p.getEstado() == null || p.getEstado()) {
            button.setEnabled(true);
            button.setVisible(true);
        } else {
            button.setEnabled(false);
            button.setVisible(false);
        }
        /*if (p.getAprobado() == 0) {*/

 /*} else {
            button.setEnabled(false);
            button.setVisible(false);
        }*/
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        descargarArchivo(new File(Constants.UPLOAD_DIR_UBI + p.getUrl()), button);
        return button;
    }

    private Button buildRemove(DocumentoLicencia p) {
        Button button = new Button(VaadinIcons.ERASER);
//        if (p.getEstado() == null || p.getEstado()) {
//            button.setEnabled(true);
//            button.setVisible(true);
//        } else {
//            button.setEnabled(false);
//            button.setVisible(false);
//        }
        /*if (p.getAprobado() == 0) {*/

 /*} else {
            button.setEnabled(false);
            button.setVisible(false);
        }*/
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        button.addClickListener(e -> eliminarDocumento(p));
        return button;
    }

    private Button buildRemoveData(LicenciasCompensar p) {
        Button button = new Button(VaadinIcons.ERASER);
//        if (p.getEstado() == null || p.getEstado()) {
//            button.setEnabled(true);
//            button.setVisible(true);
//        } else {
//            button.setEnabled(false);
//            button.setVisible(false);
//        }
        /*if (p.getAprobado() == 0) {*/

 /*} else {
            button.setEnabled(false);
            button.setVisible(false);
        }*/
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        button.addClickListener(e -> eliminarData(p));
        return button;
    }

    private void guardarDocumentosAdjuntos(Licencias solicitud) {
        try {
            for (DocumentoLicencia dl : documentoLicenciaDao.getListDocumentoLicenciaByLicencia(solicitud.getId())) {
                documentoLicenciaDao.borrar(dl);
            }
        } catch (Exception e) {
        } finally {
        }

        for (DocumentoLicencia dl : listDocumentacion) {
            dl.setEstado(true);
            dl.setLicencia(solicitud);

            documentoLicenciaDao.guardar(dl);
        }
    }

    private void descargarArchivo(File file, Button button) {
        myResource = createResource(file);
        FileDownloader fileDownloader = new FileDownloader(myResource);
        fileDownloader.extend(button);
    }

    private void eliminarDocumento(DocumentoLicencia p) {
        try {
            if (numRowSelectedArchivo >= 0) {
                listDocumentacion.remove(numRowSelectedArchivo);
                gridDocumentacionLicencia.clearSortOrder();
                gridDocumentacionLicencia.setItems(listDocumentacion);
//                            mapLicenciaCompensar = new HashMap<>();

                numRowSelectedArchivo = -1;
                spdArchivo = new DocumentoLicencia();
            }
        } catch (Exception e) {
        } finally {
        }
    }

    private void eliminarData(LicenciasCompensar p) {
        try {
            if (!txtCantDiaForm.getValue().trim().equalsIgnoreCase("SIN LIMITE") && !formulario.getValue().getCodigo().trim().equalsIgnoreCase("licencia_compensar")) {
                listProduccionDetalle = new ArrayList<>();
                gridLicenciaCompensar.clearSortOrder();
                gridLicenciaCompensar.setItems(listProduccionDetalle);
                mapLicenciaCompensar = new HashMap<>();

                numRowSelected = 0;
                spd = new LicenciasCompensar();
            } else {
                if (numRowSelected >= 0) {
                    listProduccionDetalle.remove(numRowSelected);
                    gridLicenciaCompensar.clearSortOrder();
                    gridLicenciaCompensar.setItems(listProduccionDetalle);
                    mapLicenciaCompensar = new HashMap<>();
                    SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                    listProduccionDetalle.forEach((component) -> {
                        mapLicenciaCompensar.put(formatFecs.format(component.getFechacompensar()), component);
                    });
                    numRowSelected = -1;
                    spd = new LicenciasCompensar();
                }
            }
        } catch (Exception e) {
            if (numRowSelected >= 0) {
                listProduccionDetalle.remove(numRowSelected);
                gridLicenciaCompensar.clearSortOrder();
                gridLicenciaCompensar.setItems(listProduccionDetalle);
                mapLicenciaCompensar = new HashMap<>();
                SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
                listProduccionDetalle.forEach((component) -> {
                    mapLicenciaCompensar.put(formatFecs.format(component.getFechacompensar()), component);
                });
                numRowSelected = -1;
                spd = new LicenciasCompensar();
            }
        } finally {
        }
    }

    private boolean validarHorarios() {
        boolean val = true;
        try {
            if (!mostrarFechaAnterior) {
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                Date fechaSolicitud = DateUtils.asDate(fechaHoraInicio.getValue());
                fechaSolicitud.setHours(0);
                fechaSolicitud.setMinutes(0);
                fechaSolicitud.setSeconds(0);
                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));

                if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                    val = false;
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    guardar.setVisible(false);
                } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                    guardar.setVisible(false);
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    val = false;
                } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                    val = true;
                    guardar.setVisible(true);
                }
            }
        } catch (Exception e) {
        } finally {
        }
        return val;
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web-rrhh/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(LicenciasForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            imageArchivo.setVisible(true);
            imageArchivo.setSource(new FileResource(file));

            DocumentoLicencia spd = new DocumentoLicencia();
            spd.setUrl(file.getName());
//            spd.setFechacompensar(nuevaFecha);
//            spd.setObservacion(formulario.getValue().getDescripcion().toUpperCase());
            listDocumentacion.add(spd);
            gridDocumentacionLicencia.clearSortOrder();
            gridDocumentacionLicencia.setItems(listDocumentacion);
//            labelUrlArchivo.setValue(file.getName());
        }
    };

    private StreamResource createResource(File file) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, file.getName());
    }
}
