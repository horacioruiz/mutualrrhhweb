/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.ToleranciasDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.entities.Tolerancias;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSunday;

/**
 *
 * @author Dbarreto
 */
public class ToleranciaForm extends FormLayout {

    private DateField fecha = new DateField("Fecha Desde");
    private DateField fechaHasta = new DateField("Fecha Hasta");
    ComboBox<Parametro> filterParametro = new ComboBox<>("Tolerancia");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    CheckBox checkForm = new CheckBox("Aplicar a todos");

    List<Marcaciones> listMarcation = new ArrayList<>();

    ToleranciasDao toleranciaDao = ResourceLocator.locate(ToleranciasDao.class);
    ParametroDao parametroDao = ResourceLocator.locate(ParametroDao.class);
    Tolerancias tolerancia;

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    MarcacionesDao marcacionDao = ResourceLocator.locate(MarcacionesDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>("Funcionario");
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);

    private Consumer<Tolerancias> guardarListener;
    private Consumer<Tolerancias> borrarListener;
    private Consumer<Tolerancias> cancelarListener;

    private Date viejo = new Date();
    private Funcionario func = new Funcionario();
    private String param = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "tolerancia";
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public ToleranciaForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            filterFuncionario.setPlaceholder("Seleccione funcionario");
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                filterFuncionario.setItems(funcionarioDao.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioDao.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            checkForm.setValue(false);
            checkForm.addValueChangeListener(e -> updateFormulario(e.getValue()));

            fecha.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
            filterParametro.setPlaceholder("Seleccione motivo");
            filterParametro.setItems(parametroDao.listarPorTipoCodigo("tolerancia"));
            filterParametro.setItemCaptionGenerator(Parametro::getDescripcion);

            addComponents(fecha, fechaHasta, filterFuncionario, checkForm, filterParametro, botones);

//            binder.forField(txtfDescripcion).withNullRepresentation("")
//                    .bind(Tolerancias::getDescripcion, Tolerancias::setDescripcion);
//            binder.bindInstanceFields(this);
            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fecha.getValue()), DateUtils.asDate(fecha.getValue()), UserHolder.get().getIdfuncionario().getIdfuncionario());
                    if (listSuspencion.size() > 0) {
                        Notification.show("Mensaje del Sistema", "No se crean tolerancias en días de suspenciones.", Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        if (fecha.getValue().isBefore(fechaHasta.getValue()) || fecha.getValue().isEqual(fechaHasta.getValue())) {
                            if (checkForm.getValue()) {
                                guardarAtodos();
                            } else {
                                guardar();
                            }
                        } else {
                            Notification.show("Atención", "Fecha Desde debe ser menor o igual a Fecha Hasta", Notification.Type.ERROR_MESSAGE);
                        }

                    }
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(tolerancia);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        Calendar start = Calendar.getInstance();
        start.setTime(DateUtils.asDate(fecha.getValue()));

        Calendar end = Calendar.getInstance();
        end.setTime(DateUtils.asDate(fechaHasta.getValue()));

        boolean value = false;
        while (!start.after(end)) {
            Date targetDay = start.getTime();

            try {
                try {
                    tolerancia.setFecha(targetDay);
                } catch (Exception ex) {
                    tolerancia.setFecha(null);
                } finally {
                }
                try {
                    tolerancia.setParametro(filterParametro.getValue());
                } catch (Exception ex) {
                    tolerancia.setParametro(null);
                } finally {
                }
                tolerancia.setFuncionario(filterFuncionario.getValue());
                listMarcation = marcacionDao.getByFilter(tolerancia.getFuncionario(), tolerancia.getFecha(), tolerancia.getFecha(), null, 0);
                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(tolerancia.getFecha(), tolerancia.getFecha());
                if (listFeriado.size() > 0) {
//                    Notification.show("Mensaje del Sistema", "No se asignan tolerancias a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    toleranciaDao.guardarTolerancias(tolerancia);

//                    this.setNuevo();aa11
//                    Thread t = new Thread(r);aa11
                    // Lets run Thread in background..
                    // Sometimes you need to run thread in background for your Timer application..
//                    t.start();aa11
                }
                //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } catch (Exception e) {
                e.printStackTrace();
//                Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
//                guardarListener.accept(tolerancia);
            }

            start.add(Calendar.DATE, 1);
            tolerancia = new Tolerancias();
            if (!value) {
                value = true;
            }
        }
        if (value) {
            setVisible(false);
            guardarListener.accept(tolerancia);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(tolerancia);
        }
    }

    private void borrar() {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar? hay marcaciones asociadas que serán eliminadas si aprueba.", "40%");
        confirmMessage.getOkButton().addClickListener(e -> {
            try {
                toleranciaDao.borrar(tolerancia);
                listMarcation = marcacionDao.getByFilter(tolerancia.getFuncionario(), tolerancia.getFecha(), tolerancia.getFecha(), null, 0);
                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(tolerancia.getFecha(), tolerancia.getFecha());
                if (listFeriado.size() > 0) {
                    Notification.show("Mensaje del Sistema", "No se asignan tolerancias a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
//                    Thread t = new Thread(rRemove);aa11
                    // Lets run Thread in background..
                    // Sometimes you need to run thread in background for your Timer application..
//                    t.start();aa11
                }
                setVisible(false);
                borrarListener.accept(tolerancia);
                Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
                operacion = "E";
                nuevo = "";
                //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } catch (Exception ec) {
                ec.printStackTrace();
                Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
                borrarListener.accept(tolerancia);
            }
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });
    }

    public void setGuardarListener(Consumer<Tolerancias> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Tolerancias> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Tolerancias> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(Tolerancias value) {
        try {
            if (value.getId() != null) {
                this.viejo = value.getFecha();
                this.func = value.getFuncionario();
                this.param = value.getParametro().getDescripcion();
                btnGuardar.setVisible(false);
            } else {
                fecha.setValue(DateUtils.asLocalDate(new Date()));
                filterParametro.setValue(null);
                btnGuardar.setVisible(true);
            }
        } catch (Exception e) {
            fecha.setValue(DateUtils.asLocalDate(new Date()));
            filterParametro.setValue(null);
            btnGuardar.setVisible(true);
        } finally {
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + tolerancia.getId() + "," + tolerancia.getFecha() + ")";
    }

    public void setTolerancias(Tolerancias c) {
        try {
            this.tolerancia = c;
            if (c.getId() != null) {
                fecha.setValue(DateUtils.asLocalDate(c.getFecha()));
                filterParametro.setValue(c.getParametro());
                filterFuncionario.setVisible(false);
                checkForm.setVisible(false);
            } else {
                filterFuncionario.setVisible(true);
                checkForm.setVisible(true);
                checkForm.setValue(false);
            }
            try {
                fecha.setValue(DateUtils.asLocalDate(c.getFecha()));
                fechaHasta.setValue(DateUtils.asLocalDate(c.getFecha()));
            } catch (Exception e) {
                fecha.setValue(DateUtils.asLocalDate(new Date()));
                fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
            } finally {
            }

            filterFuncionario.setValue(null);
            btnBorrar.setVisible((tolerancia.getId() != null));
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (filterParametro == null || filterParametro.isEmpty()) {
            savedEnabled = false;
        }
        if (fecha == null || fecha.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    Runnable r = new Runnable() {
        public void run() {
            if (listMarcation.isEmpty()) {
                guardarMarcacion(tolerancia.getFecha());
            } else {
                actualizarMarcacion(listMarcation, tolerancia);
            }
        }

        private void guardarMarcacion(Date fecha) {
            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
            HorarioFuncionario hf = hfDao.listarPorIdFuncionario(tolerancia.getFuncionario().getIdfuncionario());
            try {
                Calendar start = Calendar.getInstance();
                start.setTime(fecha);

                Calendar end = Calendar.getInstance();
                end.setTime(fecha);

                while (!start.after(end)) {
                    Date targetDay = start.getTime();
                    // Do Work Here
                    if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                        Marcaciones marcacion = new Marcaciones();
                        marcacion.setHorarioFuncionario(hf);

                        marcacion.setFecha(targetDay);
                        marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                        marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                        marcacion.setInmarcada(tsTolerancia);
                        marcacion.setOutmarcada(tsTolerancia);
                        marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                            HorarioLaboral hl = hlDao.getByValue("sábado");
                            hf.setHorarioLaboral(hl);
                            marcacion.setInasignada(hl.getEntrada());
                            marcacion.setOutasignada(hl.getSalida());
                        }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                        long minutesEntSal = 0;
                        try {
                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                        } catch (Exception ec) {
                            marcacion.setOutmarcada(tsTolerancia);
                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                        } finally {
                        }

                        marcacion.setMintrabajada(minutesEntSal);
//            }
                        if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                            marcacion.setMinllegadatemprana(0L);
                            marcacion.setMinllegadatardia(0L);
                            //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                        } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                            marcacion.setMinllegadatemprana(0L);
                            //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                            int difHere = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                            marcacion.setMinllegadatardia(Long.parseLong(difHere+""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                            marcacion.setMintrabajada(minutesEntSal);
                        } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                            int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                            marcacion.setMinllegadatemprana(0L);
                            marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                            marcacion.setMintrabajada(minutesEntSal);
                        }
                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                            marcacion.setMintrabajada(marcacion.getMintrabajada());
                        }
                        marcacion.setSolicitud(null);
                        marcacion.setObservacion(tolerancia.getParametro().getDescripcion().toUpperCase());
                        marcacionDao.guardar(marcacion);
                    }
                    start.add(Calendar.DATE, 1);
                }
            } catch (Exception exp) {
            } finally {
            }
        }

        private void actualizarMarcacion(List<Marcaciones> listMarcation, Tolerancias tolerancia) {
            for (Marcaciones m : listMarcation) {
                try {
                    m.setFecha(tolerancia.getFecha());
                    String obs = m.getObservacion() == null || m.getObservacion().equalsIgnoreCase("") ? tolerancia.getParametro().getDescripcion().toUpperCase() : m.getObservacion() + " - " + tolerancia.getParametro().getDescripcion().toUpperCase();
                    m.setObservacion(obs);
                    marcacionDao.guardar(m);
                } catch (Exception exp) {
                } finally {
                }
            }
        }
    };
    Runnable rRemove = new Runnable() {
        public void run() {
            actualizarMarcacion(listMarcation, tolerancia);
        }

        private void actualizarMarcacion(List<Marcaciones> listMarcation, Tolerancias tolerancia) {
            toleranciaDao.borrar(tolerancia);
            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
            for (Marcaciones m : listMarcation) {
                try {
                    System.out.println("SUMA -> " + (m.getInmarcada().getHours() + m.getInmarcada().getMinutes())
                            + " - " + (tsTolerancia.getHours() + tsTolerancia.getMinutes()));
                    if ((m.getInmarcada().getHours() + m.getInmarcada().getMinutes()) == (tsTolerancia.getHours() + tsTolerancia.getMinutes()) && (m.getOutmarcada().getHours() + m.getOutmarcada().getMinutes()) == (tsTolerancia.getHours() + tsTolerancia.getMinutes())) {
                        marcacionDao.borrar(m);
                    } else {
                        m.setFecha(tolerancia.getFecha());
                        String obs = m.getObservacion();
                        try {
                            if (obs.matches(".*- " + tolerancia.getParametro().getDescripcion().toUpperCase() + ".*")) {
                                obs = obs.replaceFirst(" - " + tolerancia.getParametro().getDescripcion().toUpperCase(), "");
                            } else {
                                obs = obs.replaceFirst(tolerancia.getParametro().getDescripcion().toUpperCase(), "");
                            }
                        } catch (Exception e) {
                            obs = obs.replaceFirst(tolerancia.getParametro().getDescripcion().toUpperCase(), "");
                        } finally {
                        }
                        m.setObservacion(obs);
                        marcacionDao.guardar(m);
                    }
                } catch (Exception e) {
                } finally {
                }
            }
        }
    };

    private void guardarAtodos() {
        List<HorarioFuncionario> listHFunc = hfDao.listadeHorarioFuncionarioTrue();
        boolean val = false;
        for (HorarioFuncionario horarioFuncionario : listHFunc) {

            Calendar start = Calendar.getInstance();
            start.setTime(DateUtils.asDate(fecha.getValue()));

            Calendar end = Calendar.getInstance();
            end.setTime(DateUtils.asDate(fechaHasta.getValue()));
            while (!start.after(end)) {
                Date targetDay = start.getTime();

                tolerancia = new Tolerancias();
                try {
                    try {
                        tolerancia.setFecha(targetDay);
                    } catch (Exception ex) {
                        tolerancia.setFecha(null);
                    } finally {
                    }
                    try {
                        tolerancia.setParametro(filterParametro.getValue());
                    } catch (Exception ex) {
                        tolerancia.setParametro(null);
                    } finally {
                    }
                    tolerancia.setFuncionario(horarioFuncionario.getFuncionario());

                    listMarcation = marcacionDao.getByFilter(tolerancia.getFuncionario(), tolerancia.getFecha(), tolerancia.getFecha(), null, 0);
                    List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(tolerancia.getFecha(), tolerancia.getFecha());
                    if (listFeriado.size() > 0) {
                        // Notification.show("Mensaje del Sistema", "No se asignan tolerancias a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        toleranciaDao.guardarTolerancias(tolerancia);
                        setVisible(false);
                        if (!val) {
                            val = true;
                        }

                        if (listMarcation.isEmpty()) {
//                            guardarMarcacionR(tolerancia.getFecha());aa11
                        } else {
//                            actualizarMarcacionR(listMarcation, tolerancia);aa11
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }

                start.add(Calendar.DATE, 1);
            }

        }
        if (val) {
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            Notification.show("Advertencia", "No se pudo guardar, verifique los datos", Notification.Type.ERROR_MESSAGE);
        }
    }

    private void guardarMarcacionR(Date fecha) {
        Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(tolerancia.getFuncionario().getIdfuncionario());
        try {
            Calendar start = Calendar.getInstance();
            start.setTime(fecha);

            Calendar end = Calendar.getInstance();
            end.setTime(fecha);

            while (!start.after(end)) {
                Date targetDay = start.getTime();
                // Do Work Here
                if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                    Marcaciones marcacion = new Marcaciones();
                    marcacion.setHorarioFuncionario(hf);

                    marcacion.setFecha(targetDay);
                    marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                    marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                    marcacion.setInmarcada(tsTolerancia);
                    marcacion.setOutmarcada(tsTolerancia);
                    marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                    if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                        HorarioLaboral hl = hlDao.getByValue("sábado");
                        hf.setHorarioLaboral(hl);
                        marcacion.setInasignada(hl.getEntrada());
                        marcacion.setOutasignada(hl.getSalida());
                    }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                    long minutesEntSal = 0;
                    try {
                        //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                        minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    } catch (Exception ec) {
                        marcacion.setOutmarcada(tsTolerancia);
                        //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                        minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    } finally {
                    }

                    marcacion.setMintrabajada(minutesEntSal);
//            }
                    if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                        marcacion.setMinllegadatemprana(0L);
                        marcacion.setMinllegadatardia(0L);
                        //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                    } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                        marcacion.setMinllegadatemprana(0L);
                        int difHere = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                        marcacion.setMinllegadatardia(Long.parseLong(difHere+""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                        //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                        minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                        marcacion.setMintrabajada(minutesEntSal);
                    } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                        int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                        marcacion.setMinllegadatemprana(0L);
                        marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                        minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                        marcacion.setMintrabajada(minutesEntSal);
                    }
                    if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                        marcacion.setMintrabajada(marcacion.getMintrabajada());
                    }
                    marcacion.setSolicitud(null);
                    marcacion.setObservacion(tolerancia.getParametro().getDescripcion().toUpperCase());
                    marcacionDao.guardar(marcacion);
                }
                start.add(Calendar.DATE, 1);
            }
        } catch (Exception exp) {
        } finally {
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void actualizarMarcacionR(List<Marcaciones> listMarcation, Tolerancias tolerancia) {
        for (Marcaciones m : listMarcation) {
            try {
                m.setFecha(tolerancia.getFecha());
                String obs = m.getObservacion() == null || m.getObservacion().equalsIgnoreCase("") ? tolerancia.getParametro().getDescripcion().toUpperCase() : m.getObservacion() + " - " + tolerancia.getParametro().getDescripcion().toUpperCase();
                m.setObservacion(obs);
                marcacionDao.guardar(m);
            } catch (Exception exp) {
            } finally {
            }
        }
    }

    private void updateFormulario(Boolean value) {
        if (value) {
            filterFuncionario.setValue(null);
            filterFuncionario.setEnabled(false);
        } else {
            filterFuncionario.setValue(null);
            filterFuncionario.setEnabled(true);
        }
    }

}
