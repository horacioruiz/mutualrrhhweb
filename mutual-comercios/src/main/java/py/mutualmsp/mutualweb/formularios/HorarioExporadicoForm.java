package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.HorarioExporadicoDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 28/6/2016.
 */
public class HorarioExporadicoForm extends Window {

    private TextField txtCedulaFuncionario = new TextField("Ingrese cédula (*)");
    private TextField txtNombreFuncionario = new TextField("Funcionario");
    private TextField txtArea = new TextField("Area");
    private TextField txtCargo = new TextField("Cargo");
    private TextArea txtObservacion = new TextArea("Observación");
    private TextField txtCantdias = new TextField("Cantidad días");
    private DateField fechaInicio = new DateField("Fecha Inicio (*)");
    private DateField fechaFin = new DateField("Fecha Fin (*)");
    private ComboBox<String> prioridad = new ComboBox<>("Prioridad");
    private int editar = 0;

    private ComboBox<Dependencia> cbDpto = new ComboBox<>("Departamento (*)");
    private ComboBox<Dependencia> cbCargo = new ComboBox<>("Sección (*)");

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Salir");
    private TextField costo = new TextField("Costo");
    private TextField horas = new TextField("Horas Trabajadas");
    private TextField tiempoRespuesta = new TextField("Tiempo de Respuesta");
    private TextField txtDependencia = new TextField("Dependencia");
    private ComboBox<Formulario> formulario = new ComboBox<>("Formulario (*)");

    Grid<HorarioExporadico> gridHorarioExporadico = new Grid<>(HorarioExporadico.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    ComboBox<Funcionario> cbFuncionario = new ComboBox<>("Funcionario (*)");
    TextField txtAreaForm = new TextField("Area");
    TextField txtCargoForm = new TextField("Cargo");
    TextField txtObservacionForm = new TextField("Descripción actividad (*)");
    DateTimeField fechaHoraForm = new DateTimeField("Fecha/Hora estipulada (*)");
    private Button btnAgregar = new Button("Agregar");

    private ComboBox<Motivos> motivo = new ComboBox<>("Motivos (*)");
    private ComboBox<Funcionario> encargado = new ComboBox<>("Encargado (*)");
//    private ComboBox<Funcionario> rrhh = new ComboBox<>("Funcionario RRHH");
//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    private TextArea txtOtroMotivo = new TextArea("Otro motivo");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<HorarioExporadico> saveListener;
    private Consumer<HorarioExporadico> deleteListener;
    private Consumer<HorarioExporadico> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
    int numRowSelected = 0;

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    FormularioDao formularioDao = ResourceLocator.locate(FormularioDao.class);
    HorarioExporadicoDao horarioExporadicoDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    HorarioExporadicoDao solicitudDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    List<HorarioExporadico> listProduccionDetalle = new ArrayList<>();
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    private boolean enviarCorreos = false;
    private HorarioExporadico solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();
//    InlineDateTimeField sample = new InlineDateTimeField();
    DateTimeField fechaHoraInicio = new DateTimeField("Fecha Inicio (*)");
    DateTimeField fechaHoraFin = new DateTimeField("Fecha Fin (*)");

    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload

    private void updateFormulario() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtAreaForm.setValue(func.getDependencia().getDescripcion());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void cargarGrilla() {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        gridHorarioExporadico.clearSortOrder();
//            gridHorarioExporadico.setCaption("Funcionarios agregados");
        gridHorarioExporadico.setItems(listProduccionDetalle);
        gridHorarioExporadico.removeAllColumns();
        gridHorarioExporadico.addColumn(e -> {
            return e.getFechadesde() != null ? formatFec.format(e.getFechadesde()) : "--";
        }).setCaption("DESDE");
        gridHorarioExporadico.addColumn(e -> {
            return e.getFechahasta() != null ? formatFec.format(e.getFechahasta()) : "--";
        }).setCaption("HASTA");
        gridHorarioExporadico.addColumn(e -> {
            return e.getLunes() ? "SI" : "NO";
        }).setCaption("LUNES");
        gridHorarioExporadico.addColumn(e -> {
            return e.getMartes() ? "SI" : "NO";
        }).setCaption("MARTES");
        gridHorarioExporadico.addColumn(e -> {
            return e.getMiercoles() ? "SI" : "NO";
        }).setCaption("MIERCOLES");
        gridHorarioExporadico.addColumn(e -> {
            return e.getJueves() ? "SI" : "NO";
        }).setCaption("JUEVES");
        gridHorarioExporadico.addColumn(e -> {
            return e.getViernes() ? "SI" : "NO";
        }).setCaption("VIERNES");
        gridHorarioExporadico.addColumn(e -> {
            return e.getSabado() ? "SI" : "NO";
        }).setCaption("SABADO");
        gridHorarioExporadico.addColumn(e -> {
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            return e.getHorarioLaboral() == null ? "--" : formatHor.format(e.getHorarioLaboral().getEntrada()) + " a " + formatHor.format(e.getHorarioLaboral().getSalida());
        }).setCaption("HORARIO");
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            gridHorarioExporadico.addComponentColumn(this::buildConfirmButton).setCaption("Eliminar");
        }
//        String patternHora = "HH:mm";
//        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
//        if (validateForm()) {
//            HorarioExporadicoProduccionDetalle spd = new HorarioExporadicoProduccionDetalle();
//            spd.setArea(txtAreaForm.getValue());
//            spd.setCargo(txtCargoForm.getValue());
//            spd.setDescripcion(txtObservacionForm.getValue());
//            spd.setFuncionario(cbFuncionario.getValue());
//            spd.setNombrefuncionario(txtNombreFuncionario.getValue());
//            try {
//                spd.setHora(Timestamp.valueOf(fechaHoraForm.getValue()));
//            } catch (Exception e) {
//                spd.setHora(null);
//            } finally {
//            }
//            try {
//                spd.setFecha(DateUtils.asDate(fechaHoraForm.getValue()));
//            } catch (Exception e) {
//                spd.setFecha(null);
//            } finally {
//            }
//            
//
//            cbFuncionario.clear();
//            cbFuncionario.setItems(new ArrayList<>());
//            Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
//            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
//            List<Funcionario> listFunc = new ArrayList<>();
//            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
//            for (Dependencia dependencia1 : listDependencia) {
//                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
//            }
//            cbFuncionario.setItems(listFunc);
//            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
//
//            txtNombreFuncionario.setValue("");
//            txtAreaForm.setValue("");
//            txtCargoForm.setValue("");
//            txtObservacionForm.setValue("");
//            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
//            try {
//                String fecha = formatFec.format(new Date());
//                String fec = fecha + "T19:00";
//                LocalDateTime ldtFin = LocalDateTime.parse(fec);
//                fechaHoraForm.setValue(ldtFin);
//            } catch (Exception e) {
//                fechaHoraForm.setValue(null);
//            } finally {
//            }
////            gridHorarioExporadico.addComponentColumn(this::buildConfirmButton).setCaption(" X ");
//        } else {
//            Notification.show("Todos los campos son obligatorios",
//                    Notification.Type.ERROR_MESSAGE);
//        }
    }

    private boolean validateForm() {
        boolean savedEnabled = true;
        if (txtAreaForm == null || txtAreaForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtCargoForm == null || txtCargoForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtObservacionForm == null || txtObservacionForm.isEmpty()) {
            savedEnabled = false;
        }
        if (cbFuncionario == null || cbFuncionario.getValue() == null) {
            savedEnabled = false;
        }
        if (fechaHoraForm == null || fechaHoraForm.getValue() == null) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void guardarDetalleProduccion(HorarioExporadico solicitud) {

    }

    private void cargarSeccion(Dependencia dependencia) {
        if (dependencia != null && cbDpto.getValue() != null) {
            cbCargo.clear();
            cbCargo.setItems(new ArrayList<>());
            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbCargo.setVisible(true);

            Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            cbFuncionario.setItems(listFunc);
            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        }
    }

    private void editarHorarioExporadicoProduccion(HorarioExporadico solicitud) {

    }

    private long calcularCantDias() {
        long substract = calcWeekDays(convertToDateViaInstant(fechaInicio.getValue()), convertToDateViaInstant(fechaFin.getValue())) + 1;
        if (isWeekendSunday(fechaFin.getValue())) {
            substract -= 1l;
        }

        List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()));
        long cantFeriado = 0;
        for (Feriado feriado : listFeriado) {
            if (!isWeekend(DateUtils.asLocalDate(feriado.getFecha()))) {
                cantFeriado++;
            }
        }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
        return (substract - cantFeriado);
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web-rrhh/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(HorarioExporadicoForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };

    public HorarioExporadicoForm() {
        try {
            VerticalLayout layout = createForm();
//            VerticalLayout layout = new VerticalLayout();
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Listado de Horario Exporadico");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }
            txtCedulaFuncionario.setEnabled(false);
//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreFuncionario.setEnabled(false);
            txtArea.setEnabled(false);
            txtCantdias.setEnabled(false);
            txtCargo.setEnabled(false);
            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            txtAreaForm.setEnabled(false);
            txtCargoForm.setEnabled(false);

            mainLayout.setVisible(false);

            txtCedulaFuncionario.addBlurListener(e -> {
                validate();
            });

            formulario.setItems(formularioDao.listaFormulario());
            formulario.setItemCaptionGenerator(Formulario::getDescripcion);

//            rrhh.setItems(funcionarioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setItems(funcionarioDao.listaFuncionario());
            encargado.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setVisible(false);

            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            gridHorarioExporadico.clearSortOrder();
//            gridHorarioExporadico.setCaption("Funcionarios agregados");
            gridHorarioExporadico.setItems(listProduccionDetalle);
            gridHorarioExporadico.removeAllColumns();
            gridHorarioExporadico.addColumn(e -> {
                return e.getFechadesde() != null ? formatFec.format(e.getFechadesde()) : "--";
            }).setCaption("DESDE");
            gridHorarioExporadico.addColumn(e -> {
                return e.getFechahasta() != null ? formatFec.format(e.getFechahasta()) : "--";
            }).setCaption("HASTA");
            gridHorarioExporadico.addColumn(e -> {
                return e.getLunes() ? "SI" : "NO";
            }).setCaption("LUNES");
            gridHorarioExporadico.addColumn(e -> {
                return e.getMartes() ? "SI" : "NO";
            }).setCaption("MARTES");
            gridHorarioExporadico.addColumn(e -> {
                return e.getMiercoles() ? "SI" : "NO";
            }).setCaption("MIERCOLES");
            gridHorarioExporadico.addColumn(e -> {
                return e.getJueves() ? "SI" : "NO";
            }).setCaption("JUEVES");
            gridHorarioExporadico.addColumn(e -> {
                return e.getViernes() ? "SI" : "NO";
            }).setCaption("VIERNES");
            gridHorarioExporadico.addColumn(e -> {
                return e.getSabado() ? "SI" : "NO";
            }).setCaption("SABADO");
            gridHorarioExporadico.addColumn(e -> {
                SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                return e.getHorarioLaboral() == null ? "--" : formatHor.format(e.getHorarioLaboral().getEntrada()) + " a " + formatHor.format(e.getHorarioLaboral().getSalida());
            }).setCaption("HORARIO");

            formulario.focus();

            gridHorarioExporadico.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (numRowSelected >= 0) {
                        ConfirmButton confirmMessage = new ConfirmButton("");
                        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el horario exporádico?", "30%");
                        confirmMessage.getOkButton().addClickListener(e -> {
                            horarioExporadicoDao.borrar(listProduccionDetalle.get(numRowSelected));

                            listProduccionDetalle.remove(numRowSelected);
                            gridHorarioExporadico.clearSortOrder();
                            gridHorarioExporadico.setItems(listProduccionDetalle);
                            numRowSelected = -1;
                            confirmMessage.closePopup();
                        });
                        confirmMessage.getCancelButton().addClickListener(e -> {
                            confirmMessage.closePopup();
                        });
                    }
                }
            });

            gridHorarioExporadico.addItemClickListener(e -> {
                if (e.getItem() != null) {
//                    spd = e.getItem();
                    numRowSelected = e.getRowIndex();
                }
            });
//            comentarios.addBlurListener(e -> {
//                validate();
//            });
//            txtCedulaFuncionario.addValueChangeListener(e -> {
//                if (e.getValue() != null && !e.getValue().equals("")) {
//                    validate();
//                }
//            });

            txtCedulaFuncionario.addValueChangeListener(e -> updateList(e.getValue()));
            formulario.addValueChangeListener(e -> cargarPorFormulario(e.getValue()));
            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));

            txtCedulaFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                        calcularCantDiaFuncionario();
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                    }
                }

                private void calcularCantDiaFuncionario() {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCantdias.setValue(dif + "");
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            fechaInicio.addValueChangeListener(e -> updateFecha());
            fechaFin.addValueChangeListener(e -> updateFecha());
            cbFuncionario.addValueChangeListener(e -> updateFormulario());

            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                try {
                    cargarGrilla();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("HorarioExporadico")) {
                        vl.addComponent(createForm());
                    }
//                    else {
//                        vl.addComponent(createFormMotivoHorarioExporadico());
//                    }

                    tab.addComponent(vl);
                }
            });
            gridHorarioExporadico.setSizeFull();
// Have some tabs
//            String[] tabs = {"HorarioExporadico"};
//            for (String caption : tabs) {
//                tabsheet.addTab(new VerticalLayout(), caption);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class No {

        public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
            return source -> null;
        }

        public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
            return (bean, fieldValue) -> {
                //no op
            };
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            if (txtCedulaFuncionario == null || txtCedulaFuncionario.isEmpty()) {
                savedEnabled = false;
            }
            if (txtNombreFuncionario == null || txtNombreFuncionario.isEmpty()) {
                savedEnabled = false;
            }
            if (formulario == null || formulario.getValue() == null) {
                savedEnabled = false;
            }
            if (motivo == null || motivo.getValue() == null) {
                savedEnabled = false;
            }
        }
        try {
            if (fechaInicio.isVisible()) {
                if (fechaInicio == null || fechaInicio.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaFin == null || fechaFin.getValue() == null) {
                    savedEnabled = false;
                }
            } else {
                if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    if (fechaHoraInicio == null || fechaHoraInicio.getValue() == null) {
                        savedEnabled = false;
                    }
                    if (fechaHoraFin == null || fechaHoraFin.getValue() == null) {
                        savedEnabled = false;
                    }
                }
            }
        } catch (Exception e) {
            if (fechaInicio.isVisible()) {
                if (fechaInicio == null || fechaInicio.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaFin == null || fechaFin.getValue() == null) {
                    savedEnabled = false;
                }
            } else {
                if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    if (fechaHoraInicio == null || fechaHoraInicio.getValue() == null) {
                        savedEnabled = false;
                    }
                    if (fechaHoraFin == null || fechaHoraFin.getValue() == null) {
                        savedEnabled = false;
                    }
                }
            }
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    public void editarRegistro(HorarioExporadico solicitud) {
        setCaption("Editar HorarioExporadico");
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.addComponent(txtCedulaFuncionario);
        horizontal.addComponent(txtNombreFuncionario);
        horizontal.addComponent(txtArea);
        horizontal.addComponent(txtCargo);

        horizontal.setWidth("100%");
        horizontal.setSpacing(true);
        horizontal.setStyleName("top-bar");

        HorizontalLayout horizontalSegundo = new HorizontalLayout();
////        horizontalSegundo.addComponent(formulario);
////        horizontalSegundo.addComponent(fechaInicio);
////        horizontalSegundo.addComponent(fechaFin);
////        horizontalSegundo.addComponent(fechaHoraInicio);
////        horizontalSegundo.addComponent(fechaHoraFin);
//        horizontalSegundo.addComponent(txtCantdias);
//        horizontalSegundo.addComponent(cbDpto);
        horizontalSegundo.addComponent(gridHorarioExporadico);

        horizontalSegundo.setWidth("100%");
        horizontalSegundo.setSpacing(true);
        horizontalSegundo.setStyleName("top-bar");

        layout.addComponent(horizontal);
        layout.addComponent(horizontalSegundo);

//        layout.addComponent(txtObservacion);
//        txtObservacion.setWidth("100%");
        HorizontalLayout horizontalButton = new HorizontalLayout();
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private VerticalLayout createFormMotivoHorarioExporadico() {
        guardar.setVisible(true);
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.addComponent(motivo);

//        horizontal.addComponent(rrhh);
        horizontal.addComponent(encargado);
        horizontal.addComponent(upload);
        horizontal.addComponent(labelUrl);

        image.setVisible(false);
        upload.setButtonCaption("Seleccionar");
        upload.addSucceededListener(receiver);

        // Prevent too big downloads 0981752315
        final long UPLOAD_LIMIT = 1000000l;
        upload.addStartedListener(new Upload.StartedListener() {
            private static final long serialVersionUID = 4728847902678459488L;

            @Override
            public void uploadStarted(Upload.StartedEvent event) {
                if (event.getContentLength() > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });

        // Check the size also during progress
        upload.addProgressListener(new Upload.ProgressListener() {
            private static final long serialVersionUID = 8587352676703174995L;

            @Override
            public void updateProgress(long readBytes, long contentLength) {
                if (readBytes > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });
        // Create uploads directory
        File uploads = new File(Constants.UPLOAD_DIR_TEMP);
        if (!uploads.exists() && !uploads.mkdir()) {
            horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
        }

        horizontal.setWidth("100%");
        horizontal.setSpacing(true);
        horizontal.setStyleName("top-bar");

        HorizontalLayout horizontalSegundo = new HorizontalLayout();
        horizontalSegundo.addComponent(txtOtroMotivo);
        HorizontalLayout horizontalTercero = new HorizontalLayout();
        horizontalTercero.addComponent(txtDependencia);
//        horizontalSegundo.addComponent(txtOtroMotivo);
        txtOtroMotivo.setWidth("100%");

        horizontalSegundo.setWidth("100%");
        horizontalSegundo.setSpacing(true);
        horizontalSegundo.setStyleName("top-bar");
        txtDependencia.setWidth("100%");

        horizontalTercero.setWidth("100%");
        horizontalTercero.setSpacing(true);
        horizontalTercero.setStyleName("top-bar");

        btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        form.addComponent(cbFuncionario);

        form.addComponent(txtAreaForm);
        form.addComponent(txtCargoForm);
        form.addComponent(txtObservacionForm);
        form.addComponent(fechaHoraForm);
        form.addComponent(btnAgregar);

        HorizontalLayout horizontalCuarto = new HorizontalLayout();
        horizontalCuarto.addComponent(gridHorarioExporadico);
        horizontalCuarto.setWidth("100%");

        mainLayout.addComponent(horizontalCuarto);
        mainLayout.addComponent(form);
        mainLayout.setWidth("100%");
//        gridHorarioExporadico.setWidth("100%");

        layout.addComponent(mainLayout);

        layout.addComponent(horizontal);
        layout.addComponent(horizontalSegundo);
        layout.addComponent(horizontalTercero);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        horizontalButton.addComponent(guardar);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);

//        FormLayout columnLayout = new FormLayout();
//// Setting the desired responsive steps for the columns in the layout
//columnLayout.setResponsiveSteps(
//           new ResponsiveStep("25em", 1),
//           new ResponsiveStep("32em", 2),
//           new ResponsiveStep("40em", 3));
//   TextField firstName = new TextField();
//   firstName.setPlaceholder("First Name");
//   TextField lastName = new TextField();
//   lastName.setPlaceholder("Last Name");
//   TextField email = new TextField();
//   email.setPlaceholder("Email");
//   TextField nickname = new TextField();
//   nickname.setPlaceholder("Username");
//   TextField website = new TextField();
//   website.setPlaceholder("Link to personal website");
//   TextField description = new TextField();
//   description.setPlaceholder("Enter a short description about yourself");
//   columnLayout.add(firstName, lastName,  nickname, email, website); 
//   // You can set the desired column span for the components individually.
//   columnLayout.setColspan(website, 2);
//   // Or just set it as you add them.
//   columnLayout.add(description, 3);
        return layout;
    }

    public void setSaveListener(Consumer<HorarioExporadico> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<HorarioExporadico> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<HorarioExporadico> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void setEnviarCorreos(boolean enviarCorreos) {
        this.enviarCorreos = enviarCorreos;
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreFuncionario.setValue("");
                txtArea.setValue("");
                txtCargo.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Button buildConfirmButton(HorarioExporadico p) {
        Button button = new Button(VaadinIcons.CHECK_SQUARE_O);

        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        button.addClickListener(e -> eliminarData(p));
        return button;
    }

    private void eliminarData(HorarioExporadico p) {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar el horario exporádico?", "30%");
        confirmMessage.getOkButton().addClickListener(e -> {
            horarioExporadicoDao.borrar(p);

            listProduccionDetalle = horarioExporadicoDao.listarPorIdFuncionarioF(p.getFuncionario().getIdfuncionario());
            gridHorarioExporadico.clearSortOrder();
            gridHorarioExporadico.setItems(listProduccionDetalle);
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });
    }

    private void cargarPorFormulario(Formulario value) {
        try {
            motivo.setValue(null);
            if (value != null) {
                motivo.setItems(motivosDao.listarPorFormulario(value));
                motivo.setItemCaptionGenerator(Motivos::getDescripcion);
                txtCantdias.setValue("0");
                if (value.getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);

                    fechaHoraInicio.setVisible(true);
                    fechaHoraFin.setVisible(true);
                    txtDependencia.setVisible(true);
                    fechaHoraFin.setCaption("Fecha Fin (*)");
                    upload.setVisible(true);
                    txtOtroMotivo.setVisible(true);

                    txtCedulaFuncionario.setVisible(true);
                    txtNombreFuncionario.setVisible(true);
                    txtArea.setVisible(true);
                    txtCargo.setVisible(true);
                    txtObservacion.setVisible(true);
                    motivo.setVisible(true);
                    mainLayout.setVisible(false);

                    cbCargo.setVisible(false);
                    cbDpto.setVisible(false);
                } else if (value.getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);
                    fechaHoraInicio.setVisible(false);

                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);

                    fechaHoraInicio.setVisible(false);
                    fechaHoraFin.setVisible(false);
                    txtDependencia.setVisible(false);

                    fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
                    upload.setVisible(false);
                    txtOtroMotivo.setVisible(false);

                    txtCedulaFuncionario.setVisible(false);
                    txtNombreFuncionario.setVisible(false);
                    txtArea.setVisible(false);
                    txtCargo.setVisible(false);
                    txtObservacion.setVisible(false);
                    motivo.setVisible(false);
                    gridHorarioExporadico.clearSortOrder();
                    mainLayout.setVisible(true);
                    txtCantdias.setVisible(false);

                    cbDpto.setItems(dptoDao.listarDepartamentosPadres());
                    cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
                    cbDpto.setVisible(true);

                    cbCargo.setItems(new ArrayList<>());
                    cbCargo.setVisible(true);

                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                    try {
                        String fecha = formatFec.format(new Date());
                        String fec = fecha + "T" + "19:00:00";
                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                        fechaHoraForm.setValue(ldtFin);
                    } catch (Exception e) {
                        fechaHoraForm.setValue(null);
                    } finally {
                    }
                } else {
                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(true);
                    fechaFin.setVisible(true);

                    fechaHoraInicio.setVisible(false);
                    fechaHoraFin.setVisible(false);
                    txtCantdias.setValue("");
                    txtCantdias.setVisible(true);
                    txtDependencia.setVisible(false);
                    upload.setVisible(true);
                    txtOtroMotivo.setVisible(true);

                    txtCedulaFuncionario.setVisible(true);
                    txtNombreFuncionario.setVisible(true);
                    txtArea.setVisible(true);
                    txtCargo.setVisible(true);
                    txtObservacion.setVisible(true);
                    motivo.setVisible(true);
                    mainLayout.setVisible(false);

                    cbCargo.setVisible(false);
                    cbDpto.setVisible(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    private void updateFecha() {
        if (fechaInicio.getValue() != null && fechaFin.getValue() != null) {
            if (fechaInicio.getValue().isBefore(fechaFin.getValue()) || fechaInicio.getValue().isEqual(fechaFin.getValue())) {
                if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getIdfuncionario() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getIdfuncionario());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            dif -= (cantSabados - listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCantdias.setValue(dif + "");
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                } else {
                    txtCantdias.setValue(calcularCantDias() + "");
                }
            }
        }
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public void nuevoRegistro(Funcionario funcionarioF, List<HorarioExporadico> listHE) {
        listProduccionDetalle = listHE;
        txtCedulaFuncionario.setValue(funcionarioF.getCedula());
        txtNombreFuncionario.setValue(funcionarioF.getNombreCompleto());
        txtArea.setValue(funcionarioF.getCargo().getDescripcion());
        txtCargo.setValue(funcionarioF.getDependencia().getDescripcion());

        gridHorarioExporadico.clearSortOrder();
//            gridHorarioExporadico.setCaption("Funcionarios agregados");
        gridHorarioExporadico.setItems(listHE);
        txtCedulaFuncionario.setEnabled(false);
        cargarGrilla();
    }

    private void save() {
    }

    private void cargarVacacionesDisponible() {
        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario());
        for (Vacaciones listado : listVacaciones) {
            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        }
    }
}
