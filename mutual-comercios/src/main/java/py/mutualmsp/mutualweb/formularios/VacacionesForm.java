package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 7/6/2016.
 */
public class VacacionesForm extends FormLayout {

    Binder<Vacaciones> binder = new Binder<>(Vacaciones.class);
    Vacaciones motivo;
    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";

    public void setCargo(Vacaciones c) {
        try {
            this.motivo = c;
            binder.setBean(c);
            if (c.getId() == null) {
                habilitado.setValue(false);
                txtUtilizado.setVisible(false);
                txtRestante.setVisible(false);

                txtPeriodo.setValue("");
                txtCantVacas.setValue("");
                comboBoxFuncionario.setValue(null);
            } else {
                txtUtilizado.setVisible(true);
                txtRestante.setVisible(true);

                txtUtilizado.setValue(c.getCantdiatomada() + "");
                txtRestante.setValue(c.getCantdiarestante() + "");

                txtPeriodo.setValue(c.getPeriodo());
                txtCantVacas.setValue(c.getCantdiavaca() + "");
                try {
                    habilitado.setValue(c.getAdelantado());
                } catch (Exception e) {
                    habilitado.setValue(false);
                } finally {
                }
                comboBoxFuncionario.setValue(c.getFuncionario());
            }
            setVisible(true);
            delete.setVisible(false);
            this.viejo = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    TextField txtAcreditado = new TextField("Acreditado", "");
    TextField txtUtilizado = new TextField("Utilizado", "");
    TextField txtRestante = new TextField("Restante", "");

    TextField txtPeriodo = new TextField("Periodo", "");
    TextField txtCantVacas = new TextField("Cantidad vacaciones", "");
    ComboBox<Funcionario> comboBoxFuncionario = new ComboBox<>("Funcionario");
    CheckBox habilitado = new CheckBox("Adelanto");

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    Button delete = new Button("Borrar");

    Binder<Vacaciones> fieldGroup = new Binder<>(Vacaciones.class);
    VacacionesDao controller = ResourceLocator.locate(VacacionesDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();
//    FormularioDao funcionarioController = ResourceLocator.locate(FormularioDao.class);
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);

    private Consumer<Vacaciones> saveListener;
    private Consumer<Vacaciones> deleteListener;
    private Consumer<Vacaciones> cancelListener;

    public VacacionesForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();

            save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            cancel.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            //VerticalLayout verticalLayout = createVerticalLayout();
            txtPeriodo.setWidth("100%");
            txtCantVacas.setWidth("100%");
            //layout.addComponent(descripcion);
            //layout.addComponent(email);

            CssLayout expander1 = new CssLayout();
            expander1.setSizeFull();
            expander1.setStyleName("expander");

            HorizontalLayout hl = new HorizontalLayout();
//            layout.addComponent(expander1);
//            layout.setExpandRatio(expander1, 1.0F);
//            CssLayout expander = new CssLayout();
//            expander.setSizeFull();
//            expander.setStyleName("expander");
//            layout.addComponent(expander);
//            layout.setExpandRatio(expander, 1.0F);
            //layout.addComponent(habilitado);
            HorizontalLayout horizontalLayout = new HorizontalLayout();

            horizontalLayout.addComponent(save);
            horizontalLayout.addComponent(delete);
            horizontalLayout.addComponent(cancel);

//            addComponents(txtPeriodo, txtCantVacas, comboBoxFuncionario, habilitado, txtUtilizado, txtRestante, expander1, hl, horizontalLayout);
            addComponents(txtPeriodo, txtCantVacas, comboBoxFuncionario, txtUtilizado, txtRestante, habilitado, horizontalLayout);

//            binder.bind(descripcion, Vacaciones::getDescripcion, Vacaciones::setDescripcion);
//            binder.forField(descripcion).withNullRepresentation("")
//                    .bind(Vacaciones::getDescripcion, Vacaciones::setDescripcion);
            /*descripcion.addValueChangeListener(m -> {
                descripcion.setValue(descripcion.getValue().toUpperCase());
            });*/
//            comboBoxFuncionario.setItems(funcionarioController.listaFormulario());
//            comboBoxFuncionario.setItemCaptionGenerator(Formulario::getDescripcion);
            comboBoxFuncionario.setItems(funcionarioController.listaFuncionario());
            comboBoxFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            //descripcion.addValueChangeListener(e -> validate());

            /*descripcion.addBlurListener(e -> {
                validate();
            });*/
//            fieldGroup.bind(descripcion, Usuarios.USERNAME);
//            fieldGroup.bind(password, Usuarios.PASSWORD);
//            fieldGroup.bind(email, Usuarios.EMAIL);
//            fieldGroup.bind(administradorCheck, Usuarios.ADMINISTRADOR);
//            fieldGroup.bind(clienteCheck, Usuarios.CLIENTES);
//            fieldGroup.bind(helpdeskCheck, Usuarios.HELPDESK);
//            fieldGroup.bind(habilitado, Usuarios.ENABLED);
//            fieldGroup.bind(comboBoxFuncionario,
//                    (Usuarios source) -> source.getFormulario(),
//                    (Usuarios bean, Formulario fieldvalue) -> {
//                        bean.setFormulario(fieldvalue);
//                    });
            save.addClickListener(cl -> {
                save();
            });

            cancel.addClickListener(cl -> {
                try {
                    cancelListener.accept(motivo);
                    // showForm(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            delete.addClickListener(cl -> {
                borrar();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void borrar() {
        try {
            controller.borrar(motivo);
            setVisible(false);
            deleteListener.accept(motivo);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            deleteListener.accept(motivo);
        }
    }

    private void save() {
        String period = "";
        try {
            period = Long.parseLong(txtPeriodo.getValue()) + "";
        } catch (Exception e) {
        } finally {
        }
        if (period.equalsIgnoreCase("")) {
            Notification.show("Atención", "Debes ingresar el año en numeros sin puntos decimales", Notification.Type.ERROR_MESSAGE);
        } else if (validate()) {
            long cantDiaVacas = 0;
            long cantDiaTomada = 0;
            cargarOtrosDatos();
            if (motivo.getId() == null) {
                for (Map.Entry<Long, String> entry : mapeo.entrySet()) {
                    String[] strArray = entry.getValue().split("-");
                    cantDiaVacas += Long.parseLong(strArray[0]);
                    cantDiaTomada += Long.parseLong(strArray[1]);
                }
                System.out.println("##### NUMBER 01 #####");
                motivo.setCantdiatomada(0l);
                //motivo.setCantdiarestante((cantDiaVacas - cantDiaTomada) + Long.parseLong(txtCantVacas.getValue()));
                motivo.setCantdiarestante(Long.parseLong(txtCantVacas.getValue()));
            } else {
                for (Map.Entry<Long, String> entry : mapeo.entrySet()) {
                    String[] strArray = entry.getValue().split("-");
                    if (entry.getKey() != motivo.getId()) {
                        cantDiaVacas += Long.parseLong(strArray[0]);
                        cantDiaTomada += Long.parseLong(strArray[1]);
                    }
                }
                long cantX = cantDiaVacas - cantDiaTomada;
                System.out.println("##### NUMBER 02 #####");
                //motivo.setCantdiarestante(cantX + Long.parseLong(txtCantVacas.getValue()));
                motivo.setCantdiarestante(Long.parseLong(txtCantVacas.getValue()));
            } 
            try {
                motivo.setCantdiatomada(Long.parseLong(txtUtilizado.getValue()));
                motivo.setCantdiarestante(Long.parseLong(txtRestante.getValue()));
                
                motivo.setPeriodo(txtPeriodo.getValue());
                motivo.setCantdiavaca(Long.parseLong(txtCantVacas.getValue()));
                motivo.setFuncionario(comboBoxFuncionario.getValue());
                motivo.setAdelantado(habilitado.getValue());
                controller.guardar(motivo);
                setVisible(false);
//                if (motivo.getId() == null) {
                saveListener.accept(motivo);
//                }

            } catch (Exception ex) {
                Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
                saveListener.accept(motivo);
                Logger.getLogger(FormularioForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (txtPeriodo == null || txtPeriodo.isEmpty()) {
            savedEnabled = false;
        }
        if (txtCantVacas == null || txtCantVacas.isEmpty()) {
            savedEnabled = false;
        }
        if (comboBoxFuncionario == null || comboBoxFuncionario.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void showForm(boolean show) {
        if (show) {
            addStyleName("visible");
        } else {
            removeStyleName("visible");
        }

        setEnabled(show);
    }

//    public void edit(Usuarios usuario) {
//        this.motivos = usuario;
//        showForm(true);
//        setVisible(true);
//        delete.setEnabled(usuario != null);
//        fieldGroup.setBean(motivos);
//        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
//        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
//        validate();
//    }
    public void setSaveListener(Consumer<Vacaciones> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Vacaciones> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Vacaciones> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public void edit(Vacaciones motivos) {
        setCaption("Editar Solicitud");
        this.motivo = motivos;
        try {
            txtPeriodo.setValue(motivos.getPeriodo());
        } catch (Exception e) {
            txtPeriodo.setValue(null);
        } finally {
        }
        try {
            comboBoxFuncionario.setValue(motivos.getFuncionario());
        } catch (Exception e) {
            txtPeriodo.setValue(null);
        } finally {
        }
        try {
            txtCantVacas.setValue(motivos.getCantdiavaca() + "");
        } catch (Exception e) {
            txtCantVacas.setValue("");
        } finally {
        }
        setVisible(true);
    }

    public void setViejo() {
        if (motivo.getId() != null) {
//            this.viejo = "(" + motivo.getId() + "," + motivo.getDescripcion() + ")";
        }
    }

    private void cargarOtrosDatos() {
        List<Vacaciones> listVacaciones = controller.listadeVacaciones(comboBoxFuncionario.getValue().getIdfuncionario());
        for (Vacaciones listado : listVacaciones) {
            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        }
    }
}
