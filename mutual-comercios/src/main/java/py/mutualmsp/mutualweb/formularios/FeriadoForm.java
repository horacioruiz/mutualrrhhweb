/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSunday;

/**
 *
 * @author Dbarreto
 */
public class FeriadoForm extends FormLayout {

    TextField txtfDescripcion = new TextField("Descripción", "");
    private DateField fecha = new DateField("Fecha");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    MarcacionesDao marcacionDao = ResourceLocator.locate(MarcacionesDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    Binder<Feriado> binder = new Binder<>(Feriado.class);

    Feriado feriado;

    private Consumer<Feriado> guardarListener;
    private Consumer<Feriado> borrarListener;
    private Consumer<Feriado> cancelarListener;

    private Date viejo = null;
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "feriado";

    public FeriadoForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            addComponents(txtfDescripcion, fecha, botones);

            binder.bind(txtfDescripcion, Feriado::getDescripcion, Feriado::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Feriado::getDescripcion, Feriado::setDescripcion);
            binder.bind(fecha, RequisitooperacionForm.No.getter(), RequisitooperacionForm.No.setter());
            binder.bindInstanceFields(this);

//            binder.bind(txtfDescripcion, Feriado::getDescripcion, Feriado::setDescripcion);
//            binder.forField(txtfDescripcion).withNullRepresentation("")
//                    .bind(Feriado::getDescripcion, Feriado::setDescripcion);
//            binder.bindInstanceFields(this);
            txtfDescripcion.addValueChangeListener(m -> {
                txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());
            });

            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(feriado);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            try {
                feriado.setDescripcion(txtfDescripcion.getValue());
            } catch (Exception ex) {
                feriado.setDescripcion("");
            } finally {
            }
            try {
                feriado.setFecha(DateUtils.asDate(fecha.getValue()));
            } catch (Exception ex) {
                feriado.setFecha(null);
            } finally {
            }
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY");
                feriado.setPeriodo(simpleDateFormat.format(feriado.getFecha()).toUpperCase());
            } catch (Exception ex) {
                feriado.setPeriodo("");
            } finally {
            }
            feriadoDao.guardar(feriado);
            Thread t = new Thread(r);
            // Lets run Thread in background..
            // Sometimes you need to run thread in background for your Timer application..
            t.start();

            setVisible(false);
            guardarListener.accept(feriado);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);

            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(feriado);
        }
    }

    Runnable r = new Runnable() {
        public void run() {
            List<Marcaciones> listmarcacion = new ArrayList<>();
            try {
                if (viejo != null) {
                    listmarcacion = marcacionDao.getByFilter(null, viejo, viejo, null, 0);
                } else {
                    listmarcacion = marcacionDao.getByFilter(null, feriado.getFecha(), feriado.getFecha(), null, 0);
                }
            } catch (Exception e) {
                listmarcacion = marcacionDao.getByFilter(null, feriado.getFecha(), feriado.getFecha(), null, 0);
            } finally {
            }
            if (listmarcacion.isEmpty()) {
                guardarMarcacion(feriado.getFecha());
            } else {
                actualizarMarcacion(listmarcacion, feriado.getFecha());
            }
        }

        private void guardarMarcacion(Date fecha) {
            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
            for (HorarioFuncionario hf : hfDao.listadeHorarioFuncionarioTrue()) {
                try {
                    Calendar start = Calendar.getInstance();
                    start.setTime(fecha);

                    Calendar end = Calendar.getInstance();
                    end.setTime(fecha);

                    while (!start.after(end)) {
                        Date targetDay = start.getTime();
                        // Do Work Here
                        if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                            Marcaciones marcacion = new Marcaciones();
                            marcacion.setHorarioFuncionario(hf);

                            marcacion.setFecha(targetDay);
                            marcacion.setInasignada(tsTolerancia);
                            marcacion.setOutasignada(tsTolerancia);
                            marcacion.setInmarcada(tsTolerancia);
                            marcacion.setOutmarcada(tsTolerancia);
                            marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                            marcacion.setMintrabajada(0l);
                            marcacion.setMinllegadatemprana(0L);
                            marcacion.setMinllegadatardia(0L);
                            marcacion.setSolicitud(null);
                            marcacion.setObservacion(feriado.getDescripcion().toUpperCase());
                            marcacionDao.guardar(marcacion);
                        }
                        start.add(Calendar.DATE, 1);
                    }
                } catch (Exception exp) {
                } finally {
                }
            }
        }

        private void actualizarMarcacion(List<Marcaciones> listmarcacion, Date feriado) {
            for (Marcaciones m : listmarcacion) {
                m.setFecha(feriado);
                try {
                    marcacionDao.guardar(m);
                } catch (Exception e) {
                } finally {
                }
            }
        }
    };
    Runnable rEliminar = new Runnable() {
        public void run() {
            List<Marcaciones> listmarcacion = new ArrayList<>();
            try {
                listmarcacion = marcacionDao.getByFilter(null, viejo, viejo, null, 0);
            } catch (Exception e) {
                listmarcacion = marcacionDao.getByFilter(null, feriado.getFecha(), feriado.getFecha(), null, 0);
            } finally {
            }
            try {
                eliminarMarcacion(listmarcacion);
            } catch (Exception e) {
            } finally {
            }
        }

        private void eliminarMarcacion(List<Marcaciones> listmarcacion) {
            for (Marcaciones m : listmarcacion) {
                try {
                    marcacionDao.borrar(m);
                } catch (Exception e) {
                } finally {
                }
            }
        }
    };

    private void borrar() {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar? hay marcaciones asociadas que serán eliminadas si aprueba.", "40%");
        confirmMessage.getOkButton().addClickListener(e -> {
            try {
                feriadoDao.borrar(feriado);
                Thread t = new Thread(rEliminar);
                // Lets run Thread in background..
                // Sometimes you need to run thread in background for your Timer application..
                t.start();
                setVisible(false);
                borrarListener.accept(feriado);
                Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
                operacion = "E";
                nuevo = "";
                //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } catch (Exception ex) {
                ex.printStackTrace();
                Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
                borrarListener.accept(feriado);
            }
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });
    }

    public void setGuardarListener(Consumer<Feriado> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Feriado> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Feriado> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(Date viejo) {
        if (feriado.getId() != null) {
            this.viejo = viejo;
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + feriado.getId() + "," + feriado.getDescripcion() + ")";
    }

    public void setFeriado(Feriado c) {
        try {
            this.feriado = c;
            if (c.getId() != null) {
                setViejo(c.getFecha());
            }
            binder.setBean(c);
            btnBorrar.setVisible((feriado.getId() != null));
            setVisible(true);
            txtfDescripcion.selectAll();
            fecha.setValue(DateUtils.asLocalDate(c.getFecha()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (txtfDescripcion == null || txtfDescripcion.isEmpty()) {
            savedEnabled = false;
        }
        if (fecha == null || fecha.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

}
