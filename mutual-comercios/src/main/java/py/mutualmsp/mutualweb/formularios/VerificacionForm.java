/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.themes.ValoTheme;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.data.provider.Query;
import com.vaadin.navigator.View;
import com.vaadin.server.Setter;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import py.mutualmsp.mutualweb.dao.AutorizadorDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.EstadoverificacionDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.ListaDetalleVerificacionCache;
import py.mutualmsp.mutualweb.dao.ModalidadDao;
import py.mutualmsp.mutualweb.dao.ProcedenciaDao;
import py.mutualmsp.mutualweb.dao.RegionalDao;
import py.mutualmsp.mutualweb.dao.RequisitooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperaciondetalleDao;
import py.mutualmsp.mutualweb.dao.VerificacionDao;
import py.mutualmsp.mutualweb.dao.VerificaciondetalleDao;
import py.mutualmsp.mutualweb.dto.DetalleVerificacionDTO;
import py.mutualmsp.mutualweb.entities.Autorizador;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Estadoverificacion;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Modalidad;
import py.mutualmsp.mutualweb.entities.Procedencia;
import py.mutualmsp.mutualweb.entities.Regional;
import py.mutualmsp.mutualweb.entities.Requisitooperacion;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.entities.Verificacion;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.DBConnection;
import py.mutualmsp.mutualweb.util.UserHolder;


/**
 *
 * @author Dbarreto
 */
public class VerificacionForm extends CssLayout implements View{
    TextField txtfIdVerificacion = new TextField("Nº Verificación");
    DateField dtfFechaVerificacion = new DateField("Fecha Verificación");
    TextField txtfHoraRecepcion = new TextField("Hora recepción", "Ingrese hora");
    TextField txtfHoraRemision = new TextField("Hora remisión", "Ingrese hora");
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    ComboBox<Modalidad> cmbModalidad = new ComboBox<>("Modalidad");
    
    VerticalLayout identificador = new VerticalLayout();
    TextField txtfCedula = new TextField("Nº Cédula/RUC");
    TextField txtfNumeroSolicitud = new TextField("Nº Solicitud");
    TextField txtfNumeroOperacion = new TextField("Nº Operación");
    DateField dtfFechaOperacion = new DateField("Fecha Operación");
    TextField txtfNumeroContrato = new TextField("Nº Contrato");
    TextField txtfNumeroBoleta = new TextField("Nº Boleta");
    TextField txtfNumeroOrdenCredito = new TextField("Nº Orden de Crédito");
    TextField txtfLocal = new TextField("Local");
    TextField txtfPuntoExpedicion = new TextField("Punto de Expedición");
    TextField txtfNumeroFactura = new TextField("Nº Factura");
    TextField txtfNumeroNotaCredito = new TextField("Nº Nota de Crédito");
    TextField txtfNumeroNotaPedido = new TextField("Nº Nota de Pedido");
    TextField txtfNumeroOrdenPago = new TextField("Nº Orden de Pago");
    TextField txtfNumeroCheque = new TextField("Nº Cheque");
    TextField txtfNumeroPlanilla = new TextField("Nº Planilla");
    TextField txtfNumeroCuenta = new TextField("Nº de Cuenta");
    TextField txtfNombreComercio = new TextField("Entidad");
    TextField txtfMes = new TextField("Mes");
    TextField txtfPeriodo = new TextField("Periodo");
    ComboBox<Regional> cmbRegional = new ComboBox<>("Regional");
    ComboBox<Procedencia> cmbProcedencia = new ComboBox<>("Procedencia");
    ComboBox<Funcionario> cmbResponsable = new ComboBox<>("Responsable");
    ComboBox<Funcionario> cmbResponsable2 = new ComboBox<>("Responsable2");
    ComboBox<Funcionario> cmbAprueba = new ComboBox<>("Aprueba");
    ComboBox<Autorizador> cmbAutorizador = new ComboBox<>("Autoriza");
    TextArea txtfObservaciones = new TextArea("Observaciones", "Ingrese Observación");
    ComboBox<Funcionario> cmbVerificador = new ComboBox<>("Verificador");
    DateField dtfFechaCorreccion = new DateField("Fecha Corrección");
    TextArea txtfCorreccion = new TextArea("Obs. Corrección", "Ingrese Observación");
    ComboBox<Dependencia> cmbDependencia = new ComboBox<>("Dependencia");
    ComboBox<Estadoverificacion> cmbEstadoVerificacion = new ComboBox<>("Estado");
    DateField dtfFechaRevision = new DateField("Fecha Revisión");
    TextArea txtfRevision = new TextArea("Obs. Revisión", "Ingrese Observación");
    ComboBox<Funcionario> cmbRevisador = new ComboBox<>("Revisado por");
    DateField dtfFechaAnulado = new DateField("Fecha Anulado");
    TextArea txtfObservacionAnulado = new TextArea("Obs. Anulado", "Ingrese Observación");
    Button btnAgregarRequisitos = new Button("Agregar requisitos");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    Button btnImprimir = new Button("Imprimir");
    Tipooperacion tipoOperacion;
    VerificacionDao verificacionDao = ResourceLocator.locate(VerificacionDao.class);
    Binder<Verificacion> binder = new Binder<>(Verificacion.class);
    
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    TipooperaciondetalleDao tipooperaciondetalleDao = ResourceLocator.locate(TipooperaciondetalleDao.class);
    ModalidadDao modalidadDao = ResourceLocator.locate(ModalidadDao.class);
    RegionalDao regionalDao = ResourceLocator.locate(RegionalDao.class);
    ProcedenciaDao procedenciaDao = ResourceLocator.locate(ProcedenciaDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    AutorizadorDao autorizadorDao = ResourceLocator.locate(AutorizadorDao.class);
    DependenciaDao dependenciaDao = ResourceLocator.locate(DependenciaDao.class);
    EstadoverificacionDao estadoverificacionDao = ResourceLocator.locate(EstadoverificacionDao.class);
    RequisitooperacionDao requisitooperacionDao = ResourceLocator.locate(RequisitooperacionDao.class);
    
    Grid<Verificaciondetalle> grillaVerificaciondetalle = new Grid<>(Verificaciondetalle.class);
    VerificaciondetalleDao verificaciondetalleDao = ResourceLocator.locate(VerificaciondetalleDao.class);
    VerificaciondetalleForm verificaciondetalleForm = new VerificaciondetalleForm();
    List<Verificaciondetalle> listaDetalle = new ArrayList<>();
    ListaDetalleVerificacionCache cache = ResourceLocator.locate(ListaDetalleVerificacionCache.class);
    //Grilla provisoria para nuevos detalles
    Grid<DetalleVerificacionDTO> grillaVerificaciondetalleDTO = new Grid<>(DetalleVerificacionDTO.class);
    List<DetalleVerificacionDTO> listaDetalleDTO = new ArrayList<>();
    Verificacion verificacion;
    
    private Consumer<Verificacion> guardarListener;
    private Consumer<Verificacion> borrarListener;
    private Consumer<Verificacion> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "verificacion";
            
            
    public VerificacionForm() {
        try {
            setSizeFull();
            addStyleName("crud-view");
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            HorizontalLayout formPrincipal = new HorizontalLayout();
            FormLayout panelIzquierdo = new FormLayout();
            FormLayout panelDerecho = new FormLayout();
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            btnAgregarRequisitos.addStyleName(MaterialTheme.BUTTON_PRIMARY);
            btnImprimir.addStyleName(MaterialTheme.BUTTON_PRIMARY);
            cmbTipooperacion.setWidth("101%");
            cmbModalidad.setWidth("101%");
            cmbVerificador.setWidth("101%");
            cmbDependencia.setWidth("101%");
            cmbRevisador.setWidth("101%");
            cmbAprueba.setWidth("101%");
            cmbAutorizador.setWidth("101%");
            cmbRegional.setWidth("101%");
            cmbProcedencia.setWidth("101%");
            cmbResponsable.setWidth("101%");
            cmbResponsable2.setWidth("101%");
            txtfObservaciones.setWidth("101%");
            txtfCorreccion.setWidth("101%");
            txtfRevision.setWidth("101%");
            txtfObservacionAnulado.setWidth("101%");
            panelIzquierdo.addComponents(txtfIdVerificacion, dtfFechaVerificacion, cmbEstadoVerificacion,  
                    txtfHoraRecepcion, txtfHoraRemision, cmbTipooperacion, identificador, cmbRegional, cmbVerificador, 
                    btnAgregarRequisitos, dtfFechaCorreccion, cmbDependencia, cmbResponsable, cmbResponsable2, cmbAutorizador);
            panelDerecho.addComponents(dtfFechaRevision, cmbRevisador, txtfObservaciones, txtfCorreccion, 
                    txtfRevision, dtfFechaAnulado, txtfObservacionAnulado, btnImprimir);
            formPrincipal.addComponentsAndExpand(panelIzquierdo, panelDerecho);
            formPrincipal.setComponentAlignment(panelIzquierdo, Alignment.TOP_LEFT);
            formPrincipal.setComponentAlignment(panelDerecho, Alignment.TOP_RIGHT);
            formPrincipal.setSpacing(true);
            
            binder.forField(txtfIdVerificacion)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getIdverificacion, Verificacion::setIdverificacion);
            txtfIdVerificacion.setEnabled(false);
            
            binder.bind(dtfFechaVerificacion, No.getter(), No.setter());
            
            binder.bind(dtfFechaOperacion, No.getter(), No.setter());
            
            List<Estadoverificacion> listaEstadoverificacions = estadoverificacionDao.listaEstadoverificacion();
            cmbEstadoVerificacion.setItems(listaEstadoverificacions);
            cmbEstadoVerificacion.setItemCaptionGenerator(Estadoverificacion::getDescripcion);
            
            binder.bind(cmbEstadoVerificacion, 
                    (Verificacion source) -> source.getIdestadoverificacion(), 
                    (Verificacion bean, Estadoverificacion fieldvalue) -> { 
                        bean.setIdestadoverificacion(fieldvalue);});
            
            binder.bind(txtfHoraRecepcion, Verificacion::getHorarecepcion, Verificacion::setHorarecepcion);
            binder.forField(txtfHoraRecepcion).withNullRepresentation("")
                    .bind(Verificacion::getHorarecepcion, Verificacion::setHorarecepcion);
            
            binder.bind(txtfHoraRemision, Verificacion::getHoraremision, Verificacion::setHoraremision);
            binder.forField(txtfHoraRemision).withNullRepresentation("")
                    .bind(Verificacion::getHoraremision, Verificacion::setHoraremision);
            
            binder.bind(txtfCedula, Verificacion::getCedula, Verificacion::setCedula);
            binder.forField(txtfCedula).withNullRepresentation("")
                    .bind(Verificacion::getCedula, Verificacion::setCedula);
            
            binder.bind(txtfLocal, Verificacion::getLocal, Verificacion::setLocal);
            binder.forField(txtfLocal).withNullRepresentation("")
                    .bind(Verificacion::getLocal, Verificacion::setLocal);
            
            binder.bind(txtfPuntoExpedicion, Verificacion::getPuntoexpedicion, Verificacion::setPuntoexpedicion);
            binder.forField(txtfPuntoExpedicion).withNullRepresentation("")
                    .bind(Verificacion::getPuntoexpedicion, Verificacion::setPuntoexpedicion);
            
            binder.bind(txtfNombreComercio, Verificacion::getNombrecomercio, Verificacion::setNombrecomercio);
            binder.forField(txtfNombreComercio).withNullRepresentation("")
                    .bind(Verificacion::getNombrecomercio, Verificacion::setNombrecomercio);
            
            binder.bind(txtfNumeroCuenta, Verificacion::getNumerocuenta, Verificacion::setNumerocuenta);
            binder.forField(txtfNumeroCuenta).withNullRepresentation("")
                    .bind(Verificacion::getNumerocuenta, Verificacion::setNumerocuenta);
            
            binder.bind(txtfMes, Verificacion::getMes, Verificacion::setMes);
            binder.forField(txtfMes).withNullRepresentation("")
                    .bind(Verificacion::getMes, Verificacion::setMes);
            
            binder.bind(txtfPeriodo, Verificacion::getPeriodo, Verificacion::setPeriodo);
            binder.forField(txtfPeriodo).withNullRepresentation("")
                    .bind(Verificacion::getPeriodo, Verificacion::setPeriodo);
            
            List<Tipooperacion> listaTipooperaciones = tipooperacionDao.listaTipooperacion();
            cmbTipooperacion.setItems(listaTipooperaciones);
            cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
            cmbTipooperacion.addValueChangeListener(e -> {
                if (e.getValue()!= null) {
                    tipoOperacion = e.getValue();
                }
                if (cmbTipooperacion.getValue()!=null){
                    mostrarIdentificador();
                    List<Modalidad> listaModalidads = modalidadDao.getListTipooperacion(cmbTipooperacion.getValue().getIdtipooperacion().toString());
                    cmbModalidad.setItems(listaModalidads);
                    cmbModalidad.setItemCaptionGenerator(Modalidad::getDescripcion);
                }
            });
            binder.bind(cmbTipooperacion, 
                    (Verificacion source) -> source.getIdtipooperacion(),
                    (Verificacion bean, Tipooperacion fieldvalue) -> {
                        bean.setIdtipooperacion(fieldvalue);});
            
            //List<Modalidad> listaModalidads = modalidadDao.listaModalidad();
            //cmbModalidad.setItems(listaModalidads);
            //cmbModalidad.setItemCaptionGenerator(Modalidad::getDescripcion);
            binder.bind(cmbModalidad, 
                    (Verificacion source) -> source.getIdmodalidad(), 
                    (Verificacion bean, Modalidad fieldvalue) -> {
                        bean.setIdmodalidad(fieldvalue);});
            
            
            List<Regional> listaRegions = regionalDao.listaRegional();
            cmbRegional.setItems(listaRegions);
            cmbRegional.setItemCaptionGenerator(Regional::getDescripcion);
            binder.bind(cmbRegional, 
                    (Verificacion source) -> source.getIdregional(),
                    (Verificacion bean, Regional fieldvalue) -> {
                       bean.setIdregional(fieldvalue);});
            
            List<Procedencia> listaProcedencias = procedenciaDao.listaProcedencia();
            cmbProcedencia.setItems(listaProcedencias);
            cmbProcedencia.setItemCaptionGenerator(Procedencia::getDescripcion);
            binder.bind(cmbProcedencia, 
                    (Verificacion source) -> source.getIdprocedencia(),
                    (Verificacion bean, Procedencia fieldvalue) -> {
                        bean.setIdprocedencia(fieldvalue);});
            
            binder.forField(txtfNumeroSolicitud)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumerosolicitud, Verificacion::setNumerosolicitud);
            
            binder.forField(txtfNumeroOperacion)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumerooperacion, Verificacion::setNumerooperacion);
            
            binder.forField(txtfNumeroContrato)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumerocontrato, Verificacion::setNumerocontrato);
            
            binder.forField(txtfNumeroBoleta)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeroboleta, Verificacion::setNumeroboleta);
            
            binder.forField(txtfNumeroOrdenCredito)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeroordencredito, Verificacion::setNumeroordencredito);
            
            binder.forField(txtfNumeroFactura)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumerofactura, Verificacion::setNumerofactura);
            
            binder.forField(txtfNumeroNotaCredito)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeronotacredito, Verificacion::setNumeronotacredito);
            
            binder.forField(txtfNumeroNotaPedido)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeronotapedido, Verificacion::setNumeronotapedido);
            
            binder.forField(txtfNumeroOrdenPago)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeroordenpago, Verificacion::setNumeroordenpago);
            
            binder.forField(txtfNumeroCheque)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumerocheque, Verificacion::setNumerocheque);
            
            binder.forField(txtfNumeroPlanilla)
                    .withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(Verificacion::getNumeroplanilla, Verificacion::setNumeroplanilla);
            
            binder.bind(txtfObservaciones, Verificacion::getObservaciones, Verificacion::setObservaciones);
            binder.forField(txtfObservaciones).withNullRepresentation("")
                    .bind(Verificacion::getObservaciones, Verificacion::setObservaciones);
            
            List<Funcionario> listaFuncionarios = funcionarioDao.listaFuncionario();
            cmbVerificador.setItems(listaFuncionarios);
            cmbVerificador.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbVerificador, 
                    (Verificacion source) -> source.getIdverificador(), 
                    (Verificacion bean, Funcionario fieldvalue) -> {
                        bean.setIdverificador(fieldvalue);});
            cmbVerificador.setEnabled(false);
            
            binder.bind(dtfFechaCorreccion, No.getter(), No.setter());
            
            binder.bind(txtfCorreccion, Verificacion::getCorreccion, Verificacion::setCorreccion);
            binder.forField(txtfCorreccion).withNullRepresentation("")
                    .bind(Verificacion::getCorreccion, Verificacion::setCorreccion);
            
            List<Dependencia> listaDependencias = dependenciaDao.listaDependencia();
            cmbDependencia.setItems(listaDependencias);
            cmbDependencia.setItemCaptionGenerator(Dependencia::getDescripcion);
            binder.bind(cmbDependencia, 
                    (Verificacion source) -> source.getIddependencia(), 
                    (Verificacion bean, Dependencia fieldvalue) -> {
                        bean.setIddependencia(fieldvalue);});
            
            List<Funcionario> listaResponsables = funcionarioDao.listaFuncionario();
            cmbResponsable.setItems(listaResponsables);
            cmbResponsable.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbResponsable, 
                    (Verificacion source) -> source.getIdresponsable(), 
                    (Verificacion bean, Funcionario fieldvalue) -> {
                        bean.setIdresponsable(fieldvalue);});
            
            List<Funcionario> listaResponsables2 = funcionarioDao.listaFuncionario();
            cmbResponsable2.setItems(listaResponsables2);
            cmbResponsable2.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbResponsable2, 
                    (Verificacion source) -> source.getIdresponsable2(), 
                    (Verificacion bean, Funcionario fieldvalue) -> {
                        bean.setIdresponsable2(fieldvalue);});
            
            cmbAprueba.setItems(listaResponsables);
            cmbAprueba.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbAprueba, 
                    (Verificacion source) -> source.getIdaprueba(),
                    (Verificacion bean, Funcionario fieldvalue) -> {
                        bean.setIdaprueba(fieldvalue);});
            
            List<Autorizador> listaAutorizadors = autorizadorDao.listaAutorizador();
            cmbAutorizador.setItems(listaAutorizadors);
            cmbAutorizador.setItemCaptionGenerator(Autorizador::getNombreFuncionario);
            binder.bind(cmbAutorizador, 
                    (Verificacion source) -> source.getIdautorizador(),
                    (Verificacion bean, Autorizador fieldvalue) -> {
                        bean.setIdautorizador(fieldvalue);});
            
            binder.bind(dtfFechaRevision, No.getter(), No.setter());
            
            binder.bind(txtfRevision, Verificacion::getRevisionfinal, Verificacion::setRevisionfinal);
            binder.forField(txtfRevision).withNullRepresentation("")
                    .bind(Verificacion::getRevisionfinal, Verificacion::setRevisionfinal);
            
            List<Funcionario> listaFuncionarios1 = funcionarioDao.listaFuncionario();
            cmbRevisador.setItems(listaFuncionarios1);
            cmbRevisador.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbRevisador, 
                    (Verificacion source) -> source.getIdrevisador(), 
                    (Verificacion bean, Funcionario fieldvalue) -> {
                        bean.setIdrevisador(fieldvalue);});
            
            binder.bind(dtfFechaAnulado, No.getter(), No.setter());
            
            binder.bind(txtfObservacionAnulado, Verificacion::getObservacionanulado, Verificacion::setObservacionanulado);
            binder.forField(txtfObservacionAnulado).withNullRepresentation("")
                    .bind(Verificacion::getObservacionanulado, Verificacion::setObservacionanulado);
            
            binder.bindInstanceFields(this);

            /*try {
                listaDetalle = verificaciondetalleDao.getListVerificaciondetalle(verificacion.getIdverificacion());
            } catch (Exception e) {
                e.printStackTrace(); 
            }*/
            btnAgregarRequisitos.setEnabled(listaDetalle.isEmpty());
            //btnGuardar.setEnabled(!listaDetalle.isEmpty());
           
            //### Poblar Grillar Requisitos ###...
            poblarGrillaRequisitos();
            mostrarDetalle(listaDetalle);
            
            grillaVerificaciondetalle.addItemClickListener(e ->{
                if (e.getMouseEventDetails().isDoubleClick()) {
                    Window subVentana = new Window("Detalle de Verificación");
                    FormLayout subContenido = new FormLayout();
                    VerticalLayout vertical = new VerticalLayout();
                    HorizontalLayout horizontal = new HorizontalLayout();
                    CheckBox chkVerificado = new CheckBox("Correcto");
                    CheckBox chkNoVerificado = new CheckBox("Incorrecto");
                    CheckBox chkIndiferente = new CheckBox("Indiferente");
                    TextArea txtfObservacion = new TextArea("Observación");
                    txtfObservacion.setWidth("101%");
                    txtfObservacion.addValueChangeListener(o ->{txtfObservacion.setValue(txtfObservacion.getValue().toUpperCase());});
                    Button btnGuardar = new Button("Guardar");
                    Button btnBorrar = new Button("Borrar");
                    Button btnCancelar = new Button("Cancelar");
                    
                    btnGuardar.addStyleName(ValoTheme.BUTTON_PRIMARY);
                    btnBorrar.addStyleName(ValoTheme.BUTTON_DANGER);
                    subVentana.setWidth("50%");
                    subVentana.setHeight("65%");
                    
                    subContenido.addComponent(new Label(e.getItem().getIdrequisito().getDescripcion()));
                    if (e.getItem().getVerificado()!=null) chkVerificado.setValue(e.getItem().getVerificado());
                    if (e.getItem().getNoverificado()!=null) chkNoVerificado.setValue(e.getItem().getNoverificado());
                    if (e.getItem().getIndiferente()!=null) chkIndiferente.setValue(e.getItem().getIndiferente());
                    subContenido.addComponent(chkVerificado);
                    subContenido.addComponent(chkNoVerificado);
                    subContenido.addComponent(chkIndiferente);
                    txtfObservacion.setValue(e.getItem().getObservacion());
                    subContenido.addComponent(txtfObservacion);
                    // Agregar componetes a la subVentana
                    vertical.addComponents(subContenido, horizontal);
                    horizontal.addComponents(btnGuardar, btnBorrar, btnCancelar);
                    subVentana.setContent(vertical);
                    
                    chkVerificado.addValueChangeListener(ch -> {
                        if (ch.getValue()) {
                            chkVerificado.setValue(ch.getValue());
                            chkNoVerificado.setValue(Boolean.FALSE);
                            chkIndiferente.setValue(Boolean.FALSE);
                        }
                    });
                    
                    chkNoVerificado.addValueChangeListener(ch -> {
                        if (ch.getValue()) {
                            chkNoVerificado.setValue(ch.getValue());
                            chkVerificado.setValue(Boolean.FALSE);
                            chkIndiferente.setValue(Boolean.FALSE);
                        }
                    });

                    chkIndiferente.addValueChangeListener(ch -> {
                        if (ch.getValue()) {
                            chkIndiferente.setValue(ch.getValue());
                            chkVerificado.setValue(Boolean.FALSE);
                            chkNoVerificado.setValue(Boolean.FALSE);
                        }
                    });

                    btnCancelar.addClickListener(c -> {subVentana.close();});
                    
                    btnGuardar.addClickListener(g -> {
                        String rviejo, rnuevo;
                        Verificaciondetalle verificaciondetalle = e.getItem();
                        //rviejo="("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito()+","+
                        //        verificaciondetalle.getIdverificacion()+","+verificaciondetalle.getVerificado()+","+
                        //        verificaciondetalle.getObservacion()+","+verificaciondetalle.getIndiferente()+","+
                        //        verificaciondetalle.getNoverificado()+")";
                        verificaciondetalle.setVerificado(chkVerificado.getValue());
                        verificaciondetalle.setNoverificado(chkNoVerificado.getValue());
                        verificaciondetalle.setIndiferente(chkIndiferente.getValue());
                        verificaciondetalle.setObservacion(txtfObservacion.getValue());
                        //verificaciondetalleDao.guardar(verificaciondetalle);
                        cache.cargarDetalle(verificaciondetalle);
                        subVentana.close();
                        listaDetalle = cache.getLista();
                                //verificaciondetalleDao.getListVerificaciondetalle(verificacion.getIdverificacion());
                        mostrarDetalle(listaDetalle);
                        //agregarRequisitos(listaDetalle, false);
                        //rnuevo="("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito()+","+
                        //        verificaciondetalle.getIdverificacion()+","+verificaciondetalle.getVerificado()+","+
                        //        verificaciondetalle.getObservacion()+","+verificaciondetalle.getIndiferente()+","+
                        //        verificaciondetalle.getNoverificado()+")";
                        //AuditoriaRegistrar.guardarAuditoria("verificaciondetalle", "M", rviejo, rnuevo);
                    });
                    
                    btnBorrar.addClickListener(b ->{
                        Verificaciondetalle verificaciondetalle = e.getItem();
                        verificaciondetalleDao.borrar(verificaciondetalle);
                        subVentana.close();
                        listaDetalle = verificaciondetalleDao.getListVerificaciondetalle(verificacion.getIdverificacion());
                        mostrarDetalle(listaDetalle);
                        //String rviejo="("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito()+","+
                        //        verificaciondetalle.getIdverificacion()+","+verificaciondetalle.getVerificado()+","+
                        //        verificaciondetalle.getObservacion()+","+verificaciondetalle.getIndiferente()+","+
                        //        verificaciondetalle.getNoverificado()+")";
                        //AuditoriaRegistrar.guardarAuditoria("verificaciondetalle", "E", rviejo, "");
                    });
                    // Centrar en la subVentana del browser
                    subVentana.center();
                    // Abrir la subVentana en la UI
                    this.getUI().addWindow(subVentana);
                }
            });
            
            txtfObservaciones.addValueChangeListener(e -> {txtfObservaciones.setValue(txtfObservaciones.getValue().toUpperCase());});
            txtfCorreccion.addValueChangeListener(e -> {txtfCorreccion.setValue(txtfCorreccion.getValue().toUpperCase());});
            txtfRevision.addValueChangeListener(e -> {txtfRevision.setValue(txtfRevision.getValue().toUpperCase());});
            txtfObservacionAnulado.addValueChangeListener(e -> {txtfObservacionAnulado.setValue(txtfObservacionAnulado.getValue().toUpperCase());});
            
            btnAgregarRequisitos.addClickListener(e ->{agregarRequisitos(null, true);});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{borrar();});
            
            btnImprimir.addClickListener(e ->{imprimir();});
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(verificacion);
            });
            
            VerticalLayout verticalLayout = new VerticalLayout();
            grillaVerificaciondetalleDTO.setVisible(false);
            verticalLayout.addComponents(formPrincipal, grillaVerificaciondetalle,grillaVerificaciondetalleDTO, botones);
            Panel panel = new Panel();
            panel.setContent(verticalLayout);
            panel.setSizeFull();
            panel.setScrollTop(4540);
            addComponent(panel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mostrarDetalle(List<Verificaciondetalle> verificaciondetalles) {
        try {
            if (verificaciondetalles==null || verificaciondetalles.isEmpty()) {
                grillaVerificaciondetalle.clearSortOrder();
                btnAgregarRequisitos.setEnabled(true);
            } else {
                grillaVerificaciondetalle.clearSortOrder();
                grillaVerificaciondetalle.setItems(verificaciondetalles);
                btnAgregarRequisitos.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void agregarRequisitos(List<Verificaciondetalle> verificaciondetalles, boolean click) {
        List<Requisitooperacion> listarequisitos = new ArrayList<>();
        try{
            if (verificaciondetalles == null || verificaciondetalles.isEmpty()){
                if (tipoOperacion  != null && click) {
                    listarequisitos = requisitooperacionDao.getRequisitoOperacionByTipoOperacion(tipoOperacion.getIdtipooperacion());
                    if(listarequisitos.isEmpty()){
                        grillaVerificaciondetalle.clearSortOrder();
                        return;
                    }
                    btnGuardar.setEnabled(true);
                    //btnAgregarRequisitos.setEnabled(false);
                }else{
                    grillaVerificaciondetalle.setItems(listaDetalle);
                    grillaVerificaciondetalle.clearSortOrder();  
                    return;
                }   
                listaDetalleDTO.clear();
                for (Requisitooperacion requisitoOperacion : listarequisitos) {
                    DetalleVerificacionDTO verificaciondetalle = new DetalleVerificacionDTO();
                    verificaciondetalle.setVerificado(Boolean.FALSE);
                    verificaciondetalle.setNoverificado(Boolean.FALSE);
                    verificaciondetalle.setIndiferente(Boolean.FALSE);
                    verificaciondetalle.setObservacion("");
                    verificaciondetalle.setIdrequisito(requisitoOperacion.getIdrequisito());
                    listaDetalleDTO.add(verificaciondetalle);
                } 
                grillaVerificaciondetalleDTO.clearSortOrder(); 
                grillaVerificaciondetalleDTO.setItems(listaDetalleDTO);
                poblarGrillaRequisitosDTO();
                grillaVerificaciondetalle.setVisible(false);
                grillaVerificaciondetalleDTO.setVisible(true);
                btnAgregarRequisitos.setEnabled(false);
                cache.setLista(listaDetalle);
            }else {
                grillaVerificaciondetalle.setItems(verificaciondetalles);
                grillaVerificaciondetalle.setVisible(true);
                grillaVerificaciondetalleDTO.setVisible(false);
                btnAgregarRequisitos.setEnabled(false);
                cache.setLista(verificaciondetalles);
            }
            //poblarGrillaRequisitos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    private void mostrarIdentificador() {
        identificador.removeAllComponents();
        if(cmbTipooperacion.getValue().getDescripcion().contains("ESTADO DE CUENTA")){
            identificador.addComponents(txtfCedula, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("INGRESOS POR CAJA")) {
            identificador.addComponents(txtfNumeroBoleta, txtfCedula, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("CDA")) {
            identificador.addComponents(txtfNumeroContrato, cmbModalidad, txtfMes);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("REGULARIZACIONES")) {
            identificador.addComponents(txtfNumeroSolicitud, txtfCedula, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("AHORRO PROGRAMADO")){
            identificador.addComponents(txtfNumeroContrato, txtfCedula, cmbProcedencia, txtfMes);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("CONTRATO DE AHORRO")){
            identificador.addComponents(txtfNumeroContrato, txtfCedula, cmbProcedencia, txtfMes);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("ORDEN DE CREDITO")) { 
             identificador.addComponents(txtfNumeroBoleta, txtfNumeroOrdenCredito, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().equals("PRESTAMO")) {
            identificador.addComponents(txtfNumeroSolicitud, txtfNumeroBoleta, txtfLocal, txtfPuntoExpedicion, txtfNumeroFactura, txtfNumeroNotaCredito, cmbModalidad, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("NOTA DE PEDIDO")) { 
            identificador.addComponents(txtfNumeroNotaPedido, txtfLocal, txtfPuntoExpedicion, txtfNumeroFactura);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("JURIDICO")) {
            identificador.addComponents(txtfNumeroPlanilla, txtfCedula);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("FONDOS A RENDIR")) {
            identificador.addComponents(txtfNumeroOrdenPago, txtfNumeroCheque);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("CHEQUES")) {
            identificador.addComponents(txtfNumeroOrdenPago, txtfNumeroCheque);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("ARQUEO DE LEGAJOS")) {
            identificador.addComponents(dtfFechaOperacion);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("FONDO FIJO")) {
            identificador.addComponents(txtfNumeroPlanilla);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("REEMBOLSOS VARIOS")) {
            identificador.addComponents(txtfCedula);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("SUBSIDIO")) {
            identificador.addComponents(txtfCedula, cmbModalidad, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("RENUNCIA")) {
            identificador.addComponents(txtfCedula, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("REEMBOLSO DE AHORRO")) {
            identificador.addComponents(txtfCedula);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("INGRESO DE SOCIO")) {
            identificador.addComponents(txtfCedula);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PAGARE")) {
            identificador.addComponents(txtfCedula, txtfNumeroSolicitud, txtfNumeroOperacion, dtfFechaOperacion, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE REPROGRAMACIONES")) { 
            identificador.addComponents(txtfMes, txtfCedula, txtfNumeroSolicitud, txtfNumeroOperacion);
        } else if (cmbTipooperacion.getValue().getDescripcion().equals("REPROGRAMACION")) { 
            identificador.addComponents(txtfCedula, txtfNumeroSolicitud, txtfLocal, txtfPuntoExpedicion, txtfNumeroFactura);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("FALLECIDOS")) { 
            identificador.addComponents(txtfCedula);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PAGOS VARIOS")) {
            identificador.addComponents(txtfNumeroOrdenPago, cmbModalidad, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE PRESTAMO")) { 
            identificador.addComponents(txtfNumeroOrdenPago, txtfNumeroOperacion, cmbModalidad, cmbProcedencia);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("COMERCIOS")) { 
            identificador.addComponents(txtfNombreComercio, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("CONCILIACIONES BANCARIAS")) { 
            identificador.addComponents(txtfMes, txtfNombreComercio, txtfNumeroCuenta);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("GRATIFICACION POR PRODUCCION")) { 
            identificador.addComponents(txtfMes, txtfNumeroPlanilla);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("DOCUMENTOS DE CIERRE")) { 
            identificador.addComponents(txtfMes);       
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("LIQUIDACION DE IVA")) { 
            identificador.addComponents(txtfMes);       
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("LIBRO")) { 
            identificador.addComponents(txtfLocal, txtfPuntoExpedicion, txtfNumeroFactura, dtfFechaOperacion, txtfNombreComercio, txtfCedula);       
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PRODUCCION DE PROMOTORES")) { 
            identificador.addComponents(txtfMes);       
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PLANILLAS SIN DESCUENTO")) { 
            identificador.addComponents(txtfMes);       
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PLANILLAS IPS")) { 
            identificador.addComponents(txtfMes);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("DOCUMENTOS DE RRHH")) { 
            identificador.addComponents(txtfMes, cmbModalidad);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PLANILLA AGUINALDO")) {
            identificador.addComponents(txtfPeriodo);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("RECIBO DE SUELDO")) {    
            identificador.addComponents(txtfPeriodo);
        } else if (cmbTipooperacion.getValue().getDescripcion().contains("PLANILLA DE SUELDOS")) {
            identificador.addComponents(txtfPeriodo);
        }
    }

    private Object cargarDescripcionLarga() {
        Object valor="";
        if(verificacion.getIdtipooperacion().getDescripcion().contains("ESTADO DE CUENTA")){
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula(); 
            valor+="        Modalidad: "+verificacion.getIdmodalidad().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("REGULARIZACIONES")) {
            valor="Nº Solicitud: "+verificacion.getNumerosolicitud() == null ? 0 : verificacion.getNumerosolicitud();
            valor+="    Cédula: "+verificacion.getCedula()+"   Modalidad: "+verificacion.getIdmodalidad().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("AHORRO PROGRAMADO")){ 
            valor="Nº Contrato: "+verificacion.getNumerocontrato()== null ? 0 : verificacion.getNumerocontrato();
            valor+="   Cédula: "+verificacion.getCedula()==null ? " " : verificacion.getCedula();
            valor+="   Procedencia: "+verificacion.getIdprocedencia().getDescripcion()==null ? " " : verificacion.getIdprocedencia().getDescripcion();
            valor+="   Mes: "+verificacion.getMes()==null ? " " : verificacion.getMes();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("CONTRATO DE AHORRO")){ 
            valor="Nº Contrato: "+verificacion.getNumerocontrato()== null ? 0 : verificacion.getNumerocontrato();
            valor+="   Cédula: "+verificacion.getCedula()==null ? " " : verificacion.getCedula();
            valor+="   Procedencia: "+verificacion.getIdprocedencia().getDescripcion()==null ? " " : verificacion.getIdprocedencia().getDescripcion();
            valor+="   Mes: "+verificacion.getMes()==null ? " " : verificacion.getMes();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("ORDEN DE CREDITO")) {  
            valor="Nº Boleta: "+verificacion.getNumeroboleta() == null ? 0 : verificacion.getNumeroboleta(); 
            valor+="   Nº Orden de Crédito: "+verificacion.getNumeroordencredito()+"    Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("INGRESOS POR CAJA")) {  
            valor="Nº Boleta: "+verificacion.getNumeroboleta() == null ? 0 : verificacion.getNumeroboleta(); 
            valor+="    Cédula: "+verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("CDA")) {  
            valor="Nº Contrato: "+verificacion.getNumerocontrato() == null ? 0 : verificacion.getNumerocontrato(); 
            valor+="   Modalidad: "+verificacion.getIdmodalidad().getDescripcion()+"    Mes: "+verificacion.getMes();
        } else if (verificacion.getIdtipooperacion().getDescripcion().equals("PRESTAMO")) {
            valor="Nº Solicitud: "+verificacion.getNumerosolicitud() == null ? 0 : verificacion.getNumerosolicitud(); 
            valor+="    Nº Boleta: "+verificacion.getNumeroboleta()+"    Nº Factura: "+verificacion.getLocal()+"-"+verificacion.getPuntoexpedicion()+"-"+verificacion.getNumerofactura()+"   Nº Nota de Crédito: "+verificacion.getNumeronotacredito()+"   Modalidad: "+verificacion.getIdmodalidad().getDescripcion()+"  Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("NOTA DE PEDIDO")) { 
            valor="Nº Nota de Pedido: "+verificacion.getNumeronotapedido() == null ? 0 : verificacion.getNumeronotapedido();
            valor+="   Nº Factura: "+verificacion.getLocal()+"-"+verificacion.getPuntoexpedicion()+"-"+verificacion.getNumerofactura();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("JURIDICO")) {
            valor="Nº Planilla: "+verificacion.getNumeroplanilla() == null ? 0 : verificacion.getNumeroplanilla(); 
            valor+="   Cédula: "+verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("FONDOS A RENDIR")) {
            valor="Nº Orden de Pago: "+verificacion.getNumeroordenpago() == null ? 0 : verificacion.getNumeroordenpago(); 
            valor+="   Nº de Cheque: "+verificacion.getNumerocheque();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("CHEQUES")) {
            valor="Nº Orden de Pago: "+verificacion.getNumeroordenpago() == null ? 0 : verificacion.getNumeroordenpago(); 
            valor+="   Nº de Cheque: "+verificacion.getNumerocheque();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("ARQUEO DE LEGAJOS")) {
            valor="Fecha: "+verificacion.getFechaoperacion() == null ? " " : verificacion.getFechaoperacion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("FONDO FIJO")) { 
            valor="Nº Planilla: "+verificacion.getNumeroplanilla() == null ? 0 : verificacion.getNumeroplanilla();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("REEMBOLSOS VARIOS")) {
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("SUBSIDIO")) {
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula();
            valor+="   Modalidad: "+verificacion.getIdmodalidad().getDescripcion()+"   Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("RENUNCIA")) {
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula(); 
            valor+="   Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("REEMBOLSO DE AHORRO")) { 
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("INGRESO DE SOCIO")) { 
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PAGARE")) { 
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula(); 
            valor+="   Nº Solicitud: "+verificacion.getNumerosolicitud()+"   Nº Operación: "+verificacion.getNumerooperacion()+"   Fecha: "+verificacion.getFechaoperacion()+"   Modalidad: "+verificacion.getIdmodalidad().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("LEGAJO DE REPROGRAMACIONES")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes(); 
            valor+="    Cédula: "+verificacion.getCedula()+"   Nº Solicitud: "+verificacion.getNumerosolicitud()+"   Nº Operación: "+verificacion.getNumerooperacion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("REPROGRAMACION")) { 
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula(); 
            valor+="   Nº Solicitud: "+verificacion.getNumerosolicitud()+"   Nº Factura: "+verificacion.getLocal()+"-"+verificacion.getPuntoexpedicion()+"-"+verificacion.getNumerofactura();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("FALLECIDOS")) { 
            valor="Cédula: "+verificacion.getCedula() == null ? " " : verificacion.getCedula();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PAGOS VARIOS")) { 
            valor="Nº Orden de Pago: "+verificacion.getNumeroordenpago() == null ? 0 : verificacion.getNumeroordenpago(); 
            valor+="   Modalidad: "+verificacion.getIdmodalidad().getDescripcion()+"  Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("LEGAJO DE PRESTAMO")) { 
            valor="Nº Orden de Pago: "+verificacion.getNumeroordenpago() == null ? 0 : verificacion.getNumeroordenpago(); 
            valor+="   Nº Operación: "+verificacion.getNumerooperacion()+"   Modalidad: "+verificacion.getIdmodalidad().getDescripcion()+"  Procedencia: "+verificacion.getIdprocedencia().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("COMERCIOS")) { 
            valor="Nombre del Comercio: "+verificacion.getNombrecomercio()== null ? " " : verificacion.getNombrecomercio(); 
            valor+="   Modalidad: "+verificacion.getIdmodalidad().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("CONCILIACIONES BANCARIAS")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes(); 
            valor+="    Banco: "+verificacion.getNombrecomercio()+"    Nº Cuenta: "+verificacion.getNumerocuenta();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("GRATIFICACION POR PRODUCCION")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes(); 
            valor+="    Nº Planilla: "+verificacion.getNumeroplanilla();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("DOCUMENTOS DE CIERRE")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes();       
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("LIQUIDACION DE IVA")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes();       
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("LIBRO")) { 
            valor="Nº Factura: "+verificacion.getLocal() == null ? " " : verificacion.getLocal(); 
            valor+="-"+verificacion.getPuntoexpedicion()+"-"+verificacion.getNumerofactura()+"   Fecha: "+verificacion.getFechaoperacion()+"   Prooveedor: "+verificacion.getNombrecomercio()+"   RUC: "+verificacion.getCedula();       
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PRODUCCION DE PROMOTORES")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes();       
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PLANILLAS SIN DESCUENTO")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes();       
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PLANILLAS IPS")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("DOCUMENTOS DE RRHH")) { 
            valor="Mes: "+verificacion.getMes() == null ? " " : verificacion.getMes(); 
            valor+="   Modalidad: "+verificacion.getIdmodalidad().getDescripcion();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PLANILLA AGUINALDO")) { 
            valor="Periodo: "+verificacion.getPeriodo() == null ? " " : verificacion.getPeriodo();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("RECIBO DE SUELDO")) {  
            valor="Periodo: "+verificacion.getPeriodo() == null ? " " : verificacion.getPeriodo();
        } else if (verificacion.getIdtipooperacion().getDescripcion().contains("PLANILLA DE SUELDOS")) { 
            valor="Periodo: "+verificacion.getPeriodo() == null ? " " : verificacion.getPeriodo();
        }
        return valor;
    }

    private void poblarGrillaRequisitos() {
        try {
            grillaVerificaciondetalle.removeAllColumns();
            grillaVerificaciondetalle.addColumn(requisito -> {
                return requisito.getIdrequisito().getDescripcion();}).setCaption("Requisito");
            grillaVerificaciondetalle.addComponentColumn(correcto -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(correcto.getVerificado()==null ? false : correcto.getVerificado());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       correcto.setVerificado(e.getValue());
                       correcto.setNoverificado(Boolean.FALSE);
                       correcto.setIndiferente(Boolean.FALSE);
                   }
                   else correcto.setVerificado(false);});
               return checkBox;
            }).setCaption("Correcto");
            grillaVerificaciondetalle.addComponentColumn(incorrecto -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(incorrecto.getNoverificado()==null ? false : incorrecto.getNoverificado());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       incorrecto.setNoverificado(e.getValue());
                       incorrecto.setVerificado(Boolean.FALSE);
                       incorrecto.setIndiferente(Boolean.FALSE);
                   }
                   else incorrecto.setNoverificado(false);});
               return checkBox;
            }).setCaption("Incorrecto");
            grillaVerificaciondetalle.addComponentColumn(indiferente -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(indiferente.getIndiferente()==null ? false : indiferente.getIndiferente());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       indiferente.setIndiferente(e.getValue());
                       indiferente.setVerificado(Boolean.FALSE);
                       indiferente.setNoverificado(Boolean.FALSE);
                   }
                   else indiferente.setIndiferente(false);});
               return checkBox;
            }).setCaption("Indiferente");
            grillaVerificaciondetalle.addColumn(Verificaciondetalle::getObservacion).setCaption("Observación");
            grillaVerificaciondetalle.setSizeFull();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void poblarGrillaRequisitosDTO() {
        try {
            grillaVerificaciondetalleDTO.removeAllColumns();
            grillaVerificaciondetalleDTO.addColumn(requisito -> {
                return requisito.getIdrequisito().getDescripcion();}).setCaption("Requisito");
            grillaVerificaciondetalleDTO.addComponentColumn(correcto -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(correcto.getVerificado()==null ? false : correcto.getVerificado());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       correcto.setVerificado(e.getValue());
                       correcto.setNoverificado(Boolean.FALSE);
                       correcto.setIndiferente(Boolean.FALSE);
                   }
                   else correcto.setVerificado(false);});
               return checkBox;
            }).setCaption("Correcto");
            grillaVerificaciondetalleDTO.addComponentColumn(incorrecto -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(incorrecto.getNoverificado()==null ? false : incorrecto.getNoverificado());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       incorrecto.setNoverificado(e.getValue());
                       incorrecto.setVerificado(Boolean.FALSE);
                       incorrecto.setIndiferente(Boolean.FALSE);
                   }
                   else incorrecto.setNoverificado(false);});
               return checkBox;
            }).setCaption("Incorrecto");
            grillaVerificaciondetalleDTO.addComponentColumn(indiferente -> {
               CheckBox checkBox = new CheckBox();
               checkBox.setValue(indiferente.getIndiferente()==null ? false : indiferente.getIndiferente());
               checkBox.addValueChangeListener(e -> {
                   if (e.getValue()) {
                       indiferente.setIndiferente(e.getValue());
                       indiferente.setVerificado(Boolean.FALSE);
                       indiferente.setNoverificado(Boolean.FALSE);
                   }
                   else indiferente.setIndiferente(false);});
               return checkBox;
            }).setCaption("Indiferente");
            grillaVerificaciondetalleDTO.addColumn(DetalleVerificacionDTO::getObservacion).setCaption("Observación");
            grillaVerificaciondetalleDTO.setSizeFull();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // convenience empty getter and setter implementation for better readability
    public static class No {
       public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
           return source -> null;
       }
       public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
           return (bean, fieldValue) -> {
               //no op
           };
       }
    }
    
    private void guardar() {
        try {
            if(dtfFechaVerificacion.getValue()==null)  verificacion.setFechaverificacion(null);
            else {java.util.Date dfechaVerificacion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaVerificacion.getValue().toString());
                verificacion.setFechaverificacion(dfechaVerificacion);}
            if(dtfFechaCorreccion.getValue()==null) verificacion.setFechacorreccion(null);
            else {java.util.Date dfechaCorreccion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaCorreccion.getValue().toString());
                verificacion.setFechacorreccion(dfechaCorreccion);}
            if(dtfFechaOperacion.getValue()==null) verificacion.setFechaoperacion(null);
            else {java.util.Date dfechaOperacion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaOperacion.getValue().toString());
                verificacion.setFechaoperacion(dfechaOperacion);}
            if(dtfFechaRevision.getValue()==null) verificacion.setFecharevision(null);
            else {java.util.Date dfechaRevision = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaRevision.getValue().toString());
                verificacion.setFecharevision(dfechaRevision);}
            if(dtfFechaAnulado.getValue()==null) verificacion.setFechaanulado(null);
            else {java.util.Date dfechaAnulado = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaAnulado.getValue().toString());
                verificacion.setFechaanulado(dfechaAnulado);}
            List<Verificaciondetalle> list = new ArrayList<>();
            List<DetalleVerificacionDTO> listDTO;
            if(verificacion.getIdverificacion()==null) {
                listDTO = grillaVerificaciondetalleDTO.getDataProvider()
                         .fetch(new Query<>())
                         .collect(Collectors.toList());
                for (DetalleVerificacionDTO dto : listDTO) {
                    Verificaciondetalle verificaciondetalle = new Verificaciondetalle();
                    verificaciondetalle.setIdrequisito(dto.getIdrequisito());
                    verificaciondetalle.setVerificado(dto.getVerificado());
                    verificaciondetalle.setNoverificado(dto.getNoverificado());
                    verificaciondetalle.setIndiferente(dto.getIndiferente());
                    verificaciondetalle.setObservacion(dto.getObservacion());
                    list.add(verificaciondetalle);
                    //String rnuevo = "("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito()+","+
                    //        verificaciondetalle.getIdverificacion()+","+verificaciondetalle.getVerificado()+","+verificaciondetalle.getObservacion()+","+
                    //        verificaciondetalle.getIndiferente()+","+verificaciondetalle.getNoverificado()+")";
                    //AuditoriaRegistrar.guardarAuditoria("verificaciondetalle", "I", "", rnuevo);
                }
            } else {
                list = grillaVerificaciondetalle.getDataProvider()
                         .fetch(new Query<>())
                         .collect(Collectors.toList());
            }
            verificacionDao.guardar(verificacion, list);
            setVisible(false);
            guardarListener.accept(verificacion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            //this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(verificacion);
        }
    }
    
    private void borrar() {
        try {
            //verificacionDao.borrar(verificacion);
            setVisible(false);
            //borrarListener.accept(verificacion);
            Notification.show("Advertencia", "No se pueden borrar los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(verificacion);
        }
    }
    
    private void imprimir() {
        try {
            final Map map = new HashMap();
            DBConnection dBConnection = new DBConnection();
            map.put("usuario", UserHolder.get().getUsuario());
            map.put("titulo", "INFORME DE VERIFICACION");
            map.put("subtitulo", "DETALLADO");
            map.put("numeroverificacion", verificacion.getIdverificacion());
            map.put("descripcion", cargarDescripcionLarga());
            StreamResource.StreamSource source = new StreamResource.StreamSource() {
                @Override
                public InputStream getStream() {
                    byte[] b = null;
                    try {
                        try {
                            System.out.println("path: "+ this.getClass().getClassLoader().getResourceAsStream("/repVerificacion.jasper"));
                            String archivo;
                            if(verificacion.getIdestadoverificacion().getDescripcion().equals("VERIFICADO")) archivo="/repVerificacion.jasper";
                            else archivo="/repVerificacion2.jasper";
                            b = JasperRunManager.runReportToPdf(getClass()
                                    .getClassLoader()
                                    .getResourceAsStream(archivo), 
                                    map, dBConnection.getConnection());
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (JRException ex) {
                        ex.printStackTrace();
                    }

                    return new ByteArrayInputStream(b);
                }
            };
            StreamResource resource = new StreamResource(source, "repVerificacion.pdf");
            resource.setMIMEType("application/pdf");
                 resource.setCacheTime(0);
            getUI().getPage().open(resource, "_blank", false);            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setGuardarListener(Consumer<Verificacion> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Verificacion> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Verificacion> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(verificacion.getIdverificacion()!=null) this.viejo = "("+verificacion.getIdverificacion()+","+dtfFechaVerificacion.getValue()+","+
                verificacion.getHorarecepcion()+","+verificacion.getHoraremision()+","+verificacion.getIdtipooperacion().getIdtipooperacion()+","+verificacion.getIdmodalidad().getIdmodalidad()+","+
                verificacion.getNumerooperacion()+","+verificacion.getObservaciones()+","+verificacion.getIdverificador().getIdfuncionario()+","+
                dtfFechaCorreccion.getValue()+","+verificacion.getCorreccion()+","+verificacion.getIddependencia().getIddependencia()+","+
                verificacion.getIdestadoverificacion().getIdestadoverificacion()+","+dtfFechaRevision.getValue()+","+verificacion.getRevisionfinal()+","+
                verificacion.getIdrevisador().getIdfuncionario()+","+dtfFechaAnulado.getValue()+","+verificacion.getObservacionanulado()+","+
                verificacion.getCedula()+","+dtfFechaOperacion.getValue()+","+verificacion.getMes()+","+verificacion.getNombrecomercio()+","+
                verificacion.getPeriodo()+","+verificacion.getIdregional().getId()+","+verificacion.getNumeroboleta()+","+verificacion.getNumerocheque()+","+
                verificacion.getNumerocontrato()+","+verificacion.getNumerofactura()+","+verificacion.getNumeronotacredito()+","+
                verificacion.getNumeronotapedido()+","+verificacion.getNumeroordencredito()+","+verificacion.getNumeroordenpago()+","+
                verificacion.getNumeroplanilla()+","+verificacion.getNumerosolicitud()+","+verificacion.getIdaprueba().getIdfuncionario()+","+
                verificacion.getIdprocedencia().getIdprocedencia()+","+verificacion.getIdresponsable().getIdfuncionario()+","+verificacion.getLocal()+","+
                verificacion.getPuntoexpedicion()+","+verificacion.getIdresponsable2().getIdfuncionario()+","+verificacion.getIdautorizador().getId()+","+verificacion.getNumerocuenta()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+verificacion.getIdverificacion()+","+dtfFechaVerificacion.getValue()+","+
                verificacion.getHorarecepcion()+","+verificacion.getHoraremision()+","+verificacion.getIdtipooperacion().getIdtipooperacion()+","+verificacion.getIdmodalidad().getIdmodalidad()+","+
                verificacion.getNumerooperacion()+","+verificacion.getObservaciones()+","+verificacion.getIdverificador().getIdfuncionario()+","+
                dtfFechaCorreccion.getValue()+","+verificacion.getCorreccion()+","+verificacion.getIddependencia().getIddependencia()+","+
                verificacion.getIdestadoverificacion().getIdestadoverificacion()+","+dtfFechaRevision.getValue()+","+verificacion.getRevisionfinal()+","+
                verificacion.getIdrevisador().getIdfuncionario()+","+dtfFechaAnulado.getValue()+","+verificacion.getObservacionanulado()+","+
                verificacion.getCedula()+","+dtfFechaOperacion.getValue()+","+verificacion.getMes()+","+verificacion.getNombrecomercio()+","+
                verificacion.getPeriodo()+","+verificacion.getIdregional().getId()+","+verificacion.getNumeroboleta()+","+verificacion.getNumerocheque()+","+
                verificacion.getNumerocontrato()+","+verificacion.getNumerofactura()+","+verificacion.getNumeronotacredito()+","+
                verificacion.getNumeronotapedido()+","+verificacion.getNumeroordencredito()+","+verificacion.getNumeroordenpago()+","+
                verificacion.getNumeroplanilla()+","+verificacion.getNumerosolicitud()+","+verificacion.getIdaprueba().getIdfuncionario()+","+
                verificacion.getIdprocedencia().getIdprocedencia()+","+verificacion.getIdresponsable().getIdfuncionario()+","+verificacion.getLocal()+","+
                verificacion.getPuntoexpedicion()+","+verificacion.getIdresponsable2().getIdfuncionario()+","+verificacion.getIdautorizador().getId()+","+verificacion.getNumerocuenta()+")";
    }

    public void setVerificacion(Verificacion v) {
        try {
            this.verificacion = v;
            binder.setBean(v);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            if (verificacion.getFechaverificacion()!= null) {
                LocalDate dt = LocalDate.parse(verificacion.getFechaverificacion().toString(), formatter);
                dtfFechaVerificacion.setValue(dt);}
            if (verificacion.getFechaoperacion()!= null) {
                LocalDate dt = LocalDate.parse(verificacion.getFechaoperacion().toString(), formatter);
                dtfFechaOperacion.setValue(dt);} 
            if (verificacion.getFechacorreccion()!= null) {
                LocalDate dt = LocalDate.parse(verificacion.getFechacorreccion().toString(), formatter);
                dtfFechaCorreccion.setValue(dt);} 
            if (verificacion.getFecharevision()!= null) {
                LocalDate dt = LocalDate.parse(verificacion.getFecharevision().toString(), formatter);
                dtfFechaRevision.setValue(dt);}
            if (verificacion.getFechaanulado()!= null) {
                LocalDate dt = LocalDate.parse(verificacion.getFechaanulado().toString(), formatter);
                dtfFechaAnulado.setValue(dt);}
            if(verificacion.getIdverificacion()==null) {
                btnBorrar.setVisible(false);
                btnImprimir.setVisible(false);
            } else {
                btnBorrar.setVisible(true);
                btnImprimir.setVisible(true);            
                btnImprimir.setEnabled(true);            
            }    
            setVisible(true);
            txtfHoraRecepcion.selectAll();   
            agregarRequisitos(verificacion.getVerificaciondetalleList(), false);
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void deshabilitarTodo(){
        dtfFechaVerificacion.setEnabled(false);
        cmbEstadoVerificacion.setEnabled(false);
        txtfHoraRecepcion.setEnabled(false);
        txtfHoraRemision.setEnabled(false);
        cmbTipooperacion.setEnabled(false);
        cmbModalidad.setEnabled(false);
        txtfNumeroOperacion.setEnabled(false);
        cmbVerificador.setEnabled(false);
        btnAgregarRequisitos.setEnabled(false);
        dtfFechaCorreccion.setEnabled(false);
        cmbDependencia.setEnabled(false);
        txtfObservaciones.setEnabled(false);
        txtfCorreccion.setEnabled(false);
        dtfFechaRevision.setEnabled(false);
        txtfRevision.setEnabled(false);
        cmbRevisador.setEnabled(false);
        cmbRegional.setEnabled(false);
        cmbResponsable.setEnabled(false);
        cmbResponsable2.setEnabled(false);
        cmbAprueba.setEnabled(false);
        cmbAutorizador.setEnabled(false);
        dtfFechaAnulado.setEnabled(false);
        txtfObservacionAnulado.setEnabled(false);
        grillaVerificaciondetalle.setEnabled(false);
        btnGuardar.setEnabled(false);
        btnBorrar.setEnabled(false);
        identificador.setEnabled(false);
    }
    
    
    public void habilitarTodo(){
        dtfFechaVerificacion.setEnabled(true);
        cmbEstadoVerificacion.setEnabled(true);
        txtfHoraRecepcion.setEnabled(true);
        txtfHoraRemision.setEnabled(true);
        cmbTipooperacion.setEnabled(true);
        cmbModalidad.setEnabled(true);
        txtfNumeroOperacion.setEnabled(true);
        cmbVerificador.setEnabled(true);
        if(verificacion.getVerificaciondetalleList().isEmpty()) btnAgregarRequisitos.setEnabled(true);
        else btnAgregarRequisitos.setEnabled(false);
        dtfFechaCorreccion.setEnabled(true);
        cmbDependencia.setEnabled(true);
        txtfObservaciones.setEnabled(true);
        txtfCorreccion.setEnabled(true);
        dtfFechaRevision.setEnabled(true);
        txtfRevision.setEnabled(true);
        cmbRevisador.setEnabled(true);
        cmbRegional.setEnabled(true);
        cmbResponsable.setEnabled(true);
        cmbResponsable2.setEnabled(true);
        cmbAprueba.setEnabled(true);
        cmbAutorizador.setEnabled(true);
        dtfFechaAnulado.setEnabled(true);
        txtfObservacionAnulado.setEnabled(true);
        grillaVerificaciondetalle.setEnabled(true);
        btnGuardar.setEnabled(true);
        btnBorrar.setEnabled(true);
        identificador.setEnabled(true);
    }
    
    
    public void cargarDatosPredeterminados(){
        if(verificacion.getIdverificacion()==null){
            Date input = new Date();
            LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalTime time = input.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
            dtfFechaVerificacion.setValue(date);
            txtfHoraRecepcion.setValue(time.getHour()+":"+String.format("%02d", time.getMinute()));
            cmbEstadoVerificacion.setSelectedItem(estadoverificacionDao.getPendiente());
            cmbTipooperacion.setSelectedItem(tipooperacionDao.getNinguno());
            cmbModalidad.setSelectedItem(modalidadDao.getNinguno());
            txtfNumeroOperacion.setValue("0");
            cmbVerificador.setSelectedItem(funcionarioDao.getFuncionario(UserHolder.get().getUsuario()));
            cmbResponsable.setSelectedItem(funcionarioDao.getNinguno());
            cmbAprueba.setSelectedItem(funcionarioDao.getNinguno());
            cmbDependencia.setSelectedItem(dependenciaDao.getNinguno());
            cmbRevisador.setSelectedItem(funcionarioDao.getNinguno());
            btnImprimir.setEnabled(false);
            btnAgregarRequisitos.setEnabled(true);
            List<Verificaciondetalle> list = new ArrayList<>();
            List<DetalleVerificacionDTO> list1 = new ArrayList<>();
            grillaVerificaciondetalle.setItems(list);
            grillaVerificaciondetalleDTO.setItems(list1);
        }
    }
    
}
