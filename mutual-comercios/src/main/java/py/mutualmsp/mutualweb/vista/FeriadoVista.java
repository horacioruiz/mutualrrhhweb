/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.formularios.FeriadoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class FeriadoVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Feriados";
    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Grid<Feriado> grillaFeriado = new Grid<>(Feriado.class);
    TextField txtfFiltro = new TextField();
    TextField txtfFiltroPeriodo = new TextField();
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    List<Feriado> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    FeriadoForm cargoForm = new FeriadoForm();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public FeriadoVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
        List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
        List<Funcionario> listFunc = new ArrayList<>();
        listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        cargoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, txtfFiltroPeriodo, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(txtfFiltroPeriodo, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

//        txtfFiltro.setPlaceholder("Filtro por descripción");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

//        txtfFiltroPeriodo.setPlaceholder("Filtro por periodo");
        txtfFiltroPeriodo.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltroPeriodo.addValueChangeListener(e -> updateListPeriodo(e.getValue()));

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        btnNuevo.addClickListener(e -> {
            grillaFeriado.asSingleSelect().clear();
            cargoForm.setFeriado(new Feriado());
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaFeriado, cargoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaFeriado, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        txtfFiltro.setPlaceholder("Búsqueda por descripción");
        txtfFiltroPeriodo.setPlaceholder("Búsqueda por periodo");

        try {
            lista = feriadoDao.listaFeriado();
            grillaFeriado.setItems(lista);
            grillaFeriado.removeAllColumns();
            grillaFeriado.addColumn(Feriado::getId).setCaption("Cód");
            grillaFeriado.addColumn(Feriado::getPeriodo).setCaption("Periodo");
            grillaFeriado.addColumn((feriado) -> simpleDateFormat.format(feriado.getFecha())).setCaption("Fecha");
            grillaFeriado.addColumn(Feriado::getDescripcion).setCaption("Descripción");
            grillaFeriado.setSizeFull();
            grillaFeriado.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        cargoForm.setVisible(false);
                    } else {
                        cargoForm.setFeriado(e.getValue());
                    }
                }
            });

            cargoForm.setGuardarListener(r -> {
                grillaFeriado.clearSortOrder();
                lista = feriadoDao.listaFeriado();
                grillaFeriado.setItems(lista);
            });
            cargoForm.setBorrarListener(r -> {
                grillaFeriado.clearSortOrder();
                lista = feriadoDao.listaFeriado();
                grillaFeriado.setItems(lista);
            });
            cargoForm.setCancelarListener(r -> {
                grillaFeriado.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            lista = feriadoDao.getFeriadoByDescripcion(value);
            grillaFeriado.setSizeFull();
            grillaFeriado.setItems(lista);
            grillaFeriado.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateListPeriodo(String value) {
        try {
            lista = feriadoDao.getFeriadoByPeriodo(value);
            grillaFeriado.setSizeFull();
            grillaFeriado.setItems(lista);
            grillaFeriado.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
