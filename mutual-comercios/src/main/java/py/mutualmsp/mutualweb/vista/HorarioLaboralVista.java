/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.formularios.HorarioLaboralForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class HorarioLaboralVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Horarios Laborales";
    Grid<HorarioLaboral> grillaHorarioLaboral = new Grid<>(HorarioLaboral.class);
    ComboBox<String> filtroEstado = new ComboBox<>();
//    TextField txtfFiltro = new TextField("Filtro");
    HorarioLaboralDao cargoDao = ResourceLocator.locate(HorarioLaboralDao.class);
    List<HorarioLaboral> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    HorarioLaboralForm horarioLaboralForm = new HorarioLaboralForm();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public HorarioLaboralVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
        List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
        List<Funcionario> listFunc = new ArrayList<>();
        listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        horarioLaboralForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();

        topLayout.addComponents(filtroEstado, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(filtroEstado, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        filtroEstado.setPlaceholder("Filtre estado");
        filtroEstado.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaHorarioLaboral.asSingleSelect().clear();
            horarioLaboralForm.setHorarioLaboral(new HorarioLaboral());
        });

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaHorarioLaboral, horarioLaboralForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaHorarioLaboral, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        List<String> listEstado = new ArrayList<>();
        listEstado.add("ACTIVO");
        listEstado.add("INACTIVO");
        filtroEstado.setItems(listEstado);

        try {
            String patternHora = "HH:mm";
            SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
            lista = cargoDao.listaHorarioLaboral();
            grillaHorarioLaboral.setItems(lista);
            grillaHorarioLaboral.removeAllColumns();
            grillaHorarioLaboral.addColumn(HorarioLaboral::getId).setCaption("COD");
            grillaHorarioLaboral.addColumn(HorarioLaboral::getDescripcion).setCaption("DESCRIPCION");
            grillaHorarioLaboral.addComponentColumn(e -> {
                return e.getStatusIcon();
            }).setCaption("ESTADO");
            grillaHorarioLaboral.addColumn(e -> {
                return e.getEntrada() == null ? "--" : simpleDateFormatHora.format(e.getEntrada());
            }).setCaption("ENTRADA");
            grillaHorarioLaboral.addColumn(e -> {
                return e.getSalida() == null ? "--" : simpleDateFormatHora.format(e.getSalida());
            }).setCaption("SALIDA");
            grillaHorarioLaboral.setSizeFull();
            grillaHorarioLaboral.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        horarioLaboralForm.setVisible(false);
                    } else {
                        horarioLaboralForm.setHorarioLaboral(e.getValue());
                        horarioLaboralForm.setViejo();
                    }
                }
            });

            horarioLaboralForm.setGuardarListener(r -> {
                grillaHorarioLaboral.clearSortOrder();
                lista = cargoDao.listaHorarioLaboral();
                grillaHorarioLaboral.setItems(lista);
            });
            horarioLaboralForm.setBorrarListener(r -> {
                grillaHorarioLaboral.clearSortOrder();
                lista = cargoDao.listaHorarioLaboral();
                grillaHorarioLaboral.setItems(lista);
            });
            horarioLaboralForm.setCancelarListener(r -> {
                grillaHorarioLaboral.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            List<HorarioLaboral> lista = new ArrayList<>();
            if (value.equalsIgnoreCase("ACTIVO")) {
                cargoDao.getListHorarioLaboralByEstado(true);
            } else if (value.equalsIgnoreCase("INACTIVO")) {
                cargoDao.getListHorarioLaboralByEstado(false);
            } else {
                cargoDao.listaHorarioLaboral();
            }
            grillaHorarioLaboral.setSizeFull();
            grillaHorarioLaboral.setItems(lista);
            grillaHorarioLaboral.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
