package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioExporadicoDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.SolicitudDetalleDao;
import py.mutualmsp.mutualweb.dto.MarcacionesDTO;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;
import py.mutualmsp.mutualweb.formularios.ConsultaMarcacionesForm;
import py.mutualmsp.mutualweb.formularios.MotivosForm;
import py.mutualmsp.mutualweb.formularios.NuevaMarcacionForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.HoraExtrasView.whatDays;
import static py.mutualmsp.mutualweb.vista.SolicitudView.ordenandoFechaString;

/**
 * Created by Alfre on 7/6/2016.
 */
public class ConsultaMarcacionesView extends CssLayout implements View {

    public static final String VIEW_NAME = "Marcaciones";

    HorizontalLayout hl2 = new HorizontalLayout();
    HorizontalLayout hl = new HorizontalLayout();
    ProgressBar spinnerCargando = new ProgressBar();

    Grid<Marcaciones> grid = new Grid<>(Marcaciones.class);
    SolicitudDetalleDao solicitudDetalleController = ResourceLocator.locate(SolicitudDetalleDao.class);
    HorarioExporadicoDao horarioExporadicoDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    //Button newUser = new Button("Nuevo");
    TextField txtfFiltro = new TextField();
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    Marcaciones marcacionSeleccionado;
//    ComboBox<Formulario> filter = new ComboBox<>("Filtre por formulario");
    MotivosForm form = new MotivosForm();
    NuevaMarcacionForm nuevaMarcacionForm = new NuevaMarcacionForm();
    ArrayList<MarcacionesDTO> listaMarcacionesDTO = new ArrayList<>();
    List<Marcaciones> listaMarcaciones = new ArrayList<>();
    Button btnImprimir = new Button("");
    Button btnFiltrar = new Button("");
    Button btnNuevo = new Button("");
    ConsultaMarcacionesForm consultaMarcacionesForm = new ConsultaMarcacionesForm();
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<String> filterEstado = new ComboBox<>();

    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    String patternHora = "HH:mm";
    SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);

    String fileName = "";
    String ubicacion = "";
    FileOutputStream fos = null;
    String destinarariosString = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public ConsultaMarcacionesView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            consultaMarcacionesForm.setVisible(false);

            btnImprimir.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnImprimir.setIcon(VaadinIcons.PRINT);
            btnFiltrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnFiltrar.setIcon(VaadinIcons.SEARCH);
            btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponents(txtfFiltro);
            horizontalLayout.setSpacing(true);
//            horizontalLayout.setComponentAlignment(upload, Alignment.MIDDLE_RIGHT);
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                filterFuncionario.setItems(funcionarioController.listaFuncionario());
                btnNuevo.setEnabled(true);
            } else {
                filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
                filterFuncionario.setValue(UserHolder.get().getIdfuncionario());
                filterFuncionario.setEnabled(false);
                btnNuevo.setEnabled(false);
                txtfFiltro.setEnabled(false);
            }
            List listaEstado = new ArrayList();
            listaEstado.add("TEMPRANO");
            listaEstado.add("TARDE");
            listaEstado.add("OBSERVACION");
            filterEstado.setItems(listaEstado);

            horizontalLayout.addComponent(filterFuncionario);
            horizontalLayout.addComponent(filterEstado);
            horizontalLayout.addComponent(fechaDesde);
            horizontalLayout.addComponent(fechaHasta);
            horizontalLayout.addComponent(btnFiltrar);
            horizontalLayout.addComponent(btnNuevo);
            horizontalLayout.addComponent(btnImprimir);

            spinnerCargando.setIndeterminate(true);
            spinnerCargando.setCaption("Cargando datos...");
            spinnerCargando.setWidth(8f, TextField.UNITS_EM);
            spinnerCargando.setHeight(8f, TextField.UNITS_EM);
            hl2.setVisible(false);

            /*fechaDesde.addValueChangeListener(e -> findByFecha());
            fechaHasta.addValueChangeListener(e -> findByFecha());*/
            //horizontalLayout.addComponent(btnCargarDatos);
            //horizontalLayout.addComponent(btnImportar);
            horizontalLayout.addStyleName("top-bar");
            txtfFiltro.setPlaceholder("ID reloj");
            filterFuncionario.setPlaceholder("Funcionario");
            filterEstado.setPlaceholder("Estado");
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            txtfFiltro.setWidth(4f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);
            filterEstado.setWidth(8f, TextField.UNITS_EM);
            form.setVisible(false);

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
            fechaDesde.setPlaceholder("Fecha desde");
            fechaHasta.setPlaceholder("Fecha hasta");

            //txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
            SimpleDateFormat sdf = new SimpleDateFormat("mm");
            SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");

//            listaMarcaciones = marcacionesController.listaMarcaciones();
            listaMarcaciones = new ArrayList<>();
            grid.setItems(listaMarcaciones);
            grid.removeAllColumns();

            grid.addColumn(e -> {
                return e.getHorarioFuncionario() == null ? "--" : e.getHorarioFuncionario().getIdreloj();
            }).setCaption("ID RELOJ");
            grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("ESTADO");
            grid.addColumn(e -> {
                return e.getNombrefunc();
            }).setCaption("FUNCIONARIO");
            grid.addColumn(e -> {
                return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
            }).setCaption("FECHA");
            grid.addColumn(e -> {
                String hs = (e.getInasignada() == null ? "" : simpleDateFormatHora.format(e.getInasignada())) + " - " + (e.getOutasignada() == null ? "" : simpleDateFormatHora.format(e.getOutasignada()));
                try {
                    HorarioExporadico he = new HorarioExporadico();
                    he.setLunes(false);
                    he.setMartes(false);
                    he.setMiercoles(false);
                    he.setJueves(false);
                    he.setViernes(false);
                    he.setSabado(false);
                    he.setFuncionario(e.getHorarioFuncionario().getFuncionario());
                    he.setFechadesde(e.getFecha());
                    he.setFechahasta(e.getFecha());
                    List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.listarExistenciaDatosWithHL(he);
                    if (!listHorarioExporadico.isEmpty()) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(e.getFecha());
                        boolean val = false;
                        switch (c.get(Calendar.DAY_OF_WEEK)) {
                            case 2:
                                val = listHorarioExporadico.get(0).getLunes();
                                break;
                            case 3:
                                val = listHorarioExporadico.get(0).getMartes();
                                break;
                            case 4:
                                val = listHorarioExporadico.get(0).getMiercoles();
                                break;
                            case 5:
                                val = listHorarioExporadico.get(0).getJueves();
                                break;
                            case 6:
                                val = listHorarioExporadico.get(0).getViernes();
                                break;
                            case 7:
                                val = listHorarioExporadico.get(0).getSabado();
                                break;
                            default:
                        }

                        if (val) {
                            hs = (listHorarioExporadico.get(0) == null ? "" : simpleDateFormatHora.format(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada())) + " - " + simpleDateFormatHora.format(listHorarioExporadico.get(0).getHorarioLaboral().getSalida());
                        }
                    }
                } catch (Exception ex) {
                } finally {
                }
                return hs;
            }).setCaption("HS LABORAL");
            grid.addColumn(e -> {
                String hs = (e.getInmarcada() == null ? "" : simpleDateFormatHora.format(e.getInmarcada())) + " - " + (e.getOutmarcada() == null ? "" : simpleDateFormatHora.format(e.getOutmarcada()));
                return hs;
            }).setCaption("HS MARCACION");
            /*grid.addColumn(e -> {
                return e.getInmarcada() == null ? "--" : simpleDateFormatHora.format(e.getInmarcada());
            }).setCaption("ENTRADA");
            grid.addColumn(e -> {
                return e.getOutmarcada() == null ? "--" : simpleDateFormatHora.format(e.getOutmarcada());
            }).setCaption("SALIDA");*/
            grid.addColumn(e -> {
                try {
                    if (e.getMintrabajada() == null) {
                        return "--";
                    } else if (e.getMintrabajada() < 0) {
                        return "--";
                    } else {
                        return e.getMintrabajada() == null ? "--" : sdfHM.format(sdf.parse(e.getMintrabajada() + ""));
                    }
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("HS TRABJ");
            grid.addColumn(e -> {
                try {
                    return e.getMinllegadatardia() == null ? "--" : sdfHM.format(sdf.parse(e.getMinllegadatardia() + ""));
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("HS TARDIA");
            grid.addColumn(e -> {
                try {
                    return e.getMinllegadatemprana() == null ? "--" : sdfHM.format(sdf.parse(e.getMinllegadatemprana() + ""));
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("HS TEMPRANA");
//            grid.addComponentColumn(this::buildConfirmButton).setCaption("OBSERVACION");
            grid.addColumn(e -> {
                try {
                    if (e.getSolicitud() == null) {
                        return e.getObservacion().toUpperCase();
                    } else {
                        String filterObs = "";
                        try {
                            filterObs = e.getObservacion() == null || e.getObservacion().equals("") ? "" : " - " + e.getObservacion().toUpperCase();
                        } catch (Exception ex) {
                        } finally {
                        }
                        return (e.getSolicitud().getId()) + " " + e.getSolicitud().getFormulario().getAbrev() + " " + filterObs;
                    }
                } catch (Exception ex) {
                    return e.getObservacion() != null ? e.getObservacion().toUpperCase() : "--";
                } finally {
                }
            }).setCaption("OBSERVACION");
            grid.setSizeFull();

            hl.addComponents(grid, consultaMarcacionesForm);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            hl2.addComponent(spinnerCargando);
            hl2.setComponentAlignment(spinnerCargando, Alignment.MIDDLE_CENTER);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(hl2);
            barAndGridLayout.addComponent(hl);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

            consultaMarcacionesForm.setGuardarListener(r -> {
                grid.clearSortOrder();
                Date fechaDesdeText = null;
                Date fechaHastaText = null;
                String estado = null;
                Funcionario func = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    func = filterFuncionario.getValue();
                } catch (Exception e) {
                    func = null;
                } finally {
                }
                try {
                    fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                    fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                } catch (Exception e) {
                    fechaDesdeText = null;
                    fechaHastaText = null;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                txtfFiltro.setValue("");

                listaMarcaciones = marcacionesController.getByFilter(func, fechaDesdeText, fechaHastaText, estado, idReloj);
                grid.setItems(listaMarcaciones);
            });

            /*filterFuncionario.addValueChangeListener(vcl -> {
                try {
                    Date fechaDesdeText = null;
                    Date fechaHastaText = null;
                    String estado = null;
                    try {
                        fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                        fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                    } catch (Exception e) {
                        fechaDesdeText = null;
                        fechaHastaText = null;
                    } finally {
                    }
                    try {
                        estado = filterEstado.getValue();
                    } catch (Exception e) {
                        estado = null;
                    } finally {
                    }
                    txtfFiltro.setValue("");

                    listaMarcaciones = marcacionesController.getByFilter(vcl.getValue(), fechaDesdeText, fechaHastaText, estado);
                    grid.setSizeFull();
                    grid.setItems(listaMarcaciones);
                    grid.clearSortOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });*/
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        consultaMarcacionesForm.setVisible(false);
                    } else {
                        if (e.getValue().getMintrabajada() < 0 || (e.getValue().getAlta() != null && e.getValue().getAlta().equalsIgnoreCase("MANUAL"))) {
                            consultaMarcacionesForm.setConsultaMarcaciones(e.getValue());
                        }
                    }
                }
            });
            btnImprimir.addClickListener(e -> {
                mostrarListaMarcacionesPDF();
            });
            btnNuevo.addClickListener(e -> {

                NuevaMarcacionForm solicitudForm = new NuevaMarcacionForm();
                UI.getCurrent().addWindow(solicitudForm);

                solicitudForm.nuevoRegistro();
                solicitudForm.setVisible(true);
                solicitudForm.setCancelListener(reclamo -> {
                    //form = null;
                    grid.clearSortOrder();
                });
                solicitudForm.setSaveListener(reclamo -> {
                    grid.clearSortOrder();
//                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
//                        lista = licenciasController.listadeLicenciasSinConfirmar();
//                    } else {
//                        lista = new ArrayList<>();
//                    }
//                    grid.setItems(lista);
//                    labelTotalizador.setCaption("Total de registros: " + lista.size());
                });
                solicitudForm.setCancelListener(r -> {
                    grid.clearSortOrder();
                });
            });
            btnFiltrar.addClickListener(e -> {
                UI ui = UI.getCurrent();

                // Instruct client side to poll for changes and show spinner
                ui.setPollInterval(500);
                hl2.setVisible(true);
                hl.setVisible(false);

                // Start background task
                CompletableFuture.runAsync(() -> {
                    filtrarDatos();
                    // Need to use access() when running from background thread
                    ui.access(() -> {
                        // Stop polling and hide spinner
                        ui.setPollInterval(-1);
                        hl2.setVisible(false);
                        hl.setVisible(true);
                    });
                });
            });
            /*filterEstado.addValueChangeListener(vcl -> {
                try {
                    Funcionario fun = null;
                    Date fechaDesdeText = null;
                    Date fechaHastaText = null;
                    try {
                        fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                        fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                    } catch (Exception e) {
                        fechaDesdeText = null;
                        fechaHastaText = null;
                    } finally {
                    }
                    try {
                        fun = filterFuncionario.getValue();
                    } catch (Exception e) {
                        fun = null;
                    } finally {
                    }
                    txtfFiltro.setValue("");

                    listaMarcaciones = marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, vcl.getValue());
                    grid.setSizeFull();
                    grid.setItems(listaMarcaciones);
                    grid.clearSortOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Button buildConfirmButton2(Marcaciones p) {
        Button button = new Button(VaadinIcons.CHECK_CIRCLE);
        try {
            if (p.getSolicitud() == null) {
                button.setVisible(false);
            } else {
                button.setEnabled(true);
                button.setVisible(true);
            }
        } catch (Exception e) {
            button.setVisible(false);
        } finally {
        }
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> mostrarPDFSolicitud(p));
        return button;
    }

    private void updateList(String value) {
        String valor = value;
        filterFuncionario.setValue(null);
        filterEstado.setValue(null);
        fechaDesde.setValue(null);
        fechaHasta.setValue(null);
        txtfFiltro.setValue(valor);
        try {
            listaMarcaciones = marcacionesController.getByIdReloj(valor);
            grid.setSizeFull();
            grid.setItems(listaMarcaciones);
            grid.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findByFecha() {
        txtfFiltro.setValue("");
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                Funcionario func = null;
                String estado = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    func = filterFuncionario.getValue();
                } catch (Exception e) {
                    func = null;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                txtfFiltro.setValue("");

                listaMarcaciones = marcacionesController.getByFilter(func, DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), estado, idReloj);
                grid.setSizeFull();
                grid.clearSortOrder();
                grid.setItems(listaMarcaciones);
            }
        }
    }

    private void mostrarPDFSolicitud(Marcaciones marcacion) {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudVacaciones(marcacion) + "}");
        } else if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudOrdenTrabajo(marcacion) + "}");
        }
//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepEmpresa", "MUTUAL NACIONAL  DE FUNCIONARIOS DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "SOLICITUD DE ORDEN DE TRABAJO");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "";
                if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                    archivo += "formulario_vacaciones";
                } else if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                    archivo += "formulario_orden_trabajo";
                }

                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "";
        if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
            archivo += "formulario_vacaciones";
        } else if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
            archivo += "formulario_orden_trabajo";
        }

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarSolicitudVacaciones(Marcaciones marcacion) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(marcacion.getSolicitud().getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("codigo", marcacion.getSolicitud().getId());
        jsonObj.put("funcionario", marcacion.getSolicitud().getFuncionario().getNombre() + " " + marcacion.getSolicitud().getFuncionario().getApellido());
        jsonObj.put("area", marcacion.getSolicitud().getAreafunc());
        jsonObj.put("cargo", marcacion.getSolicitud().getCargofunc());
        jsonObj.put("fechaIngreso", formatter.format(marcacion.getSolicitud().getFuncionario().getFechaingreso()));
        jsonObj.put("motivo", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase());

        jsonObj.put("fechadesde", formatter.format(marcacion.getSolicitud().getFechaini()));
        jsonObj.put("fechahasta", formatter.format(marcacion.getSolicitud().getFechafin()));

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private void mostrarListaMarcacionesPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarMarcaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "marcaciones";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "marcaciones";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarSolicitudOrdenTrabajo(Marcaciones marcacion) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(marcacion.getSolicitud().getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("fechas", formatter.format(marcacion.getSolicitud().getFechacreacion()));
        jsonObj.put("codigo", marcacion.getSolicitud().getId());
        jsonObj.put("fechainicio", formatter.format(marcacion.getSolicitud().getFechaini()));
        jsonObj.put("fechafin", formatter.format(marcacion.getSolicitud().getFechafin()));
        jsonObj.put("horainicio", formatterHora.format(marcacion.getSolicitud().getHoraini()));
        jsonObj.put("horafin", formatterHora.format(marcacion.getSolicitud().getHorafin()));
        jsonObj.put("funcionario", marcacion.getSolicitud().getFuncionario().getNombre() + " " + marcacion.getSolicitud().getFuncionario().getApellido());
        jsonObj.put("ci", marcacion.getSolicitud().getFuncionario().getCedula());
        jsonObj.put("area", marcacion.getSolicitud().getAreafunc());
        jsonObj.put("observacion", marcacion.getSolicitud().getObservacion());
        try {
            if (marcacion.getSolicitud().getDependencia() == null) {
                jsonObj.put("dependencia", "");
            } else {
                jsonObj.put("dependencia", marcacion.getSolicitud().getDependencia());
            }
        } catch (Exception e) {
            jsonObj.put("dependencia", "");
        } finally {
        }
        if (solicitudDetalle.get(0).getMotivo().getDescripcion().length() > 30) {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(0, 30));
            jsonObj.put("tarea2", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(30, solicitudDetalle.get(0).getMotivo().getDescripcion().length()));
        } else {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase());
            jsonObj.put("tarea2", "");
        }

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private JSONArray cargarMarcaciones() {
        long hsTrab = 0;
        long hsTrabTarde = 0;
        long hsTrabTemp = 0;
        for (Marcaciones ticket : listaMarcaciones) {
            hsTrab += ticket.getMintrabajada() == null ? 0 : ticket.getMintrabajada();
            hsTrabTarde += ticket.getMinllegadatardia() == null ? 0 : ticket.getMinllegadatardia();
            hsTrabTemp += ticket.getMinllegadatemprana() == null ? 0 : ticket.getMinllegadatemprana();
        }
        long hsTrabMod = hsTrab % 60;
        long hsTrabTardeMod = hsTrabTarde % 60;
        long hsTrabTempranoMod = hsTrabTemp % 60;

        JSONArray jsonArrayDato = new JSONArray();
        Comparator<Marcaciones> compareByDate = (Marcaciones o1, Marcaciones o2)
                -> o1.getFecha().compareTo(o2.getFecha());
        Collections.sort(listaMarcaciones, compareByDate);

//        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento("null", fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (Marcaciones ticket : listaMarcaciones) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");

                org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
                jsonObj.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
                jsonObj.put("subRepTimestamp", "");
                jsonObj.put("funcionarios", ticket.getNombrefunc());
                String hsEntrada = recuperarEntrada(ticket);//(ticket.getInasignada() == null ? "" : simpleDateFormatHora.format(ticket.getInasignada())) + " - " + (ticket.getOutasignada() == null ? "" : simpleDateFormatHora.format(ticket.getOutasignada()));
                jsonObj.put("entrada", hsEntrada);
                String hsSalida = (ticket.getInmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getInmarcada())) + " - " + (ticket.getOutmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getOutmarcada()));
                jsonObj.put("salida", hsSalida);
                jsonObj.put("fecha", ticket.getFecha() == null ? "--" : simpleDateFormat.format(ticket.getFecha()));

                //jsonObj.put("fecha", ticket.getFecha());
                jsonObj.put("hstrabajada", ticket.getMintrabajada() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMintrabajada() + "")));
                jsonObj.put("hstardia", ticket.getMinllegadatardia() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMinllegadatardia() + "")));
                jsonObj.put("hstemprana", ticket.getMinllegadatemprana() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMinllegadatemprana() + "")));
                //jsonObj.put("numot", marcacionesController.getById(ticket.getId()) == null ? "--" : marcacionesController.getById(ticket.getId()).getSolicitud().getId());
                try {
                    if (ticket.getSolicitud() == null) {
                        jsonObj.put("numot", ticket.getObservacion().toUpperCase());
                    } else {
                        String filterObs = "";
                        try {
                            filterObs = ticket.getObservacion() == null || ticket.getObservacion().equals("") ? "" : " - " + ticket.getObservacion().toUpperCase();
                        } catch (Exception ex) {
                        } finally {
                        }
                        jsonObj.put("numot", (ticket.getSolicitud().getId()) + " " + ticket.getSolicitud().getFormulario().getAbrev() + " " + filterObs);
                    }
                } catch (Exception ex) {
                    jsonObj.put("numot", ticket.getObservacion() != null ? ticket.getObservacion().toUpperCase() : "--");
                } finally {
                }
                String minHsTrab = String.valueOf(hsTrabMod).length() == 1 ? "0" + hsTrabMod : hsTrabMod + "";
                jsonObj.put("sumhstrabajada", ((hsTrab - hsTrabMod) / 60) + "." + minHsTrab);

                String minHsTrabTarde = String.valueOf(hsTrabTardeMod).length() == 1 ? "0" + hsTrabTardeMod : hsTrabTardeMod + "";
                jsonObj.put("sumhstardia", ((hsTrabTarde - hsTrabTardeMod) / 60) + "." + minHsTrabTarde);

                String minHsTrabTemp = String.valueOf(hsTrabTempranoMod).length() == 1 ? "0" + hsTrabTempranoMod : hsTrabTempranoMod + "";
                jsonObj.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);

                jsonArrayDato.add(jsonObj);
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        }
        return jsonArrayDato;
    }

    private void filtrarDatos() {
//        if (!txtfFiltro.getValue().trim().equalsIgnoreCase("")) {
//            updateList(txtfFiltro.getValue());
//        } else {
        txtfFiltro.setValue("");
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                Funcionario fun = null;
                Date fechaDesdeText = null;
                Date fechaHastaText = null;
                String estado = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                try {
                    fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                    fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                } catch (Exception e) {
                    fechaDesdeText = null;
                    fechaHastaText = null;
                } finally {
                }
                try {
                    fun = filterFuncionario.getValue();
                } catch (Exception e) {
                    fun = null;
                } finally {
                }
                txtfFiltro.setValue("");

                listaMarcaciones = marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, null, idReloj);
                ArrayList arrayDetalle = new ArrayList();
                for (Marcaciones listaMarcacione : listaMarcaciones) {
                    Map hsExporadico = new HashMap();
                    try {
                        hsExporadico = generarHsExporadico(listaMarcacione.getFecha(), listaMarcacione.getHorarioFuncionario().getFuncionario());
                    } catch (Exception e) {
                    } finally {
                    }
                    //NUEVA CON HORARIO EXPORADICO
                    if (hsExporadico.containsKey("fechadesde")) {
                        Timestamp fechaHoraExporadico = ((Timestamp) hsExporadico.get("fechadesde"));
                        Timestamp tsMarcacion = new Timestamp(fechaHoraExporadico.getTime());
                        tsMarcacion.setHours(listaMarcacione.getInmarcada().getHours());
                        tsMarcacion.setMinutes(listaMarcacione.getInmarcada().getMinutes());
                        tsMarcacion.setSeconds(listaMarcacione.getInmarcada().getSeconds());

                        try {
                            if (estado == null || estado.equalsIgnoreCase("OBSERVACION")) {
                                if ((tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) > (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes())) {
                                    long difer = (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) - (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes());
                                    listaMarcacione.setMinllegadatardia(difer);
                                    listaMarcacione.setMinllegadatemprana(0l);
                                } else {
                                    long difer = (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes()) - (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes());
                                    listaMarcacione.setMinllegadatemprana(difer);
                                    listaMarcacione.setMinllegadatardia(0l);
                                }
                                arrayDetalle.add(listaMarcacione);
                            } else if (estado.equalsIgnoreCase("TEMPRANO") && (tsMarcacion.compareTo(fechaHoraExporadico) == 0 || tsMarcacion.before(fechaHoraExporadico))) {
                                long difer = (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes()) - (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes());
                                listaMarcacione.setMinllegadatemprana(difer);
                                listaMarcacione.setMinllegadatardia(0l);
                                arrayDetalle.add(listaMarcacione);
                            } else if (estado.equalsIgnoreCase("TARDE") && tsMarcacion.after(fechaHoraExporadico)) {
                                long difer = (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) - (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes());
                                listaMarcacione.setMinllegadatardia(difer);
                                listaMarcacione.setMinllegadatemprana(0l);
                                arrayDetalle.add(listaMarcacione);
                            }
                        } catch (Exception e) {
                            arrayDetalle.add(listaMarcacione);
                        } finally {
                        }
                    } else {
                        Timestamp fechaHoraExporadico = listaMarcacione.getInasignada();
                        Timestamp tsMarcacion = new Timestamp(fechaHoraExporadico.getTime());
                        tsMarcacion.setHours(listaMarcacione.getInmarcada().getHours());
                        tsMarcacion.setMinutes(listaMarcacione.getInmarcada().getMinutes());
                        tsMarcacion.setSeconds(listaMarcacione.getInmarcada().getSeconds());

                        if (estado == null || estado.equalsIgnoreCase("OBSERVACION")) {
                            if ((tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) > (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes())) {
                                long difer = (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) - (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes());
                                listaMarcacione.setMinllegadatardia(difer);
                                listaMarcacione.setMinllegadatemprana(0l);
                            } else {
                                long difer = (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes()) - (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes());
                                listaMarcacione.setMinllegadatemprana(difer);
                                listaMarcacione.setMinllegadatardia(0l);
                            }
                            arrayDetalle.add(listaMarcacione);
                        } else if (estado.equalsIgnoreCase("TEMPRANO") && (tsMarcacion.compareTo(fechaHoraExporadico) == 0 || tsMarcacion.before(fechaHoraExporadico))) {
                            long difer = (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes()) - (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes());
                            listaMarcacione.setMinllegadatemprana(difer);
                            listaMarcacione.setMinllegadatardia(0l);
                            arrayDetalle.add(listaMarcacione);
                        } else if (estado.equalsIgnoreCase("TARDE") && tsMarcacion.after(fechaHoraExporadico)) {
                            long difer = (tsMarcacion.getHours() * 60 + tsMarcacion.getMinutes()) - (fechaHoraExporadico.getHours() * 60 + fechaHoraExporadico.getMinutes());
                            listaMarcacione.setMinllegadatardia(difer);
                            listaMarcacione.setMinllegadatemprana(0l);
                            arrayDetalle.add(listaMarcacione);
                        }
                    }
                }
                try {
                    grid.clearSortOrder();
                } catch (Exception e) {
                } finally {
                }
                grid.setSizeFull();
                grid.setItems(arrayDetalle);
            }
        } else {
            Notification.show("Fecha desde y fecha hasta son campos obligatorios para filtrar.", Notification.Type.ERROR_MESSAGE);
        }
//        }
    }

    private Map generarHsExporadico(Date targetDay, Funcionario fun) {
        Map mapeo = new HashMap();
        try {
            HorarioExporadico he = new HorarioExporadico();
            he.setLunes(false);
            he.setMartes(false);
            he.setMiercoles(false);
            he.setJueves(false);
            he.setViernes(false);
            he.setSabado(false);
            he.setFuncionario(fun);
            he.setFechadesde(targetDay);
            he.setFechahasta(targetDay);
            List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.consultarExistenciasPorDiasRecuperarDatos(whatDays(DateUtils.asLocalDate(targetDay)), targetDay, targetDay, fun.getIdfuncionario());
            if (!listHorarioExporadico.isEmpty()) {
                Calendar c = Calendar.getInstance();
                c.setTime(targetDay);
                boolean val = false;
                switch (c.get(Calendar.DAY_OF_WEEK)) {
                    case 2:
                        val = listHorarioExporadico.get(0).getLunes();
                        break;
                    case 3:
                        val = listHorarioExporadico.get(0).getMartes();
                        break;
                    case 4:
                        val = listHorarioExporadico.get(0).getMiercoles();
                        break;
                    case 5:
                        val = listHorarioExporadico.get(0).getJueves();
                        break;
                    case 6:
                        val = listHorarioExporadico.get(0).getViernes();
                        break;
                    case 7:
                        val = listHorarioExporadico.get(0).getSabado();
                        break;
                    default:
                }

                if (val) {
                    Timestamp tsDesde = new Timestamp(targetDay.getTime());
                    tsDesde.setHours(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getHours());
                    tsDesde.setMinutes(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getMinutes());
                    tsDesde.setSeconds(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getSeconds());

                    Timestamp tsHasta = new Timestamp(targetDay.getTime());
                    tsHasta.setHours(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getHours());
                    tsHasta.setMinutes(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getMinutes());
                    tsHasta.setSeconds(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getSeconds());

                    mapeo.put("fechadesde", tsDesde);
                    mapeo.put("fechahasta", tsHasta);
                }
            }
        } catch (Exception ex) {
        } finally {
        }
        return mapeo;
    }

    private String recuperarEntrada(Marcaciones e) {
        String hs = (e.getInasignada() == null ? "" : simpleDateFormatHora.format(e.getInasignada())) + " - " + (e.getOutasignada() == null ? "" : simpleDateFormatHora.format(e.getOutasignada()));
        try {
            HorarioExporadico he = new HorarioExporadico();
            he.setLunes(false);
            he.setMartes(false);
            he.setMiercoles(false);
            he.setJueves(false);
            he.setViernes(false);
            he.setSabado(false);
            he.setFuncionario(e.getHorarioFuncionario().getFuncionario());
            he.setFechadesde(e.getFecha());
            he.setFechahasta(e.getFecha());
            List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.listarExistenciaDatosWithHL(he);
            if (!listHorarioExporadico.isEmpty()) {
                Calendar c = Calendar.getInstance();
                c.setTime(e.getFecha());
                boolean val = false;
                switch (c.get(Calendar.DAY_OF_WEEK)) {
                    case 2:
                        val = listHorarioExporadico.get(0).getLunes();
                        break;
                    case 3:
                        val = listHorarioExporadico.get(0).getMartes();
                        break;
                    case 4:
                        val = listHorarioExporadico.get(0).getMiercoles();
                        break;
                    case 5:
                        val = listHorarioExporadico.get(0).getJueves();
                        break;
                    case 6:
                        val = listHorarioExporadico.get(0).getViernes();
                        break;
                    case 7:
                        val = listHorarioExporadico.get(0).getSabado();
                        break;
                    default:
                }

                if (val) {
                    hs = (listHorarioExporadico.get(0) == null ? "" : simpleDateFormatHora.format(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada())) + " - " + simpleDateFormatHora.format(listHorarioExporadico.get(0).getHorarioLaboral().getSalida());
                }
            }
        } catch (Exception ex) {
        } finally {
        }
        return hs;
    }
}
