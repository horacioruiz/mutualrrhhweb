/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.ScrollDestination;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.formularios.FuncionarioForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class FuncionarioVista extends CssLayout implements View {

    Funcionario funcionarioSeleccionado = null;
    String valorFuncionarioSeleccionado = null;

    public static final String VIEW_NAME = "Funcionario";
    Grid<Funcionario> grillaFuncionario = new Grid<>(Funcionario.class);
    TextField txtfFiltro = new TextField();
    List<Funcionario> lista = new ArrayList<>();
    List<Funcionario> listaChecked = new ArrayList<>();
    Map<Long, Boolean> listaMapCheck = new HashMap<>();
    Button btnNuevo = new Button("");
    Button btnNuevoExporadico = new Button("");
    Button btnEditarExporadico = new Button("");
    FuncionarioForm funcionarioForm = new FuncionarioForm();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public FuncionarioVista() {
        setSizeFull();
        addStyleName("crud-view");

        Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
        List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
        List<Funcionario> listFunc = new ArrayList<>();
        listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        btnNuevoExporadico.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnNuevoExporadico.setIcon(VaadinIcons.TIMER);

        btnEditarExporadico.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnEditarExporadico.setIcon(VaadinIcons.EDIT);

        funcionarioForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
//        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.addComponents(txtfFiltro);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
//        topLayout.addComponent(btnNuevo);
        topLayout.addComponent(btnNuevoExporadico);
        topLayout.addComponent(btnEditarExporadico);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.setWidth(25f, TextField.UNITS_EM);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
//            grillaFuncionario.asSingleSelect().clear();
            funcionarioForm.setFuncionario(new Funcionario(), false);
        });
        btnNuevoExporadico.addClickListener(e -> {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                if (funcionarioSeleccionado == null) {
                    funcionarioForm.setVisible(false);
                    Notification.show("Atención", "Debes seleccionar un detalle para asignar horarios.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    valorFuncionarioSeleccionado = "si";
                    funcionarioForm.setFuncionario(funcionarioSeleccionado, true);
                }
            }
        });
        btnEditarExporadico.addClickListener(e -> {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                if (funcionarioSeleccionado == null) {
                    funcionarioForm.setVisible(false);
                    Notification.show("Atención", "Debes seleccionar un detalle para editar.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    valorFuncionarioSeleccionado = "no";
                    funcionarioForm.setFuncionario(funcionarioSeleccionado, false);
                }
            }
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaFuncionario, funcionarioForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaFuncionario, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        try {
            grillaFuncionario.setSelectionMode(Grid.SelectionMode.MULTI);
            lista = funcionarioDao.listaFuncionario();
            grillaFuncionario.addColumn(usuario -> {
                if (listaMapCheck.containsKey(usuario.getIdfuncionario())) {
                    listaMapCheck.put(usuario.getIdfuncionario(), true);
                    return "SI";
                } else {
                    listaMapCheck.put(usuario.getIdfuncionario(), false);
                    return "NO";
                }
            }).setCaption("Habilitado");
            grillaFuncionario.setItems(lista);
            grillaFuncionario.removeAllColumns();
            grillaFuncionario.addColumn(Funcionario::getIdfuncionario).setCaption("Id Funcionario");
            grillaFuncionario.addColumn(Funcionario::getCedula).setCaption("Cédula");
            grillaFuncionario.addColumn(Funcionario::getNombreCompleto).setCaption("Nombre Apellido");
            grillaFuncionario.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("Estado");
//            grillaFuncionario.addColumn(Funcionario::getApellido).setCaption("Apellido");
            grillaFuncionario.setSizeFull();
//            grillaFuncionario.asSingleSelect().addValueChangeListener(e -> {
//                if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10 || usuario.getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
//                    funcionarioSeleccionado = e.getValue();
//                } else {
//                    funcionarioSeleccionado = null;aaa
//                }
//            });
            grillaFuncionario.asMultiSelect().addValueChangeListener(event -> {
                if (event.getValue() != null && !event.getValue().isEmpty()) {
                    int num = 0;
                    Funcionario func = new Funcionario();
                    List<Funcionario> listaFuncionario = new ArrayList<>();
                    for (Funcionario geozona : event.getValue()) {
                        System.out.println("-->> " + (geozona.getIdfuncionario()));
                        func = geozona;
                        listaFuncionario.add(func);
                        num++;
                        //listaChecked.add(geozona);
                    }
                    if (num == 1) {
                        funcionarioSeleccionado = func;
                        try {
                            if (valorFuncionarioSeleccionado != null) {
                                if (valorFuncionarioSeleccionado.equalsIgnoreCase("si")) {
                                    funcionarioForm.setFuncionario(funcionarioSeleccionado, true);
                                } else if (valorFuncionarioSeleccionado.equalsIgnoreCase("no")) {
                                    funcionarioForm.setFuncionario(funcionarioSeleccionado, false);
                                }
                            }
                        } catch (Exception ex) {
                        } finally {
                        }
                    } else {
                        if (listaFuncionario.size() >= 2) {
                            funcionarioForm.setFuncionario(listaFuncionario);
                            funcionarioForm.setVisible(true);
                        } else {
                            funcionarioSeleccionado = null;
                            funcionarioForm.setVisible(false);
                        }

                    }
                }
            });
            /*grillaFuncionario.asSingleSelect().addValueChangeListener(e -> {
                if (e.getValue() == null) {
                    funcionarioSeleccionado = null;
                    funcionarioForm.setVisible(false);
                } else {
                    funcionarioSeleccionado = e.getValue();
                    try {
                        if (valorFuncionarioSeleccionado != null) {
                            if (valorFuncionarioSeleccionado.equalsIgnoreCase("si")) {
                                funcionarioForm.setFuncionario(funcionarioSeleccionado, true);
                            } else if (valorFuncionarioSeleccionado.equalsIgnoreCase("no")) {
                                funcionarioForm.setFuncionario(funcionarioSeleccionado, false);
                            }
                        }
                    } catch (Exception ex) {
                    } finally {
                    }
                }
            });
            grillaFuncionario.asSingleSelect().addValueChangeListener(e -> {
                if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10 || usuario.getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
                    if (e.getValue() == null) {
                        funcionarioSeleccionado = null;
                        funcionarioForm.setVisible(false);
                    } else {
                        funcionarioSeleccionado = e.getValue();
                        try {
                            if (valorFuncionarioSeleccionado != null) {
                                if (valorFuncionarioSeleccionado.equalsIgnoreCase("si")) {
                                    funcionarioForm.setFuncionario(funcionarioSeleccionado, true);
                                } else if (valorFuncionarioSeleccionado.equalsIgnoreCase("no")) {
                                    funcionarioForm.setFuncionario(funcionarioSeleccionado, false);
                                }
                            }
                        } catch (Exception ex) {
                        } finally {
                        }
                    }
                }
            });*/

            funcionarioForm.setGuardarListener(r -> {
                grillaFuncionario.clearSortOrder();
                lista = funcionarioDao.listaFuncionario();
                grillaFuncionario.setItems(lista);
                grillaFuncionario.clearSortOrder();
            });

            funcionarioForm.setBorrarListener(r -> {
                grillaFuncionario.clearSortOrder();
            });

            funcionarioForm.setCancelarListener(r -> {
                grillaFuncionario.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            //lista = funcionarioDao.getListFuncionario(value);
            List<Funcionario> listFunc = new ArrayList<>();
            int num = 0;
            int posicion = 0;
            for (Funcionario funcionario : lista) {
                String input = funcionario.getNombreCompleto().toUpperCase();
                if (input.indexOf(value.toUpperCase()) != -1 ? true : false) {
                    listFunc.add(funcionario);
                    if (posicion == 0) {
                        posicion = num;
                    }
                }
                num++;
            }
            grillaFuncionario.scrollTo(posicion, ScrollDestination.START);

            grillaFuncionario.setSizeFull();
            //grillaFuncionario.setItems(listFunc);
            //grillaFuncionario.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
