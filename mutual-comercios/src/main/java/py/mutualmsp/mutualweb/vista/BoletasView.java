package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.BoletaCabDao;
import py.mutualmsp.mutualweb.dao.BoletaDetDao;
import py.mutualmsp.mutualweb.dao.EntidadDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.BoletaCab;
import py.mutualmsp.mutualweb.entities.Entidad;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.formularios.BoletasForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 23/6/2016.
 */
public class BoletasView extends CssLayout implements View {

    public static final String VIEW_NAME = "Ordenes Emitidas";
    //Button recibirMail = new Button("Recibir Mail");
    Button editarBoletas = new Button("");
    Button imprimirBoletas = new Button("");
    Button aceptarConformidad = new Button("Aceptar Conformidad");
    Button btnSearch = new Button();
//    ConfirmButton eliminarReclamo = new ConfirmButton("Eliminar Reclamo");
    Label labelTotalizador = new Label();

    int numRowSelected = 0;
    BoletaCab spd = new BoletaCab();

    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();

    BoletasDetalleView formSolicitudDetalle = new BoletasDetalleView();
    Button cerrarTicket = new Button("Cerrar Ticket");

    private ComboBox<Entidad> formulario = new ComboBox<>("Comercio");
    private ComboBox<String> cbPeriodo = new ComboBox<String>("Periodo");
    private TextField txtAno = new TextField("Año");
    EntidadDao formularioDao = ResourceLocator.locate(EntidadDao.class);

    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<Parametro> filterParametro = new ComboBox<>();
    ComboBox<Funcionario> filterAsignado = new ComboBox<>();
    Grid<BoletaCab> grid = new Grid<>(BoletaCab.class);
    List<BoletaCab> listBoletasCab = new ArrayList<>();
    BoletasForm solicitudForm = new BoletasForm();

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    RotacionesDao rotacionesController = ResourceLocator.locate(RotacionesDao.class);
    ParametroDao formularioController = ResourceLocator.locate(ParametroDao.class);
    BoletaCabDao boletasController = ResourceLocator.locate(BoletaCabDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();

    BoletaCabDao boletasCabDao = ResourceLocator.locate(BoletaCabDao.class);
    BoletaDetDao boletasDetDao = ResourceLocator.locate(BoletaDetDao.class);

    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionesDetalleDao = ResourceLocator.locate(VacacionesDetalleDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionesDao = ResourceLocator.locate(RotacionesDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    MarcacionesDao marcacionesDao = ResourceLocator.locate(MarcacionesDao.class);

    Button nuevaBoletas = new Button("");//FALTA QUE CUANDO AL RECHAZAR LICENCIAS NO MUESTRE EN COMPARACION HORA EXTRA

    TextField filter = new TextField();

    private DateField fechaLog = new DateField();
    private TextArea comentarioLog = new TextArea("Comentarios");
    private Button descargar = new Button("Descargar Archivo");
    String filename;
    byte[] content;
    DecimalFormat decimalFormat = new DecimalFormat("###,###,##0");
    LicenciasCompensarDao solicitudDetalleController = ResourceLocator.locate(LicenciasCompensarDao.class);
    BoletaCab solicitudSeleccionado;
    List<BoletaCab> lista = new ArrayList<>();
    ComboBox<String> filterAprobado = new ComboBox<>();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public BoletasView() {
        try {
            System.out.println("Nueva instancia BoletasView");
            setSizeFull();
            addStyleName("crud-view");
            HorizontalLayout horizontalLayout = createHorizontalLayout();

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            if (UserHolder.get().getIdfuncionario() == null) {
                UserHolder.get().setIdfuncionario(usuarioController.getByUsuario(UserHolder.get().getId()).getIdfuncionario());
            }

            formulario.setItems(formularioDao.listaEntidad());
            formulario.setItemCaptionGenerator(Entidad::getDescripcion);

            List<String> listString = new ArrayList<>();
            listString.add("ENERO");
            listString.add("FEBRERO");
            listString.add("MARZO");
            listString.add("ABRIL");
            listString.add("MAYO");
            listString.add("JUNIO");
            listString.add("JULIO");
            listString.add("AGOSTO");
            listString.add("SEPTIEMBRE");
            listString.add("OCTUBRE");
            listString.add("NOVIEMBRE");
            listString.add("DICIEMBRE");

            cbPeriodo.setItems(listString);

            SimpleDateFormat formatoAnho = new SimpleDateFormat("yyyy");
            txtAno.setValue(formatoAnho.format(new Date()));

            filterAprobado.setWidth(10f, TextField.UNITS_EM);
            filterParametro.setWidth(12f, TextField.UNITS_EM);
            filter.setWidth(8f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);
            filter.addValueChangeListener(e -> limpiarDatos());

            btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnSearch.setIcon(VaadinIcons.SEARCH);

            fechaDesde.setPlaceholder("Desde");
            fechaHasta.setPlaceholder("Hasta");

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

//            eliminarReclamo.setVisible(false);
            labelTotalizador.setCaption("Total de registros: 0");

            aceptarConformidad.setVisible(false);
            cerrarTicket.setVisible(false);
            editarBoletas.setVisible(false);
            imprimirBoletas.setVisible(false);
            filterFuncionario.setVisible(true);
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                filterFuncionario.setItems(funcionarioController.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            filterParametro.setItems(formularioController.listarPorTipoCodigo("licencias"));
            filterParametro.setPlaceholder("Filtre formularios");
            filter.setPlaceholder("Filtre código");
            filterParametro.setItemCaptionGenerator(Parametro::getDescripcion);
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

            List<String> listEstados = new ArrayList<>();
            listEstados.add("APROBADO");
            listEstados.add("RECHAZADO");
            listEstados.add("PENDIENTE");
            filterAprobado.setItems(listEstados);
            filterAprobado.setPlaceholder("Filtre aprobados");
            filterFuncionario.setPlaceholder("Filtre Funcionario");
            filterParametro.setPlaceholder("Filtre Parametro");
            filterAsignado.setPlaceholder("Filtre Asignado");

            fechaLog.setVisible(false);
            comentarioLog.setVisible(false);

//            fechaDesde.addValueChangeListener(e -> findByAll());
//            fechaHasta.addValueChangeListener(e -> findByAll());
            btnSearch.addClickListener(e -> findByAll());

            filter.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                        if (filter.getValue() != null) {
                            String num = filter.getValue();
                            filterFuncionario.setValue(null);
                            filterAprobado.setValue(null);
                            filterParametro.setValue(null);
                            fechaDesde.setValue(null);
                            fechaHasta.setValue(null);
                            cargarParametro(num);
                        }
                    }
                }

                private void cargarParametro(String num) {
                    grid.clearSortOrder();
                    filter.setValue(num);
                    try {
//                        BoletaCab s = boletasController.listarPorIdHere(Long.parseLong(num));
//                        if (s.getId() == Long.parseLong(num)) {
//                            grid.setItems(s);
//                        }
                    } catch (Exception e) {
                        grid.clearSortOrder();
                        grid.setItems(new ArrayList<>());
                        labelTotalizador.setCaption("Total de registros: " + 0);
                    } finally {
                    }
                }
            });

            SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatConHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

//            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
////                lista = boletasController.listadeBoletasSinConfirmar();
//            } else {
//                lista = new ArrayList<>();
//            }
            lista = listBoletasCab = boletasCabDao.buscarFiltros(formulario.getValue(), cbPeriodo.getValue(), txtAno.getValue());

            grid.setItems(lista);
            labelTotalizador.setCaption("Total de registros: " + lista.size());
            grid.removeAllColumns();
            grid.addColumn(e -> {
                return e.getId();
            }).setCaption("Cód");
//            grid.addComponentColumn(e -> {
//                return e.getEstadoIcon();
//            }).setCaption("Aprobado");
            grid.addColumn(e -> {
                return e.getDescripcion();
            }).setCaption("Comercios");

            grid.addColumn(e -> {
                return e.getPeriodo();
            }).setCaption("Periodo");

            grid.addColumn(e -> {
                return e.getFecha() == null ? "--" : formatSinHora.format(e.getFecha());
            }).setCaption("Fecha");
            grid.addColumn(e -> {
                String pattern = "###,###";
                DecimalFormat decimalFormat = new DecimalFormat(pattern);
//                String value = e.getImporte() + "".replace(".", "");
                String value = "0";
                try {
                    value = e.getMonto() == null ? "0" : e.getMonto() + "".replace(".", "");
                } catch (Exception ex) {
                } finally {
                }

                return decimalFormat.format(Long.parseLong(value));
            }).setCaption("Monto");

//            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
//                grid.addComponentColumn(this::buildConfirmButton).setCaption("Confirmar");
//            }
            grid.setSizeFull();
//            filterParametro.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterParametro.getValue() != null) {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
//                            filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
//            });
//            filterFuncionario.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterFuncionario.getValue() != null) {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
//                            filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
//            });
//            filterAprobado.addValueChangeListener(vcl -> {
//                filter.setValue("");
//                grid.clearSortOrder();
//                if (filterFuncionario.getValue() != null) {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                } else {
//                    List<Boletas> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                            filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get());
//                    grid.setItems(listTicket);
//                    labelTotalizador.setCaption("Total de registros: " + listTicket.size());
//                }
////                limpiarLog();
//            });

            descargar.setVisible(false);

            grid.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    try {
//                        if (numRowSelected >= 0) {
//                        if (numRowSelected >= 0 && spd.getAprobado() == 1) {
//                            InputModal inputModal = new InputModal("");
//                            inputModal.openInModalPopup("Rechazo", "");
//                            inputModal.getOkButton().addClickListener(listener -> {
//
//                                Date fechaHoy = new Date();
//                                fechaHoy.setHours(0);
//                                fechaHoy.setMinutes(0);
//                                fechaHoy.setSeconds(0);
//                                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));
//
//                                Date fechaSolicitud = spd.getFechaini();
//                                fechaSolicitud.setHours(0);
//                                fechaSolicitud.setMinutes(0);
//                                fechaSolicitud.setSeconds(0);
//                                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
//                                boolean val = false;
//                                if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
//                                    val = true;
//                                } else if (sqlDateSolicitud.before(sqlDateHoy)) {
//                                    val = false;
//                                } else if (sqlDateSolicitud.after(sqlDateHoy)) {
//                                    val = true;
//                                }
//
//                                if (val) {
//                                    if (spd.getParametro().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
//                                        Date fechaInicio = spd.getFechaini();
//                                        Date fechaFin = spd.getFechafin();
//
//                                        List<Rotaciones> listRotacion = rotacionesController.listarPorFechas(fechaFin, fechaFin, spd.getFuncionario().getIdfuncionario());
//                                        if (listRotacion.size() > 0) {
//                                            Rotaciones rotacion = listRotacion.get(0);
//                                            rotacion.setFecha(fechaInicio);
//                                            rotacionesController.guardarRotaciones(rotacion);
//                                        }
//                                    }
//                                    spd.setAprobado(2L);
//                                    spd.setFuncrrhh(UserHolder.get().getIdfuncionario());
//                                    spd.setRechazo(inputModal.getText());
//                                    boletasController.guardarBoletas(spd);
//                                    grid.clearSortOrder();
//                                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
//                                        lista = boletasController.listadeBoletasSinConfirmar();
//                                    } else {
//                                        lista = new ArrayList<>();
//                                    }
//                                    grid.setItems(lista);
//                                    inputModal.closePopup();
//                                } else {
//                                    Notification.show("Mensaje del sistema", "La licencia generada ya ha sido efectuada.", Notification.Type.ERROR_MESSAGE);
//                                }
//                            });
//                        }
                    } catch (Exception e) {

                    } finally {
                    }

                }
            });

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(horizontalLayout);
            grid.setSizeFull();
            verticalLayout.addComponent(grid);
            verticalLayout.setMargin(true);
            verticalLayout.setSpacing(true);
            verticalLayout.setSizeFull();
            verticalLayout.setExpandRatio(grid, 6F);
            verticalLayout.addComponent(labelTotalizador);
            verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
            HorizontalLayout horizontalLayout1 = crearSegundoGrid();
            horizontalLayout1.setSizeFull();
            verticalLayout.addStyleName("crud-main-layout");
            addComponent(verticalLayout);

            grid.addItemClickListener(listener -> {
                if (listener.getMouseEventDetails().isDoubleClick()) {
                    editarBoletas.setVisible(false);
                    imprimirBoletas.setVisible(false);
                    cerrarTicket.setVisible(false);

                    formSolicitudDetalle = new BoletasDetalleView();
                    long idTicket = listener.getItem().getId();
//                    formSolicitudDetalle.setIdTicket(idTicket);
                    formSolicitudDetalle.setIdTicket(listener.getItem());
                    System.out.println("EL TICKET ES EL # " + idTicket);
                    UI.getCurrent().addWindow(formSolicitudDetalle);
                    formSolicitudDetalle.setVisible(true);
                }
                if (listener.getItem() != null) {
                    spd = listener.getItem();
                    numRowSelected = listener.getRowIndex();
                }
            });
            grid.asSingleSelect().addValueChangeListener(event -> {
                solicitudSeleccionado = event.getValue();
                editarBoletas.setVisible(true);
                imprimirBoletas.setVisible(true);
            });
            editarBoletas.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
                    try {
                        BoletasForm soliForm = new BoletasForm();
                        try {
                            BoletaCab lic = boletasController.listarPorId(solicitudSeleccionado.getId());
                            if (lic.getId() != null) {
                                soliForm.editarRegistro(lic);
                            }
                        } catch (Exception e) {
//                            soliForm.editarRegistro(boletasController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                        } finally {
                        }

                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
                            listBoletasCab = new ArrayList<>();
//                            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                            listBoletasCab = boletasCabDao.buscarFiltros(formulario.getValue(), cbPeriodo.getValue(), txtAno.getValue());
//                            } else {
//                                listBoletasCab = new ArrayList<>();
//                            }
                            grid.setItems(listBoletasCab);
                            labelTotalizador.setCaption("Total de registros: " + lista.size());
                        });
                    } catch (Exception e) {
                        BoletasForm soliForm = new BoletasForm();
                        try {
//                            soliForm.editarRegistro(boletasController.listarPorId(solicitudSeleccionado.getId()));
                        } catch (Exception ex) {
//                            soliForm.editarRegistro(boletasController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                        } finally {
                        }
                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
                            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
//                                lista = boletasController.listadeBoletasSinConfirmar();
                            } else {
                                lista = new ArrayList<>();
                            }
                            grid.setItems(lista);
                            labelTotalizador.setCaption("Total de registros: " + lista.size());
                        });
                    } finally {
                    }

                }
            });
            imprimirBoletas.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
//                    try {
//                        if (solicitudSeleccionado.getConfirmado()) {
//                        if (solicitudSeleccionado.getEncargado() != null) {
                    imrpimirPDF();
//                        } else {
//                            Notification.show("La licencia no ha sido confirmada, no es posible imprimirla.", Notification.Type.HUMANIZED_MESSAGE);
//                        }
//                    } catch (Exception e) {
//                        imrpimirPDF();
//                    } finally {
//                    }

                }
            });

            nuevaBoletas.addClickListener(clickEvent -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
                if (listFeriado.size() == 0) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    BoletasForm solicitudForm = new BoletasForm();
                    UI.getCurrent().addWindow(solicitudForm);

                    solicitudForm.nuevoRegistro();
                    solicitudForm.setVisible(true);
                    solicitudForm.setCancelListener(reclamo -> {
                        //form = null;
                        grid.clearSortOrder();
                    });
                    solicitudForm.setSaveListener(reclamo -> {
                        grid.clearSortOrder();
                        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
//                            lista = boletasController.listadeBoletasSinConfirmar();
                        } else {
                            lista = new ArrayList<>();
                        }
                        grid.setItems(lista);
                        labelTotalizador.setCaption("Total de registros: " + lista.size());
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout crearSegundoGrid() {
        HorizontalLayout layout = new HorizontalLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        horizontalLayout.addComponent(descargar);
        verticalLayout.addComponent(horizontalLayout);
        CssLayout expander1 = new CssLayout();
        expander1.setSizeFull();
        expander1.setStyleName("expander");
        verticalLayout.addComponent(expander1);
        verticalLayout.setExpandRatio(expander1, 0.9F);
        verticalLayout.addComponent(fechaLog);
        comentarioLog.setSizeFull();
        verticalLayout.addComponent(comentarioLog);
        verticalLayout.setSpacing(false);

        layout.addComponent(verticalLayout);
        layout.setSpacing(true);
        return layout;
    }

    private void cargarParametro(String num) {
        grid.clearSortOrder();
        filter.setValue(num);
        try {
//            BoletaCab s = boletasController.listarPorIdHere(Long.parseLong(num));
//            if (s.getId() == Long.parseLong(num)) {
//                grid.setItems(s);
//            }
        } catch (Exception e) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
            labelTotalizador.setCaption("Total de registros: " + 0);
        } finally {
        }
    }

    private void findByAll() {
        try {
            if (formulario == null && cbPeriodo == null && txtAno.getValue().equalsIgnoreCase("")) {
                Notification.show("Mensaje del sistema", "Debe seleccionar un filtro para realizar la búsqueda!.", Notification.Type.ERROR_MESSAGE);
                grid.clearSortOrder();
                grid.setItems(new ArrayList<>());
            } else {
                listBoletasCab = boletasCabDao.buscarFiltros(formulario.getValue(), cbPeriodo.getValue(), txtAno.getValue());
//                listBoletasDetalles.add(bd);
                grid.clearSortOrder();
                grid.setItems(listBoletasCab);
            }
        } catch (Exception e) {
            Notification.show("Mensaje del sistema", "Debe seleccionar un filtro para realizar la búsqueda!.", Notification.Type.ERROR_MESSAGE);
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
        } finally {
        }

        if (!filter.getValue().trim().equals("")) {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                if (filter.getValue() != null) {
                    String num = filter.getValue();
                    filterFuncionario.setValue(null);
                    filterAprobado.setValue(null);
                    filterParametro.setValue(null);
                    fechaDesde.setValue(null);
                    fechaHasta.setValue(null);
                    cargarParametro(num);
                }
            }
        } else {
            //        DateUtils.asDate(fechaDesde.getValue())
            if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
                if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                    filtroConFecha();
                } else {
                    Notification.show("Mensaje del Sistema", "Fecha hasta debe ser mayor o igual a fecha desde.", Notification.Type.HUMANIZED_MESSAGE);
                }
            } else {
                Notification.show("Mensaje del Sistema", "Fecha desde y fecha hasta no deben quedar vacíos.", Notification.Type.HUMANIZED_MESSAGE);
            }
        }
    }

    private void filtroConFecha() {
        filter.setValue("");
        grid.clearSortOrder();
        if (filterFuncionario.getValue() != null) {
//            List<BoletaCab> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
//                    filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get(), DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios);
//            grid.setItems(listTicket);
//            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        } else {
//            List<BoletaCab> listTicket = boletasController.listadeBoletasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
//                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(), filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get(), DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios);
//            grid.setItems(listTicket);
//            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        }
    }

    private HorizontalLayout createHorizontalLayout() {
        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnSearch.setIcon(VaadinIcons.SEARCH);
        nuevaBoletas.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        nuevaBoletas.setIcon(VaadinIcons.PLUS_CIRCLE);
        editarBoletas.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        editarBoletas.setIcon(VaadinIcons.EDIT);
        imprimirBoletas.addStyleName(MaterialTheme.BUTTON_ROUND);
        imprimirBoletas.setIcon(VaadinIcons.PRINT);
        aceptarConformidad.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        aceptarConformidad.setIcon(FontAwesome.PARAGRAPH);
        cerrarTicket.addStyleName(MaterialTheme.BUTTON_ROUND + " " + ValoTheme.BUTTON_DANGER);
        cerrarTicket.setIcon(FontAwesome.EDIT);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        HorizontalLayout layoutIzquierdo = new HorizontalLayout();
        formulario.setWidth(15f, TextField.UNITS_EM);
        cbPeriodo.setWidth(10f, TextField.UNITS_EM);
        txtAno.setWidth(5f, TextField.UNITS_EM);
        layoutIzquierdo.addComponent(formulario);
        layoutIzquierdo.addComponent(cbPeriodo);
        layoutIzquierdo.addComponent(txtAno);

        layoutIzquierdo.addComponent(btnSearch);
        layoutIzquierdo.addComponent(nuevaBoletas);
        layoutIzquierdo.addComponent(editarBoletas);
        layoutIzquierdo.addComponent(imprimirBoletas);

        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        verticalLayoutIzquierda.addComponent(layoutIzquierdo);

//        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
//        layoutFuncEstado.addComponent(filterAprobado);
//        layoutFuncEstado.setExpandRatio(filterAprobado, 2);
//        layoutFuncEstado.addComponent(filterParametro);
//        layoutFuncEstado.setExpandRatio(filterParametro, 2);
//        layoutFuncEstado.addComponent(filter);
//        layoutFuncEstado.setExpandRatio(filter, 2);
//        layoutFuncEstado.addComponent(fechaDesde);
//        layoutFuncEstado.setExpandRatio(fechaDesde, 2);
//        layoutFuncEstado.addComponent(fechaHasta);
//        layoutFuncEstado.setExpandRatio(fechaHasta, 2);
//        layoutFuncEstado.addComponent(btnSearch);
//        layoutFuncEstado.setExpandRatio(btnSearch, 2);
//        verticalLayoutIzquierda.addComponent(layoutFuncEstado);
        layout.addComponent(verticalLayoutIzquierda);

//        layout.addComponent(btnSearch);
////        layout.addComponent(aceptarConformidad);
//        layout.addComponent(nuevaBoletas);
//        layout.addComponent(editarBoletas);
//        layout.addComponent(imprimirBoletas);
        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void fileReceived(String filename, String mime, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public void limpiarLog() {
        grid.clearSortOrder();
        comentarioLog.setValue("");
        fechaLog.setValue(null);
        fechaLog.setEnabled(false);
        comentarioLog.setEnabled(false);
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private void cargarVacacionesDisponible(BoletaCab s) {
//        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(s.getFuncionario().getIdfuncionario());
//        for (Vacaciones listado : listVacaciones) {
//            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
//        }
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void guardarVacaciones(BoletaCab solicitud, long tomada, long restante) {
        Calendar cal = Calendar.getInstance();

        Vacaciones vacas = new Vacaciones();
        vacas.setPeriodo(cal.get(Calendar.YEAR) + "");
        vacas.setCantdiavaca(0l);
        vacas.setCantdiatomada(tomada);
        vacas.setCantdiarestante(restante - tomada);
//        vacas.setFuncionario(solicitud.getFuncionario());
//        vacas.setBoletas(solicitud);

        vacacionesDao.guardar(vacas);
    }

    private void imrpimirPDF() {
//        Map pSQL = new HashMap<String, Object>();
//        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
////                    String val = "{\"ventas\": " + recuperarTickets() + "}";
////        pSQL.put("repJsonString", "{\"ventas\": []}");
//
//        if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
//            pSQL.put("repJsonString", "{\"ventas\": " + cargarLicenciaCompensar() + "}");
//        } else {
//            pSQL.put("repJsonString", "{\"ventas\": " + cargarLicenciaSinCompensar() + "}");
//        }
//
////                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
//        Date date = new Date();
//        Timestamp ts = new Timestamp(date.getTime());
//        String fechaArray[] = ts.toString().split(" ");
//        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//        pSQL.put("subRepTimestamp", subRepTimestamp);
//        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
////        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
//        if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
//            pSQL.put("subRepSucursal", "LICENCIA A COMPENSAR");
//        } else {
//            pSQL.put("subRepSucursal", "LICENCIAS");
//        }
//
//        pSQL.put("subRepPathLogo", subImg);
//        pSQL.put("subRepPathLogoCP", subImg);
//        StreamResource.StreamSource source = new StreamResource.StreamSource() {
//
//            public InputStream getStream() {
//                byte[] b = null;
//                String archivo = "";
//                if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
//                    archivo += "licencia_compensar";
//                } else {
//                    archivo += "licencia_sin_compensar";
//                }
//                try {
//                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
////                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
//                } catch (JRException ex) {
//                    ex.printStackTrace();
//                }
//                return new ByteArrayInputStream(b);
//            }
//        };
//
//        String archivo = "";
//        if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
//            archivo += "licencia_compensar";
//        } else {
//            archivo += "licencia_sin_compensar";
//        }
//
//        StreamResource resource = new StreamResource(source, archivo + ".pdf");
//        resource.setCacheTime(0);
//        resource.setMIMEType("application/pdf");
//
//        Window window = new Window();
//        window.setWidth(800, Sizeable.Unit.PIXELS);
//        window.setHeight(600, Sizeable.Unit.PIXELS);
//        window.setModal(true);
//        window.center();
//        BrowserFrame pdf = new BrowserFrame("test", resource);
//        pdf.setSizeFull();
//
//        window.setContent(pdf);
//        getUI().addWindow(window);
    }

    private JSONArray cargarLicenciaCompensar() {
//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
//        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
//        SimpleDateFormat sdf = new SimpleDateFormat("mm");
//        JSONParser parser = new JSONParser();
//        JSONArray jsonArrayDato = new JSONArray();
//        List<BoletasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
//        int num = 0;
//        for (BoletasCompensar solicitudProduccionDetalle : solicitudDetalle) {
//            JSONObject jsonObj = new JSONObject();
//
//            int minFin = ((solicitudProduccionDetalle.getHorafin().getHours() * 60) + solicitudProduccionDetalle.getHorafin().getMinutes());
//            int minInicio = ((solicitudProduccionDetalle.getHoraini().getHours() * 60) + solicitudProduccionDetalle.getHoraini().getMinutes());
//
//            int minFinHere = ((solicitudProduccionDetalle.getLicencia().getHorafin().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHorafin().getMinutes());
//            int minInicioHere = ((solicitudProduccionDetalle.getLicencia().getHoraini().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHoraini().getMinutes());
//
//            jsonObj.put("codigo", solicitudSeleccionado.getId());
//            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
//            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
//            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());
//            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));
//
//            jsonObj.put("fechaPermiso", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//            jsonObj.put("desdePermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHoraini()));
//            jsonObj.put("hastaPermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHorafin()));
//            try {
//                long millis = (minFinHere - minInicioHere);
//                jsonObj.put("cantPermiso", sdfHM.format(sdf.parse(millis + "")));
//            } catch (ParseException ex) {
//                jsonObj.put("cantPermiso", "00.00");
//            }
//            jsonObj.put("motivoPermiso", solicitudProduccionDetalle.getLicencia().getMotivo());
//
//            jsonObj.put("fechaCompensacion", formatter.format(solicitudProduccionDetalle.getFechacompensar()));
//            jsonObj.put("desdeCompensacion", formatHor.format(solicitudProduccionDetalle.getHoraini()));
//            jsonObj.put("hastaCompensacion", formatHor.format(solicitudProduccionDetalle.getHorafin()));
//            try {
//                long millis = minFin - minInicio;
//                jsonObj.put("cantCompensacion", sdfHM.format(sdf.parse(millis + "")));
//            } catch (ParseException ex) {
//                jsonObj.put("cantCompensacion", "00.00");
//            }
//            jsonObj.put("tareaCompensacion", solicitudProduccionDetalle.getObservacion());
//
//            jsonObj.put("num", (num + 1));
//            num++;
//
//            jsonArrayDato.add(jsonObj);
//        }
//        return jsonArrayDato;
        return null;
    }

    private JSONArray cargarLicenciaSinCompensar() {
//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
//        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
//        JSONParser parser = new JSONParser();
//        JSONArray jsonArrayDato = new JSONArray();
//        List<BoletasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
//        int num = 0;
//        for (BoletasCompensar solicitudProduccionDetalle : solicitudDetalle) {
//            JSONObject jsonObj = new JSONObject();
//
//            jsonObj.put("codigo", solicitudSeleccionado.getId());
//            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
//            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
//            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());
//            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));
//
//            switch (solicitudProduccionDetalle.getLicencia().getParametro().getCodigo().toLowerCase()) {
//                case "licencia_matrimonio":
//                    jsonObj.put("marca1", "X");
//                    jsonObj.put("desde1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca2", " ");
//                    jsonObj.put("desde2", " ");
//                    jsonObj.put("hasta2", " ");
//                    jsonObj.put("marca3", " ");
//                    jsonObj.put("desde3", " ");
//                    jsonObj.put("hasta3", " ");
//                    jsonObj.put("marca4", " ");
//                    jsonObj.put("desde4", " ");
//                    jsonObj.put("hasta4", " ");
//                    jsonObj.put("marca5", " ");
//                    jsonObj.put("desde5", " ");
//                    jsonObj.put("hasta5", " ");
//                    jsonObj.put("marca6", " ");
//                    jsonObj.put("desde6", " ");
//                    jsonObj.put("hasta6", " ");
//                    break;
//                case "licencia_duelo":
//                    jsonObj.put("marca2", "X");
//                    jsonObj.put("desde2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca1", " ");
//                    jsonObj.put("desde1", " ");
//                    jsonObj.put("hasta1", " ");
//                    jsonObj.put("marca3", " ");
//                    jsonObj.put("desde3", " ");
//                    jsonObj.put("hasta3", " ");
//                    jsonObj.put("marca4", " ");
//                    jsonObj.put("desde4", " ");
//                    jsonObj.put("hasta4", " ");
//                    jsonObj.put("marca5", " ");
//                    jsonObj.put("desde5", " ");
//                    jsonObj.put("hasta5", " ");
//                    jsonObj.put("marca6", " ");
//                    jsonObj.put("desde6", " ");
//                    jsonObj.put("hasta6", " ");
//                    break;
//                case "estudio_pap":
//                    jsonObj.put("marca3", "X");
//                    jsonObj.put("desde3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca1", " ");
//                    jsonObj.put("desde1", " ");
//                    jsonObj.put("hasta1", " ");
//                    jsonObj.put("marca2", " ");
//                    jsonObj.put("desde2", " ");
//                    jsonObj.put("hasta2", " ");
//                    jsonObj.put("marca4", " ");
//                    jsonObj.put("desde4", " ");
//                    jsonObj.put("hasta4", " ");
//                    jsonObj.put("marca5", " ");
//                    jsonObj.put("desde5", " ");
//                    jsonObj.put("hasta5", " ");
//                    jsonObj.put("marca6", " ");
//                    jsonObj.put("desde6", " ");
//                    jsonObj.put("hasta6", " ");
//                    break;
//                case "reposo_medico":
//                    jsonObj.put("marca4", "X");
//                    jsonObj.put("desde4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca1", " ");
//                    jsonObj.put("desde1", " ");
//                    jsonObj.put("hasta1", " ");
//                    jsonObj.put("marca2", " ");
//                    jsonObj.put("desde2", " ");
//                    jsonObj.put("hasta2", " ");
//                    jsonObj.put("marca3", " ");
//                    jsonObj.put("desde3", " ");
//                    jsonObj.put("hasta3", " ");
//                    jsonObj.put("marca5", " ");
//                    jsonObj.put("desde5", " ");
//                    jsonObj.put("hasta5", " ");
//                    jsonObj.put("marca6", " ");
//                    jsonObj.put("desde6", " ");
//                    jsonObj.put("hasta6", " ");
//                    break;
//                case "cambio_rotacion":
//                    jsonObj.put("marca5", "X");
//                    jsonObj.put("desde5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca1", " ");
//                    jsonObj.put("desde1", " ");
//                    jsonObj.put("hasta1", " ");
//                    jsonObj.put("marca2", " ");
//                    jsonObj.put("desde2", " ");
//                    jsonObj.put("hasta2", " ");
//                    jsonObj.put("marca3", " ");
//                    jsonObj.put("desde3", " ");
//                    jsonObj.put("hasta3", " ");
//                    jsonObj.put("marca4", " ");
//                    jsonObj.put("desde4", " ");
//                    jsonObj.put("hasta4", " ");
//                    jsonObj.put("marca6", " ");
//                    jsonObj.put("desde6", " ");
//                    jsonObj.put("hasta6", " ");
//                    break;
//                case "permiso_sin_goce_salario":
//                    jsonObj.put("marca6", "X");
//                    jsonObj.put("desde6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
//                    jsonObj.put("hasta6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
//                    jsonObj.put("marca1", " ");
//                    jsonObj.put("desde1", " ");
//                    jsonObj.put("hasta1", " ");
//                    jsonObj.put("marca2", " ");
//                    jsonObj.put("desde2", " ");
//                    jsonObj.put("hasta2", " ");
//                    jsonObj.put("marca3", " ");
//                    jsonObj.put("desde3", " ");
//                    jsonObj.put("hasta3", " ");
//                    jsonObj.put("marca4", " ");
//                    jsonObj.put("desde4", " ");
//                    jsonObj.put("hasta4", " ");
//                    jsonObj.put("marca5", " ");
//                    jsonObj.put("desde5", " ");
//                    jsonObj.put("hasta5", " ");
//                    break;
//            }
//
//            jsonObj.put("num", (num + 1));
//            num++;
//
//            jsonArrayDato.add(jsonObj);
//        }
//        return jsonArrayDato;
        return null;
    }

    private void limpiarDatos() {
        if (filter.getValue().equalsIgnoreCase("")) {
            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
        }
    }
}
