package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;

import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.UnaprobacionDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 4/7/2016.
 */
public class LicenciaDetalleView extends Window {

//    private TextArea comentarios = new TextArea("Agregar nuevo comentario");
    final Image image = new Image("Imagen");
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";
    // Create upload stream
    FileOutputStream fos = null;
    //DUpload upload = new DUpload("Subir Archivo", this::fileReceived);
    Grid<LicenciasCompensar> gridLog = new Grid<>(LicenciasCompensar.class);
    Grid<LicenciasCompensar> gridLogProduccion = new Grid<>(LicenciasCompensar.class);
    Grid<VacacionesDetalle> gridLogDetalle = new Grid<>(VacacionesDetalle.class);
    LicenciasCompensarDao solicitudDetalleController = ResourceLocator.locate(LicenciasCompensarDao.class);
    UnaprobacionDao unaprobacionDaoController = ResourceLocator.locate(UnaprobacionDao.class);
    LicenciasDao solicitudController = ResourceLocator.locate(LicenciasDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    VacacionesDao vacacionController = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionDetalleController = ResourceLocator.locate(VacacionesDetalleDao.class);
    Label labelDescripcion = new Label();
    Label labelUrl = new Label();
    public long idTicket;
    private Button guardar = new Button("Aprobar");
    private Button cancelar = new Button("Cerrar");
    private Binder<LicenciasCompensar> binder = new Binder<>(LicenciasCompensar.class);

//    Services ejb = ResourceLocator.locate(Services.class);
//    Map<Long, Funcionario> mapFuncionario = new HashMap<Long, Funcionario>();
//    EnviarMail enviarMailEJB = ResourceLocator.locate(EnviarMail.class);
    public LicenciaDetalleView() {
        VerticalLayout verticalLayout = createForm();
        setContent(verticalLayout);
        setWidth("85%");
        setHeight("95%");
//        setCaption("Comentarios");
        setModal(true);
        center();

        gridLog.setCaption("Detalle de Licencia");
        gridLogDetalle.setVisible(false);

        addCloseListener(closeEvent -> {
            close();
        });

        guardar.addClickListener(cl -> {
            try {
                save();
            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
        cancelar.addClickListener(clickEvent -> {
            close();
        });
    }

    private void poblarGrillaLogs(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
            try {
                System.out.println("poblarGrillaLogs()");
                List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
//                setCaption(solicitudDetalle.get(0).getLicencia().getFuncionario().getNombreCompleto() + "      |      Inicio: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechaini()) + " - Fin: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechafin()));
                setCaption(solicitudDetalle.get(0).getLicencia().getNombrefuncionario() + " | " + solicitudDetalle.get(0).getLicencia().getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFuncionario().getFechaingreso()));

//                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1) && (solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null)) {
                    guardar.setVisible(true);
                } else {
                    guardar.setVisible(false);
                }
                gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());
                gridLog.setItems(solicitudDetalle);
                gridLog.removeAllColumns();
                gridLog.addColumn(e -> {
                    return e.getLicencia() == null ? "" : e.getLicencia().getId();
                }).setCaption("COD");
                gridLog.addColumn(e -> {
                    return e.getFechacompensar() == null ? "--" : simpleDateFormat.format(e.getFechacompensar());
                }).setCaption("FECHA");
                if (solicitudDetalle.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                    gridLog.addColumn(e -> {
                        return e.getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getHoraini());
                    }).setCaption("HORA INICIO");
                    gridLog.addColumn(e -> {
                        return e.getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getHorafin());
                    }).setCaption("HORA FIN");
                }
                /*gridLog.addColumn(e -> {
                    return e.getObservacion() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");*/
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
                }).setCaption("1RA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado2() == null ? "" : e.getLicencia().getEncargado2().getNombreCompleto();
                }).setCaption("2DA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
                }).setCaption("RRHH");
                gridLog.addColumn(e -> {
                    return e.getObservacion();
                }).setCaption("TAREA");
                /*if (solicitudDetalle.get(0).getLicencia().getCantdia() != null && solicitudDetalle.get(0).getLicencia().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getCantdia();
                    }).setCaption("CANT DIA");
                }*/
                if (solicitudDetalle.get(0).getLicencia().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getLicencia().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }
//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");
                gridLog.setSizeFull();
                gridLogProduccion.setVisible(false);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void poblarGrillaLogs2(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
            try {
                System.out.println("poblarGrillaLogs()");
                List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
//                setCaption(solicitudDetalle.get(0).getLicencia().getFuncionario().getNombreCompleto() + "      |      Inicio: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechaini()) + " - Fin: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechafin()));
                setCaption(solicitudDetalle.get(0).getLicencia().getNombrefuncionario() + " | " + solicitudDetalle.get(0).getLicencia().getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFuncionario().getFechaingreso()));

                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                    guardar.setVisible(true);
                } else {
                    guardar.setVisible(false);
                }
                gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());
                gridLog.setItems(solicitudDetalle);
                gridLog.removeAllColumns();
                gridLog.addColumn(e -> {
                    return e.getLicencia() == null ? "" : e.getLicencia().getId();
                }).setCaption("COD");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFechaini() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechaini());
                }).setCaption("FEC INICIO");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFechafin() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechafin());
                }).setCaption("FEC FIN");
                if (solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO") || solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getLicencia().getHoraini());
                    }).setCaption("HORA INICIO");
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getLicencia().getHorafin());
                    }).setCaption("HORA FIN");
                }
                /*gridLog.addColumn(e -> {
                    return e.getObservacion() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");*/
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
                }).setCaption("ENCARGADO");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
                }).setCaption("RRHH");
                gridLog.addColumn(e -> {
                    return e.getObservacion();
                }).setCaption("TAREA");
                /*if (solicitudDetalle.get(0).getLicencia().getCantdia() != null && solicitudDetalle.get(0).getLicencia().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getCantdia();
                    }).setCaption("CANT DIA");
                }*/
                if (solicitudDetalle.get(0).getLicencia().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getLicencia().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }
//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");
                gridLog.setSizeFull();
                gridLogProduccion.setVisible(false);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void validate() {
        boolean savedEnabled = true;
//        if (comentarios == null || comentarios.isEmpty()) {
//            savedEnabled = false;
//        }
        guardar.setEnabled(savedEnabled);
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
//        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        ///layout.addComponent(upload);
        //comentarios.setNullRepresentation("");
//        comentarios.setWidth("100%");
//        comentarios.setHeight("35%");
        gridLog.setHeight("500px");
        layout.addComponent(labelDescripcion);
        layout.addComponent(gridLog);
        layout.addComponent(gridLogDetalle);
        layout.addComponent(gridLogProduccion);
        HorizontalLayout hupload = new HorizontalLayout();
        layout.addComponent(hupload);
        layout.addComponent(labelUrl);
//        layout.addComponent(comentarios);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(guardar);
        horizontalButton.addComponent(cancelar);
//        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private void save() {
        try {
            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(this.idTicket);
            Licencias solicitud = solicitudDetalle.get(0).getLicencia();
            if (solicitud.getEncargado() == null) {
                if (solicitud.getFuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || solicitud.getFuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || unaprobacionDaoController.getOne(solicitud.getFuncionario().getIdfuncionario()) != null) {
                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                }
                solicitud.setEncargado(UserHolder.get().getIdfuncionario());
                solicitudController.guardarLicencias(solicitud);
            } else {
                long idEncargado1 = solicitud.getEncargado().getIdfuncionario();
                long idEncargado2 = UserHolder.get().getIdfuncionario().getIdfuncionario();
                System.out.println(idEncargado1 + " | " + idEncargado2);
                if (idEncargado1 == idEncargado2) {
                    Notification.show("Otro encargado debe aprobar la solicitud.");
                } else {
                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                    solicitudController.guardarLicencias(solicitud);
                }
            }
            close();
        } catch (Exception e) {
            close();
        } finally {
        }

    }

    public void setComentarios(TextArea comentarios) {
//        this.comentarios = comentarios;
    }

    public void setIdTicket(Licencias solicitud) {
        Licencias soli = solicitudController.listarPorId(solicitud.getId());
        this.idTicket = solicitud.getId();
        poblarGrillaLogs(idTicket);
        /*if (!soli.getParametro().getDescripcion().equalsIgnoreCase("licencia_compensar")) {
            this.idTicket = solicitud.getId();
            poblarGrillaLogs(idTicket);
        } else {
            this.idTicket = solicitud.getId();
            poblarGrillaDetalle2(solicitud);
        }*/
    }
//    public void setIdTicket(long idTicket) {
//        this.idTicket = idTicket;
//        poblarGrillaLogs(idTicket);
//    }

    void setListaLicencia(List<Licencias> array) {
        List<LicenciasCompensar> listLicenciasCompensar = new ArrayList<>();
        for (Licencias solicitud : array) {
            listLicenciasCompensar.addAll(solicitudDetalleController.listarPorIdLicencia(solicitud.getId()));
        }
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
//        List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
        try {
            System.out.println("poblarGrillaLogs()");
//            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
            setCaption(array.get(0).getNombrefuncionario() + " | " + array.get(0).getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(array.get(0).getFuncionario().getFechaingreso()));
//                gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getId() + " )- " + solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());
            gridLog.setItems(listLicenciasCompensar);
            gridLog.removeAllColumns();
            gridLog.addColumn(e -> {
                return e.getLicencia() == null ? "" : e.getLicencia().getId();
            }).setCaption("COD");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getLicencia().getFechaini());
            }).setCaption("FEC INICIO");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getLicencia().getFechafin());
            }).setCaption("FEC FIN");
            gridLog.addColumn(e -> {
                return e.getLicencia().getCantidad();
            }).setCaption("CANT DIA");
            gridLog.addColumn(e -> {
                return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
            }).setCaption("ENCARGADO");
            gridLog.addColumn(e -> {
                return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
            }).setCaption("RRHH");
            gridLog.setSizeFull();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void poblarGrillaDetalle(Licencias solicitud) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdLicencia(solicitud.getId()) != null) {
            try {
                List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitud.getId());
                setCaption("solicitud de producción".toUpperCase());
                setCaption(solicitud.getAreafunc().toUpperCase() + " | " + solicitud.getCargofunc().toUpperCase());

//                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1) && (solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null)) {
                    guardar.setVisible(true);
                } else {
                    guardar.setVisible(false);
                }
                try {
                    gridLog.setCaption("MOTIVO RECHAZO: " + solicitud.getRechazo().toUpperCase());
                } catch (Exception e) {
                    gridLog.setCaption("");
                } finally {
                }

                gridLogProduccion.setItems(solicitudDetalle);
                gridLogProduccion.removeAllColumns();
                gridLogProduccion.addColumn(e -> {
                    return e.getId();
                }).setCaption("CODIGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getNombrefuncionario();
                }).setCaption("NOMBRE Y APELLIDO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getCargofunc().toUpperCase();
                }).setCaption("CARGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getDescripcion().toUpperCase();
                }).setCaption("DESCRIPCION ACTIVIDAD");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getFechacreacion() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechacreacion());
                }).setCaption("FECHA");

//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");
                gridLogProduccion.setSizeFull();
                gridLogProduccion.setVisible(true);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
