/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.VerificaciondetalleDao;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;
import py.mutualmsp.mutualweb.formularios.VerificaciondetalleForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class VerificaciondetalleVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Verificaciondetalle";
    Grid<Verificaciondetalle> grillaVerificaciondetalle = new Grid<>(Verificaciondetalle.class);
    VerificaciondetalleDao verificaciondetalleDao = ResourceLocator.locate(VerificaciondetalleDao.class);
    List<Verificaciondetalle> lista = new ArrayList<>();
    Button btnNuevo = new Button("Insertar");
    VerificaciondetalleForm verificaciondetalleForm = new VerificaciondetalleForm();
    
    public VerificaciondetalleVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        verificaciondetalleForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponent(btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_LEFT);
        topLayout.addStyleName("top-bar");
        
        btnNuevo.addClickListener(e ->{
            grillaVerificaciondetalle.asSingleSelect().clear();
            verificaciondetalleForm.setVerificaciondetalle(new Verificaciondetalle());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaVerificaciondetalle, verificaciondetalleForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaVerificaciondetalle, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = verificaciondetalleDao.listaVerificaciondetalle();
            grillaVerificaciondetalle.setItems(lista);
            grillaVerificaciondetalle.removeAllColumns();
            grillaVerificaciondetalle.addColumn(Verificaciondetalle::getIdrequisito).setCaption("Id Requisito");
            grillaVerificaciondetalle.addColumn(requisito -> {
                return requisito.getIdrequisito().getDescripcion();}).setCaption("Requisito");
            grillaVerificaciondetalle.addColumn(verificado -> { 
                return verificado.getVerificado()== true ? "SI" : "NO";}).setCaption("Verificado");
            grillaVerificaciondetalle.addColumn(Verificaciondetalle::getObservacion).setCaption("Observación");
            grillaVerificaciondetalle.setSizeFull();
            grillaVerificaciondetalle.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    verificaciondetalleForm.setVisible(false);
                } else {
                    verificaciondetalleForm.setVerificaciondetalle(e.getValue());
                    verificaciondetalleForm.setViejo();
                }
            });
            
            verificaciondetalleForm.setGuardarListener(ro ->{
                grillaVerificaciondetalle.clearSortOrder();
                lista = verificaciondetalleDao.listaVerificaciondetalle();
                grillaVerificaciondetalle.setItems(lista);
            });
            verificaciondetalleForm.setBorrarListener(ro ->{
                grillaVerificaciondetalle.clearSortOrder();
            });
            verificaciondetalleForm.setCancelarListener(ro ->{
                grillaVerificaciondetalle.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateList(Long idverificacion) {
        try {
           lista = verificaciondetalleDao.getListVerificaciondetalle(idverificacion);
           grillaVerificaciondetalle.setSizeFull();
           grillaVerificaciondetalle.setItems(lista);
           grillaVerificaciondetalle.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
     
}
