/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.DocumentoDao;
import py.mutualmsp.mutualweb.entities.Documento;
import py.mutualmsp.mutualweb.formularios.DocumentoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class DocumentoVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Documento";
    Grid<Documento> grillaDocumento = new Grid<>(Documento.class);
    TextField txtfFiltro = new TextField("Filtro");
    DocumentoDao documentoDao = ResourceLocator.locate(DocumentoDao.class);
    List<Documento> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    DocumentoForm documentoForm = new DocumentoForm();
    
    public DocumentoVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        documentoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaDocumento.asSingleSelect().clear();
            documentoForm.setDocumento(new Documento());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaDocumento, documentoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaDocumento, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = documentoDao.listaDocumento();
            grillaDocumento.setItems(lista);
            grillaDocumento.removeAllColumns();
            grillaDocumento.addColumn(Documento::getId).setCaption("Id Documento");
            grillaDocumento.addColumn(Documento::getDescripcion).setCaption("Descripción");
            grillaDocumento.setSizeFull();
            grillaDocumento.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    documentoForm.setVisible(false);
                } else {
                    documentoForm.setDocumento(e.getValue());
                    documentoForm.setViejo();
                }
            });
            
            documentoForm.setGuardarListener(r ->{
                grillaDocumento.clearSortOrder();
                lista = documentoDao.listaDocumento();
                grillaDocumento.setItems(lista);
            });
            documentoForm.setBorrarListener(r ->{
                grillaDocumento.clearSortOrder();
            });
            documentoForm.setCancelarListener(r ->{
                grillaDocumento.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = documentoDao.getListDocumento(value);
           grillaDocumento.setSizeFull();
           grillaDocumento.setItems(lista);
           grillaDocumento.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
