/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperaciondetalleDao;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.entities.Tipooperaciondetalle;
import py.mutualmsp.mutualweb.formularios.TipooperaciondetalleForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class TipooperaciondetalleVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Modalidad según Operación";
    Grid<Tipooperaciondetalle> grillaTipooperaciondetalle = new Grid<>(Tipooperaciondetalle.class);
    TipooperaciondetalleDao tipooperaciondetalleDao = ResourceLocator.locate(TipooperaciondetalleDao.class);
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    List<Tipooperaciondetalle> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    TipooperaciondetalleForm tipooperaciondetalleForm = new TipooperaciondetalleForm();
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    
    public TipooperaciondetalleVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        tipooperaciondetalleForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(cmbTipooperacion ,btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(cmbTipooperacion, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        cmbTipooperacion.setItems(tipooperacionDao.listaTipooperacion());
        cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
        cmbTipooperacion.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroTipoOperacion(e.getValue().getIdtipooperacion());
                }else{
                    updateList("");
                }   
            } catch (Exception ex) {
                updateList("");
            }
        });
        
        btnNuevo.addClickListener(e ->{
            grillaTipooperaciondetalle.asSingleSelect().clear();
            tipooperaciondetalleForm.setRequisitooperacion(new Tipooperaciondetalle());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaTipooperaciondetalle, tipooperaciondetalleForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaTipooperaciondetalle, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = tipooperaciondetalleDao.listaTipooperaciondetalle();
            grillaTipooperaciondetalle.setItems(lista);
            grillaTipooperaciondetalle.removeAllColumns();
            grillaTipooperaciondetalle.addColumn(Tipooperaciondetalle::getIdtipooperaciondetalle).setCaption("Id Tipo operac. detalle");
            grillaTipooperaciondetalle.addColumn(tipoOperacion -> { 
                return  tipoOperacion.getIdtipooperacion().getDescripcion();}).setCaption("Tipo Operación");
            grillaTipooperaciondetalle.addColumn(modalidad -> {
                return modalidad.getIdmodalidad().getDescripcion();}).setCaption("Modalidad");
            grillaTipooperaciondetalle.setSizeFull();
            grillaTipooperaciondetalle.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    tipooperaciondetalleForm.setVisible(false);
                } else {
                    tipooperaciondetalleForm.setRequisitooperacion(e.getValue());
                    tipooperaciondetalleForm.setViejo();
                }
            });
            
            tipooperaciondetalleForm.setGuardarListener(tod ->{
                grillaTipooperaciondetalle.clearSortOrder();
                lista = tipooperaciondetalleDao.listaTipooperaciondetalle();
                grillaTipooperaciondetalle.setItems(lista);
            });
            tipooperaciondetalleForm.setBorrarListener(tod ->{
                grillaTipooperaciondetalle.clearSortOrder();
            });
            tipooperaciondetalleForm.setCancelarListener(tod ->{
                grillaTipooperaciondetalle.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = tipooperaciondetalleDao.getListTipooperaciondetalle(value);
           grillaTipooperaciondetalle.setSizeFull();
           grillaTipooperaciondetalle.setItems(lista);
           grillaTipooperaciondetalle.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }

    private void filtroTipoOperacion(Long idtipooperacion) {
        try {
            lista = tipooperaciondetalleDao.getTipooperaciondetalleByTipoOperacion(idtipooperacion);
            grillaTipooperaciondetalle.setItems(lista);
            grillaTipooperaciondetalle.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     
}
