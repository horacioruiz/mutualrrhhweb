/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.RequisitoDao;
import py.mutualmsp.mutualweb.entities.Requisito;
import py.mutualmsp.mutualweb.formularios.RequisitoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class RequisitoVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Requisitos";
    Grid<Requisito> grillaRequisito = new Grid<>(Requisito.class);
    TextField txtfFiltro = new TextField("Filtro");
    RequisitoDao requisitoDao = ResourceLocator.locate(RequisitoDao.class);
    List<Requisito> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    RequisitoForm requisitoForm = new RequisitoForm();
    
    public RequisitoVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        requisitoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaRequisito.asSingleSelect().clear();
            requisitoForm.setRequisito(new Requisito());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaRequisito, requisitoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaRequisito, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = requisitoDao.listaRequisito();
            grillaRequisito.setItems(lista);
            grillaRequisito.removeAllColumns();
            grillaRequisito.addColumn(Requisito::getIdrequisito).setCaption("Id Requisito");
            grillaRequisito.addColumn(Requisito::getDescripcion).setCaption("Descripción");
            grillaRequisito.addColumn(tipoRequisito -> {
                return tipoRequisito.getIdtiporequisito() == null ? "" : tipoRequisito.getIdtiporequisito().getDescripcion();}).setCaption("Tipo Operación");
            grillaRequisito.setSizeFull();
            grillaRequisito.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    requisitoForm.setVisible(false);
                } else {
                    requisitoForm.setRequisito(e.getValue());
                    requisitoForm.setViejo();
                }
            });
            
            requisitoForm.setGuardarListener(r ->{
                grillaRequisito.clearSortOrder();
                lista = requisitoDao.listaRequisito();
                grillaRequisito.setItems(lista);
            });
            requisitoForm.setBorrarListener(r ->{
                grillaRequisito.clearSortOrder();
            });
            requisitoForm.setCancelarListener(r ->{
                grillaRequisito.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = requisitoDao.getListRequisito(value);
           grillaRequisito.setSizeFull();
           grillaRequisito.setItems(lista);
           grillaRequisito.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
