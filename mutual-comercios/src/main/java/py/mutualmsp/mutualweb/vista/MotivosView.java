package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.formularios.MotivosForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 7/6/2016.
 */
public class MotivosView extends CssLayout implements View {

    public static final String VIEW_NAME = "Motivos de Solicitud";
    Grid<Motivos> grid = new Grid<>(Motivos.class);
    Button newUser = new Button("");
    TextField txtfFiltro = new TextField();
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    MotivosDao motivosController = ResourceLocator.locate(MotivosDao.class);
    ComboBox<Formulario> filter = new ComboBox<>();
    MotivosForm form = new MotivosForm();
    List<Motivos> lista = new ArrayList<>();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public MotivosView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            newUser.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            newUser.setIcon(VaadinIcons.PLUS_CIRCLE);

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponents(txtfFiltro, filter, newUser);
            horizontalLayout.setSpacing(true);
            horizontalLayout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
            horizontalLayout.setComponentAlignment(newUser, Alignment.MIDDLE_RIGHT);
            horizontalLayout.addStyleName("top-bar");

//            HorizontalLayout horizontalLayout = createTopBar();
            filter.setItems(controller.listaFormulario());
//            filter.setWidth(25f, ComboBox.UNITS_EM);
            filter.setPlaceholder("Búsqueda por formulario");
            filter.setItemCaptionGenerator(Formulario::getDescripcion);

            txtfFiltro.setPlaceholder("Búsqueda por descripción");
            txtfFiltro.setWidth(15f, TextField.UNITS_EM);
            txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
            txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

            lista = motivosController.listadeMotivos();
            form.setVisible(false);

            grid.setItems(lista);
            grid.removeAllColumns();
            grid.addColumn(Motivos::getId).setCaption("Codigo");
            grid.addColumn(Motivos::getDescripcion).setCaption("Descripción");
            grid.addComponentColumn(Motivos::getEstadoIcon).setCaption("Estado");
            grid.addColumn(e -> {
                return e.getFormulario() == null ? ""
                        : e.getFormulario().getDescripcion();
            }).setCaption("Formulario");
            //        grid.sort(initialSorting.getPropertyId(), initialSorting.getDirection());
            //        grid.lazyLoadFrom(safep, safep, safep.getPageSize());

            grid.setSizeFull();
            filter.addValueChangeListener(vcl -> {
                if (vcl.getValue() != null) {
                    grid.clearSortOrder();
                    updateCombo(vcl.getValue());
                }
            });

            grid.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        form.setVisible(false);
                    } else {
                        form.setCargo(e.getValue());
                        form.setViejo();
                    }
                }
            });

            HorizontalLayout hl = new HorizontalLayout();
            hl.addComponents(grid, form);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(hl);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

            newUser.addClickListener(e -> {
                grid.asSingleSelect().clear();
                form.setCargo(new Motivos());
            });
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (e.getValue() != null) {
                    form.edit(e.getValue());
                }
            });

            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                newUser.setEnabled(true);
            } else {
                newUser.setEnabled(false);
            }

            form.setSaveListener(usuarios -> {
                if (usuarios.getId() == null) {
                    //ejb.runInTransaction(entityManager -> entityManager.persist(usuarios));
                    grid.clearSortOrder();
                    lista = motivosController.listadeMotivos();
                    grid.setItems(lista);
                    Notification.show("Nuevo motivo creado", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    //ejb.runInTransaction(entityManager -> entityManager.merge(usuarios));
                    grid.clearSortOrder();
                }
            });

            form.setCancelListener(usuarios -> {
                grid.clearSortOrder();
                form.setVisible(false);
            });

            form.setDeleteListener(usuarios -> {
                lista = motivosController.listadeMotivos();
                grid.clearSortOrder();
                grid.setItems(lista);
                form.setVisible(false);
                //            grid.getContainerDataSource().removeItem(product);
                //            grid.refreshRows();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    private void updateList(String value) {
        try {
            lista = motivosController.listadeMotivos(0, 0, value.toUpperCase());
            grid.setSizeFull();
            grid.setItems(lista);
            grid.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout createTopBar() {
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setSpacing(true);
        topLayout.setWidth("100%");
        topLayout.addComponent(filter);
        topLayout.addComponent(newUser);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(newUser, Alignment.MIDDLE_RIGHT);
        topLayout.setExpandRatio(filter, 1);
        topLayout.addStyleName("top-bar");
        return topLayout;
    }

    private void updateCombo(Formulario value) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Motivos> listUsuarios = motivosController.listadeMotivosByFormulario(0, 50, (value.getDescripcion()).toUpperCase());
        grid.setItems(listUsuarios);
    }
}
