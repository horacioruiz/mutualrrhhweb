/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.DescuentoDao;
import py.mutualmsp.mutualweb.entities.Descuento;
import py.mutualmsp.mutualweb.formularios.DescuentoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class DescuentoVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Descuento";
    Grid<Descuento> grillaDescuento = new Grid<>(Descuento.class);
    TextField txtfFiltro = new TextField("Filtro");
    DescuentoDao descuentoDao = ResourceLocator.locate(DescuentoDao.class);
    List<Descuento> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    DescuentoForm descuentoForm = new DescuentoForm();
    
    public DescuentoVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        descuentoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaDescuento.asSingleSelect().clear();
            descuentoForm.setDescuento(new Descuento());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaDescuento, descuentoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaDescuento, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = descuentoDao.listaDescuento();
            grillaDescuento.setItems(lista);
            grillaDescuento.removeAllColumns();
            grillaDescuento.addColumn(Descuento::getId).setCaption("Id Descuento");
            grillaDescuento.addColumn(Descuento::getDescripcion).setCaption("Descripción");
            grillaDescuento.setSizeFull();
            grillaDescuento.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    descuentoForm.setVisible(false);
                } else {
                    descuentoForm.setDescuento(e.getValue());
                    descuentoForm.setViejo();
                }
            });
            
            descuentoForm.setGuardarListener(r ->{
                grillaDescuento.clearSortOrder();
                lista = descuentoDao.listaDescuento();
                grillaDescuento.setItems(lista);
            });
            descuentoForm.setBorrarListener(r ->{
                grillaDescuento.clearSortOrder();
            });
            descuentoForm.setCancelarListener(r ->{
                grillaDescuento.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = descuentoDao.getListDescuento(value);
           grillaDescuento.setSizeFull();
           grillaDescuento.setItems(lista);
           grillaDescuento.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
