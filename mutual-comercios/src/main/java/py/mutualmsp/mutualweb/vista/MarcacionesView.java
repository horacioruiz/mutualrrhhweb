package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dto.MarcacionesDTO;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.formularios.MotivosForm;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSaturday;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSunday;

/**
 * Created by Alfre on 7/6/2016.
 */
public class MarcacionesView extends CssLayout implements View {

    public static final String VIEW_NAME = "Importar Marcaciones";
    Grid<Marcaciones> grid = new Grid<>(Marcaciones.class);
//    Button newUser = new Button("Nuevo");
    TextField txtfFiltro = new TextField("Archivo");
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    TextField txtfFiltroIdReloj = new TextField();
    Marcaciones marcacionSeleccionado;
//    ComboBox<Formulario> filter = new ComboBox<>("Filtre por formulario");
    MotivosForm form = new MotivosForm();
    List<Marcaciones> lista = new ArrayList<>();
    ArrayList<MarcacionesDTO> listaMarcacionesDTO = new ArrayList<>();
    ArrayList<Marcaciones> listaMarcaciones = new ArrayList<>();
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);

    HorizontalLayout horizontalLayoutFilter = new HorizontalLayout();
    HorizontalLayout hl2 = new HorizontalLayout();
    HorizontalLayout hl3 = new HorizontalLayout();
    HorizontalLayout hl = new HorizontalLayout();
    Button btnCargarDatos = new Button("");
    Button btnImportar = new Button("");
    final Image image = new Image("Imagen");

    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    String patternHora = "HH:mm";
    SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);

    ProgressBar spinnerCargando = new ProgressBar();
    ProgressBar spinnerImportando = new ProgressBar();

    String fileName = "";
    String ubicacion = "";
    FileOutputStream fos = null;
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = new Upload("", receiver);
    String destinarariosString = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public MarcacionesView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            btnCargarDatos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnCargarDatos.setIcon(VaadinIcons.UPLOAD);
            btnCargarDatos.setEnabled(false);

            upload.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnImportar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnImportar.setIcon(VaadinIcons.UPLOAD_ALT);
            btnImportar.setVisible(false);

            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                upload.setEnabled(true);
            } else {
                upload.setEnabled(false);
            }

            image.setVisible(false);
            horizontalLayoutFilter.setVisible(false);

            spinnerCargando.setIndeterminate(true);
            spinnerCargando.setCaption("Cargando datos...");
            spinnerCargando.setWidth(8f, TextField.UNITS_EM);
            spinnerCargando.setHeight(8f, TextField.UNITS_EM);
            hl2.setVisible(false);

            spinnerImportando.setIndeterminate(true);
            spinnerImportando.setCaption("Importando datos...");
            spinnerImportando.setWidth(8f, TextField.UNITS_EM);
            spinnerImportando.setHeight(8f, TextField.UNITS_EM);
            hl3.setVisible(false);

            image.setWidth("65%");
            image.setHeight("45%");
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();
            image.setSource(new FileResource(new File(basepath
                    + "/WEB-INF/images/cargando.gif")));

            txtfFiltroIdReloj.setPlaceholder("Filtre por ID reloj");
            filterFuncionario.setPlaceholder("Filtre funcionario");
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponents(txtfFiltro, upload);
            horizontalLayout.setSpacing(true);
            horizontalLayout.setComponentAlignment(upload, Alignment.MIDDLE_RIGHT);

            horizontalLayout.addComponent(upload);
            horizontalLayout.setSpacing(true);
            horizontalLayout.addComponent(btnCargarDatos);
            horizontalLayout.setComponentAlignment(btnCargarDatos, Alignment.BOTTOM_CENTER);
            horizontalLayout.setSpacing(true);
            horizontalLayout.addComponent(btnImportar);
            horizontalLayout.setComponentAlignment(btnImportar, Alignment.BOTTOM_CENTER);

            horizontalLayoutFilter.addComponents(txtfFiltroIdReloj);
            horizontalLayoutFilter.addComponents(filterFuncionario);
            horizontalLayoutFilter.setSpacing(true);

            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                filterFuncionario.setItems(funcionarioController.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }

            txtfFiltroIdReloj.addValueChangeListener(e -> updateList(e.getValue()));

            horizontalLayout.addStyleName("top-bar");
            txtfFiltro.setPlaceholder("Archivo seleccionado");
            txtfFiltro.setWidth(15f, TextField.UNITS_EM);
            txtfFiltro.setEnabled(false);
            form.setVisible(false);

            grid.setItems(lista);
            grid.removeAllColumns();
            grid.setStyleGenerator(prop -> {
                String style = prop.getMinllegadatemprana() == 0 ? "green" : "red";
                return style;
            });
            grid.addColumn(e -> {
                return e.getHorarioFuncionario() == null ? "--" : e.getHorarioFuncionario().getIdreloj();
            }).setCaption("ID RELOJ");
            /*grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("ESTADO");+*/
            grid.addColumn(e -> {
                return e.getNombrefunc();
            }).setCaption("FUNCIONARIO");
            grid.addColumn(e -> {
                return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
            }).setCaption("FECHA");
            grid.addColumn(e -> {
                String hs = (e.getInmarcada() == null ? "" : simpleDateFormatHora.format(e.getInmarcada())) + " - " + (e.getOutmarcada() == null ? "" : simpleDateFormatHora.format(e.getOutmarcada()));
                return hs;
            }).setCaption("HS MARCACION");
            /*grid.addColumn(e -> {
                return e.getInmarcada() == null ? "--" : simpleDateFormatHora.format(e.getInmarcada());
            }).setCaption("ENTRADA");
            grid.addColumn(e -> {
                return e.getOutmarcada() == null ? "--" : simpleDateFormatHora.format(e.getOutmarcada());
            }).setCaption("SALIDA");*/
            grid.addColumn(e -> {
                return e.getMintrabajada() == null ? "--" : e.getMintrabajada();
            }).setCaption("MIN TRABJ");
//            grid.addColumn(e -> {
//                return e.getMinllegadatardia() == null ? "--" : e.getMinllegadatardia();
//            }).setCaption("MIN TARD");
//            grid.addColumn(e -> {
//                return e.getMinllegadatemprana() == null ? "--" : e.getMinllegadatemprana();
//            }).setCaption("MIN TEMP");
            grid.setSizeFull();

            btnCargarDatos.addClickListener(e -> {
                UI ui = UI.getCurrent();

                // Instruct client side to poll for changes and show spinner
                ui.setPollInterval(500);
                hl2.setVisible(true);
                hl.setVisible(false);
                horizontalLayoutFilter.setVisible(false);

                // Start background task
                CompletableFuture.runAsync(() -> {

                    // Do some long running task
                    //Thread.sleep(2000);
                    try {

                        leerCsv(file.getAbsolutePath());
                        btnImportar.setVisible(true);
                    } catch (FileNotFoundException ex) {
                        System.out.println("-->> " + ex.getLocalizedMessage());
                        System.out.println("-->> " + ex.fillInStackTrace());
                    } catch (IOException ex) {
                        System.out.println("-->> " + ex.getLocalizedMessage());
                        System.out.println("-->> " + ex.fillInStackTrace());
                    } catch (ParseException ex) {
                        System.out.println("-->> " + ex.getLocalizedMessage());
                        System.out.println("-->> " + ex.fillInStackTrace());
                    } finally {
                    }

                    // Need to use access() when running from background thread
                    ui.access(() -> {
                        // Stop polling and hide spinner
                        ui.setPollInterval(-1);
                        hl2.setVisible(false);
                        hl.setVisible(true);
                        horizontalLayoutFilter.setVisible(true);
                    });
                });
            });
            grid.asSingleSelect().addValueChangeListener(event -> {
                marcacionSeleccionado = event.getValue();
            });
            filterFuncionario.addValueChangeListener(vcl -> {
                txtfFiltroIdReloj.setValue("");
                if (vcl.getValue() == null || vcl.getValue().equals("")) {
                    try {
                        grid.clearSortOrder();
                        grid.setSizeFull();
                        grid.setItems(listaMarcaciones);
                        grid.clearSortOrder();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    List listaMarca = new ArrayList<>();
                    for (Marcaciones marcacion : listaMarcaciones) {
                        HorarioFuncionario hf = hfDao.listarPorIdReloj(marcacion.getHorarioFuncionario().getIdreloj());
                        HorarioFuncionario hf2 = hfDao.listarPorIdFuncionario(vcl.getValue().getIdfuncionario());
                        if (Objects.equals(hf.getIdreloj(), hf2.getIdreloj())) {
                            listaMarca.add(marcacion);
                        }
                    }
                    Comparator<Marcaciones> compareById = (Marcaciones o1, Marcaciones o2)
                            -> o1.getHorarioFuncionario().getIdreloj().compareTo(o2.getHorarioFuncionario().getIdreloj());
                    Collections.sort(listaMarca, compareById);
                    try {
                        grid.clearSortOrder();
                        grid.setSizeFull();
                        grid.setItems(listaMarca);
                        grid.clearSortOrder();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            /*btnImportar.addClickListener(e -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
                if (listFeriado.size() == 0) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    UI ui = UI.getCurrent();

                    // Instruct client side to poll for changes and show spinner
                    ui.setPollInterval(500);
                    hl3.setVisible(true);
                    hl.setVisible(false);
                    horizontalLayoutFilter.setVisible(false);

                    ArrayList<Marcaciones> listaMarcacionesHere = new ArrayList<>();
                    listaMarcacionesHere = listaMarcaciones;
                    if (!listaMarcacionesHere.isEmpty()) {
                        for (Marcaciones marcacion : listaMarcacionesHere) {
                            Marcaciones marcacionRegistrada = marcacionesController.getByIdRelojFecha(marcacion.getHorarioFuncionario(), marcacion.getFecha());
                            HorarioFuncionario hf = marcacion.getHorarioFuncionario();
                            if (marcacionRegistrada == null) {
                                List<Feriado> listFeri = feriadoDao.listarFeriadoPorPeriodo(marcacion.getFecha(), marcacion.getFecha());
                                if (listFeri.size() == 0) {
                                    marcacionesController.guardar(marcacion);
                                } else {
                                    List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(marcacion.getFecha(), marcacion.getFecha(), marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                                    if (listSuspencion.size() == 0) {
                                        marcacionesController.guardar(marcacion);
                                    }
                                }
                            } else {
                                List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(marcacion.getFecha(), marcacion.getFecha(), marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                                if (listSuspencion.size() == 0) {
                                    // Do Work Here
                                    List<Feriado> listFeri = feriadoDao.listarFeriadoPorPeriodo(marcacion.getFecha(), marcacion.getFecha());
                                    marcacion.setId(marcacionRegistrada.getId());
                                    try {
                                        marcacion.setSolicitud(marcacionRegistrada.getSolicitud());
                                    } catch (Exception ex) {
                                    } finally {
                                    }
                                    try {
                                        marcacion.setSolicitudProduccionDetalle(marcacionRegistrada.getSolicitudProduccionDetalle());
                                    } catch (Exception ex) {
                                    } finally {
                                    }
                                    boolean tieneTolerancia = false;
                                    if (listFeri.size() == 0 && (marcacionRegistrada.getInmarcada().getHours() + marcacionRegistrada.getInmarcada().getMinutes()) == 0) {
                                        marcacionRegistrada.setInmarcada(marcacion.getInmarcada());
                                        tieneTolerancia = true;
                                    }
                                    if (!isWeekendSunday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                        if (marcacionRegistrada.getInmarcada().before(marcacion.getInmarcada())) {
                                            marcacion.setInmarcada(marcacionRegistrada.getInmarcada());
                                        }
                                        if (marcacionRegistrada.getOutmarcada().after(marcacion.getOutmarcada())) {
                                            marcacion.setOutmarcada(marcacionRegistrada.getOutmarcada());
                                        }

                                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                            HorarioLaboral hl = hlDao.getByValue("sábado");
                                            hf.setHorarioLaboral(hl);
                                            marcacion.setInasignada(hl.getEntrada());
                                            marcacion.setOutasignada(hl.getSalida());
                                        }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                        System.out.println("MARCACION -->> " + marcacion.getHorarioFuncionario().getIdreloj() + " -- " + marcacion.getFecha() + " -- " + marcacion.getOutmarcada() + " -- " + hf.getHorarioLaboral().getEntrada().getTime());
                                        long minutesEntSal = 0;
                                        try {
                                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } catch (Exception ec) {
                                            marcacion.setOutmarcada(marcacion.getInmarcada());
                                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } finally {
                                        }

                                        marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                        if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            marcacion.setMinllegadatardia(0L);
                                            //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                        } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(0L);
                                            //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                            int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                            marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));

//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                            marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        }
                                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                            marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                        }

                                        if (listFeri.size() == 0) {
                                            if (tieneTolerancia) {
                                                String obs = marcacionRegistrada.getObservacion();
                                                marcacion.setObservacion(obs);
                                                marcacionesController.guardar(marcacion);
                                            } else {
                                                marcacionesController.guardar(marcacion);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        try {
                            grid.clearSortOrder();
                        } catch (Exception exp) {
                        } finally {
                        }
                        listaMarcaciones = new ArrayList<>();
                        grid.setItems(listaMarcaciones);
                        
                        txtfFiltro.setValue("");
                        btnCargarDatos.setEnabled(false);
                        btnImportar.setVisible(false);
                        horizontalLayoutFilter.setVisible(false);
                        hl3.setVisible(false);
                        hl.setVisible(true);
                        Notification.show("Datos importados exitosamente", Notification.Type.WARNING_MESSAGE);
                    } else {
                        Notification.show("Sin detalles cargados", Notification.Type.ERROR_MESSAGE);
                    }

                    // Start background task
                    
                }
            });*/
            btnImportar.addClickListener(e -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
                if (listFeriado.size() == 0) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    UI ui = UI.getCurrent();

                    // Instruct client side to poll for changes and show spinner
                    ui.setPollInterval(500);
                    hl3.setVisible(true);
                    hl.setVisible(false);
                    horizontalLayoutFilter.setVisible(false);

                    // Start background task
                    CompletableFuture.runAsync(() -> {
                        Thread t = new Thread(r);
                        t.start();
                        // Need to use access() when running from background thread
                        ui.access(() -> {
                            // Stop polling and hide spinner
                            ui.setPollInterval(-1);
                            hl3.setVisible(false);
                            hl.setVisible(true);
                            horizontalLayoutFilter.setVisible(false);

                            grid.setItems(new ArrayList<>());
                            txtfFiltro.setValue("");
                            btnCargarDatos.setEnabled(false);
                            btnImportar.setVisible(false);
                            horizontalLayoutFilter.setVisible(false);
                            hl3.setVisible(false);
                            hl.setVisible(true);
                        });
                    });
                }
            });
            hl2.addComponent(spinnerCargando);
            hl2.setComponentAlignment(spinnerCargando, Alignment.MIDDLE_CENTER);

            hl3.addComponent(spinnerImportando);
            hl3.setComponentAlignment(spinnerImportando, Alignment.MIDDLE_CENTER);

            hl.addComponents(grid, form);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(horizontalLayoutFilter);
            barAndGridLayout.addComponent(hl3);
            barAndGridLayout.addComponent(hl2);
            barAndGridLayout.addComponent(hl);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Runnable r = new Runnable() {
        public void run() {
            ArrayList<Marcaciones> listaMarcacionesHere = new ArrayList<>();
            listaMarcacionesHere = listaMarcaciones;
            if (!listaMarcacionesHere.isEmpty()) {
                for (Marcaciones marcacion : listaMarcacionesHere) {
                    Marcaciones marcacionRegistrada = marcacionesController.getByIdRelojFecha(marcacion.getHorarioFuncionario(), marcacion.getFecha());
                    HorarioFuncionario hf = marcacion.getHorarioFuncionario();
                    if (marcacionRegistrada == null) {
                        List<Feriado> listFeri = feriadoDao.listarFeriadoPorPeriodo(marcacion.getFecha(), marcacion.getFecha());
                        if (listFeri.size() == 0) {
                            marcacionesController.guardar(marcacion);
                        } else {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(marcacion.getFecha(), marcacion.getFecha(), marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                            if (listSuspencion.size() == 0) {
                                marcacionesController.guardar(marcacion);
                            }
                        }
                    } else {
                        List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(marcacion.getFecha(), marcacion.getFecha(), marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                        if (listSuspencion.size() == 0) {
                            // Do Work Here
                            List<Feriado> listFeri = feriadoDao.listarFeriadoPorPeriodo(marcacion.getFecha(), marcacion.getFecha());
                            marcacion.setId(marcacionRegistrada.getId());
                            try {
                                marcacion.setSolicitud(marcacionRegistrada.getSolicitud());
                            } catch (Exception ex) {
                            } finally {
                            }
                            try {
                                marcacion.setSolicitudProduccionDetalle(marcacionRegistrada.getSolicitudProduccionDetalle());
                            } catch (Exception ex) {
                            } finally {
                            }
                            boolean tieneTolerancia = false;
                            if (listFeri.size() == 0 && (marcacionRegistrada.getInmarcada().getHours() + marcacionRegistrada.getInmarcada().getMinutes()) == 0) {
                                marcacionRegistrada.setInmarcada(marcacion.getInmarcada());
                                tieneTolerancia = true;
                            }
                            if (!isWeekendSunday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                if (marcacionRegistrada.getInmarcada().before(marcacion.getInmarcada())) {
                                    marcacion.setInmarcada(marcacionRegistrada.getInmarcada());
                                }
                                if (marcacionRegistrada.getOutmarcada().after(marcacion.getOutmarcada())) {
                                    marcacion.setOutmarcada(marcacionRegistrada.getOutmarcada());
                                }

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sábado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setInasignada(hl.getEntrada());
                                    marcacion.setOutasignada(hl.getSalida());
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                System.out.println("MARCACION -->> " + marcacion.getHorarioFuncionario().getIdreloj() + " -- " + marcacion.getFecha() + " -- " + marcacion.getOutmarcada() + " -- " + hf.getHorarioLaboral().getEntrada().getTime());
                                long minutesEntSal = 0;
                                try {
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } finally {
                                }

                                marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));

//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }

                                if (listFeri.size() == 0) {
                                    if (tieneTolerancia) {
                                        String obs = marcacionRegistrada.getObservacion();
                                        marcacion.setObservacion(obs);
                                        marcacionesController.guardar(marcacion);
                                    } else {
                                        marcacionesController.guardar(marcacion);
                                    }
                                }
                            }
                        }
                    }
                }
                try {
                    grid.clearSortOrder();
                } catch (Exception exp) {
                } finally {
                }
                listaMarcaciones = new ArrayList<>();
                grid.setItems(listaMarcaciones);
                txtfFiltro.setValue("");
                btnCargarDatos.setEnabled(false);
                btnImportar.setVisible(false);
                horizontalLayoutFilter.setVisible(false);
                hl3.setVisible(false);
                hl.setVisible(true);
                Notification.show("Datos importados exitosamente", Notification.Type.WARNING_MESSAGE);
            } else {
                Notification.show("Sin detalles cargados", Notification.Type.ERROR_MESSAGE);
            }
        }
    };

    private void leerExcel(String absolutePath) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(absolutePath);
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                XSSFRow row = ((XSSFRow) rows.next());
                Iterator cells = row.cellIterator();
                int i = 0;
                MarcacionesDTO marcDTO = new MarcacionesDTO();
                while (cells.hasNext()) {
                    XSSFCell cell = (XSSFCell) cells.next();
                    String result = cell.getStringCellValue();
                    switch (i) {
                        case 0:
                            try {
                                marcDTO.setId(Long.parseLong(result));
                            } catch (Exception e) {
                            } finally {
                            }
                            break;
                        case 1:
                            marcDTO.setNombreApellido(result);
                            break;
                        case 2:
                            String[] parts = result.split(" ");
                            SimpleDateFormat sdfFecha = new SimpleDateFormat("dd-M-yyyy");
                            SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");
                            String dateInString = parts[0];
                            String timeInString = parts[1];

                            Date parsedHora = sdfHora.parse(timeInString);

                            marcDTO.setFecha(sdfFecha.parse(dateInString));
                            marcDTO.setHora(new java.sql.Timestamp(parsedHora.getTime()));
                            break;
                        case 3:
                            marcDTO.setEntrada(result);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
                listaMarcacionesDTO.add(marcDTO);
            }
            for (MarcacionesDTO row : listaMarcacionesDTO) {
                if (row.getEntrada().toUpperCase().equalsIgnoreCase("IN")) {

                } else {

                }
                System.out.println("-> ID_RELOJ: " + row.getId() + " NOMBRE: " + row.getNombreApellido() + " FECHA: " + row.getFecha() + " HORA: " + row.getHora() + " ESTADO: " + row.getEntrada());
            }
//            grillaCargo.clearSortOrder();
//            grillaCargo.setItems(lista);
//            grillaCargo.removeAllColumns();
//            grillaCargo.addColumn(UsuarioMarcacionDTO::getId).setCaption("ID RELOJ");
//            grillaCargo.addColumn(UsuarioMarcacionDTO::getNombreApellido).setCaption("FUNCIONARIO");
//            grillaCargo.setSizeFull();
        } catch (IOException e) {
            System.out.println("1) " + e.fillInStackTrace());
            e.printStackTrace();
        } catch (ParseException ex) {
            Logger.getLogger(MarcacionesView.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

    private void leerCsv(String absolutePath) throws IOException, ParseException {
        File f1 = new File(absolutePath);// OUTFILE
        FileReader fR1 = new FileReader(f1);
        BufferedReader reader1 = new BufferedReader(fR1);
        HashMap<String, MarcacionesDTO> mapeo = new HashMap();
        listaMarcacionesDTO = new ArrayList<>();
        while (reader1.ready()) {
            try {
                String linea = reader1.readLine();

                StringTokenizer st = new StringTokenizer(linea, ",");
                MarcacionesDTO marcDTO = new MarcacionesDTO();
                marcDTO.setId(Long.parseLong(st.nextElement().toString()));
                marcDTO.setNombreApellido(st.nextElement().toString());
                String[] parts = st.nextElement().toString().split(" ");
                SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");
                String dateInString = parts[0];
                String timeInString = parts[1];

                Date parsedHora = sdfHora.parse(timeInString);

                marcDTO.setFecha(sdfFecha.parse(dateInString));
                marcDTO.setHora(new java.sql.Timestamp(parsedHora.getTime()));
                marcDTO.setEntrada(st.nextElement().toString());

                if (!mapeo.containsKey(marcDTO.getId() + "-" + marcDTO.getFecha())) {
                    mapeo.put(marcDTO.getId() + "-" + marcDTO.getFecha(), marcDTO);
                    listaMarcacionesDTO.add(marcDTO);
                } else {
                    MarcacionesDTO marcRegistradaDTO = mapeo.get(marcDTO.getId() + "-" + marcDTO.getFecha());
                    Timestamp timeNow = marcDTO.getHora();

                    if (marcRegistradaDTO.getHora().before(marcDTO.getHora())) {
                        marcDTO.setHora(marcRegistradaDTO.getHora());
                    } else {
                        marcDTO.setHora(marcDTO.getHora());
                    }
                    try {
                        if (marcRegistradaDTO.getHoraSalida().before(timeNow)) {
                            marcDTO.setHoraSalida(timeNow);
                        } else {
                            marcDTO.setHoraSalida(marcRegistradaDTO.getHoraSalida());
                        }
                    } catch (Exception e) {
                        marcDTO.setHoraSalida(timeNow);
                    } finally {
                    }

                    mapeo.put(marcDTO.getId() + "-" + marcDTO.getFecha(), marcDTO);
                    listaMarcacionesDTO.remove(marcRegistradaDTO);
                    listaMarcacionesDTO.add(marcDTO);
                }
//                if (!mapeo.containsKey(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha()) && !marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("BREAK")) {
//                    mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                    listaMarcacionesDTO.add(marcDTO);
//                } else if (!marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("BREAK")) {
//                    MarcacionesDTO marcRegistradaDTO = mapeo.get(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha());
//                    if (marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("IN")) {
//                        if (marcRegistradaDTO.getHora().after(marcDTO.getHora())) {
//                            mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                            listaMarcacionesDTO.remove(marcRegistradaDTO);
//                            listaMarcacionesDTO.add(marcDTO);
//                        }
//                    } else {
//                        if (marcRegistradaDTO.getHora().before(marcDTO.getHora())) {
//                            mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                            listaMarcacionesDTO.remove(marcRegistradaDTO);
//                            listaMarcacionesDTO.add(marcDTO);
//                        }
//                    }
//                }
            } catch (Exception e) {
            } finally {
            }
        }
        int i = 0;
        Comparator<MarcacionesDTO> compareById = (MarcacionesDTO o1, MarcacionesDTO o2)
                -> o1.getId().compareTo(o2.getId());
        Collections.sort(listaMarcacionesDTO, compareById);
        listaMarcaciones = new ArrayList<>();
        for (MarcacionesDTO row : listaMarcacionesDTO) {
            try {
                Marcaciones marcacion = new Marcaciones();
                HorarioFuncionario hf = hfDao.getById(row.getId());
                marcacion.setHorarioFuncionario(hf);
                i++;

                marcacion.setFecha(row.getFecha());
                marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                marcacion.setInmarcada(row.getHora());
                marcacion.setOutmarcada(row.getHoraSalida());
//            marcacion.setNombrefunc(row.getNombreApellido());
                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                if (isWeekend(DateUtils.asLocalDate(marcacion.getFecha()))) {
                    HorarioLaboral hl = hlDao.getByValue("sábado");
                    hf.setHorarioLaboral(hl);
                    marcacion.setInasignada(hl.getEntrada());
                    marcacion.setOutasignada(hl.getSalida());
                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                System.out.println("MARCACION -->> " + marcacion.getHorarioFuncionario().getIdreloj() + " -- " + marcacion.getFecha() + " -- " + marcacion.getOutmarcada() + " -- " + hf.getHorarioLaboral().getEntrada().getTime());
                long minutesEntSal = 0;
                try {
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } catch (Exception e) {
                    marcacion.setOutmarcada(marcacion.getInmarcada());
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } finally {
                }

                marcacion.setMintrabajada(minutesEntSal - 40);
//            if (marcacion.getInmarcada().before(marcacion.getInasignada())) {
//                marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
//                marcacion.setMinllegadatardia(0L);
//            } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
//                marcacion.setMinllegadatemprana(0L);
//                marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
////                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
//                minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
//                marcacion.setMintrabajada(minutesEntSal - 40);
//            } else {
//                marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
//                marcacion.setMinllegadatardia(0L);
//            }
                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    marcacion.setMinllegadatardia(0L);
                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(0L);
                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                }
                if (isWeekend(DateUtils.asLocalDate(marcacion.getFecha()))) {
                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                }
                listaMarcaciones.add(marcacion);
                System.out.println(i + ") -> ID_RELOJ: " + row.getId() + " NOMBRE: " + row.getNombreApellido() + " FECHA: " + row.getFecha() + " HORA IN: " + row.getHora() + " HORA OUT: " + row.getHoraSalida() + " ESTADO: " + row.getEntrada());
            } catch (Exception e) {
            } finally {
            }
        }
        try {
            grid.clearSortOrder();
        } catch (Exception e) {
        } finally {
        }
        grid.setItems(listaMarcaciones);
        //grid.removeAllColumns();

        //grid.setSizeFull();
        hl2.setVisible(false);
        hl.setVisible(true);
    }
//    private void leerCsv(String absolutePath) throws IOException, ParseException {
//        File f1 = new File(absolutePath);// OUTFILE
//        FileReader fR1 = new FileReader(f1);
//        BufferedReader reader1 = new BufferedReader(fR1);
//        HashMap<String, MarcacionesDTO> mapeo = new HashMap();
//        while (reader1.ready()) {
//            try {
//                String linea = reader1.readLine();
//
//                StringTokenizer st = new StringTokenizer(linea, ",");
//                MarcacionesDTO marcDTO = new MarcacionesDTO();
//                marcDTO.setId(Long.parseLong(st.nextElement().toString()));
//                marcDTO.setNombreApellido(st.nextElement().toString());
//                String[] parts = st.nextElement().toString().split(" ");
//                SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyy-MM-dd");
//                SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");
//                String dateInString = parts[0];
//                String timeInString = parts[1];
//
//                Date parsedHora = sdfHora.parse(timeInString);
//
//                marcDTO.setFecha(sdfFecha.parse(dateInString));
//                marcDTO.setHora(new java.sql.Timestamp(parsedHora.getTime()));
//                marcDTO.setEntrada(st.nextElement().toString());
////                listaMarcacionesDTO.add(marcDTO);
//
//                if (!mapeo.containsKey(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha()) && !marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("BREAK")) {
//                    mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                    listaMarcacionesDTO.add(marcDTO);
//                } else if (!marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("BREAK")) {
//                    MarcacionesDTO marcRegistradaDTO = mapeo.get(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha());
//                    if (marcDTO.getEntrada().toUpperCase().trim().equalsIgnoreCase("IN")) {
//                        if (marcRegistradaDTO.getHora().after(marcDTO.getHora())) {
//                            mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                            listaMarcacionesDTO.remove(marcRegistradaDTO);
//                            listaMarcacionesDTO.add(marcDTO);
//                        }
//                    } else {
//                        if (marcRegistradaDTO.getHora().before(marcDTO.getHora())) {
//                            mapeo.put(marcDTO.getId() + "-" + marcDTO.getEntrada().toUpperCase().trim() + "-" + marcDTO.getFecha(), marcDTO);
//                            listaMarcacionesDTO.remove(marcRegistradaDTO);
//                            listaMarcacionesDTO.add(marcDTO);
//                        }
//                    }
//                }
//            } catch (Exception e) {
//            } finally {
//            }
//        }
//        int i = 0;
//        Comparator<MarcacionesDTO> compareById = (MarcacionesDTO o1, MarcacionesDTO o2)
//                -> o1.getId().compareTo(o2.getId());
//        Collections.sort(listaMarcacionesDTO, compareById);
//        HashMap<String, Marcaciones> mapeoMarcaciones = new HashMap<String, Marcaciones>();
//        for (MarcacionesDTO row : listaMarcacionesDTO) {
//            Marcaciones marcacion = new Marcaciones()
//            
//            
//            
//            System.out.println("-> ID_RELOJ: " + row.getId() + " NOMBRE: " + row.getNombreApellido() + " FECHA: " + row.getFecha() + " HORA: " + row.getHora() + " ESTADO: " + row.getEntrada());
//        }
//    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void updateList(String value) {
        filterFuncionario.setValue(null);
        if (value.equals("")) {
            try {
                grid.clearSortOrder();
                grid.setSizeFull();
                grid.setItems(listaMarcaciones);
                grid.clearSortOrder();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            List listaMarca = new ArrayList<>();
            for (Marcaciones marcacion : listaMarcaciones) {
                if (marcacion.getHorarioFuncionario().getIdreloj() == Long.parseLong(value)) {
                    listaMarca.add(marcacion);
                }
            }
            Comparator<Marcaciones> compareById = (Marcaciones o1, Marcaciones o2)
                    -> o1.getHorarioFuncionario().getIdreloj().compareTo(o2.getHorarioFuncionario().getIdreloj());
            Collections.sort(listaMarca, compareById);
            try {
                grid.clearSortOrder();
                grid.setSizeFull();
                grid.setItems(listaMarca);
                grid.clearSortOrder();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {
            try {
//                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-archivos/" + filename);
//                url = Constants.PUBLIC_SERVER_URL + "/helpdesk/" + filename;
//                fileName = filename;
//                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-archivos/";
                fos = new FileOutputStream(file);
                btnCargarDatos.setEnabled(true);
                txtfFiltro.setValue(file.getName());
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
            return fos; // Return the output stream to write to
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent event) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
