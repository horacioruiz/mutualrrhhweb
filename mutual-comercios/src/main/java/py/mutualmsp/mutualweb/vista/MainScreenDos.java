/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class MainScreenDos extends HorizontalLayout {

    private static Menu menu;

    public MainScreenDos() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();

        setStyleName("main-screen");

        CssLayout viewContainer = new CssLayout();
        viewContainer.addStyleName("valo-content");
        viewContainer.setSizeFull();

        Navigator navigator = new Navigator(UI.getCurrent(), viewContainer);
        navigator.setErrorView(ErrorView.class);
        menu = new Menu(navigator);

        FileResource resourceFuncionario = new FileResource(new File(basepath
                + "/WEB-INF/images/funcionario.png"));
        FileResource resourceLicencia = new FileResource(new File(basepath
                + "/WEB-INF/images/licencias.png"));
        FileResource resourceSuspencion = new FileResource(new File(basepath
                + "/WEB-INF/images/suspenciones.png"));
        FileResource resourceVacaciones = new FileResource(new File(basepath
                + "/WEB-INF/images/vacaciones.png"));
        /*menu.addView(AboutView.class, AboutView.VIEW_NAME, AboutView.VIEW_NAME, VaadinIcons.CHECK);
        menu.addView(RequisitoVista.class, RequisitoVista.VIEW_NAME, RequisitoVista.VIEW_NAME, VaadinIcons.BULLETS);
        menu.addView(ModalidadVista.class, ModalidadVista.VIEW_NAME, ModalidadVista.VIEW_NAME, VaadinIcons.BULLETS);
        menu.addView(TipooperacionVista.class, TipooperacionVista.VIEW_NAME, TipooperacionVista.VIEW_NAME, VaadinIcons.BULLETS);
        menu.addView(TipooperaciondetalleVista.class, TipooperaciondetalleVista.VIEW_NAME, TipooperaciondetalleVista.VIEW_NAME, VaadinIcons.BULLETS);
        
        menu.addView(RequisitooperacionVista.class, RequisitooperacionVista.VIEW_NAME, RequisitooperacionVista.VIEW_NAME, VaadinIcons.BULLETS);
        menu.addView(VerificacionVista.class, VerificacionVista.VIEW_NAME, VerificacionVista.VIEW_NAME, VaadinIcons.BULLETS);*/
//        menu.add
//        )
//        menu.addView(FuncionarioVista.class, FuncionarioVista.VIEW_NAME, FuncionarioVista.VIEW_NAME, VaadinIcons.USER);

        /*menu.addView(UserViews.class, UserViews.VIEW_NAME, UserViews.VIEW_NAME, VaadinIcons.USER);
        menu.addView(CiudadView.class, CiudadView.VIEW_NAME, CiudadView.VIEW_NAME, VaadinIcons.MAP_MARKER);
        menu.addView(CategoriaView.class, CategoriaView.VIEW_NAME, CategoriaView.VIEW_NAME, VaadinIcons.LEVEL_UP);
        menu.addView(RutaView.class, RutaView.VIEW_NAME, RutaView.VIEW_NAME, VaadinIcons.ARROWS);
        menu.addView(FrequenciaView.class, FrequenciaView.VIEW_NAME, FrequenciaView.VIEW_NAME, VaadinIcons.CALENDAR);
        menu.addView(ClienteView.class, ClienteView.VIEW_NAME, ClienteView.VIEW_NAME, VaadinIcons.USERS);
        menu.addView(GeoZonaView.class, GeoZonaView.VIEW_NAME, GeoZonaView.VIEW_NAME, VaadinIcons.USER_CHECK);
        menu.addView(MarcacionView.class, MarcacionView.VIEW_NAME, MarcacionView.VIEW_NAME, VaadinIcons.ALARM);
        menu.addView(AsignarGeoZonaView.class, AsignarGeoZonaView.VIEW_NAME, AsignarGeoZonaView.VIEW_NAME, VaadinIcons.CHART_TIMELINE);
        menu.addView(VisitasView.class, VisitasView.VIEW_NAME, VisitasView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
        menu.addView(VisitasResumenView.class, VisitasResumenView.VIEW_NAME, VisitasResumenView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
        menu.addView(ResumenMarcacionView.class, ResumenMarcacionView.VIEW_NAME, ResumenMarcacionView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
        menu.addView(ResumenImpulsadoresView.class, ResumenImpulsadoresView.VIEW_NAME, ResumenImpulsadoresView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
        menu.addView(ResumenSupervisoresView.class, ResumenSupervisoresView.VIEW_NAME, ResumenSupervisoresView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
​        */
        addComponent(menu);
        addComponent(viewContainer);
        setExpandRatio(viewContainer, 1);
        setSizeFull();

        // notify the view menu about view changes so that it can display which view is currently active
        navigator.addViewChangeListener(new ViewChangeListener() {
            public boolean beforeViewChange(ViewChangeEvent event) {
                return true;
            }

            public void afterViewChange(ViewChangeEvent event) {
                menu.styleMenuItemOfActiveView(event.getViewName());
            }
        });
    }

    private static class Menu extends CssLayout {

        private static final String VALO_MENUITEMS = "valo-menuitems";
        private static final String VALO_MENU_TOGGLE = "valo-menu-toggle";
        private static final String VALO_MENU_VISIBLE = "valo-menu-visible";
        private Navigator navigator;
        private Map<String, Button> viewButtons = new HashMap<>();

        private CssLayout menuItemsLayout;
        private CssLayout menuPart;

        public Menu(Navigator navigator) {
            this.navigator = navigator;
            setPrimaryStyleName(ValoTheme.MENU_ROOT);
            menuPart = new CssLayout();
            menuPart.addStyleName(ValoTheme.MENU_PART);

            // header of the menu
            final HorizontalLayout top = new HorizontalLayout();
            top.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
            top.addStyleName(ValoTheme.MENU_TITLE);
            top.setSpacing(true);
            Label title = new Label("Mutual Comercios");
            title.addStyleName(ValoTheme.LABEL_H3);
            title.setSizeUndefined();
            //Image image = new Image(null, new ThemeResource("img/table-logo.png"));
            //image.setStyleName("logo");
            //top.addComponent(image);
            top.addComponent(title);
            menuPart.addComponent(top);

            // Cerrar Sesión menu item
            MenuBar CerrarSesionMenu = new MenuBar();
            CerrarSesionMenu.addItem("Cerrar Sesión", FontAwesome.SIGN_OUT, i -> {
                for (UI ui : VaadinSession.getCurrent().getUIs()) {
                    ui.access(() -> ui.getPage().setLocation("/")); // FIXME, has to be the contextPath of the app server
                }
                getSession().close();
                Page.getCurrent().reload();
            });

            CerrarSesionMenu.addStyleName("user-menu");
            menuPart.addComponent(CerrarSesionMenu);

            // SEARCHER of the menu
            final HorizontalLayout topSearch = new HorizontalLayout();
            topSearch.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            topSearch.addStyleName(ValoTheme.MENU_TITLE);
            TextField txtField = new TextField("Buscar menú");
            txtField.setPlaceholder("Buscar menú...");

            txtField.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    restablecer();

                    menu.addView(FuncionarioVista.class, FuncionarioVista.VIEW_NAME, FuncionarioVista.VIEW_NAME, VaadinIcons.USER);
                    //menu.addView(CargoVista.class, CargoVista.VIEW_NAME, CargoVista.VIEW_NAME, VaadinIcons.BULLETS);
                    //menu.addView(FormularioVista.class, FormularioVista.VIEW_NAME, FormularioVista.VIEW_NAME, VaadinIcons.BULLETS);
                    menu.addView(MotivosView.class, MotivosView.VIEW_NAME, MotivosView.VIEW_NAME, VaadinIcons.CLIPBOARD);
                    menu.addView(FeriadoVista.class, FeriadoVista.VIEW_NAME, FeriadoVista.VIEW_NAME, VaadinIcons.CALENDAR);
                    menu.addView(VacacionesView.class, VacacionesView.VIEW_NAME, VacacionesView.VIEW_NAME, VaadinIcons.CALENDAR_CLOCK);
                    menu.addView(SolicitudView.class, SolicitudView.VIEW_NAME, SolicitudView.VIEW_NAME, VaadinIcons.CLIPBOARD_CHECK);
                    menu.addView(LicenciasView.class, LicenciasView.VIEW_NAME, LicenciasView.VIEW_NAME, VaadinIcons.CLIPBOARD_TEXT);
                    menu.addView(CompensacionesView.class, CompensacionesView.VIEW_NAME, CompensacionesView.VIEW_NAME, VaadinIcons.BULLETS);
                    menu.addView(ToleranciasVista.class, ToleranciasVista.VIEW_NAME, ToleranciasVista.VIEW_NAME, VaadinIcons.HANDSHAKE);
                    menu.addView(SuspencionesVista.class, SuspencionesVista.VIEW_NAME, SuspencionesVista.VIEW_NAME, VaadinIcons.BAN);
                    menu.addView(RotacionesVista.class, RotacionesVista.VIEW_NAME, RotacionesVista.VIEW_NAME, VaadinIcons.ROTATE_RIGHT);
                    //menu.addView(ImportarUsuarios.class, ImportarUsuarios.VIEW_NAME, ImportarUsuarios.VIEW_NAME, VaadinIcons.BULLETS);
                    menu.addView(HorarioLaboralVista.class, HorarioLaboralVista.VIEW_NAME, HorarioLaboralVista.VIEW_NAME, VaadinIcons.TIMER);
                    menu.addView(MarcacionesView.class, MarcacionesView.VIEW_NAME, MarcacionesView.VIEW_NAME, VaadinIcons.UPLOAD);
                    menu.addView(ConsultaMarcacionesView.class, ConsultaMarcacionesView.VIEW_NAME, ConsultaMarcacionesView.VIEW_NAME, VaadinIcons.TIME_FORWARD);
                    menu.addView(HoraExtrasView.class, HoraExtrasView.VIEW_NAME, HoraExtrasView.VIEW_NAME, VaadinIcons.TIME_BACKWARD);

//                    menuPart.addComponent(menu);
//                    setSizeFull();
                }

            });

            topSearch.addComponent(txtField);
            menuPart.addComponent(topSearch);

            // button for toggling the visibility of the menu when on a small screen
            final Button showMenu = new Button("Menu", cl -> {
                if (menuPart.getStyleName().contains(VALO_MENU_VISIBLE)) {
                    menuPart.removeStyleName(VALO_MENU_VISIBLE);
                } else {
                    menuPart.addStyleName(VALO_MENU_VISIBLE);
                }
            });

            showMenu.addStyleName(ValoTheme.BUTTON_PRIMARY);
            showMenu.addStyleName(ValoTheme.BUTTON_SMALL);
            showMenu.addStyleName(VALO_MENU_TOGGLE);
            showMenu.setIcon(VaadinIcons.MENU);
            menuPart.addComponent(showMenu);

            // container for the navigation buttons, which are added by addView()
            menuItemsLayout = new CssLayout();
            menuItemsLayout.setPrimaryStyleName(VALO_MENUITEMS);
            menuPart.addComponent(menuItemsLayout);

            final HorizontalLayout topHead = new HorizontalLayout();
            topHead.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
            topHead.addStyleName(ValoTheme.MENU_TITLE);
            topHead.setSpacing(true);
            Label titleHere = new Label("Versión 1.0");
            titleHere.setSizeUndefined();
            //Image image = new Image(null, new ThemeResource("img/table-logo.png"));
            //image.setStyleName("logo");
            //top.addComponent(image);
            topHead.addComponent(titleHere);
            menuPart.addComponent(topHead);

            addComponent(menuPart);
        }

        public void addView(Class<? extends View> viewClass, final String name, String caption, Resource icon) {
            ///if (UserHolder.viewAccesibleToUser(name)) {
            createViewButton(name, caption, icon);
            navigator.addView(name, viewClass);
            ///}
        }

        private void restablecer() {
            menu.viewButtons.clear();
            menu.viewButtons = new HashMap<>();

            CssLayout viewContainer = new CssLayout();
            viewContainer.addStyleName("valo-content");
            viewContainer.setSizeFull();

            navigator = new Navigator(UI.getCurrent(), viewContainer);
//            menuItemsLayout = new CssLayout();
//            CssLayout viewContainer = new CssLayout();
//            viewContainer.addStyleName("valo-content");
//            viewContainer.setSizeFull();
//
//            navigator = new Navigator(UI.getCurrent(), viewContainer);
//            navigator.setErrorView(ErrorView.class);
//            menu = new Menu(navigator);
//            // notify the view menu about view changes so that it can display which view is currently active
//            navigator.addViewChangeListener(new ViewChangeListener() {
//                public boolean beforeViewChange(ViewChangeListener.ViewChangeEvent event) {
//                    return true;
//                }
//
//                public void afterViewChange(ViewChangeListener.ViewChangeEvent event) {
//                    menu.styleMenuItemOfActiveView(event.getViewName());
//                }
//            });
        }

        private void createViewButton(final String name, String caption, Resource icon) {
            Button button = new Button(caption, cl -> navigator.navigateTo(name));
            button.setPrimaryStyleName(ValoTheme.MENU_ITEM);
            button.setIcon(icon);
            menuItemsLayout.addComponent(button);
            viewButtons.put(name, button);
        }

        public void styleMenuItemOfActiveView(String viewName) {
            for (Button button : viewButtons.values()) {
                button.removeStyleName("selected");
            }

            Button selected = viewButtons.get(viewName);
            if (selected != null) {
                selected.addStyleName("selected");
            }

            menuPart.removeStyleName(VALO_MENU_VISIBLE);
        }
    }
}
