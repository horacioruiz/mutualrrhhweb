/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.formularios.CargoForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class CargoVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Cargo";
    Grid<Cargo> grillaCargo = new Grid<>(Cargo.class);
    TextField txtfFiltro = new TextField("Filtro");
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    List<Cargo> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    CargoForm cargoForm = new CargoForm();
    
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public CargoVista() {
        setSizeFull();
        addStyleName("crud-view");

        Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
        List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
        List<Funcionario> listFunc = new ArrayList<>();
        listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
        }
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        cargoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaCargo.asSingleSelect().clear();
            cargoForm.setCargo(new Cargo());
        });

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaCargo, cargoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaCargo, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        try {
            lista = cargoDao.listaCargo();
            grillaCargo.setItems(lista);
            grillaCargo.removeAllColumns();
            grillaCargo.addColumn(Cargo::getId).setCaption("Id Cargo");
            grillaCargo.addColumn(Cargo::getDescripcion).setCaption("Descripción");
            grillaCargo.setSizeFull();
            grillaCargo.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        cargoForm.setVisible(false);
                    } else {
                        cargoForm.setCargo(e.getValue());
                        cargoForm.setViejo();
                    }
                }
            });

            cargoForm.setGuardarListener(r -> {
                grillaCargo.clearSortOrder();
                lista = cargoDao.listaCargo();
                grillaCargo.setItems(lista);
            });
            cargoForm.setBorrarListener(r -> {
                grillaCargo.clearSortOrder();
            });
            cargoForm.setCancelarListener(r -> {
                grillaCargo.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            lista = cargoDao.getCargoByDescripcion(value);
            grillaCargo.setSizeFull();
            grillaCargo.setItems(lista);
            grillaCargo.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
