/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.formularios.RotacionForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class RotacionesVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Rotaciones";
    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Grid<Rotaciones> grillaRotaciones = new Grid<>(Rotaciones.class);
    TextField txtfFiltro = new TextField();
    TextField txtfFiltroPeriodo = new TextField();
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    Button btnSearch = new Button();
    List<Rotaciones> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    RotacionForm cargoForm = new RotacionForm();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public RotacionesVista() {
        setSizeFull();
        addStyleName("crud-view");

        Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
        List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
        List<Funcionario> listFunc = new ArrayList<>();
        listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnSearch.setIcon(VaadinIcons.SEARCH);

        fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
        fechaDesde.setWidth(8f, TextField.UNITS_EM);
        fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
        fechaHasta.setWidth(8f, TextField.UNITS_EM);
        filterFuncionario.setWidth(15f, TextField.UNITS_EM);

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            filterFuncionario.setItems(funcionarioController.listaFuncionario());
        } else {
            filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
        }
        filterFuncionario.setPlaceholder("Seleccione Funcionario");
        filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

        cargoForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(filterFuncionario, fechaDesde, fechaHasta, btnSearch, btnNuevo);
        topLayout.setSpacing(true);

        topLayout.addStyleName("top-bar");

        btnSearch.addClickListener(e -> findByAll());

//        txtfFiltro.setPlaceholder("Filtro por descripción");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

//        txtfFiltroPeriodo.setPlaceholder("Filtro por periodo");
        txtfFiltroPeriodo.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltroPeriodo.addValueChangeListener(e -> updateListPeriodo(e.getValue()));

        /*if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10 || usuario.getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }*/
        btnNuevo.addClickListener(e -> {
            LocalDate now = LocalDate.now(); // 2015-11-23
            LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
            LocalDate lastDay = now.with(lastDayOfYear());
            LocalDate today = LocalDate.now();

            LocalDate ld = now.plusYears(1L);
            LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
            LocalDate lastDaySecond = ld.with(lastDayOfYear());

            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

            List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
            if (listFeriado.size() == 0) {
                Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
            } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                grillaRotaciones.asSingleSelect().clear();
                cargoForm.setRotaciones(new Rotaciones());
                cargoForm.setViejo(null);
            }
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaRotaciones, cargoForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaRotaciones, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        txtfFiltro.setPlaceholder("Búsqueda por descripción");
        txtfFiltroPeriodo.setPlaceholder("Búsqueda por periodo");

        try {

            lista = new ArrayList<>();
            grillaRotaciones.setItems(lista);
            grillaRotaciones.removeAllColumns();
            grillaRotaciones.addColumn(Rotaciones::getId).setCaption("Cód");
            grillaRotaciones.addColumn(e -> {
                return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
            }).setCaption("Fecha");
            grillaRotaciones.addColumn(e -> {
                return e.getFuncionario().getNombreCompleto();
            }).setCaption("Funcionario");
            grillaRotaciones.addColumn(e -> {
                return e.getCargofunc();
            }).setCaption("Cargo");
            grillaRotaciones.addColumn(e -> {
                return e.getAreafunc();
            }).setCaption("Area");
            grillaRotaciones.setSizeFull();
            grillaRotaciones.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        cargoForm.setVisible(false);
                    } else {
                        cargoForm.setRotaciones(e.getValue());
                        cargoForm.setViejo(e.getValue());
                    }
                }
            });

            cargoForm.setGuardarListener(r -> {
                grillaRotaciones.clearSortOrder();
                filtroConFecha();
            });
            cargoForm.setBorrarListener(r -> {
                grillaRotaciones.clearSortOrder();
                filtroConFecha();
            });
            cargoForm.setCancelarListener(r -> {
                grillaRotaciones.clearSortOrder();
                filtroConFecha();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findByAll() {
        //        DateUtils.asDate(fechaDesde.getValue())
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                filtroConFecha();
            } else {
                Notification.show("Mensaje del Sistema", "Fecha hasta debe ser mayor o igual a fecha desde.", Notification.Type.HUMANIZED_MESSAGE);
            }
        } else {
            Notification.show("Mensaje del Sistema", "Fecha desde y fecha hasta no deben quedar vacíos.", Notification.Type.HUMANIZED_MESSAGE);
        }
    }

    private void updateList(String value) {
        try {
//            lista = rotacionDao.getRotacionesByDescripcion(value);
            grillaRotaciones.setSizeFull();
            grillaRotaciones.setItems(lista);
            grillaRotaciones.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateListPeriodo(String value) {
        try {
//            lista = rotacionDao.getRotacionesByPeriodo(value);
            grillaRotaciones.setSizeFull();
            grillaRotaciones.setItems(lista);
            grillaRotaciones.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtroConFecha() {
        grillaRotaciones.clearSortOrder();
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            List<Rotaciones> listTicket = rotacionDao.listarPorFechas(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getIdfuncionario());
            grillaRotaciones.setItems(listTicket);
        } else {
            List<Rotaciones> listTicket = rotacionDao.listarPorFechas(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), UserHolder.get().getIdfuncionario().getIdfuncionario());
            grillaRotaciones.setItems(listTicket);
        }
    }

}
