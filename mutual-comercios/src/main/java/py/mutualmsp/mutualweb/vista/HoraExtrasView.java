package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.google.gson.Gson;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ResourceReference;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.SolicitudDetalleDao;
import py.mutualmsp.mutualweb.dao.SolicitudProduccionDetalleDao;
import py.mutualmsp.mutualweb.dao.ToleranciasDao;
import py.mutualmsp.mutualweb.dto.MarcacionesDTO;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.Tolerancias;
import py.mutualmsp.mutualweb.formularios.ConsultaMarcacionesForm;
import py.mutualmsp.mutualweb.formularios.MotivosForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSaturday;
import static py.mutualmsp.mutualweb.vista.SolicitudView.ordenandoFechaString;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.HorarioExporadicoDao;
import py.mutualmsp.mutualweb.dao.SolicitudDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Solicitud;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.util.Constants;

/**
 * Created by Alfre on 7/6/2016.
 */
public class HoraExtrasView extends CssLayout implements View {

    public static final String VIEW_NAME = "Resumen";
    Grid<Marcaciones> grid = new Grid<>(Marcaciones.class);
    HorizontalLayout hl = new HorizontalLayout();
    HorizontalLayout h2 = new HorizontalLayout();
    HorizontalLayout h3 = new HorizontalLayout();
    SolicitudDetalleDao solicitudDetalleController = ResourceLocator.locate(SolicitudDetalleDao.class);
    HorarioExporadicoDao horarioExporadicoDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    SolicitudDao solicitudController = ResourceLocator.locate(SolicitudDao.class);
    SolicitudProduccionDetalleDao solicitudProduccionDetalleController = ResourceLocator.locate(SolicitudProduccionDetalleDao.class);
    //Button newUser = new Button("Nuevo");
    TextField txtfFiltro = new TextField();
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    ToleranciasDao toleranciaDao = ResourceLocator.locate(ToleranciasDao.class);
    ParametroDao parametrosController = ResourceLocator.locate(ParametroDao.class);
    Marcaciones marcacionSeleccionado;
    LicenciasDao licenciasDao = ResourceLocator.locate(LicenciasDao.class);
    LicenciasCompensarDao licenciasCompensarDao = ResourceLocator.locate(LicenciasCompensarDao.class);
    SuspencionesDao suspencionesDao = ResourceLocator.locate(SuspencionesDao.class);
//    ComboBox<Formulario> filter = new ComboBox<>("Filtre por formulario");
    MotivosForm form = new MotivosForm();
    ArrayList<MarcacionesDTO> listaMarcacionesDTO = new ArrayList<>();
    List<Marcaciones> listaMarcaciones = new ArrayList<>();
    Logger log = Logger.getLogger("UserViews");
    Button btnImprimir = new Button("");
    Button btnImprimirExcel = new Button("");
    Button btnFiltrar = new Button("");
    Button btnRealizarComparacionOrdProd = new Button("");
    String tmpFile = "";
    ConsultaMarcacionesForm consultaMarcacionesForm = new ConsultaMarcacionesForm();
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    ComboBox<String> filterEstado = new ComboBox<>();
    ComboBox<Funcionario> filterFunci = new ComboBox<>();
    //FileDownloader fileDownloader;
    ProgressBar spinnerCargando = new ProgressBar();
    ProgressBar spinnerComparando = new ProgressBar();
    String nombreFunc = "";
    long parametroLlegadaTardia = 0;
    long horaDiurna = 0;
    long horaNocturna = 0;

    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    String patternHora = "HH:mm";
    SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);

    String fileName = "";
    String ubicacion = "";
    FileOutputStream fos = null;
    String destinarariosString = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public HoraExtrasView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos");
            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getIdfuncionario(), funcio);
            }

            consultaMarcacionesForm.setVisible(false);
            nombreFunc = UserHolder.get().getIdfuncionario().getNombreCompleto();

            try {
                parametroLlegadaTardia = Long.parseLong(parametrosController.listarPorTipoCodigo("llegada_despues_hora").get(0).getValor());
            } catch (Exception e) {
                parametroLlegadaTardia = 0L;
            } finally {
            }
            try {
                horaDiurna = Long.parseLong(parametrosController.listarPorCodigo("horario_diurno").get(0).getValor());
            } catch (Exception e) {
                horaDiurna = 0L;
            } finally {
            }
            try {
                horaNocturna = Long.parseLong(parametrosController.listarPorCodigo("horario_nocturno").get(0).getValor());
            } catch (Exception e) {
                horaNocturna = 0L;
            } finally {
            }

            btnImprimir.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnImprimir.setIcon(VaadinIcons.PRINT);
            btnImprimirExcel.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnImprimirExcel.setIcon(VaadinIcons.DOWNLOAD_ALT);
            btnRealizarComparacionOrdProd.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            //btnRealizarComparacionOrdProd.setIcon(VaadinIcons.CHECK_SQUARE_O);
            btnRealizarComparacionOrdProd.setIcon(VaadinIcons.SEARCH);
            //btnFiltrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            //btnFiltrar.setIcon(VaadinIcons.SEARCH);

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            //horizontalLayout.addComponents(txtfFiltro);
            horizontalLayout.setSpacing(true);
//            horizontalLayout.setComponentAlignment(upload, Alignment.MIDDLE_RIGHT);
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                filterFunci.setItems(funcionarioController.listaFuncionario());
            } else {
                filterFunci.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
                filterFunci.setValue(UserHolder.get().getIdfuncionario());
                filterFunci.setEnabled(false);
            }
            List listaEstado = new ArrayList();
            listaEstado.add("APLICA");
            listaEstado.add("NO APLICA");
            filterEstado.setItems(listaEstado);

            horizontalLayout.addComponent(filterFunci);
            //horizontalLayout.addComponent(filterEstado);
            horizontalLayout.addComponent(fechaDesde);
            horizontalLayout.addComponent(fechaHasta);
            //horizontalLayout.addComponent(btnFiltrar);
            horizontalLayout.addComponent(btnRealizarComparacionOrdProd);
            horizontalLayout.addComponent(btnImprimir);
            horizontalLayout.addComponent(btnImprimirExcel);

            spinnerComparando.setIndeterminate(true);
            spinnerComparando.setCaption("Buscando Orden de Producción asociadas...");
            spinnerComparando.setVisible(true);
            spinnerComparando.setWidth(8f, TextField.UNITS_EM);
            spinnerComparando.setHeight(8f, TextField.UNITS_EM);
            h3.setVisible(false);

            spinnerCargando.setIndeterminate(true);
            spinnerCargando.setCaption("Cargando datos...");
            spinnerCargando.setWidth(8f, TextField.UNITS_EM);
            spinnerCargando.setHeight(8f, TextField.UNITS_EM);
            h2.setVisible(false);


            /*fechaDesde.addValueChangeListener(e -> findByFecha());
            fechaHasta.addValueChangeListener(e -> findByFecha());*/
            //horizontalLayout.addComponent(btnCargarDatos);
            //horizontalLayout.addComponent(btnImportar);
            horizontalLayout.addStyleName("top-bar");
            txtfFiltro.setPlaceholder("ID reloj");
            filterFunci.setPlaceholder("Funcionario");
            filterEstado.setPlaceholder("Estado");
            filterFunci.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            //filterFunci.setWidth(10f, TextField.UNITS_EM);
            //txtfFiltro.setWidth(4f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);
            filterFunci.setWidth(20f, TextField.UNITS_EM);
            form.setVisible(false);

            fechaDesde.setPlaceholder("Fecha desde");
            fechaHasta.setPlaceholder("Fecha hasta");

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

            //txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
            SimpleDateFormat sdf = new SimpleDateFormat("mm");
            SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");

//            listaMarcaciones = marcacionesController.listaMarcaciones();
            listaMarcaciones = new ArrayList<>();
            grid.setItems(listaMarcaciones);
            grid.removeAllColumns();

            grid.addColumn(e -> {
                return e.getHorarioFuncionario() == null ? "--" : e.getHorarioFuncionario().getIdreloj();
            }).setCaption("ID RELOJ");
            grid.addComponentColumn(e -> {
                if (!hayLicenciaCompensar(e.getFecha(), e.getHorarioFuncionario().getFuncionario())) {
                    if (e.getEstadoHsExtra() == null) {
                        return new Label("NO");
                    } else {
                        return e.getEstadoHsExtra2();
                    }
                } else {
                    return new Label("");
                }
            }).setCaption("APLICA");
            /*grid.addColumn(e -> {
                System.out.println("-> " + e.getId() + " - " + e.getHsextra());
                return e.getAplicaHsExtra();
            }).setCaption("APLICA");*/
            grid.addColumn(e -> {
                return e.getNombrefunc();
            }).setCaption("FUNCIONARIO");
            grid.addColumn(e -> {
                return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
            }).setCaption("FECHA");
            grid.addColumn(e -> {
                String hs = (e.getInasignada() == null ? "" : simpleDateFormatHora.format(e.getInasignada())) + " - " + (e.getOutasignada() == null ? "" : simpleDateFormatHora.format(e.getOutasignada()));
                return hs;
            }).setCaption("HS LABORAL");
            grid.addColumn(e -> {
                String hs = (e.getInmarcada() == null ? "" : simpleDateFormatHora.format(e.getInmarcada())) + " - " + (e.getOutmarcada() == null ? "" : simpleDateFormatHora.format(e.getOutmarcada()));
                return hs;
            }).setCaption("HS MARCACION");
            /*grid.addColumn(e -> {
                return e.getInmarcada() == null ? "--" : simpleDateFormatHora.format(e.getInmarcada());
            }).setCaption("ENTRADA");
            grid.addColumn(e -> {
                return e.getOutmarcada() == null ? "--" : simpleDateFormatHora.format(e.getOutmarcada());
            }).setCaption("SALIDA");*/
            grid.addColumn(e -> {
                try {
                    return e.getMintrabajada() == null ? "--" : e.getHoraTrabajada();
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("HS TRABJ");
            grid.addColumn(e -> {
                try {
                    return e.getMinllegadatardia() == null ? "--" : sdfHM.format(sdf.parse(e.getHsExtra() + ""));
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("HS EXTRA");
            grid.addColumn(e -> {
                try {
                    return e.getMinllegadatardia() == null ? "--" : e.getLlegadaTardia(parametroLlegadaTardia);
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("LLEG TARDIA");
            grid.addColumn(e -> {
                try {
//                    return e.getMinllegadatardia() == null ? "--" : e.getLlegadaDespuesHora(parametroLlegadaTardia);
                    return "--";
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("LLEG DP HS");
            grid.addColumn(e -> {
                long timeMillSalida = 0;
                try {
                    Map hsExporadico = new HashMap();
                    try {
                        hsExporadico = generarHsExporadico(e.getFecha(), e.getHorarioFuncionario().getFuncionario());
                        //NUEVA CON HORARIO EXPORADICO
                        if (hsExporadico.containsKey("fechahasta")) {
                            timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                        }
                    } catch (Exception ex) {
                    } finally {
                    }
                    return e.getOutmarcada() == null ? "--" : e.getSalidaAntesHora(timeMillSalida);
                } catch (Exception ex) {
                    return "--";
                }
            }).setCaption("SAL ANT HS");
            grid.addColumn(e -> {
                if (!hayLicenciaCompensar(e.getFecha(), e.getHorarioFuncionario().getFuncionario())) {
                    SolicitudProduccionDetalle spd = new SolicitudProduccionDetalle();
                    spd = solicitudProduccionDetalleController.listarPorIdFecha(e.getHorarioFuncionario().getFuncionario().getIdfuncionario(), e.getFecha());
                    try {
                        return e.getOutmarcada() == null ? "--" : e.getHsDiurna2(horaDiurna, horaNocturna, spd);
                    } catch (Exception ex) {
                        return "--";
                    }
                } else {
                    return "--";
                }
            }).setCaption("HS DIU");
            grid.addColumn(e -> {
                if (!hayLicenciaCompensar(e.getFecha(), e.getHorarioFuncionario().getFuncionario())) {
                    SolicitudProduccionDetalle spd = new SolicitudProduccionDetalle();
                    spd = solicitudProduccionDetalleController.listarPorIdFecha(e.getHorarioFuncionario().getFuncionario().getIdfuncionario(), e.getFecha());
                    try {
                        return e.getOutmarcada() == null ? "--" : e.getHsNocturna2(horaDiurna, horaNocturna, spd);
                    } catch (Exception ex) {
                        return "--";
                    }
                } else {
                    return "--";
                }
            }).setCaption("HS NOC");
//            grid.addColumn(e -> {
//                try {
//                    return e.getMinllegadatemprana() == null ? "--" : sdfHM.format(sdf.parse(e.getMinllegadatemprana() + ""));
//                } catch (Exception ex) {
//                    return "--";
//                }
//            }).setCaption("HS TEMPRANA");
            //grid.addComponentColumn(this::buildConfirmButton).setCaption("ORD PROD");
            grid.addColumn(e -> {
                try {
                    if (e.getSolicitud() == null) {
                        String filterObs = e.getObservacion() == null || e.getObservacion().equals("") ? "" : e.getObservacion().toUpperCase();
                        try {
                            if (e.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? e.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + e.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + e.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + e.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        return filterObs;
                    } else {
                        String filterObs = "";
                        try {
                            filterObs = e.getObservacion() == null || e.getObservacion().equals("") ? "" : " - " + e.getObservacion().toUpperCase();
                        } catch (Exception ex) {
                        } finally {
                        }
                        try {
                            if (e.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? e.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + e.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + e.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + e.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        return (e.getSolicitud().getId()) + " " + e.getSolicitud().getFormulario().getAbrev() + " " + filterObs;
                    }
                } catch (Exception ex) {
                    return e.getObservacion() != null ? e.getObservacion().toUpperCase() : "--";
                } finally {
                }
            }).setCaption("OBSERVACION");
            grid.setSizeFull();

            hl.addComponents(grid, consultaMarcacionesForm);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            h2.addComponent(spinnerCargando);
            //h2.setComponentAlignment(spinnerCargando, Alignment.MIDDLE_CENTER);

            h3.addComponent(spinnerComparando);
            //h3.setComponentAlignment(spinnerCargando, Alignment.MIDDLE_CENTER);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(hl);
            barAndGridLayout.addComponent(h2);
            barAndGridLayout.addComponent(h3);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.setExpandRatio(h2, 1);
            barAndGridLayout.setExpandRatio(h3, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

            consultaMarcacionesForm.setGuardarListener(r -> {
                grid.clearSortOrder();
                Date fechaDesdeText = null;
                Date fechaHastaText = null;
                String estado = null;
                Funcionario func = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    func = filterFunci.getValue();
                } catch (Exception e) {
                    func = null;
                } finally {
                }
                try {
                    fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                    fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                } catch (Exception e) {
                    fechaDesdeText = null;
                    fechaHastaText = null;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                txtfFiltro.setValue("");

                //ORIGINAL
                //listaMarcaciones = marcacionesController.getByFilterHoraExtra(func, fechaDesdeText, fechaHastaText, estado, idReloj);
                listaMarcaciones = marcacionesController.getByFilter(func, fechaDesdeText, fechaHastaText, estado, idReloj);
                grid.setItems(listaMarcaciones);
            });

            /*filterFunci.addValueChangeListener(vcl -> {
                try {
                    Date fechaDesdeText = null;
                    Date fechaHastaText = null;
                    String estado = null;
                    try {
                        fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                        fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                    } catch (Exception e) {
                        fechaDesdeText = null;
                        fechaHastaText = null;
                    } finally {
                    }
                    try {
                        estado = filterEstado.getValue();
                    } catch (Exception e) {
                        estado = null;
                    } finally {
                    }
                    txtfFiltro.setValue("");

                    listaMarcaciones = marcacionesController.getByFilter(vcl.getValue(), fechaDesdeText, fechaHastaText, estado);
                    grid.setSizeFull();
                    grid.setItems(listaMarcaciones);
                    grid.clearSortOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });*/
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (e.getValue() == null) {
                        consultaMarcacionesForm.setVisible(false);
                    } else {
                        if (e.getValue().getMintrabajada() < 0) {
                            consultaMarcacionesForm.setConsultaMarcaciones(e.getValue());
                        }
                    }
                }
            });
            btnImprimir.addClickListener(e -> {
                mostrarListaMarcacionesPDF();
                //Page.getCurrent().setLocation("http://www.example.com");

            });
            btnImprimirExcel.addClickListener(e -> {
                //generarExcel();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
                Date date = new Date(System.currentTimeMillis());
                //tmpFile = Constants.UPLOAD_DIR + "\\mutual-archivos\\tmp\\" + filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + ".xlsx";

                // Page.getCurrent().open("http://www.example.com", "_blank", false);
                final String basepath = tmpFile;

                Resource pdf = new FileResource(new File(tmpFile));
                /*Random random = new Random();
                int num = random.nextInt(8000);
                setResource(filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + "_" + num, pdf);
                ResourceReference rr = ResourceReference.create(pdf, this, filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + "_" + random.nextInt(8000));
                String here = rr.getURL();
                Page.getCurrent().open(here, "blank_");*/
                //setResource("help", pdf);

                ResourceReference resourceReference = ResourceReference.create(pdf,
                        this, "help");

                getState().resources.remove("help");
                getState().resources.put("help", resourceReference);

                ResourceReference rr = ResourceReference.create(pdf, this, "help");
                String here = rr.getURL();
                Page.getCurrent().open(here, "blank_");

                log.info("Descargando Excel...");
                /*if (fileDownloader != null) {
                    try {
                        btnImprimirExcel.removeExtension(fileDownloader);
                    } catch (Exception ex) {
                    } finally {
                    }
                }*/
            });
            btnFiltrar.addClickListener(e -> {
                UI ui = UI.getCurrent();

                // Instruct client side to poll for changes and show spinner
                ui.setPollInterval(500);
                h2.setVisible(true);
                hl.setVisible(false);

                // Start background task
                CompletableFuture.runAsync(() -> {
                    filtrarDatos();
                    // Need to use access() when running from background thread
                    ui.access(() -> {
                        // Stop polling and hide spinner
                        ui.setPollInterval(-1);
                        h2.setVisible(false);
                        hl.setVisible(true);
                    });
                });
            });
            btnRealizarComparacionOrdProd.addClickListener(e -> {
                boolean val = false;
                try {
                    if (filterFunci.getValue() != null && !filterFunci.getValue().equals("")) {
                        val = true;
                    }
                } catch (Exception ex) {
                } finally {
                }
                boolean valFechas = false;
                if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
                    valFechas = true;
                }

                if (val) {
                    if (valFechas) {
                        UI ui = UI.getCurrent();
                        // Instruct client side to poll for changes and show spinner
                        ui.setPollInterval(500);
                        h3.setVisible(true);
                        hl.setVisible(false);

                        // Start background task
                        CompletableFuture.runAsync(() -> {
                            compararOrdProduccion();
                            // Need to use access() when running from background thread
                            ui.access(() -> {
                                // Stop polling and hide spinner
                                ui.setPollInterval(-1);
                                h3.setVisible(false);
                                hl.setVisible(true);
                            });
                        });
                    } else {
                        Notification.show("Fecha desde y fecha hasta son campos obligatorios para filtrar.", Notification.Type.ERROR_MESSAGE);
                    }
                } else {
                    Notification.show("Debes seleccionar un funcionario para filtrar.", Notification.Type.ERROR_MESSAGE);
                }

            });
            /*filterEstado.addValueChangeListener(vcl -> {
                try {
                    Funcionario fun = null;
                    Date fechaDesdeText = null;
                    Date fechaHastaText = null;
                    try {
                        fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                        fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                    } catch (Exception e) {
                        fechaDesdeText = null;
                        fechaHastaText = null;
                    } finally {
                    }
                    try {
                        fun = filterFunci.getValue();
                    } catch (Exception e) {
                        fun = null;
                    } finally {
                    }
                    txtfFiltro.setValue("");

                    listaMarcaciones = marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, vcl.getValue());
                    grid.setSizeFull();
                    grid.setItems(listaMarcaciones);
                    grid.clearSortOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Button buildConfirmButton(Marcaciones p) {
        Button button = new Button(VaadinIcons.CHECK_CIRCLE);
        try {
            if (p.getSolicitudProduccionDetalle() == null) {
                button.setVisible(false);
            } else {
                button.setEnabled(true);
                button.setVisible(true);
            }
        } catch (Exception e) {
            button.setVisible(false);
        } finally {
        }
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> mostrarPDFSolicitud(p));
        return button;
    }

    private void updateList(String value) {
        String valor = value;
        filterFunci.setValue(null);
        filterEstado.setValue(null);
        fechaDesde.setValue(null);
        fechaHasta.setValue(null);
        txtfFiltro.setValue(valor);
        try {
            //listaMarcaciones = ;
            for (Marcaciones marcacion : marcacionesController.getByIdReloj(value)) {
                long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                minutesEntSal -= 40;
                if (marcacion.getMintrabajada() > minutesEntSal) {
                    Marcaciones marca = marcacionesController.getByIdSolicitudProduccion(marcacion.getId());
                    if (marca.getSolicitudProduccionDetalle() == null) {
                        long minExtra = (marcacion.getMintrabajada() - minutesEntSal);
                        if (minExtra >= 60) {
                            try {
                                SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                                marcacion.setSolicitudProduccionDetalle(spd);
                            } catch (Exception e) {
                            } finally {
                            }
                            marcacion.setHsextra(1);
                        } else {
                            marcacion.setHsextra(0);
                        }
                        marcacionesController.guardar(marcacion);
                    }
                    listaMarcaciones.add(marcacion);
                }
            }
            grid.setSizeFull();
            grid.setItems(listaMarcaciones);
            grid.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findByFecha() {
        txtfFiltro.setValue("");
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                Funcionario func = null;
                String estado = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    func = filterFunci.getValue();
                } catch (Exception e) {
                    func = null;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                txtfFiltro.setValue("");

                listaMarcaciones = marcacionesController.getByFilterHoraExtra(func, DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), estado, idReloj);
                grid.setSizeFull();
                grid.clearSortOrder();
                grid.setItems(listaMarcaciones);
            }
        }
    }

    private void mostrarPDFSolicitud(Marcaciones marcacion) {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudProduccion(marcacion) + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepEmpresa", "MUTUAL NACIONAL  DE FUNCIONARIOS DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "SOLICITUD DE PRODUCCION");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "";
                archivo += "formulario_produccion";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "";
        archivo += "formulario_produccion";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private void mostrarListaMarcacionesPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarMarcaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", nombreFunc);
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "hsextra";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "hsextra";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarSolicitudOrdenTrabajo(Marcaciones marcacion) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(marcacion.getSolicitud().getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("fechas", formatter.format(marcacion.getSolicitud().getFechacreacion()));
        jsonObj.put("codigo", marcacion.getSolicitud().getId());
        jsonObj.put("fechainicio", formatter.format(marcacion.getSolicitud().getFechaini()));
        jsonObj.put("fechafin", formatter.format(marcacion.getSolicitud().getFechafin()));
        jsonObj.put("horainicio", formatterHora.format(marcacion.getSolicitud().getHoraini()));
        jsonObj.put("horafin", formatterHora.format(marcacion.getSolicitud().getHorafin()));
        jsonObj.put("funcionario", marcacion.getSolicitud().getFuncionario().getNombre() + " " + marcacion.getSolicitud().getFuncionario().getApellido());
        jsonObj.put("ci", marcacion.getSolicitud().getFuncionario().getCedula());
        jsonObj.put("area", marcacion.getSolicitud().getAreafunc());
        jsonObj.put("observacion", marcacion.getSolicitud().getObservacion());
        try {
            if (marcacion.getSolicitud().getDependencia() == null) {
                jsonObj.put("dependencia", "");
            } else {
                jsonObj.put("dependencia", marcacion.getSolicitud().getDependencia());
            }
        } catch (Exception e) {
            jsonObj.put("dependencia", "");
        } finally {
        }
        if (solicitudDetalle.get(0).getMotivo().getDescripcion().length() > 30) {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(0, 30));
            jsonObj.put("tarea2", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(30, solicitudDetalle.get(0).getMotivo().getDescripcion().length()));
        } else {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase());
            jsonObj.put("tarea2", "");
        }

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private JSONArray cargarMarcaciones() {
        long hsTrab = 0;
        long hsTrabTarde = 0;
        long hsTrabTemp = 0;

        long hsCarga = 0;
        long hsLlegadaDspHs = 0;
        long hsSalidaAntsHs = 0;
        long hsDiurna = 0;
        long hsNocturna = 0;
        for (Marcaciones ticket : listaMarcaciones) {
            hsTrab += ticket.getMintrabajada() == null ? 0 : ticket.getMintrabajada();

            if (ticket.getMinllegadatardia() > 0 && ticket.getMinllegadatardia() <= parametroLlegadaTardia) {
                try {
                    hsTrabTarde += ticket.getMinllegadatardia() == null ? 0 : ticket.getMinllegadatardia();
                } catch (Exception ex) {
                } finally {
                }
            }

            hsTrabTemp += ticket.getMinllegadatemprana() == null ? 0 : ticket.getMinllegadatemprana();

            try {
                int minAsig = (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes()) - (ticket.getInasignada().getHours() * 60 + ticket.getInasignada().getMinutes());
                if (!isWeekendSaturday(DateUtils.asLocalDate(ticket.getFecha()))) {
                    minAsig -= 40;
                }
                hsCarga += minAsig;
            } catch (Exception ex) {
                hsCarga += 0;
            } finally {
            }

            if (ticket.getMinllegadatardia() > 0 && ticket.getMinllegadatardia() > parametroLlegadaTardia) {
                try {
                    hsLlegadaDspHs += ticket.getMinllegadatardia();
                } catch (Exception ex) {
                    hsLlegadaDspHs += 0;
                } finally {
                }
            }
            if ((ticket.getInmarcada().getHours() * 60 + ticket.getInmarcada().getMinutes()) == (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes())) {
                hsSalidaAntsHs += 0;
            } else {
                //long dif = TimeUnit.MILLISECONDS.toMinutes(ticket.getOutasignada().getTime() - ticket.getOutmarcada().getTime());
                long dif = (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes()) - (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes());
                if (dif > 0) {
                    try {
                        hsSalidaAntsHs += dif;
                    } catch (Exception ex) {
                        hsSalidaAntsHs += 0;
                    } finally {
                    }
                }
            }
            if (ticket.getHsExtra() > 0 && ticket.hsextra == 1) {
                if ((ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) <= (Integer.parseInt(horaNocturna + "") * 60)) {
                    int minDif = (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) - (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes());
                    try {
                        hsDiurna += minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                } else {
                    int minDif = (Integer.parseInt(horaNocturna + "") * 60) - (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes());
                    try {
                        hsDiurna += minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                }
            }
            if (ticket.getHsExtra() > 0 && ticket.hsextra == 1) {
                if ((ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) > (Integer.parseInt(horaNocturna + "") * 60)) {
                    int minDif = (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) - (Integer.parseInt(horaNocturna + "") * 60);
                    try {
                        hsNocturna = minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                }
            }
        }
        long hsTrabMod = hsTrab % 60;
        long hsTrabTardeMod = hsTrabTarde % 60;
        long hsTrabTempranoMod = hsTrabTemp % 60;

        long hsCargaMod = hsCarga % 60;
        long hsLlegadaDspHsMod = hsLlegadaDspHs % 60;
        long hsSalidaAntsHsMod = hsSalidaAntsHs % 60;
        long hsDiurnaMod = hsDiurna % 60;
        long hsNocturnaMod = hsNocturna % 60;

        JSONArray jsonArrayDato = new JSONArray();
        Comparator<Marcaciones> compareByDate = (Marcaciones o1, Marcaciones o2)
                -> o1.getFecha().compareTo(o2.getFecha());
        Collections.sort(listaMarcaciones, compareByDate);

        Locale spanishLocale = new Locale("es", "ES");
        String mesAnho = "";
//        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento("null", fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        if (listaMarcaciones.size() > 0) {
            mesAnho = DateUtils.asLocalDate(listaMarcaciones.get(0).getFecha()).format(DateTimeFormatter.ofPattern("MMMM/yyyy", spanishLocale)).toUpperCase();
        }
        ArrayList<String> timeStamp1 = new ArrayList<String>();
        ArrayList<String> timeStamp2 = new ArrayList<String>();
        ArrayList<String> timeStamp3 = new ArrayList<String>();
        ArrayList<String> timeStamp4 = new ArrayList<String>();
        ArrayList<String> timeStamp5 = new ArrayList<String>();
        ArrayList<String> timeStamp6 = new ArrayList<String>();
        ArrayList<String> timeStamp7 = new ArrayList<String>();

        ArrayList<String> totales1 = new ArrayList<String>();
        ArrayList<String> totales2 = new ArrayList<String>();
        ArrayList<String> totales3 = new ArrayList<String>();
        ArrayList<String> totales4 = new ArrayList<String>();
        ArrayList<String> totales5 = new ArrayList<String>();
        ArrayList<String> totales6 = new ArrayList<String>();
        ArrayList<String> totales7 = new ArrayList<String>();
        long idHere = 0;
        for (Marcaciones ticket : listaMarcaciones) {
            idHere++;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");

                org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
                jsonObj.put("subRepNomFun", nombreFunc);
                jsonObj.put("subRepTimestamp", "");
                jsonObj.put("mes", mesAnho);
                jsonObj.put("funcionarios", ticket.getHorarioFuncionario().getIdreloj() + " - " + ticket.getNombrefunc());

                String dateInSpanish = DateUtils.asLocalDate(ticket.getFecha()).format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy", spanishLocale));
                jsonObj.put("dia", dateInSpanish);
                if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                    jsonObj.put("aplica", ticket.getEstadoHsExtraSiNO());
                } else {
                    jsonObj.put("aplica", "NO");
                }
                String hsEntrada = (ticket.getInasignada() == null ? "" : simpleDateFormatHora.format(ticket.getInasignada())) + " - " + (ticket.getOutasignada() == null ? "" : simpleDateFormatHora.format(ticket.getOutasignada()));
                jsonObj.put("entrada", hsEntrada);
                String hsSalida = (ticket.getInmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getInmarcada())) + " - " + (ticket.getOutmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getOutmarcada()));
                jsonObj.put("salida", hsSalida);
                jsonObj.put("cargaAsignada", ticket.getHsAsignada());
                timeStamp1.add(jsonObj.get("cargaAsignada").toString());
                //jsonObj.put("hstrabajada", ticket.getMintrabajada() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMintrabajada() + "")));
                jsonObj.put("hstrabajada", ticket.getMintrabajada() == null ? "--" : ticket.getHoraTrabajada());
                timeStamp2.add(jsonObj.get("hstrabajada").toString());
                try {
                    jsonObj.put("hstardia", ticket.getMinllegadatardia() == null ? "--" : ticket.getLlegadaTardia(parametroLlegadaTardia));
                } catch (Exception ex) {
                    jsonObj.put("hstardia", "--");
                }
                timeStamp3.add(jsonObj.get("hstardia").toString());
                jsonObj.put("hstemprana", ticket.getMinllegadatemprana() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMinllegadatemprana() + "")));
                try {
//                    jsonObj.put("llegadadsphs", ticket.getMinllegadatardia() == null ? "--" : ticket.getLlegadaDespuesHora(parametroLlegadaTardia));
                    jsonObj.put("llegadadsphs", "--");
                } catch (Exception ex) {
                    jsonObj.put("llegadadsphs", "--");
                }
                timeStamp4.add(jsonObj.get("llegadadsphs").toString());
                long timeMillSalida = 0;
                Map hsExporadico = new HashMap();
                try {
                    hsExporadico = generarHsExporadico(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario());
                    //NUEVA CON HORARIO EXPORADICO
                    if (hsExporadico.containsKey("fechahasta")) {
                        timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                    }
                } catch (Exception ex) {
                } finally {
                }
                try {
                    jsonObj.put("llegadantshs", ticket.getOutmarcada() == null ? "--" : ticket.getSalidaAntesHora(timeMillSalida));
                } catch (Exception ex) {
                    jsonObj.put("llegadantshs", "--");
                }
                timeStamp5.add(jsonObj.get("llegadantshs").toString());
                try {
                    if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                        jsonObj.put("diurna", ticket.getOutmarcada() == null ? "--" : ticket.getHsDiurna2(horaDiurna, horaNocturna, ticket.getSolicitudProduccionDetalle()));
                    } else {
                        jsonObj.put("diurna", "--");
                    }
                } catch (Exception ex) {
                    jsonObj.put("diurna", "--");
                }
                timeStamp6.add(jsonObj.get("diurna").toString());
                try {
                    if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                        jsonObj.put("nocturna", ticket.getOutmarcada() == null ? "--" : ticket.getHsNocturna2(horaDiurna, horaNocturna, ticket.getSolicitudProduccionDetalle()));
                    } else {
                        jsonObj.put("nocturna", "--");
                    }
                } catch (Exception ex) {
                    jsonObj.put("nocturna", "--");
                }
                timeStamp7.add(jsonObj.get("nocturna").toString());
                //jsonObj.put("fecha", ticket.getFecha());
                //jsonObj.put("numot", marcacionesController.getById(ticket.getId()) == null ? "--" : marcacionesController.getById(ticket.getId()).getSolicitud().getId());
                try {
                    if (ticket.getSolicitud() == null) {
                        String filterObs = ticket.getObservacion() == null || ticket.getObservacion().equals("") ? "" : ticket.getObservacion().toUpperCase();
                        try {
                            if (ticket.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        jsonObj.put("numot", filterObs);
                    } else {
                        String filterObs = "";
                        try {
                            filterObs = ticket.getObservacion() == null || ticket.getObservacion().equals("") ? "" : " - " + ticket.getObservacion().toUpperCase();
                        } catch (Exception ex) {
                        } finally {
                        }
                        try {
                            if (ticket.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        jsonObj.put("numot", (ticket.getSolicitud().getId() + " " + ticket.getSolicitud().getFormulario().getAbrev()) + " " + filterObs);
                    }
                } catch (Exception ex) {
                    jsonObj.put("numot", ticket.getObservacion() != null ? ticket.getObservacion().toUpperCase() : "--");
                } finally {
                }
                String minHsTrab = String.valueOf(hsTrabMod).length() == 1 ? "0" + hsTrabMod : hsTrabMod + "";
                jsonObj.put("sumhstrabajada", ((hsTrab - hsTrabMod) / 60) + "." + minHsTrab);

                String minHsTrabTarde = String.valueOf(hsTrabTardeMod).length() == 1 ? "0" + hsTrabTardeMod : hsTrabTardeMod + "";
                jsonObj.put("sumhstardia", ((hsTrabTarde - hsTrabTardeMod) / 60) + "." + minHsTrabTarde);

                String minHsTrabTemp = String.valueOf(hsTrabTempranoMod).length() == 1 ? "0" + hsTrabTempranoMod : hsTrabTempranoMod + "";
                jsonObj.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);

                String minCarga = String.valueOf(hsCargaMod).length() == 1 ? "0" + hsCargaMod : hsCargaMod + "";
                jsonObj.put("sumCargaHs", ((hsCarga - hsCargaMod) / 60) + "." + minCarga);

                String minLlegadaDspHs = String.valueOf(hsLlegadaDspHsMod).length() == 1 ? "0" + hsLlegadaDspHsMod : hsLlegadaDspHsMod + "";
                jsonObj.put("sumLlegadaDspHs", ((hsLlegadaDspHs - hsLlegadaDspHsMod) / 60) + "." + minLlegadaDspHs);

                String minSalidaAntsHs = String.valueOf(hsSalidaAntsHsMod).length() == 1 ? "0" + hsSalidaAntsHsMod : hsSalidaAntsHsMod + "";
                jsonObj.put("sumSalidaAntHs", ((hsSalidaAntsHs - hsSalidaAntsHsMod) / 60) + "." + minSalidaAntsHs);

                String minDiurna = String.valueOf(hsDiurnaMod).length() == 1 ? "0" + hsDiurnaMod : hsDiurnaMod + "";
                jsonObj.put("sumDiurno", ((hsDiurna - hsDiurnaMod) / 60) + "." + minDiurna);

                String minNocturna = String.valueOf(hsNocturnaMod).length() == 1 ? "0" + hsNocturnaMod : hsNocturnaMod + "";
                jsonObj.put("sumNocturno", ((hsNocturna - hsNocturnaMod) / 60) + "." + minNocturna);
                if (listaMarcaciones.size() == idHere) {
                    jsonObj.put("sumCargaHs", calcularFechaHora(totales3));
                    jsonObj.put("sumhstrabajada", calcularFechaHora(totales1));
                    jsonObj.put("sumhstardia", calcularFechaHora(totales2));
                    jsonObj.put("sumLlegadaDspHs", calcularFechaHora(totales4));
                    jsonObj.put("sumSalidaAntHs", calcularFechaHora(totales5));
                    jsonObj.put("sumDiurno", calcularFechaHora(totales6));
                    jsonObj.put("sumNocturno", calcularFechaHora(totales7));

                    jsonObj.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);

                }
                jsonArrayDato.add(jsonObj);
                if ((isWeekend(DateUtils.asLocalDate(ticket.getFecha())) && idHere > 1) || listaMarcaciones.size() == idHere) {
                    org.json.simple.JSONObject jsonObjData = new org.json.simple.JSONObject();
                    jsonObjData.put("dia", "SUBTOTAL HORAS");
                    jsonObjData.put("aplica", "");
                    jsonObjData.put("entrada", "");
                    jsonObjData.put("salida", "");
                    jsonObjData.put("cargaAsignada", calcularFechaHora(timeStamp1));
                    jsonObjData.put("hstrabajada", calcularFechaHora(timeStamp2));
                    jsonObjData.put("hstardia", calcularFechaHora(timeStamp3));
                    jsonObjData.put("hstemprana", "");
                    jsonObjData.put("llegadadsphs", calcularFechaHora(timeStamp4));
                    jsonObjData.put("llegadantshs", calcularFechaHora(timeStamp5));
                    jsonObjData.put("diurna", calcularFechaHora(timeStamp6));
                    jsonObjData.put("nocturna", calcularFechaHora(timeStamp7));
                    jsonObjData.put("numot", "");

                    totales1.add(calcularFechaHora(timeStamp1));
                    totales2.add(calcularFechaHora(timeStamp2));
                    totales3.add(calcularFechaHora(timeStamp3));
                    totales4.add(calcularFechaHora(timeStamp4));
                    totales5.add(calcularFechaHora(timeStamp5));
                    totales6.add(calcularFechaHora(timeStamp6));
                    totales7.add(calcularFechaHora(timeStamp7));

                    jsonObjData.put("sumCargaHs", calcularFechaHora(totales1));
                    jsonObjData.put("sumhstrabajada", calcularFechaHora(totales2));
                    jsonObjData.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);
                    jsonObjData.put("sumhstardia", calcularFechaHora(totales3));
                    jsonObjData.put("sumLlegadaDspHs", calcularFechaHora(totales4));
                    jsonObjData.put("sumSalidaAntHs", calcularFechaHora(totales5));
                    jsonObjData.put("sumDiurno", calcularFechaHora(totales6));
                    jsonObjData.put("sumNocturno", calcularFechaHora(totales7));
//                    jsonObjData.put("sumhstrabajada", ((hsTrab - hsTrabMod) / 60) + "." + minHsTrab);
//                    jsonObjData.put("sumhstardia", ((hsTrabTarde - hsTrabTardeMod) / 60) + "." + minHsTrabTarde);
//                    jsonObjData.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);
//                    jsonObjData.put("sumCargaHs", ((hsCarga - hsCargaMod) / 60) + "." + minCarga);
//                    jsonObjData.put("sumLlegadaDspHs", ((hsLlegadaDspHs - hsLlegadaDspHsMod) / 60) + "." + minLlegadaDspHs);
//                    jsonObjData.put("sumSalidaAntHs", ((hsSalidaAntsHs - hsSalidaAntsHsMod) / 60) + "." + minSalidaAntsHs);
//                    jsonObjData.put("sumDiurno", ((hsDiurna - hsDiurnaMod) / 60) + "." + minDiurna);
//                    jsonObjData.put("sumNocturno", ((hsNocturna - hsNocturnaMod) / 60) + "." + minNocturna);

                    jsonObjData.put("subRepNomFun", nombreFunc);
                    jsonObjData.put("subRepTimestamp", "");
                    jsonObjData.put("mes", mesAnho);
                    jsonObjData.put("funcionarios", ticket.getHorarioFuncionario().getIdreloj() + " - " + ticket.getNombrefunc());
                    jsonArrayDato.add(jsonObjData);
                    timeStamp1 = new ArrayList<String>();
                    timeStamp2 = new ArrayList<String>();
                    timeStamp3 = new ArrayList<String>();
                    timeStamp4 = new ArrayList<String>();
                    timeStamp5 = new ArrayList<String>();
                    timeStamp6 = new ArrayList<String>();
                    timeStamp7 = new ArrayList<String>();
                }

            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        }
        totales1 = new ArrayList<String>();
        totales2 = new ArrayList<String>();
        totales3 = new ArrayList<String>();
        totales4 = new ArrayList<String>();
        totales5 = new ArrayList<String>();
        totales6 = new ArrayList<String>();
        totales7 = new ArrayList<String>();
        return jsonArrayDato;
    }

    private void filtrarDatos() {
//        if (!txtfFiltro.getValue().trim().equalsIgnoreCase("")) {
//            updateList(txtfFiltro.getValue());
//        } else {
        h2.setVisible(true);
        hl.setVisible(false);
        txtfFiltro.setValue("");
        if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
            if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                Funcionario fun = null;
                Date fechaDesdeText = null;
                Date fechaHastaText = null;
                String estado = null;
                long idReloj = 0;
                try {
                    idReloj = Long.parseLong(txtfFiltro.getValue());
                } catch (Exception e) {
                    idReloj = 0;
                } finally {
                }
                try {
                    estado = filterEstado.getValue();
                } catch (Exception e) {
                    estado = null;
                } finally {
                }
                try {
                    fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                    fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
                } catch (Exception e) {
                    fechaDesdeText = null;
                    fechaHastaText = null;
                } finally {
                }
                try {
                    fun = filterFunci.getValue();
                } catch (Exception e) {
                    fun = null;
                } finally {
                }
                txtfFiltro.setValue("");

                //ORIGINAL
                //listaMarcaciones = marcacionesController.getByFilterHoraExtra(fun, fechaDesdeText, fechaHastaText, estado, idReloj);
                listaMarcaciones = marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, estado, idReloj);
//                    for (Marcaciones marcacion : marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, estado, idReloj)) {
//                        long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
//                        minutesEntSal -= 40;
//                        if (marcacion.getMintrabajada() > minutesEntSal) {
//                            Marcaciones marca = marcacionesController.getByIdSolicitudProduccion(marcacion.getId());
//                            if (marca == null) {
//                                long minExtra = (marcacion.getMintrabajada() - minutesEntSal);
//                                if (minExtra >= 60) {
//                                    try {
//                                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
//                                        marcacion.setSolicitudProduccionDetalle(spd);
//                                    } catch (Exception e) {
//                                    } finally {
//                                    }
//                                    marcacion.setHsextra(1);
//                                } else {
//                                    marcacion.setHsextra(0);
//                                }
//                                marcacionesController.guardar(marcacion);
//                            }
//                            listaMarcaciones.add(marcacion);
//                        }
//                    }
                grid.setSizeFull();
                try {
                    grid.clearSortOrder();
                } catch (Exception e) {
                } finally {
                }
                grid.setItems(listaMarcaciones);
                generarExcel();
            }
        } else {
            Notification.show("Fecha desde y fecha hasta son campos obligatorios para filtrar.", Notification.Type.ERROR_MESSAGE);
        }
        hl.setVisible(true);
        h2.setVisible(false);
//        }
    }

    private JSONArray cargarSolicitudProduccion(Marcaciones marcacion) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        int num = 0;
        SolicitudProduccionDetalle solicitudProduccionDetalle = marcacionesController.getByIdSolicitudProduccion(marcacion.getId()).getSolicitudProduccionDetalle();
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("codigo", solicitudProduccionDetalle.getSolicitud().getId());
        jsonObj.put("funcionario", solicitudProduccionDetalle.getFuncionario().getNombre() + " " + solicitudProduccionDetalle.getFuncionario().getApellido());
        jsonObj.put("area", solicitudProduccionDetalle.getSolicitud().getAreafunc().toUpperCase());
        jsonObj.put("seccion", solicitudProduccionDetalle.getSolicitud().getCargofunc().toUpperCase());
        jsonObj.put("cargo", solicitudProduccionDetalle.getCargo().toUpperCase());
        jsonObj.put("fechaIngreso", formatter.format(solicitudProduccionDetalle.getSolicitud().getFechacreacion()));
        jsonObj.put("num", (num + 1));
        num++;
        jsonObj.put("motivo", solicitudProduccionDetalle.getDescripcion().toUpperCase());
        jsonObj.put("fechadesde", formatHor.format(solicitudProduccionDetalle.getHora()));

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private void compararOrdProduccion() {
        if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
            Funcionario fun = null;
            Date fechaDesdeText = null;
            Date fechaHastaText = null;
            String estado = null;
            long idReloj = 0;
            try {
                idReloj = Long.parseLong(txtfFiltro.getValue());
            } catch (Exception e) {
                idReloj = 0;
            } finally {
            }
            try {
                estado = filterEstado.getValue();
            } catch (Exception e) {
                estado = null;
            } finally {
            }
            try {
                fechaDesdeText = DateUtils.asDate(fechaDesde.getValue());
                fechaHastaText = DateUtils.asDate(fechaHasta.getValue());
            } catch (Exception e) {
                fechaDesdeText = null;
                fechaHastaText = null;
            } finally {
            }
            try {
                fun = filterFunci.getValue();
            } catch (Exception e) {
                fun = null;
            } finally {
            }
            txtfFiltro.setValue("");

            //ORIGINAL
            //listaMarcaciones = marcacionesController.getByFilterHoraExtra(fun, fechaDesdeText, fechaHastaText, estado, idReloj);
            List<Marcaciones> lisMarca = marcacionesController.getByFilter(fun, fechaDesdeText, fechaHastaText, estado, idReloj);
            listaMarcaciones = new ArrayList<>();
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Map<String, Long> mapMarcacion = new HashMap<String, Long>();
            Map<String, Marcaciones> mapMarca = new HashMap<String, Marcaciones>();
            Map<Long, HorarioFuncionario> mapHsFuncionario = new TreeMap<Long, HorarioFuncionario>();
            for (Marcaciones marcacion : lisMarca) {
                long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                minutesEntSal -= 40;
                if (marcacion.getMintrabajada() > minutesEntSal) {
                    Marcaciones marca = marcacionesController.getByIdSolicitudProduccion(marcacion.getId());
                    long minExtra = (marcacion.getMintrabajada() - minutesEntSal);
                    boolean hayODP = false;
                    try {
                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                        marcacion.setSolicitudProduccionDetalle(spd);
                        if (spd == null) {
                            hayODP = false;
                        } else {
                            hayODP = true;
                        }
                    } catch (Exception e) {
                        marcacion.setSolicitudProduccionDetalle(null);
                    } finally {
                    }
                    if (minExtra >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60 && hayODP) {
                        marcacion.setHsextra(1);
                    } else {
                        marcacion.setHsextra(0);
                    }

                    if (minExtra >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60 && hayODP) {
                        marcacion.setHsextra(1);
                    } else {
                        marcacion.setHsextra(0);
                    }

                    marcacionesController.guardar(marcacion);
                    //listaMarcaciones.add(marcacion);
                }
                mapMarcacion.put(formatter.format(marcacion.getFecha()), marcacion.getHorarioFuncionario().getIdreloj());
                mapMarca.put(formatter.format(marcacion.getFecha()), marcacion);
                mapHsFuncionario.put(marcacion.getHorarioFuncionario().getIdreloj(), marcacion.getHorarioFuncionario());
            }
            Calendar start = Calendar.getInstance();
            start.setTime(fechaDesdeText);

            Calendar end = Calendar.getInstance();
            end.setTime(fechaHastaText);

            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
            for (Map.Entry<Long, HorarioFuncionario> entry : mapHsFuncionario.entrySet()) {
                while (!start.after(end)) {
                    Date targetDay = start.getTime();
                    Marcaciones marcacion = new Marcaciones();
                    if (mapMarca.containsKey(formatter.format(targetDay))) {

                        marcacion = mapMarca.get(formatter.format(targetDay));
                        long minAcompensar = 0;
                        List<LicenciasCompensar> listLicenciaComp = licenciasCompensarDao.listarPorFechaUsuario(targetDay, filterFunci.getValue().getIdfuncionario());
                        if (listLicenciaComp.size() > 0) {
                            if (marcacion.getObservacion().trim().equalsIgnoreCase("") && listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                                marcacion.setObservacion(listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                                minAcompensar = listLicenciaComp.get(0).getCantidad();
                            } else if (listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                                minAcompensar = listLicenciaComp.get(0).getCantidad();
                            }
                        }
                        List<Suspenciones> listSuspenciones = suspencionesDao.listarPorFechas(targetDay, targetDay, filterFunci.getValue().getIdfuncionario());
                        if (listSuspenciones.size() > 0) {
                            if (marcacion.getObservacion().toUpperCase().equalsIgnoreCase("SIN MARCACION") || marcacion.getObservacion().toUpperCase().equalsIgnoreCase("")) {
                                marcacion.setObservacion(listSuspenciones.get(0).getParametro().getDescripcion().toUpperCase());
                            } else {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listSuspenciones.get(0).getParametro().getDescripcion().toUpperCase());
                            }
                        }

                        List<Tolerancias> listTolerancia = toleranciaDao.listarPorFechas(targetDay, targetDay, marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                        if (listTolerancia.size() > 0) {
                            //Si hay tolerancia recordar que la hora de entrada se considera desde una hora despues
                            if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                List<Parametro> param = parametrosController.listarPorCodigo(listTolerancia.get(0).getParametro().getCodigo());
                                long minTolerancia = Long.parseLong(param.get(0).getValor());
                                HorarioFuncionario hf = hfDao.listarPorIdFuncionario(filterFunci.getValue().getIdfuncionario());
                                marcacion.setHorarioFuncionario(hf);

                                //NUEVA CON HORARIO EXPORADICO
                                Map hsExporadico = new HashMap();
                                try {
                                    hsExporadico = generarHsExporadico(targetDay, filterFunci.getValue());
                                } catch (Exception e) {
                                } finally {
                                }
                                marcacion.setFecha(targetDay);
                                long timeMill = hf.getHorarioLaboral().getEntrada().getTime();
                                //NUEVA CON HORARIO EXPORADICO
                                if (hsExporadico.containsKey("fechadesde")) {
                                    timeMill = ((Timestamp) hsExporadico.get("fechadesde")).getTime();
                                }
                                long dif = timeMill + TimeUnit.MINUTES.toMillis(minTolerancia);
                                marcacion.setInasignada(new Timestamp(dif));

                                long timeMillSalida = hf.getHorarioLaboral().getSalida().getTime();
                                //NUEVA CON HORARIO EXPORADICO
                                if (hsExporadico.containsKey("fechahasta")) {
                                    timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                                }
                                long difSalida = timeMillSalida + TimeUnit.MINUTES.toMillis(minAcompensar);
                                marcacion.setOutasignada(new Timestamp(difSalida));

                                try {
                                    if (marcacion.getInmarcada() == null) {
                                        marcacion.setInmarcada(new Timestamp(dif));
                                    }
                                } catch (Exception e) {
                                } finally {
                                }
                                try {
                                    if (marcacion.getOutmarcada() == null) {
                                        marcacion.setOutmarcada(new Timestamp(difSalida));
                                    }
                                } catch (Exception e) {
                                } finally {
                                }
                                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sábado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setHorarioFuncionario(hf);
                                    long timeMille = hl.getEntrada().getTime();
                                    //NUEVA CON HORARIO EXPORADICO
                                    if (hsExporadico.containsKey("fechadesde")) {
                                        timeMille = ((Timestamp) hsExporadico.get("fechadesde")).getTime();
                                    }
                                    long dife = timeMille + TimeUnit.MINUTES.toMillis(minTolerancia);
                                    marcacion.setInasignada(new Timestamp(dife));

                                    long timeMillSalidaSab = hl.getSalida().getTime();
                                    //NUEVA CON HORARIO EXPORADICO
                                    if (hsExporadico.containsKey("fechahasta")) {
                                        timeMillSalidaSab = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                                    }
                                    long difSalidaSab = timeMillSalidaSab + TimeUnit.MINUTES.toMillis(minAcompensar);
                                    marcacion.setOutasignada(new Timestamp(difSalidaSab));

                                    long minSali = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                                    minSali -= 40;
                                    long minExtras = (marcacion.getMintrabajada() - minSali);
                                    try {
                                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                                        marcacion.setSolicitudProduccionDetalle(spd);
                                        if (spd == null) {
                                        } else {
                                            //el extra debe ser mayor o igual a 60, debe ingresar temprano y su hs extra debe superar 1 hora despues de su salida
                                            if (minExtras >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60) {
                                                marcacion.setHsextra(1);
                                            } else {
                                                marcacion.setHsextra(0);
                                            }
                                        }
                                    } catch (Exception e) {
                                        marcacion.setSolicitudProduccionDetalle(null);
                                    } finally {
                                    }
                                } else {
                                    //NUEVA CON HORARIO EXPORADICO
//                                    if (hsExporadico.containsKey("fechadesde")) {
//                                        marcacion.setInasignada((Timestamp) hsExporadico.get("fechadesde"));
//                                        marcacion.setOutasignada((Timestamp) hsExporadico.get("fechahasta"));
//                                    }
                                    //para ver si trabajo mas de una hora
                                    long minSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                                    minSal -= 40;
                                    long minExtra = (marcacion.getMintrabajada() - minSal);
                                    try {
                                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                                        marcacion.setSolicitudProduccionDetalle(spd);
                                        if (spd == null) {
                                        } else {
                                            if (minExtra >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60) {
                                                marcacion.setHsextra(1);
                                            } else {
                                                marcacion.setHsextra(0);
                                            }
                                        }
                                    } catch (Exception e) {
                                        marcacion.setSolicitudProduccionDetalle(null);
                                    } finally {
                                    }
                                }
//NUEVA CON HORARIO EXPORADICO
//                                if (hsExporadico.containsKey("fechadesde")) {
//                                    marcacion.setInasignada((Timestamp) hsExporadico.get("fechadesde"));
//                                    marcacion.setOutasignada((Timestamp) hsExporadico.get("fechahasta"));
//                                }
//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                long minutesEntSal = 0;
                                try {
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                } finally {
                                }

                                marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int difMarca = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(difMarca + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                            }
                            if (marcacion.getObservacion().toUpperCase().equalsIgnoreCase("SIN MARCACION") || marcacion.getObservacion().toUpperCase().equalsIgnoreCase("")) {
                                marcacion.setObservacion(listTolerancia.get(0).getParametro().getDescripcion().toUpperCase());
                            } else {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listTolerancia.get(0).getParametro().getDescripcion().toUpperCase());
                            }
                        } else {
                            marcacion = mapMarca.get(formatter.format(targetDay));
                            //NUEVA CON HORARIO EXPORADICO
                            Map hsExporadico = new HashMap();
                            try {
                                hsExporadico = generarHsExporadico(targetDay, filterFunci.getValue());
                            } catch (Exception e) {
                            } finally {
                            }
                            if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                HorarioFuncionario hf = hfDao.listarPorIdFuncionario(filterFunci.getValue().getIdfuncionario());
                                marcacion.setHorarioFuncionario(hf);

                                long timeMillSalida = hf.getHorarioLaboral().getSalida().getTime();
                                //NUEVA CON HORARIO EXPORADICO
                                if (hsExporadico.containsKey("fechahasta")) {
                                    timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                                }
                                long difSalida = timeMillSalida + TimeUnit.MINUTES.toMillis(minAcompensar);
                                marcacion.setOutasignada(new Timestamp(difSalida));
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sábado");
                                    hf.setHorarioLaboral(hl);

                                    long timeMillSalidaSab = hl.getSalida().getTime();
                                    //NUEVA CON HORARIO EXPORADICO
                                    if (hsExporadico.containsKey("fechahasta")) {
                                        timeMillSalidaSab = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                                    }
                                    long difSalidaSab = timeMillSalidaSab + TimeUnit.MINUTES.toMillis(minAcompensar);
                                    marcacion.setOutasignada(new Timestamp(difSalidaSab));

                                    long minSali = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                                    minSali -= 40;
                                    long minExtras = (marcacion.getMintrabajada() - minSali);
                                    try {
                                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                                        marcacion.setSolicitudProduccionDetalle(spd);
                                        if (spd == null) {
                                        } else {
                                            //el extra debe ser mayor o igual a 60, debe ingresar temprano y su hs extra debe superar 1 hora despues de su salida
                                            if (minExtras >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60) {
                                                marcacion.setHsextra(1);
                                            } else {
                                                marcacion.setHsextra(0);
                                            }
                                        }
                                    } catch (Exception e) {
                                        marcacion.setSolicitudProduccionDetalle(null);
                                    } finally {
                                    }
                                } else {
                                    //NUEVA CON HORARIO EXPORADICO
                                    if (hsExporadico.containsKey("fechadesde")) {
                                        marcacion.setInasignada((Timestamp) hsExporadico.get("fechadesde"));
                                        marcacion.setOutasignada((Timestamp) hsExporadico.get("fechahasta"));
                                    }
                                    //para ver si trabajo mas de una hora
                                    long minSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                                    minSal -= 40;
                                    long minExtra = (marcacion.getMintrabajada() - minSal);
                                    try {
                                        SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                                        marcacion.setSolicitudProduccionDetalle(spd);
                                        if (spd == null) {
                                        } else {
                                            if (minExtra >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) && (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) >= ((marcacion.getOutasignada().getHours() * 60 + marcacion.getOutasignada().getMinutes())) + 60) {
                                                marcacion.setHsextra(1);
                                            } else {
                                                marcacion.setHsextra(0);
                                            }
                                        }
                                    } catch (Exception e) {
                                        marcacion.setSolicitudProduccionDetalle(null);
                                    } finally {
                                    }
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                long minutesEntSal = 0;
                                try {
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                } finally {
                                }

                                marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int difMarca = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(difMarca + ""));

//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                            }
                        }
                        List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(targetDay, targetDay, entry.getValue().getFuncionario().getIdfuncionario());
                        if (listRotacion.size() > 0) {
                            if (!marcacion.getObservacion().equalsIgnoreCase("")) {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + "ROTACION");
                            } else {
                                marcacion.setObservacion("ROTACION");
                            }
                        }
                    } else {
                        //NUEVA CON HORARIO EXPORADICO
                        Map hsExporadico = new HashMap();
                        try {
                            hsExporadico = generarHsExporadico(targetDay, filterFunci.getValue());
                        } catch (Exception e) {
                        } finally {
                        }

                        long minAcompensar = 0;
                        List<LicenciasCompensar> listLicenciaComp = licenciasCompensarDao.listarPorFechaUsuario(targetDay, filterFunci.getValue().getIdfuncionario());
                        if (listLicenciaComp.size() > 0) {
                            if (listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                                minAcompensar = listLicenciaComp.get(0).getCantidad();
                            }
                        }

                        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(filterFunci.getValue().getIdfuncionario());
                        if (isWeekend(DateUtils.asLocalDate(targetDay))) {
                            HorarioLaboral hl = hlDao.getByValue("sábado");
                            hf.setHorarioLaboral(hl);
                            marcacion.setHorarioFuncionario(hf);
                            marcacion.setInasignada(hl.getEntrada());
                            //NUEVA CON HORARIO EXPORADICO
                            if (hsExporadico.containsKey("fechadesde")) {
                                marcacion.setInasignada((Timestamp) hsExporadico.get("fechadesde"));
                                marcacion.setOutasignada((Timestamp) hsExporadico.get("fechahasta"));
                            }

                            long timeMillSalida = hl.getSalida().getTime();
                            //NUEVA CON HORARIO EXPORADICO
                            if (hsExporadico.containsKey("fechahasta")) {
                                timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                            }
                            long difSalida = timeMillSalida + TimeUnit.MINUTES.toMillis(minAcompensar);
                            marcacion.setOutasignada(new Timestamp(difSalida));
                        } else {
                            marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                            //NUEVA CON HORARIO EXPORADICO
                            if (hsExporadico.containsKey("fechadesde")) {
                                marcacion.setInasignada((Timestamp) hsExporadico.get("fechadesde"));
                                marcacion.setOutasignada((Timestamp) hsExporadico.get("fechahasta"));
                            }
                            long timeMillSalida = hf.getHorarioLaboral().getSalida().getTime();
                            //NUEVA CON HORARIO EXPORADICO
                            if (hsExporadico.containsKey("fechahasta")) {
                                timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                            }
                            long difSalida = timeMillSalida + TimeUnit.MINUTES.toMillis(minAcompensar);
                            marcacion.setOutasignada(new Timestamp(difSalida));
                            marcacion.setHorarioFuncionario(hf);
                        }
                        List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(targetDay, targetDay, entry.getValue().getFuncionario().getIdfuncionario());
                        if (listRotacion.size() > 0) {
                            marcacion.setObservacion("ROTACION");
                            marcacion.setHsextra(0);
                        } else {
                            if (isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                marcacion.setObservacion("DOMINGO");
                                marcacion.setHsextra(0);
                            } else {
                                marcacion.setObservacion("SIN MARCACION");
                                marcacion.setHsextra(0);
                            }
                        }
                        if (isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                            marcacion.setInasignada(tsTolerancia);
                            marcacion.setOutasignada(tsTolerancia);
                        }
                        marcacion.setFecha(targetDay);
                        marcacion.setInmarcada(tsTolerancia);
                        marcacion.setOutmarcada(tsTolerancia);
                        marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                        marcacion.setMintrabajada(0l);
                        marcacion.setMinllegadatemprana(0L);
                        marcacion.setMinllegadatardia(0L);
                        marcacion.setSolicitud(null);
                        if (minAcompensar > 0) {
                            if (marcacion.getObservacion().trim().equalsIgnoreCase("") && listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                                marcacion.setObservacion(listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                                minAcompensar = listLicenciaComp.get(0).getCantidad();
                            } else if (listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                                minAcompensar = listLicenciaComp.get(0).getCantidad();
                            }
                        }
                        if (marcacion.getObservacion().equalsIgnoreCase("SIN MARCACION") && isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                            marcacion.setMintrabajada(270L);
                        }

                        List<Suspenciones> listSuspenciones = suspencionesDao.listarPorFechas(targetDay, targetDay, filterFunci.getValue().getIdfuncionario());
                        if (listSuspenciones.size() > 0) {
                            if (marcacion.getObservacion().toUpperCase().equalsIgnoreCase("SIN MARCACION")) {
                                marcacion.setObservacion(listSuspenciones.get(0).getParametro().getDescripcion().toUpperCase());
                            } else {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listSuspenciones.get(0).getParametro().getDescripcion().toUpperCase());
                            }
                        }
                        List<Tolerancias> listTolerancia = toleranciaDao.listarPorFechas(targetDay, targetDay, marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                        if (listTolerancia.size() > 0) {
                            List<Parametro> param = parametrosController.listarPorCodigo(listTolerancia.get(0).getParametro().getCodigo());
                            long minTolerancia = Long.parseLong(param.get(0).getValor());

                            long dif = marcacion.getInasignada().getTime() + TimeUnit.MINUTES.toMillis(minTolerancia);
                            marcacion.setInasignada(new Timestamp(dif));

                            if (marcacion.getObservacion().toUpperCase().equalsIgnoreCase("SIN MARCACION")) {
                                marcacion.setObservacion(listTolerancia.get(0).getParametro().getDescripcion().toUpperCase());
                            } else {
                                marcacion.setObservacion(marcacion.getObservacion() + " - " + listTolerancia.get(0).getParametro().getDescripcion().toUpperCase());
                            }
                        }
                    }

                    List<LicenciasCompensar> listLicenciaComp = licenciasCompensarDao.listarPorFechaUsuario(targetDay, marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario());
                    if (listLicenciaComp.size() > 0 && !listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                        if (marcacion.getObservacion().trim().equalsIgnoreCase("") || marcacion.getObservacion().trim().equalsIgnoreCase("SIN MARCACION")) {
                            marcacion.setObservacion(listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                        } else {
                            marcacion.setObservacion(marcacion.getObservacion() + " - " + listLicenciaComp.get(0).getLicencia().getId() + " " + listLicenciaComp.get(0).getLicencia().getParametro().getCodigo().replaceAll("_", " "));
                        }
                    }

                    /*try {
                        Solicitud soli = solicitudController.listarPorIdFechaSP(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                        if (soli != null) {
                            List<SolicitudProduccionDetalle> listDetalle = solicitudProduccionDetalleController.listarPorIdSolicitud(soli.getId());
                            for (SolicitudProduccionDetalle solicitudProduccionDetalle : listDetalle) {
                                if (solicitudProduccionDetalle.getFuncionario().getIdfuncionario() == marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario()) {
                                    String obs = marcacion.getObservacion();
                                    if (marcacion.getObservacion().trim().equalsIgnoreCase("") || marcacion.getObservacion().trim().equalsIgnoreCase("SIN MARCACION")) {
                                        marcacion.setObservacion(soli.getId() + " " + soli.getFormulario().getAbrev());
                                    } else {
                                        marcacion.setObservacion(obs + " - " + soli.getId() + " " + soli.getFormulario().getAbrev());
                                    }
                                }
                            }

                        }
                    } catch (Exception e) {
                    } finally {
                    }*/
                    try {
                        List<Licencias> listLicencia = licenciasDao.listarPorFechaUsuario(targetDay, filterFunci.getValue().getIdfuncionario());
                        Licencias licencia = listLicencia.get(0);
                        if (licencia.getParametro().getCodigo().equalsIgnoreCase("licencia_compensar")) {
                            if (marcacion.getObservacion().trim().equalsIgnoreCase("")) {
                                marcacion.setObservacion(licencia.getId() + " LIC A COMPENSAR");
                            } else {
                                String obs = marcacion.getObservacion();
                                marcacion.setObservacion(obs + " - " + licencia.getId() + " LIC A COMPENSAR");
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }

                    listaMarcaciones.add(marcacion);
                    start.add(Calendar.DATE, 1);
                }
            }
            grid.setSizeFull();
            try {
                grid.clearSortOrder();
            } catch (Exception e) {
            } finally {
            }
            h3.setVisible(false);
            hl.setVisible(true);
            grid.setItems(listaMarcaciones);
            generarExcel();
            Notification.show("Datos actualizados exitosamente", Notification.Type.HUMANIZED_MESSAGE);
        }
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public void generarExcel() {
        JSONObject output;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            output = new JSONObject("{\"ventas\": " + cargarMarcacionesSegundo() + "}");
            org.json.JSONArray docs = output.getJSONArray("ventas");

            //File file = new File(Constants.UPLOAD_DIR + "/mutual-archivos/" + filterFunci.getValue().getNombreCompleto() + ".xlsx");
            // String csv = CDL.toString(docs);
            //FileUtils.writeStringToFile(file, csv);
            //Notification.show("Datos exportados exitosamente", Notification.Type.HUMANIZED_MESSAGE);
        } catch (JSONException e) {
            System.out.println("-> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
            e.printStackTrace();
        } catch (IOException ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            Logger.getLogger(HoraExtrasView.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO Auto-generated catch block
    }

    private JSONArray cargarMarcacionesSegundo() throws FileNotFoundException, IOException {
        long hsTrab = 0;
        long hsTrabTarde = 0;
        long hsTrabTemp = 0;

        long hsCarga = 0;
        long hsLlegadaDspHs = 0;
        long hsSalidaAntsHs = 0;
        long hsDiurna = 0;
        long hsNocturna = 0;
        for (Marcaciones ticket : listaMarcaciones) {
            hsTrab += ticket.getMintrabajada() == null ? 0 : ticket.getMintrabajada();

            if (ticket.getMinllegadatardia() > 0 && ticket.getMinllegadatardia() <= parametroLlegadaTardia) {
                try {
                    hsTrabTarde += ticket.getMinllegadatardia() == null ? 0 : ticket.getMinllegadatardia();
                } catch (Exception ex) {
                } finally {
                }
            }

            hsTrabTemp += ticket.getMinllegadatemprana() == null ? 0 : ticket.getMinllegadatemprana();

            try {
                int minAsig = (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes()) - (ticket.getInasignada().getHours() * 60 + ticket.getInasignada().getMinutes());
                if (!isWeekendSaturday(DateUtils.asLocalDate(ticket.getFecha()))) {
                    minAsig -= 40;
                }
                hsCarga += minAsig;
            } catch (Exception ex) {
                hsCarga += 0;
            } finally {
            }

            if (ticket.getMinllegadatardia() > 0 && ticket.getMinllegadatardia() > parametroLlegadaTardia) {
                try {
                    hsLlegadaDspHs += ticket.getMinllegadatardia();
                } catch (Exception ex) {
                    hsLlegadaDspHs += 0;
                } finally {
                }
            }
            if ((ticket.getInmarcada().getHours() * 60 + ticket.getInmarcada().getMinutes()) == (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes())) {
                hsSalidaAntsHs += 0;
            } else {
                //long dif = TimeUnit.MILLISECONDS.toMinutes(ticket.getOutasignada().getTime() - ticket.getOutmarcada().getTime());
                int dif = (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes()) - (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes());
                if (dif > 0) {
                    try {
                        hsSalidaAntsHs += dif;
                    } catch (Exception ex) {
                        hsSalidaAntsHs += 0;
                    } finally {
                    }
                }
            }
            if (ticket.getHsExtra() > 0 && ticket.hsextra == 1) {
                if ((ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) <= (Integer.parseInt(horaNocturna + "") * 60)) {
                    int minDif = (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) - (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes());
                    try {
                        hsDiurna += minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                } else {
                    int minDif = (Integer.parseInt(horaNocturna + "") * 60) - (ticket.getOutasignada().getHours() * 60 + ticket.getOutasignada().getMinutes());
                    try {
                        hsDiurna += minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                }
            }
            if (ticket.getHsExtra() > 0 && ticket.hsextra == 1) {
                if ((ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) > (Integer.parseInt(horaNocturna + "") * 60)) {
                    int minDif = (ticket.getOutmarcada().getHours() * 60 + ticket.getOutmarcada().getMinutes()) - (Integer.parseInt(horaNocturna + "") * 60);
                    try {
                        hsNocturna = minDif;
                    } catch (Exception ex) {
                    } finally {
                    }
                }
            }
        }
        long hsTrabMod = hsTrab % 60;
        long hsTrabTardeMod = hsTrabTarde % 60;
        long hsTrabTempranoMod = hsTrabTemp % 60;

        long hsCargaMod = hsCarga % 60;
        long hsLlegadaDspHsMod = hsLlegadaDspHs % 60;
        long hsSalidaAntsHsMod = hsSalidaAntsHs % 60;
        long hsDiurnaMod = hsDiurna % 60;
        long hsNocturnaMod = hsNocturna % 60;

        JSONArray jsonArrayDato = new JSONArray();
        Comparator<Marcaciones> compareByDate = (Marcaciones o1, Marcaciones o2)
                -> o1.getFecha().compareTo(o2.getFecha());
        Collections.sort(listaMarcaciones, compareByDate);

        Locale spanishLocale = new Locale("es", "ES");
        String mesAnho = "";
//        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento("null", fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        if (listaMarcaciones.size() > 0) {
            mesAnho = DateUtils.asLocalDate(listaMarcaciones.get(0).getFecha()).format(DateTimeFormatter.ofPattern("MMMM/yyyy", spanishLocale)).toUpperCase();
        }
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Report");
        Row row = null;
        int here = 0;
        HashMap<String, Date> mapping = new HashMap();
        ArrayList<String> timeStamp1 = new ArrayList<String>();
        ArrayList<String> timeStamp2 = new ArrayList<String>();
        ArrayList<String> timeStamp3 = new ArrayList<String>();
        ArrayList<String> timeStamp4 = new ArrayList<String>();
        ArrayList<String> timeStamp5 = new ArrayList<String>();
        ArrayList<String> timeStamp6 = new ArrayList<String>();
        ArrayList<String> timeStamp7 = new ArrayList<String>();

        ArrayList<String> totales1 = new ArrayList<String>();
        ArrayList<String> totales2 = new ArrayList<String>();
        ArrayList<String> totales3 = new ArrayList<String>();
        ArrayList<String> totales4 = new ArrayList<String>();
        ArrayList<String> totales5 = new ArrayList<String>();
        ArrayList<String> totales6 = new ArrayList<String>();
        ArrayList<String> totales7 = new ArrayList<String>();
        int idHere = 0;
        for (Marcaciones ticket : listaMarcaciones) {
            idHere++;
            org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");

                jsonObj.put("subRepNomFun", nombreFunc);
                jsonObj.put("subRepTimestamp", "");
                jsonObj.put("mes", mesAnho);
                jsonObj.put("funcionarios", ticket.getHorarioFuncionario().getIdreloj() + " - " + ticket.getNombrefunc());

                String dateInSpanish = DateUtils.asLocalDate(ticket.getFecha()).format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy", spanishLocale));
                jsonObj.put("dia", dateInSpanish);
                mapping.put(dateInSpanish, ticket.getFecha());
                if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                    jsonObj.put("aplica", ticket.getEstadoHsExtraSiNO());
                } else {
                    jsonObj.put("aplica", "NO");
                }
                String hsEntrada = (ticket.getInasignada() == null ? "" : simpleDateFormatHora.format(ticket.getInasignada())) + " - " + (ticket.getOutasignada() == null ? "" : simpleDateFormatHora.format(ticket.getOutasignada()));
                jsonObj.put("entrada", hsEntrada);
                String hsSalida = (ticket.getInmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getInmarcada())) + " - " + (ticket.getOutmarcada() == null ? "" : simpleDateFormatHora.format(ticket.getOutmarcada()));
                jsonObj.put("salida", hsSalida);
                //jsonObj.put("hstrabajada", ticket.getMintrabajada() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMintrabajada() + "")));
                jsonObj.put("hstrabajada", ticket.getMintrabajada() == null ? "--" : ticket.getHoraTrabajada());
                jsonObj.put("cargaAsignada", ticket.getHsAsignada());
                try {
                    jsonObj.put("hstardia", ticket.getMinllegadatardia() == null ? "--" : ticket.getLlegadaTardia(parametroLlegadaTardia));
                } catch (Exception ex) {
                    jsonObj.put("hstardia", "--");
                }
                jsonObj.put("hstemprana", ticket.getMinllegadatemprana() == null ? "--" : sdfHM.format(sdf.parse(ticket.getMinllegadatemprana() + "")));
                try {
//                    jsonObj.put("llegadadsphs", ticket.getMinllegadatardia() == null ? "--" : ticket.getLlegadaDespuesHora(parametroLlegadaTardia));
                    jsonObj.put("llegadadsphs", "--");
                } catch (Exception ex) {
                    jsonObj.put("llegadadsphs", "--");
                }
                long timeMillSalida = 0;
                Map hsExporadico = new HashMap();
                try {
                    hsExporadico = generarHsExporadico(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario());
                    //NUEVA CON HORARIO EXPORADICO
                    if (hsExporadico.containsKey("fechahasta")) {
                        timeMillSalida = ((Timestamp) hsExporadico.get("fechahasta")).getTime();
                    }
                } catch (Exception ex) {
                } finally {
                }
                try {
                    jsonObj.put("llegadantshs", ticket.getOutmarcada() == null ? "--" : ticket.getSalidaAntesHora(timeMillSalida));
                } catch (Exception ex) {
                    jsonObj.put("llegadantshs", "--");
                }
                try {
                    if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                        jsonObj.put("diurna", ticket.getOutmarcada() == null ? "--" : ticket.getHsDiurna2(horaDiurna, horaNocturna, ticket.getSolicitudProduccionDetalle()));
                    } else {
                        jsonObj.put("diurna", "--");
                    }
                } catch (Exception ex) {
                    jsonObj.put("diurna", "--");
                }
                try {
                    if (!hayLicenciaCompensar(ticket.getFecha(), ticket.getHorarioFuncionario().getFuncionario())) {
                        jsonObj.put("nocturna", ticket.getOutmarcada() == null ? "--" : ticket.getHsNocturna2(horaDiurna, horaNocturna, ticket.getSolicitudProduccionDetalle()));
                    } else {
                        jsonObj.put("nocturna", "--");
                    }
                } catch (Exception ex) {
                    jsonObj.put("nocturna", "--");
                }
                //jsonObj.put("fecha", ticket.getFecha());
                //jsonObj.put("numot", marcacionesController.getById(ticket.getId()) == null ? "--" : marcacionesController.getById(ticket.getId()).getSolicitud().getId());
                try {
                    if (ticket.getSolicitud() == null) {
                        String filterObs = ticket.getObservacion() == null || ticket.getObservacion().equals("") ? "" : ticket.getObservacion().toUpperCase();
                        try {
                            if (ticket.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        jsonObj.put("numot", filterObs);
                    } else {
                        String filterObs = "";
                        try {
                            filterObs = ticket.getObservacion() == null || ticket.getObservacion().equals("") ? "" : " - " + ticket.getObservacion().toUpperCase();
                        } catch (Exception ex) {
                        } finally {
                        }
                        try {
                            if (ticket.getSolicitudProduccionDetalle() != null) {
                                filterObs += filterObs.equals("") ? ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev() : " - " + ticket.getSolicitudProduccionDetalle().getSolicitud().getId() + " " + ticket.getSolicitudProduccionDetalle().getSolicitud().getFormulario().getAbrev();
                            }
                        } catch (Exception ep) {
                        } finally {
                        }
                        jsonObj.put("numot", (ticket.getSolicitud().getId() + " " + ticket.getSolicitud().getFormulario().getAbrev()) + " " + filterObs);
                    }
                } catch (Exception ex) {
                    jsonObj.put("numot", ticket.getObservacion() != null ? ticket.getObservacion().toUpperCase() : "--");
                } finally {
                }
                String minHsTrab = String.valueOf(hsTrabMod).length() == 1 ? "0" + hsTrabMod : hsTrabMod + "";
                jsonObj.put("sumhstrabajada", ((hsTrab - hsTrabMod) / 60) + "." + minHsTrab);

                String minHsTrabTarde = String.valueOf(hsTrabTardeMod).length() == 1 ? "0" + hsTrabTardeMod : hsTrabTardeMod + "";
                jsonObj.put("sumhstardia", ((hsTrabTarde - hsTrabTardeMod) / 60) + "." + minHsTrabTarde);

                String minHsTrabTemp = String.valueOf(hsTrabTempranoMod).length() == 1 ? "0" + hsTrabTempranoMod : hsTrabTempranoMod + "";
                jsonObj.put("sumhstemprana", ((hsTrabTemp - hsTrabTempranoMod) / 60) + "." + minHsTrabTemp);

                String minCarga = String.valueOf(hsCargaMod).length() == 1 ? "0" + hsCargaMod : hsCargaMod + "";
                jsonObj.put("sumCargaHs", ((hsCarga - hsCargaMod) / 60) + "." + minCarga);

                String minLlegadaDspHs = String.valueOf(hsLlegadaDspHsMod).length() == 1 ? "0" + hsLlegadaDspHsMod : hsLlegadaDspHsMod + "";
                jsonObj.put("sumLlegadaDspHs", ((hsLlegadaDspHs - hsLlegadaDspHsMod) / 60) + "." + minLlegadaDspHs);

                String minSalidaAntsHs = String.valueOf(hsSalidaAntsHsMod).length() == 1 ? "0" + hsSalidaAntsHsMod : hsSalidaAntsHsMod + "";
                jsonObj.put("sumSalidaAntHs", ((hsSalidaAntsHs - hsSalidaAntsHsMod) / 60) + "." + minSalidaAntsHs);

                String minDiurna = String.valueOf(hsDiurnaMod).length() == 1 ? "0" + hsDiurnaMod : hsDiurnaMod + "";
                jsonObj.put("sumDiurno", ((hsDiurna - hsDiurnaMod) / 60) + "." + minDiurna);

                String minNocturna = String.valueOf(hsNocturnaMod).length() == 1 ? "0" + hsNocturnaMod : hsNocturnaMod + "";
                jsonObj.put("sumNocturno", ((hsNocturna - hsNocturnaMod) / 60) + "." + minNocturna);

                jsonArrayDato.add(jsonObj);
                boolean cabecera = false;
                if (here == 0) {
                    //PARA AGREGAR TITULO
                    sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 12));
                    row = sheet.createRow(here);

                    XSSFCellStyle style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontHeightInPoints((short) 1);
                    font.setBold(true);
                    style.setFont(font);
                    style.setAlignment(CellStyle.ALIGN_CENTER);

                    Cell cell1 = row.createCell(1);
                    row.setRowStyle(style);
                    cell1.setCellValue("PLANILLA INDIVIDUAL DE ASISTENCIA MES DE " + mesAnho);
                    cabecera = true;
                }
                if (cabecera) {
                    //PARA AGREGAR NOMBRE DE FUNCIONARIO
                    sheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 12));
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);

                    cell1.setCellValue("FUNCIONARIO/A: " + ticket.getHorarioFuncionario().getIdreloj() + " - " + ticket.getNombrefunc());
                    cabecera = true;
                }
                if (cabecera) {
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue("FECHA/DIA");
                    Cell cell2 = row.createCell(2);
                    cell2.setCellValue("APLICA");
                    Cell cell3 = row.createCell(3);
                    cell3.setCellValue("HS LABORAL");
                    Cell cell4 = row.createCell(4);
                    cell4.setCellValue("HS MARCACION");
                    Cell cell5 = row.createCell(5);
                    cell5.setCellValue("CARGA HORARIA");
                    Cell cell6 = row.createCell(6);
                    cell6.setCellValue("HS TRABAJADA");
                    Cell cell7 = row.createCell(7);
                    cell7.setCellValue("LLEGADA TARDIA");
                    Cell cell8 = row.createCell(8);
                    cell8.setCellValue("LLEGADA DESPUES HS");
                    Cell cell9 = row.createCell(9);
                    cell9.setCellValue("SALIDA ANTES HS");
                    Cell cell10 = row.createCell(10);
                    cell10.setCellValue("HS DIURNA");
                    Cell cell11 = row.createCell(11);
                    cell11.setCellValue("HS NOCTURNA");
                    Cell cell12 = row.createCell(12);
                    cell12.setCellValue("OBSERVACION");
                    cabecera = false;
                }
                row = sheet.createRow(++here);
                Cell cell1 = row.createCell(1);
                cell1.setCellValue(jsonObj.get("dia").toString());
                Cell cell2 = row.createCell(2);
                cell2.setCellValue(jsonObj.get("aplica").toString());
                Cell cell3 = row.createCell(3);
                cell3.setCellValue(jsonObj.get("entrada").toString());
                Cell cell4 = row.createCell(4);
                cell4.setCellValue(jsonObj.get("salida").toString());
                Cell cell5 = row.createCell(5);
                cell5.setCellValue(jsonObj.get("cargaAsignada").toString());
                timeStamp1.add(jsonObj.get("cargaAsignada").toString());
                Cell cell6 = row.createCell(6);
                cell6.setCellValue(jsonObj.get("hstrabajada").toString());
                timeStamp2.add(jsonObj.get("hstrabajada").toString());
                Cell cell7 = row.createCell(7);
                cell7.setCellValue(jsonObj.get("hstardia").toString());
                timeStamp3.add(jsonObj.get("hstardia").toString());
                Cell cell8 = row.createCell(8);
                cell8.setCellValue(jsonObj.get("llegadadsphs").toString());
                timeStamp4.add(jsonObj.get("llegadadsphs").toString());
                Cell cell9 = row.createCell(9);
                cell9.setCellValue(jsonObj.get("llegadantshs").toString());
                timeStamp5.add(jsonObj.get("llegadantshs").toString());
                Cell cell10 = row.createCell(10);
                cell10.setCellValue(jsonObj.get("diurna").toString());
                timeStamp6.add(jsonObj.get("diurna").toString());
                Cell cell11 = row.createCell(11);
                cell11.setCellValue(jsonObj.get("nocturna").toString());
                timeStamp7.add(jsonObj.get("nocturna").toString());
                Cell cell12 = row.createCell(12);
                cell12.setCellValue(jsonObj.get("numot").toString());
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
            if ((isWeekendSaturday(DateUtils.asLocalDate(mapping.get(jsonObj.get("dia").toString()))) && idHere > 1) || listaMarcaciones.size() == idHere) {
                row = sheet.createRow(++here);
                Cell cell1 = row.createCell(1);
                cell1.setCellValue("SUBTOTAL HORAS");
                Cell cell2 = row.createCell(2);
                cell2.setCellValue("");
                Cell cell3 = row.createCell(3);
                cell3.setCellValue("");
                Cell cell4 = row.createCell(4);
                cell4.setCellValue("");
                Cell cell5 = row.createCell(5);
                cell5.setCellValue(calcularFechaHora(timeStamp1));
                Cell cell6 = row.createCell(6);
                cell6.setCellValue(calcularFechaHora(timeStamp2));
                Cell cell7 = row.createCell(7);
                cell7.setCellValue(calcularFechaHora(timeStamp3));
                Cell cell8 = row.createCell(8);
                cell8.setCellValue(calcularFechaHora(timeStamp4));
                Cell cell9 = row.createCell(9);
                cell9.setCellValue(calcularFechaHora(timeStamp5));
                Cell cell10 = row.createCell(10);
                cell10.setCellValue(calcularFechaHora(timeStamp6));
                Cell cell11 = row.createCell(11);
                cell11.setCellValue(calcularFechaHora(timeStamp7));
                Cell cell12 = row.createCell(12);
                cell12.setCellValue("");

                totales1.add(calcularFechaHora(timeStamp1));
                totales2.add(calcularFechaHora(timeStamp2));
                totales3.add(calcularFechaHora(timeStamp3));
                totales4.add(calcularFechaHora(timeStamp4));
                totales5.add(calcularFechaHora(timeStamp5));
                totales6.add(calcularFechaHora(timeStamp6));
                totales7.add(calcularFechaHora(timeStamp7));

                timeStamp1 = new ArrayList<String>();
                timeStamp2 = new ArrayList<String>();
                timeStamp3 = new ArrayList<String>();
                timeStamp4 = new ArrayList<String>();
                timeStamp5 = new ArrayList<String>();
                timeStamp6 = new ArrayList<String>();
                timeStamp7 = new ArrayList<String>();

            }
        }
        if (here > 0) {
            row = sheet.createRow(++here);
            Cell cell1 = row.createCell(1);
            cell1.setCellValue("");
            Cell cell2 = row.createCell(2);
            cell2.setCellValue("");
            Cell cell3 = row.createCell(3);
            cell3.setCellValue("");
            Cell cell4 = row.createCell(4);
            cell4.setCellValue("TOTALES");
            Cell cell5 = row.createCell(5);
            Gson gson = new Gson();
            String jsonString = gson.toJson(jsonArrayDato.get((jsonArrayDato.size() - 1)));
            JSONObject jsonObj = new JSONObject(jsonString);
//            cell5.setCellValue(jsonObj.getString("sumCargaHs").toString());
            cell5.setCellValue(calcularFechaHora(totales1));
            Cell cell6 = row.createCell(6);
//            cell6.setCellValue(jsonObj.getString("sumhstrabajada").toString());
            cell6.setCellValue(calcularFechaHora(totales2));
            Cell cell7 = row.createCell(7);
//            cell7.setCellValue(jsonObj.getString("sumhstardia").toString());
            cell7.setCellValue(calcularFechaHora(totales3));
            Cell cell8 = row.createCell(8);
//            cell8.setCellValue(jsonObj.getString("sumLlegadaDspHs").toString());
            cell8.setCellValue(calcularFechaHora(totales4));
            Cell cell9 = row.createCell(9);
//            cell9.setCellValue(jsonObj.getString("sumSalidaAntHs").toString());
            cell9.setCellValue(calcularFechaHora(totales5));
            Cell cell10 = row.createCell(10);
//            cell10.setCellValue(jsonObj.getString("sumDiurno").toString());
            cell10.setCellValue(calcularFechaHora(totales6));
            Cell cell11 = row.createCell(11);
//            cell11.setCellValue(jsonObj.getString("sumNocturno").toString());
            cell11.setCellValue(calcularFechaHora(totales7));
            Cell cell12 = row.createCell(12);
            cell12.setCellValue("");

            totales1 = new ArrayList<String>();
            totales2 = new ArrayList<String>();
            totales3 = new ArrayList<String>();
            totales4 = new ArrayList<String>();
            totales5 = new ArrayList<String>();
            totales6 = new ArrayList<String>();
            totales7 = new ArrayList<String>();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
        Date date = new Date(System.currentTimeMillis());
        tmpFile = Constants.UPLOAD_DIR + "\\mutual-archivos\\tmp\\" + filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + ".xlsx";
        try (FileOutputStream outputStream = new FileOutputStream(tmpFile)) {
            workbook.write(outputStream);

            //  outputStream.flush();
            //  outputStream.close();
            // File file = new File(tmpFile);
            // StreamResource myResource;
            // myResource = createResource(file, filterFunci.getValue().getNombreCompleto() + "_" + formatter.format(date) + ".xlsx");
            // fileDownloader = new FileDownloader(myResource);
            //Page.getCurrent().getJavaScript().execute("window.open('https://www.w3schools.com');");
            //UI.getCurrent().getPage().getJavaScript().execute("window.open('https://www.w3schools.com');");
            // Page.getCurrent().setLocation("http://www.google.com");
            //fileDownloader.extend(btnImprimirExcel);
        }

        return jsonArrayDato;
    }

    /*private void compararOrdProduccion() {
        if (!listaMarcaciones.isEmpty()) {aaaaaaaaaaaaaaaaaaaaaa
            List<Marcaciones> lisMarca = listaMarcaciones;
            listaMarcaciones = new ArrayList<>();
            for (Marcaciones marcacion : lisMarca) {
                long minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutasignada().getTime() - marcacion.getInasignada().getTime());
                minutesEntSal -= 40;
                if (marcacion.getMintrabajada() > minutesEntSal) {
                    Marcaciones marca = marcacionesController.getByIdSolicitudProduccion(marcacion.getId());
                    if (marca == null) {
                        long minExtra = (marcacion.getMintrabajada() - minutesEntSal);
//                        la hora extra debe ser mayor a 60 min (1 hora) y debe ingresar antes o igual a su horario
                        try {
                            SolicitudProduccionDetalle spd = solicitudProduccionDetalleController.listarPorIdFecha(marcacion.getHorarioFuncionario().getFuncionario().getIdfuncionario(), marcacion.getFecha());
                            marcacion.setSolicitudProduccionDetalle(spd);
                            marcacion.setHsextra(1);
                        } catch (Exception e) {
                            marcacion.setHsextra(0);
                        } finally {
                        }
                        if (minExtra >= 60 && (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) <= (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                        } else {
                            marcacion.setHsextra(0);
                        }
                        marcacionesController.guardar(marcacion);
                    }
                    listaMarcaciones.add(marcacion);
                }
            }
            grid.setSizeFull();
            try {
                grid.clearSortOrder();
            } catch (Exception e) {
            } finally {
            }
            h3.setVisible(false);
            hl.setVisible(true);
            grid.setItems(listaMarcaciones);
            Notification.show("Datos actualizados exitosamente", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            Notification.show("No existen detalles para realizar la actualización.", Notification.Type.ERROR_MESSAGE);
        }
    }*/
    private static String format(long s) {
        if (s < 10) {
            return "0" + s;
        } else {
            return "" + s;
        }
    }

    private String calcularFechaHora(ArrayList<String> timestampsList) {
        try {
            long tm = 0;
            for (String tmp : timestampsList) {
                String[] arr = tmp.split("\\.");
                tm += 60 * Integer.parseInt(arr[1]);
                tm += 3600 * Integer.parseInt(arr[0]);
            }

            long hh = tm / 3600;
            tm %= 3600;
            long mm = tm / 60;
            return (format(hh) + "." + format(mm));
        } catch (Exception e) {
            long tm = 0;
            for (String tmp : timestampsList) {
                try {
                    String[] arr = tmp.split("\\.");
                    tm += 60 * Integer.parseInt(arr[1]);
                    tm += 3600 * Integer.parseInt(arr[0]);
                } catch (Exception ex) {
                    tm += 60 * 0;
                    tm += 3600 * 0;
                } finally {
                }
            }
            long hh = tm / 3600;
            tm %= 3600;
            long mm = tm / 60;
            return (format(hh) + "." + format(mm));
        } finally {
        }
    }

    private StreamResource createResource(File file, String nomarchivo) {
        return new StreamResource(new StreamSource() {
            @Override
            public InputStream getStream() {

                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }

            }
        }, nomarchivo);
    }

    public static String whatDays(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case MONDAY:
                return "LUNES";
            case TUESDAY:
                return "MARTES";
            case WEDNESDAY:
                return "MIERCOLES";
            case THURSDAY:
                return "JUEVES";
            case FRIDAY:
                return "VIERNES";
            case SATURDAY:
                return "SABADO";
            default:
                return "";
        }
    }

    private Map generarHsExporadico(Date targetDay, Funcionario fun) {
        Map mapeo = new HashMap();
        try {
            HorarioExporadico he = new HorarioExporadico();
            he.setLunes(false);
            he.setMartes(false);
            he.setMiercoles(false);
            he.setJueves(false);
            he.setViernes(false);
            he.setSabado(false);
            he.setFuncionario(fun);
            he.setFechadesde(targetDay);
            he.setFechahasta(targetDay);
            List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.consultarExistenciasPorDiasRecuperarDatos(whatDays(DateUtils.asLocalDate(targetDay)), targetDay, targetDay, fun.getIdfuncionario());
            if (!listHorarioExporadico.isEmpty()) {
                Calendar c = Calendar.getInstance();
                c.setTime(targetDay);
                boolean val = false;
                switch (c.get(Calendar.DAY_OF_WEEK)) {
                    case 2:
                        val = listHorarioExporadico.get(0).getLunes();
                        break;
                    case 3:
                        val = listHorarioExporadico.get(0).getMartes();
                        break;
                    case 4:
                        val = listHorarioExporadico.get(0).getMiercoles();
                        break;
                    case 5:
                        val = listHorarioExporadico.get(0).getJueves();
                        break;
                    case 6:
                        val = listHorarioExporadico.get(0).getViernes();
                        break;
                    case 7:
                        val = listHorarioExporadico.get(0).getSabado();
                        break;
                    default:
                }

                if (val) {
                    Timestamp tsDesde = new Timestamp(targetDay.getTime());
                    tsDesde.setHours(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getHours());
                    tsDesde.setMinutes(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getMinutes());
                    tsDesde.setSeconds(listHorarioExporadico.get(0).getHorarioLaboral().getEntrada().getSeconds());

                    Timestamp tsHasta = new Timestamp(targetDay.getTime());
                    tsHasta.setHours(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getHours());
                    tsHasta.setMinutes(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getMinutes());
                    tsHasta.setSeconds(listHorarioExporadico.get(0).getHorarioLaboral().getSalida().getSeconds());

                    mapeo.put("fechadesde", tsDesde);
                    mapeo.put("fechahasta", tsHasta);
                }
            }
        } catch (Exception ex) {
        } finally {
        }
        return mapeo;
    }

    public boolean hayLicenciaCompensar(Date targetDay, Funcionario fun) {
        List<LicenciasCompensar> listLicenciaComp = licenciasCompensarDao.listarPorFechaUsuario(targetDay, fun.getIdfuncionario());
        if (listLicenciaComp.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

}
