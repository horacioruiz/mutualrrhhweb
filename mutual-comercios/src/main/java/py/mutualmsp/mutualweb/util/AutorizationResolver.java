/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Usuario;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class AutorizationResolver implements Serializable {

    @PersistenceContext
    EntityManager em;
    private Usuario usuario;

    public Usuario authorizedUser(String user, String pass){
        try {
            String sql = "select u from Usuario u where u.usuario = :usuario and u.contrasena = :contrasena and u.activo = true";
            List<Usuario> lista = em.createQuery(sql)
                    .setParameter("usuario", user)
                    .setParameter("contrasena", pass)
                    .getResultList();
            if (!lista.isEmpty()){
                usuario = lista.get(0);
                return usuario;
            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
