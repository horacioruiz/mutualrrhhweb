/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.ModalidadDao;
import py.mutualmsp.mutualweb.entities.Modalidad;
import py.mutualmsp.mutualweb.formularios.ModalidadForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class ModalidadVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Modalidades";
    Grid<Modalidad> grillaModalidad = new Grid<>(Modalidad.class);
    TextField txtfFiltro = new TextField("Filtro");
    ModalidadDao modalidadDao = ResourceLocator.locate(ModalidadDao.class);
    List<Modalidad> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    ModalidadForm modalidadForm = new ModalidadForm();
    
    public ModalidadVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        modalidadForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaModalidad.asSingleSelect().clear();
            modalidadForm.setModalidad(new Modalidad());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaModalidad, modalidadForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaModalidad, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = modalidadDao.listaModalidad();
            grillaModalidad.setItems(lista);
            grillaModalidad.removeAllColumns();
            grillaModalidad.addColumn(Modalidad::getIdmodalidad).setCaption("Id Modalidad");
            grillaModalidad.addColumn(Modalidad::getDescripcion).setCaption("Descripción");
            grillaModalidad.setSizeFull();
            grillaModalidad.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    modalidadForm.setVisible(false);
                } else {
                    modalidadForm.setModalidad(e.getValue());
                    modalidadForm.setViejo();
                }
            });
            
            modalidadForm.setGuardarListener(r ->{
                grillaModalidad.clearSortOrder();
                lista = modalidadDao.listaModalidad();
                grillaModalidad.setItems(lista);
            });
            modalidadForm.setBorrarListener(r ->{
                grillaModalidad.clearSortOrder();
            });
            modalidadForm.setCancelarListener(r ->{
                grillaModalidad.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = modalidadDao.getListModalidad(value);
           grillaModalidad.setSizeFull();
           grillaModalidad.setItems(lista);
           grillaModalidad.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
