/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.sass.internal.parser.ParseException;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.mutualmsp.mutualweb.dao.VerificacionDao;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.dao.EstadoverificacionDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Verificacion;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.entities.Estadoverificacion;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;
import py.mutualmsp.mutualweb.formularios.VerificacionForm;
import py.mutualmsp.mutualweb.util.DBConnection;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */

public class VerificacionVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Verificaciones";
    public String textofiltro = " ";
    public String paramTipooperacion = "%";
    public String paramEstado = "%";
    public String paramVerificador = "%";
    Grid<Verificacion> grillaVerificacion = new Grid<>(Verificacion.class);
    VerificacionDao verificacionDao = ResourceLocator.locate(VerificacionDao.class);
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    EstadoverificacionDao estadoverificacionDao = ResourceLocator.locate(EstadoverificacionDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);

    List<Verificacion> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    Button btnReportes = new Button("Reportes");
    VerificacionForm verificacionForm = new VerificacionForm();
    DateField dtfFechaDesde = new DateField("Fecha Desde");
    DateField dtfFechaHasta = new DateField("Hasta");
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    ComboBox<Estadoverificacion> cmbEstadoVerificacion = new ComboBox<>("Estado Verificación");
    ComboBox<Funcionario> cmbVerificador = new ComboBox<>("Verificador");
    //TextField txtfVerificador = new TextField("Verificador");
    
    public VerificacionVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        btnReportes.addStyleName(MaterialTheme.BUTTON_FRIENDLY);
        verificacionForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        VerticalLayout izquierda = new VerticalLayout();
        VerticalLayout centroizquierda = new VerticalLayout();
        VerticalLayout centroderecha = new VerticalLayout();
        VerticalLayout derecha = new VerticalLayout();
        izquierda.addComponents(dtfFechaDesde, dtfFechaHasta);
        centroizquierda.addComponents(cmbTipooperacion, cmbEstadoVerificacion);
        //centroderecha.addComponents(txtfVerificador);
        centroderecha.addComponents(cmbVerificador);
        derecha.addComponents(btnNuevo, btnReportes);
        topLayout.addComponents(izquierda, centroizquierda, centroderecha, derecha);
        topLayout.setSpacing(true);
        //topLayout.setComponentAlignment(cmbTipooperacion, Alignment.MIDDLE_LEFT);
        //topLayout.setComponentAlignment(cmbEstadoVerificacion, Alignment.MIDDLE_CENTER);
        //topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_CENTER);
        //topLayout.setComponentAlignment(btnReportes, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        dtfFechaHasta.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")) {
                    java.util.Date fechaDesde = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaDesde.getValue().toString());
                    java.util.Date fechaHasta = new SimpleDateFormat("yyyy-MM-dd").parse(e.getValue().toString());
                    lista=verificacionDao.getVerificacionByFecha(fechaDesde, fechaHasta);
                    grillaVerificacion.setItems(lista);
                    //grillaVerificacion.clearSortOrder();
                    textofiltro="FECHA DE: "+new SimpleDateFormat("dd/MM/yyyy").format(fechaDesde)+" A "+new SimpleDateFormat("dd/MM/yyyy").format(fechaHasta);
                } else {
                    updateList("");
                    textofiltro=" ";
                }    
            } catch (Exception ex) {
                updateList("");
                textofiltro=" ";
            }
        });
        
        cmbTipooperacion.setItems(tipooperacionDao.listaTipooperacion());
        cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
        cmbTipooperacion.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroTipoOperacion(e.getValue().getIdtipooperacion());
                    //textofiltro+=" TIPO OPERACION: "+cmbTipooperacion.getValue().getDescripcion();
                    paramTipooperacion="%"+cmbTipooperacion.getValue().getDescripcion()+"%";
                } else {
                    updateList("");
                    textofiltro+=" ";
                    paramTipooperacion="%";
                }   
            } catch (Exception ex) {
                updateList("");
                textofiltro+=" ";
                paramTipooperacion="%";
            }
        });
        
        cmbEstadoVerificacion.setItems(estadoverificacionDao.listaEstadoverificacion());
        cmbEstadoVerificacion.setItemCaptionGenerator(Estadoverificacion::getDescripcion);
        cmbEstadoVerificacion.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroEstado(e.getValue().getIdestadoverificacion());
                    //textofiltro+=" ESTADO: "+cmbEstadoVerificacion.getValue().getDescripcion();
                    paramEstado="%"+cmbEstadoVerificacion.getValue().getDescripcion()+"%";                    
                }else {
                    updateList("");
                    textofiltro+=" ";
                    paramEstado="%";
                }
            } catch (Exception ex) {
                updateList("");
                textofiltro+=" ";
                paramEstado="%";
            }
        });
        
        cmbVerificador.setItems(funcionarioDao.listaVerificador());
        cmbVerificador.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        cmbVerificador.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroVerificador2(e.getValue().getId());
                    paramVerificador="%"+cmbVerificador.getValue().getNombreCompleto().toUpperCase()+"%";
                } else {
                    updateList("");
                    textofiltro+=" ";
                    paramVerificador="%";
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                updateList("");
                textofiltro+=" ";
                paramVerificador="%";
            }
        });
        /*txtfVerificador.setPlaceholder("Filtro de búsqueda");
        txtfVerificador.setValueChangeMode(ValueChangeMode.LAZY);
        txtfVerificador.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroVerificador(e.getValue());
                    //textofiltro+=" VERIFICADO POR: "+e.getValue();
                    paramVerificador="%"+txtfVerificador.getValue().toUpperCase()+"%";
                } else {
                    updateList("");
                    textofiltro+=" ";
                    paramVerificador="%";
                }
            } catch (Exception ex) {
                updateList("");
                textofiltro+=" ";
                paramVerificador="%";
            }
        });*/
        
        btnNuevo.addClickListener(e ->{
            topLayout.setVisible(false);
            verificacionForm.setVisible(true);
            grillaVerificacion.setVisible(false);
            verificacionForm.setVerificacion(new Verificacion());
            verificacionForm.cargarDatosPredeterminados();
        });
        
        btnReportes.addClickListener(e ->{
            opcionesImpresion();
            //imprimir();
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaVerificacion, verificacionForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaVerificacion, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = verificacionDao.listaVerificacion();
            Collections.reverseOrder();
            Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
            //lista.sort((o1, o2) -> {
            //    return o2.getIdverificacion().compareTo(o1.getIdverificacion()); //To change body of generated lambdas, choose Tools | Templates.
            //});
            grillaVerificacion.setItems(lista);
            grillaVerificacion.removeAllColumns();
            grillaVerificacion.addColumn(Verificacion::getIdverificacion).setCaption("Id Verificación");
            grillaVerificacion.addColumn(Verificacion::getFechaverificacion).setCaption("Fecha");
            grillaVerificacion.addColumn(tipoOperacion -> {
                return tipoOperacion.getIdtipooperacion().getDescripcion();}).setCaption("Tipo Operación");
            grillaVerificacion.addColumn(estado -> {
                return estado.getIdestadoverificacion().getDescripcion();}).setCaption("Estado");
            grillaVerificacion.addColumn(verificador -> {
                return verificador.getIdverificador().getNombreCompleto();}).setCaption("Verificado por");
            grillaVerificacion.addColumn(revisador -> {
                return revisador.getIdrevisador().getNombreCompleto();}).setCaption("Revisado por");
            grillaVerificacion.setSizeFull();
            grillaVerificacion.asSingleSelect().addValueChangeListener(e ->{
//                if (e.getValue()==null){
//                    verificacionForm.setVisible(false);
//                } else {
//                    verificacionForm.setVerificacion(e.getValue());
//                }
            });
            grillaVerificacion.addItemClickListener(e -> {
                if (e.getMouseEventDetails().isDoubleClick()) {
                    verificacionForm.setVerificacion(e.getItem());
                    verificacionForm.setVisible(true);
                    if(e.getItem().getIdestadoverificacion().getDescripcion().contains("ANULADO") || e.getItem().getIdestadoverificacion().getDescripcion().contains("VERIFICADO")) {
                        Object valor = UserHolder.get().getIdnivelusuario();
                        if (UserHolder.get().getIdnivelusuario()!=8) {
                            verificacionForm.deshabilitarTodo();
                        }
                    } else verificacionForm.habilitarTodo();
                    
                    topLayout.setVisible(false);
                    grillaVerificacion.setVisible(false);
                    //verificacionForm.setViejo();
                }
            });
            
            verificacionForm.setGuardarListener(r ->{
                //grillaVerificacion.clearSortOrder();
                lista = verificacionDao.listaVerificacion();
                Collections.reverseOrder();
                Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
                grillaVerificacion.setItems(lista);
                topLayout.setVisible(true);
                grillaVerificacion.setVisible(true);
            });
            verificacionForm.setBorrarListener(r ->{
                //grillaVerificacion.clearSortOrder();
                lista = verificacionDao.listaVerificacion();
                Collections.reverseOrder();
                Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
                grillaVerificacion.setItems(lista);
                topLayout.setVisible(true);
                grillaVerificacion.setVisible(true);
            });
            verificacionForm.setCancelarListener(r ->{
                //grillaVerificacion.clearSortOrder();
                topLayout.setVisible(true);
                grillaVerificacion.setVisible(true);
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = verificacionDao.getListVerificacion(value);
           Collections.reverseOrder();
           Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
           grillaVerificacion.setSizeFull();
           grillaVerificacion.setItems(lista);
           //grillaVerificacion.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
    private void filtroTipoOperacion(Long idtipooperacion) {
        try {
            lista = verificacionDao.getVerificacionByTipoOperacion(idtipooperacion);
            Collections.reverseOrder();
            Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
            grillaVerificacion.setItems(lista);
            //grillaVerificacion.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    } 

    private void filtroEstado(Long idestadoverificacion) {
        try {
            lista = verificacionDao.getVerificacionByEstado(idestadoverificacion);
            Collections.reverseOrder();
            Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
            grillaVerificacion.setItems(lista);
            //grillaVerificacion.clearSortOrder();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtroVerificador(String valor) {
        try {
            lista = verificacionDao.getVerificacionByVerificador(valor);
            Collections.reverseOrder();
            Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
            grillaVerificacion.setItems(lista);
            //grillaVerificacion.clearSortOrder();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void filtroVerificador2(Long idverificador) {
        try {
            lista = verificacionDao.getVerificacionByVerificador2(idverificador);
            Collections.reverseOrder();
            Collections.sort(lista, Comparator.comparing((Verificacion e) -> e.getIdverificacion()).reversed());
            grillaVerificacion.setItems(lista);
            //grillaVerificacion.clearSortOrder();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void setTextoFiltro() {
        if(cmbTipooperacion.getValue()!=null) {
            if(!textofiltro.contains("TIPO")) {
                textofiltro+=" - TIPO OPERACION: "+cmbTipooperacion.getValue().getDescripcion();
            }
        }
        if(cmbEstadoVerificacion.getValue()!=null) {
            if(!textofiltro.contains("ESTADO")) {
                textofiltro+=" - ESTADO: "+cmbEstadoVerificacion.getValue().getDescripcion();
            }
        }
        if(cmbVerificador.getValue()!=null) {
            if(!textofiltro.contains("VERIFICADO POR")) {
                textofiltro+=" - VERIFICADO POR: "+cmbVerificador.getValue().getNombreCompleto();
            }
        }
        /*if(!txtfVerificador.getValue().isEmpty()) {
            if(!textofiltro.contains("VERIFICADO POR")) {
                textofiltro+=" - VERIFICADO POR: "+txtfVerificador.getValue().toUpperCase();
            }
        }*/
    }

    private void opcionesImpresion(){
        Window subVentana = new Window("Opciones de Impresión");
        FormLayout subContenido = new FormLayout();
        FormLayout subContenido2 = new FormLayout();
        //VerticalLayout vertical = new VerticalLayout();
        HorizontalLayout horizontal = new HorizontalLayout();
        Button btnImprimirLista = new Button("Imprimir Listado");
        Button btnImprimirResumen = new Button("Resumen por tipo");
        Button btnExcel = new Button("Exportar a Excel");
        Button btnResumen = new Button("Resumen en Excel");
        Button btnDescargar = new Button("Descargar");
        btnDescargar.setVisible(false);
        Button btnCerrar = new Button("Cerrar");
        subVentana.setWidth("40%");
        subVentana.setHeight("55%");
        btnImprimirLista.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
        btnImprimirResumen.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
        btnExcel.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
        btnResumen.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
        
        subContenido.addComponent(btnImprimirLista);
        subContenido.addComponent(btnExcel);
        subContenido2.addComponent(btnImprimirResumen);
        subContenido2.addComponent(btnResumen);
        subContenido2.addComponent(btnCerrar);
        subContenido.addComponent(btnDescargar);
        //vertical.addComponent(subContenido);
        horizontal.addComponents(subContenido, subContenido2);
        //subVentana.setContent(vertical);
        subVentana.setContent(horizontal);
        
        btnImprimirLista.addClickListener(i -> {
            imprimirLista();
        });
        
        btnImprimirResumen.addClickListener(i -> {
            imprimirResumen();
        });
        
        btnExcel.addClickListener(exc -> {
           try {
                String fechaDesde = dtfFechaDesde.getValue().toString();
                String fechaHasta = dtfFechaHasta.getValue().toString();
                List<Verificacion> listaexcel = verificacionDao.getVerificacionByTodos(fechaDesde, fechaHasta, paramTipooperacion, paramEstado, paramVerificador);
                StreamResource myResource = createResource(generarExcel(listaexcel));
                FileDownloader fileDownloader = new FileDownloader(myResource);
                fileDownloader.extend(btnDescargar);
                btnDescargar.setVisible(true);
                } catch (ParseException ex) {
                }
        });
        
        btnResumen.addClickListener(res ->{
            try {
                String fechaDesde = dtfFechaDesde.getValue().toString();
                String fechaHasta = dtfFechaHasta.getValue().toString();
                List<Verificacion> listaexcel = verificacionDao.getVerificacionByTodos(fechaDesde, fechaHasta, paramTipooperacion, paramEstado, paramVerificador);
                StreamResource myResource = createResource(generarResumenExcel(listaexcel));
                FileDownloader fileDownloader = new FileDownloader(myResource);
                fileDownloader.extend(btnDescargar);
                btnDescargar.setVisible(true);
            } catch (ParseException ex) {
            } catch (java.text.ParseException ex) {
                Logger.getLogger(VerificacionVista.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        btnCerrar.addClickListener(c -> {subVentana.close();});
        
        // Centrar en la subVentana del browser
        subVentana.center();
        // Abrir la subVentana en la UI
        this.getUI().addWindow(subVentana);
    }
    
    private void imprimirLista() {
        try {
            String fechaDesde = dtfFechaDesde.getValue().toString();
            String fechaHasta = dtfFechaHasta.getValue().toString();
            final Map map = new HashMap();
            DBConnection dBConnection = new DBConnection();
            map.put("usuario", UserHolder.get().getUsuario());
            map.put("titulo", "LISTADO DE VERIFICACIONES");
            this.setTextoFiltro();
            map.put("subtitulo", textofiltro);
            map.put("fechaDesde",fechaDesde);
            map.put("fechaHasta",fechaHasta);
            map.put("tipooperacion", paramTipooperacion);
            map.put("estado", paramEstado);
            map.put("verificador", paramVerificador);
            StreamResource.StreamSource source = new StreamResource.StreamSource() {
                @Override
                public InputStream getStream() {
                    byte[] b = null;
                    try {
                        try {
                            b = JasperRunManager.runReportToPdf(getClass()
                                    .getClassLoader()
                                    .getResourceAsStream("/repVerificacionLista.jasper"), 
                                    map, dBConnection.getConnection());
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (JRException ex) {
                       ex.printStackTrace();
                    }

                    return new ByteArrayInputStream(b);
                }
            };
            StreamResource resource = new StreamResource(source, "repVerificacionLista.pdf");
            resource.setMIMEType("application/pdf");
            resource.setCacheTime(0);
            getUI().getPage().open(resource, "_blank", false);            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private File generarExcel(List<Verificacion> listaResumen){
        try {
            //lugar temporal donde se guarda el archivo 
            //String tmpFile = "C:\\Temp\\listverific.xls";
            String tmpFile = "C:\\uploads\\temp\\listverific.xls";
            List<Verificacion> listaXlsVerificacion = listaResumen;
            Collections.sort(listaXlsVerificacion, (o1, o2) -> o1.getIdverificacion().compareTo(o2.getIdverificacion()));
            XSSFWorkbook workbook = new XSSFWorkbook();
            CreationHelper createHelper = workbook.getCreationHelper();
            XSSFSheet sheet = workbook.createSheet("Listado Verificacion");
            // Creamos la cabecera
            List<String> cabeceras = new ArrayList<>();
            cabeceras.add("Nº VERIF.");
            cabeceras.add("FECHA");
            cabeceras.add("TIPO OPERACION");
            cabeceras.add("MODALIDAD");
            cabeceras.add("CEDULA");
            cabeceras.add("Nº OPERACION");
            cabeceras.add("VERIFICADOR");
            cabeceras.add("ESTADO");
            cabeceras.add("RESPONSABLE");
            cabeceras.add("OBSERVACION");

            int rowNum = 0, cabeceraNum = 0;
            Row rowCabecera = sheet.createRow(rowNum++);
            for (String cabecera : cabeceras) {
                Cell cellHeader = rowCabecera.createCell(cabeceraNum++);
                cellHeader.setCellValue(cabecera);
                // Negrita
                CellStyle cellStyleHeader = cellHeader.getCellStyle();
                XSSFFont fontBold = workbook.createFont();
                fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                fontBold.setBold(true);
                cellStyleHeader.setFont(fontBold);
                cellHeader.setCellStyle(cellStyleHeader);
            }
            // Generamos los datos
            for (Verificacion fila : listaXlsVerificacion) {
                Row rowRespuesta = sheet.createRow(rowNum++);
                cabeceraNum = 0;
                // No me interesa el index 0
                for (int i = 0; i < cabeceras.size(); i++) {
                    Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                    // i = 3 -> PDV, i = 4 -> Ubicacion
                    if (i ==0) {
                        cellValue.setCellValue(fila.getIdverificacion());
                    } else if (i == 1) {
                        cellValue.setCellValue(fila.getFechaverificacion()== null ? "" : fila.getFechaverificacion().toString());
                    } else if (i == 2) {
                        cellValue.setCellValue(fila.getIdtipooperacion() == null ? "" : fila.getIdtipooperacion().getDescripcion());
                    } else if (i ==3) {
                        cellValue.setCellValue(fila.getIdmodalidad() == null ? "" : fila.getIdmodalidad().getDescripcion());
                    } else if (i == 4){
                        cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula()) ;
                    } else if (i == 5){
                        cellValue.setCellValue(fila.getNumerooperacion() == null ? 0 :fila.getNumerooperacion());
                    } else if (i == 6){
                        cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                    } else if (i == 7){
                        cellValue.setCellValue(fila.getIdestadoverificacion() == null ? "" : fila.getIdestadoverificacion().getDescripcion());
                    } else if (i == 8){
                        cellValue.setCellValue(fila.getIdresponsable() == null  ? "" : fila.getIdresponsable().getNombreCompleto());
                    } else if (i == 9){
                        cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                    }
                }
            }

            try {
                FileOutputStream outputStream = new FileOutputStream(tmpFile);
                workbook.write(outputStream);
                outputStream.flush();
                outputStream.close();

                File file = new File(tmpFile);
                return file;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    private File generarResumenExcel(List<Verificacion> listaResumen) throws java.text.ParseException{
        try {
            //String tmpFile = "C:\\Temp\\resumenverific.xls";
            String tmpFile = "C:\\uploads\\temp\\resumenverific.xls";
            List<Verificacion> resumenXlsVerificacion = listaResumen;
            Collections.sort(resumenXlsVerificacion, (o1, o2) -> o1.getIdverificacion().compareTo(o2.getIdverificacion()));
            XSSFWorkbook workbook = new XSSFWorkbook();
            CreationHelper createHelper = workbook.getCreationHelper();
            XSSFSheet sheet = workbook.createSheet("Resumen Verificacion");
            List<String> cabeceras = new ArrayList<>();
            int rowNum = 0, cabeceraNum = 0;
            Row rowCabecera = sheet.createRow(rowNum++);
            if (cmbTipooperacion.getValue()==null){
                Notification.show("Advertencia", "Debe seleccionar un tipo de control", Notification.Type.ERROR_MESSAGE);
                return null;
            }
            Cell cellHeader = rowCabecera.createCell(0);
            cellHeader.setCellValue("CONTROL DE "+cmbTipooperacion.getValue().getDescripcion()+" - DE "+dtfFechaDesde.getValue().toString()+" A "+dtfFechaHasta.getValue().toString());
            if (cmbTipooperacion.getValue().getDescripcion().contains("PAGARE")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº SOLICITUD");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("FECHA OPERACION");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(verificacion.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("ENTREGADO POR");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < cabeceras.size(); i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumerosolicitud() == null ? 0 : fila.getNumerosolicitud());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerooperacion() == null ? 0 : fila.getNumerooperacion());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getFechaoperacion() == null ? "" : fila.getFechaoperacion().toString());
                        } else if (i == 5) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" :fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(fila.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+7;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdaprueba() == null ? "" : fila.getIdaprueba().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                }
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("ESTADO DE CUENTA")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(verificacion.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("ENTREGADO POR");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 3; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(fila.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+7;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdaprueba() == null ? "" : fila.getIdaprueba().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("CONTRATO DE AHORRO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº CONTRATO");
                cabeceras.add("CEDULA");
                cabeceras.add("MES");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(verificacion.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 5; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumerocontrato()== null ? 0 : fila.getNumerocontrato());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(fila.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("RENUNCIA")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("CEDULA");
                cabeceras.add("PROCEDENCIA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 4; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getIdprocedencia()== null ? "" : fila.getIdprocedencia().getDescripcion());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("JURIDICO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº PLANILLA");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(verificacion.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("APROBADO POR");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 4; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumeroplanilla()== null ? 0 : fila.getNumeroplanilla());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    //Collections.sort(fila.getVerificaciondetalleList(), (o1, o2) -> o1.getIdverificaciondetalle().compareTo(o2.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("INGRESO DE SOCIO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RECEPCIONADO POR");
                cabeceras.add("RESPONSABLE DE CARGA");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 3; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+7;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable2() == null ? "" : fila.getIdresponsable2().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("INGRESOS POR CAJA")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("CEDULA");
                cabeceras.add("Nº RECIBO");
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("REALIZADO EN");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < cabeceras.size(); i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumeroboleta()== null ? 0 : fila.getNumeroboleta());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == 5) {
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == 6) {
                            cellValue.setCellValue(fila.getIdprocedencia() == null  ? "" : fila.getIdprocedencia().getDescripcion());
                        } else if (i == 7) {
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == 8) {
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == 9) {
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        } 
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE REPROGRAMACION")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº SOLICITUD");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("FECHA");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("APROBADO POR");
                cabeceras.add("OFICIAL DE CREDITO");
                cabeceras.add("LIQUIDADO POR");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 6; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumerosolicitud()== null ? 0 : fila.getNumerosolicitud());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerooperacion()== null ? 0 : fila.getNumerooperacion());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getFechaoperacion()== null ? "" : fila.getFechaoperacion().toString());
                        } else if (i == 5) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+8;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdaprueba()== null ? "" : fila.getIdaprueba().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdresponsable2() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+7){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("HORAS EXTRA")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº PLANILLA");
                cabeceras.add("MES");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("RESPONSABLE DE CARGA");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 5; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumeroplanilla()== null ? 0 : fila.getNumeroplanilla());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+5;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("CHEQUES")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("FECHA VERIF.");
                cabeceras.add("FECHA ARQUEO");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("RESPONSABLE DE ARQUEO");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 4; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getFechaverificacion()== null ? "" : fila.getFechaoperacion().toString());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getFechaoperacion()== null ? "" : fila.getFechaoperacion().toString());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+5;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().equals("PRESTAMO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("MES");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("APROBADO POR");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 5; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerooperacion()== null ? 0 : fila.getNumerooperacion());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+7;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional()== null ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdaprueba()== null ? "" : fila.getIdaprueba().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("PAGOS VARIOS")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº ORDEN PAGO");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("Nº CHEQUE");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 5; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumeroordenpago()== null ? 0 : fila.getNumeroordenpago());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerooperacion()== null ? 0 : fila.getNumerooperacion());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getNumerocheque()== null ? 0 : fila.getNumerocheque());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional()== null ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("REEMBOLSO DE AHORRO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("MES FINALIZA");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 4; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getCedula()== null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional()== null ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("FALLECIDOS")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("MES");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 4; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getCedula()== null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional()== null ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().equals("REPROGRAMACION")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("MES");
                cabeceras.add("Nº SOLICITUD");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("CEDULA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("LIQUIDADO POR");
                cabeceras.add("APROBADO POR");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 6; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getMes()== null ? "" : fila.getMes());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerosolicitud()== null ? 0 : fila.getNumerosolicitud());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getNumerooperacion()== null ? 0 : fila.getNumerooperacion());
                        } else if (i == 5) {
                            cellValue.setCellValue(fila.getCedula() == null ? "" : fila.getCedula());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+7;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());                            
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdaprueba()== null ? "" : fila.getIdaprueba().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+6){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            } else if (cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE PRESTAMO")) {
                cabeceras.add("Nº ORDEN");
                cabeceras.add("Nº VERIFICACION");
                cabeceras.add("Nº OPERACION");
                cabeceras.add("Nº CHEQUE");
                cabeceras.add("FECHA");
                for (Verificacion verificacion : resumenXlsVerificacion) {
                    Collections.sort(verificacion.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : verificacion.getVerificaciondetalleList()) {
                        cabeceras.add(verificaciondetalle.getIdrequisito().getDescripcion()+" ("+verificaciondetalle.getIdrequisito().getIdtiporequisito().getDescripcion()+")");
                    }
                    break;
                }
                cabeceras.add("OBSERVACIONES");
                cabeceras.add("REGION");
                cabeceras.add("RESPONSABLE");
                cabeceras.add("VERIFICADOR");
                cabeceras.add("ESTADO");
                rowCabecera = sheet.createRow(rowNum++);
                for (String cabecera : cabeceras) {
                    cellHeader = rowCabecera.createCell(cabeceraNum++);
                    cellHeader.setCellValue(cabecera);
                    CellStyle cellStyleHeader = cellHeader.getCellStyle();
                    XSSFFont fontBold = workbook.createFont();
                    fontBold.setColor(new XSSFColor(Color.decode("#010000")));
                    fontBold.setBold(true);
                    cellStyleHeader.setFont(fontBold);
                    cellHeader.setCellStyle(cellStyleHeader);
                }
                
                for (Verificacion fila : resumenXlsVerificacion) {
                    Row rowRespuesta = sheet.createRow(rowNum++);
                    cabeceraNum = 0;
                    for (int i = 0; i < 5; i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i ==0) {
                            cellValue.setCellValue(rowNum-2);
                        } else if (i == 1) {
                            cellValue.setCellValue(fila.getIdverificacion()== null ? 0 : fila.getIdverificacion());
                        } else if (i == 2) {
                            cellValue.setCellValue(fila.getNumerooperacion()== null ? 0 : fila.getNumerooperacion());
                        } else if (i == 3) {
                            cellValue.setCellValue(fila.getNumerocheque()== null ? 0 : fila.getNumerocheque());
                        } else if (i == 4) {
                            cellValue.setCellValue(fila.getFechaoperacion()== null ? "" : fila.getFechaoperacion().toString());
                            break;
                        } 
                    }
                    Collections.sort(fila.getVerificaciondetalleList(),
                            Comparator.comparing((Verificaciondetalle e) -> e.getIdrequisito().getIdtiporequisito().getIdtiporequisito())
                                        .thenComparing(e -> e.getIdverificaciondetalle()));
                    for (Verificaciondetalle verificaciondetalle : fila.getVerificaciondetalleList()) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        cellValue.setCellValue(verificaciondetalle.getNoverificado()== null ? 0 : verificaciondetalle.getNoverificado() ? 1 : 0);
                    }
                    for (int i = fila.getVerificaciondetalleList().size()+1;i<fila.getVerificaciondetalleList().size()+6;i++) {
                        Cell cellValue = rowRespuesta.createCell(cabeceraNum++);
                        if (i == fila.getVerificaciondetalleList().size()+1){
                            cellValue.setCellValue(fila.getObservaciones() == null ? "" : fila.getObservaciones());
                        } else if (i == fila.getVerificaciondetalleList().size()+2){
                            cellValue.setCellValue(fila.getIdregional() == null  ? "" : fila.getIdregional().getDescripcion());
                        } else if (i == fila.getVerificaciondetalleList().size()+3){
                            cellValue.setCellValue(fila.getIdresponsable() == null ? "" : fila.getIdresponsable().getNombreCompleto());                            
                        } else if (i == fila.getVerificaciondetalleList().size()+4){
                            cellValue.setCellValue(fila.getIdverificador() == null ? "" : fila.getIdverificador().getNombreCompleto());
                        } else if (i == fila.getVerificaciondetalleList().size()+5){
                            cellValue.setCellValue(fila.getIdestadoverificacion()== null ? "" : fila.getIdestadoverificacion().getDescripcion());
                            break;
                        }
                    }
                } 
            }
            
            try {
                FileOutputStream outputStream = new FileOutputStream(tmpFile);
                workbook.write(outputStream);
                outputStream.flush();
                outputStream.close();

                File file = new File(tmpFile);
                return file;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    private StreamResource createResource(File file) {
        return new StreamResource(new StreamSource() {
            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(file);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, "listadoVerificacion.xls");
    }

    private void imprimirResumen() {
        try {
            if (cmbTipooperacion.getValue()==null){
                Notification.show("Advertencia", "Debe seleccionar un tipo de control", Notification.Type.ERROR_MESSAGE);
                return;
            }
            String fechaDesde = dtfFechaDesde.getValue().toString();
            String fechaHasta = dtfFechaHasta.getValue().toString();
            final Map map = new HashMap();
            DBConnection dBConnection = new DBConnection();
            map.put("usuario", UserHolder.get().getUsuario());
            map.put("titulo", "RESUMEN DE VERIFICACIONES");
            this.setTextoFiltro();
            map.put("subtitulo", textofiltro);
            map.put("fechaDesde",fechaDesde);
            map.put("fechaHasta",fechaHasta);
            map.put("tipooperacion", paramTipooperacion);
            map.put("estado", paramEstado);
            map.put("verificador", paramVerificador);
            StreamResource.StreamSource source = new StreamResource.StreamSource() {
                @Override
                public InputStream getStream() {
                    byte[] b = null;
                    try {
                        try {
                            b = JasperRunManager.runReportToPdf(getClass()
                                    .getClassLoader()
                                    .getResourceAsStream(getRuta()), 
                                    map, dBConnection.getConnection());
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(VerificacionForm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (JRException ex) {
                       ex.printStackTrace();
                    }

                    return new ByteArrayInputStream(b);
                }

                private String getRuta() {
                    String ruta="";
                    if(cmbTipooperacion.getValue().getDescripcion().contains("PAGARE")) ruta="/repVerificacionPagare.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("ESTADO DE CUENTA")) ruta="/repVerificacionEstado.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("AHORRO PROGRAMADO")) ruta="/repVerificacionContratoAhorro.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("CONTRATO DE AHORRO")) ruta="/repVerificacionContratoAhorro.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("JURIDICO")) ruta="/repVerificacionFondoJuridico.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("RENUNCIA")) ruta="/repVerificacionRenuncia.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("INGRESO DE SOCIO")) ruta="/repVerificacionIngresoSocio.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("INGRESOS POR CAJA")) ruta="/repVerificacionIngresoCaja.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().equals("REPROGRAMACION")) ruta="/repVerificacionReprogramacion.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("CONCILIACIONES BANCARIAS")) ruta="/repVerificacionConciliaciones.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("DOCUMENTOS DE CIERRE")) ruta="/repVerificacionDocumentosCierre.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("LIQUIDACION DE IVA")) ruta="/repVerificacionLiquidacionIVA.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("PRODUCCION DE PROMOTORES")) ruta="/repVerificacionProduccionPromotores.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("GRATIFICACION POR PRODUCCION")) ruta="/repVerificacionHorasExtra.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("DOCUMENTOS DE RRHH")) ruta="/repVerificacionDocumentosRRHH.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("CHEQUES")) ruta="/repVerificacionCheque.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("ARQUEO DE LEGAJOS")) ruta="/repVerificacionArqueoLegajos.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("REEMBOLSO DE AHORRO")) ruta="/repVerificacionReembolsoAhorro.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("FALLECIDOS")) ruta="/repVerificacionFallecidos.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE PRESTAMO")) ruta="/repVerificacionLegajoPrestamo.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().contains("LEGAJO DE REPROGRAMACION")) ruta="/repVerificacionLegajoReprogramacion.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().equals("PRESTAMO")) ruta="/repVerificacionPrestamo.jasper";
                    else if(cmbTipooperacion.getValue().getDescripcion().equals("LIBRO")) ruta="/repVerificacionLibroCompras.jasper";
                    return ruta;
                }
            };
            StreamResource resource = new StreamResource(source, "repVerificacionResumen.pdf");
            resource.setMIMEType("application/pdf");
            resource.setCacheTime(0);
            getUI().getPage().open(resource, "_blank", false);            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
        
}
