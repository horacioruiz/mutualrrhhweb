/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.formularios.DependenciaForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class DependenciaVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Dependencia";
    Grid<Dependencia> grillaDependencia = new Grid<>(Dependencia.class);
    TextField txtfFiltro = new TextField("Filtro");
    DependenciaDao dependenciaDao = ResourceLocator.locate(DependenciaDao.class);
    List<Dependencia> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    DependenciaForm dependenciaForm = new DependenciaForm();
    
    public DependenciaVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        dependenciaForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaDependencia.asSingleSelect().clear();
            dependenciaForm.setDependencia(new Dependencia());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaDependencia, dependenciaForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaDependencia, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = dependenciaDao.listaDependencia();
            grillaDependencia.setItems(lista);
            grillaDependencia.removeAllColumns();
            grillaDependencia.addColumn(Dependencia::getId).setCaption("Id Dependencia");
            grillaDependencia.addColumn(Dependencia::getDescripcion).setCaption("Descripción");
            grillaDependencia.setSizeFull();
            grillaDependencia.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    dependenciaForm.setVisible(false);
                } else {
                    dependenciaForm.setDependencia(e.getValue());
                }
            });
            
            dependenciaForm.setGuardarListener(r ->{
                grillaDependencia.clearSortOrder();
                lista = dependenciaDao.listaDependencia();
                grillaDependencia.setItems(lista);
            });
            dependenciaForm.setBorrarListener(r ->{
                grillaDependencia.clearSortOrder();
            });
            dependenciaForm.setCancelarListener(r ->{
                grillaDependencia.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = dependenciaDao.getListDependencia(value);
           grillaDependencia.setSizeFull();
           grillaDependencia.setItems(lista);
           grillaDependencia.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
    
     
}
