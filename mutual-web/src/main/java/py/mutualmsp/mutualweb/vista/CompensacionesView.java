package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.simple.JSONArray;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;

import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;
import py.mutualmsp.mutualweb.formularios.CompensacionesForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 4/7/2016.
 */
public class CompensacionesView extends CssLayout implements View {

    public static final String VIEW_NAME = "Compensaciones";
//    private TextArea comentarios = new TextArea("Agregar nuevo comentario");
    final Image image = new Image("Imagen");
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<String> filterAprobado = new ComboBox<>();
    TextField filter = new TextField();
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    Button btnSearch = new Button();
    // Create upload stream
    FileOutputStream fos = null;
    Button btnImprimir = new Button("");

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    ParametroDao formularioController = ResourceLocator.locate(ParametroDao.class);
    LicenciasDao licenciasController = ResourceLocator.locate(LicenciasDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();
    //DUpload upload = new DUpload("Subir Archivo", this::fileReceived);
    Grid<LicenciasCompensar> gridLog = new Grid<>(LicenciasCompensar.class);
    Grid<LicenciasCompensar> gridLogProduccion = new Grid<>(LicenciasCompensar.class);
    Grid<VacacionesDetalle> gridLogDetalle = new Grid<>(VacacionesDetalle.class);
    LicenciasCompensarDao solicitudDetalleController = ResourceLocator.locate(LicenciasCompensarDao.class);
    LicenciasDao solicitudController = ResourceLocator.locate(LicenciasDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    VacacionesDao vacacionController = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionDetalleController = ResourceLocator.locate(VacacionesDetalleDao.class);

    CompensacionesForm compensacionesForm = new CompensacionesForm();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    Label labelDescripcion = new Label();
    Label labelUrl = new Label();
    public long idTicket;
    private Button guardar = new Button("Aprobar");
    private Button cancelar = new Button("Cerrar");
    private Binder<LicenciasCompensar> binder = new Binder<>(LicenciasCompensar.class);

//    Services ejb = ResourceLocator.locate(Services.class);
//    Map<Long, Funcionario> mapFuncionario = new HashMap<Long, Funcionario>();
//    EnviarMail enviarMailEJB = ResourceLocator.locate(EnviarMail.class);
    public CompensacionesView() {
        //VerticalLayout verticalLayout = createForm();
        HorizontalLayout horizontalLayout = createHorizontalLayout();
        //setContent(verticalLayout);
        setSizeFull();
        addStyleName("crud-view");
//        setCaption("Comentarios");
        //setModal(true);
        //center();

        List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
        List<Dependencia> listDependencia = new ArrayList<>();
        List<Funcionario> listFunc = new ArrayList<>();
        for (Dependencia dependencia : depen) {
            listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                } 
        }
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getId(), funcio);
        }

        //gridLog.setCaption("Compensaciones");
        gridLogDetalle.setVisible(false);

        guardar.addClickListener(cl -> {
            try {
                save();
            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
        compensacionesForm.setVisible(false);
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            filterFuncionario.setItems(funcionarioController.listaFuncionario());
        } else {
            filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            filterFuncionario.setEnabled(false);
            filterFuncionario.setValue(UserHolder.get().getIdfuncionario());
        }
        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnSearch.setIcon(VaadinIcons.SEARCH);
        btnImprimir.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnImprimir.setIcon(VaadinIcons.PRINT);
        filter.setPlaceholder("Filtre código");
        filter.setWidth(5f, TextField.UNITS_EM);
        filterAprobado.setWidth(8f, TextField.UNITS_EM);
        fechaDesde.setWidth(8f, TextField.UNITS_EM);
        fechaHasta.setWidth(8f, TextField.UNITS_EM);
        filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

        fechaDesde.setPlaceholder("Desde");
        fechaHasta.setPlaceholder("Hasta");

        fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
        fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

        List<String> listEstados = new ArrayList<>();
        listEstados.add("SI");
        listEstados.add("NO");
        filterAprobado.setItems(listEstados);
        filterAprobado.setPlaceholder("Cumple");
        filterFuncionario.setPlaceholder("Filtre Funcionario");

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        gridLog.setSizeFull();

        HorizontalLayout horizontalLayoutHere = new HorizontalLayout();
        horizontalLayoutHere.addComponents(gridLog, compensacionesForm);
        horizontalLayoutHere.setSizeFull();
        horizontalLayoutHere.setExpandRatio(gridLog, 6F);

        verticalLayout.addComponent(horizontalLayoutHere);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(horizontalLayoutHere, 1);
        verticalLayout.addStyleName("crud-main-layout");
//        verticalLayout.setExpandRatio(gridLog, 6F);

        btnSearch.addClickListener(e -> poblarGrillaLogs(0l));

        btnImprimir.addClickListener(e -> {
            mostrarListaPDF();
            //Page.getCurrent().setLocation("http://www.example.com");

        });

        gridLog.setItems(new ArrayList<>());
        gridLog.removeAllColumns();
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        gridLog.addColumn(e -> {
            return e.getLicencia() == null ? "" : e.getLicencia().getId();
        }).setCaption("COD");
        gridLog.addColumn(e -> {
            return e.getFechacompensar() == null ? "--" : e.getLicencia().getFuncionario().getNombreCompleto();
        }).setCaption("FUNCIONARIO");
        gridLog.addColumn(e -> {
            return e.getFechacompensar() == null ? "--" : simpleDateFormat.format(e.getFechacompensar());
        }).setCaption("FECHA");
        gridLog.addColumn(e -> {
            return e.getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getHoraini());
        }).setCaption("HS INICIO");
        gridLog.addColumn(e -> {
            return e.getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getHorafin());
        }).setCaption("HS FIN");
        gridLog.addColumn(e -> {
            return recuperarHoraSalida(e.getLicencia().getFuncionario(), e.getFechacompensar());
        }).setCaption("HS SALIDA");
        gridLog.addComponentColumn(e -> {
            String basepath = VaadinService.getCurrent()
                    .getBaseDirectory().getAbsolutePath();
            if (recuperarSiCumple(e.getLicencia().getFuncionario(), e.getFechacompensar(), e.getHorafin()).equals("SI")) {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/good.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            } else {
                FileResource resource = new FileResource(new File(basepath
                        + "/WEB-INF/images/wrong.png"));
                Image imagen = new Image("Image from file", resource);
                imagen.setWidth(2, Sizeable.Unit.EM);
                return imagen;
            }
        }).setCaption("CUMPLE");


        /*gridLog.addColumn(e -> {
                    return e.getObservacion() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");*/
 /*gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
                }).setCaption("1RA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado2() == null ? "" : e.getLicencia().getEncargado2().getNombreCompleto();
                }).setCaption("2DA APROBACION");*/
        gridLog.addColumn(e
                -> {
            return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
        }
        ).setCaption(
                "RRHH");
        gridLog.addColumn(e
                -> {
            return e.getObservacion();
        }
        ).setCaption(
                "TAREA");
//        if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
        gridLog.addComponentColumn(this::buildConfirmButton).setCaption("Modificar");
//        }
        /*if (solicitudDetalle.get(0).getLicencia().getCantdia() != null && solicitudDetalle.get(0).getLicencia().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getCantdia();
                    }).setCaption("CANT DIA");
                }
                if (solicitudDetalle.get(
                        0).getLicencia().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getLicencia().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }*/
//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");

        gridLog.setSizeFull();

        gridLogProduccion.setVisible(
                false);
        gridLogDetalle.setVisible(
                false);
        gridLog.setVisible(
                true);

        compensacionesForm.setGuardarListener(r -> {
            gridLog.clearSortOrder();
            poblarGrillaLogs(0l);
        });

        compensacionesForm.setBorrarListener(r -> {
            gridLog.clearSortOrder();
            gridLog.setSizeFull();
        });

        compensacionesForm.setCancelarListener(r -> {
            gridLog.clearSortOrder();
            poblarGrillaLogs(0l);
        });
        //verticalLayout.addComponent(labelTotalizador);
        //verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
        //HorizontalLayout horizontalLayout1 = crearSegundoGrid();
        //horizontalLayout1.setSizeFull();
        verticalLayout.addStyleName("crud-main-layout");
        addComponent(verticalLayout);

        Licencias solicitud = new Licencias();
        solicitud.setId(24l);
        setIdTicket(solicitud);
    }

    private void poblarGrillaLogs(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        //if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
        try {
            System.out.println("poblarGrillaLogs()");
            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorFechasHere(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getId(), filter.getValue().equalsIgnoreCase("") ? null : filter.getValue(), "licencia_compensar");
            List<LicenciasCompensar> listSolicitud = new ArrayList<>();
            try {
                if (filterAprobado.getValue() != null || !filterAprobado.getValue().equalsIgnoreCase("")) {
                    if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("SI")) {
                        for (LicenciasCompensar licenciasCompensar : solicitudDetalle) {
                            if (recuperarSiCumple(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar(), licenciasCompensar.getHorafin()).equals("SI")) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else {
                        for (LicenciasCompensar licenciasCompensar : solicitudDetalle) {
                            if (recuperarSiCumple(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar(), licenciasCompensar.getHorafin()).equals("NO")) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    }
                } else {
                    listSolicitud = solicitudDetalle;
                }
            } catch (Exception e) {
                listSolicitud = solicitudDetalle;
            } finally {
            }

//                setCaption(solicitudDetalle.get(0).getLicencia().getFuncionario().getNombreCompleto() + "      |      Inicio: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechaini()) + " - Fin: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechafin()));
//                setCaption(solicitudDetalle.get(0).getLicencia().getNombrefuncionario() + " | " + solicitudDetalle.get(0).getLicencia().getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFuncionario().getFechaingreso()));

            /*if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                guardar.setVisible(true);
            } else {
                guardar.setVisible(false);
            }*/
            gridLog.clearSortOrder();
            if (listSolicitud.isEmpty() || listSolicitud.size() == 0) {
                gridLog.setItems(new ArrayList<>());
            } else {
                gridLog.setItems(listSolicitud);
            }
            //gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());

            gridLog.removeAllColumns();
            gridLog.addColumn(e -> {
                return e.getLicencia() == null ? "" : e.getLicencia().getId();
            }).setCaption("COD");
            gridLog.addColumn(e -> {
                return e.getFechacompensar() == null ? "--" : e.getLicencia().getFuncionario().getNombreCompleto();
            }).setCaption("FUNCIONARIO");
            gridLog.addColumn(e -> {
                return e.getFechacompensar() == null ? "--" : simpleDateFormat.format(e.getFechacompensar());
            }).setCaption("FECHA");
            gridLog.addColumn(e -> {
                return e.getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getHoraini());
            }).setCaption("HS INICIO");
            gridLog.addColumn(e -> {
                return e.getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getHorafin());
            }).setCaption("HS FIN");
            gridLog.addColumn(e -> {
                return recuperarHoraSalida(e.getLicencia().getFuncionario(), e.getFechacompensar());
            }).setCaption("HS SALIDA");
            gridLog.addComponentColumn(e -> {
                String basepath = VaadinService.getCurrent()
                        .getBaseDirectory().getAbsolutePath();
                if (recuperarSiCumple(e.getLicencia().getFuncionario(), e.getFechacompensar(), e.getHorafin()).equals("SI")) {
                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/good.png"));
                    Image imagen = new Image("Image from file", resource);
                    imagen.setWidth(2, Sizeable.Unit.EM);
                    return imagen;
                } else {
                    FileResource resource = new FileResource(new File(basepath
                            + "/WEB-INF/images/wrong.png"));
                    Image imagen = new Image("Image from file", resource);
                    imagen.setWidth(2, Sizeable.Unit.EM);
                    return imagen;
                }
            }).setCaption("CUMPLE");


            /*gridLog.addColumn(e -> {
                    return e.getObservacion() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");*/
 /*gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
                }).setCaption("1RA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado2() == null ? "" : e.getLicencia().getEncargado2().getNombreCompleto();
                }).setCaption("2DA APROBACION");*/
            gridLog.addColumn(e
                    -> {
                return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
            }
            ).setCaption(
                    "RRHH");
            gridLog.addColumn(e
                    -> {
                return e.getObservacion();
            }
            ).setCaption(
                    "TAREA");
//            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            gridLog.addComponentColumn(this::buildConfirmButton).setCaption("Modificar");
//            }
            /*if (solicitudDetalle.get(0).getLicencia().getCantdia() != null && solicitudDetalle.get(0).getLicencia().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getCantdia();
                    }).setCaption("CANT DIA");
                }
                if (solicitudDetalle.get(
                        0).getLicencia().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getLicencia().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }*/
//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");

            gridLog.setSizeFull();

            gridLogProduccion.setVisible(
                    false);
            gridLogDetalle.setVisible(
                    false);
            gridLog.setVisible(
                    true);

//            gridLog.asSingleSelect().addValueChangeListener(e -> {
//                if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10 || usuario.getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
//                    if (e.getValue() == null) {
//                        compensacionesForm.setVisible(false);
//                        Notification.show("Atención", "Debes seleccionar un detalle para editar.", Notification.Type.HUMANIZED_MESSAGE);
//                    } else {
//                        if (recuperarSiCumple(e.getValue().getLicencia().getFuncionario(), e.getValue().getFechacompensar(), e.getValue().getHorafin()).equals("SI")) {
//                            Notification.show("Atención", "Horario cumplido, no es posible editar.", Notification.Type.HUMANIZED_MESSAGE);
//                        } else {
//                            compensacionesForm.setFuncionario(e.getValue());
//                            compensacionesForm.setVisible(true);
//                        }
//
//                    }
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        //}
    }

    private void poblarGrillaLogs2(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
            try {
                System.out.println("poblarGrillaLogs()");
                List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
//                setCaption(solicitudDetalle.get(0).getLicencia().getFuncionario().getNombreCompleto() + "      |      Inicio: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechaini()) + " - Fin: " + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFechafin()));
                setCaption(solicitudDetalle.get(0).getLicencia().getNombrefuncionario() + " | " + solicitudDetalle.get(0).getLicencia().getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(solicitudDetalle.get(0).getLicencia().getFuncionario().getFechaingreso()));

                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                    guardar.setVisible(true);
                } else {
                    guardar.setVisible(false);
                }
                //gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());
                gridLog.setItems(solicitudDetalle);
                gridLog.removeAllColumns();
                gridLog.addColumn(e -> {
                    return e.getLicencia() == null ? "" : e.getLicencia().getId();
                }).setCaption("COD");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFechaini() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechaini());
                }).setCaption("FEC INICIO");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFechafin() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechafin());
                }).setCaption("FEC FIN");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getLicencia().getHoraini());
                }).setCaption("HS INICIO");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getLicencia().getHorafin());
                }).setCaption("HS FIN");
                gridLog.addColumn(e -> {
                    return recuperarHoraSalida(e.getLicencia().getFuncionario(), e.getFechacompensar());
                }).setCaption("HS SALIDA");
                gridLog.addComponentColumn(e -> {
                    String basepath = VaadinService.getCurrent()
                            .getBaseDirectory().getAbsolutePath();
                    if (recuperarSiCumple(e.getLicencia().getFuncionario(), e.getFechacompensar(), e.getHorafin()).equals("SI")) {
                        FileResource resource = new FileResource(new File(basepath
                                + "/WEB-INF/images/good.png"));
                        Image imagen = new Image("Image from file", resource);
                        imagen.setWidth(2, Sizeable.Unit.EM);
                        return imagen;
                    } else {
                        FileResource resource = new FileResource(new File(basepath
                                + "/WEB-INF/images/wrong.png"));
                        Image imagen = new Image("Image from file", resource);
                        imagen.setWidth(2, Sizeable.Unit.EM);
                        return imagen;
                    }
                }).setCaption("CUMPLE");
                /*gridLog.addColumn(e -> {
                    return e.getObservacion() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");
                gridLog.addColumn(e -> {
                    return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
                }).setCaption("ENCARGADO");*/
                gridLog.addColumn(e -> {
                    return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
                }).setCaption("RRHH");
                gridLog.addColumn(e -> {
                    return e.getObservacion();
                }).setCaption("TAREA");

//                if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                gridLog.addComponentColumn(this::buildConfirmButton).setCaption("Modificar");
//                }
                /*if (solicitudDetalle.get(0).getLicencia().getCantdia() != null && solicitudDetalle.get(0).getLicencia().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getLicencia().getCantdia();
                    }).setCaption("CANT DIA");
                }
                if (solicitudDetalle.get(0).getLicencia().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getLicencia().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }*/
//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");
                gridLog.setSizeFull();
                gridLogProduccion.setVisible(false);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void validate() {
        boolean savedEnabled = true;
//        if (comentarios == null || comentarios.isEmpty()) {
//            savedEnabled = false;
//        }
        guardar.setEnabled(savedEnabled);
    }

    private HorizontalLayout createHorizontalLayout() {
        //nuevaLicencias.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        //editarLicencias.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        //imprimirLicencias.addStyleName(MaterialTheme.BUTTON_ROUND);
        //aceptarConformidad.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        //aceptarConformidad.setIcon(FontAwesome.PARAGRAPH);
        //cerrarTicket.addStyleName(MaterialTheme.BUTTON_ROUND + " " + ValoTheme.BUTTON_DANGER);
        //cerrarTicket.setIcon(FontAwesome.EDIT);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        //verticalLayoutIzquierda.addComponent(layoutIzquierdo);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filterFuncionario);
        layoutFuncEstado.setExpandRatio(filterFuncionario, 2);
        layoutFuncEstado.addComponent(filterAprobado);
        layoutFuncEstado.setExpandRatio(filterAprobado, 2);
        layoutFuncEstado.addComponent(filter);
        layoutFuncEstado.setExpandRatio(filter, 2);
        layoutFuncEstado.addComponent(fechaDesde);
        layoutFuncEstado.setExpandRatio(fechaDesde, 2);
        layoutFuncEstado.addComponent(fechaHasta);
        layoutFuncEstado.setExpandRatio(fechaHasta, 2);
        layoutFuncEstado.addComponent(btnSearch);
        layoutFuncEstado.setExpandRatio(btnSearch, 2);
        layoutFuncEstado.addComponent(btnImprimir);
        layoutFuncEstado.setExpandRatio(btnImprimir, 2);
        verticalLayoutIzquierda.addComponent(layoutFuncEstado);

        layout.addComponent(verticalLayoutIzquierda);

        /*layout.addComponent(cerrarTicket);
        layout.addComponent(aceptarConformidad);
        layout.addComponent(nuevaLicencias);
        layout.addComponent(editarLicencias);
        layout.addComponent(imprimirLicencias);*/
        layout.setStyleName("top-bar");
        return layout;
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
//        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        ///layout.addComponent(upload);
        //comentarios.setNullRepresentation("");
//        comentarios.setWidth("100%");
//        comentarios.setHeight("35%");
        gridLog.setHeight("500px");
        //layout.addComponent(labelDescripcion);
        layout.addComponent(gridLog);
        layout.addComponent(gridLogDetalle);
        layout.addComponent(gridLogProduccion);
        HorizontalLayout hupload = new HorizontalLayout();
        layout.addComponent(hupload);
        //layout.addComponent(labelUrl);
//        layout.addComponent(comentarios);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(guardar);
        horizontalButton.addComponent(cancelar);
//        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private void save() {
        try {
            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(this.idTicket);
            Licencias solicitud = solicitudDetalle.get(0).getLicencia();
            if (solicitud.getEncargado() == null) {
                solicitud.setEncargado(UserHolder.get().getIdfuncionario());
                solicitudController.guardarLicencias(solicitud);
            } else {
                long idEncargado1 = solicitud.getEncargado().getId();
                long idEncargado2 = UserHolder.get().getIdfuncionario().getId();
                System.out.println(idEncargado1 + " | " + idEncargado2);
                if (idEncargado1 == idEncargado2) {
                    Notification.show("Otro encargado debe aprobar la solicitud.");
                } else {
                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                    solicitudController.guardarLicencias(solicitud);
                }
            }
        } catch (Exception e) {
        } finally {
        }

    }

    public void setComentarios(TextArea comentarios) {
//        this.comentarios = comentarios;
    }

    public void setIdTicket(Licencias solicitud) {
        Licencias soli = solicitudController.listarPorId(solicitud.getId());
        this.idTicket = solicitud.getId();
        poblarGrillaLogs(idTicket);
        /*if (!soli.getParametro().getDescripcion().equalsIgnoreCase("licencia_compensar")) {
            this.idTicket = solicitud.getId();
            poblarGrillaLogs(idTicket);
        } else {
            this.idTicket = solicitud.getId();
            poblarGrillaDetalle2(solicitud);
        }*/
    }
//    public void setIdTicket(long idTicket) {
//        this.idTicket = idTicket;
//        poblarGrillaLogs(idTicket);
//    }

    void setListaLicencia(List<Licencias> array) {
        List<LicenciasCompensar> listLicenciasCompensar = new ArrayList<>();
        for (Licencias solicitud : array) {
            listLicenciasCompensar.addAll(solicitudDetalleController.listarPorIdLicencia(solicitud.getId()));
        }
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
//        List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
        try {
            System.out.println("poblarGrillaLogs()");
//            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(id);
//            setCaption(array.get(0).getNombrefuncionario() + " | " + array.get(0).getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(array.get(0).getFuncionario().getFechaingreso()));
//                gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getId() + " )- " + solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());
            gridLog.setItems(listLicenciasCompensar);
            gridLog.removeAllColumns();
            gridLog.addColumn(e -> {
                return e.getLicencia() == null ? "" : e.getLicencia().getId();
            }).setCaption("COD");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getLicencia().getFechaini());
            }).setCaption("FEC INICIO");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getLicencia().getFechafin());
            }).setCaption("FEC FIN");
            gridLog.addColumn(e -> {
                return e.getLicencia().getCantidad();
            }).setCaption("CANT DIA");
            gridLog.addColumn(e -> {
                return e.getLicencia().getEncargado() == null ? "" : e.getLicencia().getEncargado().getNombreCompleto();
            }).setCaption("ENCARGADO");
            gridLog.addColumn(e -> {
                return e.getLicencia().getFuncrrhh() == null ? "" : e.getLicencia().getFuncrrhh().getNombreCompleto();
            }).setCaption("RRHH");
            gridLog.setSizeFull();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void poblarGrillaDetalle(Licencias solicitud) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdLicencia(solicitud.getId()) != null) {
            try {
                List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitud.getId());
                setCaption("solicitud de producción".toUpperCase());
                setCaption(solicitud.getAreafunc().toUpperCase() + " | " + solicitud.getCargofunc().toUpperCase());

                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && solicitudDetalle.get(0).getLicencia().getEncargado() == null || solicitudDetalle.get(0).getLicencia().getEncargado2() == null) {
                    guardar.setVisible(true);
                } else {
                    guardar.setVisible(false);
                }
                try {
                    gridLog.setCaption("MOTIVO RECHAZO: " + solicitud.getRechazo().toUpperCase());
                } catch (Exception e) {
                    gridLog.setCaption("");
                } finally {
                }

                gridLogProduccion.setItems(solicitudDetalle);
                gridLogProduccion.removeAllColumns();
                gridLogProduccion.addColumn(e -> {
                    return e.getId();
                }).setCaption("CODIGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getNombrefuncionario();
                }).setCaption("NOMBRE Y APELLIDO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getCargofunc().toUpperCase();
                }).setCaption("CARGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getDescripcion().toUpperCase();
                }).setCaption("DESCRIPCION ACTIVIDAD");
                gridLogProduccion.addColumn(e -> {
                    return e.getLicencia().getFechacreacion() == null ? "--" : simpleDateFormat.format(e.getLicencia().getFechacreacion());
                }).setCaption("FECHA");

//                gridLog.addColumn(e -> {
//                    return e.getLicencia() == null ? "" : e.getLicencia().get;
//                }).setCaption("Descripción");
                gridLogProduccion.setSizeFull();
                gridLogProduccion.setVisible(true);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String recuperarHoraSalida(Funcionario func, Date fechacompensar) {
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<Marcaciones> listaMarcaciones = marcacionesController.getByFilterHoraExtraHere(func, fechacompensar, fechacompensar, null, 0l);
        if (listaMarcaciones.isEmpty()) {
            return "00:00";
        } else {
            return simpleDateFormatHora.format(listaMarcaciones.get(0).getOutmarcada());
        }
    }

    private String recuperarSiCumple(Funcionario func, Date fechacompensar, Timestamp horafin) {
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<Marcaciones> listaMarcaciones = marcacionesController.getByFilterHoraExtraHere(func, fechacompensar, fechacompensar, null, 0l);

        if (listaMarcaciones.isEmpty()) {
            return "NO";
        } else {
            try {
                int minMarcada = ((listaMarcaciones.get(0).getOutmarcada().getHours() * 60) + listaMarcaciones.get(0).getOutmarcada().getMinutes());
                int minAquedar = ((horafin.getHours() * 60) + horafin.getMinutes());

                if (minAquedar <= minMarcada) {
                    return "SI";
                } else {
                    return "NO";
                }
            } catch (Exception e) {
                return "NO";
            } finally {
            }
        }
    }

    private Button buildConfirmButton(LicenciasCompensar p) {
        Button button = new Button(VaadinIcons.CHECK_SQUARE_O);
        if (recuperarSiCumple(p.getLicencia().getFuncionario(), p.getFechacompensar(), p.getHorafin()).equals("SI")) {
            button.setEnabled(false);
            button.setVisible(false);
        } else {
            button.setEnabled(true);
            button.setVisible(true);
        }
        /*if (p.getAprobado() == 0) {*/

 /*} else {
            button.setEnabled(false);
            button.setVisible(false);
        }*/
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> aprobarSolicitud(p));
        return button;
    }

    private void aprobarSolicitud(LicenciasCompensar p) {
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            if (p == null) {
                compensacionesForm.setVisible(false);
                Notification.show("Atención", "Debes seleccionar un detalle para editar.", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                if (recuperarSiCumple(p.getLicencia().getFuncionario(), p.getFechacompensar(), p.getHorafin()).equals("SI")) {
                    Notification.show("Atención", "Horario cumplido, no es posible editar.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    compensacionesForm.setFuncionario(p);
                    compensacionesForm.setVisible(true);
                }

            }
        } else {
            Notification.show("Atención", "No posee los permisos necesarios para modificar el horario de compensación.", Notification.Type.HUMANIZED_MESSAGE);
        }
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private void mostrarListaPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarCompensaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
//        pSQL.put("subRepNomFun", nombreFunc);
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "compensaciones";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    Logger.getLogger(CompensacionesView.class.getName()).log(Level.SEVERE, null, ex);
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "compensaciones";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarCompensaciones() {
        String pattern = "dd-MM-yyyy";
        JSONArray jsonArrayDato = new JSONArray();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        //if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
        try {
            List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorFechasHere(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getId(), filter.getValue().equalsIgnoreCase("") ? null : filter.getValue(), "licencia_compensar");
            List<LicenciasCompensar> listSolicitud = new ArrayList<>();
            try {
                if (filterAprobado.getValue() != null || !filterAprobado.getValue().equalsIgnoreCase("")) {
                    if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("SI")) {
                        for (LicenciasCompensar licenciasCompensar : solicitudDetalle) {
                            if (recuperarSiCumple(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar(), licenciasCompensar.getHorafin()).equals("SI")) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else {
                        for (LicenciasCompensar licenciasCompensar : solicitudDetalle) {
                            if (recuperarSiCumple(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar(), licenciasCompensar.getHorafin()).equals("NO")) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    }
                } else {
                    listSolicitud = solicitudDetalle;
                }
            } catch (Exception e) {
                listSolicitud = solicitudDetalle;
            } finally {
            }
            for (LicenciasCompensar licenciasCompensar : listSolicitud) {
                org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
                jsonObj.put("codigo", licenciasCompensar.getLicencia().getId());
                jsonObj.put("funcionario", licenciasCompensar.getLicencia().getFuncionario().getNombreCompleto());
                jsonObj.put("fecha", simpleDateFormat.format(licenciasCompensar.getFechacompensar()));
                jsonObj.put("fechaemision", simpleDateFormat.format(new Date()));
                jsonObj.put("inicio", formatterHora.format(licenciasCompensar.getHoraini()));
                jsonObj.put("fin", formatterHora.format(licenciasCompensar.getHorafin()));
                jsonObj.put("salida", recuperarHoraSalida(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar()));
                if (recuperarSiCumple(licenciasCompensar.getLicencia().getFuncionario(), licenciasCompensar.getFechacompensar(), licenciasCompensar.getHorafin()).equals("SI")) {
                    jsonObj.put("cumple", "SI");
                } else {
                    jsonObj.put("cumple", "NO");
                }
                jsonObj.put("tarea", licenciasCompensar.getObservacion());
                jsonArrayDato.add(jsonObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArrayDato;
    }

}
