/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.ProfesionDao;
import py.mutualmsp.mutualweb.entities.Profesion;
import py.mutualmsp.mutualweb.formularios.ProfesionForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class ProfesionVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Profesión";
    Grid<Profesion> grillaProfesion = new Grid<>(Profesion.class);
    TextField txtfFiltro = new TextField("Filtro");
    ProfesionDao profesionDao = ResourceLocator.locate(ProfesionDao.class);
    List<Profesion> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    ProfesionForm profesionForm = new ProfesionForm();
    
    
    public ProfesionVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        profesionForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro ,btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaProfesion.asSingleSelect().clear();
            profesionForm.setProfesion(new Profesion());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaProfesion, profesionForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaProfesion, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = profesionDao.listaProfesion();
            grillaProfesion.setItems(lista);
            grillaProfesion.removeAllColumns();
            grillaProfesion.addColumn(Profesion::getId).setCaption("Id Profesion");
            grillaProfesion.addColumn(Profesion::getDescripcion).setCaption("Descripcion");
            grillaProfesion.addColumn(activo -> { 
                return activo.getActivo() == true ? "SI" : "NO"  ;}).setCaption("Activo");
            grillaProfesion.setSizeFull();
            grillaProfesion.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    profesionForm.setVisible(false);
                } else {
                    profesionForm.setProfesion(e.getValue());
                    profesionForm.setViejo();
                }
            });
            
            profesionForm.setGuardarListener(ro ->{
                grillaProfesion.clearSortOrder();
                lista = profesionDao.listaProfesion();
                grillaProfesion.setItems(lista);
            });
            profesionForm.setBorrarListener(ro ->{
                grillaProfesion.clearSortOrder();
            });
            profesionForm.setCancelarListener(ro ->{
                grillaProfesion.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = profesionDao.getListProfesion(value);
           grillaProfesion.setSizeFull();
           grillaProfesion.setItems(lista);
           grillaProfesion.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }
  
    
     
}
