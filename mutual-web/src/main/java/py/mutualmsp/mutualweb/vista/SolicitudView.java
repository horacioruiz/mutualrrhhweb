package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.EmpresaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.SolicitudDao;
import py.mutualmsp.mutualweb.dao.SolicitudDetalleDao;
import py.mutualmsp.mutualweb.dao.SolicitudProduccionDetalleDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Empresa;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Solicitud;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;
import py.mutualmsp.mutualweb.formularios.SolicitudForm;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.InputModal;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 23/6/2016.
 */
public class SolicitudView extends CssLayout implements View {

    public static final String VIEW_NAME = "Solicitud";
    //Button recibirMail = new Button("Recibir Mail");
    Button editarSolicitud = new Button("");
    Button imprimirSolicitud = new Button("");
    Button aceptarConformidad = new Button("Aceptar Conformidad");
    Button btnSearch = new Button();
    CheckBox checkForm = new CheckBox("Carga fecha anterior");
    CheckBox checkFormVacas = new CheckBox("Vacaciones menores");
    
//    ConfirmButton eliminarReclamo = new ConfirmButton("Eliminar Reclamo");
    Label labelTotalizador = new Label();

    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();

    Button cerrarTicket = new Button("Cerrar Ticket");
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<Empresa> filterFilial = new ComboBox<>();
    ComboBox<Formulario> filterFormulario = new ComboBox<>();
    ComboBox<Funcionario> filterAsignado = new ComboBox<>();
    Grid<Solicitud> grid = new Grid<>(Solicitud.class);
    SolicitudForm solicitudForm = new SolicitudForm();
    

    int numRowSelected = 0;
    Solicitud spd = new Solicitud();

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    FormularioDao formularioController = ResourceLocator.locate(FormularioDao.class);
    SolicitudDao solicitudController = ResourceLocator.locate(SolicitudDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();

    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionesDetalleDao = ResourceLocator.locate(VacacionesDetalleDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionesDao = ResourceLocator.locate(RotacionesDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    MarcacionesDao marcacionesDao = ResourceLocator.locate(MarcacionesDao.class);

    Button nuevaSolicitud = new Button("");
    private SolicitudDetalleView formSolicitudDetalle = new SolicitudDetalleView();

    TextField filter = new TextField();
    ParametroDao parametroController = ResourceLocator.locate(ParametroDao.class);

    private DateField fechaLog = new DateField();
    private TextArea comentarioLog = new TextArea("Comentarios");
    private Button descargar = new Button("Descargar Archivo");
    String filename;
    byte[] content;
    DecimalFormat decimalFormat = new DecimalFormat("###,###,##0");
    SolicitudDetalleDao solicitudDetalleController = ResourceLocator.locate(SolicitudDetalleDao.class);
    SolicitudProduccionDetalleDao solicitudProduccionDetalleController = ResourceLocator.locate(SolicitudProduccionDetalleDao.class);
    Solicitud solicitudSeleccionado;
    List<Solicitud> lista = new ArrayList<>();
    ComboBox<String> filterAprobado = new ComboBox<>();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();
    
    EmpresaDao empresaDao =  ResourceLocator.locate(EmpresaDao.class);
    Map<String, Empresa> mapeoEmpresa = new HashMap();
    
    public SolicitudView() {
        try {
            System.out.println("Nueva instancia SolicitudView");
            setSizeFull();
            addStyleName("crud-view");
            HorizontalLayout horizontalLayout = createHorizontalLayout();

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia
                        .getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                }                
            }           
            
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }

            if (UserHolder.get().getIdfuncionario() == null) {
                UserHolder.get().setIdfuncionario(usuarioController.getByUsuario(UserHolder.get().getId()).getIdfuncionario());
            }
            
            filterAprobado.setWidth(10f, TextField.UNITS_EM);
            filterFormulario.setWidth(12f, TextField.UNITS_EM);
            filter.setWidth(8f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);

            btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnSearch.setIcon(VaadinIcons.SEARCH);

            fechaDesde.setPlaceholder("Desde");
            fechaHasta.setPlaceholder("Hasta");

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

//            eliminarReclamo.setVisible(false);
            labelTotalizador.setCaption("Total de registros: 0");

            
            aceptarConformidad.setVisible(false);
            cerrarTicket.setVisible(false);
            editarSolicitud.setVisible(false);
            imprimirSolicitud.setVisible(false);
            filterFuncionario.setVisible(true);
            filterFilial.setVisible(true);
//            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
                    || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                filterFuncionario.setItems(funcionarioController.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            
            filterFormulario.setItems(formularioController.listaFormulario());
            filterFormulario.setPlaceholder("Filtre formularios");
            filter.setPlaceholder("Filtre código");
            filterFormulario.setItemCaptionGenerator(Formulario::getDescripcion);
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            filterFilial.setItemCaptionGenerator(Empresa::getDescripcion);

            filter.addValueChangeListener(e -> limpiarDatos());
            
            filterFilial.setItems(empresaDao.getFiliales());

            List<String> listEstados = new ArrayList<>();
            listEstados.add("APROBADO");
            listEstados.add("RECHAZADO");
            listEstados.add("PENDIENTE");
            filterAprobado.setItems(listEstados);
            filterAprobado.setPlaceholder("Filtre aprobados");
            filterFuncionario.setPlaceholder("Filtre Funcionario");
            filterFilial.setPlaceholder("Filtre Filial");
            filterFormulario.setPlaceholder("Filtre Formulario");
            filterAsignado.setPlaceholder("Filtre Asignado");

            fechaLog.setVisible(false);
            comentarioLog.setVisible(false);

            List<Parametro> listParame = parametroController.listarPorCodigo("cargar_fecha_anterior_solicitud");
            if (listParame.get(0).getValor().equalsIgnoreCase("SI")) {
                checkForm.setValue(true);
            } else {
                checkForm.setValue(false);
            }
            List<Parametro> listParametros = parametroController.listarPorCodigo("cargar_vacaciones_menor");
            if (listParametros.get(0).getValor().equalsIgnoreCase("SI")) {
                checkFormVacas.setValue(true);
            } else {
                checkFormVacas.setValue(false);
            }

            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                checkForm.setVisible(true);
                checkFormVacas.setVisible(true);
            } else {
                checkForm.setVisible(false);
                checkFormVacas.setVisible(false);
            }

//            fechaDesde.addValueChangeListener(e -> findByAll());
//            fechaHasta.addValueChangeListener(e -> findByAll());
            btnSearch.addClickListener(e -> findByAll());

            checkForm.addValueChangeListener(e -> updateFormulario(e.getValue()));
            checkFormVacas.addValueChangeListener(e -> updateFormularioVacaciones(e.getValue()));

            filter.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
//                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                            || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                        if (filter.getValue() != null) {
                            String num = filter.getValue();
                            filterFuncionario.setValue(null);
                            filterFilial.setValue(null);
                            filterAprobado.setValue(null);
                            filterFormulario.setValue(null);
                            fechaDesde.setValue(null);
                            fechaHasta.setValue(null);
                            cargarFormulario(num);
                        }
                    }
                }

                private void cargarFormulario(String num) {
                    grid.clearSortOrder();
                    filter.setValue(num);
                    try {
                        Solicitud s = solicitudController.listarPorIdFuncionario(Long.parseLong(num));
                        if (s.getId() == Long.parseLong(num)) {
                            grid.setItems(s);
                        }
                    } catch (Exception e) {
                        grid.clearSortOrder();
                        grid.setItems(new ArrayList<>());
                    } finally {
                    }
                }
            });

            SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatConHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

//            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
                    || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                lista = solicitudController.listadeSolicitudSinConfirmar();
            } else {
                lista = new ArrayList<>();
            }

            grid.setItems(lista);
            labelTotalizador.setCaption("Total de registros: " + lista.size());
            grid.removeAllColumns();
            grid.addColumn(e -> {
                return e.getId();
            }).setCaption("Cód");
            grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("Aprobado");
            grid.addColumn(e -> {
                return e.getFechaini() == null ? "--" : formatSinHora.format(e.getFechaini());
            }).setCaption("Fecha Inicio");
            grid.addColumn(e -> {
                return e.getFechafin() == null ? "--" : formatSinHora.format(e.getFechafin());
            }).setCaption("Fecha Fin");
//            grid.addComponentColumn(e -> {
//                return e.getConfirmadoIcon();
//            }).setCaption("Confirmado");
            grid.addColumn(e -> {
                return e.getFormulario().getDescripcion(); 
            }).setCaption("Formulario");
            grid.addColumn(e -> {
                return e.getFuncionario() == null ? e.getAreafunc().toString().toUpperCase() : e.getFuncionario().getNombreCompleto();
            }).setCaption("Funcionario");
            grid.addColumn(e -> {
                return e.getObservacion();
            }).setCaption("Observación");
            grid.addColumn(e -> {
                return (formatSinHora.format(e.getFechacreacion()));
            }).setCaption("Fecha Creación");
            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                grid.addComponentColumn(this::buildConfirmButton).setCaption("Confirmar");
            }
            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                grid.addComponentColumn(this::eliminarSolicitud).setCaption("Eliminar");
            }
            grid.setSizeFull();
            grid.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    try {
                        if (numRowSelected >= 0 && spd.getAprobado() == 1) {
                            InputModal inputModal = new InputModal("");
                            inputModal.openInModalPopup("Rechazo", "");
                            inputModal.getOkButton().addClickListener(listener -> {
                                Date fechaHoy = new Date();
                                fechaHoy.setHours(0);
                                fechaHoy.setMinutes(0);
                                fechaHoy.setSeconds(0);
                                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                                Date fechaSolicitud = spd.getFechaini();
                                fechaSolicitud.setHours(0);
                                fechaSolicitud.setMinutes(0);
                                fechaSolicitud.setSeconds(0);
                                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                                if (spd.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                                    VacacionesDetalle vacacionDetalle = vacacionesDetalleDao.listaVacacionesDetallePorIdSolicitud(spd.getId());
                                    long cantData = vacacionDetalle.getCantDia();
                                    vacacionDetalle.setCantDia(0l);
                                    vacacionesDetalleDao.guardar(vacacionDetalle);

                                    long idVacacion = vacacionDetalle.getVacaciones().getId();
                                    Vacaciones vacas = vacacionesDao.listaVacacionesPorId(idVacacion);
                                    vacas.setCantdiatomada(vacas.getCantdiatomada() - cantData);
                                    vacas.setCantdiarestante(vacas.getCantdiarestante() + cantData);
                                    vacacionesDao.guardar(vacas);

                                    List<Marcaciones> listMaracacion = marcacionesController.getBySolicitud(spd.getId());
                                    for (Marcaciones marcaciones : listMaracacion) {
                                        marcaciones.setSolicitud(null);
                                        marcacionesController.guardar(marcaciones);
                                    }
                                } else if (spd.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                                    List<Marcaciones> listMaracacion = marcacionesController.getBySolicitud(spd.getId());
                                    for (Marcaciones marcaciones : listMaracacion) {
                                        marcaciones.setSolicitud(null);
                                        marcacionesController.guardar(marcaciones);
                                    }
                                } else {
                                    try {
                                        List<SolicitudProduccionDetalle> listPD = solicitudProduccionDetalleController.listarPorIdSolicitud(spd.getId());
                                        for (SolicitudProduccionDetalle solicitudProduccionDetalle : listPD) {
                                            List<Marcaciones> listMaracacion = marcacionesController.getBySolicitudProduccion(solicitudProduccionDetalle.getId());
                                            for (Marcaciones marcaciones : listMaracacion) {
                                                marcaciones.setSolicitudProduccionDetalle(null);
                                                marcacionesController.guardar(marcaciones);
                                            }
                                        }
                                    } catch (Exception e) {
                                    } finally {
                                    }
                                }
                                spd.setAprobado(2L);
                                spd.setFuncrrhh(UserHolder.get().getIdfuncionario());
                                spd.setRechazo(inputModal.getText());
                                solicitudController.guardarSolicitud(spd);
                                grid.clearSortOrder();
//                                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                    lista = solicitudController.listadeSolicitudSinConfirmar();
                                } else {
                                    lista = new ArrayList<>();
                                }
                                grid.setItems(lista);
                                inputModal.closePopup();
                            });
                        }
                    } catch (Exception e) {

                    } finally {
                    }

                }
            });

            grid.addItemClickListener(e -> {
                if (e.getItem() != null) {
                    spd = e.getItem();
                    numRowSelected = e.getRowIndex();
                }
            });

            descargar.setVisible(false);

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(horizontalLayout);
            grid.setSizeFull();
            verticalLayout.addComponent(grid);
            verticalLayout.setMargin(true);
            verticalLayout.setSpacing(true);
            verticalLayout.setSizeFull();
            verticalLayout.setExpandRatio(grid, 6F);
            verticalLayout.addComponent(labelTotalizador);
            verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
            HorizontalLayout horizontalLayout1 = crearSegundoGrid();
            horizontalLayout1.setSizeFull();
            verticalLayout.addStyleName("crud-main-layout");
            addComponent(verticalLayout);

            grid.addItemClickListener(listener -> {
                if (listener.getMouseEventDetails().isDoubleClick()) {
                    editarSolicitud.setVisible(false);
                    imprimirSolicitud.setVisible(false);
                    cerrarTicket.setVisible(false);

                    formSolicitudDetalle = new SolicitudDetalleView();
                    long idTicket = listener.getItem().getId();
//                    formSolicitudDetalle.setIdTicket(idTicket);
                    formSolicitudDetalle.setIdTicket(listener.getItem());
                    System.out.println("EL TICKET ES EL # " + idTicket);
                    UI.getCurrent().addWindow(formSolicitudDetalle);
                    formSolicitudDetalle.setVisible(true);
                }
            });
            grid.asSingleSelect().addValueChangeListener(event -> {
                solicitudSeleccionado = event.getValue();
                editarSolicitud.setVisible(true);
                imprimirSolicitud.setVisible(true);
            });
            editarSolicitud.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
                    try {
                        if (solicitudSeleccionado.getAprobado() > 0) {
                            Notification.show("La solicitud ha sido confirmada, no es posible editar.", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            SolicitudForm soliForm = new SolicitudForm();
                            try {
                                soliForm.editarRegistro(solicitudController.listarPorId(solicitudSeleccionado.getId()));
                            } catch (Exception e) {
                                soliForm.editarRegistro(solicitudController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                            } finally {
                            }

                            UI.getCurrent().addWindow(soliForm);
                            soliForm.setVisible(true);

                            soliForm.setCancelListener(reclamo -> {
                                grid.clearSortOrder();
                            });

                            soliForm.setSaveListener(reclamo -> {
                                grid.clearSortOrder();
//                                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                    lista = solicitudController.listadeSolicitudSinConfirmar();
                                } else {
                                    lista = new ArrayList<>();
                                }
                                grid.setItems(lista);
                            });
                        }
                    } catch (Exception e) {
                        SolicitudForm soliForm = new SolicitudForm();
                        try {
                            soliForm.editarRegistro(solicitudController.listarPorId(solicitudSeleccionado.getId()));
                        } catch (Exception ex) {
                            soliForm.editarRegistro(solicitudController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                        } finally {
                        }
                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
                            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                lista = solicitudController.listadeSolicitudSinConfirmar();
                            } else {
                                lista = new ArrayList<>();
                            }
                            grid.setItems(lista);
                        });
                    } finally {
                    }

                }
            });
            imprimirSolicitud.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
//                    try {
//                        if (solicitudSeleccionado.getConfirmado()) {
//                        if (solicitudSeleccionado.getEncargado() != null) {
                    imrpimirPDF();
//                        } else {
//                            Notification.show("La solicitud no ha sido confirmada, no es posible imprimirla.", Notification.Type.HUMANIZED_MESSAGE);
//                        }
//                    } catch (Exception e) {
//                        imrpimirPDF();
//                    } finally {
//                    }

                }
            });

            nuevaSolicitud.addClickListener(clickEvent -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
                if (listFeriado.size() == 0) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    SolicitudForm solicitudForm = new SolicitudForm();
                    UI.getCurrent().addWindow(solicitudForm);

                    solicitudForm.nuevoRegistro();
                    solicitudForm.setVisible(true);
                    solicitudForm.setCancelListener(reclamo -> {
                        //form = null;
                        grid.clearSortOrder();
                    });
                    solicitudForm.setSaveListener(reclamo -> {
                        grid.clearSortOrder();
                        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                                || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                            lista = solicitudController.listadeSolicitudSinConfirmar();
                        } else {
                            lista = new ArrayList<>();
                        }
                        grid.setItems(lista);
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout crearSegundoGrid() {
        HorizontalLayout layout = new HorizontalLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        horizontalLayout.addComponent(descargar);
        verticalLayout.addComponent(horizontalLayout);
        CssLayout expander1 = new CssLayout();
        expander1.setSizeFull();
        expander1.setStyleName("expander");
        verticalLayout.addComponent(expander1);
        verticalLayout.setExpandRatio(expander1, 0.9F);
        verticalLayout.addComponent(fechaLog);
        comentarioLog.setSizeFull();
        verticalLayout.addComponent(comentarioLog);
        verticalLayout.setSpacing(false);

        layout.addComponent(verticalLayout);
        layout.setSpacing(true);
        return layout;
    }

    private void cargarFormulario(String num) {
        grid.clearSortOrder();
        filter.setValue(num);
        try {
            Solicitud s = solicitudController.listarPorIdFuncionario(Long.parseLong(num));
            if (s.getId() == Long.parseLong(num)) {
                grid.setItems(s);
            }
        } catch (Exception e) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
        } finally {
        }
    }

    private void findByAll() {
        if (!filter.getValue().trim().equals("")) {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                if (filter.getValue() != null) {
                    String num = filter.getValue();
                    filterFuncionario.setValue(null);
                    filterFilial.setValue(null);
                    filterAprobado.setValue(null);
                    filterFormulario.setValue(null);
                    fechaDesde.setValue(null);
                    fechaHasta.setValue(null);
                    cargarFormulario(num);
                }
            }
        } else {
            if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
                if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) 
                        || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                    filtroConFecha();
                } else {
                    Notification.show("Mensaje del Sistema", "Fecha hasta debe ser mayor o igual a fecha desde.", Notification.Type.HUMANIZED_MESSAGE);
                }
            } else {
                Notification.show("Mensaje del Sistema", "Fecha desde y fecha hasta no deben quedar vacíos.", Notification.Type.HUMANIZED_MESSAGE);
            }
        }
    }

    private void filtroConFecha() {
        filter.setValue("");
        grid.clearSortOrder();
        //validar idFilial
        Long idfilial = 0l;
        if (filterFuncionario.getValue() != null && filterFilial.getValue() == null) 
            idfilial = filterFuncionario.getValue().getIdFilial();
        else if (filterFuncionario.getValue() == null && filterFilial.getValue() != null)
            idfilial = filterFilial.getValue().getId();
        else 
            idfilial = 0l;
        if (filterFuncionario.getValue() != null) {
            List<Solicitud> listTicket = solicitudController.listadeSolicitudByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
                    filterFormulario.getValue() == null ? null : filterFormulario.getValue().getDescripcion(), UserHolder.get(),
                    DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios , 
                    filterFuncionario.getValue() != null ? filterFuncionario.getValue().getIdFilial() : filterFilial.getValue().getId());
            grid.setItems(listTicket);
            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        } else {
            List<Solicitud> listTicket = solicitudController
                    .listadeSolicitudByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
                    filterFormulario.getValue() == null ? null : filterFormulario.getValue().getDescripcion(), UserHolder.get(), 
                    DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios,idfilial);
            grid.setItems(listTicket);
            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        }
    }

    private HorizontalLayout createHorizontalLayout() {
        nuevaSolicitud.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        nuevaSolicitud.setIcon(VaadinIcons.PLUS_CIRCLE);
        editarSolicitud.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        editarSolicitud.setIcon(VaadinIcons.EDIT);
        imprimirSolicitud.addStyleName(MaterialTheme.BUTTON_ROUND);
        imprimirSolicitud.setIcon(VaadinIcons.PRINT);
        aceptarConformidad.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        aceptarConformidad.setIcon(FontAwesome.PARAGRAPH);
        cerrarTicket.addStyleName(MaterialTheme.BUTTON_ROUND + " " + ValoTheme.BUTTON_DANGER);
        cerrarTicket.setIcon(FontAwesome.EDIT);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        HorizontalLayout layoutIzquierdo = new HorizontalLayout();
        layoutIzquierdo.addComponent(filterFuncionario);
        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        verticalLayoutIzquierda.addComponent(layoutIzquierdo);
        layoutIzquierdo.addComponent(filterFilial);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filterAprobado);
        layoutFuncEstado.setExpandRatio(filterAprobado, 2);
        layoutFuncEstado.addComponent(filterFormulario);
        layoutFuncEstado.setExpandRatio(filterFormulario, 2);
        layoutFuncEstado.addComponent(filter);
        layoutFuncEstado.setExpandRatio(filter, 2);
        layoutFuncEstado.addComponent(fechaDesde);
        layoutFuncEstado.setExpandRatio(fechaDesde, 2);
        layoutFuncEstado.addComponent(fechaHasta);
        layoutFuncEstado.setExpandRatio(fechaHasta, 2);
        layoutFuncEstado.addComponent(btnSearch);
        layoutFuncEstado.setExpandRatio(btnSearch, 2);
        verticalLayoutIzquierda.addComponent(layoutFuncEstado);

        layout.addComponent(verticalLayoutIzquierda);

        layout.addComponent(cerrarTicket);
        layout.addComponent(aceptarConformidad);
        layout.addComponent(nuevaSolicitud);
        layout.setComponentAlignment(nuevaSolicitud, Alignment.TOP_CENTER);
        layout.addComponent(editarSolicitud);
        layout.addComponent(imprimirSolicitud);
        layout.addComponent(checkForm);
        layout.addComponent(checkFormVacas);
//        layout.setSpacing(true);
        layout.setComponentAlignment(checkForm, Alignment.TOP_CENTER);
        layout.setComponentAlignment(checkFormVacas, Alignment.TOP_CENTER);
        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void fileReceived(String filename, String mime, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public void limpiarLog() {
        grid.clearSortOrder();
        comentarioLog.setValue("");
        fechaLog.setValue(null);
        fechaLog.setEnabled(false);
        comentarioLog.setEnabled(false);
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private Button eliminarSolicitud(Solicitud p) {
        Button button = new Button(VaadinIcons.ERASER);
        try {
            if (p.getFuncrrhh().getId() == null) {
                button.setEnabled(true);
                button.setVisible(true);
            } else {
                button.setEnabled(false);
                button.setVisible(false);
            }
        } catch (Exception e) {
            button.setEnabled(true);
            button.setVisible(true);
        } finally {
        }

        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        button.addClickListener(e -> eliminarLicencias(p));
        return button;
    }

    private Button buildConfirmButton(Solicitud p) {
        Button button = new Button(VaadinIcons.CHECK_SQUARE_O);
        if (p.getAprobado() == 0) {
            button.setEnabled(true);
            button.setVisible(true);
        } else {
            button.setEnabled(false);
            button.setVisible(false);
        }
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> aprobarSolicitud(p));
        return button;
    }

    private void cargarVacacionesDisponible(Solicitud s) {
        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(s.getFuncionario().getId());
        for (Vacaciones listado : listVacaciones) {
            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        }
    }

    private void eliminarLicencias(Solicitud spd) {
        try {
//                        if (numRowSelected >= 0) {
            if (spd.getId() >= 0 && spd.getAprobado() == 0) {
                InputModal inputModal = new InputModal("");
                inputModal.openInModalPopup("Rechazo", "");
                inputModal.getOkButton().addClickListener(listener -> {
                    Date fechaHoy = new Date();
                    fechaHoy.setHours(0);
                    fechaHoy.setMinutes(0);
                    fechaHoy.setSeconds(0);
                    java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                    Date fechaSolicitud = spd.getFechaini();
                    fechaSolicitud.setHours(0);
                    fechaSolicitud.setMinutes(0);
                    fechaSolicitud.setSeconds(0);
                    java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                    boolean val = false;
                    if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                        val = true;
                    } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                        val = false;
                    } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                        val = true;
                    }

                    if (val) {
                        if (spd.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                            VacacionesDetalle vacacionDetalle = vacacionesDetalleDao.listaVacacionesDetallePorIdSolicitud(spd.getId());
                            long cantData = vacacionDetalle.getCantDia();
                            vacacionDetalle.setCantDia(0l);
                            vacacionesDetalleDao.guardar(vacacionDetalle);

                            long idVacacion = vacacionDetalle.getVacaciones().getId();
                            Vacaciones vacas = vacacionesDao.listaVacacionesPorId(idVacacion);
                            vacas.setCantdiatomada(vacas.getCantdiatomada() - cantData);
                            vacas.setCantdiarestante(vacas.getCantdiarestante() + cantData);
                            vacacionesDao.guardar(vacas);

                            List<Marcaciones> listMaracacion = marcacionesController.getBySolicitud(spd.getId());
                            for (Marcaciones marcaciones : listMaracacion) {
                                marcaciones.setSolicitud(null);
                                marcacionesController.guardar(marcaciones);
                            }
                        } else if (spd.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                            List<Marcaciones> listMaracacion = marcacionesController.getBySolicitud(spd.getId());
                            for (Marcaciones marcaciones : listMaracacion) {
                                marcaciones.setSolicitud(null);
                                marcacionesController.guardar(marcaciones);
                            }
                        } else {
                            try {
                                List<SolicitudProduccionDetalle> listPD = solicitudProduccionDetalleController.listarPorIdSolicitud(spd.getId());
                                for (SolicitudProduccionDetalle solicitudProduccionDetalle : listPD) {
                                    List<Marcaciones> listMaracacion = marcacionesController.getBySolicitudProduccion(solicitudProduccionDetalle.getId());
                                    for (Marcaciones marcaciones : listMaracacion) {
                                        marcaciones.setSolicitudProduccionDetalle(null);
                                        marcacionesController.guardar(marcaciones);
                                    }
                                }
                            } catch (Exception e) {
                            } finally {
                            }
                        }
                        spd.setAprobado(2L);
                        spd.setFuncrrhh(UserHolder.get().getIdfuncionario());
                        spd.setRechazo(inputModal.getText());
                        solicitudController.guardarSolicitud(spd);
                        grid.clearSortOrder();
//                                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                                || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                            lista = solicitudController.listadeSolicitudSinConfirmar();
                        } else {
                            lista = new ArrayList<>();
                        }
                        grid.setItems(lista);
                        inputModal.closePopup();
                    } else {
                        Notification.show("Mensaje del sistema", "La solicitud generada ya ha sido efectuada.", Notification.Type.ERROR_MESSAGE);
                    }

                });
            }
        } catch (Exception e) {

        } finally {
        }
    }

    private void aprobarSolicitud(Solicitud p) {
        if (p.getEncargado() == null || p.getEncargado2() == null) {
            Notification.show("Mensaje del Sistema", "El/los encargado/s no confirmaron la solicitud", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            ConfirmButton confirmMessage = new ConfirmButton("");
            confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea aprobar la solicitud?", "20%");
            confirmMessage.getOkButton().addClickListener(e -> {
                p.setAprobado(1L);
                p.setFuncrrhh(UserHolder.get().getIdfuncionario());
                Solicitud soli = solicitudController.guardarSolicitud(p);
                if (soli.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                    Vacaciones listado = vacacionesDao.listadeVacacionesPorPeriodo(p.getFuncionario().getId(), p.getPeriodovacas());
                    double diasVacasDisponibles = listado.getCantdiarestante();

                    long diaATomar = p.getCantdia();
                    if (diasVacasDisponibles >= diaATomar) {
//                        for (Vacaciones listado : listVacaciones) {
                        if (listado.getPeriodo().equalsIgnoreCase(soli.getPeriodovacas())) {
                            double dif = listado.getCantdiarestante() - diaATomar;
                            VacacionesDetalle vd = new VacacionesDetalle();
                            vd.setSolicitud(soli);
                            vd.setVacaciones(listado);
                            vd.setCantDia(diaATomar);

                            listado.setCantdiarestante(dif);
                            listado.setCantdiatomada((listado.getCantdiavaca() - dif));
                            diaATomar = 0;

                            vacacionesDao.guardar(listado);
                            vacacionesDetalleDao.guardar(vd);
                        }
                        /*if (listado.getCantdiarestante() > 0) {
                            long dif = listado.getCantdiarestante() - diaATomar;
                            if (dif >= 0 && diaATomar > 0) {
                                VacacionesDetalle vd = new VacacionesDetalle();
                                vd.setSolicitud(soli);
                                vd.setVacaciones(listado);
                                vd.setCantDia(diaATomar);

                                listado.setCantdiarestante(dif);
                                listado.setCantdiatomada((listado.getCantdiavaca() - dif));
                                diaATomar = 0;

                                vacacionesDao.guardar(listado);
                                vacacionesDetalleDao.guardar(vd);
                            } else if (listado.getCantdiarestante() > 0 && dif < 0 && diaATomar > 0) {
                                VacacionesDetalle vd = new VacacionesDetalle();
                                vd.setSolicitud(soli);
                                vd.setVacaciones(listado);
                                vd.setCantDia(listado.getCantdiarestante());

                                diaATomar -= listado.getCantdiarestante();

                                listado.setCantdiarestante(0l);
                                listado.setCantdiatomada(listado.getCantdiavaca());
                                vacacionesDao.guardar(listado);
                                vacacionesDetalleDao.guardar(vd);
                            }
                        }*/
//                        }
                        //REGISTRAR MARCACION EN LA FECHA
                        List<Marcaciones> listArray = marcacionesDao.getByFilter(soli.getFuncionario(), soli.getFechaini(), soli.getFechafin(), null, 0);
                        if (listArray.isEmpty()) {
                            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                            try {
                                HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                                HorarioLaboral horaLaboral = hf.getHorarioLaboral();
                                Calendar start = Calendar.getInstance();
                                start.setTime(soli.getFechaini());

                                Calendar end = Calendar.getInstance();
                                end.setTime(soli.getFechafin());
                                //PARA VERIFICACION DE FERIADOS
                                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(soli.getFechaini(), soli.getFechafin());
                                Map mapeo = new HashMap();
                                for (Feriado feriado : listFeriado) {
                                    mapeo.put(formatter.format(feriado.getFecha()), feriado);
                                }
                                List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(soli.getFechaini(), soli.getFechafin(), soli.getFuncionario().getId());
                                Map<String, Suspenciones> mapeoSuspencion = new HashMap();
                                for (Suspenciones feriado : listSuspencion) {
                                    mapeoSuspencion.put(formatter.format(feriado.getFechadesde()), feriado);
                                }
                                List<Rotaciones> listRotacion = rotacionesDao.listarPorFechas(soli.getFechaini(), soli.getFechafin(), soli.getFuncionario().getId());
                                Map<String, Rotaciones> mapeoRotaciones = new HashMap();
                                for (Rotaciones feriado : listRotacion) {
                                    mapeoRotaciones.put(formatter.format(feriado.getFecha()), feriado);
                                }
                                while (!start.after(end)) {
                                    Date targetDay = start.getTime();
                                    // Do Work Here
                                    if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                        Marcaciones marcacion = new Marcaciones();

                                        if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                            HorarioLaboral hl = hlDao.getByValue("sabado");
                                            hf.setHorarioLaboral(hl);
                                            marcacion.setInasignada(hl.getEntrada());
                                            marcacion.setOutasignada(hl.getSalida());
                                            marcacion.setInmarcada(hl.getEntrada());
                                            marcacion.setOutmarcada(hl.getSalida());
                                        } else {
                                            hf.setHorarioLaboral(horaLaboral);
                                            marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                            marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                            marcacion.setInmarcada(hf.getHorarioLaboral().getEntrada());
                                            marcacion.setOutmarcada(hf.getHorarioLaboral().getSalida());
                                        }
                                        marcacion.setHorarioFuncionario(hf);
                                        marcacion.setFecha(targetDay);
                                        marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                        long minutesEntSal = 0;
                                        try {
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } catch (Exception ec) {
                                            marcacion.setOutmarcada(marcacion.getInmarcada());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } finally {
                                        }

                                        marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                        if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            marcacion.setMinllegadatardia(0L);
                                            //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                        } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(0L);
                                            //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                            int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                            marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                            marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        }
                                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                            marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                        }
                                        marcacion.setSolicitud(soli);
                                        if (!mapeo.containsKey(formatter.format(targetDay)) && !mapeoSuspencion.containsKey(formatter.format(targetDay))) {
                                            if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha())) && mapeoRotaciones.containsKey(formatter.format(targetDay))) {
                                                marcacionesDao.guardar(marcacion);
                                            } else {
                                                if (!isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                                    marcacionesDao.guardar(marcacion);
                                                }
                                            }
                                        }
                                    }
                                    start.add(Calendar.DATE, 1);
                                }
                            } catch (Exception exp) {
                            } finally {
                            }
                        } else {
                            try {
                                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                                HorarioLaboral horaLaboral = hf.getHorarioLaboral();
                                Calendar start = Calendar.getInstance();
                                start.setTime(soli.getFechaini());

                                Calendar end = Calendar.getInstance();
                                end.setTime(soli.getFechafin());
                                //PARA VERIFICACION DE FERIADOS
                                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(soli.getFechaini(), soli.getFechafin());
                                Map<String, Marcaciones> mapeo = new HashMap();
                                for (Marcaciones marcacion : listArray) {
                                    mapeo.put(formatter.format(marcacion.getFecha()), marcacion);
                                }
                                Map mapeoFeriado = new HashMap();
                                for (Feriado feriado : listFeriado) {
                                    mapeoFeriado.put(formatter.format(feriado.getFecha()), feriado);
                                }
                                Map<String, Suspenciones> mapeoSuspencion = new HashMap();
                                List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(soli.getFechaini(), soli.getFechafin(), soli.getFuncionario().getId());
                                for (Suspenciones feriado : listSuspencion) {
                                    mapeoSuspencion.put(formatter.format(feriado.getFechadesde()), feriado);
                                }
                                List<Rotaciones> listRotacion = rotacionesDao.listarPorFechas(soli.getFechaini(), soli.getFechafin(), soli.getFuncionario().getId());
                                Map<String, Rotaciones> mapeoRotaciones = new HashMap();
                                for (Rotaciones feriado : listRotacion) {
                                    mapeoRotaciones.put(formatter.format(feriado.getFecha()), feriado);
                                }
                                while (!start.after(end)) {
                                    Date targetDay = start.getTime();
                                    // Do Work Here
                                    if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                        hf.setHorarioLaboral(horaLaboral);
                                        Marcaciones marcacion = new Marcaciones();
                                        marcacion.setHorarioFuncionario(hf);

                                        marcacion.setFecha(targetDay);
                                        marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                        marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                        marcacion.setInmarcada(hf.getHorarioLaboral().getEntrada());
                                        marcacion.setOutmarcada(hf.getHorarioLaboral().getSalida());
                                        marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                            HorarioLaboral hl = hlDao.getByValue("sabado");
                                            hf.setHorarioLaboral(hl);
                                            marcacion.setHorarioFuncionario(hf);
                                            marcacion.setInasignada(hl.getEntrada());
                                            marcacion.setOutasignada(hl.getSalida());
                                        }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                        long minutesEntSal = 0;
                                        try {
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } catch (Exception ec) {
                                            marcacion.setOutmarcada(marcacion.getInmarcada());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                        } finally {
                                        }

                                        marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                        if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            marcacion.setMinllegadatardia(0L);
                                            //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                        } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                            marcacion.setMinllegadatemprana(0L);
                                            //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                            int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                            marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                            //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                            int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                            marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                            marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                            minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                            marcacion.setMintrabajada(minutesEntSal - 40);
                                        }
                                        if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                            marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                        }
                                        if (!mapeo.containsKey(formatter.format(targetDay))) {
                                            try {
                                                if (marcacion.getSolicitud() == null) {
                                                    marcacion.setSolicitud(soli);
                                                } else {
                                                    String obs = marcacion.getObservacion() == null || marcacion.getObservacion().equalsIgnoreCase("") ? soli.getId() + " " + soli.getFormulario().getAbrev() : marcacion.getObservacion() + " - " + soli.getId() + " " + soli.getFormulario().getAbrev();
                                                    marcacion.setObservacion(obs);
                                                }
                                            } catch (Exception ex) {
                                                marcacion.setSolicitud(soli);
                                            } finally {
                                            }
                                            if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                                marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                            }
                                            if (!mapeoFeriado.containsKey(formatter.format(targetDay)) && !mapeoSuspencion.containsKey(formatter.format(targetDay))) {
                                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha())) && mapeoRotaciones.containsKey(formatter.format(targetDay))) {
                                                    marcacionesDao.guardar(marcacion);
                                                } else {
                                                    if (!isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                                        marcacionesDao.guardar(marcacion);
                                                    }
                                                }
                                            }
                                        } else {
                                            Marcaciones marcaciones = mapeo.get(formatter.format(targetDay));
                                            try {
                                                marcaciones.setHsextra(marcacion.getHsExtra());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setInasignada(marcacion.getInasignada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setInmarcada(marcacion.getInmarcada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setMinllegadatardia(marcacion.getMinllegadatardia());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setMinllegadatemprana(marcacion.getMinllegadatemprana());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setMintrabajada(marcacion.getMintrabajada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setMintrabajadaasignada(marcacion.getMintrabajadaasignada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setNombrefunc(marcacion.getNombrefunc());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setOutasignada(marcacion.getOutasignada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setOutmarcada(marcacion.getOutmarcada());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                marcaciones.setSolicitudProduccionDetalle(marcacion.getSolicitudProduccionDetalle());
                                            } catch (Exception ex) {
                                            } finally {
                                            }
                                            try {
                                                if (marcaciones.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                                                    String obs = marcaciones.getObservacion() == null || marcaciones.getObservacion().equalsIgnoreCase("") ? soli.getId() + " " + soli.getFormulario().getAbrev() : marcaciones.getObservacion() + " - " + soli.getId() + " " + soli.getFormulario().getAbrev();
                                                    marcaciones.setObservacion(obs);
                                                }
                                            } catch (Exception ex) {
                                                if (marcaciones.getSolicitud() == null) {
                                                    marcaciones.setSolicitud(soli);
                                                }
                                            } finally {
                                            }
                                            if (!mapeoFeriado.containsKey(formatter.format(targetDay)) && !mapeoSuspencion.containsKey(formatter.format(targetDay))) {
                                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha())) && mapeoRotaciones.containsKey(formatter.format(targetDay))) {
                                                    marcacionesDao.guardar(marcaciones);
                                                } else {
                                                    if (!isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                                        marcacionesDao.guardar(marcaciones);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    start.add(Calendar.DATE, 1);
                                }
                            } catch (Exception exp) {
                            } finally {
                            }
                        }
                        Notification.show("Mensaje del Sistema", "Datos confirmados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        p.setAprobado(0L);
                        p.setFuncrrhh(null);
                        solicitudController.guardarSolicitud(p);
                        Notification.show("Mensaje del Sistema", "Verificar la cantidad de días disponibles para el periodo " + p.getPeriodovacas(), Notification.Type.ERROR_MESSAGE);
                    }
                } else if (soli.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                    List listArray = marcacionesDao.getByFilter(soli.getFuncionario(), soli.getFechaini(), soli.getFechafin(), null, 0);
                    if (listArray.isEmpty()) {
                        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                        Calendar start = Calendar.getInstance();
                        start.setTime(soli.getFechaini());

                        Calendar end = Calendar.getInstance();
                        end.setTime(soli.getFechafin());

                        while (!start.after(end)) {
                            Date targetDay = start.getTime();
                            // Do Work Here
                            if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                Marcaciones marcacion = new Marcaciones();
                                marcacion.setHorarioFuncionario(hf);

                                marcacion.setFecha(targetDay);
                                marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                marcacion.setInmarcada(soli.getHoraini());
                                marcacion.setOutmarcada(soli.getHorafin());
                                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sabado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setInasignada(hl.getEntrada());
                                    marcacion.setOutasignada(hl.getSalida());
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                long minutesEntSal = 0;
                                try {
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } finally {
                                }

                                marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                                marcacion.setSolicitud(soli);
                                marcacionesDao.guardar(marcacion);
                            }
                            start.add(Calendar.DATE, 1);
                        }
                    } else {
                        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                        Calendar start = Calendar.getInstance();
                        start.setTime(soli.getFechaini());

                        Calendar end = Calendar.getInstance();
                        end.setTime(soli.getFechafin());
                        Timestamp tsHora = (soli.getHoraini());
                        Timestamp tsHoraSolicitud = tsHora;

                        while (!start.after(end)) {
                            Date targetDay = start.getTime();

                            Marcaciones marcacion = marcacionesDao.getByFilter(hf.getFuncionario(), targetDay, targetDay, null, 0).get(0);
                            boolean isVacation = false;
                            try {
                                if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                                    isVacation = true;
                                }
                            } catch (Exception ex) {
                            } finally {
                            }
                            // Do Work Here
                            if (isVacation) {
                                marcacion.setObservacion(marcacion.getObservacion() == null || marcacion.getObservacion().equalsIgnoreCase("") ? soli.getId() + " " + soli.getFormulario().getAbrev() : marcacion.getObservacion() + " - " + soli.getId() + " " + soli.getFormulario().getAbrev());
                                marcacionesDao.guardar(marcacion);
                            } else if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                marcacion.setHorarioFuncionario(hf);

                                marcacion.setFecha(targetDay);
                                marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                if ((marcacion.getInmarcada().getHours() + marcacion.getOutmarcada().getMinutes()) == 0) {
                                    marcacion.setInmarcada(soli.getHoraini());
                                    marcacion.setOutmarcada(soli.getHorafin());
                                } else {

                                    Timestamp tsInMarcada = new Timestamp(targetDay.getTime());
                                    tsInMarcada.setHours(marcacion.getInmarcada().getHours());
                                    tsInMarcada.setMinutes(marcacion.getInmarcada().getMinutes());
                                    tsInMarcada.setSeconds(marcacion.getInmarcada().getSeconds());

                                    Timestamp tsOutMarcada = new Timestamp(targetDay.getTime());
                                    tsOutMarcada.setHours(marcacion.getOutmarcada().getHours());
                                    tsOutMarcada.setMinutes(marcacion.getOutmarcada().getMinutes());
                                    tsOutMarcada.setSeconds(marcacion.getOutmarcada().getSeconds());

                                    if (tsHoraSolicitud.before(tsInMarcada)) {
                                        marcacion.setInmarcada(tsHoraSolicitud);
                                    }
                                    if (soli.getHorafin().after(tsOutMarcada)) {
                                        marcacion.setOutmarcada(soli.getHorafin());
                                    }
                                }

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sabado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setInasignada(hl.getEntrada());
                                    marcacion.setOutasignada(hl.getSalida());
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                System.out.println("MARCACION -->> " + marcacion.getHorarioFuncionario().getIdreloj() + " -- " + marcacion.getFecha() + " -- " + marcacion.getOutmarcada() + " -- " + hf.getHorarioLaboral().getEntrada().getTime());
                                long minutesEntSal = 0;
                                try {
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } finally {
                                }

                                marcacion.setMintrabajada(minutesEntSal - 40);
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                                try {
                                    if (marcacion.getSolicitud() == null) {
                                        marcacion.setSolicitud(soli);
                                    } else {
                                        marcacion.setObservacion(marcacion.getObservacion() == null || marcacion.getObservacion().equalsIgnoreCase("") ? marcacion.getSolicitud().getId() + " " + marcacion.getSolicitud().getFormulario().getAbrev() : marcacion.getObservacion() + " - " + marcacion.getSolicitud().getId() + " " + marcacion.getSolicitud().getFormulario().getAbrev());
                                    }
                                } catch (Exception ex) {
                                    marcacion.setSolicitud(soli);
                                } finally {
                                }
                                marcacionesDao.guardar(marcacion);
                            }
                            start.add(Calendar.DATE, 1);
                        }
                    }
                }

                grid.clearSortOrder();
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    lista = solicitudController.listadeSolicitudSinConfirmar();
                } else {
                    lista = new ArrayList<>();
                }
                grid.setItems(lista);
                confirmMessage.closePopup();
            });
            confirmMessage.getCancelButton().addClickListener(e -> {
                confirmMessage.closePopup();

                InputModal inputModal = new InputModal("");
                inputModal.openInModalPopup("Rechazo", "");
                inputModal.getOkButton().addClickListener(listener -> {
                    p.setAprobado(2L);
                    p.setFuncrrhh(UserHolder.get().getIdfuncionario());
                    p.setRechazo(inputModal.getText());
                    solicitudController.guardarSolicitud(p);
                    grid.clearSortOrder();
                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                            || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                        lista = solicitudController.listadeSolicitudSinConfirmar();
                    } else {
                        lista = new ArrayList<>();
                    }
                    grid.setItems(lista);
                    inputModal.closePopup();
                });
            });
        }
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void guardarVacaciones(Solicitud solicitud, double tomada, double restante) {
        Calendar cal = Calendar.getInstance();

        Vacaciones vacas = new Vacaciones();
        vacas.setPeriodo(cal.get(Calendar.YEAR) + "");
        vacas.setCantdiavaca(0d);
        vacas.setCantdiatomada(tomada);
        vacas.setCantdiarestante(restante - tomada);
        vacas.setFuncionario(solicitud.getFuncionario());
//        vacas.setSolicitud(solicitud);

        vacacionesDao.guardar(vacas);
    }

    private void imrpimirPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PERMISO")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudPermiso() + "}");
        } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudVacaciones() + "}");
        } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudOrdenTrabajo() + "}");
        } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            pSQL.put("repJsonString", "{\"ventas\": " + cargarSolicitudProduccion() + "}");
        }

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase());
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "";
                if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PERMISO")) {
                    archivo += "formulario_permiso";
                } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                    archivo += "formulario_vacaciones";
                } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                    archivo += "formulario_orden_trabajo";
                } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    archivo += "formulario_produccion";
                }
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "";
        if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PERMISO")) {
            archivo += "formulario_permiso";
        } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
            archivo += "formulario_vacaciones";
        } else if (solicitudSeleccionado.getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
            archivo += "formulario_orden_trabajo";
        }

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarSolicitudPermiso() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(solicitudSeleccionado.getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("funcionario", solicitudSeleccionado.getFuncionario().getNombre() + " " + solicitudSeleccionado.getFuncionario().getApellido());
        jsonObj.put("codigo", solicitudSeleccionado.getId());
        jsonObj.put("area", solicitudSeleccionado.getAreafunc());
        jsonObj.put("cargo", solicitudSeleccionado.getCargofunc());
        String motivoDescri = solicitudDetalle.get(0).getMotivo().getDescripcion().toLowerCase().trim();
        switch (motivoDescri) {
            case "llegada tardía":
                jsonObj.put("llegada_tarde", "X");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "salida antes de hora":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "X");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "ausencia":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "X");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "salida durante la jornada":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "X");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "estudios-exámenes universitarios-capacitaciones":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "X");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "otros(especificar en obs)":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "X");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "enfermedad-consulta médica-reposo médico":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "X");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "comisionamiento ":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "X");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "particular":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "X");
                break;
            case "matrimonio":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "X");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            case "fallecimiento de cónyuge, hijos, padres, abuelos,hnos. ":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "X");
                jsonObj.put("particular", "   ");
                break;
            case "gravidez – paternidad - lactancia":
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "X");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
            default:
                jsonObj.put("llegada_tarde", "   ");
                jsonObj.put("ausencia", "   ");
                jsonObj.put("salida_antes", "   ");
                jsonObj.put("salida_jornada", "   ");
                jsonObj.put("estudios", "   ");
                jsonObj.put("otros", "   ");
                jsonObj.put("enfermedad", "   ");
                jsonObj.put("comisionamiento", "   ");
                jsonObj.put("matrimonio", "   ");
                jsonObj.put("gravidez", "   ");
                jsonObj.put("fallecimiento", "   ");
                jsonObj.put("particular", "   ");
                break;
        }

        jsonObj.put("observacion", solicitudSeleccionado.getObservacion());
        jsonObj.put("cantdia", solicitudSeleccionado.getCantdia());
        jsonObj.put("fechas", formatter.format(solicitudSeleccionado.getFechaini()) + " a " + formatter.format(solicitudSeleccionado.getFechafin()));

        try {
            if (solicitudDetalle.get(0).getUrlimagen() != null || !solicitudDetalle.get(0).getUrlimagen().equalsIgnoreCase("")) {
                jsonObj.put("si", "X");
                jsonObj.put("no", "   ");
                jsonObj.put("documento", solicitudDetalle.get(0).getUrlimagen());
            } else {
                jsonObj.put("si", "   ");
                jsonObj.put("no", "X");
                jsonObj.put("documento", "   ");
            }
        } catch (Exception e) {
            jsonObj.put("si", "   ");
            jsonObj.put("no", "X");
            jsonObj.put("documento", "   ");
        } finally {
        }

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private JSONArray cargarSolicitudVacaciones() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(solicitudSeleccionado.getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("codigo", solicitudSeleccionado.getId());
        jsonObj.put("funcionario", solicitudSeleccionado.getFuncionario().getNombre() + " " + solicitudSeleccionado.getFuncionario().getApellido());
        jsonObj.put("area", solicitudSeleccionado.getAreafunc());
        jsonObj.put("cargo", solicitudSeleccionado.getCargofunc());
        jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFuncionario().getFechaingreso()));
        try {
            jsonObj.put("motivo", (Long.parseLong(solicitudDetalle.get(0).getSolicitud().getPeriodovacas()) - 1) + "/" + solicitudDetalle.get(0).getSolicitud().getPeriodovacas().toUpperCase());
        } catch (Exception e) {
            jsonObj.put("motivo", "");
        }

        jsonObj.put("fechadesde", formatter.format(solicitudSeleccionado.getFechaini()));
        jsonObj.put("fechahasta", formatter.format(solicitudSeleccionado.getFechafin()));

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private JSONArray cargarSolicitudProduccion() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudProduccionDetalle> solicitudDetalle = solicitudProduccionDetalleController.listarPorIdSolicitud(solicitudSeleccionado.getId());
        int num = 0;
        for (SolicitudProduccionDetalle solicitudProduccionDetalle : solicitudDetalle) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("codigo", solicitudSeleccionado.getId());
            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));
            jsonObj.put("seccion", solicitudProduccionDetalle.getSolicitud().getCargofunc().toUpperCase());
            jsonObj.put("area", solicitudProduccionDetalle.getSolicitud().getAreafunc().toUpperCase());

            jsonObj.put("funcionario", solicitudProduccionDetalle.getFuncionario().getNombre() + " " + solicitudProduccionDetalle.getFuncionario().getApellido());
            jsonObj.put("cargo", solicitudProduccionDetalle.getCargo().toUpperCase());
            jsonObj.put("motivo", solicitudProduccionDetalle.getDescripcion().toUpperCase());
            jsonObj.put("fechadesde", formatter.format(solicitudProduccionDetalle.getFecha()) + " " + formatHor.format(solicitudProduccionDetalle.getHora()));
            jsonObj.put("num", (num + 1));
            num++;

            jsonArrayDato.add(jsonObj);
        }
        return jsonArrayDato;
    }

    private JSONArray cargarSolicitudOrdenTrabajo() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(solicitudSeleccionado.getId());

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("fechas", formatter.format(solicitudSeleccionado.getFechacreacion()));
        jsonObj.put("codigo", solicitudSeleccionado.getId());
        jsonObj.put("fechainicio", formatter.format(solicitudSeleccionado.getFechaini()));
        jsonObj.put("fechafin", formatter.format(solicitudSeleccionado.getFechafin()));
        jsonObj.put("horainicio", formatterHora.format(solicitudSeleccionado.getHoraini()));
        jsonObj.put("horafin", formatterHora.format(solicitudSeleccionado.getHorafin()));
        jsonObj.put("funcionario", solicitudSeleccionado.getFuncionario().getNombre() + " " + solicitudSeleccionado.getFuncionario().getApellido());
        jsonObj.put("ci", solicitudSeleccionado.getFuncionario().getCedula());
        jsonObj.put("area", solicitudSeleccionado.getAreafunc());
        jsonObj.put("observacion", solicitudSeleccionado.getObservacion());
        try {
            if (solicitudSeleccionado.getDependencia() == null) {
                jsonObj.put("dependencia", "");
            } else {
                jsonObj.put("dependencia", solicitudSeleccionado.getDependencia());
            }
        } catch (Exception e) {
            jsonObj.put("dependencia", "");
        } finally {
        }
        if (solicitudDetalle.get(0).getMotivo().getDescripcion().length() > 30) {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(0, 30));
            jsonObj.put("tarea2", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase().substring(30, solicitudDetalle.get(0).getMotivo().getDescripcion().length()));
        } else {
            jsonObj.put("tarea", solicitudDetalle.get(0).getMotivo().getDescripcion().toUpperCase());
            jsonObj.put("tarea2", "");
        }

        jsonArrayDato.add(jsonObj);
        return jsonArrayDato;
    }

    private void limpiarDatos() {
        if (filter.getValue().equalsIgnoreCase("")) {
            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
        }
    }

    private void updateFormulario(Boolean value) {
        if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            List<Parametro> listParam = parametroController.listarPorCodigo("cargar_fecha_anterior_solicitud");
            if (value) {
                listParam.get(0).setValor("SI");
            } else {
                listParam.get(0).setValor("NO");
            }
            parametroController.guardarParametro(listParam.get(0));
        }
    }

    private void updateFormularioVacaciones(Boolean value) {
        if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            List<Parametro> listParam = parametroController.listarPorCodigo("cargar_vacaciones_menor");
            if (value) {
                listParam.get(0).setValor("SI");
            } else {
                listParam.get(0).setValor("NO");
            }
            parametroController.guardarParametro(listParam.get(0));
        }
    }
}
