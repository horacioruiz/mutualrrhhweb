package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.formularios.DependenciafuncionarioForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 7/6/2016.
 */
public class DependenciafuncionarioView extends CssLayout implements View {

    public static final String VIEW_NAME = "Dependencia de Funcionarios";
    Grid<Funcionario> grid = new Grid<>(Funcionario.class);
    Button newUser = new Button("");
    TextField txtfFiltro = new TextField();
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    MotivosDao motivosController = ResourceLocator.locate(MotivosDao.class);
    private ComboBox<Dependencia> cbDpto = new ComboBox<>("");
    private ComboBox<Cargo> cbCargo = new ComboBox<>("");
    ComboBox<Funcionario> filter = new ComboBox<>();
    DependenciafuncionarioForm form = new DependenciafuncionarioForm();
    List<Funcionario> lista = new ArrayList<>();
    Button searchUser = new Button("");
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);

    public DependenciafuncionarioView() {
        try {
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            searchUser.addStyleName(MaterialTheme.BUTTON_ROUND);
            searchUser.setIcon(VaadinIcons.SEARCH);

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                    if (listFunc.isEmpty()) {
                        listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                    }else{
                        listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                    } 
            }
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }

            filter.setItems(funcionarioController.listaFuncionario());
            filter.setWidth(18f, ComboBox.UNITS_EM);
            filter.setPlaceholder("Búsqueda por funcionario");
            filter.setItemCaptionGenerator(Funcionario::getNombreCompleto);

            newUser.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            newUser.setIcon(VaadinIcons.PLUS_CIRCLE);

            cbCargo.setPlaceholder("Búsqueda por Cargo");
            cbCargo.setWidth(18f, ComboBox.UNITS_EM);
            cbCargo.setItems(cargoDao.listaCargo());
            cbCargo.setItemCaptionGenerator(Cargo::getDescripcion);
            cbCargo.setVisible(true);

//            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));

            cbDpto.setItems(dptoDao.listaDependencia());
            cbDpto.setPlaceholder("Búsqueda por Dependencia");
            cbDpto.setWidth(18f, ComboBox.UNITS_EM);
            cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbDpto.setVisible(true);

//            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponents(cbDpto, cbCargo, filter, searchUser);
//            horizontalLayout.setSpacing(true);
//            horizontalLayout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
//            horizontalLayout.setComponentAlignment(newUser, Alignment.MIDDLE_RIGHT);
            horizontalLayout.addStyleName("top-bar");

            lista = funcionarioDao.listaFuncionario();
            form.setVisible(false);

            grid.setItems(lista);
            grid.removeAllColumns();
            grid.addColumn(Funcionario::getId).setCaption("Codigo");
            grid.addColumn(Funcionario::getNombreCompleto).setCaption("Funcionario");
//            grid.addColumn(Funcionario::getDependencia).setCaption("Funcionario");
            grid.addColumn(e -> {
                try {
//                    return dptoDao.getDependenciaById(e.getDependencia().getIddependenciapadre()).getDescripcion();
                    return e.getDependencia().getDescripcion().toUpperCase();
                } catch (Exception ex) {
                    return "--";
                } finally {
                }
            }).setCaption("Dependencia");
            grid.addColumn(e -> {
                try {
                    return e.getCargo().getDescripcion().toUpperCase();
                } catch (Exception ex) {
                    return "--";
                } finally {
                }
            }).setCaption("Cargo");
            //        grid.sort(initialSorting.getPropertyId(), initialSorting.getDirection());
            //        grid.lazyLoadFrom(safep, safep, safep.getPageSize());

            grid.setSizeFull();

            grid.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    if (e.getValue() == null) {
                        form.setVisible(false);
                    } else {
                        form.setCargo(e.getValue());
                        form.setViejo();
                    }
                }
            });

            HorizontalLayout hl = new HorizontalLayout();
            hl.addComponents(grid, form);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(hl);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

            newUser.addClickListener(e -> {
                grid.asSingleSelect().clear();
                form.setCargo(new Funcionario());
            });
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (e.getValue() != null) {
//                    form.edit(e.getValue());
                }
            });

            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                newUser.setEnabled(true);
            } else {
                newUser.setEnabled(false);
            }

            form.setSaveListener(usuarios -> {
                if (usuarios.getId() == null) {
                    //ejb.runInTransaction(entityManager -> entityManager.persist(usuarios));
                    grid.clearSortOrder();
                    lista = funcionarioController.listaFuncionario();
                    grid.setItems(lista);
                    Notification.show("Nuevo motivo creado", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    //ejb.runInTransaction(entityManager -> entityManager.merge(usuarios));
                    grid.clearSortOrder();
                }
            });

            searchUser.addClickListener(e -> {
                grid.clearSortOrder();
                updateCombo();
            });

            form.setCancelListener(usuarios -> {
                grid.clearSortOrder();
                form.setVisible(false);
            });

            form.setDeleteListener(usuarios -> {
                lista = funcionarioController.listaFuncionario();
                grid.clearSortOrder();
                grid.setItems(lista);
                form.setVisible(false);
                //            grid.getContainerDataSource().removeItem(product);
                //            grid.refreshRows();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    private void updateList(String value) {
        try {
//            lista = motivosController.listadeMotivos(0, 0, value.toUpperCase());
            lista = funcionarioController.listaFuncionario();
            grid.setSizeFull();
            grid.setItems(lista);
            grid.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout createTopBar() {
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setSpacing(true);
        topLayout.setWidth("100%");
        topLayout.addComponent(filter);
        topLayout.addComponent(newUser);
        topLayout.setSpacing(true);
//        topLayout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
//        topLayout.setComponentAlignment(newUser, Alignment.MIDDLE_RIGHT);
//        topLayout.setExpandRatio(filter, 1);
        topLayout.addStyleName("top-bar");
        return topLayout;
    }

//    private void cargarSeccion(Dependencia dependencia) {
//        if (dependencia != null && cbDpto.getValue() != null) {
//            cbCargo.clear();
//            cbCargo.setItems(new ArrayList<>());
//            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
//            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
//            cbCargo.setPlaceholder("Búsqueda por Cargo");
//            cbCargo.setVisible(true);
//
//            Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
//            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
//            List<Funcionario> listFunc = new ArrayList<>();
//            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
//            for (Dependencia dependencia1 : listDependencia) {
//                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
//            }
//            filter.setItems(listFunc);
//            filter.setItemCaptionGenerator(Funcionario::getNombreCompleto);
//        }
//    }

    private void updateCombo() {
        List<Funcionario> listUsuarios = funcionarioController.listarFuncionarioParametro(cbDpto.getValue(), cbCargo.getValue(), filter.getValue());
        grid.setItems(listUsuarios);
    }
}
