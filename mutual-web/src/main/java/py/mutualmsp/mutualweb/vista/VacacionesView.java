package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.simple.JSONArray;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.EmpresaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Empresa;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.formularios.VacacionesForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.SolicitudView.ordenandoFechaString;

/**
 * Created by Alfre on 7/6/2016.
 */
public class VacacionesView extends CssLayout implements View {

    public static final String VIEW_NAME = "Vacaciones";
    Grid<Vacaciones> grid = new Grid<>(Vacaciones.class);
    Button newUser = new Button("");
    private TextField periodo = new TextField();
    Button searchUser = new Button("");
    FormularioDao controller = ResourceLocator.locate(FormularioDao.class);
    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    VacacionesDao motivosController = ResourceLocator.locate(VacacionesDao.class);
    EmpresaDao empresaDao = ResourceLocator.locate(EmpresaDao.class);
    ComboBox<Funcionario> filter = new ComboBox<>();
    Label labelTotalizador = new Label();
    VacacionesForm form = new VacacionesForm();
    List<Vacaciones> lista = new ArrayList<>();
    Button btnImprimir = new Button("");
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    
    Date desde = null;
    Date hasta = null;

    private ComboBox<Dependencia> cbDpto = new ComboBox<>();
    //private ComboBox<Cargo> cbCargo = new ComboBox<>("");
    private ComboBox<Empresa> comboFilial = new ComboBox<>();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class); 
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();
    ZoneId defaultZoneId = ZoneId.systemDefault();

    public VacacionesView() {
        try {  
            System.out.print("Nueva instancia de ClienteView");
            setSizeFull();
            addStyleName("crud-view");

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia
                        .getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                }                
            }
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }
            
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);
            
            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
            fechaDesde.setDateFormat("MM-dd");
            fechaHasta.setDateFormat("MM-dd");
            
            cbDpto.setItems(dptoDao.listaDependencia());
            cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbDpto.setVisible(true);

//            cbCargo.setItems(cargoDao.listaCargo());
//            cbCargo.setItemCaptionGenerator(Cargo::getDescripcion);
 //           cbCargo.setVisible(true);
            
            comboFilial.setItems(empresaDao.getFiliales());
            comboFilial.setItemCaptionGenerator(Empresa::getDescripcion);
            comboFilial.setVisible(true);

//            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));
            newUser.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            newUser.setIcon(VaadinIcons.PLUS_CIRCLE);
            searchUser.addStyleName(MaterialTheme.BUTTON_ROUND);
            searchUser.setIcon(VaadinIcons.SEARCH);

            btnImprimir.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnImprimir.setIcon(VaadinIcons.PRINT);
                
            HorizontalLayout horizontalLayout = createTopBar();

            filter.setWidth(20f, ComboBox.UNITS_EM);
//            horizontalLayout.setSpacing(true);
//            v2.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
//            v2.setComponentAlignment(newUser, Alignment.MIDDLE_RIGHT);
//            horizontalLayout.addStyleName("top-bar");

//            labelTotalizador.setCaption("Acreditado: 0 | Utilizado: 0 | Restante: 0");
//            HorizontalLayout horizontalLayout = createTopBar();
            if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1) 
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
                    || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                filter.setItems(funcionarioController.listaFuncionario());
            } else {
                filter.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
//            filter.setItems(funcionarioController.listaFuncionario());
//            filter.setWidth(25f, ComboBox.UNITS_EM);
            filter.setPlaceholder("Filtre por Funcionario");
            filter.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            form.setVisible(false);

            periodo.setWidth(10f, TextField.UNITS_EM);
            periodo.setPlaceholder("Periodo");

            cbDpto.setPlaceholder("Dependencia (*)");
            //cbCargo.setPlaceholder("Cargo (*)");
            comboFilial.setPlaceholder("Filial");

            grid.removeAllColumns();
            grid.addColumn(Vacaciones::getId).setCaption("Codigo");
            grid.addColumn(e -> {
                return e.getFuncionario() == null ? ""
                        : e.getFuncionario().getNombre() + " " + e.getFuncionario().getApellido();
            }).setCaption("Funcionario");
            grid.addColumn(e -> {
                SimpleDateFormat formatFecs = new SimpleDateFormat("dd-MM-yyyy");
                return formatFecs.format(e.getFuncionario().getFechaingreso());
            }).setCaption("Ingreso");
            grid.addColumn(e -> {
                return (Long.parseLong(e.getPeriodo()) - 1) + "/" + e.getPeriodo();
            }).setCaption("Periodo");
            grid.addColumn(e -> {
                SimpleDateFormat formatFecs = new SimpleDateFormat("dd-MM-yyyy");
                Date fecha = (e.getFuncionario().getFechaingreso());

                Calendar cal = Calendar.getInstance();
                cal.setTime(fecha);
                cal.set(Calendar.YEAR, Integer.parseInt(e.getPeriodo()));

                return formatFecs.format(cal.getTime());
            }).setCaption("Per Desde");
            grid.addColumn(e -> {
                SimpleDateFormat formatFecs = new SimpleDateFormat("dd-MM-yyyy");
                Date fecha = (e.getFuncionario().getFechaingreso());

                Calendar cal = Calendar.getInstance();
                cal.setTime(fecha);
                cal.set(Calendar.YEAR, Integer.parseInt(e.getPeriodo()));
                cal.add(Calendar.MONTH, 6);

                return formatFecs.format(cal.getTime());
            }).setCaption("Per Hasta");
//            grid.addColumn(Vacaciones::getPeriodo).setCaption("Periodo");
            grid.addColumn(Vacaciones::getCantdiavaca).setCaption("Acreditado");
            grid.addColumn(Vacaciones::getCantdiatomada).setCaption("Utilizado");
            grid.addColumn(Vacaciones::getCantdiarestante).setCaption("Restante");
            grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("Adelanto");
//            grid.addComponentColumn(Vacaciones::getEstadoIcon).setCaption("Solicitud");

            grid.setSizeFull();
            filter.addValueChangeListener(vcl -> {
                if (vcl.getValue() != null) {
                    grid.clearSortOrder();
                } else {   
                    grid.clearSortOrder();
                }
            });

            HorizontalLayout hl = new HorizontalLayout();
            hl.addComponents(grid, form);
            hl.setSizeFull();
            hl.setExpandRatio(grid, 1);

            VerticalLayout barAndGridLayout = new VerticalLayout();
            barAndGridLayout.addComponent(horizontalLayout);
            barAndGridLayout.addComponent(hl);

//            barAndGridLayout.addComponent(labelTotalizador);
//            barAndGridLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
            barAndGridLayout.setMargin(true);
            barAndGridLayout.setSpacing(true);
            barAndGridLayout.setSizeFull();
            barAndGridLayout.setExpandRatio(hl, 1);
            barAndGridLayout.addStyleName("crud-main-layout");
            addComponent(barAndGridLayout);

//            if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1) || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                newUser.setEnabled(true);
            } else {
                newUser.setEnabled(false);
            }

            grid.asSingleSelect().addValueChangeListener(e -> {
//                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getIdfuncionario())) {
                if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    if (e.getValue() == null) {
                        form.setVisible(false);
                    } else {
                        form.setCargo(e.getValue());
                        form.setViejo();

                        form.setSaveListener(usuarios -> {
                            if (usuarios.getId() != null) {
                                //ejb.runInTransaction(entityManager -> entityManager.persist(usuarios));
                                grid.clearSortOrder();

                                updateCombo(usuarios.getFuncionario());
//                        lista = motivosController.listadeVacaciones();
//                        grid.setItems(lista);
                                Notification.show("Vacaciones actualizadas correctamente!.", Notification.Type.HUMANIZED_MESSAGE);
                            } else {
                                grid.clearSortOrder();

                                //ejb.runInTransaction(entityManager -> entityManager.merge(usuarios));
                                grid.clearSortOrder();
                                cargarTotalizador(lista);
                            }
                        });
                    }
                }
            });
            
            newUser.addClickListener(e -> {
                grid.asSingleSelect().clear();
                form.setCargo(new Vacaciones());

                form.setSaveListener(usuarios -> {
                    if (usuarios.getId() != null) {
                        //ejb.runInTransaction(entityManager -> entityManager.persist(usuarios));
                        grid.clearSortOrder();

                        updateCombo(usuarios.getFuncionario());
//                        lista = motivosController.listadeVacaciones();
//                        grid.setItems(lista);
                        Notification.show("Vacaciones creada exitosamente!.", Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        grid.clearSortOrder();

                        //ejb.runInTransaction(entityManager -> entityManager.merge(usuarios));
                        grid.clearSortOrder();
                        cargarTotalizador(lista);
                    }
                });
            });
            btnImprimir.addClickListener(e -> {
                mostrarListaVacacionesPDF();
            });
            searchUser.addClickListener(e -> {
                grid.clearSortOrder();
                updateCombo(filter.getValue());
            });
            
            grid.addItemClickListener(listener -> {
                if (listener.getMouseEventDetails().isDoubleClick()) {
//                    if (listener.getItem().getSolicitud() == null) {
//                        Notification.show("Sin solicitudes de vacaciones asociadas", Notification.Type.ERROR_MESSAGE);
//                    } else {
                    SolicitudDetalleView formSolicitudDetalle = new SolicitudDetalleView();
                    //listener.getItem().getPeriodo()
//                    List<Vacaciones> listVacas = motivosController.listarPorVacacionesSolicitud(listener.getItem().getPeriodo());
//                    List<Solicitud> array = new ArrayList();
//                    for (Vacaciones vacas : listVacas) {
//                        array.add(motivosController.listaVacacionesPorId(vacas.getId()).getSolicitud());
//                    }
                    formSolicitudDetalle.setListaSolicitud(listener.getItem().getId());
                    UI.getCurrent().addWindow(formSolicitudDetalle);
                    formSolicitudDetalle.setVisible(true);
//                    }
                }
            });
            grid.asSingleSelect().addValueChangeListener(e -> {
                if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    if (e.getValue() != null) {
                        if (e.getValue().getCantdiavaca() == 0) {

                        } else {
//                            form.edit(e.getValue());
//
//                            form.setSaveListener(usuarios -> {
//                                if (usuarios.getId() == null) {
//                                    grid.clearSortOrder();
//                                    updateCombo(usuarios.getFuncionario());
//                                    Notification.show("Nueva vacacion creada", Notification.Type.HUMANIZED_MESSAGE);
//                                } else {
//                                    grid.clearSortOrder();
//                                }
//                            });
                        }
                    }
                }
            });

            form.setCancelListener(usuarios -> {
                grid.clearSortOrder();
                form.setVisible(false);
            });

            form.setDeleteListener(usuarios -> {
                grid.clearSortOrder();
                updateCombo(usuarios.getFuncionario());
//                lista = motivosController.listadeVacaciones();
//                grid.setItems(lista);
                form.setVisible(false);
                //            grid.getContainerDataSource().removeItem(product);
                //            grid.refreshRows();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    private void updateList(String value) {
        try {
            grid.setSizeFull();
            grid.setItems(lista);
            grid.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout createTopBar() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        HorizontalLayout layoutIzquierdo = new HorizontalLayout();
        layoutIzquierdo.addComponents(cbDpto, comboFilial, periodo);
       
        
        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        verticalLayoutIzquierda.addComponent(layoutIzquierdo);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filter);
        layoutFuncEstado.setExpandRatio(filter, 2);
        layoutFuncEstado.addComponent(fechaDesde);
        layoutFuncEstado.setExpandRatio(fechaDesde, 2);
        layoutFuncEstado.addComponent(fechaHasta);
        layoutFuncEstado.setExpandRatio(fechaHasta, 2);
        layoutFuncEstado.addComponent(searchUser);
        layoutFuncEstado.setExpandRatio(searchUser, 2);
        layoutFuncEstado.addComponent(newUser);
        layoutFuncEstado.setExpandRatio(newUser, 2);
        layoutFuncEstado.addComponent(btnImprimir);
        layoutFuncEstado.setExpandRatio(btnImprimir, 2);
        verticalLayoutIzquierda.addComponent(layoutFuncEstado);

        layout.addComponent(verticalLayoutIzquierda);
        layout.setStyleName("top-bar");
        return layout;
    }

    private void updateCombo2(Funcionario valor) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        List<Vacaciones> listUsuarios = motivosController.getListVacaciones(value.getIdfuncionario() + "");
//        grid.setItems(listUsuarios);
        Map<String, Vacaciones> mapeo = new HashMap();
        double vacasAcreditada = 0;
        double vacasTomadas = 0;
        double vacasRestante = 0;
        for (Vacaciones vacaciones : motivosController.listadeVacaciones(valor.getId())) {
            if (!mapeo.containsKey(vacaciones.getFuncionario().getId() + "-" + vacaciones.getPeriodo())) {
                vacasAcreditada += vacaciones.getCantdiavaca();
                vacasTomadas += vacaciones.getCantdiatomada();
                vacasRestante = vacasAcreditada - vacasTomadas;

                vacaciones.setCantdiatomada(vacasTomadas);
                vacaciones.setCantdiarestante(vacasRestante);
                mapeo.put(vacaciones.getFuncionario().getId() + "-" + vacaciones.getPeriodo(), vacaciones);
            } else {
                Vacaciones vacas = (Vacaciones) mapeo.get(vacaciones.getFuncionario().getId() + "-" + vacaciones.getPeriodo());

                vacasAcreditada += vacaciones.getCantdiavaca();
                vacasTomadas += vacaciones.getCantdiatomada();
                vacasRestante = vacasAcreditada - vacasTomadas;

                if (vacas.getCantdiavaca() > 0) {
                    if (vacaciones.getCantdiavaca() > 0) {
                        vacas.setCantdiavaca(vacas.getCantdiavaca() + vacaciones.getCantdiavaca());
                    }
                } else {
                    vacas.setCantdiavaca(vacaciones.getCantdiavaca());
                }

                vacas.setCantdiatomada(vacasTomadas);
                vacas.setCantdiarestante(vacasRestante);

                mapeo.put(vacaciones.getFuncionario().getId() + "-" + vacaciones.getPeriodo(), vacas);
            }
        }
        lista = new ArrayList<>();
        for (Map.Entry<String, Vacaciones> entry : mapeo.entrySet()) {
            Vacaciones value = entry.getValue();
            lista.add(value);
        }
        if (lista.isEmpty()) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
            cargarTotalizador(lista);
        } else {
            Collections.sort(lista, Vacaciones.idComparator);
            grid.setItems(lista);
            cargarTotalizador(lista);
        }
    }
    
    private void updateCombo(Funcionario valor) {
        lista = new ArrayList<>();
        String carg = "";
        String dep = "";
        long idfun = 0;
        try {
            if (comboFilial.getValue() != null && !comboFilial.getValue().getDescripcion().equals("---")) {
                carg = comboFilial.getValue().getId().toString();
                lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());
            }
        } catch (Exception e) {
        } finally {
        }
        try {
            if (cbDpto.getValue() != null && !cbDpto.getValue().getDescripcion().equals("---")) {
                dep = cbDpto.getValue().getId().toString();
                lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());
            }
        } catch (Exception e) {
        } finally {
        }
        try {
            if (valor == null) {
                idfun = 0;
            } else {
                idfun = valor.getId();
            }
        } catch (Exception e) {
        } finally {
        }
        try {
            if (filter.getValue() != null) {
                lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());
            } else{
                if (filter.getValue() == null) {
                    grid.clearSortOrder();
                }                
            }
        } catch (Exception e) {
        } finally {
        }
        
        try {
            if (filter.getValue() == null && cbDpto.getValue() == null && comboFilial.getValue() == null) {
                filtroFecha();
            } else{
                lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());
            }
        } catch (Exception e) {
        } finally {
        }
//        try{
//            if (filter.getValue() == null) {
//               filtroFecha();                 
//            } else{
//                lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());
//            }
//        } catch (Exception e) {
//        } finally {}
     
        //lista = motivosController.filtrarVacaciones(idfun, dep, carg, periodo.getValue());

        if (lista == null || lista.isEmpty()) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
            cargarTotalizador(lista);
        } else {
            grid.setItems(lista);
            cargarTotalizador(lista);
        }
                
    }
    
    private void filtroFecha(){
            if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
                desde = Date.from(fechaDesde.getValue().atStartOfDay(defaultZoneId).toInstant());
                hasta = Date.from(fechaHasta.getValue().atStartOfDay(defaultZoneId).toInstant());
                lista = motivosController.filtroPorFechaIngreso(desde, hasta);
                grid.clearSortOrder();
                grid.setItems(lista);
            }else{
                lista = motivosController.listaVacaciones();
                grid.clearSortOrder();
                grid.setItems(lista);
            }      
    }

//    private void cargarVacaciones() {
//        Map<String, Vacaciones> mapeo = new HashMap();
//        for (Vacaciones vacaciones : motivosController.listadeVacaciones()) {
//            if (!mapeo.containsKey(vacaciones.getFuncionario().getIdfuncionario() + "-" + vacaciones.getPeriodo())) {
//                mapeo.put(vacaciones.getFuncionario().getIdfuncionario() + "-" + vacaciones.getPeriodo(), vacaciones);
//            } else {
//                Vacaciones vacas = (Vacaciones) mapeo.get(vacaciones.getFuncionario().getIdfuncionario() + "-" + vacaciones.getPeriodo());
//                vacas.setCantdiavaca(vacas.getCantdiavaca() + vacaciones.getCantdiavaca());
//                vacas.setCantdiatomada(vacas.getCantdiatomada() + vacaciones.getCantdiatomada());
//                vacas.setCantdiarestante(vacas.getCantdiarestante() + vacaciones.getCantdiarestante());
//                mapeo.put(vacaciones.getFuncionario().getIdfuncionario() + "-" + vacaciones.getPeriodo(), vacas);
//            }
//        }
//
//        for (Map.Entry<String, Vacaciones> entry : mapeo.entrySet()) {
//            Vacaciones value = entry.getValue();
//            lista.add(value);
//            // ...
//        }
//        grid.setItems(lista);
//    }
    private void cargarTotalizador(List<Vacaciones> lista) {
        long acreditado = 0;
        long utilizado = 0;
        long restante = 0;
        if (lista != null && !lista.isEmpty()) {
            for (Vacaciones vacaciones : lista) {
                acreditado += vacaciones.getCantdiavaca();
                utilizado += vacaciones.getCantdiatomada();
                restante += vacaciones.getCantdiarestante();
            }
        }
        labelTotalizador.setCaption("Acreditado: " + acreditado + " | Utilizado: " + utilizado + " | Restante: " + restante);
        labelTotalizador.setVisible(true);
    }

//    private void cargarSeccion(Dependencia dependencia) {
//        if (dependencia != null && cbDpto.getValue() != null) {
//            cbCargo.clear();
//            cbCargo.setItems(cargoDao.listaCargo());
////            cbCargo.setItems(cargoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
//            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
//            cbCargo.setVisible(true);
//
//            Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
//            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
//            List<Funcionario> listFunc = new ArrayList<>();
//            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
//            for (Dependencia dependencia1 : listDependencia) {
//                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
//            }
//            filter.setItems(listFunc);
//            filter.setItemCaptionGenerator(Funcionario::getNombreCompleto);
//        }
//    }
    private void mostrarListaVacacionesPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarVacaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
        pSQL.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "vacaciones";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "vacaciones";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarVacaciones() {
        lista = new ArrayList<>();
        Funcionario valor = null;
        try {
            valor = filter.getValue();
        } catch (Exception e) {
        } finally {
        }
        if (periodo.getValue().equals("") && comboFilial.getValue() == null && valor != null) {
            lista = motivosController.listadeVacaciones(valor.getId());
        } else if (!periodo.getValue().equals("") && comboFilial.getValue() != null) {
            lista = motivosController.listadeVacaciones(valor == null ? 0l : valor.getId(), comboFilial.getValue(), periodo.getValue());
        } else if (!periodo.getValue().equals("") && valor != null) {
            lista = motivosController.listadeVacaciones(valor.getId(), periodo.getValue());
        } else if (filter.getValue() != null) {
            lista = motivosController.listadeVacaciones(valor.getId());
        } else {
            if (comboFilial.getValue() == null) {
                lista = motivosController.listadeVacacionesCargoData(valor == null ? 0l : valor.getId(), cbDpto.getValue(), periodo.getValue());
            } else {
                lista = motivosController.listadeVacacionesFilial(valor == null ? 0l : valor.getId(), comboFilial.getValue());
            }
        }

        Comparator<Vacaciones> compareByDate = (Vacaciones o1, Vacaciones o2)
                -> o1.getFuncionario().getDependencia().getIddependenciapadre().compareTo(o2.getFuncionario().getDependencia().getIddependenciapadre());
        Collections.sort(lista, compareByDate);

        JSONArray jsonArrayDato = new JSONArray();
        for (Vacaciones ticket : lista) {
            org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
            jsonObj.put("subRepNomFun", UserHolder.get().getIdfuncionario().getNombreCompleto());
            jsonObj.put("subRepTimestamp", "");
            jsonObj.put("funcionarios", ticket.getFuncionario().getNombreCompleto());
            SimpleDateFormat formatFecs = new SimpleDateFormat("dd-MM-yyyy");
            jsonObj.put("ingreso", formatFecs.format(ticket.getFuncionario().getFechaingreso()));
            jsonObj.put("seccion", ticket.getFuncionario().getDependencia().getDescripcion().toUpperCase());
            jsonObj.put("departamento", ticket.getFuncionario().getDependencia().getDescripcion().toUpperCase());
            jsonObj.put("periodo", (Long.parseLong(ticket.getPeriodo()) - 1) + "/" + ticket.getPeriodo());
            //jsonObj.put("fecha", ticket.getFecha());
            formatFecs = new SimpleDateFormat("dd-MM-yyyy");
            Date fecha = (ticket.getFuncionario().getFechaingreso());
            Calendar cal = Calendar.getInstance();
            cal.setTime(fecha);
            cal.set(Calendar.YEAR, Integer.parseInt(ticket.getPeriodo()));
            jsonObj.put("desde", formatFecs.format(cal.getTime()));
            Date fechaIng = (ticket.getFuncionario().getFechaingreso());
            Calendar calIng = Calendar.getInstance();
            calIng.setTime(fechaIng);
            calIng.set(Calendar.YEAR, Integer.parseInt(ticket.getPeriodo()));
            calIng.add(Calendar.MONTH, 6);
            jsonObj.put("hasta", formatFecs.format(calIng.getTime()));
            jsonObj.put("acreditado", ticket.getCantdiavaca());
            jsonObj.put("utilizado", ticket.getCantdiatomada());
            jsonObj.put("restante", ticket.getCantdiarestante());
            jsonObj.put("adelanto", ticket.getAdelantado() == null || !ticket.getAdelantado() ? "NO" : "SI");
            //jsonObj.put("numot", marcacionesController.getById(ticket.getId()) == null ? "--" : marcacionesController.getById(ticket.getId()).getSolicitud().getId());
            jsonArrayDato.add(jsonObj);
        }
        return jsonArrayDato;
    }
}
