/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.RequisitooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.entities.Requisitooperacion;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.formularios.RequisitooperacionForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */

public class RequisitooperacionVista extends CssLayout implements View{
    public static final String VIEW_NAME = "Requisito según operación";
    Grid<Requisitooperacion> grillaRequisitooperacion = new Grid<>(Requisitooperacion.class);
    RequisitooperacionDao requisitooperacionDao = ResourceLocator.locate(RequisitooperacionDao.class);
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    List<Requisitooperacion> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    RequisitooperacionForm requisitooperacionForm = new RequisitooperacionForm();
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    
    public RequisitooperacionVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        requisitooperacionForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(cmbTipooperacion ,btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(cmbTipooperacion, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        cmbTipooperacion.setItems(tipooperacionDao.listaTipooperacion());
        cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
        cmbTipooperacion.addValueChangeListener(e -> {
            try {
                if(e.getValue() != null || !e.equals("")){
                    filtroTipoOperacion(e.getValue().getIdtipooperacion());
                }else{
                    updateList("");
                }   
            } catch (Exception ex) {
                updateList("");
            }
        });
        
        btnNuevo.addClickListener(e ->{
            grillaRequisitooperacion.asSingleSelect().clear();
            requisitooperacionForm.setRequisitooperacion(new Requisitooperacion());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaRequisitooperacion, requisitooperacionForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaRequisitooperacion, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = requisitooperacionDao.listaRequisitooperacion();
            grillaRequisitooperacion.setItems(lista);
            grillaRequisitooperacion.removeAllColumns();
            grillaRequisitooperacion.addColumn(Requisitooperacion::getIdrequisitooperacion).setCaption("Id Requisito Operación");
            grillaRequisitooperacion.addColumn(tipoOperacion -> { 
                return  tipoOperacion.getIdtipooperacion().getDescripcion();}).setCaption("Tipo Operación");
            grillaRequisitooperacion.addColumn(requisito -> {
                return requisito.getIdrequisito().getDescripcion();}).setCaption("Requisito");
            grillaRequisitooperacion.addColumn(activo -> { 
                return activo.getActivo() == true ? "SI" : "NO"  ;}).setCaption("Activo");
            grillaRequisitooperacion.addColumn(Requisitooperacion::getFechaactivacion).setCaption("Activación");
            grillaRequisitooperacion.addColumn(Requisitooperacion::getFechadesactivacion).setCaption("Desactivación");
            grillaRequisitooperacion.setSizeFull();
            grillaRequisitooperacion.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null){
                    requisitooperacionForm.setVisible(false);
                } else {
                    requisitooperacionForm.setRequisitooperacion(e.getValue());
                    requisitooperacionForm.setViejo();
                }
            });
            
            requisitooperacionForm.setGuardarListener(ro ->{
                grillaRequisitooperacion.clearSortOrder();
                lista = requisitooperacionDao.listaRequisitooperacion();
                grillaRequisitooperacion.setItems(lista);
            });
            requisitooperacionForm.setBorrarListener(ro ->{
                grillaRequisitooperacion.clearSortOrder();
            });
            requisitooperacionForm.setCancelarListener(ro ->{
                grillaRequisitooperacion.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
           lista = requisitooperacionDao.getListRequisito(value);
           grillaRequisitooperacion.setSizeFull();
           grillaRequisitooperacion.setItems(lista);
           grillaRequisitooperacion.clearSortOrder();
       } catch (Exception e) {
           e.printStackTrace();
       }        
    }

    private void filtroTipoOperacion(Long idtipooperacion) {
        try {
            lista = requisitooperacionDao.getRequisitoOperacionByTipoOperacion(idtipooperacion);
            grillaRequisitooperacion.setItems(lista);
            grillaRequisitooperacion.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     
}
