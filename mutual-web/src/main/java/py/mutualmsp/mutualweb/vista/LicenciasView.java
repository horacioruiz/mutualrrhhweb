package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.sun.webkit.dom.EventImpl;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.EmpresaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.LicenciasCompensarDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Empresa;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.LicenciasCompensar;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.formularios.LicenciasForm;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.InputModal;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 23/6/2016.
 */
public class LicenciasView extends CssLayout implements View {

    public static final String VIEW_NAME = "Licencias";
    //Button recibirMail = new Button("Recibir Mail");
    Button editarLicencias = new Button("");
    Button imprimirLicencias = new Button("");
    Button aceptarConformidad = new Button("Aceptar Conformidad");
    Button btnSearch = new Button();
//    ConfirmButton eliminarReclamo = new ConfirmButton("Eliminar Reclamo");
    Label labelTotalizador = new Label();

    int numRowSelected = 0;
    Licencias spd = new Licencias();

    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();

    LicenciaDetalleView formSolicitudDetalle = new LicenciaDetalleView();
    Button cerrarTicket = new Button("Cerrar Ticket");
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<Empresa> filterFilial = new ComboBox<>();
    ComboBox<Parametro> filterParametro = new ComboBox<>();
    ComboBox<Funcionario> filterAsignado = new ComboBox<>();
    Grid<Licencias> grid = new Grid<>(Licencias.class);
    LicenciasForm solicitudForm = new LicenciasForm();

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    RotacionesDao rotacionesController = ResourceLocator.locate(RotacionesDao.class);
    ParametroDao formularioController = ResourceLocator.locate(ParametroDao.class);
    LicenciasDao licenciasController = ResourceLocator.locate(LicenciasDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();

    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionesDetalleDao = ResourceLocator.locate(VacacionesDetalleDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionesDao = ResourceLocator.locate(RotacionesDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);
    MarcacionesDao marcacionesDao = ResourceLocator.locate(MarcacionesDao.class);

    Button nuevaLicencias = new Button("");//FALTA QUE CUANDO AL RECHAZAR LICENCIAS NO MUESTRE EN COMPARACION HORA EXTRA

    TextField filter = new TextField();

    private DateField fechaLog = new DateField();
    private TextArea comentarioLog = new TextArea("Comentarios");
    private Button descargar = new Button("Descargar Archivo");
    String filename;
    byte[] content;
    DecimalFormat decimalFormat = new DecimalFormat("###,###,##0");
    LicenciasCompensarDao solicitudDetalleController = ResourceLocator.locate(LicenciasCompensarDao.class);
    Licencias solicitudSeleccionado;
    List<Licencias> lista = new ArrayList<>();
    ComboBox<String> filterAprobado = new ComboBox<>();
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();
    EmpresaDao empresaDao =  ResourceLocator.locate(EmpresaDao.class);
    Map<String, Empresa> mapeoEmpresa = new HashMap();

    public LicenciasView() {
        try {
            System.out.println("Nueva instancia LicenciasView");
            setSizeFull();
            addStyleName("crud-view");
            HorizontalLayout horizontalLayout = createHorizontalLayout();

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia
                        .getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                }                
            } 
                       
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }

            if (UserHolder.get().getIdfuncionario() == null) {
                UserHolder.get().setIdfuncionario(usuarioController.getByUsuario(UserHolder.get().getId()).getIdfuncionario());
            }

            filterAprobado.setWidth(10f, TextField.UNITS_EM);
            filterParametro.setWidth(12f, TextField.UNITS_EM);
            filter.setWidth(8f, TextField.UNITS_EM);
            fechaDesde.setWidth(8f, TextField.UNITS_EM);
            fechaHasta.setWidth(8f, TextField.UNITS_EM);
            filter.addValueChangeListener(e -> limpiarDatos());

            btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnSearch.setIcon(VaadinIcons.SEARCH);

            fechaDesde.setPlaceholder("Desde");
            fechaHasta.setPlaceholder("Hasta");

            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

//            eliminarReclamo.setVisible(false);
            labelTotalizador.setCaption("Total de registros: 0");

            aceptarConformidad.setVisible(false);
            cerrarTicket.setVisible(false);
            editarLicencias.setVisible(false);
            imprimirLicencias.setVisible(false);
            filterFuncionario.setVisible(true);
            filterFilial.setVisible(true);
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
                    || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                filterFuncionario.setItems(funcionarioController.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            
            filterParametro.setItems(formularioController.listarPorTipoCodigo("licencias"));
            filterParametro.setPlaceholder("Filtre formularios");
            filter.setPlaceholder("Filtre código");
            filterParametro.setItemCaptionGenerator(Parametro::getDescripcion);
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            filterFilial.setItemCaptionGenerator(Empresa::getDescripcion);

            filterFilial.setItems(empresaDao.getFiliales());
            
            List<String> listEstados = new ArrayList<>();
            listEstados.add("APROBADO");
            listEstados.add("RECHAZADO");
            listEstados.add("PENDIENTE");
            filterAprobado.setItems(listEstados);
            filterAprobado.setPlaceholder("Filtre aprobados");
            filterFuncionario.setPlaceholder("Filtre Funcionario");
            filterFilial.setPlaceholder("Filtre Filial");
            filterParametro.setPlaceholder("Filtre Parametro");
            filterAsignado.setPlaceholder("Filtre Asignado");

            fechaLog.setVisible(false);
            comentarioLog.setVisible(false);

//            fechaDesde.addValueChangeListener(e -> findByAll());
//            fechaHasta.addValueChangeListener(e -> findByAll());
            btnSearch.addClickListener(e -> findByAll());
            
//            filter.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
//                @Override
//                public void handleAction(Object sender, Object target) {
//                    // Do nice stuff
//                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1
//                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1
//                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
//                            || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
//                            || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
//                        filterFuncionario.setItems(funcionarioController.listaFuncionario());
//                        if (filter.getValue() != null) {
//                            String num = filter.getValue();
//                            filterFuncionario.setValue(null);
//                            filterAprobado.setValue(null);
//                            filterParametro.setValue(null);
//                            fechaDesde.setValue(null);
//                            fechaHasta.setValue(null);
//                            filterFilial.setValue(null);
//                            cargarParametro(num);
//                        }
//                    }                        
//                }         
//                private void cargarParametro(String num) {
//                    grid.clearSortOrder();
//                    filter.setValue(num);
//                    try {
//                        Licencias s = licenciasController.listarPorIdHere(Long.parseLong(num));
//                        if (s.getId() == Long.parseLong(num)) {
//                            grid.setItems(s);
//                        }
//                    } catch (Exception e) {
//                        grid.clearSortOrder();
//                        grid.setItems(new ArrayList<>());
//                        labelTotalizador.setCaption("Total de registros: " + 0);
//                    } finally {
//                    }
//                }
//            });

            filter.addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                            || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())
                            || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                        filterFuncionario.setItems(funcionarioController.listaFuncionario());
                            if (filter.getValue() != null && !filter.isEmpty()) {
                                String num = filter.getValue();
                                filterFuncionario.setValue(null);
                                filterAprobado.setValue(null);
                                filterParametro.setValue(null);
                                fechaDesde.setValue(null);
                                fechaHasta.setValue(null);
                                filterFilial.setValue(null);
                                cargarParametro(num);
                            } else{
                                lista = licenciasController.listadeLicenciasSinConfirmar();
                                grid.setItems(lista);                                                        
                            }
                    }                                
            });               
            
//            List listFunc = this.funcionarioDao.getListFuncionario(value, idDependencia );
//            if (!listFunc.isEmpty()) {
//                this.grillaFuncionario.setItems(listFunc);
//            } else {
//                listFunc = new ArrayList();
//                this.grillaFuncionario.setItems(listFunc);
//            }
//            this.grillaFuncionario.setSizeFull();

            SimpleDateFormat formatSinHora = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatConHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("INFORMATICA") != -1
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                lista = licenciasController.listadeLicenciasSinConfirmar();
            } else {
                lista = new ArrayList<>();
            }

            grid.setItems(lista);
            labelTotalizador.setCaption("Total de registros: " + lista.size());
            grid.removeAllColumns();
            grid.addColumn(e -> {
                return e.getId();
            }).setCaption("Cód");
            grid.addComponentColumn(e -> {
                return e.getEstadoIcon();
            }).setCaption("Aprobado");
            grid.addColumn(e -> {
                return e.getFechaini() == null ? "--" : formatSinHora.format(e.getFechaini());
            }).setCaption("Fecha Inicio");
            grid.addColumn(e -> {
                return e.getFechafin() == null ? "--" : formatSinHora.format(e.getFechafin());
            }).setCaption("Fecha Fin");
            grid.addColumn(e -> {
                return e.getDescripcion();
            }).setCaption("Licencias");
            grid.addColumn(e -> {
                return e.getNombrefuncionario();
            }).setCaption("Funcionario");
            grid.addColumn(e -> {
                return (formatSinHora.format(e.getFechacreacion()));
            }).setCaption("Fecha Creación");
            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                grid.addComponentColumn(this::buildConfirmButton).setCaption("Confirmar");
            }
            if (mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                grid.addComponentColumn(this::eliminarSolicitud).setCaption("Eliminar");
            }
            grid.setSizeFull();
            descargar.setVisible(false);
            
            grid.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    try {
//                        if (numRowSelected >= 0) {
                        if (numRowSelected >= 0 && spd.getAprobado() == 1) {
                            InputModal inputModal = new InputModal("");
                            inputModal.openInModalPopup("Rechazo", "");
                            inputModal.getOkButton().addClickListener(listener -> {

                                Date fechaHoy = new Date();
                                fechaHoy.setHours(0);
                                fechaHoy.setMinutes(0);
                                fechaHoy.setSeconds(0);
                                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                                Date fechaSolicitud = spd.getFechaini();
                                fechaSolicitud.setHours(0);
                                fechaSolicitud.setMinutes(0);
                                fechaSolicitud.setSeconds(0);
                                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                                boolean val = true;
                                if (spd.getParametro().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                                    Date fechaInicio = spd.getFechaini();
                                    Date fechaFin = spd.getFechafin();

                                    List<Rotaciones> listRotacion = rotacionesController.listarPorFechas(fechaFin, fechaFin, spd.getFuncionario().getId());
                                    if (listRotacion.size() > 0) {
                                        Rotaciones rotacion = listRotacion.get(0);
                                        rotacion.setFecha(fechaInicio);
                                        rotacionesController.guardarRotaciones(rotacion);
                                    }
                                }
                                spd.setAprobado(2L);
                                spd.setFuncrrhh(UserHolder.get().getIdfuncionario());
                                spd.setRechazo(inputModal.getText());
                                licenciasController.guardarLicencias(spd);
                                grid.clearSortOrder();
                                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("INFORMATICA") != -1
                                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                    lista = licenciasController.listadeLicenciasSinConfirmar();
                                } else {
                                    lista = new ArrayList<>();
                                }
                                grid.setItems(lista);
                                inputModal.closePopup();
                            });
                        }
                    } catch (Exception e) {

                    } finally {
                    }

                }
            });

            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(horizontalLayout);
            grid.setSizeFull();
            verticalLayout.addComponent(grid);
            verticalLayout.setMargin(true);
            verticalLayout.setSpacing(true);
            verticalLayout.setSizeFull();
            verticalLayout.setExpandRatio(grid, 6F);
            verticalLayout.addComponent(labelTotalizador);
            verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
            HorizontalLayout horizontalLayout1 = crearSegundoGrid();
            horizontalLayout1.setSizeFull();
            verticalLayout.addStyleName("crud-main-layout");
            addComponent(verticalLayout);

            grid.addItemClickListener(listener -> {
                if (listener.getMouseEventDetails().isDoubleClick()) {
                    editarLicencias.setVisible(false);
                    imprimirLicencias.setVisible(false);
                    cerrarTicket.setVisible(false);

                    formSolicitudDetalle = new LicenciaDetalleView();
                    long idTicket = listener.getItem().getId();
//                    formSolicitudDetalle.setIdTicket(idTicket);
                    formSolicitudDetalle.setIdTicket(listener.getItem());
                    System.out.println("EL TICKET ES EL # " + idTicket);
                    UI.getCurrent().addWindow(formSolicitudDetalle);
                    formSolicitudDetalle.setVisible(true);
                }
                if (listener.getItem() != null) {
                    spd = listener.getItem();
                    numRowSelected = listener.getRowIndex();
                }
            });
            grid.asSingleSelect().addValueChangeListener(event -> {
                solicitudSeleccionado = event.getValue();
                editarLicencias.setVisible(true);
                imprimirLicencias.setVisible(true);
            });
            editarLicencias.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
                    try {
                        if (solicitudSeleccionado.getAprobado() > 0) {
                            Notification.show("La licencia ha sido confirmada, no es posible editar.", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            LicenciasForm soliForm = new LicenciasForm();
                            try {
                                Licencias lic = licenciasController.listarPorId(solicitudSeleccionado.getId());
                                if (lic.getId() != null) {
                                    soliForm.editarRegistro(lic);
                                }
                            } catch (Exception e) {
                                soliForm.editarRegistro(licenciasController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                            } finally {
                            }

                            UI.getCurrent().addWindow(soliForm);
                            soliForm.setVisible(true);

                            soliForm.setCancelListener(reclamo -> {
                                grid.clearSortOrder();
                            });

                            soliForm.setSaveListener(reclamo -> {
                                grid.clearSortOrder();
                                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                    lista = licenciasController.listadeLicenciasSinConfirmar();
                                } else {
                                    lista = new ArrayList<>();
                                }
                                grid.setItems(lista);
                                labelTotalizador.setCaption("Total de registros: " + lista.size());
                            });
                        }
                    } catch (Exception e) {
                        LicenciasForm soliForm = new LicenciasForm();
                        try {
                            soliForm.editarRegistro(licenciasController.listarPorId(solicitudSeleccionado.getId()));
                        } catch (Exception ex) {
                            soliForm.editarRegistro(licenciasController.listarPorIdFuncionario(solicitudSeleccionado.getId()));
                        } finally {
                        }
                        UI.getCurrent().addWindow(soliForm);
                        soliForm.setVisible(true);

                        soliForm.setCancelListener(reclamo -> {
                            grid.clearSortOrder();
                        });

                        soliForm.setSaveListener(reclamo -> {
                            grid.clearSortOrder();
                            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                                lista = licenciasController.listadeLicenciasSinConfirmar();
                            } else {
                                lista = new ArrayList<>();
                            }
                            grid.setItems(lista);
                            labelTotalizador.setCaption("Total de registros: " + lista.size());
                        });
                    } finally {
                    }

                }
            });
            imprimirLicencias.addClickListener(clickEvent -> {
                if (solicitudSeleccionado != null) {
//                    try {
//                        if (solicitudSeleccionado.getConfirmado()) {
//                        if (solicitudSeleccionado.getEncargado() != null) {
                    imprimirPDF();
//                        } else {
//                            Notification.show("La licencia no ha sido confirmada, no es posible imprimirla.", Notification.Type.HUMANIZED_MESSAGE);
//                        }
//                    } catch (Exception e) {
//                        imprimirPDF();
//                    } finally {
//                    }

                }
            });

            nuevaLicencias.addClickListener(clickEvent -> {
                LocalDate now = LocalDate.now(); // 2015-11-23
                LocalDate firstDay = now.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDay = now.with(lastDayOfYear());
                LocalDate today = LocalDate.now();

                LocalDate ld = now.plusYears(1L);
                LocalDate firstDaySecond = ld.with(firstDayOfYear()); // 2015-01-01
                LocalDate lastDaySecond = ld.with(lastDayOfYear());

                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDay), DateUtils.asDate(lastDay));

                List<Feriado> listFeriadoSecond = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(firstDaySecond), DateUtils.asDate(lastDaySecond));
                if (listFeriado.size() == 0) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + today.getYear() + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else if (listFeriadoSecond.size() == 0 && today.getMonthValue() == 12) {
                    Notification.show("Mensaje del Sistema", "Es necesario crear los feriados correspondiente al año " + (today.getYear() + 1) + ".", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    LicenciasForm solicitudForm = new LicenciasForm();
                    UI.getCurrent().addWindow(solicitudForm);

                    solicitudForm.nuevoRegistro();
                    solicitudForm.setVisible(true);
                    solicitudForm.setCancelListener(reclamo -> {
                        //form = null;
                        grid.clearSortOrder();
                    });
                    solicitudForm.setSaveListener(reclamo -> {
                        grid.clearSortOrder();
                        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                            lista = licenciasController.listadeLicenciasSinConfirmar();
                        } else {
                            lista = new ArrayList<>();
                        }
                        grid.setItems(lista);
                        labelTotalizador.setCaption("Total de registros: " + lista.size());
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HorizontalLayout crearSegundoGrid() {
        HorizontalLayout layout = new HorizontalLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        horizontalLayout.addComponent(descargar);
        verticalLayout.addComponent(horizontalLayout);
        CssLayout expander1 = new CssLayout();
        expander1.setSizeFull();
        expander1.setStyleName("expander");
        verticalLayout.addComponent(expander1);
        verticalLayout.setExpandRatio(expander1, 0.9F);
        verticalLayout.addComponent(fechaLog);
        comentarioLog.setSizeFull();
        verticalLayout.addComponent(comentarioLog);
        verticalLayout.setSpacing(false);

        layout.addComponent(verticalLayout);
        layout.setSpacing(true);
        return layout;
    }

    private void cargarParametro(String num) {
        grid.clearSortOrder();
        filter.setValue(num);
        try {
            Licencias s = licenciasController.listarPorIdHere(Long.parseLong(num));
            if (s.getId() == Long.parseLong(num)) {
                grid.setItems(s);
            }
        } catch (Exception e) {
            grid.clearSortOrder();
            grid.setItems(new ArrayList<>());
            labelTotalizador.setCaption("Total de registros: " + 0);
        } finally {
        }
    }

    private void findByAll() {
        if (!filter.getValue().trim().equals("")) {
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1
                    || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                    || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                if (filter.getValue() != null) {
                    String num = filter.getValue();
                    filterFuncionario.setValue(null);
                    filterFilial.setValue(null);
                    filterAprobado.setValue(null);
                    filterParametro.setValue(null);
                    fechaDesde.setValue(null);
                    fechaHasta.setValue(null);
                    cargarParametro(num);
                }
            }
        } else {
            //        DateUtils.asDate(fechaDesde.getValue())
            if (fechaDesde.getValue() != null && fechaHasta.getValue() != null) {
                if (fechaDesde.getValue().isBefore(fechaHasta.getValue()) || fechaDesde.getValue().isEqual(fechaHasta.getValue())) {
                    filtroConFecha();
                } else {
                    Notification.show("Mensaje del Sistema", "Fecha hasta debe ser mayor o igual a fecha desde.", Notification.Type.HUMANIZED_MESSAGE);
                }
            } else {
                Notification.show("Mensaje del Sistema", "Fecha desde y fecha hasta no deben quedar vacíos.", Notification.Type.HUMANIZED_MESSAGE);
            }
        }
    }

    private void filtroConFecha() {
        filter.setValue("");
        grid.clearSortOrder();
        //validar filial
        Long idfilial = 0l;
        if (filterFuncionario.getValue() != null && filterFilial.getValue() == null) 
            idfilial = filterFuncionario.getValue().getIdFilial();
        else if (filterFuncionario.getValue() == null && filterFilial.getValue() != null)
            idfilial = filterFilial.getValue().getId();
        else 
            idfilial = 0l;
        if (filterFuncionario.getValue() != null) {
            List<Licencias> listTicket = licenciasController.listadeLicenciasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(),
                    filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get(), 
                    DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios,
                    idfilial);
            grid.setItems(listTicket);
            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        } else {
            List<Licencias> listTicket = licenciasController
                    .listadeLicenciasByFuncionarioAndUsuario(0, 0, filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getNombreCompleto(),
                    filterAprobado.getValue() == null ? null : filterAprobado.getValue(), 
                    filterParametro.getValue() == null ? null : filterParametro.getValue().getDescripcion(), UserHolder.get(),
                    DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), mapeoFuncionarios,
                    idfilial);
            grid.setItems(listTicket);
            labelTotalizador.setCaption("Total de registros: " + listTicket.size());
        }
    }

    private HorizontalLayout createHorizontalLayout() {
        nuevaLicencias.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        nuevaLicencias.setIcon(VaadinIcons.PLUS_CIRCLE);
        editarLicencias.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        editarLicencias.setIcon(VaadinIcons.EDIT);
        imprimirLicencias.addStyleName(MaterialTheme.BUTTON_ROUND);
        imprimirLicencias.setIcon(VaadinIcons.PRINT);
        aceptarConformidad.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        aceptarConformidad.setIcon(FontAwesome.PARAGRAPH);
        cerrarTicket.addStyleName(MaterialTheme.BUTTON_ROUND + " " + ValoTheme.BUTTON_DANGER);
        cerrarTicket.setIcon(FontAwesome.EDIT);

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        HorizontalLayout layoutIzquierdo = new HorizontalLayout();
        layoutIzquierdo.addComponent(filterFuncionario);
        layoutIzquierdo.addComponent(filterFilial);
        layoutIzquierdo.addComponent(nuevaLicencias);
        
        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        verticalLayoutIzquierda.addComponent(layoutIzquierdo);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filterAprobado);
        layoutFuncEstado.setExpandRatio(filterAprobado, 2);
        layoutFuncEstado.addComponent(filterParametro);
        layoutFuncEstado.setExpandRatio(filterParametro, 2);
        layoutFuncEstado.addComponent(filter);
        layoutFuncEstado.setExpandRatio(filter, 2);
        layoutFuncEstado.addComponent(fechaDesde);
        layoutFuncEstado.setExpandRatio(fechaDesde, 2);
        layoutFuncEstado.addComponent(fechaHasta);
        layoutFuncEstado.setExpandRatio(fechaHasta, 2);
        layoutFuncEstado.addComponent(btnSearch);
        layoutFuncEstado.setExpandRatio(btnSearch, 2);
        verticalLayoutIzquierda.addComponent(layoutFuncEstado);

        layout.addComponent(verticalLayoutIzquierda);

        layout.addComponent(cerrarTicket);
        layout.addComponent(aceptarConformidad);
        layout.addComponent(editarLicencias);
        layout.addComponent(imprimirLicencias);
        layout.setStyleName("top-bar");
        return layout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private void fileReceived(String filename, String mime, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public void limpiarLog() {
        grid.clearSortOrder();
        comentarioLog.setValue("");
        fechaLog.setValue(null);
        fechaLog.setEnabled(false);
        comentarioLog.setEnabled(false);
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private Button buildConfirmButton(Licencias p) {
        Button button = new Button(VaadinIcons.CHECK_SQUARE_O);
        if (p.getAprobado() == 0) {
            button.setEnabled(true);
            button.setVisible(true);
        } else {
            button.setEnabled(false);
            button.setVisible(false);
        }
        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> aprobarLicencias(p));
        return button;
    }

    private Button eliminarSolicitud(Licencias p) {
        Button button = new Button(VaadinIcons.ERASER);
        try {
            if (p.getFuncrrhh().getId() == null) {
                button.setEnabled(true);
                button.setVisible(true);
            } else {
                button.setEnabled(false);
                button.setVisible(false);
            }
        } catch (Exception e) {
            button.setEnabled(true);
            button.setVisible(true);
        } finally {
        }

        button.addStyleName(ValoTheme.BUTTON_SMALL + " " + MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
        button.addClickListener(e -> eliminarLicencias(p));
        return button;
    }

    private void cargarVacacionesDisponible(Licencias s) {
        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(s.getFuncionario().getId());
        for (Vacaciones listado : listVacaciones) {
            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        }
    }

    private void eliminarLicencias(Licencias spd) {
        try {
//                        if (numRowSelected >= 0) {
            if (spd.getId() >= 0 && spd.getAprobado() == 0) {
                InputModal inputModal = new InputModal("");
                inputModal.openInModalPopup("Rechazo", "");
                inputModal.getOkButton().addClickListener(listener -> {

                    Date fechaHoy = new Date();
                    fechaHoy.setHours(0);
                    fechaHoy.setMinutes(0);
                    fechaHoy.setSeconds(0);
                    java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                    Date fechaSolicitud = spd.getFechaini();
                    fechaSolicitud.setHours(0);
                    fechaSolicitud.setMinutes(0);
                    fechaSolicitud.setSeconds(0);
                    java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                    boolean val = false;
                    if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                        val = true;
                    } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                        val = false;
                    } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                        val = true;
                    }

                    if (val) {
                        if (spd.getParametro().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                            Date fechaInicio = spd.getFechaini();
                            Date fechaFin = spd.getFechafin();

                            List<Rotaciones> listRotacion = rotacionesController.listarPorFechas(fechaFin, fechaFin, spd.getFuncionario().getId());
                            if (listRotacion.size() > 0) {
                                Rotaciones rotacion = listRotacion.get(0);
                                rotacion.setFecha(fechaInicio);
                                rotacionesController.guardarRotaciones(rotacion);
                            }
                        }
                        spd.setAprobado(2L);
                        spd.setFuncrrhh(UserHolder.get().getIdfuncionario());
                        spd.setRechazo(inputModal.getText());
                        licenciasController.guardarLicencias(spd);
                        grid.clearSortOrder();
                        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                                || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                                || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                            lista = licenciasController.listadeLicenciasSinConfirmar();
                        } else {
                            lista = new ArrayList<>();
                        }
                        grid.setItems(lista);
                        inputModal.closePopup();
                    } else {
                        Notification.show("Mensaje del sistema", "La licencia generada ya ha sido efectuada.", Notification.Type.ERROR_MESSAGE);
                    }
                });
            }
        } catch (Exception e) {

        } finally {
        }
    }

    private void aprobarLicencias(Licencias p) {
        if (p.getEncargado() == null || p.getEncargado2() == null) {
            Notification.show("Mensaje del Sistema", "El encargado no confirmó la licencia", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            ConfirmButton confirmMessage = new ConfirmButton("");
            confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea aprobar la licencia?", "20%");
            confirmMessage.getOkButton().addClickListener(e -> {
                p.setAprobado(1L);
                p.setFuncrrhh(UserHolder.get().getIdfuncionario());
                Licencias soli = licenciasController.guardarLicencias(p);

                grid.clearSortOrder();
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                        || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    lista = licenciasController.listadeLicenciasSinConfirmar();
                } else {
                    lista = new ArrayList<>();
                }
                if (p.getParametro().getCodigo().trim().equalsIgnoreCase("cambio_rotacion")) {
                    List<Rotaciones> listRotacion = rotacionesDao.listarPorFechas2(p.getFechaini(), p.getFechaini(), p.getFuncionario().getId());
                    if (listRotacion.size() > 0) {
                        Rotaciones rota = listRotacion.get(0);
                        rota.setFecha(p.getFechafin());
                        rotacionesDao.guardarRotaciones(rota);
                    }
                } else if (p.getParametro().getCodigo().trim().equalsIgnoreCase("permiso_sin_goce_salario") || p.getParametro().getCodigo().trim().equalsIgnoreCase("reposo_medico")) {
                    List<LicenciasCompensar> listaCompensars = solicitudDetalleController.listarPorIdLicencia(p.getId());
                    if (listaCompensars.size() > 0) {
                        Date fechaInicio = new Date();
                        Date fechaFin = new Date();
                        int num = 0;
                        Funcionario func = new Funcionario();
                        for (LicenciasCompensar listaCompensar : listaCompensars) {
                            func.setId(soli.getFuncionario().getId());

                            if (num == 0) {
                                fechaInicio = listaCompensar.getFechacompensar();
                            }
                            fechaFin = listaCompensar.getFechacompensar();
                            num++;
                        }
                        Suspenciones rota = new Suspenciones();
                        rota.setId(null);
                        rota.setFuncionario(func);
                        rota.setNombrefuncionario(soli.getNombrefuncionario());
                        rota.setAreafunc(soli.getAreafunc());
                        rota.setCargofunc(soli.getCargofunc());
                        rota.setDescripcion(soli.getParametro().getDescripcion());

                        Parametro param = new Parametro();
                        param.setId(soli.getParametro().getId());

                        rota.setParametro(param);
                        rota.setFechadesde(fechaInicio);
                        rota.setFechahasta(fechaFin);

                        suspencionDao.guardarSuspenciones(rota);
                    }
                } else if (p.getParametro().getCodigo().trim().equalsIgnoreCase("licencia_obligaciones") || p.getParametro().getCodigo().trim().equalsIgnoreCase("vacuna_covid_acompanamiento")) {
                    List listArray = marcacionesDao.getByFilter(soli.getFuncionario(), soli.getFechaini(), soli.getFechafin(), null, 0);
                    if (listArray.isEmpty()) {
                        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                        Calendar start = Calendar.getInstance();
                        start.setTime(soli.getFechaini());

                        Calendar end = Calendar.getInstance();
                        end.setTime(soli.getFechafin());

                        while (!start.after(end)) {
                            Date targetDay = start.getTime();
                            // Do Work Here
                            if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                Marcaciones marcacion = new Marcaciones();
                                marcacion.setHorarioFuncionario(hf);

                                marcacion.setFecha(targetDay);
                                marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                marcacion.setInmarcada(soli.getHoraini());
                                marcacion.setOutmarcada(soli.getHorafin());
                                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sabado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setInasignada(hl.getEntrada());
                                    marcacion.setOutasignada(hl.getSalida());
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                long minutesEntSal = 0;
                                try {
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } finally {
                                }

                                if (minutesEntSal >= 450) {
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
                                    marcacion.setMintrabajada(minutesEntSal);
                                }

//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    if (minutesEntSal >= 450) {
                                        marcacion.setMintrabajada(minutesEntSal - 40);
                                    } else {
                                        marcacion.setMintrabajada(minutesEntSal);
                                    }
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    if (minutesEntSal >= 450) {
                                        marcacion.setMintrabajada(minutesEntSal - 40);
                                    } else {
                                        marcacion.setMintrabajada(minutesEntSal);
                                    }
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                                marcacion.setSolicitud(null);
                                marcacion.setObservacion("LIC OBL");
                                marcacionesDao.guardar(marcacion);
                            }
                            start.add(Calendar.DATE, 1);
                        }
                    } else {
                        HorarioFuncionario hf = hfDao.listarPorIdFuncionario(soli.getFuncionario().getId());
                        Calendar start = Calendar.getInstance();
                        start.setTime(soli.getFechaini());

                        Calendar end = Calendar.getInstance();
                        end.setTime(soli.getFechafin());
                        Timestamp tsHora = (soli.getHoraini());
                        Timestamp tsHoraSolicitud = tsHora;

                        while (!start.after(end)) {
                            Date targetDay = start.getTime();

                            Marcaciones marcacion = marcacionesDao.getByFilter(hf.getFuncionario(), targetDay, targetDay, null, 0).get(0);
                            boolean isVacation = false;
                            try {
                                if (marcacion.getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                                    isVacation = true;
                                }
                            } catch (Exception ex) {
                            } finally {
                            }
                            // Do Work Here
                            if (isVacation) {
                                marcacion.setObservacion(marcacion.getObservacion() == null || marcacion.getObservacion().equalsIgnoreCase("") ? soli.getId() + " " + "LIC OBL" : marcacion.getObservacion() + " - " + soli.getId() + " " + "LIC OBL");
                                marcacionesDao.guardar(marcacion);
                            } else if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                                marcacion.setHorarioFuncionario(hf);

                                marcacion.setFecha(targetDay);
                                marcacion.setInasignada(hf.getHorarioLaboral().getEntrada());
                                marcacion.setOutasignada(hf.getHorarioLaboral().getSalida());
                                marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                                if ((marcacion.getInmarcada().getHours() + marcacion.getOutmarcada().getMinutes()) == 0) {
                                    marcacion.setInmarcada(soli.getHoraini());
                                    marcacion.setOutmarcada(soli.getHorafin());
                                } else {

                                    Timestamp tsInMarcada = new Timestamp(targetDay.getTime());
                                    tsInMarcada.setHours(marcacion.getInmarcada().getHours());
                                    tsInMarcada.setMinutes(marcacion.getInmarcada().getMinutes());
                                    tsInMarcada.setSeconds(marcacion.getInmarcada().getSeconds());

                                    Timestamp tsOutMarcada = new Timestamp(targetDay.getTime());
                                    tsOutMarcada.setHours(marcacion.getOutmarcada().getHours());
                                    tsOutMarcada.setMinutes(marcacion.getOutmarcada().getMinutes());
                                    tsOutMarcada.setSeconds(marcacion.getOutmarcada().getSeconds());

                                    if (tsHoraSolicitud.before(tsInMarcada)) {
                                        marcacion.setInmarcada(tsHoraSolicitud);
                                    }
                                    if (soli.getHorafin().after(tsOutMarcada)) {
                                        marcacion.setOutmarcada(soli.getHorafin());
                                    }
                                }

                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    HorarioLaboral hl = hlDao.getByValue("sabado");
                                    hf.setHorarioLaboral(hl);
                                    marcacion.setInasignada(hl.getEntrada());
                                    marcacion.setOutasignada(hl.getSalida());
                                }

//            Si llego temprano se calcula igual desde las 7:20 hasta su salida para las horas trabajadas
                                System.out.println("MARCACION -->> " + marcacion.getHorarioFuncionario().getIdreloj() + " -- " + marcacion.getFecha() + " -- " + marcacion.getOutmarcada() + " -- " + hf.getHorarioLaboral().getEntrada().getTime());
                                long minutesEntSal = 0;
                                try {
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } catch (Exception ec) {
                                    marcacion.setOutmarcada(marcacion.getInmarcada());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                } finally {
                                }

                                if (minutesEntSal >= 450) {
                                    marcacion.setMintrabajada(minutesEntSal - 40);
                                } else {
                                    marcacion.setMintrabajada(minutesEntSal);
                                }
//            }
                                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    marcacion.setMinllegadatardia(0L);
                                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                                    marcacion.setMinllegadatemprana(0L);
                                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                                    int hereDif = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                                    marcacion.setMinllegadatardia(Long.parseLong(hereDif + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    if (minutesEntSal >= 450) {
                                        marcacion.setMintrabajada(minutesEntSal - 40);
                                    } else {
                                        marcacion.setMintrabajada(minutesEntSal);
                                    }
                                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());

                                    if (minutesEntSal >= 450) {
                                        marcacion.setMintrabajada(minutesEntSal - 40);
                                    } else {
                                        marcacion.setMintrabajada(minutesEntSal);
                                    }
                                }
                                if (isWeekendSaturday(DateUtils.asLocalDate(marcacion.getFecha()))) {
                                    marcacion.setMintrabajada(marcacion.getMintrabajada() + 40);
                                }
                                try {
                                    if (marcacion.getSolicitud() == null) {
                                        marcacion.setSolicitud(null);
                                    } else {
                                        marcacion.setObservacion(marcacion.getObservacion() == null || marcacion.getObservacion().equalsIgnoreCase("") ? marcacion.getSolicitud().getId() + " " + marcacion.getSolicitud().getFormulario().getAbrev() : marcacion.getObservacion() + " - " + marcacion.getSolicitud().getId() + " " + marcacion.getSolicitud().getFormulario().getAbrev());
                                    }
                                } catch (Exception ex) {
                                    marcacion.setSolicitud(null);
                                } finally {
                                }
                                marcacionesDao.guardar(marcacion);
                            }
                            start.add(Calendar.DATE, 1);
                        }
                    }
                }
//                if (soli.getFuncionario().getEmail() != null) {
//                    try {
//                        Utilidades.sendeEmail("Mutual Web RRHH",
//                                "Su Licencia N° " + soli.getId() + " ha sido aprobado exitosamente!.",
//                                Arrays.asList(soli.getFuncionario().getEmail()),
//                                "Mutual RRHH");
//                    } catch (EmailException ex) {
//                        System.out.println("EXCEPTION -> " + ex.getLocalizedMessage());
//                    } catch (IOException ex) {
//                        System.out.println("EXCEPTION -> " + ex.getLocalizedMessage());
//                    }
//                }

                grid.setItems(lista);
                labelTotalizador.setCaption("Total de registros: " + lista.size());
                confirmMessage.closePopup();
            });
            confirmMessage.getCancelButton().addClickListener(e -> {
                confirmMessage.closePopup();

                InputModal inputModal = new InputModal("");
                inputModal.openInModalPopup("Rechazo", "");
                inputModal.getOkButton().addClickListener(listener -> {
                    p.setAprobado(2L);
                    p.setFuncrrhh(UserHolder.get().getIdfuncionario());
                    p.setRechazo(inputModal.getText());
                    licenciasController.guardarLicencias(p);
                    grid.clearSortOrder();
                    if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                            || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1
                            || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                        lista = licenciasController.listadeLicenciasSinConfirmar();
                    } else {
                        lista = new ArrayList<>();
                    }
//                    if (p.getFuncionario().getEmail() != null) {
//                        try {
//                            Utilidades.sendeEmail("Mutual Web RRHH",
//                                    "Su Licencia N° " + p.getId() + " ha sido rechazada. El motivo es el siguiente: " + inputModal.getText(),
//                                    Arrays.asList(p.getFuncionario().getEmail()),
//                                    "Mutual RRHH");
//                        } catch (IOException exs) {
//                            System.out.println("EXCEPTION -> " + exs.getLocalizedMessage());
//                        } catch (EmailException exs) {
//                            System.out.println("EXCEPTION -> " + exs.getLocalizedMessage());
//                        }
//                    }

                    grid.setItems(lista);
                    labelTotalizador.setCaption("Total de registros: " + lista.size());
                    inputModal.closePopup();
                });
            });
        }
    }

    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void guardarVacaciones(Licencias solicitud, double tomada, double restante) {
        Calendar cal = Calendar.getInstance();

        Vacaciones vacas = new Vacaciones();
        vacas.setPeriodo(cal.get(Calendar.YEAR) + "");
        vacas.setCantdiavaca(0d);
        vacas.setCantdiatomada(tomada);
        vacas.setCantdiarestante(restante - tomada);
        vacas.setFuncionario(solicitud.getFuncionario());
//        vacas.setLicencias(solicitud);

        vacacionesDao.guardar(vacas);
    }

    private void imprimirPDF() {
        if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_obligaciones") 
                || solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("vacuna_covid_acompanamiento")) {
            Notification.show("Mensaje del sistema", "La licencia no dispone de documentos para ser visualizada.", Notification.Type.ERROR_MESSAGE);
        } else {
            Map pSQL = new HashMap<String, Object>();
            InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

            if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                pSQL.put("repJsonString", "{\"ventas\": " + cargarLicenciaCompensar() + "}");
            } else {
                pSQL.put("repJsonString", "{\"ventas\": " + cargarLicenciaSinCompensar() + "}");
            }

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
            if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                pSQL.put("subRepSucursal", "LICENCIA A COMPENSAR");
            } else {
                pSQL.put("subRepSucursal", "LICENCIAS");
            }

            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImg);
            StreamResource.StreamSource source = new StreamResource.StreamSource() {

                public InputStream getStream() {
                    byte[] b = null;
                    String archivo = "";
                    if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                        archivo += "licencia_compensar";
                    } else {
                        archivo += "licencia_sin_compensar";
                    }
                    try {
                        b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                    } catch (JRException ex) {
                        ex.printStackTrace();
                    }
                    return new ByteArrayInputStream(b);
                }
            };

            String archivo = "";
            if (solicitudSeleccionado.getParametro().getCodigo().toLowerCase().equalsIgnoreCase("licencia_compensar")) {
                archivo += "licencia_compensar";
            } else {
                archivo += "licencia_sin_compensar";
            }

            StreamResource resource = new StreamResource(source, archivo + ".pdf");
            resource.setCacheTime(0);
            resource.setMIMEType("application/pdf");

            Window window = new Window();
            window.setWidth(800, Sizeable.Unit.PIXELS);
            window.setHeight(600, Sizeable.Unit.PIXELS);
            window.setModal(true);
            window.center();
            BrowserFrame pdf = new BrowserFrame("test", resource);
            pdf.setSizeFull();

            window.setContent(pdf);
            getUI().addWindow(window);
        }
    }

    private JSONArray cargarLicenciaCompensar() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
        int num = 0;
        for (LicenciasCompensar solicitudProduccionDetalle : solicitudDetalle) {
            JSONObject jsonObj = new JSONObject();

            int minFin = ((solicitudProduccionDetalle.getHorafin().getHours() * 60) + solicitudProduccionDetalle.getHorafin().getMinutes());
            int minInicio = ((solicitudProduccionDetalle.getHoraini().getHours() * 60) + solicitudProduccionDetalle.getHoraini().getMinutes());

            int minFinHere = ((solicitudProduccionDetalle.getLicencia().getHorafin().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHorafin().getMinutes());
            int minInicioHere = ((solicitudProduccionDetalle.getLicencia().getHoraini().getHours() * 60) + solicitudProduccionDetalle.getLicencia().getHoraini().getMinutes());

            jsonObj.put("codigo", solicitudSeleccionado.getId());
            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());
            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));

            jsonObj.put("fechaPermiso", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
            jsonObj.put("desdePermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHoraini()));
            jsonObj.put("hastaPermiso", formatHor.format(solicitudProduccionDetalle.getLicencia().getHorafin()));
            try {
                long millis = (minFinHere - minInicioHere);
                jsonObj.put("cantPermiso", sdfHM.format(sdf.parse(millis + "")));
            } catch (ParseException ex) {
                jsonObj.put("cantPermiso", "00.00");
            }
            jsonObj.put("motivoPermiso", solicitudProduccionDetalle.getLicencia().getMotivo());

            jsonObj.put("fechaCompensacion", formatter.format(solicitudProduccionDetalle.getFechacompensar()));
            jsonObj.put("desdeCompensacion", formatHor.format(solicitudProduccionDetalle.getHoraini()));
            jsonObj.put("hastaCompensacion", formatHor.format(solicitudProduccionDetalle.getHorafin()));
            try {
                long millis = minFin - minInicio;
                jsonObj.put("cantCompensacion", sdfHM.format(sdf.parse(millis + "")));
            } catch (ParseException ex) {
                jsonObj.put("cantCompensacion", "00.00");
            }
            jsonObj.put("tareaCompensacion", solicitudProduccionDetalle.getObservacion());

            jsonObj.put("num", (num + 1));
            num++;

            jsonArrayDato.add(jsonObj);
        }
        return jsonArrayDato;
    }

    private JSONArray cargarLicenciaSinCompensar() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<LicenciasCompensar> solicitudDetalle = solicitudDetalleController.listarPorIdLicencia(solicitudSeleccionado.getId());
        int num = 0;
        for (LicenciasCompensar solicitudProduccionDetalle : solicitudDetalle) {
            JSONObject jsonObj = new JSONObject();

            jsonObj.put("codigo", solicitudSeleccionado.getId());
            jsonObj.put("funcionario", solicitudProduccionDetalle.getLicencia().getNombrefuncionario());
            jsonObj.put("area", solicitudProduccionDetalle.getLicencia().getAreafunc().toUpperCase());
            jsonObj.put("seccion", solicitudProduccionDetalle.getLicencia().getCargofunc().toUpperCase());
            jsonObj.put("fechaIngreso", formatter.format(solicitudSeleccionado.getFechacreacion()));

            switch (solicitudProduccionDetalle.getLicencia().getParametro().getCodigo().toLowerCase()) {
                case "licencia_matrimonio":
                    jsonObj.put("marca1", "X");
                    jsonObj.put("desde1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta1", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "licencia_duelo":
                    jsonObj.put("marca2", "X");
                    jsonObj.put("desde2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta2", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "estudio_pap":
                    jsonObj.put("marca3", "X");
                    jsonObj.put("desde3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta3", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "reposo_medico":
                    jsonObj.put("marca4", "X");
                    jsonObj.put("desde4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta4", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "cambio_rotacion":
                    jsonObj.put("marca5", "X");
                    jsonObj.put("desde5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta5", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca6", " ");
                    jsonObj.put("desde6", " ");
                    jsonObj.put("hasta6", " ");
                    break;
                case "permiso_sin_goce_salario":
                    jsonObj.put("marca6", "X");
                    jsonObj.put("desde6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechaini()));
                    jsonObj.put("hasta6", formatter.format(solicitudProduccionDetalle.getLicencia().getFechafin()));
                    jsonObj.put("marca1", " ");
                    jsonObj.put("desde1", " ");
                    jsonObj.put("hasta1", " ");
                    jsonObj.put("marca2", " ");
                    jsonObj.put("desde2", " ");
                    jsonObj.put("hasta2", " ");
                    jsonObj.put("marca3", " ");
                    jsonObj.put("desde3", " ");
                    jsonObj.put("hasta3", " ");
                    jsonObj.put("marca4", " ");
                    jsonObj.put("desde4", " ");
                    jsonObj.put("hasta4", " ");
                    jsonObj.put("marca5", " ");
                    jsonObj.put("desde5", " ");
                    jsonObj.put("hasta5", " ");
                    break;
            }

            jsonObj.put("num", (num + 1));
            num++;

            jsonArrayDato.add(jsonObj);
        }
        return jsonArrayDato;
    }

    private void limpiarDatos() {
        if (filter.getValue().equalsIgnoreCase("")) {
            fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
            fechaHasta.setValue(DateUtils.asLocalDate(new Date()));
        }
    }
}
