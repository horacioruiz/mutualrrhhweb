/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dto.UsuarioMarcacionDTO;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class ImportarUsuarios extends CssLayout implements View {

    public static final String VIEW_NAME = "Importar ID Reloj";
    Grid<UsuarioMarcacionDTO> grillaCargo = new Grid<>(UsuarioMarcacionDTO.class);
    TextField txtfFiltro = new TextField();
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    FuncionarioDao funcioanrioDao = ResourceLocator.locate(FuncionarioDao.class);
    HorarioFuncionarioDao horarioFuncionarioDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    List<UsuarioMarcacionDTO> lista = new ArrayList<>();
    Button btnNuevo = new Button("Seleccionar");
    Button btnCargarDatos = new Button("");
    Button btnImportar = new Button("");
    final Image image = new Image("Imagen");

    String fileName = "";
    String ubicacion = "";
    FileOutputStream fos = null;
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = new Upload("", receiver);
    String destinarariosString = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";
    
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public ImportarUsuarios() {
        setSizeFull();
        addStyleName("crud-view");
        
        List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
        List<Dependencia> listDependencia = new ArrayList<>();
        List<Funcionario> listFunc = new ArrayList<>();
        for (Dependencia dependencia : depen) {
            listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                } 
        }
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getId(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        btnCargarDatos.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_FRIENDLY);
        btnCargarDatos.setIcon(VaadinIcons.UPLOAD);
        btnCargarDatos.setEnabled(false);
        btnImportar.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnImportar.setIcon(VaadinIcons.UPLOAD_ALT);
        btnImportar.setVisible(false);
        btnNuevo.setEnabled(true);
        image.setVisible(false);

        image.setWidth("65%");
        image.setHeight("45%");
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        image.setSource(new FileResource(new File(basepath
                + "/WEB-INF/images/cargando.gif")));

        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponent(txtfFiltro);
        topLayout.addComponent(upload);
        topLayout.addComponent(btnCargarDatos);
        topLayout.addComponent(btnImportar);
        topLayout.setSpacing(true);
        
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Archivo Seleccionado");
        txtfFiltro.setEnabled(false);

        btnNuevo.addClickListener(e -> {
            grillaCargo.asSingleSelect().clear();
        });
        btnCargarDatos.addClickListener(e -> {
            try {
                leerExcel(file.getAbsolutePath());
                btnImportar.setVisible(true);
            } catch (FileNotFoundException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } catch (IOException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        });
        btnImportar.addClickListener(e -> {
            if (!lista.isEmpty()) {
                image.setVisible(true);
                grillaCargo.setVisible(false);
                for (UsuarioMarcacionDTO usuarioMarcacionDTO : lista) {
                    String nombreApellido = usuarioMarcacionDTO.getNombreApellido();
                    Funcionario func = funcioanrioDao.listarPorNombreApellido(nombreApellido);
                    if (func.getId() == null) {
                        func = funcioanrioDao.listarPorApellidoNombre(nombreApellido);
                    }
                    HorarioFuncionario marcacion = horarioFuncionarioDao.getById(usuarioMarcacionDTO.getId());
                    if (marcacion.getIdreloj() == null) {
                        try {
                            marcacion = new HorarioFuncionario();
                            marcacion.setFuncionario(func);
                            marcacion.setHorarioLaboral(null);
                            marcacion.setIdreloj(usuarioMarcacionDTO.getId());

                            horarioFuncionarioDao.guardarHorarioFuncionario(marcacion);
                        } catch (Exception ex) {
                            System.out.println("-->>" + ex.fillInStackTrace());
                        } finally {
                        }
                    }
                }
                grillaCargo.clearSortOrder();
                lista = new ArrayList<>();
                txtfFiltro.setValue("");
                btnCargarDatos.setEnabled(false);
                btnImportar.setVisible(false);
                image.setVisible(false);
                grillaCargo.setVisible(true);
                Notification.show("Datos importados exitosamente", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                Notification.show("Sin detalles cargados", Notification.Type.ERROR_MESSAGE);
            }
        });

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        grillaCargo.setSizeFull();

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaCargo);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaCargo, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(image);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(image, 1);

        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        addComponent(barAndGridLayout);
    }

    private void leerExcel(String absolutePath) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(absolutePath);
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                XSSFRow row = ((XSSFRow) rows.next());
                Iterator cells = row.cellIterator();
                int i = 0;
                UsuarioMarcacionDTO umDTO = new UsuarioMarcacionDTO();
                while (cells.hasNext()) {
                    XSSFCell cell = (XSSFCell) cells.next();
                    String result = cell.getStringCellValue();
                    if (i == 0) {
                        try {
                            umDTO.setId(Long.parseLong(result));
                        } catch (Exception e) {
                        } finally {
                        }
                    } else {
                        if (!Objects.isNull(umDTO.getId())) {
                            umDTO.setNombreApellido(result);
                        }
                        lista.add(umDTO);
                    }
                    i++;
                    System.out.println(result);
                }
            }
            grillaCargo.clearSortOrder();
            grillaCargo.setItems(lista);
            grillaCargo.removeAllColumns();
            grillaCargo.addColumn(UsuarioMarcacionDTO::getId).setCaption("ID RELOJ");
            grillaCargo.addColumn(UsuarioMarcacionDTO::getNombreApellido).setCaption("FUNCIONARIO");
            grillaCargo.setSizeFull();
        } catch (IOException e) {
            System.out.println("1) " + e.fillInStackTrace());
            e.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

//    private void leerExcel(String absolutePath) throws FileNotFoundException, IOException {
//        FileInputStream fis = new FileInputStream(new File(absolutePath));
////creating workbook instance that refers to .xls file  
//        HSSFWorkbook wb = new HSSFWorkbook(fis);
////creating a Sheet object to retrieve the object  
//        HSSFSheet sheet = wb.getSheetAt(0);
////evaluating cell type   
//        FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
//        for (Row row : sheet) //iteration over row using for each loop  
//        {
//            for (Cell cell : row) //iteration over cell using for each loop  
//            {
//                switch (formulaEvaluator.evaluateInCell(cell).getCellType()) {
//                    case Cell.CELL_TYPE_NUMERIC:   //field that represents numeric cell type  
////getting the value of the cell as a number  
//                        System.out.print(cell.getNumericCellValue() + "\t\t");
//                        break;
//                    case Cell.CELL_TYPE_STRING:    //field that represents string cell type  
////getting the value of the cell as a string  
//                        System.out.print(cell.getStringCellValue() + "\t\t");
//                        break;
//                }
//            }
//            System.out.println();
//        }
//    }
    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {
            try {
//                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-archivos/" + filename);
                fos = new FileOutputStream(file);
                btnCargarDatos.setEnabled(true);
                txtfFiltro.setValue(file.getName());
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
//            image.setVisible(true);
//            image.setSource(new FileResource(file));

        }
    }
};
