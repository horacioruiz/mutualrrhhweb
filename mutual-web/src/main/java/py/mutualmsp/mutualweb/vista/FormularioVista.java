/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.formularios.FormularioForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class FormularioVista extends CssLayout implements View {

    public static final String VIEW_NAME = "Formulario";
    Grid<Formulario> grillaCargo = new Grid<>(Formulario.class);
    TextField txtfFiltro = new TextField("Filtro");
    FormularioDao formularioDao = ResourceLocator.locate(FormularioDao.class);
    List<Formulario> lista = new ArrayList<>();
    Button btnNuevo = new Button("");
    FormularioForm formularioForm = new FormularioForm();
    
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public FormularioVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
        List<Dependencia> listDependencia = new ArrayList<>();
        List<Funcionario> listFunc = new ArrayList<>();
        for (Dependencia dependencia : depen) {
            listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                } 
        }
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getId(), funcio);
        }

        btnNuevo.addStyleName(MaterialTheme.BUTTON_ROUND + " " +MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);

        formularioForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");

        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));

        btnNuevo.addClickListener(e -> {
            grillaCargo.asSingleSelect().clear();
            formularioForm.setCargo(new Formulario());
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaCargo, formularioForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaCargo, 1);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");

        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            btnNuevo.setEnabled(true);
        } else {
            btnNuevo.setEnabled(false);
        }

        try {
            lista = formularioDao.listaFormulario();
            grillaCargo.setItems(lista);
            grillaCargo.removeAllColumns();
            grillaCargo.addColumn(Formulario::getId).setCaption("Id Formulario");
            grillaCargo.addColumn(Formulario::getDescripcion).setCaption("Descripción");
//            grillaCargo.addColumn(Formulario::getEstadoIcon).setCaption("Estado");
//            Resource res = new ThemeResource("C:\\Users\\hruiz\\Documents\\Logopequeño2.png");

// Display the image without caption
//            Image image = new Image(null, res);
//            grillaCargo.addColumn(i -> new Image("C:\\Users\\hruiz\\Documents\\Logopequeño2.png", "alt text")).setCaption("Estado");
            grillaCargo.addComponentColumn(Formulario::getEstadoIcon).setCaption("Estado");
            grillaCargo.setSizeFull();
            grillaCargo.asSingleSelect().addValueChangeListener(e -> {
                if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                    if (e.getValue() == null) {
                        formularioForm.setVisible(false);
                    } else {
                        formularioForm.setCargo(e.getValue());
                        formularioForm.setViejo();
                    }
                }
            });

            formularioForm.setGuardarListener(r -> {
                grillaCargo.clearSortOrder();
                lista = formularioDao.listaFormulario();
                grillaCargo.setItems(lista);
            });
            formularioForm.setBorrarListener(r -> {
                grillaCargo.clearSortOrder();
            });
            formularioForm.setCancelarListener(r -> {
                grillaCargo.clearSortOrder();
            });

            addComponent(barAndGridLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String value) {
        try {
            lista = formularioDao.getFormularioByDescripcion(value);
            grillaCargo.setSizeFull();
            grillaCargo.setItems(lista);
            grillaCargo.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
