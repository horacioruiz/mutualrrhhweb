package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.SolicitudDao;
import py.mutualmsp.mutualweb.dao.SolicitudDetalleDao;
import py.mutualmsp.mutualweb.dao.SolicitudProduccionDetalleDao;
import py.mutualmsp.mutualweb.dao.UnaprobacionDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Solicitud;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 4/7/2016.
 */
public class SolicitudDetalleView extends Window {

//    private TextArea comentarios = new TextArea("Agregar nuevo comentario");
    final Image image = new Image("Imagen");
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";
    // Create upload stream
    FileOutputStream fos = null;
    //DUpload upload = new DUpload("Subir Archivo", this::fileReceived);
    Grid<SolicitudDetalle> gridLog = new Grid<>(SolicitudDetalle.class);
    Grid<SolicitudProduccionDetalle> gridLogProduccion = new Grid<>(SolicitudProduccionDetalle.class);
    Grid<VacacionesDetalle> gridLogDetalle = new Grid<>(VacacionesDetalle.class);
    UnaprobacionDao unaprobacionDaoController = ResourceLocator.locate(UnaprobacionDao.class);
    SolicitudDetalleDao solicitudDetalleController = ResourceLocator.locate(SolicitudDetalleDao.class);
    SolicitudProduccionDetalleDao solicitudProduccionDetalleController = ResourceLocator.locate(SolicitudProduccionDetalleDao.class);
    SolicitudDao solicitudController = ResourceLocator.locate(SolicitudDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    VacacionesDao vacacionController = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionDetalleController = ResourceLocator.locate(VacacionesDetalleDao.class);
    Label labelDescripcion = new Label();
    Label labelUrl = new Label();
    public long idTicket;
    private Button guardar = new Button("Aprobar");
    private Button cancelar = new Button("Cerrar");
    private Binder<SolicitudDetalle> binder = new Binder<>(SolicitudDetalle.class);

//    Services ejb = ResourceLocator.locate(Services.class);
//    Map<Long, Funcionario> mapFuncionario = new HashMap<Long, Funcionario>();
//    EnviarMail enviarMailEJB = ResourceLocator.locate(EnviarMail.class);
    public SolicitudDetalleView() {
        VerticalLayout verticalLayout = createForm();
        setContent(verticalLayout);
        setWidth("85%");
        setHeight("95%");
//        setCaption("Comentarios");
        setModal(true);
        center();

        gridLog.setCaption("Detalle de Solicitud");
        gridLogDetalle.setVisible(false);

        addCloseListener(closeEvent -> {
            close();
        });

        guardar.addClickListener(cl -> {
            try {
                save();
            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
        cancelar.addClickListener(clickEvent -> {
            close();
        });
    }

    private void poblarGrillaLogs(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudDetalleController.listarPorIdSolicitud(id) != null) {
            try {
                System.out.println("poblarGrillaLogs()");
                List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(id);
//                setCaption(solicitudDetalle.get(0).getSolicitud().getFuncionario().getNombreCompleto() + "      |      Inicio: " + simpleDateFormat.format(solicitudDetalle.get(0).getSolicitud().getFechaini()) + " - Fin: " + simpleDateFormat.format(solicitudDetalle.get(0).getSolicitud().getFechafin()));
                setCaption(solicitudDetalle.get(0).getSolicitud().getFuncionario().getNombreCompleto() + " | " + solicitudDetalle.get(0).getSolicitud().getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(solicitudDetalle.get(0).getSolicitud().getFuncionario().getFechaingreso()));

//                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && (solicitudDetalle.get(0).getSolicitud().getEncargado() == null || solicitudDetalle.get(0).getSolicitud().getEncargado2() == null)) {
                System.out.println("1) ========>>" + UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase());
                System.out.println("2) ========>>" + solicitudDetalle.get(0).getSolicitud().getEncargado());
                System.out.println("3) ========>>" + solicitudDetalle.get(0).getSolicitud().getEncargado2());
                if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1) 
                        && (solicitudDetalle.get(0).getSolicitud().getEncargado() == null 
                        || solicitudDetalle.get(0).getSolicitud().getEncargado2() == null)) {
                    guardar.setVisible(true);
                } else {
                    if (UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().toUpperCase().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                            || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().toUpperCase().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL")
                            || UserHolder.get().getIdfuncionario().getDependencia().getId()== 12) {
                        guardar.setVisible(true);
                    } else {
                        guardar.setVisible(false);
                    }
                }
                gridLog.setCaption(solicitudDetalle.get(0).getSolicitud().getFormulario().getDescripcion().toUpperCase());
                gridLog.setItems(solicitudDetalle);
                gridLog.removeAllColumns();
                gridLog.addColumn(e -> {
                    return e.getSolicitud() == null ? "" : e.getSolicitud().getId();
                }).setCaption("COD");
                gridLog.addColumn(e -> {
                    return e.getSolicitud().getFechaini() == null ? "--" : simpleDateFormat.format(e.getSolicitud().getFechaini());
                }).setCaption("FEC INICIO");
                gridLog.addColumn(e -> {
                    return e.getSolicitud().getFechafin() == null ? "--" : simpleDateFormat.format(e.getSolicitud().getFechafin());
                }).setCaption("FEC FIN");
                if (solicitudDetalle.get(0).getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO") || solicitudDetalle.get(0).getSolicitud().getFormulario().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    gridLog.addColumn(e -> {
                        return e.getSolicitud().getHoraini() == null ? "--" : simpleDateFormatHora.format(e.getSolicitud().getHoraini());
                    }).setCaption("HORA INICIO");
                    gridLog.addColumn(e -> {
                        return e.getSolicitud().getHorafin() == null ? "--" : simpleDateFormatHora.format(e.getSolicitud().getHorafin());
                    }).setCaption("HORA FIN");
                }
                gridLog.addColumn(e -> {
                    return e.getMotivo() == null ? "" : e.getMotivo().getDescripcion();
                }).setCaption("MOTIVO");
                gridLog.addColumn(e -> {
                    return e.getSolicitud().getEncargado() == null ? "" : e.getSolicitud().getEncargado().getNombreCompleto();
                }).setCaption("1RA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getSolicitud().getEncargado2() == null ? "" : e.getSolicitud().getEncargado2().getNombreCompleto();
                }).setCaption("2DA APROBACION");
                gridLog.addColumn(e -> {
                    return e.getSolicitud().getFuncrrhh() == null ? "" : e.getSolicitud().getFuncrrhh().getNombreCompleto();
                }).setCaption("RRHH");
                gridLog.addColumn(e -> {
                    return e.getDescripcion();
                }).setCaption("OTRO MOTIVO");
                if (solicitudDetalle.get(0).getSolicitud().getCantdia() != null && solicitudDetalle.get(0).getSolicitud().getCantdia() > 0) {
                    gridLog.addColumn(e -> {
                        return e.getSolicitud().getCantdia();
                    }).setCaption("CANT DIA");
                }
                if (solicitudDetalle.get(0).getSolicitud().getAprobado() == 2) {
                    try {
                        gridLog.addColumn(e -> {
                            return solicitudDetalle.get(0).getSolicitud().getRechazo().toUpperCase();
                        }).setCaption("MOTIVO RECHAZO");
                    } catch (Exception e) {
                    } finally {
                    }
                }
//                gridLog.addColumn(e -> {
//                    return e.getSolicitud() == null ? "" : e.getSolicitud().get;
//                }).setCaption("Descripción");
                gridLog.setSizeFull();
                gridLogProduccion.setVisible(false);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
//        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        ///layout.addComponent(upload);
        //comentarios.setNullRepresentation("");
//        comentarios.setWidth("100%");
//        comentarios.setHeight("35%");
        gridLog.setHeight("500px");
        layout.addComponent(labelDescripcion);
        layout.addComponent(gridLog);
        layout.addComponent(gridLogDetalle);
        layout.addComponent(gridLogProduccion);
        HorizontalLayout hupload = new HorizontalLayout();
        layout.addComponent(hupload);
        layout.addComponent(labelUrl);
//        layout.addComponent(comentarios);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(guardar);
        horizontalButton.addComponent(cancelar);
//        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private void save() {
        try {
            List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(this.idTicket);
            Solicitud solicitud = solicitudDetalle.get(0).getSolicitud();
            String descripcionDependencia = UserHolder.get().getIdfuncionario().getDependencia().getDescripcion();
            
            if (esDependenciaValida(descripcionDependencia)) {
                if (solicitud.getEncargado() == null) {
                    solicitud.setEncargado(UserHolder.get().getIdfuncionario());
                }
                if (solicitud.getEncargado2() == null) {
                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                }
                if (solicitud.getFuncrrhh() == null) {
                    solicitud.setFuncrrhh(UserHolder.get().getIdfuncionario());
                }
            } else {
                //Si es encargao o jefe y el funcionario en cuestion esta dentro de unaprobacion aproba 1 y 2 
                //sino hacer lo de abajo
                //unaprobacionDaoController.getOne(solicitud.getFuncionario().getId())
                if (unaprobacionDaoController.getOne(solicitud.getFuncionario().getId()) != null) {
                    if (solicitud.getEncargado() == null) {
                        solicitud.setEncargado(UserHolder.get().getIdfuncionario());
                    }
                    if (solicitud.getEncargado2() == null) {
                        solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                    }
                } 
                if (solicitud.getEncargado() == null || cargo(solicitud)) {
                    solicitud.setEncargado(UserHolder.get().getIdfuncionario());
                }else if (solicitud.getEncargado2() == null || cargo(solicitud)) {
                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
                }
            }
            solicitudController.guardarSolicitud(solicitud);
            close();
        } catch (Exception e) {
            Notification.show("Ocurrio un error al aprobar la Licencia: " + e.getMessage());
            close();
        }
         finally {
        }
    }           
             
    private boolean cargo(Solicitud solicitud) {
        String descripcionCargo = solicitud.getFuncionario().getCargo().getDescripcion();
        return "JEFE".equals(descripcionCargo) || "ENCARG".equals(descripcionCargo) || "COORDINADOR".equals(descripcionCargo);
    }
    
    private boolean esDependenciaValida(String descripcionDependencia) {
        return "SECCION DE SALARIOS Y BENEFICIOS".equals(descripcionDependencia)
                || "SECCION DE DESARROLLO ORGANIZACIONAL".equals(descripcionDependencia)
                || "DEPARTAMENTO DE RECURSOS HUMANOS".equals(descripcionDependencia);
    }
            //List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(this.idTicket);
            //Solicitud solicitud = solicitudDetalle.get(0).getSolicitud();
//            if (solicitud.getEncargado() == null) {
//                if (solicitud.getFuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
//                        || solicitud.getFuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
//                        || solicitud.getFuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
//                        || unaprobacionDaoController.getOne(solicitud.getFuncionario().getId()) != null) {
//                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
//                }
//                solicitud.setEncargado(UserHolder.get().getIdfuncionario());
//                solicitudController.guardarSolicitud(solicitud);
//            } else {
//                long idEncargado1 = solicitud.getEncargado().getId();
//                long idEncargado2 = UserHolder.get().getIdfuncionario().getId();
//                System.out.println(idEncargado1 + " | " + idEncargado2);
//                if (idEncargado1 == idEncargado2) {
//                    Notification.show("Otro encargado debe aprobar la solicitud.");
//                } else {
//                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
//                    solicitudController.guardarSolicitud(solicitud);
//                }
//            }

//            close();
//        } catch (Exception e) {
//            List<SolicitudProduccionDetalle> solicitudDetalle = solicitudProduccionDetalleController.listarPorIdSolicitud(this.idTicket);
//            Solicitud solicitud = solicitudDetalle.get(0).getSolicitud();
//            boolean hayJefes = false;
//            for (SolicitudProduccionDetalle solicitudProduccionDetalle : solicitudDetalle) {
//                try {
//                    if (solicitudProduccionDetalle.getCargo().toUpperCase().indexOf("JEFE") != -1 
//                            || solicitudProduccionDetalle.getCargo().toUpperCase().indexOf("ENCARG") != -1 
//                            || solicitudProduccionDetalle.getCargo().toUpperCase().indexOf("COORDINADOR") != -1) {
//                        hayJefes = true;
//                    }
//                } catch (Exception ex) {
//                } finally {
//                }
//            }
//
////            solicitud = solicitudController.listarPorIdSecond(solicitud.getId());
//            if (solicitud.getEncargado() == null) {
//                try {
//                    if (hayJefes) {
//                        solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
//                    }
//                } catch (Exception ex) {
//                } finally {
//                }
//
//                solicitud.setEncargado(UserHolder.get().getIdfuncionario());
//                solicitudController.guardarSolicitud(solicitud);
//            } else {
//                long idEncargado1 = solicitud.getEncargado().getId();
//                long idEncargado2 = UserHolder.get().getIdfuncionario().getId();
//                System.out.println(idEncargado1 + " | " + idEncargado2);
//                if (idEncargado1 == idEncargado2) {
//                    Notification.show("Otro encargado debe aprobar la solicitud.");
//                } else {
//                    solicitud.setEncargado2(UserHolder.get().getIdfuncionario());
//                    solicitudController.guardarSolicitud(solicitud);
//                }
//            }
//            close();
        
    public void setComentarios(TextArea comentarios) {
//        this.comentarios = comentarios;
    }

    public void setIdTicket(Solicitud solicitud) {
        Solicitud soli = solicitudController.listarPorIdAndFormulario(solicitud.getId());
        if (!soli.getFormulario().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            this.idTicket = solicitud.getId();
            poblarGrillaLogs(idTicket);
        } else {
            this.idTicket = solicitud.getId();
            poblarGrillaDetalle(solicitud);
        }
    }
//    public void setIdTicket(long idTicket) {
//        this.idTicket = idTicket;
//        poblarGrillaLogs(idTicket);
//    }

    void setListaSolicitud(List<Solicitud> array) {
        List<SolicitudDetalle> listSolicitudDetalle = new ArrayList<>();
        for (Solicitud solicitud : array) {
            listSolicitudDetalle.addAll(solicitudDetalleController.listarPorIdSolicitud(solicitud.getId()));
        }
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
//        List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(id);
        try {
            System.out.println("poblarGrillaLogs()");
//            List<SolicitudDetalle> solicitudDetalle = solicitudDetalleController.listarPorIdSolicitud(id);
            setCaption(array.get(0).getFuncionario().getNombreCompleto() + " | " + array.get(0).getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(array.get(0).getFuncionario().getFechaingreso()));
//                gridLog.setCaption(solicitudDetalle.get(0).getSolicitud().getId() + " )- " + solicitudDetalle.get(0).getSolicitud().getFormulario().getDescripcion().toUpperCase());
            gridLog.setItems(listSolicitudDetalle);
            gridLog.removeAllColumns();
            gridLog.addColumn(e -> {
                return e.getSolicitud() == null ? "" : e.getSolicitud().getId();
            }).setCaption("COD");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getSolicitud().getFechaini());
            }).setCaption("FEC INICIO");
            gridLog.addColumn(e -> {
                return simpleDateFormat.format(e.getSolicitud().getFechafin());
            }).setCaption("FEC FIN");
            gridLog.addColumn(e -> {
                return e.getSolicitud().getCantdia();
            }).setCaption("CANT DIA");
            gridLog.addColumn(e -> {
                return e.getSolicitud().getEncargado() == null ? "" : e.getSolicitud().getEncargado().getNombreCompleto();
            }).setCaption("ENCARGADO");
            gridLog.addColumn(e -> {
                return e.getSolicitud().getFuncrrhh() == null ? "" : e.getSolicitud().getFuncrrhh().getNombreCompleto();
            }).setCaption("RRHH");
            gridLog.setSizeFull();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setListaSolicitud(long idVacacion) {
        try {
            gridLogDetalle.setVisible(true);
            gridLog.setVisible(false);
            gridLogProduccion.setVisible(false);
//        guardar.setVisible(false);

            Vacaciones vacas = vacacionController.listaVacacionesPorId(idVacacion);
            gridLogDetalle.setCaption("DETALLE DE VACACIONES | PERIODO " + vacas.getPeriodo());
            List<VacacionesDetalle> listVacas = vacacionDetalleController.listaVacacionesDetallePorIdVacaciones(vacas.getId());

            String pattern = "dd-MM-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String patternHora = "HH:mm";
            SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
            try {
                System.out.println("poblarGrillaLogs()");
                setCaption(vacas.getFuncionario().getNombreCompleto() + " | " + vacas.getFuncionario().getCargo().getDescripcion() + " | Ingreso:" + simpleDateFormat.format(vacas.getFuncionario().getFechaingreso()));
                gridLogDetalle.setItems(listVacas);
                gridLogDetalle.removeAllColumns();
                gridLogDetalle.addColumn(e -> {
                    return e.getSolicitud() == null ? "" : e.getSolicitud().getId();
                }).setCaption("SOLICITUD");
                gridLogDetalle.addColumn(e -> {
                    return simpleDateFormat.format(e.getSolicitud().getFechaini());
                }).setCaption("FEC INICIO");
                gridLogDetalle.addColumn(e -> {
                    return simpleDateFormat.format(e.getSolicitud().getFechafin());
                }).setCaption("FEC FIN");
                gridLogDetalle.addColumn(e -> {
                    return e.getCantDia();
                }).setCaption("CANT DIA");
                gridLogDetalle.addColumn(e -> {
                    return e.getSolicitud().getEncargado() == null ? "" : e.getSolicitud().getEncargado().getNombreCompleto();
                }).setCaption("ENCARGADO");
                gridLogDetalle.addColumn(e -> {
                    return e.getSolicitud().getFuncrrhh() == null ? "" : e.getSolicitud().getFuncrrhh().getNombreCompleto();
                }).setCaption("RRHH");
                gridLogDetalle.setSizeFull();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        } finally {
        }
    }

    private void poblarGrillaDetalle(Solicitud solicitud) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (solicitudProduccionDetalleController.listarPorIdSolicitud(solicitud.getId()) != null) {
            try {
                List<SolicitudProduccionDetalle> solicitudDetalle = solicitudProduccionDetalleController.listarPorIdSolicitud(solicitud.getId());
                setCaption("solicitud de producción".toUpperCase());
                setCaption(solicitud.getAreafunc().toUpperCase() + " | " + solicitud.getCargofunc().toUpperCase());

//                if ((UserHolder.get().getIdnivelusuario() == 8 || UserHolder.get().getIdnivelusuario() == 9 || UserHolder.get().getIdnivelusuario() == 10) && (solicitudDetalle.get(0).getSolicitud().getEncargado() == null || solicitudDetalle.get(0).getSolicitud().getEncargado2() == null)) {
                System.out.println("1) ========>>" + UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase());
                System.out.println("2) ========>>" + solicitudDetalle.get(0).getSolicitud().getEncargado());
                System.out.println("3) ========>>" + solicitudDetalle.get(0).getSolicitud().getEncargado2());
                if ((UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARG") != -1 
                        || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1) 
                        && (solicitudDetalle.get(0).getSolicitud().getEncargado() == null || solicitudDetalle.get(0).getSolicitud().getEncargado2() == null)) {
                    guardar.setVisible(true);
                } else {
                    if (UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().toUpperCase().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                            || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().toUpperCase().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL")) {
                        guardar.setVisible(true);
                    } else {
                        guardar.setVisible(false);
                    }
                }
                try {
                    gridLog.setCaption("MOTIVO RECHAZO: " + solicitud.getRechazo().toUpperCase());
                } catch (Exception e) {
                    gridLog.setCaption("");
                } finally {
                }

                gridLogProduccion.setItems(solicitudDetalle);
                gridLogProduccion.removeAllColumns();
                gridLogProduccion.addColumn(e -> {
                    return e.getId();
                }).setCaption("CODIGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getNombrefuncionario();
                }).setCaption("NOMBRE Y APELLIDO");
                gridLogProduccion.addColumn(e -> {
                    return e.getCargo().toUpperCase();
                }).setCaption("CARGO");
                gridLogProduccion.addColumn(e -> {
                    return e.getDescripcion().toUpperCase();
                }).setCaption("DESCRIPCION ACTIVIDAD");
                gridLogProduccion.addColumn(e -> {
                    return e.getSolicitud().getEncargado() == null ? "" : e.getSolicitud().getEncargado().getNombreCompleto();
                }).setCaption("1RA APROBACION");
                gridLogProduccion.addColumn(e -> {
                    return e.getSolicitud().getEncargado2() == null ? "" : e.getSolicitud().getEncargado2().getNombreCompleto();
                }).setCaption("2DA APROBACION");
                gridLogProduccion.addColumn(e -> {
                    return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
                }).setCaption("FECHA");
                gridLogProduccion.addColumn(e -> {
                    return e.getHora() == null ? "--" : simpleDateFormatHora.format(e.getHora());
                }).setCaption("HORARIO");
//                gridLog.addColumn(e -> {
//                    return e.getSolicitud() == null ? "" : e.getSolicitud().get;
//                }).setCaption("Descripción");
                gridLogProduccion.setSizeFull();
                gridLogProduccion.setVisible(true);
                gridLogDetalle.setVisible(false);
                gridLog.setVisible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
