/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.formularios.TipooperacionForm;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class TipooperacionVista extends CssLayout implements View{
    public static String VIEW_NAME = "Tipo de Operación";
    Grid<Tipooperacion> grillaTipooperacion = new Grid<>(Tipooperacion.class);
    TextField txtfFiltro = new TextField("Filtro");
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    List<Tipooperacion> lista = new ArrayList<>();
    Button btnNuevo = new Button("Nuevo");
    TipooperacionForm tipooperacionForm = new TipooperacionForm();
    
    public TipooperacionVista() {
        setSizeFull();
        addStyleName("crud-view");
        
        btnNuevo.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        btnNuevo.setIcon(VaadinIcons.PLUS_CIRCLE);
        
        tipooperacionForm.setVisible(false);
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.addComponents(txtfFiltro, btnNuevo);
        topLayout.setSpacing(true);
        topLayout.setComponentAlignment(txtfFiltro, Alignment.MIDDLE_LEFT);
        topLayout.setComponentAlignment(btnNuevo, Alignment.MIDDLE_RIGHT);
        topLayout.addStyleName("top-bar");
        
        txtfFiltro.setPlaceholder("Filtro de búsqueda");
        txtfFiltro.setValueChangeMode(ValueChangeMode.LAZY);
        txtfFiltro.addValueChangeListener(e -> updateList(e.getValue()));
        
        btnNuevo.addClickListener(e ->{
            grillaTipooperacion.asSingleSelect().clear();
            tipooperacionForm.setTipooperacion(new Tipooperacion());
        });
        
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponents(grillaTipooperacion, tipooperacionForm);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(grillaTipooperacion, 1);
        
        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponents(topLayout, horizontalLayout);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(horizontalLayout, 1);
        barAndGridLayout.addStyleName("crud-main-layout");
        
        try {
            lista = tipooperacionDao.listaTipooperacion();
            grillaTipooperacion.setItems(lista);
            grillaTipooperacion.removeAllColumns();
            grillaTipooperacion.addColumn(Tipooperacion::getIdtipooperacion).setCaption("Id Tipo Operación");
            grillaTipooperacion.addColumn(Tipooperacion::getDescripcion).setCaption("Descripción");
            grillaTipooperacion.setSizeFull();
            grillaTipooperacion.asSingleSelect().addValueChangeListener(e ->{
                if (e.getValue()==null) {
                    tipooperacionForm.setVisible(false);
                } else {
                    tipooperacionForm.setTipooperacion(e.getValue());
                    tipooperacionForm.setViejo();
                }
            });
            
            tipooperacionForm.setGuardarListener(t ->{
                grillaTipooperacion.clearSortOrder();
                lista = tipooperacionDao.listaTipooperacion();
                grillaTipooperacion.setItems(lista);
            });
            tipooperacionForm.setBorrarListener(t ->{
                grillaTipooperacion.clearSortOrder();
            });
            tipooperacionForm.setCancelarListener(t ->{
                grillaTipooperacion.clearSortOrder();
            });
            
            addComponent(barAndGridLayout);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateList(String valor) {
        try {
            lista = tipooperacionDao.getListTipooperacion(valor);
            grillaTipooperacion.setSizeFull();
            grillaTipooperacion.setItems(lista);
            grillaTipooperacion.clearSortOrder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
