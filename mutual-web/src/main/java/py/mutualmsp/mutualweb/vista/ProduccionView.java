package py.mutualmsp.mutualweb.vista;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.json.simple.JSONArray;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.SolicitudProduccionDetalleDao;
import py.mutualmsp.mutualweb.dao.LicenciasDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;

import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.UsuarioDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDetalleDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;
import py.mutualmsp.mutualweb.formularios.CompensacionesForm;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 4/7/2016.
 */
public class ProduccionView extends CssLayout implements View {

    public static final String VIEW_NAME = "Solicitudes de Producción";
//    private TextArea comentarios = new TextArea("Agregar nuevo comentario");
    final Image image = new Image("Imagen");
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    ComboBox<Funcionario> filterFuncionario = new ComboBox<>();
    ComboBox<String> filterAprobado = new ComboBox<>();
    TextField filter = new TextField();
    private DateField fechaDesde = new DateField();
    private DateField fechaHasta = new DateField();
    Button btnSearch = new Button();
    // Create upload stream
    FileOutputStream fos = null;
    Button btnImprimir = new Button("");

    FuncionarioDao funcionarioController = ResourceLocator.locate(FuncionarioDao.class);
    ParametroDao formularioController = ResourceLocator.locate(ParametroDao.class);
    LicenciasDao licenciasController = ResourceLocator.locate(LicenciasDao.class);
    MarcacionesDao marcacionesController = ResourceLocator.locate(MarcacionesDao.class);
    UsuarioDao usuarioController = ResourceLocator.locate(UsuarioDao.class);
    HashMap<Long, String> mapeo = new HashMap<>();
    //DUpload upload = new DUpload("Subir Archivo", this::fileReceived);
    Grid<SolicitudProduccionDetalle> gridLog = new Grid<>(SolicitudProduccionDetalle.class);
    Grid<SolicitudProduccionDetalle> gridLogProduccion = new Grid<>(SolicitudProduccionDetalle.class);
    Grid<VacacionesDetalle> gridLogDetalle = new Grid<>(VacacionesDetalle.class);
    SolicitudProduccionDetalleDao solicitudDetalleController = ResourceLocator.locate(SolicitudProduccionDetalleDao.class);
    LicenciasDao solicitudController = ResourceLocator.locate(LicenciasDao.class);
    MotivosDao motivoController = ResourceLocator.locate(MotivosDao.class);
    VacacionesDao vacacionController = ResourceLocator.locate(VacacionesDao.class);
    VacacionesDetalleDao vacacionDetalleController = ResourceLocator.locate(VacacionesDetalleDao.class);

    List<Dependencia> listDependencia = new ArrayList<>();

    CompensacionesForm compensacionesForm = new CompensacionesForm();
    private ComboBox<Dependencia> cbDpto = new ComboBox<>();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    Label labelDescripcion = new Label();
    Label labelUrl = new Label();
    public long idTicket;
    private Button guardar = new Button("Aprobar");
    private Button cancelar = new Button("Cerrar");
    private Binder<SolicitudProduccionDetalle> binder = new Binder<>(SolicitudProduccionDetalle.class);

//    Services ejb = ResourceLocator.locate(Services.class);
//    Map<Long, Funcionario> mapFuncionario = new HashMap<Long, Funcionario>();
//    EnviarMail enviarMailEJB = ResourceLocator.locate(EnviarMail.class);
    public ProduccionView() {
        //VerticalLayout verticalLayout = createForm();
        HorizontalLayout horizontalLayout = createHorizontalLayout();
        //setContent(verticalLayout);
        setSizeFull();
        addStyleName("crud-view");
        listDependencia = new ArrayList<>();
//        setCaption("Comentarios");
        //setModal(true);
        //center();

        List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
        List<Dependencia> listDependencia = new ArrayList<>();
        List<Funcionario> listFunc = new ArrayList<>();
        for (Dependencia dependencia : depen) {
            listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                if (listFunc.isEmpty()) {
                    listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                }else{
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                } 
        }
        for (Dependencia dependencia1 : listDependencia) {
            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
        }
        for (Funcionario funcio : listFunc) {
            mapeoFuncionarios.put(funcio.getId(), funcio);
        }

        cbDpto.setPlaceholder("Filtre Departamento");
        cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);

        //gridLog.setCaption("Compensaciones");
        gridLogDetalle.setVisible(false);

        guardar.addClickListener(cl -> {
            try {
//                save();
            } catch (Exception e) {
                e.printStackTrace();
                Notification.show(e.getMessage());
            }
        });
        compensacionesForm.setVisible(false);
        if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
            filterFuncionario.setItems(funcionarioController.listaFuncionario());
        } else {
            filterFuncionario.setItems(funcionarioController.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            filterFuncionario.setEnabled(false);
            filterFuncionario.setValue(UserHolder.get().getIdfuncionario());
        }
        btnSearch.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        btnSearch.setIcon(VaadinIcons.SEARCH);
        btnImprimir.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        btnImprimir.setIcon(VaadinIcons.PRINT);
        filter.setPlaceholder("Filtre código");
        filter.setWidth(5f, TextField.UNITS_EM);
        filterAprobado.setWidth(12f, TextField.UNITS_EM);
        filterFuncionario.setWidth(20f, TextField.UNITS_EM);
        cbDpto.setWidth(20f, TextField.UNITS_EM);
        fechaDesde.setWidth(8f, TextField.UNITS_EM);
        fechaHasta.setWidth(8f, TextField.UNITS_EM);
        filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

        fechaDesde.setPlaceholder("Desde");
        fechaHasta.setPlaceholder("Hasta");

        fechaDesde.setValue(DateUtils.asLocalDate(new Date()));
        fechaHasta.setValue(DateUtils.asLocalDate(new Date()));

        List<String> listEstados = new ArrayList<>();
        listEstados.add("APROBADO");
        listEstados.add("RECHAZADO");
        listEstados.add("PENDIENTE");
        filterAprobado.setItems(listEstados);
        filterAprobado.setPlaceholder("Filtre Aprobado");
        filterFuncionario.setPlaceholder("Filtre Funcionario");

        cbDpto.addValueChangeListener(e -> cargarSeccion());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        gridLog.setSizeFull();

        HorizontalLayout horizontalLayoutHere = new HorizontalLayout();
        horizontalLayoutHere.addComponents(gridLog, compensacionesForm);
        horizontalLayoutHere.setSizeFull();
        horizontalLayoutHere.setExpandRatio(gridLog, 6F);

        verticalLayout.addComponent(horizontalLayoutHere);
        verticalLayout.setMargin(true);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setExpandRatio(horizontalLayoutHere, 1);
        verticalLayout.addStyleName("crud-main-layout");
//        verticalLayout.setExpandRatio(gridLog, 6F);

        btnSearch.addClickListener(e -> poblarGrillaLogs(0l));

        btnImprimir.addClickListener(e -> {
            mostrarListaPDF();
            //Page.getCurrent().setLocation("http://www.example.com");

        });

        gridLog.setItems(new ArrayList<>());
        gridLog.removeAllColumns();
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        gridLog.addColumn(e -> {
            return e.getId() == null ? "" : e.getId();
        }).setCaption("COD");
        gridLog.addColumn(e -> {
            return e.getFuncionario() == null ? "--" : e.getFuncionario().getNombreCompleto();
        }).setCaption("FUNCIONARIO");
        gridLog.addColumn(e -> {
            return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
        }).setCaption("FECHA");
        gridLog.addColumn(e -> {
            return e.getHora() == null ? "--" : simpleDateFormatHora.format(e.getHora());
        }).setCaption("HORA");
        gridLog.addColumn(e -> {
            return e.getArea() == null ? "--" : simpleDateFormatHora.format(e.getArea());
        }).setCaption("AREA");
        gridLog.addColumn(e -> {
            return e.getDescripcion() == null ? "--" : e.getDescripcion();
        }).setCaption("ACTIVIDAD");
        gridLog.setSizeFull();

        gridLogProduccion.setVisible(
                false);
        gridLogDetalle.setVisible(
                false);
        gridLog.setVisible(
                true);

        compensacionesForm.setGuardarListener(r -> {
            gridLog.clearSortOrder();
            poblarGrillaLogs(0l);
        });

        compensacionesForm.setBorrarListener(r -> {
            gridLog.clearSortOrder();
            gridLog.setSizeFull();
        });

        compensacionesForm.setCancelarListener(r -> {
            gridLog.clearSortOrder();
            poblarGrillaLogs(0l);
        });
        //verticalLayout.addComponent(labelTotalizador);
        //verticalLayout.setComponentAlignment(labelTotalizador, Alignment.BOTTOM_RIGHT);
        //HorizontalLayout horizontalLayout1 = crearSegundoGrid();
        //horizontalLayout1.setSizeFull();
        verticalLayout.addStyleName("crud-main-layout");
        addComponent(verticalLayout);

        Licencias solicitud = new Licencias();
        solicitud.setId(24l);
        setIdTicket(solicitud);
    }

    private void cargarSeccion() {
        if (cbDpto.getValue() != null) {
            listDependencia = dptoDao.listarSubDependencia(cbDpto.getValue().getId());
        }
    }

    private void poblarGrillaLogs(Long id) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        //if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
        try {
            System.out.println("poblarGrillaLogs()");
            List<SolicitudProduccionDetalle> solicitudDetalle = solicitudDetalleController.listarPorFechasSPD(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getId(), listDependencia);
            List<SolicitudProduccionDetalle> listSolicitud = new ArrayList<>();
            try {
                if (filterAprobado.getValue() != null || !filterAprobado.getValue().equalsIgnoreCase("")) {
                    if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("PENDIENTE")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 0) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("RECHAZADO")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 2) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("APROBADO")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 1) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
//                            if (licenciasCompensar.getSolicitud().getAprobado() == 1) {
                            listSolicitud.add(licenciasCompensar);
//                            }
                        }
                    }
                } else {
                    listSolicitud = solicitudDetalle;
                }
            } catch (Exception e) {
                listSolicitud = solicitudDetalle;
            } finally {
            }
            gridLog.clearSortOrder();
            if (listSolicitud.isEmpty() || listSolicitud.size() == 0) {
                gridLog.setItems(new ArrayList<>());
            } else {
                gridLog.setItems(listSolicitud);
            }
            //gridLog.setCaption(solicitudDetalle.get(0).getLicencia().getParametro().getDescripcion().toUpperCase());

            gridLog.removeAllColumns();
            gridLog.addColumn(e -> {
                return e.getId() == null ? "" : e.getId();
            }).setCaption("COD");
            gridLog.addComponentColumn(e -> {
                return e.getSolicitud().getEstadoIcon();
            }).setCaption("APROBADO");
            gridLog.addColumn(e -> {
                return e.getFuncionario() == null ? "--" : e.getFuncionario().getNombreCompleto();
            }).setCaption("FUNCIONARIO");
            gridLog.addColumn(e -> {
                return e.getFecha() == null ? "--" : simpleDateFormat.format(e.getFecha());
            }).setCaption("FECHA");
            gridLog.addColumn(e -> {
                return e.getHora() == null ? "--" : simpleDateFormatHora.format(e.getHora());
            }).setCaption("HORA");
            gridLog.addColumn(e -> {
                return e.getArea() == null ? "--" : e.getArea();
            }).setCaption("AREA");
            gridLog.addColumn(e -> {
                return e.getDescripcion() == null ? "--" : e.getDescripcion();
            }).setCaption("ACTIVIDAD");

            gridLog.setSizeFull();

            gridLogProduccion.setVisible(
                    false);
            gridLogDetalle.setVisible(
                    false);
            gridLog.setVisible(
                    true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //}
    }

    private HorizontalLayout createHorizontalLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setWidth("100%");
        VerticalLayout verticalLayoutIzquierda = new VerticalLayout();
        //verticalLayoutIzquierda.addComponent(layoutIzquierdo);

        HorizontalLayout layoutFuncEstado = new HorizontalLayout();
        layoutFuncEstado.addComponent(filterFuncionario);
        layoutFuncEstado.setExpandRatio(filterFuncionario, 2);
        layoutFuncEstado.addComponent(cbDpto);
        layoutFuncEstado.setExpandRatio(cbDpto, 2);

        HorizontalLayout layoutFunc = new HorizontalLayout();
        layoutFunc.addComponent(filterAprobado);
        layoutFunc.setExpandRatio(filterAprobado, 2);
        layoutFunc.addComponent(fechaDesde);
        layoutFunc.setExpandRatio(fechaDesde, 2);
        layoutFunc.addComponent(fechaHasta);
        layoutFunc.setExpandRatio(fechaHasta, 2);
        layoutFunc.addComponent(btnSearch);
        layoutFunc.setExpandRatio(btnSearch, 2);
        layoutFunc.addComponent(btnImprimir);
        layoutFunc.setExpandRatio(btnImprimir, 2);

        verticalLayoutIzquierda.addComponent(layoutFuncEstado);
        verticalLayoutIzquierda.addComponent(layoutFunc);

        layout.addComponent(verticalLayoutIzquierda);

        /*layout.addComponent(cerrarTicket);
        layout.addComponent(aceptarConformidad);
        layout.addComponent(nuevaLicencias);
        layout.addComponent(editarLicencias);
        layout.addComponent(imprimirLicencias);*/
        layout.setStyleName("top-bar");
        return layout;
    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
//        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        ///layout.addComponent(upload);
        //comentarios.setNullRepresentation("");
//        comentarios.setWidth("100%");
//        comentarios.setHeight("35%");
        gridLog.setHeight("500px");
        //layout.addComponent(labelDescripcion);
        layout.addComponent(gridLog);
        layout.addComponent(gridLogDetalle);
        layout.addComponent(gridLogProduccion);
        HorizontalLayout hupload = new HorizontalLayout();
        layout.addComponent(hupload);
        //layout.addComponent(labelUrl);
//        layout.addComponent(comentarios);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(guardar);
        horizontalButton.addComponent(cancelar);
//        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    public void setIdTicket(Licencias solicitud) {
        Licencias soli = solicitudController.listarPorId(solicitud.getId());
        this.idTicket = solicitud.getId();
        poblarGrillaLogs(idTicket);
        /*if (!soli.getParametro().getDescripcion().equalsIgnoreCase("licencia_compensar")) {
            this.idTicket = solicitud.getId();
            poblarGrillaLogs(idTicket);
        } else {
            this.idTicket = solicitud.getId();
            poblarGrillaDetalle2(solicitud);
        }*/
    }
//    public void setIdTicket(long idTicket) {
//        this.idTicket = idTicket;
//        poblarGrillaLogs(idTicket);
//    }

    private String recuperarHoraSalida(Funcionario func, Date fechacompensar) {
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<Marcaciones> listaMarcaciones = marcacionesController.getByFilterHoraExtraHere(func, fechacompensar, fechacompensar, null, 0l);
        if (listaMarcaciones.isEmpty()) {
            return "00:00";
        } else {
            return simpleDateFormatHora.format(listaMarcaciones.get(0).getOutmarcada());
        }
    }

    private String recuperarSiCumple(Funcionario func, Date fechacompensar, Timestamp horafin) {
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<Marcaciones> listaMarcaciones = marcacionesController.getByFilterHoraExtraHere(func, fechacompensar, fechacompensar, null, 0l);

        if (listaMarcaciones.isEmpty()) {
            return "NO";
        } else {
            try {
                int minMarcada = ((listaMarcaciones.get(0).getOutmarcada().getHours() * 60) + listaMarcaciones.get(0).getOutmarcada().getMinutes());
                int minAquedar = ((horafin.getHours() * 60) + horafin.getMinutes());

                if (minAquedar <= minMarcada) {
                    return "SI";
                } else {
                    return "NO";
                }
            } catch (Exception e) {
                return "NO";
            } finally {
            }
        }
    }

    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

    private void mostrarListaPDF() {
        Map pSQL = new HashMap<String, Object>();
        InputStream subImg = getClass().getClassLoader().getResourceAsStream("img/logo.png");
//                    String val = "{\"ventas\": " + recuperarTickets() + "}";
//        pSQL.put("repJsonString", "{\"ventas\": []}");

        pSQL.put("repJsonString", "{\"ventas\": " + cargarCompensaciones() + "}");

//                pSQL.put("subRepNomFun", UserHolder.get().getFuncionario().getNombreCompleto());
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        pSQL.put("subRepTimestamp", subRepTimestamp);
//        pSQL.put("subRepNomFun", nombreFunc);
        pSQL.put("subRepEmpresa", "MUTUAL NAC DE FUNC DEL MINISTERIO DE SALUD PÚBLICA Y BIENESTAR SOCIAL ");
//        pSQL.put("subRepSucursal", "FORMULARIO DE PERMISO");
        pSQL.put("subRepSucursal", "CASA CENTRAL");
        pSQL.put("subRepPathLogo", subImg);
        pSQL.put("subRepPathLogoCP", subImg);
        StreamResource.StreamSource source = new StreamResource.StreamSource() {

            public InputStream getStream() {
                byte[] b = null;
                String archivo = "solicitudprod";
                try {
                    b = JasperRunManager.runReportToPdf(getClass().getClassLoader().getResourceAsStream(archivo + ".jasper"), pSQL);
//                            b = JasperRunManager.runReportToPdf("C:\\Users\\hruiz\\Documents\\reporte\\ticket_mutual.jasper", pSQL);
                } catch (JRException ex) {
                    Logger.getLogger(ProduccionView.class.getName()).log(Level.SEVERE, null, ex);
                }
                return new ByteArrayInputStream(b);
            }
        };

        String archivo = "solicitudprod";

        StreamResource resource = new StreamResource(source, archivo + ".pdf");
        resource.setCacheTime(0);
        resource.setMIMEType("application/pdf");

        Window window = new Window();
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        BrowserFrame pdf = new BrowserFrame("test", resource);
        pdf.setSizeFull();

        window.setContent(pdf);
        getUI().addWindow(window);
    }

    private JSONArray cargarCompensaciones() {
        String pattern = "dd-MM-yyyy";
        JSONArray jsonArrayDato = new JSONArray();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        SimpleDateFormat formatterHora = new SimpleDateFormat("HH:mm");
        //if (solicitudDetalleController.listarPorIdLicencia(id) != null) {
        try {
            List<SolicitudProduccionDetalle> solicitudDetalle = solicitudDetalleController.listarPorFechasSPD(DateUtils.asDate(fechaDesde.getValue()), DateUtils.asDate(fechaHasta.getValue()), filterFuncionario.getValue() == null ? null : filterFuncionario.getValue().getId(), listDependencia);
            List<SolicitudProduccionDetalle> listSolicitud = new ArrayList<>();
            try {
                if (filterAprobado.getValue() != null || !filterAprobado.getValue().equalsIgnoreCase("")) {
                    if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("PENDIENTE")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 0) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("RECHAZADO")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 2) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else if (filterAprobado.getValue().toUpperCase().equalsIgnoreCase("APROBADO")) {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
                            if (licenciasCompensar.getSolicitud().getAprobado() == 1) {
                                listSolicitud.add(licenciasCompensar);
                            }
                        }
                    } else {
                        for (SolicitudProduccionDetalle licenciasCompensar : solicitudDetalle) {
//                            if (licenciasCompensar.getSolicitud().getAprobado() == 1) {
                            listSolicitud.add(licenciasCompensar);
//                            }
                        }
                    }
                } else {
                    listSolicitud = solicitudDetalle;
                }
            } catch (Exception e) {
                listSolicitud = solicitudDetalle;
            } finally {
            }
            for (SolicitudProduccionDetalle licenciasCompensar : listSolicitud) {
                org.json.simple.JSONObject jsonObj = new org.json.simple.JSONObject();
                jsonObj.put("codigo", licenciasCompensar.getId());
                jsonObj.put("fechaemision", simpleDateFormat.format(new Date()));
                jsonObj.put("funcionario", licenciasCompensar.getFuncionario().getNombreCompleto());
                jsonObj.put("fecha", simpleDateFormat.format(licenciasCompensar.getFecha()));
                jsonObj.put("hora", formatterHora.format(licenciasCompensar.getHora()));
                jsonObj.put("area", licenciasCompensar.getArea().toUpperCase());
                jsonObj.put("tarea", licenciasCompensar.getDescripcion());
                if (licenciasCompensar.getSolicitud().getAprobado() == 1) {
                    jsonObj.put("estado", "SI");
                } else if (licenciasCompensar.getSolicitud().getAprobado() == 2) {
                    jsonObj.put("estado", "NO");
                } else if (licenciasCompensar.getSolicitud().getAprobado() == 0) {
                    jsonObj.put("estado", "-");
                }

                jsonArrayDato.add(jsonObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArrayDato;
    }

}
