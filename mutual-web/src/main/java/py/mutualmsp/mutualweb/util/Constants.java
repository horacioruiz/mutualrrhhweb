package py.mutualmsp.mutualweb.util;

/**
 *
 * @author xergio
 */
public class Constants {

    public static final String SECRET_KEY = "MUTUAL-MSPYBS";

    public static final String PUBLIC_SERVER_URL = "http://192.81.130.21";
    public static final String PUBLIC_API_URL = PUBLIC_SERVER_URL + ":8080/mutual-socios-ws/api/";
    public static final String PUBLIC_API_CONFIRM_URL = PUBLIC_API_URL + "auth/public/confirm/";

    public static final String HOST_NAME_SMTP = "chi-node50.websitehostserver.net";
    public static final String PUERT0_SMTP = "465";
    public static final String USER_NAME_SMTP = "mutualmovil@mutualmsp.org.py";
    public static final String PASS_SMTP = ".mutual.2019*";

    public static final String FILE_FORM_NAME = "fileData";
    public static final String UPLOAD_DIR_UBI = "C:\\uploads\\mutual-web-rrhh\\";
    public static final String UPLOAD_DIR = "C:\\uploads";
    public static final String UPLOAD_DIR_TEMP = "C:\\uploads\\temp";
//    public static final String UPLOAD_DIR = "/sedf/tests/checkit/uploads";

    public static final String DB_USR_TIPO_ADMIN_ID = "1";
    public static final String DB_USR_TIPO_USUARIO_ID = "2";
    public static final String DB_USR_TIPO_CLIENTE_ID = "3";

    public static final Integer DB_TIPOSEGMENTO_MAPA = 1;
    public static final Integer DB_TIPOSEGMENTO_EDAD = 2;
    public static final Integer DB_TIPOSEGMENTO_SEXO = 3;
    public static final String DB_TIPOSEGMENTO_SPLITTER = "|";

    public static final Integer DB_PREMIO_EFECTIVO_ID = 1;
}
