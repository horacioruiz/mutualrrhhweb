/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import java.io.IOException;
import java.util.List;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Hruiz
 */
public class Utilidades {

    public static void sendeEmail(String subject,
            String mensaje,
            List<String> emails, String fromApp) throws EmailException, IOException {

        // Create the email message
        Email email = new SimpleEmail();
        email.setCharset("UTF-8");
        email.setHostName(Constants.HOST_NAME_SMTP);
        email.setSmtpPort(465);
        email.setSSLOnConnect(true);
        email.setAuthenticator(new DefaultAuthenticator(Constants.USER_NAME_SMTP, Constants.PASS_SMTP));
        for (String s : emails) {
            email.addTo(s);
        }
        //email.addCc(config.getMailCC());
        email.setFrom(Constants.USER_NAME_SMTP, fromApp);
        email.setSubject(subject);
        email.setMsg(mensaje);
        // send the email
        email.send();
    }

}
