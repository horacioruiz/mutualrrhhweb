/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.util;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.entities.Usuario;
import py.mutualmsp.mutualweb.formularios.DependenciafuncionarioForm;
import py.mutualmsp.mutualweb.vista.CompensacionesView;
import py.mutualmsp.mutualweb.vista.ConsultaMarcacionesView;
import py.mutualmsp.mutualweb.vista.DependenciafuncionarioView;
import py.mutualmsp.mutualweb.vista.FeriadoVista;
import py.mutualmsp.mutualweb.vista.FuncionarioVista;
import py.mutualmsp.mutualweb.vista.HoraExtrasView;
import py.mutualmsp.mutualweb.vista.HorarioLaboralVista;
import py.mutualmsp.mutualweb.vista.LicenciasView;
import py.mutualmsp.mutualweb.vista.MarcacionesView;
import py.mutualmsp.mutualweb.vista.MotivosView;
import py.mutualmsp.mutualweb.vista.PapView;
import py.mutualmsp.mutualweb.vista.ProduccionView;
import py.mutualmsp.mutualweb.vista.RotacionesVista;
import py.mutualmsp.mutualweb.vista.SolicitudView;
import py.mutualmsp.mutualweb.vista.SuspencionesVista;
import py.mutualmsp.mutualweb.vista.ToleranciasVista;
import py.mutualmsp.mutualweb.vista.VacacionesView;

/**
 *
 * @author Dbarreto
 */
public class UserHolder {

    public static Usuario get() {
        Usuario user = (Usuario) VaadinSession.getCurrent().getAttribute(Usuario.class.getName());
        return user;
    }

    public static void setUsuarios(Usuario user) {
        VaadinSession.getCurrent().setAttribute(Usuario.class.getName(), user);
        poblarFormularios();
    }

    public static boolean viewAccesibleToUser(String viewName) {
        return get().isInAnyRole(viewNames_roles.get(viewName));
    }

    public static final Map<String, List<Boolean>> viewNames_roles = new HashMap<>();

    public static void poblarFormularios() {
//        viewNames_roles.put(TicketsView.VIEW_NAME, Arrays.asList(get().getAdministrador(),get().getHelpdesk() ,  get().getClientes()));
//        viewNames_roles.put(UsersView.VIEW_NAME, Arrays.asList(true, true));
//        viewNames_roles.put(FuncionarioView.VIEW_NAME, Arrays.asList(true, true));
//        viewNames_roles.put(ProyectosView.VIEW_NAME, Arrays.asList(true, true));
//        viewNames_roles.put(TipoReclamoView.VIEW_NAME, Arrays.asList(true, true));
//        viewNames_roles.put(ReportesReclamos.VIEW_NAME, Arrays.asList(true, true));

        viewNames_roles.put(FuncionarioVista.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), 
                UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"),
                UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));

        viewNames_roles.put(MotivosView.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(DependenciafuncionarioView.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(FeriadoVista.VIEW_NAME,  Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(VacacionesView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(SolicitudView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(LicenciasView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(ProduccionView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(CompensacionesView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(PapView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(ToleranciasVista.VIEW_NAME, Arrays.asList(true, true));
//        viewNames_roles.put(SuspencionesVista.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(SuspencionesVista.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(RotacionesVista.VIEW_NAME, Arrays.asList(true, true));
        //viewNames_roles.put(ImportarUsuarios.VIEW_NAME, ImportarUsuarios.VIEW_NAME, ImportarUsuarios.VIEW_NAME, VaadinIcons.BULLETS);
        viewNames_roles.put(HorarioLaboralVista.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(MarcacionesView.VIEW_NAME, Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES")));
        viewNames_roles.put(ConsultaMarcacionesView.VIEW_NAME,Arrays.asList(UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") 
                        || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES"), UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE DESARROLLO ORGANIZACIONAL") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("SECCION DE SALARIOS Y BENEFICIOS") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA") || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("REDES") 
                || UserHolder.get().getIdfuncionario().getDependencia().getDescripcion().contains("INTERNO") ));
//        viewNames_roles.put(ConsultaMarcacionesView.VIEW_NAME, Arrays.asList(true, true));
        viewNames_roles.put(HoraExtrasView.VIEW_NAME, Arrays.asList(true, true));
    }
//    private void setRolesAllowedInView(String viewName, String ... roles) {
//
//    }
}
