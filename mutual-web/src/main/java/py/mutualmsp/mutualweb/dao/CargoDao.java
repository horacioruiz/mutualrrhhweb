/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class CargoDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Cargo> listaCargo() {
        List<Cargo> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Cargo.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Cargo cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Cargo cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Cargo> getListCargo(String valor) {
        List<Cargo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where  id=" + valor + ";", Cargo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Cargo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Cargo> getCargoByDescripcionLista(String valor) {
        List<Cargo> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Cargo.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Cargo c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Cargo getCargoByDescripcion(String valor) {
        Cargo dependencia = new Cargo();
        try {
            dependencia = (Cargo) em.createQuery("select c from Cargo c WHERE c.descripcion LIKE :descri")
                    .setParameter("descri", valor)
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dependencia;
    }

    public Cargo getCargoByDescripcionCargo(String valor) {
        Cargo lista = new Cargo();
        try {
            if (valor != null && !valor.equals("")) {
                lista = (Cargo) em.createNativeQuery("SELECT * FROM public.cargo "
                        + "                   where upper(descripcion) LIKE upper('" + valor + "');", Cargo.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } else {
                lista = (Cargo) em.createQuery("select c from Cargo c", Cargo.class)
                        .setMaxResults(1)
                        .getSingleResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Cargo getNinguno() {
        Cargo ninguno = null;
        try {
            ninguno = (Cargo) em.createNativeQuery("SELECT * FROM public.cargo "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Cargo.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Cargo) em.createQuery("select c from Cargo c")
                    .getResultList();
        }
        return ninguno;
    }
}
