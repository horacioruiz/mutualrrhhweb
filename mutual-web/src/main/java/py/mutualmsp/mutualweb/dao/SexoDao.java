/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Sexo;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class SexoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Sexo> listaSexos() {
        List<Sexo> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Sexo.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Sexo sexo){
        try {
            em.merge(sexo);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Sexo sexo) {
        try {
            em.remove(em.contains(sexo) ? sexo : em.merge(sexo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Sexo> getListSexo(String valor) {
        List<Sexo> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.sexo " +
"                   where  id="+valor+";", Sexo.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select s from Sexo s")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
