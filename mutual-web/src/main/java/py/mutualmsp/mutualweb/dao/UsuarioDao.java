/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Departamento;
import py.mutualmsp.mutualweb.entities.Usuario;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class UsuarioDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Usuario> listaUsuario() {
        List<Usuario> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("Usuario.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(Usuario cargo) {
        try {
            em.merge(cargo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Usuario cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Usuario> getListUsuario(String valor) {
        List<Usuario> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.usuario "
                        + "                   where  id=" + valor + ";", Usuario.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Usuario c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Usuario> getUsuarioByDescripcion(String valor) {
        List<Usuario> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.usuario "
                        + "                   where lower(descripcion) LIKE '" + valor + "%';", Usuario.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Usuario c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Usuario getNinguno() {
        Usuario ninguno = null;
        try {
            ninguno = (Usuario) em.createNativeQuery("SELECT * FROM public.usuario "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Usuario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Usuario) em.createQuery("select c from Usuario c")
                    .getResultList();
        }
        return ninguno;
    }

    public Usuario getByUsuario(Long id) {
        Usuario lista = new Usuario();
        try {
            lista = (Usuario) em.createQuery("select c from Usuario c JOIN FETCH c.id func WHERE c.id=:idUsuario")
                    .setParameter("idUsuario", id).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Usuario getUsuarioByIDFuncionario(Long id) {
        Usuario ninguno = null;
        try {
            String sql = "SELECT * FROM public.usuario "
                    + "               where activo=TRUE AND idfuncionario=" + id + " limit 1";
            System.out.println("SQL -> " + sql);
            ninguno = (Usuario) em.createNativeQuery(sql, Usuario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ninguno;
    }
}
