/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Requisito;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class RequisitoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Requisito> listaRequisito() {
        List<Requisito> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Requisito.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Requisito requisito){
        try {
            em.merge(requisito);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Requisito requisito) {
        try {
            em.remove(em.contains(requisito) ? requisito : em.merge(requisito));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Requisito> getListRequisito(String value) {
        List<Requisito> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.requisito " +
"                   where  lower(descripcion) like lower('%"+value+"%');", Requisito.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select r from Requisito r")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Requisito> getListRequisitoExistente(String value) {
        List<Requisito> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.requisito " +
                "where  lower(descripcion) like lower('%"+value+"%');", Requisito.class)
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
}
