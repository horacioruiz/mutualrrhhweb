/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Promotor;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class PromotorDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Promotor> listaPromotor() {
        List<Promotor> lista = new ArrayList<>();
        List<Promotor> lista2 = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Promotor.findAll").getResultList();
           for (Promotor promotor : lista) {
                promotor.setNombreFuncionario(promotor.getIdfuncionario().getApellido()+", "+promotor.getIdfuncionario().getNombre()+")");
                lista2.add(promotor);
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista2;
    }
    
    public void guardar(Promotor promotor){
        try {
            em.merge(promotor);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Promotor promotor) {
        try {
            em.remove(em.contains(promotor) ? promotor : em.merge(promotor));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Promotor> getListPromotor(String valor) {
        List<Promotor> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.promotor " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Promotor.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select p from Promotor p")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
