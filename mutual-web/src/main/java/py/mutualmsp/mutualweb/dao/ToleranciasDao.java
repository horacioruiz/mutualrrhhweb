package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Tolerancias;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class ToleranciasDao {

    @PersistenceContext
    private EntityManager em;

    public Tolerancias guardarTolerancias(Tolerancias usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Tolerancias> listadeTolerancias(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Tolerancias p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Tolerancias.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Tolerancias> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Tolerancias> listadeTolerancias() {
        try {
            String sql = "select u from Tolerancias u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Tolerancias> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Tolerancias p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, Tolerancias.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Tolerancias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Tolerancias getByIdUsuario(Long idusuario) {
        return (Tolerancias) em.find(Tolerancias.class, idusuario);
    }

    public List<Tolerancias> listadeToleranciasByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from Tolerancias p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Tolerancias.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Tolerancias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Tolerancias cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tolerancias> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Tolerancias p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, Tolerancias.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Tolerancias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Tolerancias listarPorIdFuncionario(Long id) {
        try {
            return (Tolerancias) em.createQuery("select hf from Tolerancias hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public Tolerancias getById(Long id) {
        Tolerancias marcacion = null;
        try {
            marcacion = (Tolerancias) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", Tolerancias.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new Tolerancias();
        }
        return marcacion;
    }

    public Tolerancias listarPorIdReloj(Long idReloj) {
        try {
            return (Tolerancias) em.createQuery("select hf from Tolerancias hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<Tolerancias> listarPorFechas(Date fechaDesde, Date fechaHasta, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (id != null) {
            filterFuncionario = "AND f.id="+id;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.tolerancias p LEFT join funcionario f on p.idfuncionario=f.id WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' "+filterFuncionario+" ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Tolerancias.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
