package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Suspenciones;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class RotacionesDao {

    @PersistenceContext
    private EntityManager em;

    public Rotaciones guardarRotaciones(Rotaciones usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }
    
    public List<Funcionario> getFuncionario(){
        List<Funcionario> lista = em.createNativeQuery("select * from funcionario ", Funcionario.class)
                .getResultList();
        return lista;
    }

    public List<Rotaciones> listadeRotaciones(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Rotaciones p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Rotaciones.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Rotaciones> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Rotaciones> listadeRotaciones() {
        try {
            String sql = "select u from Rotaciones u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Rotaciones> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Rotaciones p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, Rotaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Rotaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Rotaciones getByIdUsuario(Long idusuario) {
        return (Rotaciones) em.find(Rotaciones.class, idusuario);
    }

    public List<Rotaciones> listadeRotacionesByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from Rotaciones p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Rotaciones.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Rotaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Rotaciones cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Rotaciones> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Rotaciones p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, Rotaciones.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Rotaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Rotaciones listarPorIdFuncionario(Long id) {
        try {
            return (Rotaciones) em.createQuery("select hf from Rotaciones hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }
   
    public List<Rotaciones> listarPorIdFuncionarioFecha(Long id) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(new Date());
        String filterFuncionario = "";
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        try {
            //String sql = "select hf from Rotaciones hf JOIN FETCH hf.funcionario f WHERE f.id=:id AND hf.fecha>=:fec";
            String sql = "select * ";
            sql += "from rrhh.rotaciones p LEFT join funcionario f on p.id=f.id WHERE p.fecha >= '" + fechaDesdeText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Rotaciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return new ArrayList<>();
    }

    public Rotaciones getById(Long id) {
        Rotaciones marcacion = null;
        try {
            marcacion = (Rotaciones) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", Rotaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new Rotaciones();
        }
        return marcacion;
    }

    public Rotaciones listarPorIdReloj(Long idReloj) {
        try {
            return (Rotaciones) em.createQuery("select hf from Rotaciones hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<Rotaciones> listarPorFechas(Date fechaDesde, Date fechaHasta, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (id != null) {
            filterFuncionario = "AND p.idfuncionario=" + id;
        }
        try {
            //String sql = "select hf from Rotaciones hf JOIN FETCH hf.funcionario f WHERE f.id=:id AND hf.fecha>=:fec";
            String sql = " select * ";
            sql += "from rrhh.rotaciones p LEFT join funcionario f on p.idfuncionario=f.id WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' " + filterFuncionario + " ORDER BY p.fecha DESC";
            Query query = em.createNativeQuery(sql, Rotaciones.class);
            System.out.println("sql" + sql);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return new ArrayList<>();
    }
    
    //DANI
    public List<Rotaciones> listarPorFechas(Date fechaDesde, Date fechaHasta) {
       DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        try {
            String sql = " select * from rrhh.rotaciones r \n" +
            "where r.fecha between '" + fechaDesdeText + "' and '" + fechaHastaText + "'  ORDER BY r.fecha ASC";
            Query query = em.createNativeQuery(sql, Rotaciones.class);
            System.out.println("sql" + sql);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
        
    public List<Rotaciones> listarPorFechas2(Date fechaDesde, Date fechaHasta, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        try {
            //String sql = "select hf from Rotaciones hf JOIN FETCH hf.funcionario f WHERE f.id=:id AND hf.fecha>=:fec";
            String sql = "select * ";
            sql += "from rrhh.rotaciones p LEFT join funcionario f on p.id=f.id WHERE p.fecha >= '" + fechaDesdeText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Rotaciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return new ArrayList<>();
    }

    public List<Rotaciones> listarRotacionesPorPeriodoFuncionario(Date inicio, Date fin, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(inicio);
        String fechaHastaText = formatter.format(fin);
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.rotaciones p LEFT join funcionario f on p.id=f.id WHERE p.fecha BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Suspenciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
