/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Pais;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class PaisDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Pais> listaPaises() {
        List<Pais> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Pais.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Pais pais){
        try {
            em.merge(pais);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Pais pais) {
        try {
            em.remove(em.contains(pais) ? pais : em.merge(pais));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Pais> getListPais(String valor) {
        List<Pais> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.pais " +
"                   where  id="+valor+";", Pais.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select p from Pais p")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
