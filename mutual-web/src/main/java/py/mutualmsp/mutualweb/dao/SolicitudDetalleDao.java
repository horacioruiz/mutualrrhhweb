package py.mutualmsp.mutualweb.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SolicitudDetalleDao {

    @PersistenceContext
    private EntityManager em;

    public SolicitudDetalle guardarSolicitudDetalle(SolicitudDetalle usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<SolicitudDetalle> listadeSolicitudDetalle(int firstRow, int pageSize, String filter) {

        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from SolicitudDetalle p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudDetalle.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudDetalle> listadeSolicitudDetalle() {
        try {
            String sql = "select u from SolicitudDetalle u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SolicitudDetalle> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from SolicitudDetalle p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, SolicitudDetalle.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SolicitudDetalle getByIdUsuario(Long idusuario) {
        return (SolicitudDetalle) em.find(SolicitudDetalle.class, idusuario);
    }

    public List<SolicitudDetalle> listadeSolicitudDetalleByFormulario(int init, int finish, String filter) {
        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from SolicitudDetalle p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudDetalle.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(SolicitudDetalle cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SolicitudDetalle> listarPorIdSolicitud(Long id) {
        try {
            return em.createQuery("select sd from SolicitudDetalle sd join fetch sd.solicitud s where s.id = :id ORDER BY sd.id")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.fillInStackTrace());
            System.out.println("-->> " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
