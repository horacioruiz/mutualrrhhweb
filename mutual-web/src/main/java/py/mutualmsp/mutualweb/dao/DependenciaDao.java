/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DependenciaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Dependencia> listaDependencia() {
        List<Dependencia> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Dependencia.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Dependencia> getDependenciaByDescripcion(String valor, String valor2) {
        Dependencia dependencia = null;
        try {
            String sql = "SELECT * FROM public.dependencia dep "
                    + "where  lower(dep.descripcion) like lower('%" + valor + "%')\n"
                    + "or lower(dep.descripcion) like lower('%" + valor2 + "%');";
            List<Dependencia> listdependencia = em.createNativeQuery(sql, Dependencia.class)
                    .getResultList();
            return listdependencia;
        } catch (Exception e) {
            e.printStackTrace();
            return em.createQuery("select f from Dependencia f")
                    .getResultList();
        }
        //return dependencia;
    }
    
    public Dependencia getDependenciaByDescripcionCargo(String valor) {
        Dependencia dependencia = null;
        try {
            String sql = "SELECT * FROM public.dependencia dep "
                    + "where  lower(dep.descripcion) like lower('%" + valor + "%');";
            List<Dependencia> listdependencia = em.createNativeQuery(sql, Dependencia.class)
                    .getResultList();
            dependencia = listdependencia.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            dependencia = (Dependencia) em.createQuery("select f from Dependencia f")
                    .getResultList();
        }
        return dependencia;
    }

    public Dependencia getDependenciaByDescripcionDependencia(String valor) {
        Dependencia dependencia = new Dependencia();
        try {
            dependencia = (Dependencia) em.createQuery("select c from Dependencia c WHERE c.descripcion=:descri")
                    .setParameter("descri", valor)
                    //                    .setMaxResults(1)
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dependencia;
    }

    public void guardar(Dependencia dependencia) {
        try {
            em.merge(dependencia);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(Dependencia dependencia) {
        try {
            em.remove(em.contains(dependencia) ? dependencia : em.merge(dependencia));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Dependencia> getListDependencia(String valor) {
        List<Dependencia> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.dependencia "
                        + "                   where  lower(descripcion) like lower('%" + valor + "%');", Dependencia.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select d from Dependencia d")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Dependencia getNinguno() {
        Dependencia ninguno = null;
        try {
            ninguno = (Dependencia) em.createNativeQuery("SELECT * FROM public.dependencia "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Dependencia.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Dependencia) em.createQuery("select d from Dependencia d")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Dependencia> listarDepartamentosPadres() {
        List<Dependencia> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from Dependencia c WHERE c.iddependenciapadre=7").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Dependencia> listarSubDependencia(long idDependenciaPadre) {
        List<Dependencia> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from Dependencia c WHERE c.iddependenciapadre=:idPadre")
                    .setParameter("idPadre", idDependenciaPadre).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Dependencia getDependenciaById(Long id) {
        Dependencia dependencia = null;
        try {
            dependencia = (Dependencia) em.createNativeQuery("SELECT * FROM public.dependencia dep "
                    + "where id=" + id + ";", Dependencia.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            dependencia = (Dependencia) em.createQuery("select f from Dependencia f")
                    .getResultList();
        }
        return dependencia;
    }

    public Dependencia getDependenciaByNivel(String nivel) {
        try {
            nivel = nivel.replace(".00", "");
            nivel = nivel.substring(0, (nivel.length() - 3));
        } catch (Exception e) {
        } finally {
        }
        Dependencia dependencia = null;
        try {
            dependencia = (Dependencia) em.createNativeQuery("SELECT * FROM public.dependencia dep "
                    + "where nivel like '" + nivel + "%' AND iddependenciapadre=7;", Dependencia.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            dependencia = new Dependencia();
        }
        return dependencia;
    }

    public Dependencia buscarPadreHijo(Dependencia padre, Dependencia hijo) {
        Dependencia dependencia = null;
        try {
            String sql = "SELECT * FROM public.dependencia dep "
                    + "where dep.id=" + hijo.getId() + " AND dep.iddependenciapadre=" + padre.getId() + ";";
            dependencia = (Dependencia) em.createNativeQuery(sql, Dependencia.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            dependencia = null;
        }
        return dependencia;
    }
}
