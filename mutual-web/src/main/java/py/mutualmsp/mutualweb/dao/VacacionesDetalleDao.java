/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.entities.VacacionesDetalle;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class VacacionesDetalleDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<VacacionesDetalle> listaVacacionesDetalle() {
        List<VacacionesDetalle> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from VacacionesDetalle c").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public VacacionesDetalle listaVacacionesDetallePorId(long idVaca) {
        VacacionesDetalle vacas = new VacacionesDetalle();
        try {
            vacas = (VacacionesDetalle) em.createQuery("select c from VacacionesDetalle c JOIN FETCH c.solicitud s WHERE c.id=:idVaca")
                    .setParameter("idVaca", idVaca).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vacas;
    }
    public VacacionesDetalle listaVacacionesDetallePorIdSolicitud(long idVaca) {
        VacacionesDetalle vacas = new VacacionesDetalle();
        try {
            vacas = (VacacionesDetalle) em.createQuery("select c from VacacionesDetalle c JOIN FETCH c.solicitud s WHERE s.id=:idVaca")
                    .setParameter("idVaca", idVaca).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vacas;
    }

    public List<VacacionesDetalle> listaVacacionesDetallePorIdVacaciones(long idVaca) {
        List<VacacionesDetalle> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from VacacionesDetalle c JOIN FETCH c.vacaciones v WHERE v.id=:idVacas ORDER BY c.id ASC")
                    .setParameter("idVacas", idVaca).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public VacacionesDetalle guardar(VacacionesDetalle cargo) {
        if (cargo.getId() == null) {
            em.persist(cargo);
        } else {
            em.merge(cargo);
        }
        em.flush();
        return cargo;

    }

    public void borrar(VacacionesDetalle cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<VacacionesDetalle> getListVacaciones(String valor) {
        List<VacacionesDetalle> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones "
                        + "where id=" + valor + " ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<VacacionesDetalle> getVacacionesByDescripcion(String valor) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.id=:idFunc";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", Long.parseLong(valor));
            int i = query.getResultList().size();
            System.out.print(i);
            List<VacacionesDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public VacacionesDetalle getNinguno() {
        VacacionesDetalle ninguno = null;
        try {
            ninguno = (VacacionesDetalle) em.createNativeQuery("SELECT * FROM rrhh.formulario "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Vacaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (VacacionesDetalle) em.createQuery("select c from Vacaciones c")
                    .getResultList();
        }
        return ninguno;
    }

    public List<VacacionesDetalle> listadeVacaciones() {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p ORDER BY p.id";
            Query query = em.createQuery(sql, Vacaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<VacacionesDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<VacacionesDetalle> listadeVacaciones(long idFuncionario) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.id=:idFunc ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", idFuncionario);
            int i = query.getResultList().size();
            System.out.print(i);
            List<VacacionesDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<VacacionesDetalle> listadeVacacionesPorCiFuncionario(String value) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.cedula=:ci ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("ci", "'" + value + "'");
            int i = query.getResultList().size();
            System.out.print(i);
            List<VacacionesDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<VacacionesDetalle> listarPorVacacionesSolicitud(String periodo) {
//        try {
//            String sql = "select p ";
//            sql += "from Vacaciones p WHERE p.periodo=:periodo AND p.solicitud is not null ORDER BY p.id ASC";
//            Query query = em.createQuery(sql, Vacaciones.class);
//            query.setParameter("periodo", "'" + periodo + "'");
//            int i = query.getResultList().size();
//            System.out.print(i);
//            List<Vacaciones> results = query.getResultList();
//            return results;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
        List<VacacionesDetalle> lista = new ArrayList<>();
        try {
            if (periodo != null && !periodo.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones "
                        + "where periodo='" + periodo + "' AND idsolicitud is not null ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
