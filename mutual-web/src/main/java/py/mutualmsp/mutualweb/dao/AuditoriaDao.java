/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Auditoria;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class AuditoriaDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Auditoria> listaAuditoria() {
        List<Auditoria> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Auditoria.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Auditoria auditoria){
        try {
            em.merge(auditoria);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Auditoria auditoria) {
        try {
            em.remove(em.contains(auditoria) ? auditoria : em.merge(auditoria));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Auditoria> getListAuditoria(String valor) {
        List<Auditoria> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.auditoria " +
"                   where  id="+valor+";", Auditoria.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select a from Auditoria a")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
