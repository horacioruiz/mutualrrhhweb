/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Estadoverificacion;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class EstadoverificacionDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<Estadoverificacion> listaEstadoverificacion() {
        List<Estadoverificacion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Estadoverificacion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Estadoverificacion estadoverificacion) {
        try {
            em.merge(estadoverificacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(Estadoverificacion estadoverificacion) {
        try {
            em.remove(em.contains(estadoverificacion) ? estadoverificacion : em.merge(estadoverificacion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Estadoverificacion> getListEstadoverificacion(String valor) {
        List<Estadoverificacion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")){
                lista = em.createNativeQuery("SELECT * FROM public.estadoverificacion " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Estadoverificacion.class)
                   .getResultList();
            } else {
                lista = em.createQuery("select e from Estadoverificacion e")
                   .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public Estadoverificacion getPendiente() {
        Estadoverificacion pendiente = null;
        try {
            pendiente = (Estadoverificacion) em.createNativeQuery("SELECT * FROM public.estadoverificacion " +
            "               where  lower(descripcion) like lower('%PENDIENTE%');", Estadoverificacion.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            pendiente = (Estadoverificacion) em.createQuery("select e from Estadoverificacion e")
                .getResultList();
        }
        return pendiente;
    }
}
