/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Empresa;
import py.mutualmsp.mutualweb.entities.Vacaciones;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class VacacionesDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Vacaciones> listaVacaciones() {
        List<Vacaciones> lista = new ArrayList<>();

        try {
            lista = em.createQuery("select c from Vacaciones c JOIN FETCH c.funcionario f WHERE f.idestado=13").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Vacaciones listaVacacionesPorId(long idVaca) {
        Vacaciones vacas = new Vacaciones();
        try {
//            vacas = (Vacaciones) em.createQuery("select c from Vacaciones c JOIN FETCH c.funcionario f WHERE c.id=:idVaca AND f.idestado=13")
//                    .setParameter("idVaca", idVaca).getSingleResult();
            vacas = (Vacaciones) em.createNativeQuery("SELECT * FROM rrhh.vacaciones v LEFT JOIN funcionario f ON f.id=v.idfuncionario "
                        + "where v.id =" + idVaca + " AND f.idestado=13 ORDER BY f.id limit 1;", Vacaciones.class)
                        .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vacas;
    }

    public Vacaciones guardar(Vacaciones cargo) {
        if (cargo.getId() == null) {
            em.persist(cargo);
        } else {
            em.merge(cargo);
        }
        em.flush();
        return cargo;

    }

    public void borrar(Vacaciones cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Vacaciones> getListVacaciones(String valor) {
        List<Vacaciones> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones v JOIN FETCH v.funcionario f ON f.id=v.idfuncionario "
                        + "where v.idfuncionario =" + valor + " AND f.idestado=13 ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c JOIN FETCH c.funcionario f WHERE f.idestado=13")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Vacaciones> getVacacionesByDescripcion(String valor) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.id=:idFunc AND f.idestado=13";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", Long.parseLong(valor));
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vacaciones getNinguno() {
        Vacaciones ninguno = null;
        try {
            ninguno = (Vacaciones) em.createNativeQuery("SELECT * FROM rrhh.formulario "
                    + "               where  lower(descripcion) like lower('%NINGUNO%');", Vacaciones.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Vacaciones) em.createQuery("select c from Vacaciones c JOIN FETCH c.funcionario f WHERE f.idestado=13")
                    .getResultList();
        }
        return ninguno;
    }

    public List<Vacaciones> listadeVacaciones() {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.idestado=13 ORDER BY p.id";
            Query query = em.createQuery(sql, Vacaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacaciones(long idFuncionario) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.id=:idFunc AND f.idestado=13 ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", idFuncionario);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vacaciones listadeVacacionesPorPeriodo(long idFuncionario, String periodo) {
        try {
            String sql = "select * ";
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario "
                    + " left join public.dependencia depen on depen.id=f.id WHERE p.periodo='" + periodo + "' and "
                    + " f.id=" + idFuncionario + " AND f.idestado=13 ORDER BY p.id ASC";
            Query query = em.createNativeQuery(sql, Vacaciones.class);
//            query.setParameter("idFunc", idFuncionario);
//            query.setParameter("periodo", periodo);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results.get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesON(long idFuncionario) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.id=:idFunc AND p.cantdiarestante>0 AND f.idestado=13 ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("idFunc", idFuncionario);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesPorCiFuncionario(String value) {
        try {
            String sql = "select p ";
            sql += "from Vacaciones p JOIN FETCH p.funcionario f WHERE f.cedula=:ci AND f.idestado=13 ORDER BY p.id ASC";
            Query query = em.createQuery(sql, Vacaciones.class);
            query.setParameter("ci", "'" + value + "'");
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listarPorVacacionesSolicitud(String periodo) {
//        try {
//            String sql = "select p ";
//            sql += "from Vacaciones p WHERE p.periodo=:periodo AND p.solicitud is not null ORDER BY p.id ASC";
//            Query query = em.createQuery(sql, Vacaciones.class);
//            query.setParameter("periodo", "'" + periodo + "'");
//            int i = query.getResultList().size();
//            System.out.print(i);
//            List<Vacaciones> results = query.getResultList();
//            return results;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
        List<Vacaciones> lista = new ArrayList<>();
        try {
            if (periodo != null && !periodo.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM rrhh.vacaciones v left JOIN public.funcionario f on f.id = f.id  "
                        + "where v.periodo='" + periodo + "' AND v.idsolicitud is not null AND f.idestado=13 ORDER BY id;", Vacaciones.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select c from Vacaciones c JOIN FETCH Funcionario f WHERE f.idestado=13")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Vacaciones> listadeVacaciones(Long id, Empresa filial, String periodo) {
        try {
            String sql = "select * ";
            Query query = null;
            if (id == 0) {
                sql += " from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                    "left join public.empresa e on e.id=f.idfilial WHERE p.periodo='" +periodo+ "' and \n" +
                    "e.id=:idfilial AND f.idestado=13 ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class)
                        .setParameter("idfilial", filial.getId());
            } else {
                sql += " from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                " left join public.empresa e on e.id=f.idfilial  WHERE p.periodo='"+periodo+"' and \n" +
                " e.id=:idfilial AND \n" +
                " f.id= "+id+" AND f.idestado=13 ORDER BY p.id ASC";
                query = em.createNativeQuery(sql, Vacaciones.class)
                        .setParameter("idfilial", filial.getId());
            }

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacaciones(Long id, String periodo) {
        try {
            String sql = "select * ";
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario "
                    + " left join public.cargo depen on depen.id=f.idcargo WHERE p.periodo='" + periodo + "' and "
                    + " f.id=" + id + " AND f.idestado=13 ORDER BY p.id ASC";
            Query query = em.createNativeQuery(sql, Vacaciones.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> listadeVacacionesFilial(Long id, Empresa filial) {
        try {
            String sql = "select * ";
            Query query = null;
            if (id == 0 && filial != null) {
                sql += " from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                " left join public.empresa e on e.id=f.idfilial  WHERE e.id=:idfilial AND f.idestado=13 \n" +
                " ORDER BY f.nombre ASC";
                query = em.createNativeQuery(sql, Vacaciones.class)
                        .setParameter("idfilial", filial.getId());
            } else if (id != 0 && filial != null) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                " left join public.empresa e  on e.id=f.idfilial WHERE e.id = :idfilial AND f.idestado=13 and \n" +
                " f.id= "+id+"  ORDER BY p.id";
                query = em.createNativeQuery(sql, Vacaciones.class);
            } else if (id != 0 && filial == null) {
                sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                " WHERE f.idestado=13 and f.id=:id \n" +
                " ORDER BY f.nombre asc, p.periodo";
                query = em.createNativeQuery(sql, Vacaciones.class)
                        .setParameter("id", id);
            } else {
                sql += " from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario \n" +
                " left join public.empresa e on e.id=f.idfilial WHERE e.id= :idfilial AND f.idestado=13 \n" +
                " ORDER BY f.nombre asc, p.periodo;";
                query = em.createNativeQuery(sql, Vacaciones.class)
                        .setParameter("idfilial", filial.getId());
            }

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Vacaciones> filtrarVacaciones(Long id, String dependencia, String filial, String periodo) {
        try {
            Query query = null;
            String parametro = "";
            if (!dependencia.equalsIgnoreCase("")) {
                parametro += " AND depen.id=" + dependencia + " ";
            }
            if (!filial.equalsIgnoreCase("")) {
                parametro += " AND e.id=" + filial + " ";
            }
            if (!periodo.equalsIgnoreCase("")) {
                parametro += " AND periodo like '%" + periodo + "%' ";
            }
            if (id > 0) {
                parametro += " AND f.id=" + id + " ";
            }
            String sql = "SELECT * "
                    + " from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario "
                    + " left join public.dependencia depen on depen.id=f.iddependencia "
                    + " left join public.empresa e on e.id=f.idfilial "
                    + " WHERE f.idestado=13 " + parametro + " ORDER BY f.nombre, p.periodo ASC";
            System.out.println("filtrarVacaciones ==> " + sql);
            query = em.createNativeQuery(sql, Vacaciones.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<Vacaciones> filtroPorFechaIngreso(Date fechaDesde, Date fechaHasta){
        DateFormat formatter = new SimpleDateFormat("MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        try {
            String sql = "SELECT subquery.*, f.nombre || ' ' || f.apellido AS nombre, f.fechaingreso \n" +
            "FROM (SELECT DISTINCT ON (v.idfuncionario) v.*\n" +
            "FROM rrhh.vacaciones v\n" +
            "lEFT JOIN public.funcionario f ON f.id = v.idfuncionario\n" +
            "WHERE f.idestado = 13\n" +
            "AND f.fechaingreso IS NOT NULL\n" +
            "AND TO_CHAR(f.fechaingreso, 'MM-dd') BETWEEN '"+fechaDesdeText+"' AND '"+fechaHastaText+"'\n" +
            "ORDER BY v.idfuncionario -- Ordena por funcionario y fecha de inicio de la vacación\n" +
            ") AS subquery\n" +
            "LEFT JOIN funcionario f ON f.id = subquery.idfuncionario\n" +
            "ORDER BY f.fechaingreso ASC;";
            Query query = em.createNativeQuery(sql, Vacaciones.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<Vacaciones> listadeVacacionesCargoData(Long id, Dependencia depen, String periodo) {
        String periodoData = "";
        try {
            if (!periodo.equals("")) {
                periodoData = " AND p.periodo='" + periodo + "'";
            }
            String sql = "select * ";
            Query query = null;
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario "
                    + " left join public.dependencia depen on depen.id=f.id WHERE depen.id=" + depen.getId() + " AND f.idestado=13 "
                    + periodoData + " ORDER BY f.nombre ASC";
            query = em.createNativeQuery(sql, Vacaciones.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;

        } catch (Exception e) {
            if (!periodo.equals("")) {
                periodoData = " WHERE p.periodo='" + periodo + "'";
            }
            String sql = "select * ";
            Query query = null;
            sql += "from rrhh.vacaciones p left JOIN public.funcionario f on f.id = p.idfuncionario "
                    + " left join public.dependencia depen on depen.id=f.id "
                    + periodoData + " WHERE f.idestado=13 ORDER BY f.nombre ASC";
            query = em.createNativeQuery(sql, Vacaciones.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Vacaciones> results = query.getResultList();
            return results;
        }
    }

}
