/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Empresa;

/**
 *
 * @author HVillalba
 */


@Stateless
public class EmpresaDao {
    
    
    @PersistenceContext
    EntityManager em;
    
    
    public List<Empresa> getFiliales(){
        List<Empresa> lista = em.createQuery("select e from Empresa e order by e.id").getResultList();
        
        return lista;
    }
    
}
