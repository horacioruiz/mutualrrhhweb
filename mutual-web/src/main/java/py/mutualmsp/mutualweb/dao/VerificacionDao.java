/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import py.mutualmsp.mutualweb.entities.Verificacion;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class VerificacionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Verificacion> listaVerificacion() {
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Verificacion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Verificacion verificacion, List<Verificaciondetalle> detalle){
        try {
            if (verificacion.getIdverificacion() == null) {
               em.persist(verificacion);
            }else{
                em.merge(verificacion);
            }
            em.flush();
            for (Verificaciondetalle verificaciondetalle : detalle) {
                verificaciondetalle.setIdverificacion(verificacion);
                em.merge(verificaciondetalle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Verificacion verificacion) {
        try {
            em.remove(em.contains(verificacion) ? verificacion : em.merge(verificacion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Verificacion> getListVerificacion(String value) {
        List<Verificacion> lista = new ArrayList<>();
       try {
           if (value != null && !value.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.verificacion " +
"                   where  lower(observacion) like lower('%"+value+"%');", Verificacion.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select v from Verificacion v")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public List<Verificacion> getVerificacionByFecha(java.util.Date fdesde, java.util.Date fhasta) {
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select v from Verificacion v where v.fechaverificacion between :fdesde and :fhasta")
                    .setParameter("fdesde", fdesde, TemporalType.DATE)
                    .setParameter("fhasta", fhasta, TemporalType.DATE)
                    .getResultList();
        } catch (Exception e) {
        }
        return lista;
    }
    
    public List<Verificacion> getVerificacionByTodos(String fdesde, String fhasta, String tipooperacion, String estado, String verificador) {
        List<Verificacion> lista = new ArrayList<>();
        try {
            /*System.out.println("fdesde: " + fdesde );
            System.out.println("fhasta: " + fhasta);
            System.out.println("tipooperacion: " + tipooperacion);
            System.out.println("estado: " + estado);
            System.out.println("verificador: " + verificador);
            String sql = "select v from Verificacion v where v.fechaverificacion between TO_DATE(:fdesde, 'YYYY-MM-DD') and TO_DATE(:fhasta, 'YYYY-MM-DD')"
                    + " and v.idtipooperacion.idtipooperacion.descripcion like :tipooperacion"
                    + " and v.idestadoverificacion.idestadoverificacion.descripcion like :estado"
                    + " and (v.idverificador.id.nombre || ' ' || v.idverificador.id.apellido) like :verificador";
            System.out.println("SQL: " + sql );
            lista = em.createQuery(sql)
                    .setParameter("fdesde", fdesde)  //.setParameter("fdesde", fdesde, TemporalType.DATE)
                    .setParameter("fhasta", fhasta) //.setParameter("fhasta", fhasta, TemporalType.DATE)
                    .setParameter("tipooperacion", tipooperacion)
                    .setParameter("estado", estado)
                    .setParameter("verificador", verificador)
                    .getResultList();*/
            String sql = "SELECT * FROM public.verificacion v"
                    + " LEFT JOIN public.tipooperacion t ON v.idtipooperacion = t.idtipooperacion"
                    + " LEFT JOIN public.estadoverificacion e ON v.idestadoverificacion = e.idestadoverificacion"
                    + " LEFT JOIN public.funcionario f ON v.idverificador = f.id"
                    + " WHERE v.fechaverificacion between TO_DATE('"+fdesde+"', 'YYYY-MM-DD') AND TO_DATE('"+fhasta+"', 'YYYY-MM-DD')"
                    + " AND t.descripcion like '"+tipooperacion+"'"
                    + " AND e.descripcion like '"+estado+"'"
                    + " AND (f.nombre || ' ' || f.apellido) like '"+verificador+"';";
            System.out.println("SQL: " + sql );
            lista = em.createNativeQuery(sql, Verificacion.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public List<Verificacion> getVerificacionByTipoOperacion(Long id){
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select v from Verificacion v where v.idtipooperacion.idtipooperacion = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return lista;
    }
    
    public List<Verificacion> getVerificacionByEstado(Long id){
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select v from Verificacion v where v.idestadoverificacion.idestadoverificacion = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public List<Verificacion> getVerificacionByVerificador(String valor){
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select v from Verificacion v where lower(v.idverificador.nombre || ' ' || v.idverificador.apellido) like lower('%"+valor+"%')")
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public List<Verificacion> getVerificacionByVerificador2(Long id){
        List<Verificacion> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select v from Verificacion v where v.idverificador.id = :id ")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
}
