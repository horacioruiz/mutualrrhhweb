/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;

/**
 *
 * @author Dbarreto
 */

@Singleton
public class ListaDetalleVerificacionCache {
    
    
    List<Verificaciondetalle> lista = new ArrayList();

    public List<Verificaciondetalle> getLista() {
        return lista;
    }
    public void setLista(List<Verificaciondetalle> lista) {
        this.lista = lista;
    }
    
    public void cargarDetalle(Verificaciondetalle verificaciondetalle){
        List<Verificaciondetalle> listaFinal = new ArrayList();
        try {
            for (Verificaciondetalle param : lista) {
                if (param.getIdrequisito() == verificaciondetalle.getIdrequisito()) {
                    listaFinal.add(verificaciondetalle);
                }else{
                    listaFinal.add(param);
                }
            }
            lista = listaFinal;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
