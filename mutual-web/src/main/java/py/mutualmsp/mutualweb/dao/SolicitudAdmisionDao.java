/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.SolicitudAdmision;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class SolicitudAdmisionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<SolicitudAdmision> listaSolicitudAdmision() {
        List<SolicitudAdmision> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("SolicitudAdmision.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(SolicitudAdmision solicitudAdmision){
        try {
            em.merge(solicitudAdmision);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(SolicitudAdmision solicitudAdmision) {
        try {
            em.remove(em.contains(solicitudAdmision) ? solicitudAdmision : em.merge(solicitudAdmision));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SolicitudAdmision> getListSolicitudAdmision(String valor) {
        List<SolicitudAdmision> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.solicitud_admision " +
"                   where  id="+valor+";", SolicitudAdmision.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select s from SolicitudAdmision s")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
