package py.mutualmsp.mutualweb.dao;

import com.vaadin.ui.Notification;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import javax.persistence.TypedQuery;
import py.mutualmsp.mutualweb.entities.AdjuntosAdmision;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class HorarioExporadicoDao {

    @PersistenceContext
    private EntityManager em;

    public HorarioExporadico guardarHorarioExporadico(HorarioExporadico usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<HorarioExporadico> listadeHorarioExporadico(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioExporadico p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, HorarioExporadico.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioExporadico> listadeHorarioExporadico() {
        try {
            String sql = "select u from HorarioExporadico u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioExporadico> listadeHorarioExporadicoTrue() {
        try {
            String sql = "select u from HorarioExporadico u JOIN FETCH u.funcionario f WHERE f.idestado=13";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioExporadico> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from HorarioExporadico p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, HorarioExporadico.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioExporadico getByIdUsuario(Long idusuario) {
        return (HorarioExporadico) em.find(HorarioExporadico.class, idusuario);
    }

    public List<HorarioExporadico> listadeHorarioExporadicoByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioExporadico p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, HorarioExporadico.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public void borrar(HorarioExporadico cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public List<HorarioExporadico> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from HorarioExporadico p JOIN FETCH p.formulario f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, HorarioExporadico.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioExporadico listarPorIdFuncionario(Long id) {
        try {
            return (HorarioExporadico) em.createQuery("select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<HorarioExporadico> listarPorIdFuncionarioF(Long id) {
        try {
            String sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE f.id=:id";
            Query query = em.createQuery(sql, HorarioExporadico.class).setParameter("id", id);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioExporadico getById(Long id) {
        HorarioExporadico marcacion = null;
        try {
            marcacion = (HorarioExporadico) em.createNativeQuery("SELECT * FROM rrhh.horario_funcionario "
                    + " where idreloj=" + id + ";", HorarioExporadico.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            marcacion = new HorarioExporadico();
        }
        return marcacion;
    }

    public HorarioExporadico listarPorIdReloj(Long idReloj) {
        try {
            return (HorarioExporadico) em.createQuery("select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<HorarioExporadico> listarExistenciaDatos(HorarioExporadico he) {
        String fechaHastaText;
        String fechaDesdeText;
        SimpleDateFormat formatter;
        String sql = "";
        TypedQuery query = null;
        if ((he.getLunes().booleanValue() || he.getMartes().booleanValue() || he.getMiercoles().booleanValue() || he.getJueves().booleanValue() || he.getViernes().booleanValue() || he.getSabado().booleanValue() || he.getDomingo().booleanValue()) && he.getFechadesde() != null && he.getFechahasta() != null) {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            fechaDesdeText = formatter.format(he.getFechadesde());
            fechaHastaText = formatter.format(he.getFechahasta());
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE  f.id=:idFunc AND (hf.lunes=:lunes AND hf.martes=:martes AND hf.miercoles=:miercoles AND hf.jueves=:jueves AND hf.viernes=:viernes AND hf.sabado=:sabado AND hf.domingo=:domingo) AND  ((hf.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (hf.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR  (hf.fechadesde<='" + fechaDesdeText + " 00:00:00' AND hf.fechahasta>='" + fechaHastaText + " 23:59:59'))";
            query = this.em.createQuery(sql, HorarioExporadico.class)
                    .setParameter("lunes", he.getLunes() == null ? false : he.getLunes())
                    .setParameter("martes", he.getMartes() == null ? false : he.getMartes())
                    .setParameter("miercoles", he.getMiercoles() == null ? false : he.getMiercoles())
                    .setParameter("jueves", he.getJueves() == null ? false : he.getJueves())
                    .setParameter("viernes", he.getViernes() == null ? false : he.getViernes())
                    .setParameter("idFunc", he.getFuncionario().getId())
                    .setParameter("sabado", he.getSabado() == null ? false : he.getSabado())
                    .setParameter("domingo", he.getDomingo() == null ? false : he.getDomingo());
        } else if (he.getFechadesde() != null && he.getFechahasta() != null) {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            fechaDesdeText = formatter.format(he.getFechadesde());
            fechaHastaText = formatter.format(he.getFechahasta());
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE f.id=:idFunc  AND ((hf.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (hf.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR  (hf.fechadesde<='" + fechaDesdeText + " 00:00:00' AND hf.fechahasta>='" + fechaHastaText + " 23:59:59')) ";
            query = this.em.createQuery(sql, HorarioExporadico.class).setParameter("idFunc", he.getFuncionario().getId());
        } else {
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f WHERE  (f.id=:idFunc) AND (hf.lunes=:lunes AND hf.martes=:martes AND hf.miercoles=:miercoles AND hf.jueves=:jueves AND hf.viernes=:viernes AND hf.sabado=:sabado AND hf.domingo=:domingo AND )";
            query = this.em.createQuery(sql, HorarioExporadico.class)
                    .setParameter("lunes", he.getLunes() == null ? false : he.getLunes())
                    .setParameter("martes", he.getMartes() == null ? false : he.getMartes())
                    .setParameter("miercoles", he.getMiercoles() == null ? false : he.getMiercoles())
                    .setParameter("jueves", he.getJueves() == null ? false : he.getJueves())
                    .setParameter("viernes", he.getViernes() == null ? false : he.getViernes())
                    .setParameter("idFunc", he.getFuncionario().getId())
                    .setParameter("sabado",he.getSabado() == null ? false : he.getSabado())
                    .setParameter("domingo", he.getDomingo() == null ? false : he.getDomingo());
        }

        try {
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        } finally {
        }
    }

    public List<HorarioExporadico> listarExistenciaDatosWithHL(HorarioExporadico he) {
        String fechaHastaText;
        String fechaDesdeText;
        SimpleDateFormat formatter;
        String sql = "";
        TypedQuery query = null;
        he.setLunes(he.getLunes()==null ? false :he.getLunes());
        he.setMartes(he.getMartes()== null ? false : he.getMartes());
        he.setMiercoles(he.getMiercoles()== null ? false : he.getMiercoles());
        he.setJueves(he.getJueves()== null ? false : he.getJueves());
        he.setViernes(he.getViernes()== null ? false : he.getViernes());
        he.setSabado(he.getSabado()== null ? false : he.getSabado());
        he.setDomingo(he.getDomingo()== null ? false : he.getDomingo());
        if ((he.getLunes() 
                || he.getMartes().booleanValue() 
                || he.getMiercoles().booleanValue() 
                || he.getJueves().booleanValue() 
                || he.getViernes().booleanValue() 
                || he.getSabado().booleanValue() 
                || he.getDomingo().booleanValue())
                && he.getFechadesde() != null && he.getFechahasta() != null) {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            fechaDesdeText = formatter.format(he.getFechadesde());
            fechaHastaText = formatter.format(he.getFechahasta());
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f JOIN FETCH hf.horarioLaboral hl WHERE  f.id=:idFunc AND (hf.lunes=:lunes OR hf.martes=:martes OR hf.miercoles=:miercoles OR hf.jueves=:jueves OR hf.viernes=:viernes OR hf.sabado=:sabado OR hf.domingo=:domingo) AND ((hf.fechadesde BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') OR (hf.fechahasta BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') OR  (hf.fechadesde<='" + fechaDesdeText + "' AND hf.fechahasta>='" + fechaHastaText + "'))";
            query = this.em.createQuery(sql, HorarioExporadico.class)
                    .setParameter("lunes", he.getLunes() == null ? false : he.getLunes())
                    .setParameter("martes", he.getMartes() == null ? false : he.getMartes())
                    .setParameter("miercoles", he.getMiercoles() == null ? false : he.getMiercoles())
                    .setParameter("jueves", he.getJueves() == null ? false : he.getJueves())
                    .setParameter("viernes", he.getViernes() == null ? false : he.getViernes())
                    .setParameter("idFunc", he.getFuncionario().getId())
                    .setParameter("sabado", he.getSabado() == null ? false : he.getSabado())
                    .setParameter("domingo", he.getDomingo() == null ? false : he.getDomingo());
        } else if (he.getFechadesde() != null && he.getFechahasta() != null) {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            fechaDesdeText = formatter.format(he.getFechadesde());
            fechaHastaText = formatter.format(he.getFechahasta());
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f JOIN FETCH hf.horarioLaboral hl WHERE f.id=:idFunc  AND ((hf.fechadesde BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') OR (hf.fechahasta BETWEEN '" + fechaDesdeText + "' AND '" + fechaHastaText + "') OR  (hf.fechadesde<='" + fechaDesdeText + "' AND hf.fechahasta>='" + fechaHastaText + "')) ";
            query = this.em.createQuery(sql, HorarioExporadico.class)
                    .setParameter("idFunc", he.getFuncionario().getId());
        } else {
            sql = "select hf from HorarioExporadico hf JOIN FETCH hf.funcionario f JOIN FETCH hf.horarioLaboral hl WHERE  (f.id=:idFunc) AND (hf.lunes=:lunes OR hf.martes=:martes OR hf.miercoles=:miercoles OR hf.jueves=:jueves OR hf.viernes=:viernes OR hf.sabado=:sabado OR hf.domingo=:domingo)";
            query = this.em.createQuery(sql, HorarioExporadico.class)
                    .setParameter("lunes", he.getLunes() == null ? false : he.getLunes())
                    .setParameter("martes", he.getMartes() == null ? false : he.getMartes())
                    .setParameter("miercoles", he.getMiercoles() == null ? false : he.getMiercoles())
                    .setParameter("jueves", he.getJueves() == null ? false : he.getJueves())
                    .setParameter("viernes", he.getViernes() == null ? false : he.getViernes())
                    .setParameter("idFunc", he.getFuncionario().getId())
                    .setParameter("sabado", he.getSabado() == null ? false : he.getSabado())
                    .setParameter("domingo", he.getDomingo() == null ? false : he.getDomingo());
        }
        try {
            List results;
            int i = query.getResultList().size();
            System.out.print(i);
            List list = results = query.getResultList();
            return list;
        }
        catch (Exception e) {
            e.getLocalizedMessage();
            List<HorarioExporadico> list = null;
            return list;
        }
    }

    public boolean consultarExistenciasPorDias(String dia, Date desde, Date hasta, Long idFuncionario) {
        String sql = "";
        Query query = null;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(desde);
        String fechaHastaText = formatter.format(hasta);

        if (dia.equalsIgnoreCase("LUNES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.lunes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("MARTES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.martes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("MIERCOLES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.miercoles=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("JUEVES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.jueves=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("VIERNES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.viernes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("SABADO")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.sabado=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("DOMINGO")) {
                sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE   func.id=" + idFuncionario + " AND he.domingo=TRUE AND  ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR  (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        }

        try {
            query = em.createNativeQuery(sql, HorarioExporadico.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            if (i > 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.getLocalizedMessage();
            return false;
        } finally {
        }
    }

    public List<HorarioExporadico> consultarExistenciasPorDiasRecuperarDatos(String dia, Date desde, Date hasta, Long idFuncionario) {
        String sql = "";
        Query query = null;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(desde);
        String fechaHastaText = formatter.format(hasta);

        if (dia.equalsIgnoreCase("LUNES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.lunes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("MARTES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.martes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("MIERCOLES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.miercoles=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("JUEVES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.jueves=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("VIERNES")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.viernes=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("SABADO")) {
            sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE  "
                    + " func.id=" + idFuncionario + " AND he.sabado=TRUE AND "
                    + " ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR "
                    + " (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        } else if (dia.equalsIgnoreCase("DOMINGO")) {
                sql = "select * from rrhh.horario_exporadico he left JOIN funcionario  func on func.id=he.idfuncionario WHERE   func.id=" + idFuncionario + " AND he.domingo=TRUE AND  ((he.fechadesde BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR (he.fechahasta BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') OR  (he.fechadesde<='" + fechaDesdeText + " 00:00:00' AND he.fechahasta>='" + fechaHastaText + " 23:59:59'))";
        }

        try {
            query = em.createNativeQuery(sql, HorarioExporadico.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioExporadico> results = query.getResultList();
            if (i > 0) {
                return results;
            } else {
                return new ArrayList<>();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        } finally {
        }
    }
}
