package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Usuario;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class LicenciasDao {

    @PersistenceContext
    private EntityManager em;

    DependenciaDao dependenciaDao = ResourceLocator.locate(DependenciaDao.class);

    public Licencias guardarLicencias(Licencias usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Licencias> listadeLicencias(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Licencias p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Licencias.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Licencias> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Licencias> listadeLicencias() {
        try {
            String sql = "select u from Licencias u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Licencias> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Licencias p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, Licencias.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Licencias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Licencias getByIdUsuario(Long idusuario) {
        return (Licencias) em.find(Licencias.class, idusuario);
    }

    public List<Licencias> listadeLicenciasByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from Licencias p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.parametro f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, Licencias.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Licencias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrar(Licencias cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Licencias> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from Licencias p JOIN FETCH p.parametro f WHERE f.id=:idForm";
            Query query = em.createQuery(sql, Licencias.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<Licencias> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Licencias listarPorIdFuncionario(Long id) {
        try {
            return (Licencias) em.createQuery("select hf from Licencias hf JOIN FETCH hf.funcionario f WHERE f.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public Licencias getById(Long id) {
        Licencias marcacion = null;
        try {
            marcacion = (Licencias) em.createNativeQuery("SELECT * FROM rrhh.parametro "
                    + " where idreloj=" + id + ";", Licencias.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            marcacion = new Licencias();
        }
        return marcacion;
    }

    public Licencias listarPorIdReloj(Long idReloj) {
        try {
            return (Licencias) em.createQuery("select hf from Licencias hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public List<Licencias> listarPorFechas(Date fechaDesde, Date fechaHasta, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.licencias p LEFT join funcionario f on p.id=f.id WHERE p.fechaini BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59' " + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Licencias.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<Licencias> listarPorFechasUsoAnual(Date fechaDesde, Date fechaHasta, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.licencias p LEFT join rrhh.parametro param on p.idparametro=param.id LEFT join funcionario f on p.id=f.id WHERE param.codigo='estudio_pap' AND p.fechaini BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59' " + filterFuncionario + " AND p.aprobado=1 ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, Licencias.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<Licencias> listadeLicenciasSinConfirmar() {
        try {
            String sql = "select u from Licencias u JOIN FETCH u.funcionario func WHERE u.aprobado=0 ORDER BY u.id DESC";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(sql);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Licencias listarPorId(Long id) {
        try {
            return (Licencias) em.createQuery("select r from Licencias r JOIN FETCH r.funcionario f JOIN FETCH r.parametro form WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("-> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
            //return (Licencias) em.createQuery("select r from Licencias r JOIN FETCH r.funcionario f JOIN FETCH r.parametro form ORDER BY r.id DESC")
            //.setParameter("id", id)
            ///      .getSingleResult();
            return new Licencias();
        } finally {
        }
    }

    public Licencias listarPorIdHere(Long id) {
        try {
            return (Licencias) em.createQuery("select r from Licencias r JOIN FETCH r.funcionario f JOIN FETCH r.parametro form WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return new Licencias();
        } finally {
        }
    }

    private List<Licencias> listadeLicenciaSinFuncionario(String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, String fechaDesde, String fechaHasta, java.util.Map<Long, Funcionario> mapeoFuncionarios, Long idFilial) {
        // String filterFechas = " (p.fechaini>='" + fechaDesde + "' AND p.fechafin<='" + fechaHasta + "') ";
        String filterFechas = " (p.fechaini>='" + fechaDesde + "' AND p.fechaini<='" + fechaHasta + "') ";
        try { //Si teiene nivel jefe, encargado o administrador para que liste todos
            if (usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 
                    || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                    || mapeoFuncionarios.containsKey(usuario.getIdfuncionario().getId())
                    || usuario.getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                String sql = "select * ";
                if (filtroAprobado == null) {
                    String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                    sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro "
                            + "left join funcionario f on f.id= p.id ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + "";
                    sql += " and f.idfilial = :idfilial order by p.id DESC";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro"
                         + " left join funcionario f on f.id= p.id ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND p.aprobado=" + val + " ";
                    sql += " and f.idfilial = :idfilial order by p.id DESC ";
                }
                Query query = em.createNativeQuery(sql, Licencias.class)
                        .setParameter("idfilial", idFilial);
                int i = query.getResultList().size();
                System.out.print(i);
                List<Licencias> results = query.getResultList();
                return results;
            } else {
                Dependencia depen = dependenciaDao.getDependenciaById(usuario.getIdfuncionario().getDependencia().getId());
                String descripcionArea = depen.getDescripcion().toUpperCase();
                if (depen.getIddependenciapadre() != 7) {
                    descripcionArea = dependenciaDao.getDependenciaByNivel(depen.getNivel()).getDescripcion();
                }
//                while (depen.getIddependenciapadre() != 7) {
//                    for (Dependencia depenc : dependenciaDao.listarSubDependencia(depen.getIddependenciapadre())) {
//                        if (depenc.getIddependenciapadre() == 7) {
//                            depen = depenc;
//                            descripcionArea = depenc.getDescripcion().toUpperCase();
//                            break;
//                        }
//                    }
//                }
                //Si no tiene nivel jefe, encargado o administrador para que liste solo sus licenciases
                String sql = "select * ";
                if (filtroAprobado == null) {

                    String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                    String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro left join funcionario func on p.id=func.id ";
//                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) order by p.id DESC ";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                    String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro left join funcionario func on p.id=func.id ";
//                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND p.aprobado=" + val + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND p.aprobado=" + val + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) order by p.id DESC ";
                }
                Query query = em.createNativeQuery(sql, Licencias.class);
                int i = query.getResultList().size();
                System.out.print(i);
                List<Licencias> results = query.getResultList();
                return results;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Licencias> listadeLicenciasByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, Date fechaDesde, Date fechaHasta, java.util.Map<Long, Funcionario> mapeoFuncionarios, Long idFilial) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        //String filterFechas = " (p.fechaini>='" + fechaDesdeText + "' AND p.fechafin<='" + fechaHastaText + "') ";
        String filterFechas = " (p.fechaini>='" + fechaDesdeText + " 00:00:00' AND p.fechaini<='" + fechaHastaText + " 23:59:59') ";
        String parametroFilial = "";
        if(idFilial != 0)
            parametroFilial = "and func.idfilial = " + idFilial;
        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            return listadeLicenciaSinFuncionario(filtroFuncionario, filtroAprobado, filtroFormulario, usuario, fechaDesdeText + " 00:00:00", fechaHastaText + " 23:59:59", mapeoFuncionarios, idFilial);
        } else {
            try {
                //Si teiene nivel jefe, encargado o administrador para que liste todos
                if (usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 
                        || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                        || mapeoFuncionarios.containsKey(usuario.getIdfuncionario().getId())
                        || usuario.getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                    String sql = "select * ";
                    if (filtroAprobado == null) {
                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                        String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                        sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro"
                            + " left join funcionario func on p.idfuncionario=func.id "
                            + " where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
                        sql += " "+parametroFilial+" and upper(form.descripcion) LIKE '" + parametro + "' AND " + filterFechas + " order by p.id DESC ";
                    } else if (filtroAprobado != null) {
                        long val = 0L;
                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                            val = 1L;
                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                            val = 2L;
                        }

                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                        String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro";
                        sql += " left join funcionario func on p.idfuncionario=func.id "
                            + " where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
                        sql += " "+parametroFilial+" and upper(form.descripcion) LIKE '" + parametro + "' and " + filterFechas + " AND p.aprobado=" + val + " order by p.id DESC ";
                    }
                    Query query = em.createNativeQuery(sql, Licencias.class);
                    int i = query.getResultList().size();
                    System.out.print(i);
                    List<Licencias> results = query.getResultList();
                    return results;
                } else {
                    Dependencia depen = dependenciaDao.getDependenciaById(usuario.getIdfuncionario().getDependencia().getId());
                    String descripcionArea = depen.getDescripcion().toUpperCase();
                    if (depen.getIddependenciapadre() != 7) {
                        descripcionArea = dependenciaDao.getDependenciaByNivel(depen.getNivel()).getDescripcion();
                    }
                    //Si no tiene nivel jefe, encargado o administrador para que liste solo sus licenciases
                    String sql = "select * ";
                    if (filtroAprobado == null) {

                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                        String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro";
                        sql += " left join funcionario func on p.idfuncionario=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + parametro + "' AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + parametro + "' order by p.id DESC ";
                    } else if (filtroAprobado != null) {
                        long val = 0L;
                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                            val = 1L;
                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                            val = 2L;
                        }

                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                        String parametro = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.licencias p left JOIN rrhh.parametro form on form.id=p.idparametro";
                        sql += " left join funcionario func on p.id=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + parametro + "' and p.aprobado=" + val + " AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + parametro + "' and p.aprobado=" + val + " order by p.id DESC ";
                    }
                    Query query = em.createNativeQuery(sql, Licencias.class);
                    int i = query.getResultList().size();
                    System.out.print(i);
                    List<Licencias> results = query.getResultList();
                    return results;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public List<Licencias> listarPorFechaUsuario(Date targetDay, Long id) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(targetDay);
        String fechaHastaText = formatter.format(targetDay);
        if (id != null) {
            filterFuncionario = "AND lic.idfuncionario=" + id;
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.licencias lic left join funcionario f on f.id = lic.idfuncionario "
                    + " WHERE (date(lic.fechaini) BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59') " + filterFuncionario + " AND lic.aprobado=1 ORDER BY lic.id DESC";
            Query query = em.createNativeQuery(sql, Licencias.class);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
