package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SolicitudProduccionDetalleDao {

    @PersistenceContext
    private EntityManager em;

    public SolicitudProduccionDetalle guardarSolicitudProduccionDetalle(SolicitudProduccionDetalle usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<SolicitudProduccionDetalle> listadeSolicitudProduccionDetalle(int firstRow, int pageSize, String filter) {

        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from SolicitudProduccionDetalle p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudProduccionDetalle.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudProduccionDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<SolicitudProduccionDetalle> listadeSolicitudProduccionDetalle() {
        try {
            String sql = "select u from SolicitudProduccionDetalle u ";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<SolicitudProduccionDetalle> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from SolicitudProduccionDetalle p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE";
            Query query = em.createQuery(sql, SolicitudProduccionDetalle.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudProduccionDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public SolicitudProduccionDetalle getByIdUsuario(Long idusuario) {
        return (SolicitudProduccionDetalle) em.find(SolicitudProduccionDetalle.class, idusuario);
    }

    public List<SolicitudProduccionDetalle> listadeSolicitudProduccionDetalleByFormulario(int init, int finish, String filter) {
        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from SolicitudProduccionDetalle p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id";
            }
            Query query = em.createQuery(sql, SolicitudProduccionDetalle.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<SolicitudProduccionDetalle> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public void borrar(SolicitudProduccionDetalle cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public List<SolicitudProduccionDetalle> listarPorIdSolicitud(Long id) {
        try {
            return em.createQuery("select sd from SolicitudProduccionDetalle sd join fetch sd.solicitud s where s.id = :id ORDER BY sd.id")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            e.getLocalizedMessage();
            return null;
        }
    }

    public SolicitudProduccionDetalle listarPorIdFecha(long id, Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fecha);
        try {
            String sql = "SELECT * "
                    + " FROM rrhh.solicitud_produccion_detalle spd left join rrhh.solicitud s on s.id=spd.idsolicitud"
                    + " WHERE spd.id=" + id + " AND s.aprobado=1 AND spd.fecha>='" + fechaDesdeText + " 00:00:00' AND spd.fecha<='" + fechaDesdeText + " 23:59:59'";
            return (SolicitudProduccionDetalle) em.createNativeQuery(sql, SolicitudProduccionDetalle.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return null;
        } finally {
        }
    }

    public List<SolicitudProduccionDetalle> listarPorFechasSPD(Date fechaDesde, Date fechaHasta, Long id, List<Dependencia> cargos) {
        String filterFuncionario = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        if (id != null) {
            filterFuncionario = "AND f.id=" + id;
        }
        String filterCargo = "";
        if (cargos.size() > 0) {
            filterCargo += " AND (";
        }
        int num = 0;
        for (Dependencia cargo : cargos) {
            if (num == 0) {
                filterCargo += " lower(area) = '" + cargo.getDescripcion().toLowerCase() + "'";
            } else {
                filterCargo += " OR lower(area) = '" + cargo.getDescripcion().toLowerCase() + "'";
            }
            num++;
        }
        if (cargos.size() > 0) {
            filterCargo += " )";
        }
        try {
            String sql = "select * ";
            sql += "from rrhh.solicitud_produccion_detalle p LEFT join funcionario f on p.id=f.id WHERE p.fecha BETWEEN '" + fechaDesdeText + " 00:00:00' AND '" + fechaHastaText + " 23:59:59' " + filterCargo + filterFuncionario + " ORDER BY p.id DESC";
            Query query = em.createNativeQuery(sql, SolicitudProduccionDetalle.class);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return new ArrayList<>();
    }
}
