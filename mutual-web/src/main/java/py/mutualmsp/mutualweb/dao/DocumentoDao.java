/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Documento;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class DocumentoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Documento> listaDocumento() {
        List<Documento> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Documento.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Documento documento){
        try {
            em.merge(documento);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Documento documento) {
        try {
            em.remove(em.contains(documento) ? documento : em.merge(documento));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Documento> getListDocumento(String valor) {
        List<Documento> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.documento " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Documento.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select d from Documento d")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Documento getNinguno() {
        Documento ninguno = null;
        try {
            ninguno = (Documento) em.createNativeQuery("SELECT * FROM public.documento " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Documento.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Documento) em.createQuery("select d from Documento d")
                .getResultList();
        }
        return ninguno;
    }
}
