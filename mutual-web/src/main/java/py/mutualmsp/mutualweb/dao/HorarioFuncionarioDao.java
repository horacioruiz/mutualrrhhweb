package py.mutualmsp.mutualweb.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;

@Stateless
public class HorarioFuncionarioDao {
    @PersistenceContext
    private EntityManager em;

    public HorarioFuncionario guardarHorarioFuncionario(HorarioFuncionario usuarios) {
        if (usuarios.getIdreloj() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionario(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param AND p.estado=TRUE order by p.id";
            }
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionario() {
        try {
            String sql = "select u from HorarioFuncionario u AND u.estado=TRUE";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listadeHorarioFuncionarioTrue() {
        try {
            String sql = "select u from HorarioFuncionario u JOIN FETCH u.funcionario f WHERE f.idestado=13 AND u.estado=TRUE";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public List<HorarioFuncionario> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE AND p.estado=TRUE";
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioFuncionario getByIdUsuario(Long idusuario) {
        return (HorarioFuncionario) em.find(HorarioFuncionario.class, idusuario);
    }

    public List<HorarioFuncionario> listadeHorarioFuncionarioByFormulario(int init, int finish, String filter) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param AND p.estado=TRUE order by p.id";
            }
            Query query = em.createQuery(sql, HorarioFuncionario.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public void borrar(HorarioFuncionario cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public List<HorarioFuncionario> listarPorFormulario(Formulario value) {
        try {
            String sql = "select p ";
            sql += "from HorarioFuncionario p JOIN FETCH p.formulario f WHERE f.id=:idForm AND p.estado=TRUE";
            Query query = em.createQuery(sql, HorarioFuncionario.class).setParameter("idForm", value.getId());
            int i = query.getResultList().size();
            System.out.print(i);
            List<HorarioFuncionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return null;
    }

    public HorarioFuncionario listarPorIdFuncionario(Long id) {
        try {
            return (HorarioFuncionario) em.createQuery("select hf from HorarioFuncionario hf JOIN FETCH hf.funcionario f WHERE f.id=:id "
                    + "AND hf.estado=TRUE")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }

    public HorarioFuncionario getById(Long id) {
        HorarioFuncionario marcacion = null;
        try {
            String sql = "SELECT * FROM rrhh.horario_funcionario "
                    + " where idreloj=" + id + " AND estado=TRUE;";
            System.out.println("SQL ONE -> " + sql);
            marcacion = (HorarioFuncionario) em.createNativeQuery(sql, HorarioFuncionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            marcacion = new HorarioFuncionario();
        } finally {
        }
        return marcacion;
    }
    
    public HorarioFuncionario getByCedula(String cedula) {
        HorarioFuncionario marcacion = null;
        try {
            String sql = "SELECT hf.* FROM rrhh.horario_funcionario hf \n" +
            " left join funcionario f \n" +
            " on f.id = hf.idfuncionario \n" +
            " where f.cedula = :cedula and f.idestado = 13 AND hf.estado=TRUE;";
            System.out.println("SQL ONE -> " + sql);
            marcacion = (HorarioFuncionario) em.createNativeQuery(sql, HorarioFuncionario.class)
                    .setParameter("cedula", cedula)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            marcacion = new HorarioFuncionario();
        } finally {
        }
        return marcacion;
    }

    public HorarioFuncionario listarPorIdReloj(Long idReloj) {
        try {
            return (HorarioFuncionario) em.createQuery("select hf from HorarioFuncionario hf JOIN FETCH hf.funcionario f WHERE hf.idreloj=:id AND "
                    + "AND hf.estado=TRUE")
                    .setParameter("id", idReloj)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
        }
    }
}
