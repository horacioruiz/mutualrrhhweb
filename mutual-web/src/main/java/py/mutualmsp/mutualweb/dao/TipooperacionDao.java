/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Tipooperacion;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class TipooperacionDao {
    @PersistenceContext
    private EntityManager em;
    
    public List<Tipooperacion> listaTipooperacion() {
        List<Tipooperacion> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Tipooperacion.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Tipooperacion tipooperacion) {
        try {
            em.merge(tipooperacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void borrar(Tipooperacion tipooperacion) {
        try {
            em.remove(em.contains(tipooperacion) ? tipooperacion : em.merge(tipooperacion));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Tipooperacion> getListTipooperacion(String valor) {
        List<Tipooperacion> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")){
                lista = em.createNativeQuery("SELECT * FROM public.tipooperacion " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Tipooperacion.class)
                   .getResultList();
            } else {
                lista = em.createQuery("select t from Tipooperacion t")
                   .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public Tipooperacion getNinguno() {
        Tipooperacion ninguno = null;
        try {
            ninguno = (Tipooperacion) em.createNativeQuery("SELECT * FROM public.tipooperacion " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Tipooperacion.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Tipooperacion) em.createQuery("select t from Tipooperacion t")
                .getResultList();
        }
        return ninguno;
    }
    
    
}
