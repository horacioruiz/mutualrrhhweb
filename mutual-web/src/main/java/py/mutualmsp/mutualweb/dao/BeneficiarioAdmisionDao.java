/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.BeneficiarioAdmision;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class BeneficiarioAdmisionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<BeneficiarioAdmision> listaBeneficiarioAdmision() {
        List<BeneficiarioAdmision> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("BeneficiarioAdmision.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(BeneficiarioAdmision beneficiarioAdmision){
        try {
            em.merge(beneficiarioAdmision);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(BeneficiarioAdmision beneficiarioAdmision) {
        try {
            em.remove(em.contains(beneficiarioAdmision) ? beneficiarioAdmision : em.merge(beneficiarioAdmision));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<BeneficiarioAdmision> getListBeneficiarioAdmision(Long idsolicitud) {
        List<BeneficiarioAdmision> lista = new ArrayList<>();
       try {
           lista = em.createNativeQuery("SELECT * FROM public.beneficiario_admision " +
"                   where  idsolicitud_admision="+idsolicitud+" order by id;", BeneficiarioAdmision.class)
                   .getResultList();
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
