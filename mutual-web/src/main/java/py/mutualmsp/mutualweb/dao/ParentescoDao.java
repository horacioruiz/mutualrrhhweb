/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Parentesco;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class ParentescoDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Parentesco> listaParentesco() {
        List<Parentesco> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Parentesco.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Parentesco parentesco){
        try {
            em.merge(parentesco);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Parentesco parentesco) {
        try {
            em.remove(em.contains(parentesco) ? parentesco : em.merge(parentesco));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Parentesco> getListParentesco(String valor) {
        List<Parentesco> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.parentesco " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Parentesco.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select p from Parentesco p")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Parentesco getNinguno() {
        Parentesco ninguno = null;
        try {
            ninguno = (Parentesco) em.createNativeQuery("SELECT * FROM public.parentesco " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Parentesco.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Parentesco) em.createQuery("select p from Parentesco p")
                .getResultList();
        }
        return ninguno;
    }
}
