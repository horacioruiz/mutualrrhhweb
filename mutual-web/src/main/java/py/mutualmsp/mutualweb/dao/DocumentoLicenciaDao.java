/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.DocumentoLicencia;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class DocumentoLicenciaDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<DocumentoLicencia> listaDocumentoLicencia() {
        List<DocumentoLicencia> lista = new ArrayList<>();

        try {
            lista = em.createNamedQuery("DocumentoLicencia.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void guardar(DocumentoLicencia barrio) {
        try {
            em.merge(barrio);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void borrar(DocumentoLicencia barrio) {
        try {
            em.remove(em.contains(barrio) ? barrio : em.merge(barrio));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<DocumentoLicencia> getListDocumentoLicencia(String valor) {
        List<DocumentoLicencia> lista = new ArrayList<>();
        try {
            if (valor != null && !valor.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM public.documento_licencia "
                        + "                   where id=" + valor + ";", DocumentoLicencia.class)
                        .getResultList();
            } else {
                lista = em.createQuery("select b from DocumentoLicencia b")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<DocumentoLicencia> getListDocumentoLicenciaByLicencia(Long id) {
        List<DocumentoLicencia> lista = new ArrayList<>();
        try {
//            if (valor != null && !valor.equals("")) {
            String sql = "SELECT * FROM rrhh.documento_licencia "
                    + "                   where idlicencia=" + id + ";";
            System.out.println("-> " + sql);
            lista = em.createNativeQuery(sql, DocumentoLicencia.class)
                    .getResultList();
//            } else {
//                lista = em.createQuery("select b from DocumentoLicencia b")
//                        .getResultList();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

}
