package py.mutualmsp.mutualweb.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Solicitud;
import py.mutualmsp.mutualweb.entities.Usuario;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 * Created by Alfre on 8/6/2016.
 */
@Stateless
public class SolicitudDao {

    DependenciaDao dependenciaDao = ResourceLocator.locate(DependenciaDao.class);

    @PersistenceContext
    private EntityManager em;

    public Solicitud guardarSolicitud(Solicitud usuarios) {
        if (usuarios.getId() == null) {
            em.persist(usuarios);
        } else {
            em.merge(usuarios);
        }
        em.flush();
        return usuarios;
    }

    public List<Solicitud> listadeSolicitud(int firstRow, int pageSize, String filter) {
        try {
            String sql = "select p ";
            sql += "from Solicitud p ";
            if (filter != null && !filter.equals("")) {
                sql += "where upper(p.descripcion) like :param order by p.id DESC";
            }
            Query query = em.createQuery(sql, Solicitud.class);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase() + "%");
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Solicitud> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Solicitud> listadeSolicitud() {
        try {
            String sql = "select u from Solicitud u ORDER BY u.id DESC";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Solicitud> listadeSolicitudSinConfirmar() {
        try {
            String sql = "select u from Solicitud u WHERE u.aprobado=0 ORDER BY u.id DESC";
            Query query = em.createQuery(sql);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Solicitud> listadeSolicitudSinConfirmarPorFilial(Long idFilial) {
        try {
            String sql = "select s.* from rrhh.solicitud s  \n" +
            "left join funcionario f \n" +
            "on f.id = s.id \n" +
            "WHERE s.aprobado=0 and f.idfilial = :idfilial \n" +
            "ORDER BY s.id DESC";
            Query query = em.createNativeQuery(sql, Solicitud.class);
            int i = query.getResultList().size();
            System.out.print(i);
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<Solicitud> listarAdministradores() {
        try {
            String sql = "select p ";
            sql += "from Solicitud p JOIN FETCH p.funcionario f WHERE p.administrador=TRUE ORDER BY p.id DESC";
            Query query = em.createQuery(sql, Solicitud.class);
            int i = query.getResultList().size();
            System.out.print(i);
            List<Solicitud> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Solicitud getByIdUsuario(Long idusuario) {
        return (Solicitud) em.find(Solicitud.class, idusuario);
    }

    public List<Solicitud> listadeSolicitudByFormulario(int init, int finish, String filter) {
        try {
//            String sql = "select p.idusuario, ";
            String sql = "select p ";
//            sql += "p.funcionario.nombre, ";
//            sql += "p.username, ";
//            sql += "p.enabled, ";
//            sql += "p.administrador, ";
//            sql += "p.clientes, ";
//            sql += "p.supervisor ";
            sql += "from Solicitud p";
            if (filter != null && !filter.equals("")) {
                sql += " JOIN FETCH p.formulario f where upper(f.descripcion) like :param order by p.id DESC";
            }
            Query query = em.createQuery(sql, Solicitud.class);
            //.setFirstResult(firstRow)
            // .setMaxResults(pageSize);
            if (filter != null && !filter.equals("")) {
                query.setParameter("param", filter.toUpperCase());
            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Solicitud> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Solicitud> listadeSolicitudByFuncionario(int init, int finish, String filtroFuncionario, String filtroConfirmado, String filtroAprobado) {
        try {
            String sql = "select p ";
            if (filtroConfirmado == null && filtroAprobado == null) {
                sql += "from Solicitud p";
                sql += " JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) order by p.id DESC";
            } else if (filtroConfirmado != null && filtroAprobado != null) {
                sql += "from Solicitud p";
                sql += " JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) and p.aprobado=:aprobado order by p.id DESC";
            } else if (filtroConfirmado != null) {
                sql += "from Solicitud p";
                sql += " JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) order by p.id DESC";
            } else if (filtroAprobado != null) {
                sql += "from Solicitud p";
                sql += " JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) and p.aprobado=:aprobado order by p.id DESC";
            }
            Query query = em.createQuery(sql, Solicitud.class);
            query.setParameter("param", filtroFuncionario == null ? "%%" : filtroFuncionario.toUpperCase() + "%");
            if (filtroAprobado != null) {
                if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                    query.setParameter("aprobado", 1L);
                } else {
                    query.setParameter("aprobado", 2L);
                }

            }
            int i = query.getResultList().size();
            System.out.print(i);
            List<Solicitud> results = query.getResultList();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    public List<Solicitud> listadeSolicitudByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario) {
//        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//            return listadeSolicitudSinFuncionario(filtroFuncionario, filtroAprobado, filtroFormulario, usuario);
//        } else {
//            try {
//                //Si teiene nivel jefe, encargado o administrador para que liste todos
//                if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10 || usuario.getIdfuncionario().getDependencia().getDescripcion().equalsIgnoreCase("DEPARTAMENTO DE RECURSOS HUMANOS")) {
//                    String sql = "select * ";
//                    if (filtroAprobado == null) {
//                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
//                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
//                        sql += "from Solicitud p left JOIN formulario form on form.id=p.idformulario";
//                        sql += " left join funcionario func on p.id=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND upper(form.descripcion) LIKE '" + formulario + "' order by p.id DESC ";
//                    } else if (filtroAprobado != null) {
//                        long val = 0L;
//                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                            val = 1L;
//                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
//                            val = 2L;
//                        }
//
//                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
//                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
//
//                        sql += "from Solicitud p left JOIN formulario form on form.id=p.idformulario";
//                        sql += " left join funcionario func on p.id=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND upper(form.descripcion) LIKE '" + formulario + "' and p.aprobado=" + val + " order by p.id DESC ";
//                    }
//                    Query query = em.createNativeQuery(sql, Solicitud.class);
//                    int i = query.getResultList().size();
//                    System.out.print(i);
//                    List<Solicitud> results = query.getResultList();
//                    return results;
//                } else {
//                    Dependencia depen = dependenciaDao.getDependenciaById(usuario.getIdfuncionario().getDependencia().getIddependencia());
//                    String descripcionArea = depen.getDescripcion().toUpperCase();
//                    if (depen.getIddependenciapadre() != 7) {
//                        descripcionArea = dependenciaDao.getDependenciaByNivel(depen.getNivel()).getDescripcion();
//                    }
//                    //Si no tiene nivel jefe, encargado o administrador para que liste solo sus solicitudes
//                    String sql = "select * ";
//                    if (filtroAprobado == null) {
//
//                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
//                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
//
//                        sql += "from Solicitud p left JOIN formulario form on form.id=p.idformulario";
//                        sql += " left join funcionario func on p.id=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND upper(form.descripcion) LIKE '" + formulario + "' AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
//                    } else if (filtroAprobado != null) {
//                        long val = 0L;
//                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                            val = 1L;
//                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
//                            val = 2L;
//                        }
//
//                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
//                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
//
//                        sql += "from Solicitud p left JOIN formulario form on form.id=p.idformulario";
//                        sql += " left join funcionario func on p.id=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND upper(form.descripcion) LIKE '" + formulario + "' and p.aprobado=" + val + " AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
//                    }
//                    Query query = em.createNativeQuery(sql, Solicitud.class);
//                    int i = query.getResultList().size();
//                    System.out.print(i);
//                    List<Solicitud> results = query.getResultList();
//                    return results;
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }
    public List<Solicitud> listadeSolicitudByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, Date fechaDesde, Date fechaHasta, Map<Long, Funcionario> mapeoFuncionarios, Long idFilial) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fechaDesde);
        String fechaHastaText = formatter.format(fechaHasta);
        String filterFechas = " (p.fechaini>='" + fechaDesdeText + " 00:00:00' AND p.fechafin<='" + fechaHastaText + " 23:59:59') ";
        String parametroFilial = "";
        if (idFilial!=0) 
            parametroFilial += "and func.idfilial = " + idFilial;
        
        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            return listadeSolicitudSinFuncionario(filtroFuncionario, filtroAprobado, filtroFormulario, usuario, fechaDesdeText + " 00:00:00", fechaHastaText + " 23:59:59", mapeoFuncionarios, idFilial);
        } else {
            try {
                //Si teiene nivel jefe, encargado o administrador para que liste todos
                if (usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                        || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 
                        || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                        || mapeoFuncionarios.containsKey(usuario.getIdfuncionario().getId())
                        || usuario.getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                    String sql = "select * ";
                    if (filtroAprobado == null) {
                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                        sql += " from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario ";
                        sql += " left join funcionario func on p.idfuncionario = func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + formulario + "' "+parametroFilial+" order by p.id DESC ";
                    } else if (filtroAprobado != null) {
                        long val = 0L;
                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                            val = 1L;
                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                            val = 2L;
                        }

                        String funcionario = filtroFuncionario == null ? "%%" : "%" + filtroFuncionario.toUpperCase() + "%";
                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario";
                        sql += " left join funcionario func on p.idfuncionario = func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
                        sql += " AND upper(form.descripcion) LIKE '" + formulario + "' AND " + filterFechas + " and p.aprobado=" + val + " "+parametroFilial+" order by p.id DESC ";
                    }
                    Query query = em.createNativeQuery(sql, Solicitud.class);
                    int i = query.getResultList().size();
                    System.out.print(i);
                    List<Solicitud> results = query.getResultList();
                    return results;
                } else {
                    Dependencia depen = dependenciaDao.getDependenciaById(usuario.getIdfuncionario().getDependencia().getId());
                    String descripcionArea = depen.getDescripcion().toUpperCase();
                    if (depen.getIddependenciapadre() != 7) {
                        descripcionArea = dependenciaDao.getDependenciaByNivel(depen.getNivel()).getDescripcion();
                    }
                    //Si no tiene nivel jefe, encargado o administrador para que liste solo sus solicitudes
                    String sql = "select * ";
                    if (filtroAprobado == null) {

                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario";
                        sql += " left join funcionario func on p.idfuncionario = func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + formulario + "' AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + formulario + "' order by p.id DESC ";
                    } else if (filtroAprobado != null) {
                        long val = 0L;
                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                            val = 1L;
                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                            val = 2L;
                        }

                        String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                        String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                        sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario";
                        sql += " left join funcionario func on p.idfuncionario=func.id where ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) ";
//                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + formulario + "' and p.aprobado=" + val + " AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                        sql += " AND " + filterFechas + " AND upper(form.descripcion) LIKE '" + formulario + "' and p.aprobado=" + val + " order by p.id DESC ";
                    }
                    Query query = em.createNativeQuery(sql, Solicitud.class);
                    int i = query.getResultList().size();
                    System.out.print(i);
                    List<Solicitud> results = query.getResultList();
                    return results;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
//    public List<Solicitud> listadeSolicitudByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario) {
//        if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//            return listadeSolicitudSinFuncionario(filtroAprobado, filtroFormulario, usuario);
//        } else {
//            try {
//                //Si teiene nivel jefe, encargado o administrador para que liste todos
//                if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10) {
//                    String sql = "select p ";
//                    if (filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where ((concat(f.nombre, ' ', f.apellido) like :param) OR f is null) AND upper(formu.descripcion) LIKE :formulario order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where ((concat(f.nombre, ' ', f.apellido) like :param) OR f is null) AND upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                    Query query = em.createQuery(sql, Solicitud.class);
//                    query.setParameter("param", filtroFuncionario == null ? "%%" : filtroFuncionario.toUpperCase() + "%");
//                    query.setParameter("formulario", filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%");
//                    if (filtroAprobado != null) {
//                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                            query.setParameter("aprobado", 1L);
//                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
//                            query.setParameter("aprobado", 2L);
//                        } else {
//                            query.setParameter("aprobado", 0L);
//                        }
//                    }
//                    int i = query.getResultList().size();
//                    System.out.print(i);
//                    List<Solicitud> results = query.getResultList();
//                    return results;
//                } else {
//                    //Si no teiene nivel jefe, encargado o administrador para que liste solo sus solicitudes
//                    String sql = "select p ";
//                    if (filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where ((concat(f.nombre, ' ', f.apellido) like :param) OR f is null) AND upper(formu.descripcion) LIKE :formulario order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where ((concat(f.nombre, ' ', f.apellido) like :param) OR f is null) AND upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                    Query query = em.createQuery(sql, Solicitud.class);
//                    query.setParameter("param", filtroFuncionario == null ? "%%" : filtroFuncionario.toUpperCase() + "%");
//                    query.setParameter("formulario", filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%");
//                    if (filtroAprobado != null) {
//                        if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                            query.setParameter("aprobado", 1L);
//                        } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
//                            query.setParameter("aprobado", 2L);
//                        } else {
//                            query.setParameter("aprobado", 0L);
//                        }
//
//                    }
//                    int i = query.getResultList().size();
//                    System.out.print(i);
//                    List<Solicitud> results = query.getResultList();
//                    return results;
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }
//    public List<Solicitud> listadeSolicitudByFuncionarioAndUsuario(int init, int finish, String filtroFuncionario, String filtroConfirmado, String filtroAprobado, String filtroFormulario, Usuario usuario) {
//        try {
//            //Si teiene nivel jefe, encargado o administrador para que liste todos
//            if (usuario.getIdnivelusuario() == 8 || usuario.getIdnivelusuario() == 9 || usuario.getIdnivelusuario() == 10) {
//                String sql = "select p ";
//                if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//                    if (filtroConfirmado == null && filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where upper(formu.descripcion) LIKE :formulario order by p.id DESC";
//                    } else if (filtroConfirmado != null && filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado and p.aprobado=:aprobado order by p.id DESC";
//                    } else if (filtroConfirmado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                } else {
//                    if (filtroConfirmado == null && filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where upper(formu.descripcion) LIKE :formulario AND (concat(f.nombre, ' ', f.apellido) like :param) order by p.id DESC";
//                    } else if (filtroConfirmado != null && filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado and p.aprobado=:aprobado order by p.id DESC";
//                    } else if (filtroConfirmado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu JOIN FETCH p.funcionario f where (concat(f.nombre, ' ', f.apellido) like :param) AND upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                }
//
//                Query query = em.createQuery(sql, Solicitud.class);
//                if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//                } else {
//                    query.setParameter("param", filtroFuncionario == null ? "%%" : filtroFuncionario.toUpperCase() + "%");
//                }
//                query.setParameter("formulario", filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%");
//                if (filtroConfirmado != null) {
//                    if (filtroConfirmado.equalsIgnoreCase("CONFIRMADO")) {
//                        query.setParameter("confirmado", true);
//                    } else {
//                        query.setParameter("confirmado", false);
//                    }
//                }
//                if (filtroAprobado != null) {
//                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                        query.setParameter("aprobado", true);
//                    } else {
//                        query.setParameter("aprobado", false);
//                    }
//
//                }
//                int i = query.getResultList().size();
//                System.out.print(i);
//                List<Solicitud> results = query.getResultList();
//                return results;
//            } else {
//                //Si no teiene nivel jefe, encargado o administrador para que liste solo sus solicitudes
//                String sql = "select p ";
//                if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//                    if (filtroConfirmado == null && filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario order by p.id DESC";
//                    } else if (filtroConfirmado != null && filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado and p.aprobado=:aprobado order by p.id DESC";
//                    } else if (filtroConfirmado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                } else {
//                    if (filtroConfirmado == null && filtroAprobado == null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.funcionario f JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario order by p.id DESC";
//                    } else if (filtroConfirmado != null && filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.funcionario f JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado and p.aprobado=:aprobado order by p.id DESC";
//                    } else if (filtroConfirmado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.funcionario f JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.confirmado=:confirmado order by p.id DESC";
//                    } else if (filtroAprobado != null) {
//                        sql += "from Solicitud p";
//                        sql += " JOIN FETCH p.funcionario f JOIN FETCH p.formulario formu where f.id=:idFunc AND upper(formu.descripcion) LIKE :formulario and p.aprobado=:aprobado order by p.id DESC";
//                    }
//                }
//                Query query = em.createQuery(sql, Solicitud.class);
//                if (filtroFormulario != null && filtroFormulario.equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
//                } else {
//                    query.setParameter("idFunc", usuario.getIdfuncionario().getIdfuncionario());
//                }
//                query.setParameter("formulario", filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%");
//                if (filtroConfirmado != null) {
//                    if (filtroConfirmado.equalsIgnoreCase("CONFIRMADO")) {
//                        query.setParameter("confirmado", true);
//                    } else {
//                        query.setParameter("confirmado", false);
//                    }
//                }
//                if (filtroAprobado != null) {
//                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
//                        query.setParameter("aprobado", true);
//                    } else {
//                        query.setParameter("aprobado", false);
//                    }
//
//                }
//                int i = query.getResultList().size();
//                System.out.print(i);
//                List<Solicitud> results = query.getResultList();
//                return results;
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public void borrar(Solicitud cargo) {
        try {
            em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Solicitud listarPorIdSecond(Long id) {
        try {
            String sql = "SELECT * FROM rrhh.solicitud soli left join rrhh.formulario form on soli.idformulario=form.id "
                    + " left join public.funcionario fun on soli.id=fun.id "
                    //                    + " WHERE soli.id=" + id + " AND soli.aprobado=1 AND soli.fechaini>='" + fechaDesdeText + " 00:00:00' and soli.fechafin<='" + fechaDesdeText + " 23:59:59' and UPPER(form.descripcion)='SOLICITUD DE PRODUCCIÓN' ";
                    + " WHERE soli.id=" + id + " ";
//            System.out.println("SQL -> " + sql);
            return (Solicitud) em.createNativeQuery(sql, Solicitud.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
        }

    }

    public Solicitud listarPorId(Long id) {
        try {
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.funcionario f JOIN FETCH r.formulario form WHERE r.id = :data ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.funcionario f JOIN FETCH r.formulario form ORDER BY r.id DESC")
                    //                    .setParameter("id", id)
                    .getSingleResult();
        } finally {
        }
    }

    public Solicitud listarPorIdFuncionario(Long id) {
        try {
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.formulario form WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.formulario form ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } finally {
        }
    }

    public Solicitud listarPorIdAndFormulario(Long id) {
        try {
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.formulario form WHERE r.id=:id ORDER BY r.id DESC")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("-->> " + e.fillInStackTrace());
            System.out.println("-->> " + e.getLocalizedMessage());
            return (Solicitud) em.createQuery("select r from Solicitud r JOIN FETCH r.formulario form ORDER BY r.id DESC")
                    .getSingleResult();
        } finally {
        }
    }

    private List<Solicitud> listadeSolicitudSinFuncionario(String filtroFuncionario, String filtroAprobado, String filtroFormulario, Usuario usuario, String fechaDesde, String fechaHasta, Map<Long, Funcionario> mapeoFuncionarios, Long idFilial) {
        String filterFechas = " (p.fechaini>='" + fechaDesde + "' AND p.fechafin<='" + fechaHasta + "') ";
        try { //Si teiene nivel jefe, encargado o administrador para que liste todos
            if (usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 
                    || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 
                    || usuario.getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("COORDINADOR") != -1 
                    || mapeoFuncionarios.containsKey(usuario.getIdfuncionario().getId())
                    || usuario.getIdfuncionario().getDependencia().getDescripcion().contains("INFORMATICA")) {
                String sql = "select * ";
                if (filtroAprobado == null) {
                    String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";
                    sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario "
                        +  " left join funcionario f on f.id =  p.id";
                    sql += " WHERE f.idfilial = :idfilial and upper(form.descripcion) LIKE '" + formulario + "' AND " + filterFechas + " order by p.id DESC ";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario "
                    +  " left join funcionario f on f.id =  p.id";
                    sql += " WHERE f.idfilial = :idfilial and upper(form.descripcion) LIKE '" + formulario + "' and " + filterFechas + " AND p.aprobado=" + val + " order by p.id DESC ";
                }
                Query query = em.createNativeQuery(sql, Solicitud.class)
                        .setParameter("idfilial", idFilial);
                int i = query.getResultList().size();
                System.out.print(i);
                List<Solicitud> results = query.getResultList();
                return results;
            } else {
                Dependencia depen = dependenciaDao.getDependenciaById(usuario.getIdfuncionario().getDependencia().getId());
                String descripcionArea = depen.getDescripcion().toUpperCase();
                if (depen.getIddependenciapadre() != 7) {
                    descripcionArea = dependenciaDao.getDependenciaByNivel(depen.getNivel()).getDescripcion();
                }
                //Si no tiene nivel jefe, encargado o administrador para que liste solo sus solicitudes
                String sql = "select * ";
                if (filtroAprobado == null) {

                    String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                    String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario left join funcionario func on p.id=func.id ";
//                    sql += " WHERE upper(form.descripcion) LIKE '" + formulario + "' AND " + filterFechas + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + formulario + "' AND " + filterFechas + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) order by p.id DESC ";
                } else if (filtroAprobado != null) {
                    long val = 0L;
                    if (filtroAprobado.equalsIgnoreCase("APROBADO")) {
                        val = 1L;
                    } else if (filtroAprobado.equalsIgnoreCase("RECHAZADO")) {
                        val = 2L;
                    }

                    String funcionario = usuario.getIdfuncionario() == null ? "" : usuario.getIdfuncionario().getNombreCompleto().toUpperCase();
                    String formulario = filtroFormulario == null ? "%%" : "%" + filtroFormulario.toUpperCase() + "%";

                    sql += "from rrhh.solicitud p left JOIN rrhh.formulario form on form.id=p.idformulario left join funcionario func on p.id=func.id ";
//                    sql += " WHERE upper(form.descripcion) LIKE '" + formulario + "' and " + filterFechas + " AND p.aprobado=" + val + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) AND upper(p.areafunc)='" + descripcionArea + "' order by p.id DESC ";
                    sql += " WHERE upper(form.descripcion) LIKE '" + formulario + "' and " + filterFechas + " AND p.aprobado=" + val + " AND ((concat(func.nombre, ' ', func.apellido) like '" + funcionario + "') OR func is null) order by p.id DESC ";
                }
                Query query = em.createNativeQuery(sql, Solicitud.class);
                int i = query.getResultList().size();
                System.out.print(i);
                List<Solicitud> results = query.getResultList();
                return results;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Solicitud listarPorIdFecha(Long id, Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fecha);
        try {
            String sql = "SELECT * FROM rrhh.solicitud soli left join rrhh.formulario form on soli.idformulario=form.id "
                    //                    + " WHERE soli.id=" + id + " AND soli.aprobado=1 AND soli.fechaini>='" + fechaDesdeText + " 00:00:00' and soli.fechafin<='" + fechaDesdeText + " 23:59:59' and UPPER(form.descripcion)='SOLICITUD DE PRODUCCIÓN' ";
                    + " WHERE soli.id=" + id + " AND soli.aprobado=1 AND soli.fechaini>='" + fechaDesdeText + " 00:00:00' and soli.fechafin<='" + fechaDesdeText + " 23:59:59' and UPPER(form.descripcion)='SOLICITUD DE PERMISO' ";
//            System.out.println("SQL -> " + sql);
            return (Solicitud) em.createNativeQuery(sql, Solicitud.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
        }
    }

    public Solicitud listarPorIdFechaSP(Long id, Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDesdeText = formatter.format(fecha);
        try {
            String sql = "SELECT * FROM rrhh.solicitud soli left join rrhh.formulario form on soli.idformulario=form.id "
                    + " WHERE soli.aprobado=1 AND soli.fechaini>='" + fechaDesdeText + " 00:00:00' and soli.fechafin<='" + fechaDesdeText + " 23:59:59' and UPPER(form.descripcion)='SOLICITUD DE PRODUCCIÓN' ";
//                    + " WHERE soli.id=" + id + " AND soli.aprobado=1 AND soli.fechaini>='" + fechaDesdeText + " 00:00:00' and soli.fechafin<='" + fechaDesdeText + " 23:59:59' and UPPER(form.descripcion)='SOLICITUD DE PERMISO' ";
//            System.out.println("SQL -> " + sql);
            return (Solicitud) em.createNativeQuery(sql, Solicitud.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
        }
    }
}
