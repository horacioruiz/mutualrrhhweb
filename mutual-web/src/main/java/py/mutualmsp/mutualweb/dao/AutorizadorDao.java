/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Autorizador;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class AutorizadorDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Autorizador> listaAutorizador() {
        List<Autorizador> lista = new ArrayList<>();
        List<Autorizador> lista2 = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Autorizador.findAll").getResultList();
           for (Autorizador autorizador : lista) {
                autorizador.setNombreFuncionario(autorizador.getIdnivelaprobacion().getDescripcion()
                        +" ("+autorizador.getIdfuncionario().getApellido()+", "+autorizador.getIdfuncionario().getNombre()+")");
                lista2.add(autorizador);
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista2;
    }
    
    public void guardar(Autorizador autorizador){
        try {
            em.merge(autorizador);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Autorizador autorizador) {
        try {
            em.remove(em.contains(autorizador) ? autorizador : em.merge(autorizador));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Autorizador> getListAutorizador(String valor) {
        List<Autorizador> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.autorizador " +
"                   where  id="+valor+";", Autorizador.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select a from Autorizador a")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
