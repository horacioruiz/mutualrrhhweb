/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.AdjuntosAdmision;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class AdjuntosAdmisionDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<AdjuntosAdmision> listaAdjuntosAdmision() {
        List<AdjuntosAdmision> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("AdjuntosAdmision.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(AdjuntosAdmision adjuntosAdmision){
        try {
            em.merge(adjuntosAdmision);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(AdjuntosAdmision adjuntosAdmision) {
        try {
            em.remove(em.contains(adjuntosAdmision) ? adjuntosAdmision : em.merge(adjuntosAdmision));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<AdjuntosAdmision> getListAdjuntosAdmision(String valor) {
        List<AdjuntosAdmision> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.adjuntos_admision " +
"                   where  id="+valor+";", AdjuntosAdmision.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select a from AdjuntosAdmision a")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
