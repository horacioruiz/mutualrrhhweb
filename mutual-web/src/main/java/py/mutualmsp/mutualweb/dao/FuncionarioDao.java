/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Vacaciones;

/**
 *
 * @author Dbarreto
 */
@Stateless
public class FuncionarioDao implements Serializable {

    @PersistenceContext
    private EntityManager em;

    public List<Funcionario> listaFuncionario() {
        List<Funcionario> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Funcionario.findAll").getResultList();
            lista = em.createNativeQuery("select * from funcionario f where (f.idtipofuncionario = 17 \n" +
            "or f.idtipofuncionario = 17 \n" +
            "or f.idtipofuncionario = 18 \n" +
            "or f.idtipofuncionario = 323 \n" +
            "or f.idtipofuncionario = 228 \n" +
            "or f.idtipofuncionario = 229 \n" +
            "or f.idtipofuncionario = 230 \n" +
            "or f.idtipofuncionario = 284)\n " +
            "and f.idestado = 13 and id  <> 0  and id <> 14 \n" +
            " order by id , apellido, nombre,cedula", Funcionario.class)
                     .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public List<Funcionario> listaFuncionarioPorFilial(Long idFilial) {
        List<Funcionario> lista = new ArrayList<>();
        try {  
            lista = em.createNativeQuery("select * from funcionario f where (f.idtipofuncionario = 17 or f.idtipofuncionario = 18 or f.idtipofuncionario = 284 or f.idtipofuncionario = 283  or f.idtipofuncionario = 17 or f.idtipofuncionario = 271 or f.idtipofuncionario = 323) and f.idestado = 13 and id  <> 0  and id <> 14 and idfilial = :idfilial \n" +
            " order by id , apellido, nombre,cedula", Funcionario.class)
                    .setParameter("idfilial", idFilial)
                     .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public List<Funcionario> listaVerificador() {
        List<Funcionario> lista = new ArrayList<>();
        try {
            //lista = em.createNamedQuery("Funcionario.findAll").getResultList();  
            lista = em.createNativeQuery("SELECT * FROM public.funcionario "
                    + "                   where  idestado=13 and id=13;", Funcionario.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Funcionario guardar(Funcionario funcionario) {
        if (funcionario.getId()== null) {
            em.persist(funcionario);
        } else {
            em.merge(funcionario);
        }
        em.flush();
        return funcionario;
    }

    public void borrar(Funcionario funcionario) {
        try {
            em.remove(em.contains(funcionario) ? funcionario : em.merge(funcionario));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Funcionario> getListFuncionario(String value, Long idDependencia) {
        List<Funcionario> lista = new ArrayList<>();
        try {
            if (value != null && !value.equals("")) {
                lista = em.createNativeQuery("SELECT * FROM funcionario f \n"
                        + "where lower(cedula || nombre || apellido) like lower('%" + value + "%') \n"
                        + "AND f.idtipofuncionario in (17, 18, 323, 228, 229, 230, 284) \n"
                        + "AND f.idestado=13 \n"
                        + "and f.id <> 0 \n"
                        + "and f.id <> 14 ", Funcionario.class)
                        .getResultList();
            }else if(idDependencia != null && idDependencia != 0){
                lista = em.createNativeQuery("SELECT * FROM public.funcionario where  idestado=13 and iddependencia= :id", Funcionario.class)
                        .setParameter("id", idDependencia)
                        .getResultList();
            } else {
                lista = em.createQuery("select f from Funcionario f \n"
                        + "WHERE f.idtipofuncionario in (17, 18, 323, 228, 229, 230, 284) \n"
                        + "AND f.idestado=13 \n"
                        + "and id <> 0 \n"   
                        + "and id <> 14 \n")
                        .getResultList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public Funcionario getNinguno() {
        Funcionario ninguno = null;
        try {
            ninguno = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario "
                    + " where  lower(nombre) like lower('%NINGUNO%') AND idestado=13;", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Funcionario) em.createQuery("select f from Funcionario f WHERE f.idestado=13")
                    .getResultList();
        }
        return ninguno;
    }

    public Funcionario getFuncionario(String valor) {
        Funcionario funcionario = null;
        try {
            funcionario = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario f "
                    + "left join usuario u ON  f.id = u.idfuncionario "
                    + "where  lower(u.usuario) like lower('%" + valor + "%') AND f.idestado=13;", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            funcionario = (Funcionario) em.createQuery("select f from Funcionario f WHERE f.idestado=13")
                    .getResultList();
        }
        return funcionario;
    }

    public Funcionario getFuncionarioById(String valor) {
        Funcionario funcionario = null;
        try {
            funcionario = (Funcionario) em.createNativeQuery("SELECT * FROM public.funcionario f "
                    + "left join usuario u ON  f.id = u.idfuncionario "
                    + "where id=" + valor + " AND f.idestado=13;", Funcionario.class)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return funcionario;
    }

    public Funcionario listarFuncionarioPorCI(String value) {
        Funcionario lista = new Funcionario();
        Query query = null;
        try {
//            lista = (Funcionario) em.createQuery("select c from Funcionario c JOIN FETCH c.cargo car WHERE c.cedula=:ci AND c.idestado=13")
//                    .setParameter("ci", value).setMaxResults(1).getSingleResult();
//            lista = (Funcionario) em.createQuery("select c from Funcionario c JOIN FETCH c.cargo car WHERE c.cedula=:ci AND c.idestado=13")
//                    .setParameter("ci", value).getResultList().get(0);
            
            String sql = "SELECT * "
                    + " from public.funcionario f "
                    + " WHERE f.idestado=13 AND f.cedula='" + value + "' ORDER BY f.id ASC";
            System.out.println("SQL FUNC ==> " + sql);
            query = em.createNativeQuery(sql, Funcionario.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Funcionario> results = query.getResultList();
            return results.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public List<Funcionario> ListarPorDependencia(Long id) {
        List<Funcionario> lista = new ArrayList<>();
        try {
            lista = em.createQuery("select c from Funcionario c JOIN FETCH c.dependencia dep WHERE (dep.id=:idDep OR dep.iddependenciapadre=:idDep) AND c.idestado=13")
                    .setParameter("idDep", id).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Funcionario listarPorNombreApellido(String nombreApellido) {
        String[] parts = nombreApellido.split(" ");
        Funcionario lista = new Funcionario();
        try {
            List<Funcionario> listaFunc = new ArrayList<>();
            listaFunc = em.createQuery("select func from Funcionario func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[1] + "%")
                    .setParameter("apellido", "%" + parts[0] + "%")
                    .getResultList();
            if (!listaFunc.isEmpty()) {
                lista = listaFunc.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public Funcionario listarPorApellidoNombre(String apellidoNombre) {
        String[] parts = apellidoNombre.split(" ");
        Funcionario lista = new Funcionario();
        try {
            lista = (Funcionario) em.createQuery("select func from Funcionario func WHERE UPPER(func.nombre) LIKE :nombre AND UPPER(func.apellido) LIKE :apellido AND func.idestado=13")
                    .setParameter("nombre", "%" + parts[1] + "%")
                    .setParameter("apellido", "%" + parts[0] + "%")
                    .getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("--> " + e.getLocalizedMessage());
            System.out.println("-> " + e.fillInStackTrace());
        }
        return lista;
    }

    public List<Funcionario> listarFuncionarioParametro(Dependencia padre, Cargo hijo, Funcionario funcionario) {
        Funcionario lista = new Funcionario();
        try {
            Query query = null;
            String parametro = "";
            if (padre != null) {
                parametro += " AND depen.id=" + padre.getId() + " ";
            }
            if (hijo != null) {
                parametro += " AND car.id=" + hijo.getId() + " ";
            }
            if (funcionario != null) {
                parametro += " AND f.id=" + funcionario.getId()+ " ";
            }
            String sql = "SELECT * "
                    + " from public.funcionario f "
                    + " left join public.dependencia depen on depen.id=f.id "
                    + " left join public.cargo car on car.id=f.idcargo "
                    + " WHERE f.idestado=13 " + parametro + " ORDER BY f.id ASC";
            System.out.println("filtrarFuncionarioParametro ==> " + sql);
            query = em.createNativeQuery(sql, Funcionario.class);

            int i = query.getResultList().size();
            System.out.print(i);
            List<Funcionario> results = query.getResultList();
            return results;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
