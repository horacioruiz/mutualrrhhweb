/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Analisis;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class AnalisisDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Analisis> listaAnalisis() {
        List<Analisis> lista = new ArrayList<>();
        
        try {
            lista = em.createNamedQuery("Analisis.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Analisis analisis){
        try {
            em.merge(analisis);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Analisis analisis) {
        try {
            em.remove(em.contains(analisis) ? analisis : em.merge(analisis));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Analisis> getListAnalisis(String valor) {
        List<Analisis> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.analisis " +
"                   where  id="+valor+";", Analisis.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select a from Analisis a")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    
}
