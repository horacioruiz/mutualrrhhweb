/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.mutualmsp.mutualweb.entities.Modalidad;


/**
 *
 * @author Dbarreto
 */
@Stateless
public class ModalidadDao implements Serializable{
    @PersistenceContext
    private EntityManager em;
    
    public List<Modalidad> listaModalidad() {
        List<Modalidad> lista = new ArrayList<>();
        try {
            lista = em.createNamedQuery("Modalidad.findAll").getResultList();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public void guardar(Modalidad modalidad){
        try {
            em.merge(modalidad);
        } catch (Exception e) {
            e.printStackTrace();
        }
                
    }
    
    public void borrar(Modalidad modalidad) {
        try {
            em.remove(em.contains(modalidad) ? modalidad : em.merge(modalidad));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Modalidad> getListModalidad(String valor) {
        List<Modalidad> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.modalidad " +
"                   where  lower(descripcion) like lower('%"+valor+"%');", Modalidad.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select m from Modalidad m")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }

    public List<Modalidad> getListTipooperacion(String valor) {
        List<Modalidad> lista = new ArrayList<>();
       try {
           if (valor != null && !valor.equals("")) {
               lista = em.createNativeQuery("SELECT * FROM public.modalidad m " +
                "left join public.tipooperaciondetalle td ON m.idmodalidad=td.idmodalidad " +
                "where  td.idtipooperacion="+valor+";", Modalidad.class)
                   .getResultList();
           }else{
               lista = em.createQuery("select m from Modalidad m")
                   .getResultList();
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return lista;
    }
    
    public Modalidad getNinguno() {
        Modalidad ninguno = null;
        try {
            ninguno = (Modalidad) em.createNativeQuery("SELECT * FROM public.modalidad " +
            "               where  lower(descripcion) like lower('%NINGUNO%');", Modalidad.class)
                   .getSingleResult();       
        } catch (Exception e) {
            e.printStackTrace();
            ninguno = (Modalidad) em.createQuery("select m from Modalidad m")
                .getResultList();
        }
        return ninguno;
    }
}
