/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Estadoverificacion;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Modalidad;
import py.mutualmsp.mutualweb.entities.Procedencia;
import py.mutualmsp.mutualweb.entities.Region;
import py.mutualmsp.mutualweb.entities.Tipooperacion;

/**
 *
 * @author DBarreto
 */
public class VerificacionDTO  implements Serializable {
    private Long idverificacion;
    private Date fechaverificacion;
    private String horarecepcion;
    private String horaremision;
    private String cedula;
    private Long numerosolicitud;
    private Long numerooperacion;
    private Date fechaoperacion;
    private Long numerocontrato;
    private Long numeroboleta;
    private Long numeroordencredito;
    private String local;
    private String puntoexpedicion;
    private Long numerofactura;
    private Long numeronotacredito;
    private Long numeronotapedido;
    private Long numeroordenpago;
    private Long numerocheque;
    private Long numeroplanilla;
    private Procedencia idprocedencia;
    private String nombrecomercio;
    private String mes;
    private String periodo;
    private Region idregional;
    private String observaciones;
    private Date fechacorreccion;
    private String correccion;
    private Date fecharevision;
    private String revisionfinal;
    private String observacionanulado;

    public Long getIdverificacion() {
        return idverificacion;
    }

    public void setIdverificacion(Long idverificacion) {
        this.idverificacion = idverificacion;
    }

    public Date getFechaverificacion() {
        return fechaverificacion;
    }

    public void setFechaverificacion(Date fechaverificacion) {
        this.fechaverificacion = fechaverificacion;
    }

    public String getHorarecepcion() {
        return horarecepcion;
    }

    public void setHorarecepcion(String horarecepcion) {
        this.horarecepcion = horarecepcion;
    }

    public String getHoraremision() {
        return horaremision;
    }

    public void setHoraremision(String horaremision) {
        this.horaremision = horaremision;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Long getNumerosolicitud() {
        return numerosolicitud;
    }

    public void setNumerosolicitud(Long numerosolicitud) {
        this.numerosolicitud = numerosolicitud;
    }

    public Long getNumerooperacion() {
        return numerooperacion;
    }

    public void setNumerooperacion(Long numerooperacion) {
        this.numerooperacion = numerooperacion;
    }

    public Date getFechaoperacion() {
        return fechaoperacion;
    }

    public void setFechaoperacion(Date fechaoperacion) {
        this.fechaoperacion = fechaoperacion;
    }

    public Long getNumerocontrato() {
        return numerocontrato;
    }

    public void setNumerocontrato(Long numerocontrato) {
        this.numerocontrato = numerocontrato;
    }

    public Long getNumeroboleta() {
        return numeroboleta;
    }

    public void setNumeroboleta(Long numeroboleta) {
        this.numeroboleta = numeroboleta;
    }

    public Long getNumeroordencredito() {
        return numeroordencredito;
    }

    public void setNumeroordencredito(Long numeroordencredito) {
        this.numeroordencredito = numeroordencredito;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getPuntoexpedicion() {
        return puntoexpedicion;
    }

    public void setPuntoexpedicion(String puntoexpedicion) {
        this.puntoexpedicion = puntoexpedicion;
    }

    public Long getNumerofactura() {
        return numerofactura;
    }

    public void setNumerofactura(Long numerofactura) {
        this.numerofactura = numerofactura;
    }

    public Long getNumeronotacredito() {
        return numeronotacredito;
    }

    public void setNumeronotacredito(Long numeronotacredito) {
        this.numeronotacredito = numeronotacredito;
    }

    public Long getNumeronotapedido() {
        return numeronotapedido;
    }

    public void setNumeronotapedido(Long numeronotapedido) {
        this.numeronotapedido = numeronotapedido;
    }

    public Long getNumeroordenpago() {
        return numeroordenpago;
    }

    public void setNumeroordenpago(Long numeroordenpago) {
        this.numeroordenpago = numeroordenpago;
    }

    public Long getNumerocheque() {
        return numerocheque;
    }

    public void setNumerocheque(Long numerocheque) {
        this.numerocheque = numerocheque;
    }

    public Long getNumeroplanilla() {
        return numeroplanilla;
    }

    public void setNumeroplanilla(Long numeroplanilla) {
        this.numeroplanilla = numeroplanilla;
    }

    public Procedencia getIdprocedencia() {
        return idprocedencia;
    }

    public void setIdprocedencia(Procedencia idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    public String getNombrecomercio() {
        return nombrecomercio;
    }

    public void setNombrecomercio(String nombrecomercio) {
        this.nombrecomercio = nombrecomercio;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Region getIdregional() {
        return idregional;
    }

    public void setIdregional(Region idregional) {
        this.idregional = idregional;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechacorreccion() {
        return fechacorreccion;
    }

    public void setFechacorreccion(Date fechacorreccion) {
        this.fechacorreccion = fechacorreccion;
    }

    public String getCorreccion() {
        return correccion;
    }

    public void setCorreccion(String correccion) {
        this.correccion = correccion;
    }

    public Date getFecharevision() {
        return fecharevision;
    }

    public void setFecharevision(Date fecharevision) {
        this.fecharevision = fecharevision;
    }

    public String getRevisionfinal() {
        return revisionfinal;
    }

    public void setRevisionfinal(String revisionfinal) {
        this.revisionfinal = revisionfinal;
    }

    public String getObservacionanulado() {
        return observacionanulado;
    }

    public void setObservacionanulado(String observacionanulado) {
        this.observacionanulado = observacionanulado;
    }
    
    
}
