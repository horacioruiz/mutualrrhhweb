/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import py.mutualmsp.mutualweb.entities.Requisito;

/**
 *
 * @author Dbarreto
 */
public class DetalleVerificacionDTO implements  Serializable{
    @Getter
    @Setter
    private Long idverificaciondetalle;
    @Getter
    @Setter
    private Boolean verificado;
    @Getter
    @Setter
    private String observacion;
    @Getter
    @Setter
    private Requisito idrequisito;
    @Getter
    @Setter    
    private Boolean indiferente;
    @Getter
    @Setter    
    private Boolean noverificado;
    @Getter
    @Setter
    private VerificacionDTO idverificacion;

    
}
