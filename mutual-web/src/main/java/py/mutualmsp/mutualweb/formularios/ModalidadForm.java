/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.ModalidadDao;
import py.mutualmsp.mutualweb.entities.Modalidad;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class ModalidadForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    ModalidadDao modalidadDao = ResourceLocator.locate(ModalidadDao.class);
    //AuditoriaDao auditoriaDao = ResourceLocator.locate(AuditoriaDao.class);
    Binder<Modalidad> binder = new Binder<>(Modalidad.class);
    
    Modalidad modalidad;
    //Auditoria auditoria;
    
    private Consumer<Modalidad> guardarListener;
    private Consumer<Modalidad> borrarListener;
    private Consumer<Modalidad> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "modalidad";
            
    public ModalidadForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(txtfDescripcion, botones);
            
            binder.bind(txtfDescripcion, Modalidad::getDescripcion, Modalidad::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Modalidad::getDescripcion, Modalidad::setDescripcion);
            binder.bindInstanceFields(this);
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(modalidad);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            modalidadDao.guardar(modalidad);
            setVisible(false);
            guardarListener.accept(modalidad);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            /*auditoria = new Auditoria();
            auditoria.setNombretabla("modalidad");
            if(viejo.equals("")) auditoria.setOperacion("I");
            else auditoria.setOperacion("M");
            auditoria.setValorviejo(viejo);
            this.setNuevo();
            auditoria.setValornuevo(nuevo);
            java.util.Date date = new java.util.Date();
            auditoria.setFechaactualizacion(date);
            auditoria.setUsuario(UserHolder.get().getUsuario());
            auditoriaDao.guardar(auditoria);*/
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(modalidad);
        }
    }
    
    private void borrar() {
        try {
            modalidadDao.borrar(modalidad);
            setVisible(false);
            borrarListener.accept(modalidad);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(modalidad);
        }
    }
    
    public void setGuardarListener(Consumer<Modalidad> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Modalidad> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Modalidad> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }
    
    public void setViejo(){
        if(modalidad.getIdmodalidad()!=null) this.viejo = "("+modalidad.getIdmodalidad()+","+modalidad.getDescripcion()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+modalidad.getIdmodalidad()+","+modalidad.getDescripcion()+")";
    }

    public void setModalidad(Modalidad m) {
        try {
            this.modalidad = m;
            binder.setBean(m);
            btnBorrar.setVisible((modalidad.getIdmodalidad()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
