/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.RequisitoDao;
import py.mutualmsp.mutualweb.dao.RequisitooperacionDao;
import py.mutualmsp.mutualweb.dao.TipooperacionDao;
import py.mutualmsp.mutualweb.entities.Requisito;
import py.mutualmsp.mutualweb.entities.Requisitooperacion;
import py.mutualmsp.mutualweb.entities.Tipooperacion;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class RequisitooperacionForm extends FormLayout{
    ComboBox<Tipooperacion> cmbTipooperacion = new ComboBox<>("Tipo Operación");
    ComboBox<Requisito> cmbRequisito = new ComboBox<>("Requisito");
    CheckBox chkActivo = new CheckBox("Activo");
    DateField dtfFechaActivacion = new DateField("Fecha Activación");
    DateField dtfFechaDesactivacion = new DateField("Fecha Desactivación");
    
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    RequisitooperacionDao requisitooperacionDao = ResourceLocator.locate(RequisitooperacionDao.class);
    Binder<Requisitooperacion> binder = new Binder<>(Requisitooperacion.class);
    
    TipooperacionDao tipooperacionDao = ResourceLocator.locate(TipooperacionDao.class);
    RequisitoDao requisitoDao = ResourceLocator.locate(RequisitoDao.class);
    
    Requisitooperacion requisitooperacion;
    
    private Consumer<Requisitooperacion> guardarListener;
    private Consumer<Requisitooperacion> borrarListener;
    private Consumer<Requisitooperacion> cancelarListener;
            
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "requisitooperacion";
            
    public RequisitooperacionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            List<Tipooperacion> listaTipooperaciones = tipooperacionDao.listaTipooperacion();
            cmbTipooperacion.setItems(listaTipooperaciones);
            cmbTipooperacion.setItemCaptionGenerator(Tipooperacion::getDescripcion);
            
            binder.bind(cmbTipooperacion, 
                    (Requisitooperacion source) -> source.getIdtipooperacion(),
                    (Requisitooperacion bean, Tipooperacion fieldvalue) -> {
                    bean.setIdtipooperacion(fieldvalue);});
            
            List<Requisito> listaRequisitos = requisitoDao.listaRequisito();
            cmbRequisito.setItems(listaRequisitos);
            cmbRequisito.setItemCaptionGenerator(Requisito::getDescripcion);
            
            binder.bind(cmbRequisito, 
                    (Requisitooperacion source) -> source.getIdrequisito(),
                    (Requisitooperacion bean, Requisito fieldvalue) -> {
                    bean.setIdrequisito(fieldvalue);});
            
            chkActivo.setValue(Boolean.TRUE);
            binder.bind(chkActivo, Requisitooperacion::getActivo, Requisitooperacion::setActivo);
            
            binder.bind(dtfFechaActivacion, No.getter(), No.setter());
            
            binder.bind(dtfFechaDesactivacion, No.getter(), No.setter());
            
            btnGuardar.addStyleName(ValoTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(ValoTheme.BUTTON_DANGER);
            
            addComponents(cmbTipooperacion, cmbRequisito, chkActivo, botones);
            
            chkActivo.addValueChangeListener(c ->{
                Date input = new Date();
                LocalDate date = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalTime time = input.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
                if (c.getValue()==Boolean.TRUE) {
                    dtfFechaActivacion.setValue(date);
                    dtfFechaDesactivacion.setValue(null);
                } else if (c.getValue()==Boolean.FALSE) {
                    dtfFechaDesactivacion.setValue(date);
                }
            });
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(requisitooperacion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // convenience empty getter and setter implementation for better readability
    public static class No {
       public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
           return source -> null;
       }
       public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
           return (bean, fieldValue) -> {
               //no op
           };
       }
    }
    
    private void guardar() {
        try {
            if(dtfFechaActivacion.getValue()==null)  requisitooperacion.setFechaactivacion(null);
            else {java.util.Date dfechaActivacion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaActivacion.getValue().toString());
                requisitooperacion.setFechaactivacion(dfechaActivacion);}
            if(dtfFechaDesactivacion.getValue()==null)  requisitooperacion.setFechadesactivacion(null);
            else {java.util.Date dfechaDesactivacion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaDesactivacion.getValue().toString());
            requisitooperacion.setFechadesactivacion(dfechaDesactivacion);}
            if (requisitooperacion.getActivo()==null) requisitooperacion.setActivo(Boolean.FALSE);
            requisitooperacionDao.guardar(requisitooperacion);
            setVisible(false);
            guardarListener.accept(requisitooperacion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(requisitooperacion);
        }
    }
    
    private void borrar() {
        try {
            requisitooperacionDao.borrar(requisitooperacion);
            setVisible(false);
            borrarListener.accept(requisitooperacion);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(requisitooperacion);
        }
    }
    
    public void setGuardarListener(Consumer<Requisitooperacion> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Requisitooperacion> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Requisitooperacion> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dt=null,dt1=null;
        if(requisitooperacion.getFechaactivacion()!=null ) dt = LocalDate.parse(requisitooperacion.getFechaactivacion().toString(), formatter);
        if(requisitooperacion.getFechadesactivacion()!=null) dt1=LocalDate.parse(requisitooperacion.getFechadesactivacion().toString(), formatter);
        if(requisitooperacion.getIdrequisitooperacion()!=null) this.viejo = "("+requisitooperacion.getIdrequisitooperacion()+","+requisitooperacion.getIdrequisito().getIdrequisito()+","+requisitooperacion.getIdtipooperacion().getIdtipooperacion()+","+requisitooperacion.getActivo()+","+dt+","+dt1+")";
    }
    
    public void setNuevo() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dt=null,dt1=null;
        if(requisitooperacion.getFechaactivacion()!=null ) dt = LocalDate.parse(requisitooperacion.getFechaactivacion().toString(), formatter);
        if(requisitooperacion.getFechadesactivacion()!=null) dt1=LocalDate.parse(requisitooperacion.getFechadesactivacion().toString(), formatter);
        this.nuevo = "("+requisitooperacion.getIdrequisitooperacion()+","+requisitooperacion.getIdrequisito().getIdrequisito()+","+requisitooperacion.getIdtipooperacion().getIdtipooperacion()+","+requisitooperacion.getActivo()+","+dt+","+dt1+")";
    }

    public void setRequisitooperacion(Requisitooperacion ro) {
        try {
            this.requisitooperacion = ro;
            binder.setBean(ro);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            if (requisitooperacion.getFechaactivacion()!= null) {
                LocalDate dt = LocalDate.parse(requisitooperacion.getFechaactivacion().toString(), formatter);
                dtfFechaActivacion.setValue(dt);}
            if (requisitooperacion.getFechadesactivacion()!= null) {
                LocalDate dt = LocalDate.parse(requisitooperacion.getFechadesactivacion().toString(), formatter);
                dtfFechaDesactivacion.setValue(dt);}
            btnBorrar.setVisible((requisitooperacion.getIdrequisitooperacion()!=null));
            setVisible(true);
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
