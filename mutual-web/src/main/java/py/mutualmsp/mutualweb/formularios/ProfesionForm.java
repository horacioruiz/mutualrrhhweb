/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.ProfesionDao;
import py.mutualmsp.mutualweb.entities.Profesion;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class ProfesionForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    CheckBox chkActivo = new CheckBox("Activo");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    ProfesionDao profesionDao = ResourceLocator.locate(ProfesionDao.class);
    Binder<Profesion> binder = new Binder<>(Profesion.class);
    
    Profesion profesion;
    
    private Consumer<Profesion> guardarListener;
    private Consumer<Profesion> borrarListener;
    private Consumer<Profesion> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "profesion";
            
    public ProfesionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(txtfDescripcion, chkActivo, botones);
            
            binder.bind(txtfDescripcion, Profesion::getDescripcion, Profesion::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Profesion::getDescripcion, Profesion::setDescripcion);
            binder.bindInstanceFields(this);
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            chkActivo.setValue(Boolean.TRUE);
            binder.bind(chkActivo, Profesion::getActivo, Profesion::setActivo);
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(profesion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            profesionDao.guardar(profesion);
            setVisible(false);
            guardarListener.accept(profesion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(profesion);
        }
    }
    
    private void borrar() {
        try {
            profesionDao.borrar(profesion);
            setVisible(false);
            borrarListener.accept(profesion);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(profesion);
        }
    }
    
    public void setGuardarListener(Consumer<Profesion> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Profesion> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Profesion> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }
    
    public void setViejo(){
        if(profesion.getId()!=null) this.viejo = "("+profesion.getId()+","+profesion.getDescripcion()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+profesion.getId()+","+profesion.getDescripcion()+")";
    }

    public void setProfesion(Profesion p) {
        try {
            this.profesion = p;
            binder.setBean(p);
            btnBorrar.setVisible((profesion.getId()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
