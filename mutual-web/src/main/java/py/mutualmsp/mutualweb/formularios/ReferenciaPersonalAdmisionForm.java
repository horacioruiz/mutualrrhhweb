/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.ParentescoDao;
import py.mutualmsp.mutualweb.dao.ReferenciaPersonalAdmisionDao;
import py.mutualmsp.mutualweb.entities.Parentesco;
import py.mutualmsp.mutualweb.entities.ReferenciaPersonalAdmision;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class ReferenciaPersonalAdmisionForm extends FormLayout{
    TextField txtfNombre = new TextField("Nombre", "Ingrese nombre");
    TextField txtfApellido = new TextField("Apellido", "Ingrese apellido");
    TextField txtfTelefono = new TextField("Telefono", "Ingrese nº de teléfono");
    ComboBox<Parentesco> cmbParentesco = new ComboBox<>("Parentesco");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    ReferenciaPersonalAdmisionDao referenciaPersonalAdmisionDao = ResourceLocator.locate(ReferenciaPersonalAdmisionDao.class);
    ParentescoDao parentescoDao = ResourceLocator.locate(ParentescoDao.class);
    Binder<ReferenciaPersonalAdmision> binder = new Binder<>(ReferenciaPersonalAdmision.class);
    
    ReferenciaPersonalAdmision referenciaPersonalAdmision;
    
    private Consumer<ReferenciaPersonalAdmision> guardarListener;
    private Consumer<ReferenciaPersonalAdmision> borrarListener;
    private Consumer<ReferenciaPersonalAdmision> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "referencia_personal_admision";
            
    public ReferenciaPersonalAdmisionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            cmbParentesco.setWidth("101%");
            
            addComponents(txtfNombre, txtfApellido, cmbParentesco, txtfTelefono, botones);
            
            binder.bind(txtfNombre, ReferenciaPersonalAdmision::getNombre, ReferenciaPersonalAdmision::setNombre);
            binder.forField(txtfNombre).withNullRepresentation("")
                    .bind(ReferenciaPersonalAdmision::getNombre, ReferenciaPersonalAdmision::setNombre);
            binder.bindInstanceFields(this);
            
            binder.bind(txtfApellido, ReferenciaPersonalAdmision::getApellido, ReferenciaPersonalAdmision::setApellido);
            binder.forField(txtfApellido).withNullRepresentation("")
                    .bind(ReferenciaPersonalAdmision::getApellido, ReferenciaPersonalAdmision::setApellido);
            binder.bindInstanceFields(this);
            
            binder.bind(txtfTelefono, ReferenciaPersonalAdmision::getTelefono, ReferenciaPersonalAdmision::setTelefono);
            binder.forField(txtfTelefono).withNullRepresentation("")
                    .bind(ReferenciaPersonalAdmision::getTelefono, ReferenciaPersonalAdmision::setTelefono);
            binder.bindInstanceFields(this);
            
            List<Parentesco> listaParentescos = parentescoDao.listaParentesco();
            cmbParentesco.setItems(listaParentescos);
            cmbParentesco.setItemCaptionGenerator(Parentesco::getDescripcion);
            binder.bind(cmbParentesco, 
                    (ReferenciaPersonalAdmision source) -> source.getIdparentesco(),
                    (ReferenciaPersonalAdmision bean, Parentesco fieldvalue) -> {
                        bean.setIdparentesco(fieldvalue);});
            
            txtfNombre.addValueChangeListener(n -> {txtfNombre.setValue(txtfNombre.getValue().toUpperCase());});
            txtfApellido.addValueChangeListener(a -> {txtfApellido.setValue(txtfApellido.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(referenciaPersonalAdmision);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            referenciaPersonalAdmisionDao.guardar(referenciaPersonalAdmision);
            setVisible(false);
            guardarListener.accept(referenciaPersonalAdmision);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(referenciaPersonalAdmision);
        }
    }
    
    private void borrar() {
        try {
            referenciaPersonalAdmisionDao.borrar(referenciaPersonalAdmision);
            setVisible(false);
            borrarListener.accept(referenciaPersonalAdmision);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(referenciaPersonalAdmision);
        }
    }
    
    public void setGuardarListener(Consumer<ReferenciaPersonalAdmision> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<ReferenciaPersonalAdmision> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<ReferenciaPersonalAdmision> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(referenciaPersonalAdmision.getId()!=null) this.viejo = "("+referenciaPersonalAdmision.getId()+","+referenciaPersonalAdmision.getIdsolicitudAdmision().getId()+","+referenciaPersonalAdmision.getNombre()+","+referenciaPersonalAdmision.getApellido()+","+referenciaPersonalAdmision.getIdparentesco().getId()+","+referenciaPersonalAdmision.getTelefono()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+referenciaPersonalAdmision.getId()+","+referenciaPersonalAdmision.getIdsolicitudAdmision().getId()+","+referenciaPersonalAdmision.getNombre()+","+referenciaPersonalAdmision.getApellido()+","+referenciaPersonalAdmision.getIdparentesco().getId()+","+referenciaPersonalAdmision.getTelefono()+")";
    }

    public void setRequisito(ReferenciaPersonalAdmision r) {
        try {
            this.referenciaPersonalAdmision = r;
            binder.setBean(r);
            btnBorrar.setVisible((referenciaPersonalAdmision.getId()!=null));
            setVisible(true);
            txtfNombre.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
