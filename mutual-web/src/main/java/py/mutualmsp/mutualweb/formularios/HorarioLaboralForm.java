/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */
public class HorarioLaboralForm extends FormLayout {

//    TextField txtfDescripcion = new TextField("Descripción", "");
    CheckBox habilitado = new CheckBox("Habilitado");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    TextField txtDescripcion = new TextField("Descripcion");
    ComboBox<String> horaEntrada = new ComboBox<>("Hs. Entrada");
    ComboBox<String> minutoEntrada = new ComboBox<>("Minuto");
    ComboBox<String> horaSalida = new ComboBox<>("Hs Salida");
    ComboBox<String> minutoSalida = new ComboBox<>("Minuto");

    HorarioLaboralDao cargoDao = ResourceLocator.locate(HorarioLaboralDao.class);
    Binder<HorarioLaboral> binder = new Binder<>(HorarioLaboral.class);
//    TimeField timePickerEntrada = new TimeField();
//    TimeField timePickerSalida = new TimeField();

    HorarioLaboral cargo;

    private Consumer<HorarioLaboral> guardarListener;
    private Consumer<HorarioLaboral> borrarListener;
    private Consumer<HorarioLaboral> cancelarListener;

    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "cargo";

    public HorarioLaboralForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();

            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            ArrayList<String> myHourIn = new ArrayList<String>();
            for (int i = 0; i < 24; i++) {
                myHourIn.add("" + i);
            }
            horaEntrada.setItems(myHourIn);
            horaEntrada.setWidth(5f, ComboBox.UNITS_EM);
            horaSalida.setItems(myHourIn);
            horaSalida.setWidth(5f, ComboBox.UNITS_EM);
            ArrayList<String> myHourOut = new ArrayList<String>();
            for (int i = 0; i < 60; i++) {
                if ((String.valueOf(i)).length() == 1) {
                    myHourOut.add("0" + i);
                } else {
                    myHourOut.add(i + "");
                }
            }
            minutoEntrada.setItems(myHourOut);
            minutoEntrada.setWidth(5f, ComboBox.UNITS_EM);
            minutoSalida.setItems(myHourOut);
            minutoSalida.setWidth(5f, ComboBox.UNITS_EM);

            HorizontalLayout entradaIn = new HorizontalLayout();
            entradaIn.addComponent(horaEntrada);
            entradaIn.addComponent(new Label(":"));
            entradaIn.addComponent(minutoEntrada);

            HorizontalLayout salidaOut = new HorizontalLayout();
            salidaOut.addComponent(horaSalida);
            salidaOut.addComponent(new Label(":"));
            salidaOut.addComponent(minutoSalida);

            addComponents(txtDescripcion, entradaIn, salidaOut, habilitado, botones);

//            binder.bind(cb, HorarioLaboral::getDescripcion, HorarioLaboral::setDescripcion);
//            binder.forField(txtfDescripcion).withNullRepresentation("")
//                    .bind(HorarioLaboral::getDescripcion, HorarioLaboral::setDescripcion);
//            binder.bindInstanceFields(this);
//            txtfDescripcion.addValueChangeListener(m -> {
//                txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());
//            });
            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    if (Long.parseLong(horaSalida.getValue()) == Long.parseLong(horaEntrada.getValue())) {
                        if (Long.parseLong(minutoEntrada.getValue()) > Long.parseLong(minutoSalida.getValue())) {
                            Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
                        } else if (Long.parseLong(horaSalida.getValue()) < Long.parseLong(horaEntrada.getValue())) {
                            Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
                        } else {
                            guardar();
                        }
                    } else if (Long.parseLong(horaSalida.getValue()) < Long.parseLong(horaEntrada.getValue())) {
                        Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
                    } else {
                        guardar();
                    }
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(cargo);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (horaEntrada.getValue() == null || horaEntrada.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (horaSalida.getValue() == null || horaSalida.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (minutoEntrada.getValue() == null || minutoEntrada.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (minutoSalida.getValue() == null || minutoSalida.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (txtDescripcion.getValue() == null || txtDescripcion.getValue().isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void guardar() {
        try {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String vHoraEntrada = horaEntrada.getValue().length() == 1 ? "0" + horaEntrada.getValue() : horaEntrada.getValue();
                String vMinutoEntrada = minutoEntrada.getValue().length() == 1 ? "0" + minutoEntrada.getValue() : minutoEntrada.getValue();
                String vHoraSalida = horaSalida.getValue().length() == 1 ? "0" + horaSalida.getValue() : horaSalida.getValue();
                String vMinutoSalida = minutoSalida.getValue().length() == 1 ? "0" + minutoSalida.getValue() : minutoSalida.getValue();
                Date parsedDateIn = dateFormat.parse("2020-03-12 " + vHoraEntrada + ":" + vMinutoEntrada + ":00");
                if (vHoraSalida.trim().equalsIgnoreCase("12")) {
                    SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    Date parsedDateOut = dateFormat2.parse("2020-03-12 12:" + vMinutoSalida + ":00 PM");
//                    cargo.setSalida(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                    cargo.setSalida(new Timestamp(parsedDateOut.getTime()));
                } else {
                    Date parsedDateOut = dateFormat.parse("2020-03-12 " + vHoraSalida + ":" + vMinutoSalida + ":00");
                    cargo.setSalida(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));
                }

                cargo.setEntrada(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateIn)));
                cargo.setEstado(habilitado.getValue() == null ? false : habilitado.getValue());
                cargo.setDescripcion(txtDescripcion.getValue().toUpperCase().trim());
            } catch (Exception e) {
                cargo.setEntrada(null);
                cargo.setSalida(null);
            } finally {
            }
            cargoDao.guardar(cargo);
            setVisible(false);
            guardarListener.accept(cargo);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if (viejo.equals("")) {
                operacion = "I";
            } else {
                operacion = "M";
            }
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(cargo);
        }
    }

    private void borrar() {
        try {
            cargoDao.borrar(cargo);
            setVisible(false);
            borrarListener.accept(cargo);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(cargo);
        }
    }

    public void setGuardarListener(Consumer<HorarioLaboral> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<HorarioLaboral> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<HorarioLaboral> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo() {
        if (cargo.getId() != null) {
            this.viejo = "(" + cargo.getId() + "," + cargo.getEntrada() + ")";
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + cargo.getId() + "," + cargo.getEntrada() + ")";
    }

    public void setHorarioLaboral(HorarioLaboral c) {
        try {
            if (c.getId() != null) {
                this.cargo = c;
                this.habilitado.setValue(c.getEstado());
                try {
                    txtDescripcion.setValue(c.getDescripcion());
                } catch (Exception e) {
                } finally {
                }
                horaEntrada.setValue(c.getEntrada().getHours() + "");
                minutoEntrada.setValue(String.valueOf(c.getEntrada().getMinutes()).length() == 1 ? "0" + c.getEntrada().getMinutes() : String.valueOf(c.getEntrada().getMinutes()));
                horaSalida.setValue(c.getSalida().getHours() + "");
                minutoSalida.setValue(String.valueOf(c.getSalida().getMinutes()).length() == 1 ? "0" + c.getSalida().getMinutes() : String.valueOf(c.getSalida().getMinutes()));
                btnBorrar.setVisible((cargo.getId() != null));
                setVisible(true);
                this.viejo = "";
            } else {
                this.cargo = c;
                this.habilitado.setValue(false);
                horaEntrada.setValue("");
                minutoEntrada.setValue("");
                horaSalida.setValue("");
                minutoSalida.setValue("");
                btnBorrar.setVisible((cargo.getId() != null));
                setVisible(true);
                this.viejo = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
