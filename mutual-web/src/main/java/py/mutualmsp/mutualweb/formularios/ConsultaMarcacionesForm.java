/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import static py.mutualmsp.mutualweb.vista.MarcacionesView.isWeekend;

/**
 *
 * @author Dbarreto
 */
public class ConsultaMarcacionesForm extends FormLayout {

    TextField txtIdreloj = new TextField("ID reloj");
    TextField txtFuncionario = new TextField("Funcionario");
    TextField txtFecha = new TextField("Fecha");

    ComboBox<String> horaEntrada = new ComboBox<>("Hs. Entrada");
    ComboBox<String> minutoEntrada = new ComboBox<>("Minuto");
    ComboBox<String> horaSalida = new ComboBox<>("Hs Salida");
    ComboBox<String> minutoSalida = new ComboBox<>("Minuto");

    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");

    MarcacionesDao marcacionDao = ResourceLocator.locate(MarcacionesDao.class);
    Binder<Marcaciones> binder = new Binder<>(Marcaciones.class);

    Marcaciones marcacion;

    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);

    private Consumer<Marcaciones> guardarListener;
    private Consumer<Marcaciones> borrarListener;
    private Consumer<Marcaciones> cancelarListener;

    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "marcacion";

    public ConsultaMarcacionesForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            ArrayList<String> myHourIn = new ArrayList<String>();
            for (int i = 0; i < 24; i++) {
                myHourIn.add("" + i);
            }
            horaEntrada.setItems(myHourIn);
            horaEntrada.setWidth(5f, ComboBox.UNITS_EM);
            horaSalida.setItems(myHourIn);
            horaSalida.setWidth(5f, ComboBox.UNITS_EM);
            ArrayList<String> myHourOut = new ArrayList<String>();
            for (int i = 0; i < 60; i++) {
                if ((String.valueOf(i)).length() == 1) {
                    myHourOut.add("0" + i);
                } else {
                    myHourOut.add(i + "");
                }
            }
            minutoEntrada.setItems(myHourOut);
            minutoEntrada.setWidth(5f, ComboBox.UNITS_EM);
            minutoSalida.setItems(myHourOut);
            minutoSalida.setWidth(5f, ComboBox.UNITS_EM);

            HorizontalLayout entradaIn = new HorizontalLayout();
            entradaIn.addComponent(horaEntrada);
            entradaIn.addComponent(new Label(":"));
            entradaIn.addComponent(minutoEntrada);

            HorizontalLayout salidaOut = new HorizontalLayout();
            salidaOut.addComponent(horaSalida);
            salidaOut.addComponent(new Label(":"));
            salidaOut.addComponent(minutoSalida);

            addComponents(txtIdreloj, txtFuncionario, txtFecha, entradaIn, salidaOut, botones);

            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(marcacion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (horaEntrada.getValue() == null || horaEntrada.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (horaSalida.getValue() == null || horaSalida.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (minutoEntrada.getValue() == null || minutoEntrada.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (minutoSalida.getValue() == null || minutoSalida.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (txtIdreloj.getValue() == null || txtIdreloj.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (txtFuncionario.getValue() == null || txtFuncionario.getValue().isEmpty()) {
            savedEnabled = false;
        }
        if (txtFecha.getValue() == null || txtFecha.getValue().isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void guardar() {
        if (validate()) {
            if (Long.parseLong(horaSalida.getValue()) == Long.parseLong(horaEntrada.getValue())) {
                if (Long.parseLong(minutoEntrada.getValue()) > Long.parseLong(minutoSalida.getValue())) {
                    Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
                } else if (Long.parseLong(horaSalida.getValue()) < Long.parseLong(horaEntrada.getValue())) {
                    Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
                } else {
                    guardarMarcacion();
                }
            } else if (Long.parseLong(horaSalida.getValue()) < Long.parseLong(horaEntrada.getValue())) {
                Notification.show("Atención", "La hora de salida debe ser mayor a la entrada", Notification.Type.ERROR_MESSAGE);
            } else {
                guardarMarcacion();
            }
        } else {
            Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
        }
    }

    public void setGuardarListener(Consumer<Marcaciones> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Marcaciones> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Marcaciones> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

//    public void setViejo() {
//        if (marcacion.getId() != null) {
//            this.viejo = "(" + marcacion.getId() + "," + marcacion.getDescripcion() + ")";
//        }
//    }
//
//    public void setNuevo() {
//        this.nuevo = "(" + marcacion.getId() + "," + marcacion.getDescripcion() + ")";
//    }
    public void setConsultaMarcaciones(Marcaciones c) {
        try {
            this.marcacion = c;
            txtIdreloj.setValue(c.getHorarioFuncionario().getIdreloj() + "");
            txtFuncionario.setValue(c.getNombrefunc());
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String fecha = formatter.format(c.getFecha());
            txtFecha.setValue(fecha);
            txtIdreloj.setEnabled(false);
            txtFuncionario.setEnabled(false);
            txtFecha.setEnabled(false);

            horaEntrada.setValue(c.getInmarcada().getHours() + "");
            minutoEntrada.setValue(String.valueOf(c.getInmarcada().getMinutes()).length() == 1 ? "0" + c.getInmarcada().getMinutes() : String.valueOf(c.getInmarcada().getMinutes()));
            horaSalida.setValue(c.getOutmarcada().getHours() + "");
            minutoSalida.setValue(String.valueOf(c.getOutmarcada().getMinutes()).length() == 1 ? "0" + c.getOutmarcada().getMinutes() : String.valueOf(c.getOutmarcada().getMinutes()));

            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarMarcacion() {
        try {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String vHoraEntrada = horaEntrada.getValue().length() == 1 ? "0" + horaEntrada.getValue() : horaEntrada.getValue();
                String vMinutoEntrada = minutoEntrada.getValue().length() == 1 ? "0" + minutoEntrada.getValue() : minutoEntrada.getValue();
                String vHoraSalida = horaSalida.getValue().length() == 1 ? "0" + horaSalida.getValue() : horaSalida.getValue();
                String vMinutoSalida = minutoSalida.getValue().length() == 1 ? "0" + minutoSalida.getValue() : minutoSalida.getValue();
                Date parsedDateIn = dateFormat.parse("2020-03-12 " + vHoraEntrada + ":" + vMinutoEntrada + ":00");
                Date parsedDateOut = null;
                if (vHoraSalida.equalsIgnoreCase("12")) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.set(Calendar.HOUR_OF_DAY, 12);
                    calendar.set(Calendar.MINUTE, Integer.parseInt(vMinutoSalida));
                    calendar.set(Calendar.SECOND, 00);
                    parsedDateOut = calendar.getTime();
                } else {
                    parsedDateOut = dateFormat.parse("2020-03-12 " + vHoraSalida + ":" + vMinutoSalida + ":00");
                }
                HorarioFuncionario hf = hfDao.getById(marcacion.getHorarioFuncionario().getIdreloj());

                if (isWeekend(DateUtils.asLocalDate(marcacion.getFecha()))) {
                    HorarioLaboral hl = hlDao.getByValue("sabado");
                    hf.setHorarioLaboral(hl);
                }

                marcacion.setInmarcada(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateIn)));
                marcacion.setOutmarcada(Timestamp.valueOf(DateUtils.asLocalDateTime(parsedDateOut)));

                long minutesEntSal = 0;
                try {
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } catch (Exception e) {
                    marcacion.setOutmarcada(marcacion.getInmarcada());
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                } finally {
                }

                marcacion.setMintrabajada(minutesEntSal - 40);
//                if (marcacion.getInmarcada().before(marcacion.getInasignada())) {
                if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) < (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    marcacion.setMinllegadatardia(0L);
                    //                } else if (marcacion.getInmarcada().after(marcacion.getInasignada())) {
                } else if ((marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) > (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes())) {
                    marcacion.setMinllegadatemprana(0L);
                    //marcacion.setMinllegadatardia(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInmarcada().getTime() - marcacion.getInasignada().getTime()));
                    int difHere = (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes()) - (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes());
                    marcacion.setMinllegadatardia(Long.parseLong(difHere + ""));
//                si llega tarde su diferencia de marcacion es en base a la hora que marco no a la que tiene asignada (07:20)
                    //minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - marcacion.getInmarcada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                } else {
//                    marcacion.setMinllegadatemprana(TimeUnit.MILLISECONDS.toMinutes(marcacion.getInasignada().getTime() - marcacion.getInmarcada().getTime()));
                    int marc = (marcacion.getInasignada().getHours() * 60 + marcacion.getInasignada().getMinutes()) - (marcacion.getInmarcada().getHours() * 60 + marcacion.getInmarcada().getMinutes());
                    marcacion.setMinllegadatemprana(Long.valueOf(marc + ""));
                    marcacion.setMinllegadatardia(0L);

//                    minutesEntSal = TimeUnit.MILLISECONDS.toMinutes(marcacion.getOutmarcada().getTime() - hf.getHorarioLaboral().getEntrada().getTime());
                    minutesEntSal = (marcacion.getOutmarcada().getHours() * 60 + marcacion.getOutmarcada().getMinutes()) - (hf.getHorarioLaboral().getEntrada().getHours() * 60 + hf.getHorarioLaboral().getEntrada().getMinutes());
                    marcacion.setMintrabajada(minutesEntSal - 40);
                }
            } catch (Exception e) {
                marcacion.setInmarcada(null);
                marcacion.setOutmarcada(null);
            } finally {
            }
            marcacionDao.guardar(marcacion);
            setVisible(false);
            guardarListener.accept(marcacion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(marcacion);
        }
    }

}
