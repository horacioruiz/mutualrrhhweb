package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 7/6/2016.
 */
public class MotivosForm extends FormLayout {

    Binder<Motivos> binder = new Binder<>(Motivos.class);
    Motivos motivo;
    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";

    public void setCargo(Motivos c) {
        try {
            this.motivo = c;
            binder.setBean(c);
            delete.setVisible((motivo.getId() != null));
            setVisible(true);
            descripcion.selectAll();
            comboBoxFormulario.setValue(c.getFormulario());
            this.viejo = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    TextField descripcion = new TextField("Descripción", "");
    ComboBox<Formulario> comboBoxFormulario = new ComboBox<>("Formulario");
    CheckBox habilitado = new CheckBox("Habilitado");

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    Button delete = new Button("Borrar");

    Binder<Motivos> fieldGroup = new Binder<>(Motivos.class);
    MotivosDao controller = ResourceLocator.locate(MotivosDao.class);
    FormularioDao formulariosController = ResourceLocator.locate(FormularioDao.class);

    private Consumer<Motivos> saveListener;
    private Consumer<Motivos> deleteListener;
    private Consumer<Motivos> cancelListener;
    Motivos motivos;

    public MotivosForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();

            save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            cancel.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            //VerticalLayout verticalLayout = createVerticalLayout();
            descripcion.setWidth("100%");
            //layout.addComponent(descripcion);
            //layout.addComponent(email);

            CssLayout expander1 = new CssLayout();
            expander1.setSizeFull();
            expander1.setStyleName("expander");

            HorizontalLayout hl = new HorizontalLayout();
//            layout.addComponent(expander1);
//            layout.setExpandRatio(expander1, 1.0F);
//            CssLayout expander = new CssLayout();
//            expander.setSizeFull();
//            expander.setStyleName("expander");
//            layout.addComponent(expander);
//            layout.setExpandRatio(expander, 1.0F);
            //layout.addComponent(habilitado);
            HorizontalLayout horizontalLayout = new HorizontalLayout();

            horizontalLayout.addComponent(save);
            horizontalLayout.addComponent(delete);
            horizontalLayout.addComponent(cancel);

            addComponents(descripcion, comboBoxFormulario, habilitado, expander1, hl, horizontalLayout);

            binder.bind(descripcion, Motivos::getDescripcion, Motivos::setDescripcion);
            binder.forField(descripcion).withNullRepresentation("")
                    .bind(Motivos::getDescripcion, Motivos::setDescripcion);
            binder.bindInstanceFields(this);

            descripcion.addValueChangeListener(m -> {
                descripcion.setValue(descripcion.getValue().toUpperCase());
            });

            comboBoxFormulario.setItems(formulariosController.listaFormulario());
            comboBoxFormulario.setItemCaptionGenerator(Formulario::getDescripcion);

            comboBoxFormulario.addValueChangeListener(e -> validate());
            descripcion.addValueChangeListener(e -> validate());

            descripcion.addBlurListener(e -> {
                validate();
            });

//            fieldGroup.bind(descripcion, Usuarios.USERNAME);
//            fieldGroup.bind(password, Usuarios.PASSWORD);
//            fieldGroup.bind(email, Usuarios.EMAIL);
//            fieldGroup.bind(administradorCheck, Usuarios.ADMINISTRADOR);
//            fieldGroup.bind(clienteCheck, Usuarios.CLIENTES);
//            fieldGroup.bind(helpdeskCheck, Usuarios.HELPDESK);
//            fieldGroup.bind(habilitado, Usuarios.ENABLED);
//            fieldGroup.bind(comboBoxFormulario,
//                    (Usuarios source) -> source.getFormulario(),
//                    (Usuarios bean, Formulario fieldvalue) -> {
//                        bean.setFormulario(fieldvalue);
//                    });
            save.addClickListener(cl -> {
                if (validateOpcion()) {
                    save();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            cancel.addClickListener(cl -> {
                try {
                    cancelListener.accept(motivos);
                    showForm(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            delete.addClickListener(cl -> {
                borrar();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void borrar() {
        try {
            controller.borrar(motivo);
            setVisible(false);
            deleteListener.accept(motivo);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            deleteListener.accept(motivo);
        }
    }

    private void save() {
        try {
            motivo.setDescripcion(descripcion.getValue());
            motivo.setFormulario(comboBoxFormulario.getValue());
            motivo.setEstado(habilitado.getValue());
            controller.guardarMotivos(motivo);
            setVisible(false);
            saveListener.accept(motivo);
        } catch (Exception ex) {
            Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
            saveListener.accept(motivo);
            Logger.getLogger(FormularioForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void validate() {
        boolean savedEnabled = true;
        if (descripcion == null || descripcion.isEmpty()) {
            savedEnabled = false;
        }
        if (comboBoxFormulario == null || comboBoxFormulario.isEmpty()) {
            savedEnabled = false;
        }
        save.setEnabled(savedEnabled);
    }

    private void showForm(boolean show) {
//        if (show) {
//            addStyleName("visible");
//        } else {
//            removeStyleName("visible");
//        }
//
//        setEnabled(show);
    }

//    public void edit(Usuarios usuario) {
//        this.motivos = usuario;
//        showForm(true);
//        setVisible(true);
//        delete.setEnabled(usuario != null);
//        fieldGroup.setBean(motivos);
//        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
//        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
//        validate();
//    }
    public void setSaveListener(Consumer<Motivos> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Motivos> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Motivos> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public void edit(Motivos motivos) {

    }

    public void setViejo() {
        if (motivo.getId() != null) {
            this.viejo = "(" + motivo.getId() + "," + motivo.getDescripcion() + ")";
        }
    }

    private boolean validateOpcion() {
        boolean savedEnabled = true;
        if (descripcion == null || descripcion.isEmpty()) {
            savedEnabled = false;
        }
        if (comboBoxFormulario == null || comboBoxFormulario.isEmpty()) {
            savedEnabled = false;
        }

        return savedEnabled;
    }
}
