/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.RequisitoDao;
import py.mutualmsp.mutualweb.dao.TiporequisitoDao;
import py.mutualmsp.mutualweb.entities.Requisito;
import py.mutualmsp.mutualweb.entities.Tiporequisito;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class RequisitoForm extends FormLayout{
    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    ComboBox<Tiporequisito> cmbTiporequisito = new ComboBox<>("Tipo Requisito");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    RequisitoDao requisitoDao = ResourceLocator.locate(RequisitoDao.class);
    TiporequisitoDao tiporequisitoDao = ResourceLocator.locate(TiporequisitoDao.class);
    Binder<Requisito> binder = new Binder<>(Requisito.class);
    
    Requisito requisito;
    
    private Consumer<Requisito> guardarListener;
    private Consumer<Requisito> borrarListener;
    private Consumer<Requisito> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "requisito";
            
    public RequisitoForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            cmbTiporequisito.setWidth("101%");
            
            addComponents(txtfDescripcion, cmbTiporequisito, botones);
            
            binder.bind(txtfDescripcion, Requisito::getDescripcion, Requisito::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Requisito::getDescripcion, Requisito::setDescripcion);
            binder.bindInstanceFields(this);
            
            List<Tiporequisito> listaTiporequisitos = tiporequisitoDao.listaTiporequisito();
            cmbTiporequisito.setItems(listaTiporequisitos);
            cmbTiporequisito.setItemCaptionGenerator(Tiporequisito::getDescripcion);
            binder.bind(cmbTiporequisito, 
                    (Requisito source) -> source.getIdtiporequisito(),
                    (Requisito bean, Tiporequisito fieldvalue) -> {
                        bean.setIdtiporequisito(fieldvalue);});
            
            txtfDescripcion.addValueChangeListener(m -> {txtfDescripcion.setValue(txtfDescripcion.getValue().toUpperCase());});
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(requisito);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            List<Requisito> lista = new ArrayList<>();
            lista = requisitoDao.getListRequisitoExistente(txtfDescripcion.getValue());
            if (lista.isEmpty() || lista.size()==1) {
                requisitoDao.guardar(requisito);
                setVisible(false);
                guardarListener.accept(requisito);
                Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                if(viejo.equals("")) operacion="I";
                else operacion="M";
                this.setNuevo();
                AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } else {
                Notification.show("Advertencia", "Requisito duplicado. No se pudo guardar", Notification.Type.ERROR_MESSAGE);
                guardarListener.accept(requisito);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(requisito);
        }
    }
    
    private void borrar() {
        try {
            requisitoDao.borrar(requisito);
            setVisible(false);
            borrarListener.accept(requisito);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(requisito);
        }
    }
    
    public void setGuardarListener(Consumer<Requisito> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Requisito> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Requisito> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(requisito.getIdrequisito()!=null) this.viejo = "("+requisito.getIdrequisito()+","+requisito.getDescripcion()+","+requisito.getIdtiporequisito().getIdtiporequisito()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+requisito.getIdrequisito()+","+requisito.getDescripcion()+","+requisito.getIdtiporequisito().getIdtiporequisito()+")";
    }

    public void setRequisito(Requisito r) {
        try {
            this.requisito = r;
            binder.setBean(r);
            btnBorrar.setVisible((requisito.getIdrequisito()!=null));
            setVisible(true);
            txtfDescripcion.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
