/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.SolicitudView.isWeekendSunday;

/**
 *
 * @author Dbarreto
 */
public class SuspencionForm extends FormLayout {

    private DateField fechaInicio = new DateField("Fecha Inicio");
    private DateField fechaFin = new DateField("Fecha Fin");
    ComboBox<Parametro> filterParametro = new ComboBox<>("Suspención");
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>("Funcionario");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    List<Marcaciones> listMarcation = new ArrayList<>();

    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    ParametroDao parametroDao = ResourceLocator.locate(ParametroDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    Suspenciones suspencion;

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    MarcacionesDao marcacionDao = ResourceLocator.locate(MarcacionesDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);

    private Consumer<Suspenciones> guardarListener;
    private Consumer<Suspenciones> borrarListener;
    private Consumer<Suspenciones> cancelarListener;

    private Date viejoInicio = new Date();
    private Date viejoFin = new Date();
    private Funcionario func = new Funcionario();
    private String param = "";
    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "suspencion";
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public SuspencionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                    if (listFunc.isEmpty()) {
                        listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                    }else{
                        listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                    } 
            }
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            fechaInicio.setValue(DateUtils.asLocalDate(new Date()));
            fechaFin.setValue(DateUtils.asLocalDate(new Date()));
            filterParametro.setPlaceholder("Seleccione motivo");
            List<Parametro> listParametro = parametroDao.listarPorTipoCodigo("suspencion");
            filterParametro.setItems(listParametro);
            filterParametro.setItemCaptionGenerator(Parametro::getDescripcion);
            filterParametro.setValue(listParametro.get(0));

            filterFuncionario.setPlaceholder("Seleccione funcionario");
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                filterFuncionario.setItems(funcionarioDao.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioDao.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

            addComponents(fechaInicio, fechaFin, filterFuncionario, filterParametro, botones);

//            binder.forField(txtfDescripcion).withNullRepresentation("")
//                    .bind(Suspenciones::getDescripcion, Suspenciones::setDescripcion);
//            binder.bindInstanceFields(this);
            btnGuardar.addClickListener(e -> {
                if (validate()) {
//                    List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fecha-.getValue()), DateUtils.asDate(fecha.getValue()));
//                    if (listFeriado.size() > 0) {
//                        Notification.show("Mensaje del Sistema", "No se crean suspenciones en días feriados.", Notification.Type.HUMANIZED_MESSAGE);
//                    } else {
                    guardar();
//                    }
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(suspencion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {

            Calendar start = Calendar.getInstance();
            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

            Calendar end = Calendar.getInstance();
            end.setTime(DateUtils.asDate(fechaFin.getValue()));

            try {
                suspencion.setParametro(filterParametro.getValue());
                suspencion.setDescripcion(filterParametro.getValue().getDescripcion());
            } catch (Exception ex) {
                suspencion.setParametro(null);
                suspencion.setDescripcion("");
            } finally {
            }
            suspencion.setFuncionario(filterFuncionario.getValue());
            suspencion.setNombrefuncionario(filterFuncionario.getValue().getNombreCompleto());
            suspencion.setAreafunc(filterFuncionario.getValue().getCargo().getDescripcion());
            suspencion.setCargofunc(filterFuncionario.getValue().getDependencia().getDescripcion());
            try {
                suspencion.setFechadesde(DateUtils.asDate(fechaInicio.getValue()));
            } catch (Exception ex) {
                suspencion.setFechadesde(null);
            } finally {
            }
            try {
                suspencion.setFechahasta(DateUtils.asDate(fechaFin.getValue()));
            } catch (Exception ex) {
                suspencion.setFechahasta(null);
            } finally {
            }
            suspencionDao.guardarSuspenciones(suspencion);
//                if (targetDay.compareTo(DateUtils.asDate(fechaFin.getValue())) == 0) {
            setVisible(false);
            guardarListener.accept(suspencion);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
//                }
//            while (!start.after(end)) {
//                Date targetDay = start.getTime();
//                suspencion.setId(null);
//                // Do Work Here
////                if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
//                
//
//                listMarcation = marcacionDao.getByFilter(suspencion.getFuncionario(), suspencion.getFecha(), suspencion.getFecha(), null, 0);
////                    List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(suspencion.getFecha(), suspencion.getFecha());
////                    if (listFeriado.size() > 0) {
////                        Notification.show("Mensaje del Sistema", "No se asignan suspenciones a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
////                    } else {
//                suspencionDao.guardarSuspenciones(suspencion);
//                if (targetDay.compareTo(DateUtils.asDate(fechaFin.getValue())) == 0) {
//                    setVisible(false);
//                    guardarListener.accept(suspencion);
//                    Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
//                }
//
////                    }
////                }
//                start.add(Calendar.DATE, 1);
//            }
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(suspencion);
        }
    }

    private void borrar() {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar? hay marcaciones asociadas que serán eliminadas si aprueba.", "40%");
        confirmMessage.getOkButton().addClickListener(e -> {
            try {
                suspencionDao.borrar(suspencion);
                listMarcation = marcacionDao.getByFilter(suspencion.getFuncionario(), suspencion.getFechadesde(), suspencion.getFechahasta(), null, 0);
                List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(suspencion.getFechadesde(), suspencion.getFechahasta());
//                if (listFeriado.size() > 0) {
//                    Notification.show("Mensaje del Sistema", "No se asignan suspenciones a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
//                } else {
//                    Thread t = new Thread(rRemove);aa11
                // Lets run Thread in background..
                // Sometimes you need to run thread in background for your Timer application..
//                    t.start();aa11
//                }
                setVisible(false);
                borrarListener.accept(suspencion);
                Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
                operacion = "E";
                nuevo = "";
                //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } catch (Exception ec) {
                ec.printStackTrace();
                Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
                borrarListener.accept(suspencion);
            }
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });
    }

    public void setGuardarListener(Consumer<Suspenciones> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Suspenciones> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Suspenciones> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(Suspenciones value) {
        try {
            if (value.getId() != null) {
                this.viejoInicio = value.getFechadesde();
                this.viejoFin = value.getFechahasta();
                this.func = value.getFuncionario();
                this.param = value.getParametro().getDescripcion();
                btnGuardar.setVisible(false);
            } else {
                fechaInicio.setValue(DateUtils.asLocalDate(new Date()));
                fechaFin.setValue(DateUtils.asLocalDate(new Date()));
                filterParametro.setValue(null);
                btnGuardar.setVisible(true);
            }
        } catch (Exception e) {
            fechaInicio.setValue(DateUtils.asLocalDate(new Date()));
            fechaFin.setValue(DateUtils.asLocalDate(new Date()));
            filterParametro.setValue(null);
            btnGuardar.setVisible(true);
        } finally {
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + suspencion.getId() + "," + suspencion.getFechadesde() + ")";
    }

    public void setSuspenciones(Suspenciones c) {
        try {
            this.suspencion = c;
            if (c.getId() != null) {
                fechaInicio.setValue(DateUtils.asLocalDate(c.getFechadesde()));
                fechaFin.setValue(DateUtils.asLocalDate(c.getFechahasta()));
                filterParametro.setValue(c.getParametro());
                filterFuncionario.setValue(c.getFuncionario());
            } else {
                filterParametro.setValue(null);
                filterFuncionario.setValue(null);
            }
            btnBorrar.setVisible((suspencion.getId() != null));
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (filterParametro == null || filterParametro.isEmpty()) {
            savedEnabled = false;
        }
        if (filterFuncionario == null || filterFuncionario.isEmpty()) {
            savedEnabled = false;
        }
        if (fechaInicio == null || fechaInicio.isEmpty()) {
            savedEnabled = false;
        }
        if (fechaFin == null || fechaFin.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    Runnable r = new Runnable() {
        public void run() {
            if (listMarcation.isEmpty()) {
                guardarMarcacion(suspencion.getFechadesde());
            } else {
                actualizarMarcacion(listMarcation, suspencion);
            }
        }

        private void guardarMarcacion(Date fecha) {
            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
//            for (HorarioFuncionario hf : hfDao.listadeHorarioFuncionarioTrue()) {
            HorarioFuncionario hf = hfDao.listarPorIdFuncionario(suspencion.getFuncionario().getId());
            try {
                Calendar start = Calendar.getInstance();
                start.setTime(fecha);

                Calendar end = Calendar.getInstance();
                end.setTime(fecha);

                while (!start.after(end)) {
                    Date targetDay = start.getTime();
                    // Do Work Here
                    if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
                        Marcaciones marcacion = new Marcaciones();
                        marcacion.setHorarioFuncionario(hf);

                        marcacion.setFecha(targetDay);
                        marcacion.setInasignada(tsTolerancia);
                        marcacion.setOutasignada(tsTolerancia);
                        marcacion.setInmarcada(tsTolerancia);
                        marcacion.setOutmarcada(tsTolerancia);
                        marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());

                        marcacion.setMintrabajada(0l);
                        marcacion.setMinllegadatemprana(0L);
                        marcacion.setMinllegadatardia(0L);
                        marcacion.setSolicitud(null);
                        marcacion.setObservacion(suspencion.getDescripcion().toUpperCase());
                        marcacionDao.guardar(marcacion);
                    }
                    start.add(Calendar.DATE, 1);
                }
            } catch (Exception exp) {
            } finally {
            }
        }
//        private void guardarMarcacion(Date fecha) {
//            Timestamp tsTolerancia = Timestamp.valueOf("1966-08-30 00:00:00");
//            for (HorarioFuncionario hf : hfDao.listadeHorarioFuncionarioTrue()) {
//                try {
//                    Calendar start = Calendar.getInstance();
//                    start.setTime(fecha);
//
//                    Calendar end = Calendar.getInstance();
//                    end.setTime(fecha);
//
//                    while (!start.after(end)) {
//                        Date targetDay = start.getTime();
//                        // Do Work Here
//                        if (!isWeekendSunday(DateUtils.asLocalDate(targetDay))) {
//                            Marcaciones marcacion = new Marcaciones();
//                            marcacion.setHorarioFuncionario(hf);
//
//                            marcacion.setFecha(targetDay);
//                            marcacion.setInasignada(tsTolerancia);
//                            marcacion.setOutasignada(tsTolerancia);
//                            marcacion.setInmarcada(tsTolerancia);
//                            marcacion.setOutmarcada(tsTolerancia);
//                            marcacion.setNombrefunc(hf.getFuncionario().getNombreCompleto());
//
//                            marcacion.setMintrabajada(0l);
//                            marcacion.setMinllegadatemprana(0L);
//                            marcacion.setMinllegadatardia(0L);
//                            marcacion.setSolicitud(null);
//                            marcacion.setObservacion(suspencion.getDescripcion().toUpperCase());
//                            marcacionDao.guardar(marcacion);
//                        }
//                        start.add(Calendar.DATE, 1);
//                    }
//                } catch (Exception exp) {
//                } finally {
//                }
//            }
//        }

        private void actualizarMarcacion(List<Marcaciones> listMarcation, Suspenciones suspencion) {
            for (Marcaciones m : listMarcation) {
                try {
                    m.setFecha(suspencion.getFechadesde());
                    String obs = m.getObservacion() == null || m.getObservacion().equalsIgnoreCase("") ? suspencion.getParametro().getDescripcion().toUpperCase() : m.getObservacion() + " - " + suspencion.getParametro().getDescripcion().toUpperCase();
                    m.setObservacion(obs);
                    marcacionDao.guardar(m);
                } catch (Exception exp) {
                } finally {
                }
            }
        }
    };
    Runnable rRemove = new Runnable() {
        public void run() {
            actualizarMarcacion(listMarcation, suspencion);
        }

        private void actualizarMarcacion(List<Marcaciones> listMarcation, Suspenciones suspencion) {
            suspencionDao.borrar(suspencion);
            Timestamp tsSuspencion = Timestamp.valueOf("1966-08-30 00:00:00");
            for (Marcaciones m : listMarcation) {
                try {
                    System.out.println("SUMA -> " + (m.getInmarcada().getHours() + m.getInmarcada().getMinutes())
                            + " - " + (tsSuspencion.getHours() + tsSuspencion.getMinutes()));
                    if ((m.getInmarcada().getHours() + m.getInmarcada().getMinutes()) == (tsSuspencion.getHours() + tsSuspencion.getMinutes()) && (m.getOutmarcada().getHours() + m.getOutmarcada().getMinutes()) == (tsSuspencion.getHours() + tsSuspencion.getMinutes())) {
                        marcacionDao.borrar(m);
                    } else {
                        m.setFecha(suspencion.getFechadesde());
                        String obs = m.getObservacion();
                        try {
                            if (obs.matches(".*- " + suspencion.getDescripcion().toUpperCase() + ".*")) {
                                obs = obs.replaceFirst(" - " + suspencion.getDescripcion().toUpperCase(), "");
                            } else {
                                obs = obs.replaceFirst(suspencion.getDescripcion().toUpperCase(), "");
                            }
                        } catch (Exception e) {
                            obs = obs.replaceFirst(suspencion.getDescripcion().toUpperCase(), "");
                        } finally {
                        }
                        m.setObservacion(obs);
                        marcacionDao.guardar(m);
                    }
                } catch (Exception e) {
                } finally {
                }
            }
        }
    };

}
