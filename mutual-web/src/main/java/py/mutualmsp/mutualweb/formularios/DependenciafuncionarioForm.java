package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 7/6/2016.
 */
public class DependenciafuncionarioForm extends FormLayout {

    Binder<Funcionario> binder = new Binder<>(Funcionario.class);
    Funcionario motivo;
    private String viejo = "";
    private String nuevo = "";
    private String operacion = "";
    long idFuncionario = 0l;
    Dependencia dependencia = new Dependencia();
    Cargo cargo = new Cargo();

    public void setCargo(Funcionario c) {
        try {
            this.motivo = c;
            binder.setBean(c);
            delete.setVisible((motivo.getId() != null));
            descripcion.setValue(c.getNombreCompleto());
            descripcion.setEnabled(false);
            try {
//                dependencia = dptoDao.getDependenciaById(c.getDependencia().getIddependencia());
                dependencia = c.getDependencia();
            } catch (Exception ex) {
                dependencia = null;
            } finally {
            }
            try {
                cargo = c.getCargo();
            } catch (Exception ex) {
                cargo = null;
            } finally {
            }
            cbCargo.setValue(cargo);
            cbDpto.setValue(dependencia);
//            cbCargo.setValue(dependenciaHijo);
            setVisible(true);
//            showForm(true);
            descripcion.selectAll();
//            comboBoxFormulario.setValue(c.getFormulario());
            this.viejo = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    TextField descripcion = new TextField("Funcionario/a");
    private ComboBox<Dependencia> cbDpto = new ComboBox<>("Dependencia");
    private ComboBox<Cargo> cbCargo = new ComboBox<>("Cargo");
    CheckBox habilitado = new CheckBox("Habilitado");

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);

    Button save = new Button("Guardar");
    Button cancel = new Button("Cancelar");
    Button delete = new Button("Borrar");

    Binder<Funcionario> fieldGroup = new Binder<>(Funcionario.class);
    FuncionarioDao controller = ResourceLocator.locate(FuncionarioDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    FormularioDao formulariosController = ResourceLocator.locate(FormularioDao.class);

    private Consumer<Funcionario> saveListener;
    private Consumer<Funcionario> deleteListener;
    private Consumer<Funcionario> cancelListener;
    Funcionario motivos;

    public DependenciafuncionarioForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();

            save.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
//            delete.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            cancel.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            //VerticalLayout verticalLayout = createVerticalLayout();
            descripcion.setWidth("100%");
            //layout.addComponent(descripcion);
            //layout.addComponent(email);

//            descripcion.setPlaceholder("");
            CssLayout expander1 = new CssLayout();
            expander1.setSizeFull();
            expander1.setStyleName("expander");

            HorizontalLayout hl = new HorizontalLayout();
//            layout.addComponent(expander1);
//            layout.setExpandRatio(expander1, 1.0F);
//            CssLayout expander = new CssLayout();
//            expander.setSizeFull();
//            expander.setStyleName("expander");
//            layout.addComponent(expander);
//            layout.setExpandRatio(expander, 1.0F);
            //layout.addComponent(habilitado);
            HorizontalLayout horizontalLayout = new HorizontalLayout();

            horizontalLayout.addComponent(save);
//            horizontalLayout.addComponent(delete);
            horizontalLayout.addComponent(cancel);

            addComponents(descripcion, cbDpto, cbCargo, expander1, hl, horizontalLayout);

//            binder.bind(descripcion, Funcionario::getDescripcion, Funcionario::setDescripcion);
//            binder.forField(descripcion).withNullRepresentation("")
//                    .bind(Funcionario::getDescripcion, Funcionario::setDescripcion);
//            binder.bindInstanceFields(this);
//            descripcion.addValueChangeListener(m -> {
//                descripcion.setValue(descripcion.getValue().toUpperCase());
//            });
            cbDpto.setItems(dptoDao.listaDependencia());
            cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);

            cbCargo.setItems(cargoDao.listaCargo());
            cbCargo.setItemCaptionGenerator(Cargo::getDescripcion);

//            cbDpto.addValueChangeListener(e -> validate());
//            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));
//            descripcion.addValueChangeListener(e -> validate());

            descripcion.addBlurListener(e -> {
                validate();
            });

//            fieldGroup.bind(descripcion, Usuarios.USERNAME);
//            fieldGroup.bind(password, Usuarios.PASSWORD);
//            fieldGroup.bind(email, Usuarios.EMAIL);
//            fieldGroup.bind(administradorCheck, Usuarios.ADMINISTRADOR);
//            fieldGroup.bind(clienteCheck, Usuarios.CLIENTES);
//            fieldGroup.bind(helpdeskCheck, Usuarios.HELPDESK);
//            fieldGroup.bind(habilitado, Usuarios.ENABLED);
//            fieldGroup.bind(comboBoxFormulario,
//                    (Usuarios source) -> source.getFormulario(),
//                    (Usuarios bean, Formulario fieldvalue) -> {
//                        bean.setFormulario(fieldvalue);
//                    });
            save.addClickListener(cl -> {
                if (validateOpcion()) {
                    save();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            cancel.addClickListener(cl -> {
                try {
                    cancelListener.accept(motivos);
                    showForm(false);
                    setVisible(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            delete.addClickListener(cl -> {
                borrar();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void borrar() {
        try {
            controller.borrar(motivo);
            setVisible(false);
            deleteListener.accept(motivo);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion = "E";
            nuevo = "";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            deleteListener.accept(motivo);
        }
    }

    private void save() {
        try {
            if (validateOpcion()) {
                //                buscarDependenciaHijoConPadre();
//                this.motivo.setDependencia(dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion()));
                this.motivo.setDependencia(dptoDao.getDependenciaByDescripcionDependencia(cbDpto.getValue().getDescripcion()));
//                this.motivo.setDependencia(dependencia);
                this.motivo.setCargo(cargoDao.getCargoByDescripcion(cbCargo.getValue().getDescripcion()));
                funcionarioDao.guardar(motivo);

                setVisible(false);
                saveListener.accept(motivo);
                Notification.show("Atención", "Datos actualizados correctamente", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
            }
//            motivo.setDescripcion(descripcion.getValue());
//            motivo.setFormulario(comboBoxFormulario.getValue());
//            motivo.setEstado(habilitado.getValue());
//            controller.guardarFuncionario(motivo);

        } catch (Exception ex) {
            saveListener.accept(motivo);
            Logger.getLogger(FormularioForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void validate() {
        boolean savedEnabled = true;
        if (descripcion == null || descripcion.isEmpty()) {
            savedEnabled = false;
        }
        if (cbDpto == null || cbDpto.isEmpty()) {
            savedEnabled = false;
        }
        if (cbCargo == null || cbCargo.isEmpty()) {
            savedEnabled = false;
        }
//        save.setEnabled(savedEnabled);
    }

    private void showForm(boolean show) {
//        if (show) {
//            addStyleName("visible");
//        } else {
//            removeStyleName("visible");
//        }

//        setEnabled(show);
    }

//    public void edit(Usuarios usuario) {
//        this.motivos = usuario;
//        showForm(true);
//        setVisible(true);
//        delete.setEnabled(usuario != null);
//        fieldGroup.setBean(motivos);
//        // !!! ??? Scroll to the top as this is not a Panel, using JavaScript
//        Page.getCurrent().getJavaScript().execute("window.document.getElementById('" + getId() + "').scrollTop = 0;");
//        validate();
//    }
    public void setSaveListener(Consumer<Funcionario> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Funcionario> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Funcionario> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public void edit(Funcionario motivos) {

    }

    public void setViejo() {
//        if (motivo.getId() != null) {
//            this.viejo = "(" + motivo.getId() + "," + motivo.getDescripcion() + ")";
//        }
    }

    private boolean validateOpcion() {
        boolean savedEnabled = true;
        if (descripcion == null || descripcion.isEmpty()) {
            savedEnabled = false;
        }
        if (cbDpto == null || cbDpto.isEmpty()) {
            savedEnabled = false;
        }
        if (cbCargo == null || cbCargo.isEmpty()) {
            savedEnabled = false;
        }
//        if (comboBoxFormulario == null || comboBoxFormulario.isEmpty()) {
//            savedEnabled = false;
//        }

        return savedEnabled;
    }

//    private void cargarSeccion(Dependencia dependencia) {
//        if (dependencia != null && cbDpto.getValue() != null) {
//            cbCargo.clear();
//            cbCargo.setItems(new ArrayList<>());
//            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
//            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
////            cbCargo.setPlaceholder("Búsqueda por Sección");
//            cbCargo.setVisible(true);
//
//            Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
//            List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getIddependencia());
//            List<Funcionario> listFunc = new ArrayList<>();
//            listFunc = funcionarioDao.ListarPorDependencia(depen.getIddependencia());
//            for (Dependencia dependencia1 : listDependencia) {
//                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getIddependencia()));
//            }
//        }
//    }
//    private void buscarDependenciaHijoConPadre() {
//        Dependencia dependencia = dptoDao.buscarPadreHijo(cbDpto.getValue(), cbCargo.getValue());
//        if (dependencia == null) {
////            dependencia = new Dependencia();
////            dependencia.set
//            Notification.show("Atención", "NO EXISTE EL DEPARTAMENTO Y LA SECCION ASOCIADA.!", Notification.Type.ERROR_MESSAGE);
//        } else {
//            this.motivo.setDependencia(dependencia);
//            funcionarioDao.guardar(motivo);
//        }
//    }
}
