/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author Dbarreto
 */
public class DependenciaForm extends FormLayout {

    TextField txtfDescripcion = new TextField("Descripción", "Ingrese descripción");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    DependenciaDao dependenciaDao = ResourceLocator.locate(DependenciaDao.class);
    Binder<Dependencia> binder = new Binder<>(Dependencia.class);

    Dependencia dependencia;

    private Consumer<Dependencia> guardarListener;
    private Consumer<Dependencia> borrarListener;
    private Consumer<Dependencia> cancelarListener;

    public DependenciaForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            addComponents(txtfDescripcion, botones);

            binder.bind(txtfDescripcion, Dependencia::getDescripcion, Dependencia::setDescripcion);
            binder.forField(txtfDescripcion).withNullRepresentation("")
                    .bind(Dependencia::getDescripcion, Dependencia::setDescripcion);
            binder.bindInstanceFields(this);

            btnGuardar.addClickListener(e -> {
                guardar();
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(dependencia);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            dependenciaDao.guardar(dependencia);
            setVisible(false);
            guardarListener.accept(dependencia);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(dependencia);
        }
    }

    private void borrar() {
        try {
            dependenciaDao.borrar(dependencia);
            setVisible(false);
            borrarListener.accept(dependencia);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(dependencia);
        }
    }

    public void setGuardarListener(Consumer<Dependencia> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Dependencia> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Dependencia> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setDependencia(Dependencia d) {
        try {
            this.dependencia = d;
            binder.setBean(d);
            btnBorrar.setVisible((dependencia.getId()!= null));
            setVisible(true);
            txtfDescripcion.selectAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
