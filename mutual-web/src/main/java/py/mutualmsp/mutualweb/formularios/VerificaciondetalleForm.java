/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.ListaDetalleVerificacionCache;
import py.mutualmsp.mutualweb.dao.RequisitoDao;
import py.mutualmsp.mutualweb.dao.VerificaciondetalleDao;
import py.mutualmsp.mutualweb.entities.Requisito;
import py.mutualmsp.mutualweb.entities.Verificaciondetalle;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;


/**
 *
 * @author Dbarreto
 */
public class VerificaciondetalleForm extends FormLayout{
    ComboBox<Requisito> cmbRequisito = new ComboBox<>("Requisito");
    CheckBox chkVerificado = new CheckBox("Verificado");
    TextField txtfObservacion = new TextField("Observación");
    
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    
    VerificaciondetalleDao verificaciondetalleDao = ResourceLocator.locate(VerificaciondetalleDao.class);
    ListaDetalleVerificacionCache cache = ResourceLocator.locate(ListaDetalleVerificacionCache.class);
    Binder<Verificaciondetalle> binder = new Binder<>(Verificaciondetalle.class);
    
    RequisitoDao requisitoDao = ResourceLocator.locate(RequisitoDao.class);
    
    Verificaciondetalle verificaciondetalle;
    
    private Consumer<Verificaciondetalle> guardarListener;
    private Consumer<Verificaciondetalle> borrarListener;
    private Consumer<Verificaciondetalle> cancelarListener;
            
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "verificaciondetalle";
    
    public VerificaciondetalleForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            List<Requisito> listaRequisitos = requisitoDao.listaRequisito();
            cmbRequisito.setItems(listaRequisitos);
            cmbRequisito.setItemCaptionGenerator(Requisito::getDescripcion);
            
            binder.bind(cmbRequisito, 
                    (Verificaciondetalle source) -> source.getIdrequisito(),
                    (Verificaciondetalle bean, Requisito fieldvalue) -> {
                    bean.setIdrequisito(fieldvalue);});
            
            chkVerificado.setValue(Boolean.TRUE);
            binder.bind(chkVerificado, Verificaciondetalle::getVerificado, Verificaciondetalle::setVerificado);
            
            binder.bind(txtfObservacion, Verificaciondetalle::getObservacion, Verificaciondetalle::setObservacion);
            binder.forField(txtfObservacion).withNullRepresentation("")
                    .bind(Verificaciondetalle::getObservacion, Verificaciondetalle::setObservacion);
            binder.bindInstanceFields(this);
            
            btnGuardar.addStyleName(ValoTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(ValoTheme.BUTTON_DANGER);
            
            addComponents(cmbRequisito, chkVerificado, txtfObservacion, botones);
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(verificaciondetalle);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            //verificaciondetalleDao.guardar(verificaciondetalle);
            cache.cargarDetalle(verificaciondetalle);
            setVisible(false);
            guardarListener.accept(verificaciondetalle);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(verificaciondetalle);
        }
    }
    
    private void borrar() {
        try {
            verificaciondetalleDao.borrar(verificaciondetalle);
            setVisible(false);
            borrarListener.accept(verificaciondetalle);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(verificaciondetalle);
        }
    }
    
    public void setGuardarListener(Consumer<Verificaciondetalle> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Verificaciondetalle> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Verificaciondetalle> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(){
        if(verificaciondetalle.getIdverificaciondetalle()!=null) this.viejo = "("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito().getIdrequisito()+","+
                                verificaciondetalle.getIdverificacion().getIdverificacion()+","+verificaciondetalle.getVerificado()+","+
                                verificaciondetalle.getObservacion()+","+verificaciondetalle.getIndiferente()+","+
                                verificaciondetalle.getNoverificado()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+verificaciondetalle.getIdverificaciondetalle()+","+verificaciondetalle.getIdrequisito().getIdrequisito()+","+
                                verificaciondetalle.getIdverificacion().getIdverificacion()+","+verificaciondetalle.getVerificado()+","+
                                verificaciondetalle.getObservacion()+","+verificaciondetalle.getIndiferente()+","+
                                verificaciondetalle.getNoverificado()+")";
    }

    public void setVerificaciondetalle(Verificaciondetalle vd) {
        try {
            this.verificaciondetalle = vd;
            binder.setBean(vd);
            btnBorrar.setVisible((verificaciondetalle.getIdverificaciondetalle()!=null));
            setVisible(true);
            this.viejo="";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
