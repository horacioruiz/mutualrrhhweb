package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FormularioDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.MotivosDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dao.SolicitudDao;
import py.mutualmsp.mutualweb.dao.SolicitudDetalleDao;
import py.mutualmsp.mutualweb.dao.SolicitudProduccionDetalleDao;
import py.mutualmsp.mutualweb.dao.SuspencionesDao;
import py.mutualmsp.mutualweb.dao.VacacionesDao;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Formulario;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Motivos;
import py.mutualmsp.mutualweb.entities.Parametro;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.entities.Solicitud;
import py.mutualmsp.mutualweb.entities.SolicitudDetalle;
import py.mutualmsp.mutualweb.entities.SolicitudProduccionDetalle;
import py.mutualmsp.mutualweb.entities.Suspenciones;
import py.mutualmsp.mutualweb.entities.Vacaciones;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 * Created by Alfre on 28/6/2016.
 */
public class SolicitudForm extends Window {

    private TextField txtCedulaFuncionario = new TextField("Ingrese cédula (*)");
    private TextField txtNombreFuncionario = new TextField("Funcionario");
    private TextField txtArea = new TextField("Area");
    private TextField txtCargo = new TextField("Cargo");
    private TextArea txtObservacion = new TextArea("Observación");
    private TextField txtCantdias = new TextField("Cantidad días");
    private DateField fechaInicio = new DateField("Fecha Inicio (*)");
    private DateField fechaFin = new DateField("Fecha Fin (*)");
    private ComboBox<String> prioridad = new ComboBox<>("Prioridad");
    ParametroDao parametroController = ResourceLocator.locate(ParametroDao.class);
    VacacionesDao vacacionesController = ResourceLocator.locate(VacacionesDao.class);
    boolean mostrarFechaAnterior = false;
    private int editar = 0;

    private ComboBox<Dependencia> cbDpto = new ComboBox<>("Departamento (*)");
    private ComboBox<Dependencia> cbCargo = new ComboBox<>("Sección (*)");

    private Button guardar = new Button("Guardar");
    private Button cancelar = new Button("Cancelar");
    private TextField costo = new TextField("Costo");
    private TextField horas = new TextField("Horas Trabajadas");
    private TextField tiempoRespuesta = new TextField("Tiempo de Respuesta");
    private TextField txtDependencia = new TextField("Dependencia");
    private ComboBox<Formulario> formulario = new ComboBox<>("Formulario (*)");
    private ComboBox<Vacaciones> vacacionesListado = new ComboBox<>("Periodo (*)");
    private TextField txtCantPeriodo = new TextField("Cant días disponible");
    SolicitudProduccionDetalle funcionarioSeleccionado = new SolicitudProduccionDetalle();

    Grid<SolicitudProduccionDetalle> gridFuncionario = new Grid<>(SolicitudProduccionDetalle.class);
    final FormLayout form = new FormLayout();
    final HorizontalLayout mainLayout = new HorizontalLayout();
    ComboBox<Funcionario> cbFuncionario = new ComboBox<>("Funcionario (*)");
    TextField txtAreaForm = new TextField("Area");

    ParametroDao parametrosController = ResourceLocator.locate(ParametroDao.class);
    long parametroVacaciones = 0;
    boolean booleanVacaciones = false;

    TextField txtCargoForm = new TextField("Cargo");
    TextField txtObservacionForm = new TextField("Descripción actividad (*)");
    DateTimeField fechaHoraForm = new DateTimeField("Fecha/Hora estipulada (*)");
    private Button btnAgregar = new Button("Agregar");

    private ComboBox<Motivos> motivo = new ComboBox<>("Motivos (*)");
    private ComboBox<Funcionario> encargado = new ComboBox<>("Encargado (*)");
//    private ComboBox<Funcionario> rrhh = new ComboBox<>("Funcionario RRHH");
//    private TextField txtOtroMotivo = new TextField("Otro motivo");
    private TextArea txtOtroMotivo = new TextArea("Otro motivo");
    ImageReceiver receiver = new ImageReceiver();
    Upload upload = upload = new Upload("Subir archivo", receiver);
    final Image image = new Image("Imagen");
    Label labelUrl = new Label();
    String fileName = "";
    String ubicacion = "";
    String filename;
    byte[] content;
    public File file;
    String url = "";

    private Consumer<Solicitud> saveListener;
    private Consumer<Solicitud> deleteListener;
    private Consumer<Solicitud> cancelListener;

    HashMap<Long, String> mapeo = new HashMap<>();
    SolicitudProduccionDetalle spd = new SolicitudProduccionDetalle();
    int numRowSelected = 0;

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    SuspencionesDao suspencionDao = ResourceLocator.locate(SuspencionesDao.class);
    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    VacacionesDao vacacionesDao = ResourceLocator.locate(VacacionesDao.class);
    FormularioDao formularioDao = ResourceLocator.locate(FormularioDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    MotivosDao motivosDao = ResourceLocator.locate(MotivosDao.class);
    SolicitudDao solicitudDao = ResourceLocator.locate(SolicitudDao.class);
    SolicitudDetalleDao solicitudDetalleDao = ResourceLocator.locate(SolicitudDetalleDao.class);
    SolicitudProduccionDetalleDao solicitudProduccionDetalleDao = ResourceLocator.locate(SolicitudProduccionDetalleDao.class);

    private boolean enviarCorreos = false;
    private Solicitud solicitud;
    String destinarariosString = "";
    TabSheet tabsheet = new TabSheet();
//    InlineDateTimeField sample = new InlineDateTimeField();
    DateTimeField fechaHoraInicio = new DateTimeField("Fecha Inicio (*)");
    DateTimeField fechaHoraFin = new DateTimeField("Fecha Fin (*)");

    List<SolicitudProduccionDetalle> listProduccionDetalle = new ArrayList<>();

    // Create upload stream
    FileOutputStream fos = null; // Stream to write to
    // Implement both receiver that saves upload in a file and
    // listener for successful upload

    private void updateFormulario() {
        if (cbFuncionario != null && cbFuncionario.getValue() != null) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(cbFuncionario.getValue().getCedula());
            txtAreaForm.setValue(func.getDependencia().getDescripcion());
            txtCargoForm.setValue(func.getCargo().getDescripcion());
            txtNombreFuncionario.setValue(func.getNombreCompleto());
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
        }
    }

    private void cargarGrilla() {
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        if (validateForm()) {
            boolean val = true;
            if (!mostrarFechaAnterior) {
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                Date fechaSolicitud = DateUtils.asDate(fechaHoraForm.getValue());
                fechaSolicitud.setHours(0);
                fechaSolicitud.setMinutes(0);
                fechaSolicitud.setSeconds(0);
                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                    val = false;
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    val = false;
                } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                    val = true;
                }
            }
            if (val) {
                SolicitudProduccionDetalle spd = new SolicitudProduccionDetalle();
                spd.setArea(txtAreaForm.getValue());
                spd.setCargo(txtCargoForm.getValue());
                spd.setDescripcion(txtObservacionForm.getValue());
                spd.setFuncionario(cbFuncionario.getValue());
                spd.setNombrefuncionario(txtNombreFuncionario.getValue());
                try {
                    spd.setHora(Timestamp.valueOf(fechaHoraForm.getValue()));
                } catch (Exception e) {
                    spd.setHora(null);
                } finally {
                }
                try {
                    spd.setFecha(DateUtils.asDate(fechaHoraForm.getValue()));
                } catch (Exception e) {
                    spd.setFecha(null);
                } finally {
                }
                listProduccionDetalle.add(spd);
                gridFuncionario.clearSortOrder();
                gridFuncionario.setCaption("Funcionarios agregados");
                gridFuncionario.setItems(listProduccionDetalle);
                gridFuncionario.removeAllColumns();
                gridFuncionario.addColumn(e -> {
                    return e.getNombrefuncionario();
                }).setCaption("NOMBRE Y APELLIDO");
                gridFuncionario.addColumn(e -> {
                    return e.getCargo();
                }).setCaption("CARGO");
                gridFuncionario.addColumn(e -> {
                    return e.getDescripcion();
                }).setCaption("ACTIVIDAD");
                gridFuncionario.addColumn(e -> {
                    return e.getHora() == null ? "--" : simpleDateFormatHora.format(e.getHora());
                }).setCaption("HORA ESTIPULADA");

                cbFuncionario.clear();
                cbFuncionario.setItems(new ArrayList<>());
//                Dependencia depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
//                Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
//                List<Dependencia> listDependencia = dptoDao.listarSubDependencia(depen.getId());
//                List<Funcionario> listFunc = new ArrayList<>();
//                listFunc = funcionarioDao.ListarPorDependencia(depen.getId());
                List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
                List<Dependencia> listDependencia = new ArrayList<>();
                List<Funcionario> listFunc = new ArrayList<>();
                for (Dependencia dependencia : depen) {
                    listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                        if (listFunc.isEmpty()) {
                            listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                        }else{
                            listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                        } 
                }
                for (Dependencia dependencia1 : listDependencia) {
                    listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
                }
                cbFuncionario.setItems(listFunc);
                cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

                txtNombreFuncionario.setValue("");
                txtAreaForm.setValue("");
                txtCargoForm.setValue("");
                txtObservacionForm.setValue("");
                SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                try {
                    String fecha = formatFec.format(new Date());
                    String fec = fecha + "T19:00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraForm.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraForm.setValue(null);
                } finally {
                }
//            gridFuncionario.addComponentColumn(this::buildConfirmButton).setCaption(" X ");
            }
        } else {
            Notification.show("Todos los campos son obligatorios",
                    Notification.Type.ERROR_MESSAGE);
        }
    }

    private boolean validateForm() {
        boolean savedEnabled = true;
        if (txtAreaForm == null || txtAreaForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtCargoForm == null || txtCargoForm.isEmpty()) {
            savedEnabled = false;
        }
        if (txtObservacionForm == null || txtObservacionForm.isEmpty()) {
            savedEnabled = false;
        }
        if (cbFuncionario == null || cbFuncionario.getValue() == null) {
            savedEnabled = false;
        }
        if (fechaHoraForm == null || fechaHoraForm.getValue() == null) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void guardarDetalleProduccion(Solicitud solicitud) {
        if (editar == 0) {
            if (listProduccionDetalle.size() > 0) {
                for (SolicitudProduccionDetalle solicitudProduccionDetalle : listProduccionDetalle) {
                    solicitudProduccionDetalle.setSolicitud(solicitud);
                        solicitudProduccionDetalleDao.guardarSolicitudProduccionDetalle(solicitudProduccionDetalle);
                }
            } else {
                Notification.show("Debe agregar detalles a la solicitud",
                        Notification.Type.ERROR_MESSAGE);
            }
        } else {
            if (listProduccionDetalle.size() > 0) {
                solicitudProduccionDetalleDao.listarPorIdSolicitud(solicitud.getId()).forEach((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalleDao.borrar(solicitudProduccionDetalle);
                });
                listProduccionDetalle.stream().map((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalle.setSolicitud(solicitud);
                    return solicitudProduccionDetalle;
                }).forEachOrdered((solicitudProduccionDetalle) -> {
                    solicitudProduccionDetalleDao.guardarSolicitudProduccionDetalle(solicitudProduccionDetalle);
                });
            } else {
                Notification.show("Debe agregar detalles a la solicitud",
                        Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void cargarSeccion(Dependencia dependencia) {
        if (dependencia != null && cbDpto.getValue() != null) {
            cbCargo.clear();
            cbCargo.setItems(new ArrayList<>());
//            cbCargo.setItems(dptoDao.listarSubDependencia(cbDpto.getValue().getIddependencia()));
            cbCargo.setItems(dptoDao.listaDependencia());
            cbCargo.setItemCaptionGenerator(Dependencia::getDescripcion);
            cbCargo.setVisible(true);

            //Dependencia depen = dptoDao.getDependenciaByDescripcion(cbDpto.getValue().getDescripcion().toLowerCase());
            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia1 : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia1.getId());
                    if (listFunc.isEmpty()) {
                        listFunc = funcionarioDao.ListarPorDependencia(dependencia1.getId());
                    }else{
                        listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
                    } 
            }
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            cbFuncionario.setItems(listFunc);
            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        }
    }

    private void cargarSubSeccion(Dependencia depen) {
        cbFuncionario.setItems(new ArrayList<>());
        if (cbCargo.getValue() != null) {
            List<Funcionario> listFunc = new ArrayList<>();
            listFunc = funcionarioDao.ListarPorDependencia(depen.getId());
//            listFunc.addAll(listFunc);

            cbFuncionario.setItems(listFunc);
            cbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
        }
    }

    private void editarSolicitudProduccion(Solicitud solicitud) {
        List<SolicitudProduccionDetalle> listSolicitudDetalle = solicitudProduccionDetalleDao.listarPorIdSolicitud(solicitud.getId());
        try {
            txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
        } catch (Exception e) {
            txtCedulaFuncionario.setValue("");
        } finally {
        }
        try {
            txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
        } catch (Exception e) {
            txtDependencia.setValue("");
        } finally {
        }
        try {
            txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
        } catch (Exception e) {
            txtNombreFuncionario.setValue("");
        } finally {
        }
        try {
            txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
        } catch (Exception e) {
            txtCargo.setValue("");
        } finally {
        }
        try {
            txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
        } catch (Exception e) {
            txtArea.setValue("");
        } finally {
        }
        try {
            formulario.setValue(solicitud.getFormulario());
            cargarPorFormulario(solicitud.getFormulario());
        } catch (Exception e) {
            formulario.setValue(null);
        } finally {
        }
        try {
            if (solicitud.getCantdia() == null) {
                txtCantdias.setValue("0");
            } else {
                txtCantdias.setValue(solicitud.getCantdia() + "");
            }
        } catch (Exception e) {
            txtCantdias.setValue("0");
        } finally {
        }
        try {
            txtObservacion.setValue(solicitud.getObservacion());
        } catch (Exception e) {
            txtObservacion.setValue("");
        } finally {
        }
        try {
            encargado.setValue(solicitud.getEncargado());
        } catch (Exception e) {
            encargado.setValue(null);
        } finally {
        }
        try {
            txtOtroMotivo.setValue(listSolicitudDetalle.get(0).getDescripcion());
        } catch (Exception e) {
            txtOtroMotivo.setValue("");
        } finally {
        }
        labelUrl.setValue("");
        motivo.setValue(null);
        txtDependencia.setVisible(false);
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);
        fechaHoraInicio.setVisible(false);

        fechaInicio.setValue(null);
        fechaFin.setValue(null);
        fechaHoraInicio.setValue(null);
        fechaHoraFin.setValue(null);

        fechaInicio.setVisible(false);
        fechaFin.setVisible(false);

        fechaHoraInicio.setVisible(false);
        fechaHoraFin.setVisible(false);
        txtDependencia.setVisible(false);

        fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
        upload.setVisible(false);
        txtOtroMotivo.setVisible(false);

        txtCedulaFuncionario.setVisible(false);
        txtNombreFuncionario.setVisible(false);
        txtArea.setVisible(false);
        txtCargo.setVisible(false);
        txtObservacion.setVisible(false);
        motivo.setVisible(false);
        gridFuncionario.clearSortOrder();
        mainLayout.setVisible(true);
        txtCantdias.setVisible(false);

        cbCargo.setItems(new ArrayList<>());
        cbCargo.setVisible(true);

        cbDpto.setItems(dptoDao.listarDepartamentosPadres());
        cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
        cbDpto.setVisible(true);
        //cbDpto.setValue(dptoDao.getDependenciaByDescripcion(solicitud.getAreafunc().toLowerCase()));
        dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
        cbCargo.setValue(dptoDao.getDependenciaByDescripcionCargo(solicitud.getCargofunc().toLowerCase()));

        mainLayout.setVisible(true);
        cargarDetalleProduccion(solicitud);
        try {
            String fecha = formatFec.format(solicitud.getFechafin());
            String hora = formatHor.format(solicitud.getHorafin());
            String fec = fecha + "T" + hora + ":00";
            LocalDateTime ldtFin = LocalDateTime.parse(fec);
            fechaHoraFin.setValue(ldtFin);
        } catch (Exception e) {
            fechaHoraFin.setValue(null);
        } finally {
        }
    }

    private void cargarDetalleProduccion(Solicitud solicitud) {
        SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
        String patternHora = "HH:mm";
        SimpleDateFormat simpleDateFormatHora = new SimpleDateFormat(patternHora);
        List<SolicitudProduccionDetalle> spd = solicitudProduccionDetalleDao.listarPorIdSolicitud(solicitud.getId());
        listProduccionDetalle = spd;
        gridFuncionario.clearSortOrder();
        gridFuncionario.setCaption("Funcionario/s agregado/s");
        gridFuncionario.setItems(listProduccionDetalle);
        gridFuncionario.removeAllColumns();
        gridFuncionario.addColumn(e -> {
            return e.getNombrefuncionario();
        }).setCaption("NOMBRE Y APELLIDO");
        gridFuncionario.addColumn(e -> {
            return e.getCargo();
        }).setCaption("CARGO");
        gridFuncionario.addColumn(e -> {
            return e.getDescripcion();
        }).setCaption("ACTIVIDAD");
        gridFuncionario.addColumn(e -> {
            return e.getFecha() == null ? "--" : formatFec.format(e.getFecha());
        }).setCaption("FECHA");
        gridFuncionario.addColumn(e -> {
            return e.getHora() == null ? "--" : simpleDateFormatHora.format(e.getHora());
        }).setCaption("HORA ESTIPULADA");
    }

    private long calcularCantDias() {
        try {
            long substract = calcWeekDays(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue())) + 1;
            if (isWeekendSunday(fechaFin.getValue())) {
                substract -= 1l;
            }

            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()));
            long cantFeriado = 0;
            for (Feriado feriado : listFeriado) {
                if (!isWeekend(DateUtils.asLocalDate(feriado.getFecha()))) {
                    cantFeriado++;
                }
            }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
            return (substract - cantFeriado);
        } catch (Exception e) {
            return 0l;
        } finally {
        }

    }

    private void cargarCantVacaciones(Vacaciones valor) {
        try {
            double val = (valor.getCantdiavaca() - valor.getCantdiatomada());
            try {
                String value = String.valueOf(val).replace(".0", "");
                txtCantPeriodo.setValue(value);
            } catch (Exception ex) {
                txtCantPeriodo.setValue(0 + "");
            }
        } catch (Exception e) {
            txtCantPeriodo.setValue(0 + "");
        }
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {

            try {
                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-web-rrhh/" + filename);
                url = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/" + filename;
                fileName = filename;
                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-web-rrhh/";
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                Logger.getLogger(SolicitudForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
            labelUrl.setValue(file.getName());
        }
    };

    public SolicitudForm() {
        try {
//            VerticalLayout layout = createForm();
            VerticalLayout layout = new VerticalLayout(tabsheet);
            setContent(layout);
            setWidth("90%");
            setHeight("85%");
            setCaption("Agregar Solicitud");
            //setWindowMode(WindowMode.MAXIMIZED);
            setModal(true);
            center();

            try {
                parametroVacaciones = Long.parseLong(parametrosController.listarPorCodigo("parametro_vacaciones").get(0).getValor());
            } catch (Exception e) {
                parametroVacaciones = 0L;
            } finally {
            }
            try {
                if (parametrosController.listarPorCodigo("cargar_vacaciones_menor").get(0).getValor().equalsIgnoreCase("SI")) {
                    booleanVacaciones = true;
                } else {
                    booleanVacaciones = false;
                }
            } catch (Exception e) {
            } finally {
            }
//            sample.setValue(LocalDateTime.now());
//            sample.setLocale(Locale.US);
//            sample.setResolution(DateTimeResolution.MINUTE);
            txtNombreFuncionario.setEnabled(false);
            txtArea.setEnabled(false);
            txtCantdias.setEnabled(false);
            txtCargo.setEnabled(false);
            fechaHoraInicio.setVisible(false);
            fechaHoraFin.setVisible(false);
            txtCantPeriodo.setEnabled(false);

            List<Parametro> listParame = parametroController.listarPorCodigo("cargar_fecha_anterior_solicitud");
            if (listParame.get(0).getValor().equalsIgnoreCase("SI")) {
                mostrarFechaAnterior = true;
            } else {
                mostrarFechaAnterior = false;
            }

            cbCargo.setVisible(false);
            cbDpto.setVisible(false);

            txtAreaForm.setEnabled(false);
            txtCargoForm.setEnabled(false);

            mainLayout.setVisible(false);

            txtCedulaFuncionario.addBlurListener(e -> {
                validate();
            });

            gridFuncionario.asSingleSelect().addValueChangeListener(event -> {
                if (event.getValue() != null) {
                    funcionarioSeleccionado = event.getValue();
                    cbFuncionario.setValue(funcionarioSeleccionado.getFuncionario());
                    txtObservacionForm.setValue(funcionarioSeleccionado.getDescripcion().toUpperCase());

                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                    try {
                        String fecha = formatFec.format(funcionarioSeleccionado.getFecha());
                        String hora = formatHor.format(funcionarioSeleccionado.getHora());
                        String fec = fecha + "T" + hora + ":00";
                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                        fechaHoraForm.setValue(ldtFin);
                    } catch (Exception e) {
                        fechaHoraForm.setValue(null);
                    } finally {
                    }
                }
            });

            formulario.setItems(formularioDao.listaFormulario());
            formulario.setItemCaptionGenerator(Formulario::getDescripcion);

//            rrhh.setItems(funcionarioDao.listaFuncionario());
//            rrhh.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setItems(funcionarioDao.listaFuncionario());
            encargado.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            encargado.setVisible(false);

            formulario.focus();

            gridFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.DELETE, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (numRowSelected >= 0) {
                        listProduccionDetalle.remove(numRowSelected);
                        gridFuncionario.clearSortOrder();
                        gridFuncionario.setItems(listProduccionDetalle);
                        numRowSelected = -1;
                        spd = new SolicitudProduccionDetalle();
                    }
                }
            });
            gridFuncionario.addItemClickListener(e -> {
                if (e.getItem() != null) {
                    spd = e.getItem();
                    numRowSelected = e.getRowIndex();
                }
            });
//            comentarios.addBlurListener(e -> {
//                validate();
//            });
//            txtCedulaFuncionario.addValueChangeListener(e -> {
//                if (e.getValue() != null && !e.getValue().equals("")) {
//                    validate();
//                }
//            });

            txtCedulaFuncionario.addValueChangeListener(e -> updateList(e.getValue()));
            formulario.addValueChangeListener(e -> cargarPorFormulario(e.getValue()));
            vacacionesListado.addValueChangeListener(e -> cargarCantVacaciones(e.getValue()));
            cbDpto.addValueChangeListener(e -> cargarSeccion(e.getValue()));
//            cbCargo.addValueChangeListener(e -> cargarSubSeccion(e.getValue()));

            txtCedulaFuncionario.addShortcutListener(new ShortcutListener("Shortcut", ShortcutAction.KeyCode.ENTER, null) {
                @Override
                public void handleAction(Object sender, Object target) {
                    // Do nice stuff
                    if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                        calcularCantDiaFuncionario();

                        try {
                            Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                                vacacionesListado.setItems(vacacionesController.listadeVacacionesON(func.getId()));
                                vacacionesListado.setItemCaptionGenerator(Vacaciones::getPeriodo);
                                vacacionesListado.setVisible(true);
                                txtCantPeriodo.setVisible(true);
                            } else {
                                vacacionesListado.setVisible(false);
                                txtCantPeriodo.setVisible(false);
                            }
                        } catch (Exception exp) {
                        } finally {
                        }
                    }
                }

                private void calcularCantDiaFuncionario() {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    try {
                        txtArea.setValue(func.getDependencia().getDescripcion());
                    } catch (Exception e) {
                        txtArea.setValue("");
                    } finally {
                    }
                    try {
                        txtCargo.setValue(func.getCargo().getDescripcion());
                    } catch (Exception e) {
                        txtCargo.setValue("");
                    } finally {
                    }
                    try {
                        txtNombreFuncionario.setValue(func.getNombreCompleto());
                    } catch (Exception e) {
                        txtNombreFuncionario.setValue("");
                    } finally {
                    }

                    long dif = calcularCantDias();
                    try {
                        if (func.getId() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getId());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        
                        ///Comentamos por ahora la rotacion
                        /*if (func.getId() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getId());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            //dif -= (cantSabados - listRotacion.size());aaaaaaaaaaaaa
                            dif -= (listRotacion.size());
                        }*/
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCantdias.setValue(dif + "");
                }
            });

            addCloseListener(closeEvent -> {
                close();
            });

            fechaInicio.addValueChangeListener(e -> updateFecha());
            fechaFin.addValueChangeListener(e -> updateFecha());
            cbFuncionario.addValueChangeListener(e -> updateFormulario());

            guardar.addClickListener(cl -> {
                try {
                    save();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
            btnAgregar.addClickListener(cl -> {
                try {
                    cargarGrilla();
                } catch (Exception e) {
                    e.printStackTrace();
                    Notification.show(e.getMessage());
                }
            });
//            guardar.setEnabled(false);
            cancelar.addClickListener(clickEvent -> {
                close();
            });

// Create tab content dynamically when tab is selected
            tabsheet.addSelectedTabChangeListener(
                    new TabSheet.SelectedTabChangeListener() {
                public void selectedTabChange(SelectedTabChangeEvent event) {
                    // Find the tabsheet
                    TabSheet tabsheet = event.getTabSheet();

                    // Find the tab (here we know it's a layout)
                    Layout tab = (Layout) tabsheet.getSelectedTab();

                    // Get the tab caption from the tab object
                    String caption = tabsheet.getTab(tab).getCaption();

                    // Fill the tab content
                    tab.removeAllComponents();
                    VerticalLayout vl = new VerticalLayout();
                    vl.setWidth("100%");
//                    vl.addComponent(new Label("HOLA MUNDO ->>"+caption));
                    if (caption.equalsIgnoreCase("Solicitud")) {
                        vl.addComponent(createForm());
                    } else {
                        vl.addComponent(createFormMotivoSolicitud());
                    }

                    tab.addComponent(vl);
                }
            });

// Have some tabs
            String[] tabs = {"Solicitud", "Motivo Solicitud"};
            for (String caption : tabs) {
                tabsheet.addTab(new VerticalLayout(), caption);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static class No {

        public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
            return source -> null;
        }

        public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
            return (bean, fieldValue) -> {
                //no op
            };
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            if (txtCedulaFuncionario == null || txtCedulaFuncionario.isEmpty()) {
                savedEnabled = false;
            }
            if (txtNombreFuncionario == null || txtNombreFuncionario.isEmpty()) {
                savedEnabled = false;
            }
            if (formulario == null || formulario.getValue() == null) {
                savedEnabled = false;
            }
            if (motivo == null || motivo.getValue() == null) {
                savedEnabled = false;
            }
        }
        try {
            if (fechaInicio.isVisible()) {
                if (fechaInicio == null || fechaInicio.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaFin == null || fechaFin.getValue() == null) {
                    savedEnabled = false;
                }
            } else {
                if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    if (fechaHoraInicio == null || fechaHoraInicio.getValue() == null) {
                        savedEnabled = false;
                    }
                    if (fechaHoraFin == null || fechaHoraFin.getValue() == null) {
                        savedEnabled = false;
                    }
                }
            }
        } catch (Exception e) {
            if (fechaInicio.isVisible()) {
                if (fechaInicio == null || fechaInicio.getValue() == null) {
                    savedEnabled = false;
                }
                if (fechaFin == null || fechaFin.getValue() == null) {
                    savedEnabled = false;
                }
            } else {
                if (!formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    if (fechaHoraInicio == null || fechaHoraInicio.getValue() == null) {
                        savedEnabled = false;
                    }
                    if (fechaHoraFin == null || fechaHoraFin.getValue() == null) {
                        savedEnabled = false;
                    }
                }
            }
        } finally {
        }
//        if (encargado == null || encargado.getValue() == null) {
//            savedEnabled = false;
//        }
//        guardar.setEnabled(savedEnabled);
        return savedEnabled;
    }

    public void editarRegistro(Solicitud solicitud) {
        setCaption("Editar Solicitud");
        this.solicitud = solicitud;

        if (solicitud.getFormulario().getDescripcion().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
            editar = 1;
            editarSolicitudProduccion(solicitud);
        } else {
            List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
            try {
                txtCedulaFuncionario.setValue(solicitud.getFuncionario().getCedula());
            } catch (Exception e) {
                txtCedulaFuncionario.setValue(null);
            } finally {
            }
            try {
                txtDependencia.setValue(solicitud.getDependencia().toUpperCase());
            } catch (Exception e) {
                txtDependencia.setValue("");
            } finally {
            }
            try {
                txtNombreFuncionario.setValue(solicitud.getFuncionario().getNombre() + " " + solicitud.getFuncionario().getApellido());
            } catch (Exception e) {
                txtNombreFuncionario.setValue(null);
            } finally {
            }
            try {
                txtCargo.setValue(solicitud.getFuncionario().getCargo().getDescripcion());
            } catch (Exception e) {
                txtCargo.setValue(null);
            } finally {
            }
            try {
                txtArea.setValue(solicitud.getFuncionario().getDependencia().getDescripcion());
            } catch (Exception e) {
                txtArea.setValue(null);
            } finally {
            }
            try {
                formulario.setValue(solicitud.getFormulario());
                cargarPorFormulario(solicitud.getFormulario());
            } catch (Exception e) {
                formulario.setValue(null);
            } finally {
            }
            try {
                if (solicitud.getCantdia() == null) {
                    txtCantdias.setValue("0");
                } else {
                    txtCantdias.setValue(solicitud.getCantdia() + "");
                }
            } catch (Exception e) {
                txtCantdias.setValue("0");
            } finally {
            }
            try {
                txtObservacion.setValue(solicitud.getObservacion());
            } catch (Exception e) {
                txtObservacion.setValue(null);
            } finally {
            }
            try {
                encargado.setValue(solicitud.getEncargado());
            } catch (Exception e) {
                encargado.setValue(null);
            } finally {
            }
            try {
                txtOtroMotivo.setValue(listSolicitudDetalle.get(0).getDescripcion());
            } catch (Exception e) {
                txtOtroMotivo.setValue("");
            } finally {
            }
            try {
                labelUrl.setValue(listSolicitudDetalle.get(0).getUrlimagen());
            } catch (Exception e) {
                labelUrl.setValue("");
            } finally {
            }
            try {
                motivo.setValue(listSolicitudDetalle.get(0).getMotivo());
            } catch (Exception e) {
                motivo.setValue(null);
            } finally {
            }
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                txtDependencia.setVisible(true);
            } else {
                txtDependencia.setVisible(false);
            }
            SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                fechaHoraInicio.setVisible(true);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);

                mainLayout.setVisible(false);
                try {
                    String fecha = formatFec.format(solicitud.getFechaini());
                    String hora = formatHor.format(solicitud.getHoraini());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtInicio = LocalDateTime.parse(fec);
                    fechaHoraInicio.setValue(ldtInicio);
                } catch (Exception e) {
                    fechaHoraInicio.setValue(null);
                } finally {
                }
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(true);
                fechaInicio.setVisible(false);
                fechaFin.setVisible(false);
                txtDependencia.setVisible(false);
                fechaHoraInicio.setValue(null);

                mainLayout.setVisible(true);
                try {
                    String fecha = formatFec.format(solicitud.getFechafin());
                    String hora = formatHor.format(solicitud.getHorafin());
                    String fec = fecha + "T" + hora + ":00";
                    LocalDateTime ldtFin = LocalDateTime.parse(fec);
                    fechaHoraFin.setValue(ldtFin);
                } catch (Exception e) {
                    fechaHoraFin.setValue(null);
                } finally {
                }
            } else {
                fechaHoraInicio.setVisible(false);
                fechaHoraFin.setVisible(false);
                fechaInicio.setVisible(true);
                fechaFin.setVisible(true);

                mainLayout.setVisible(false);
                try {
                    fechaInicio.setValue(DateUtils.asLocalDate(solicitud.getFechaini()));
                } catch (Exception e) {
                    fechaInicio.setValue(null);
                } finally {
                }
                try {
                    fechaFin.setValue(DateUtils.asLocalDate(solicitud.getFechafin()));
                } catch (Exception e) {
                    fechaFin.setValue(null);
                } finally {
                }
            }
            try {
                Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                    vacacionesListado.setItems(vacacionesController.listadeVacacionesON(func.getId()));
                    vacacionesListado.setItemCaptionGenerator(Vacaciones::getPeriodo);
                    for (Vacaciones listadeVacaciones : vacacionesController.listadeVacacionesON(func.getId())) {
                        if (listadeVacaciones.getPeriodo().equalsIgnoreCase(solicitud.getPeriodovacas())) {
                            vacacionesListado.setValue(listadeVacaciones);
                            txtCantPeriodo.setValue((listadeVacaciones.getCantdiavaca() - listadeVacaciones.getCantdiatomada()) + "");
                        }
                    }
                    vacacionesListado.setVisible(true);
                    txtCantPeriodo.setVisible(true);
                } else {
                    vacacionesListado.setVisible(false);
                    txtCantPeriodo.setVisible(false);
                }
            } catch (Exception exp) {
            } finally {
            }
        }

        try {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
            if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                vacacionesListado.setItems(vacacionesController.listadeVacacionesON(func.getId()));
                vacacionesListado.setItemCaptionGenerator(Vacaciones::getPeriodo);
                vacacionesListado.setVisible(true);
                txtCantPeriodo.setVisible(true);
            } else {
                vacacionesListado.setVisible(false);
                txtCantPeriodo.setVisible(false);
            }
        } catch (Exception exp) {
        } finally {
        }

    }

    private VerticalLayout createForm() {
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.addComponent(txtCedulaFuncionario);
        horizontal.addComponent(txtNombreFuncionario);
        horizontal.addComponent(txtArea);
        horizontal.addComponent(txtCargo);

        horizontal.setWidth("100%");
        horizontal.setSpacing(true);
        horizontal.setStyleName("top-bar");

        cbDpto.setWidth(20f, TextField.UNITS_EM);
        cbCargo.setWidth(20f, TextField.UNITS_EM);

        HorizontalLayout horizontalSegundo = new HorizontalLayout();
        horizontalSegundo.addComponent(formulario);
        horizontalSegundo.addComponent(fechaInicio);
        horizontalSegundo.addComponent(fechaFin);
        horizontalSegundo.addComponent(fechaHoraInicio);
        horizontalSegundo.addComponent(fechaHoraFin);
        horizontalSegundo.addComponent(txtCantdias);
        horizontalSegundo.addComponent(cbDpto);
        horizontalSegundo.addComponent(cbCargo);

        horizontalSegundo.setWidth("100%");
        horizontalSegundo.setSpacing(true);
        horizontalSegundo.setStyleName("top-bar");

        HorizontalLayout horizontalPeriodoVacas = new HorizontalLayout();
        horizontalPeriodoVacas.addComponent(vacacionesListado);
        horizontalPeriodoVacas.addComponent(txtCantPeriodo);
        horizontalPeriodoVacas.setWidth("50%");
        horizontalPeriodoVacas.setSpacing(true);
        horizontalPeriodoVacas.setStyleName("top-bar");

        layout.addComponent(horizontalSegundo);
        layout.addComponent(horizontal);
        layout.addComponent(horizontalPeriodoVacas);
//        layout.addComponent(txtObservacion);
        txtObservacion.setWidth("100%");

        HorizontalLayout horizontalButton = new HorizontalLayout();
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);
        return layout;
    }

    private VerticalLayout createFormMotivoSolicitud() {
        guardar.setVisible(true);
        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("crud-view");
        layout.setMargin(true);
        layout.setSpacing(true);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.addComponent(motivo);

//        horizontal.addComponent(rrhh);
        horizontal.addComponent(encargado);
        horizontal.addComponent(upload);
        horizontal.addComponent(labelUrl);

        image.setVisible(false);
        upload.setButtonCaption("Seleccionar");
        upload.addSucceededListener(receiver);

        // Prevent too big downloads 0981752315
        final long UPLOAD_LIMIT = 1000000l;
        upload.addStartedListener(new Upload.StartedListener() {
            private static final long serialVersionUID = 4728847902678459488L;

            @Override
            public void uploadStarted(Upload.StartedEvent event) {
                if (event.getContentLength() > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });

        // Check the size also during progress
        upload.addProgressListener(new Upload.ProgressListener() {
            private static final long serialVersionUID = 8587352676703174995L;

            @Override
            public void updateProgress(long readBytes, long contentLength) {
                if (readBytes > UPLOAD_LIMIT) {
                    Notification.show("El archivo es muy grande",
                            Notification.Type.ERROR_MESSAGE);
                    upload.interruptUpload();
                }
            }
        });
        // Create uploads directory
        File uploads = new File(Constants.UPLOAD_DIR_TEMP);
        if (!uploads.exists() && !uploads.mkdir()) {
            horizontal.addComponent(new Label("ERROR: No se pudo crear la carpeta"));
        }

        horizontal.setWidth("100%");
        horizontal.setSpacing(true);
        horizontal.setStyleName("top-bar");

        HorizontalLayout horizontalSegundo = new HorizontalLayout();
        horizontalSegundo.addComponent(txtOtroMotivo);
        HorizontalLayout horizontalTercero = new HorizontalLayout();
        horizontalTercero.addComponent(txtDependencia);
//        horizontalSegundo.addComponent(txtOtroMotivo);
        txtOtroMotivo.setWidth("100%");

        horizontalSegundo.setWidth("100%");
        horizontalSegundo.setSpacing(true);
        horizontalSegundo.setStyleName("top-bar");
        txtDependencia.setWidth("100%");

        horizontalTercero.setWidth("100%");
        horizontalTercero.setSpacing(true);
        horizontalTercero.setStyleName("top-bar");

        btnAgregar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        form.addComponent(cbFuncionario);

        form.addComponent(txtAreaForm);
        form.addComponent(txtCargoForm);
        form.addComponent(txtObservacionForm);
        form.addComponent(fechaHoraForm);
        form.addComponent(btnAgregar);
        mainLayout.addComponent(gridFuncionario);
        mainLayout.addComponent(form);
        mainLayout.setWidth("100%");

        layout.addComponent(mainLayout);

        layout.addComponent(horizontal);
        layout.addComponent(horizontalSegundo);
        layout.addComponent(horizontalTercero);

        HorizontalLayout horizontalButton = new HorizontalLayout();
        guardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
        horizontalButton.addComponent(guardar);
        cancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
        horizontalButton.addComponent(cancelar);
        horizontalButton.setSpacing(true);
        layout.addComponent(horizontalButton);

//        FormLayout columnLayout = new FormLayout();
//// Setting the desired responsive steps for the columns in the layout
//columnLayout.setResponsiveSteps(
//           new ResponsiveStep("25em", 1),
//           new ResponsiveStep("32em", 2),
//           new ResponsiveStep("40em", 3));
//   TextField firstName = new TextField();
//   firstName.setPlaceholder("First Name");
//   TextField lastName = new TextField();
//   lastName.setPlaceholder("Last Name");
//   TextField email = new TextField();
//   email.setPlaceholder("Email");
//   TextField nickname = new TextField();
//   nickname.setPlaceholder("Username");
//   TextField website = new TextField();
//   website.setPlaceholder("Link to personal website");
//   TextField description = new TextField();
//   description.setPlaceholder("Enter a short description about yourself");
//   columnLayout.add(firstName, lastName,  nickname, email, website); 
//   // You can set the desired column span for the components individually.
//   columnLayout.setColspan(website, 2);
//   // Or just set it as you add them.
//   columnLayout.add(description, 3);
        return layout;
    }

    public void setSaveListener(Consumer<Solicitud> saveListener) {
        this.saveListener = saveListener;
    }

    public void setDeleteListener(Consumer<Solicitud> deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setCancelListener(Consumer<Solicitud> cancelListener) {
        this.cancelListener = cancelListener;
    }

    public boolean isEnviarCorreos() {
        return enviarCorreos;
    }

    public void setEnviarCorreos(boolean enviarCorreos) {
        this.enviarCorreos = enviarCorreos;
    }

    public static long calcWeekDays(final Date start, final Date end) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = start;
        Date date2 = end;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);

        int numberOfDays = 0;
        while (cal1.before(cal2)) {
//            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
            if ((Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }

    private void updateList(String value) {
        try {
            if (value.equalsIgnoreCase("")) {
                txtNombreFuncionario.setValue("");
                txtArea.setValue("");
                txtCargo.setValue("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarPorFormulario(Formulario value) {
        try {
            motivo.setValue(null);
            if (value != null) {
                motivo.setItems(motivosDao.listarPorFormulario(value));
                motivo.setItemCaptionGenerator(Motivos::getDescripcion);
                txtCantdias.setValue("0");
                if (value.getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);

                    fechaHoraInicio.setVisible(true);
                    fechaHoraFin.setVisible(true);
                    txtDependencia.setVisible(true);
                    fechaHoraFin.setCaption("Fecha Fin (*)");
                    upload.setVisible(true);
                    txtOtroMotivo.setVisible(true);

                    txtCedulaFuncionario.setVisible(true);
                    txtNombreFuncionario.setVisible(true);
                    txtArea.setVisible(true);
                    txtCargo.setVisible(true);
                    txtObservacion.setVisible(true);
                    motivo.setVisible(true);
                    mainLayout.setVisible(false);
                    txtCantdias.setVisible(false);

                    cbCargo.setVisible(false);
                    cbDpto.setVisible(false);
                    vacacionesListado.setVisible(false);
                    txtCantPeriodo.setVisible(false);
                } else if (value.getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);
                    fechaHoraInicio.setVisible(false);

                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(false);
                    fechaFin.setVisible(false);

                    fechaHoraInicio.setVisible(false);
                    fechaHoraFin.setVisible(false);
                    txtDependencia.setVisible(false);

                    fechaHoraFin.setCaption("Fecha/Hora Estipulada (*)");
                    upload.setVisible(false);
                    txtOtroMotivo.setVisible(false);

                    txtCedulaFuncionario.setVisible(false);
                    txtNombreFuncionario.setVisible(false);
                    txtArea.setVisible(false);
                    txtCargo.setVisible(false);
                    txtObservacion.setVisible(false);
                    motivo.setVisible(false);
                    gridFuncionario.clearSortOrder();
                    mainLayout.setVisible(true);
                    txtCantdias.setVisible(false);

                    cbDpto.setItems(dptoDao.listarDepartamentosPadres());
                    cbDpto.setItemCaptionGenerator(Dependencia::getDescripcion);
                    cbDpto.setVisible(true);

                    cbCargo.setItems(new ArrayList<>());
                    cbCargo.setVisible(true);

                    SimpleDateFormat formatFec = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
                    try {
                        String fecha = formatFec.format(new Date());
                        String fec = fecha + "T" + "19:00:00";
                        LocalDateTime ldtFin = LocalDateTime.parse(fec);
                        fechaHoraForm.setValue(ldtFin);
                    } catch (Exception e) {
                        fechaHoraForm.setValue(null);
                    } finally {
                    }
                    vacacionesListado.setVisible(false);
                    txtCantPeriodo.setVisible(false);
                } else {
                    fechaInicio.setValue(null);
                    fechaFin.setValue(null);
                    fechaHoraInicio.setValue(null);
                    fechaHoraFin.setValue(null);

                    fechaInicio.setVisible(true);
                    fechaFin.setVisible(true);

                    fechaHoraInicio.setVisible(false);
                    fechaHoraFin.setVisible(false);
                    txtCantdias.setValue("");
                    txtCantdias.setVisible(true);
                    txtDependencia.setVisible(false);
                    upload.setVisible(true);
                    txtOtroMotivo.setVisible(true);

                    txtCedulaFuncionario.setVisible(true);
                    txtNombreFuncionario.setVisible(true);
                    txtArea.setVisible(true);
                    txtCargo.setVisible(true);
                    txtObservacion.setVisible(true);
                    motivo.setVisible(true);
                    mainLayout.setVisible(false);

                    cbCargo.setVisible(false);
                    cbDpto.setVisible(false);
                    vacacionesListado.setVisible(true);
                    txtCantPeriodo.setVisible(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public Date convertToDateViaInstant(LocalDate dateToConvert) {
//        return java.util.Date.from(dateToConvert.atStartOfDay()
//                .atZone(ZoneId.systemDefault())
//                .toInstant());
//    }
    public static boolean isWeekendSunday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    private void updateFecha() {
        if (fechaInicio.getValue() != null && fechaFin.getValue() != null) {
            if (fechaInicio.getValue().isBefore(fechaFin.getValue()) || fechaInicio.getValue().isEqual(fechaFin.getValue())) {
                if (txtCedulaFuncionario.getValue() != null && !txtCedulaFuncionario.getValue().equals("")) {
                    Funcionario func = funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue());
                    txtArea.setValue(func.getCargo().getDescripcion());
                    txtCargo.setValue(func.getDependencia().getDescripcion());
                    txtNombreFuncionario.setValue(func.getNombreCompleto());

                    long dif = calcularCantDias();
                    try {
                        if (func.getId() != null) {
                            List<Suspenciones> listSuspencion = suspencionDao.listarPorPeriodoFuncionario(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getId());
                            if (listSuspencion.size() > 0) {
                                dif -= listSuspencion.size();
                            }
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (func.getId() != null) {
                            List<Rotaciones> listRotacion = rotacionDao.listarPorFechas(DateUtils.asDate(fechaInicio.getValue()), DateUtils.asDate(fechaFin.getValue()), func.getId());
                            Calendar start = Calendar.getInstance();
                            start.setTime(DateUtils.asDate(fechaInicio.getValue()));

                            Calendar end = Calendar.getInstance();
                            end.setTime(DateUtils.asDate(fechaFin.getValue()));
                            int cantSabados = 0;
                            while (!start.after(end)) {
                                Date targetDay = start.getTime();
                                if (isWeekendSaturday(DateUtils.asLocalDate(targetDay))) {
                                    cantSabados++;
                                }
                                start.add(Calendar.DATE, 1);
                            }
                            //dif -= (cantSabados - listRotacion.size());aaaaaaaaaaaa
                            ///dif -= (listRotacion.size());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                long substract = calcWeekDays(fechaInicio.getValue(), fechaFin.getValue());
                    txtCantdias.setValue(dif + "");
//                    grid.setItems(controller.getFuncionario(filter.getValue().toUpperCase()));
                } else {
                    txtCantdias.setValue(calcularCantDias() + "");
                }
            }
        }
    }

    public static boolean isWeekend(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public void nuevoRegistro() {
        solicitud = new Solicitud();
        solicitud.setFechacreacion(new Date());
    }

    private void save() {
        if (validate()) {
            boolean value = true;
            if (!mostrarFechaAnterior) {
                Date fechaSolicitud = new Date();
                if (fechaInicio.isVisible()) {
                    fechaSolicitud = DateUtils.asDate(fechaInicio.getValue());
                } else {
                    try {
                        fechaSolicitud = DateUtils.asDate(fechaHoraInicio.getValue());
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    fechaSolicitud = DateUtils.asDate(fechaHoraForm.getValue());
                }
                Date fechaHoy = new Date();
                fechaHoy.setHours(0);
                fechaHoy.setMinutes(0);
                fechaHoy.setSeconds(0);
                java.sql.Date sqlDateHoy = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaHoy));

                fechaSolicitud.setHours(0);
                fechaSolicitud.setMinutes(0);
                fechaSolicitud.setSeconds(0);
                java.sql.Date sqlDateSolicitud = java.sql.Date.valueOf(DateUtils.asLocalDate(fechaSolicitud));
                if (sqlDateSolicitud.compareTo(sqlDateHoy) == 0) {
                    value = false;
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                } else if (sqlDateSolicitud.before(sqlDateHoy)) {
                    Notification.show("No es posible registrar fechas anteriores o igual a la de hoy, consulte con el administrador para cargarlas.",
                            Notification.Type.ERROR_MESSAGE);
                    value = false;
                } else if (sqlDateSolicitud.after(sqlDateHoy)) {
                    value = true;
                }
            }

            if (value) {
                if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                    solicitud.setEncargado(null);
                    try {
                        solicitud.setFormulario(formulario.getValue());
                    } catch (Exception e) {
                        solicitud.setFormulario(null);
                    } finally {
                    }
                    try {
                        solicitud.setCargofunc(cbCargo.getValue().getDescripcion());
                    } catch (Exception e) {
                        solicitud.setCargofunc("");
                    } finally {
                    }
                    try {
                        solicitud.setAreafunc(cbDpto.getValue().getDescripcion());
                    } catch (Exception e) {
                        solicitud.setAreafunc("");
                    } finally {
                    }

                    if (cbCargo == null || cbCargo.getValue() == null || cbDpto == null || cbDpto.getValue() == null) {
                        Notification.show("Atención", "Completar los campos obligatorios", Notification.Type.ERROR_MESSAGE);
                    } else {
                        if (listProduccionDetalle.size() > 0) {
                            solicitud.setAprobado(0L);
                            solicitud.setFechaini(listProduccionDetalle.get(0).getFecha());
                            solicitud.setFechafin(listProduccionDetalle.get(0).getFecha());
                            solicitud = solicitudDao.guardarSolicitud(solicitud);
                            guardarDetalleProduccion(solicitud);
                            setVisible(false);
                            saveListener.accept(solicitud);
                            Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            Notification.show("Debe agregar detalles a la solicitud",
                                    Notification.Type.ERROR_MESSAGE);
                        }
                    }
                } else {
                    boolean valorVacas = true;
                    if (vacacionesListado.isVisible()) {
                        if (txtCantPeriodo.getValue().equalsIgnoreCase("")) {
                            txtCantPeriodo.setValue("0");
                        }
                        if (Double.parseDouble(txtCantPeriodo.getValue()) >= Double.parseDouble(txtCantdias.getValue())) {
                            System.out.println("1) " + Double.parseDouble(txtCantPeriodo.getValue()));
                            System.out.println("2) " + Long.parseLong(txtCantdias.getValue()));
                            System.out.println("3) " + parametroVacaciones);
                            if ((Double.parseDouble(txtCantPeriodo.getValue()) >= parametroVacaciones) 
                                    && (parametroVacaciones <= Double.parseDouble(txtCantdias.getValue()))) {
                                valorVacas = true;
                                solicitud.setPeriodovacas(vacacionesListado.getValue().getPeriodo());
                            } else if (Double.parseDouble(txtCantPeriodo.getValue()) == Double.parseDouble(txtCantdias.getValue())) {
                                valorVacas = true;
                                solicitud.setPeriodovacas(vacacionesListado.getValue().getPeriodo());
                            } else {
//                                if (!booleanVacaciones) {
//                                    Notification.show("Cantidad de días a solicitar debe ser como mínimo " + parametroVacaciones + " días, o en caso de tener menos de " + parametroVacaciones + " días se debe seleccionar el total del periodo",
//                                            Notification.Type.ERROR_MESSAGE);
//                                    valorVacas = false;
//                                } else {
                                    valorVacas = true;
                                    solicitud.setPeriodovacas(vacacionesListado.getValue().getPeriodo());
                                
                            }
                        } else {
                            Notification.show("Cantidad de días a solicitar debe ser menor o igual a la disponibilidad del periodo",
                                    Notification.Type.ERROR_MESSAGE);
                            valorVacas = false;
                        }
                    }

                    if (valorVacas) {
                        boolean val = false;
                        if (fechaInicio.isVisible()) {
                            val = fechaInicio.getValue().isBefore(fechaFin.getValue()) || fechaInicio.getValue().isEqual(fechaFin.getValue());
                        } else {
                            try {
                                val = fechaHoraInicio.getValue().isBefore(fechaHoraFin.getValue()) || fechaHoraInicio.getValue().isEqual(fechaHoraFin.getValue());
                            } catch (Exception e) {
                                if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE PRODUCCIÓN")) {
                                    val = true;
                                }
                            } finally {
                            }
                        }
                        if (val) {
                            try {
                                try {
                                    solicitud.setFuncionario(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()));
                                } catch (Exception e) {
                                    solicitud.setFuncionario(null);
                                } finally {
                                }
                                try {
                                    solicitud.setAreafunc(txtArea.getValue());
                                } catch (Exception e) {
                                    solicitud.setAreafunc(null);
                                } finally {
                                }
                                try {
                                    solicitud.setCargofunc(txtCargo.getValue());
                                } catch (Exception e) {
                                    solicitud.setCargofunc(null);
                                } finally {
                                }
                                try {
                                    solicitud.setFormulario(formulario.getValue());
                                } catch (Exception e) {
                                    solicitud.setFormulario(null);
                                } finally {
                                }
                                try {
                                    solicitud.setDependencia(txtDependencia.getValue());
                                } catch (Exception e) {
                                    solicitud.setDependencia("");
                                } finally {
                                }
                                try {
                                    if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE ORDEN DE TRABAJO")) {
                                        try {
                                            solicitud.setFechaini(DateUtils.asDate(fechaHoraInicio.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setFechaini(null);
                                        } finally {
                                        }
                                        try {
                                            solicitud.setFechafin(DateUtils.asDate(fechaHoraFin.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setFechafin(null);
                                        } finally {
                                        }
                                        try {
                                            solicitud.setHoraini(Timestamp.valueOf(fechaHoraInicio.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setHoraini(null);
                                        } finally {
                                        }
                                        try {
                                            solicitud.setHorafin(Timestamp.valueOf(fechaHoraFin.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setHorafin(null);
                                        } finally {
                                        }
                                    } else {
                                        try {
                                            solicitud.setFechaini(DateUtils.asDate(fechaInicio.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setFechaini(null);
                                        } finally {
                                        }
                                        try {
                                            solicitud.setFechafin(DateUtils.asDate(fechaFin.getValue()));
                                        } catch (Exception e) {
                                            solicitud.setFechafin(null);
                                        } finally {
                                        }
                                    }
                                } catch (Exception e) {
                                } finally {
                                }

                                try {
                                    solicitud.setCantdia(Long.parseLong(txtCantdias.getValue()));
                                } catch (Exception e) {
                                    solicitud.setCantdia(null);
                                } finally {
                                }
                                try {
                                    solicitud.setObservacion(txtObservacion.getValue());
                                } catch (Exception e) {
                                    solicitud.setObservacion(null);
                                } finally {
                                }
                                try {
                                    solicitud.setEncargado(encargado.getValue());
                                } catch (Exception e) {
                                    solicitud.setEncargado(null);
                                } finally {
                                }
                                try {
                                    if (solicitud.getId() == null) {
                                        solicitud.setAprobado(0l);
                                    }
//                solicitud.setFuncrrhh(rrhh.getValue());
                                } catch (Exception e) {
                                } finally {
                                }
                                if (formulario.getValue().getDescripcion().toUpperCase().equalsIgnoreCase("SOLICITUD DE VACACIONES")) {
                                    double cantDiaVacas = 0;
                                    double cantDiaTomada = 0;
                                    cargarVacacionesDisponible();
                                    for (Map.Entry<Long, String> entry : mapeo.entrySet()) {
                                        String[] strArray = entry.getValue().split("-");
                                        cantDiaVacas += Double.parseDouble(strArray[0].replace(".0", ""));
                                        cantDiaTomada += Double.parseDouble(strArray[1].replace(".0", ""));
                                    }
                                    double dif = cantDiaVacas - cantDiaTomada;
                                    if (dif >= Double.parseDouble(txtCantdias.getValue())) {
                                        solicitud = solicitudDao.guardarSolicitud(solicitud);
                                        guardarDetalle(solicitud);
                                        setVisible(false);
                                        //guardarVacaciones(solicitud, cantDiaTomada, cantDiaVacas);
                                        saveListener.accept(solicitud);
                                        Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                                    } else {
                                        Notification.show("Mensaje del Sistema", "Cantidad de vacaciones supera lo disponible", Notification.Type.HUMANIZED_MESSAGE);
                                    }
                                } else {
                                    solicitud = solicitudDao.guardarSolicitud(solicitud);
                                    guardarDetalle(solicitud);
                                    setVisible(false);
                                    saveListener.accept(solicitud);
                                    Notification.show("Mensaje del Sistema", "Datos registrados correctamente", Notification.Type.HUMANIZED_MESSAGE);
                                }
                            } catch (Exception ex) {
                                Notification.show("Atención", "Ocurio un error al intentar borrar el registro", Notification.Type.ERROR_MESSAGE);
                                saveListener.accept(solicitud);
                                Logger
                                        .getLogger(FuncionarioForm.class
                                                .getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {
                            Notification.show("Atención", "La fecha de inicio debe ser anterior o igual a la fecha fin", Notification.Type.ERROR_MESSAGE);
                        }
                    }
                }
            }
        } else {
            Notification.show("Atención", "Completar los campos obligatorios", Notification.Type.ERROR_MESSAGE);
        }

    }

    private void guardarDetalle(Solicitud solicitud) {
        SolicitudDetalle detalle = new SolicitudDetalle();
        List<SolicitudDetalle> listSolicitudDetalle = solicitudDetalleDao.listarPorIdSolicitud(solicitud.getId());
        if (listSolicitudDetalle.size() > 0) {
            detalle = listSolicitudDetalle.get(0);
        }
        try {
            detalle.setMotivo(motivo.getValue());
        } catch (Exception e) {
            detalle.setMotivo(null);
        } finally {
        }
        try {
            detalle.setDescripcion(txtOtroMotivo.getValue());
        } catch (Exception e) {
            detalle.setDescripcion(null);
        } finally {
        }
        try {
            detalle.setUrlimagen(file.getAbsolutePath());
        } catch (Exception e) {
            detalle.setUrlimagen(null);
        } finally {
        }
        try {
            detalle.setSolicitud(solicitud);
        } catch (Exception e) {
            detalle.setSolicitud(null);
        } finally {
        }

        solicitudDetalleDao.guardarSolicitudDetalle(detalle);
    }

    private void cargarVacacionesDisponible() {
        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacaciones(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getId());
//        List<Vacaciones> listVacaciones = vacacionesDao.listadeVacacionesPorPeriodo(funcionarioDao.listarFuncionarioPorCI(txtCedulaFuncionario.getValue()).getIdfuncionario(), vacacionesListado.getValue().getPeriodo());
        for (Vacaciones listado : listVacaciones) {
            mapeo.put(listado.getId(), listado.getCantdiavaca() + "-" + listado.getCantdiatomada());
        }
    }
}
