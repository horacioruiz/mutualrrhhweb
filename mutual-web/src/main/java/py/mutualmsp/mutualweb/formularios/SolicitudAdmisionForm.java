/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.server.Setter;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import py.mutualmsp.mutualweb.dao.BarrioDao;
import py.mutualmsp.mutualweb.dao.BeneficiarioAdmisionDao;
import py.mutualmsp.mutualweb.dao.CargoDao;
import py.mutualmsp.mutualweb.dao.CiudadDao;
import py.mutualmsp.mutualweb.dao.EstadoSolicitudAdmisionDao;
import py.mutualmsp.mutualweb.dao.EstadocivilDao;
import py.mutualmsp.mutualweb.dao.FormacionAcademicaDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.GiraduriaDao;
import py.mutualmsp.mutualweb.dao.InstitucionDao;
import py.mutualmsp.mutualweb.dao.ListaBeneficiarioCache;
import py.mutualmsp.mutualweb.dao.PaisDao;
import py.mutualmsp.mutualweb.dao.ParentescoDao;
import py.mutualmsp.mutualweb.dao.ProfesionDao;
import py.mutualmsp.mutualweb.dao.PromotorDao;
import py.mutualmsp.mutualweb.dao.RubroDao;
import py.mutualmsp.mutualweb.dao.SexoDao;
import py.mutualmsp.mutualweb.dao.SolicitudAdmisionDao;
import py.mutualmsp.mutualweb.dao.TipocasaDao;
import py.mutualmsp.mutualweb.entities.Barrio;
import py.mutualmsp.mutualweb.entities.BeneficiarioAdmision;
import py.mutualmsp.mutualweb.entities.Cargo;
import py.mutualmsp.mutualweb.entities.Ciudad;
import py.mutualmsp.mutualweb.entities.EstadoSolicitudAdmision;
import py.mutualmsp.mutualweb.entities.Estadocivil;
import py.mutualmsp.mutualweb.entities.FormacionAcademica;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.Giraduria;
import py.mutualmsp.mutualweb.entities.Institucion;
import py.mutualmsp.mutualweb.entities.Pais;
import py.mutualmsp.mutualweb.entities.Parentesco;
import py.mutualmsp.mutualweb.entities.Profesion;
import py.mutualmsp.mutualweb.entities.Promotor;
import py.mutualmsp.mutualweb.entities.ReferenciaPersonalAdmision;
import py.mutualmsp.mutualweb.entities.Rubro;
import py.mutualmsp.mutualweb.entities.Sexo;
import py.mutualmsp.mutualweb.entities.SolicitudAdmision;
import py.mutualmsp.mutualweb.entities.Tipocasa;
import py.mutualmsp.mutualweb.util.AuditoriaRegistrar;
import py.mutualmsp.mutualweb.util.ResourceLocator;

/**
 *
 * @author DBarreto
 */
public class SolicitudAdmisionForm extends FormLayout{
    TextField txtfId = new TextField("Id");
    TextField txtfNumeroSolicitud = new TextField("Nº Solicitud");
    TextField txtfCedula = new TextField("Cédula");
    TextField txtfNombre = new TextField("Nombre", "Ingrese nombre");
    TextField txtfApellido = new TextField("Apellido", "Ingrese apellido");
    TextField txtfCelular = new TextField("Nº Teléfono celular");
    TextField txtfLineaBaja = new TextField("Nº Teléfono línea baja");
    TextField txtfEmail = new TextField("email");
    TextField txtfDireccion = new TextField("Dirección");
    TextField txtfNumeroCasa = new TextField("Nº Casa");
    TextField txtfNumeroActa = new TextField("Nº Acta");
    TextField txtfNumeroResolucion = new TextField("Nº Resolución");
    TextField txtfNumeroSesion = new TextField("Nº Sesión");
    TextField txtfSeccion = new TextField("Sección");
    TextArea txtfObservaciones = new TextArea("Observaciones", "Ingrese Observación");
    TextArea txtfObservacionComite = new TextArea("Observación Comité", "Ingrese Observación");
    ComboBox<EstadoSolicitudAdmision> cmbEstado = new ComboBox<>("Estado");
    ComboBox<Profesion> cmbProfesion = new ComboBox<>("Profesión u oficio");
    ComboBox<Ciudad> cmbLugarNacimiento = new ComboBox<>("Lugar de Nacimiento");
    ComboBox<Pais> cmbNacionalidad = new ComboBox<>("Nacionalidad");
    ComboBox<Estadocivil> cmbEstadoCivil = new ComboBox<>("Estado civil");
    ComboBox<Barrio> cmbBarrio = new ComboBox<>("Barrio");
    ComboBox<Ciudad> cmbCiudadResidencia = new ComboBox<>("Ciudad Residencia");
    ComboBox<Tipocasa> cmbTipoCasa = new ComboBox<>("Tipo Casa");
    ComboBox<FormacionAcademica> cmbFormacionAcademica = new ComboBox<>("Formación Académica");
    ComboBox<Institucion> cmbInstitucion = new ComboBox<>("Institución");
    ComboBox<Cargo> cmbCargo = new ComboBox<>("Cargo");
    ComboBox<Giraduria> cmbGiraduria = new ComboBox<>("Giraduría");
    ComboBox<Promotor> cmbPromotor = new ComboBox<>("Promotor");
    ComboBox<Funcionario> cmbFuncionario = new ComboBox<>("Oficial");
    RadioButtonGroup<Sexo> radSexo = new RadioButtonGroup<>("Sexo");
    RadioButtonGroup<Rubro> radRubro = new RadioButtonGroup<>("Rubro");
    DateField dtfFechaNacimiento = new DateField("Fecha Nacimiento");
    DateField dtfFechaDesvinculacion = new DateField("Fecha Desvinculación");
    DateField dtfFechaIngreso = new DateField("Fecha Ingreso");
    DateField dtfFechaActa = new DateField("Fecha Acta");
    DateField dtfFechaResolucion = new DateField("Fecha Resolución");
    CheckBox chkExSocio = new CheckBox("Ex Socio");
    CheckBox chkReuneRequisitos = new CheckBox("Reúne requisitos");
    CheckBox chkAprobado = new CheckBox("Aprobado");
    CheckBox chkApruebaComision = new CheckBox("Aprobado por Comisión Directiva");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");
    private EntityManager em;
    
    SolicitudAdmisionDao solicitudAdmisionDao = ResourceLocator.locate(SolicitudAdmisionDao.class);
    Binder<SolicitudAdmision> binder = new Binder<>(SolicitudAdmision.class);
    SolicitudAdmision solicitudAdmision;
    
    EstadoSolicitudAdmisionDao estadoSolicitudAdmisionDao = ResourceLocator.locate(EstadoSolicitudAdmisionDao.class);
    ProfesionDao profesionDao = ResourceLocator.locate(ProfesionDao.class);
    CiudadDao ciudadDao = ResourceLocator.locate(CiudadDao.class);
    PaisDao paisDao = ResourceLocator.locate(PaisDao.class);
    EstadocivilDao estadocivilDao = ResourceLocator.locate(EstadocivilDao.class);
    BarrioDao barrioDao = ResourceLocator.locate(BarrioDao.class);
    TipocasaDao tipocasaDao = ResourceLocator.locate(TipocasaDao.class);
    FormacionAcademicaDao formacionAcademicaDao = ResourceLocator.locate(FormacionAcademicaDao.class);
    InstitucionDao institucionDao = ResourceLocator.locate(InstitucionDao.class);
    CargoDao cargoDao = ResourceLocator.locate(CargoDao.class);
    GiraduriaDao giraduriaDao = ResourceLocator.locate(GiraduriaDao.class);
    PromotorDao promotorDao = ResourceLocator.locate(PromotorDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    SexoDao sexoDao = ResourceLocator.locate(SexoDao.class);
    RubroDao rubroDao = ResourceLocator.locate(RubroDao.class);
             
    Grid<BeneficiarioAdmision> grillaBeneficiarios = new Grid<>(BeneficiarioAdmision.class);
    BeneficiarioAdmisionDao beneficiarioAdmisionDao = ResourceLocator.locate(BeneficiarioAdmisionDao.class);
    ListaBeneficiarioCache cache = ResourceLocator.locate(ListaBeneficiarioCache.class);
    List<BeneficiarioAdmision> listaBeneficiarios = new ArrayList<>();
    Grid<ReferenciaPersonalAdmision> grillaReferencias = new Grid<>(ReferenciaPersonalAdmision.class);
    
    private Consumer<SolicitudAdmision> guardarListener;
    private Consumer<SolicitudAdmision> borrarListener;
    private Consumer<SolicitudAdmision> cancelarListener;
    
    private String viejo="";
    private String nuevo="";
    private String operacion="";
    public static final String NOMBRE_TABLA = "solicitud_admision";
            
    public SolicitudAdmisionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);
            
            Panel panelDatosPersonales = new Panel();
            HorizontalLayout hDatosPersonales = new HorizontalLayout();
            FormLayout formDatosPersonales1 = new FormLayout();
            formDatosPersonales1.addComponents(txtfId, txtfNumeroSolicitud, cmbEstado, txtfCedula, txtfNombre, txtfApellido, cmbProfesion, 
                    dtfFechaNacimiento, cmbNacionalidad, txtfLineaBaja, radSexo);
            FormLayout formDatosPersonales2 = new FormLayout();
            formDatosPersonales2.addComponents(cmbLugarNacimiento, txtfCelular, txtfEmail, cmbEstadoCivil, txtfDireccion, cmbCiudadResidencia, 
                    cmbBarrio, txtfNumeroCasa, cmbTipoCasa, cmbFormacionAcademica, chkExSocio, dtfFechaDesvinculacion);
            hDatosPersonales.addComponents(formDatosPersonales1, formDatosPersonales2);
            panelDatosPersonales.setContent(hDatosPersonales);
            
            
            Panel panelDatosLaborales = new Panel();
            HorizontalLayout hDatosLaborales = new HorizontalLayout();
            FormLayout formDatosLaborales1 = new FormLayout();
            formDatosLaborales1.addComponents(cmbInstitucion, cmbCargo, txtfSeccion);
            FormLayout formDatosLaborales2 = new FormLayout();
            formDatosLaborales2.addComponents(dtfFechaIngreso, radRubro, cmbGiraduria);
            hDatosLaborales.addComponents(formDatosLaborales1, formDatosLaborales2);
            panelDatosLaborales.setContent(hDatosLaborales);
            
            
            Panel panelBeneficiarios = new Panel();
            VerticalLayout vBeneficiarios = new VerticalLayout();
            vBeneficiarios.addComponent(grillaBeneficiarios);
            panelBeneficiarios.setContent(vBeneficiarios);
            
            
            Panel panelReferencias = new Panel();
            VerticalLayout vReferencias = new VerticalLayout();
            vReferencias.addComponent(grillaReferencias);
            panelReferencias.setContent(vReferencias);
            
            Panel panelAnalisis = new Panel();
            HorizontalLayout hAnalisis = new HorizontalLayout();
            
            Panel panelDictamen = new Panel();
            Panel panelResolucion = new Panel();
            
            //int indice;
            //List<Indice> lista = new ArrayList<>();
            //lista=em.createNativeQuery("SELECT last_value+increment_by FROM solicitud_admision_id_seq;", Indice.class).getResultList();
            
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND+" "+MaterialTheme.BUTTON_FRIENDLY);
            
            addComponents(panelDatosPersonales, panelDatosLaborales, panelBeneficiarios, panelReferencias, panelAnalisis, 
                    panelDictamen, panelResolucion, botones);
            
            binder.forField(txtfId).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getId, SolicitudAdmision::setId);
            txtfId.setEnabled(false);
            
            binder.forField(txtfNumeroSolicitud).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getNumerosolicitud, SolicitudAdmision::setNumerosolicitud);
            
            binder.forField(txtfNumeroCasa).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getNumerocasa, SolicitudAdmision::setNumerocasa);
            
            binder.forField(txtfNumeroActa).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getNumeroacta, SolicitudAdmision::setNumeroacta);
            
            binder.forField(txtfNumeroResolucion).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getNumeroresolucion, SolicitudAdmision::setNumeroresolucion);
            
            binder.forField(txtfNumeroSesion).withNullRepresentation("")
                    .withConverter(new StringToLongConverter(Long.valueOf(0), "Long only"))
                    .bind(SolicitudAdmision::getNumerosesion, SolicitudAdmision::setNumerosesion);
            
            binder.bind(txtfCedula, SolicitudAdmision::getCedula, SolicitudAdmision::setCedula);
            binder.forField(txtfCedula).withNullRepresentation("")
                    .bind(SolicitudAdmision::getCedula, SolicitudAdmision::setCedula);
            
            binder.bind(txtfNombre, SolicitudAdmision::getNombre, SolicitudAdmision::setNombre);
            binder.forField(txtfNombre).withNullRepresentation("")
                    .bind(SolicitudAdmision::getNombre, SolicitudAdmision::setNombre);
            
            binder.bind(txtfApellido, SolicitudAdmision::getApellido, SolicitudAdmision::setApellido);
            binder.forField(txtfApellido).withNullRepresentation("")
                    .bind(SolicitudAdmision::getApellido, SolicitudAdmision::setApellido);
            
            binder.bind(txtfCelular, SolicitudAdmision::getTelefonocelular, SolicitudAdmision::setTelefonocelular);
            binder.forField(txtfCelular).withNullRepresentation("")
                    .bind(SolicitudAdmision::getTelefonocelular, SolicitudAdmision::setTelefonocelular);
            
            binder.bind(txtfLineaBaja, SolicitudAdmision::getTelefonolineabaja, SolicitudAdmision::setTelefonolineabaja);
            binder.forField(txtfLineaBaja).withNullRepresentation("")
                    .bind(SolicitudAdmision::getTelefonocelular, SolicitudAdmision::setTelefonolineabaja);
            
            binder.bind(txtfEmail, SolicitudAdmision::getEmail, SolicitudAdmision::setEmail);
            binder.forField(txtfEmail).withNullRepresentation("")
                    .bind(SolicitudAdmision::getEmail, SolicitudAdmision::setEmail);
            
            binder.bind(txtfDireccion, SolicitudAdmision::getDireccion, SolicitudAdmision::setDireccion);
            binder.forField(txtfDireccion).withNullRepresentation("")
                    .bind(SolicitudAdmision::getDireccion, SolicitudAdmision::setDireccion);
            
            binder.bind(txtfSeccion, SolicitudAdmision::getSeccion, SolicitudAdmision::setSeccion);
            binder.forField(txtfSeccion).withNullRepresentation("")
                    .bind(SolicitudAdmision::getSeccion, SolicitudAdmision::setSeccion);
            
            binder.bind(txtfObservaciones, SolicitudAdmision::getObservaciones, SolicitudAdmision::setObservaciones);
            binder.forField(txtfObservaciones).withNullRepresentation("")
                    .bind(SolicitudAdmision::getObservaciones, SolicitudAdmision::setObservaciones);
            
            binder.bind(txtfObservacionComite, SolicitudAdmision::getObservacioncomite, SolicitudAdmision::setObservacioncomite);
            binder.forField(txtfObservacionComite).withNullRepresentation("")
                    .bind(SolicitudAdmision::getObservacioncomite, SolicitudAdmision::setObservaciones);
            
            List<EstadoSolicitudAdmision> listaEstadoSolicitudAdmisions = estadoSolicitudAdmisionDao.listaEstadoSolicitudAdmision();
            cmbEstado.setItems(listaEstadoSolicitudAdmisions);
            cmbEstado.setItemCaptionGenerator(EstadoSolicitudAdmision::getDescripcion);
            binder.bind(cmbEstado, 
                    (SolicitudAdmision source) -> source.getIdestadosolicitud(), 
                    (SolicitudAdmision bean, EstadoSolicitudAdmision fieldvalue) -> { 
                        bean.setIdestadosolicitud(fieldvalue);});
            
            List<Profesion> listaProfesions = profesionDao.listaProfesion();
            cmbProfesion.setItems(listaProfesions);
            cmbProfesion.setItemCaptionGenerator(Profesion::getDescripcion);
            binder.bind(cmbProfesion, 
                    (SolicitudAdmision source) -> source.getIdprofesion(), 
                    (SolicitudAdmision bean, Profesion fieldvalue) -> { 
                        bean.setIdprofesion(fieldvalue);});
            
            List<Ciudad> litaCiudads = ciudadDao.listaCiudad();
            cmbLugarNacimiento.setItems(litaCiudads);
            cmbCiudadResidencia.setItems(litaCiudads);
            cmbLugarNacimiento.setItemCaptionGenerator(Ciudad::getDescripcion);
            cmbCiudadResidencia.setItemCaptionGenerator(Ciudad::getDescripcion);
            binder.bind(cmbLugarNacimiento, 
                    (SolicitudAdmision source) -> source.getIdlugarnacimiento(), 
                    (SolicitudAdmision bean, Ciudad fieldvalue) -> { 
                        bean.setIdlugarnacimiento(fieldvalue);});
            binder.bind(cmbCiudadResidencia, 
                    (SolicitudAdmision source) -> source.getIdciudadresidencia(), 
                    (SolicitudAdmision bean, Ciudad fieldvalue) -> { 
                        bean.setIdciudadresidencia(fieldvalue);});
            
            List<Pais> listaPaises = paisDao.listaPaises();
            cmbNacionalidad.setItems(listaPaises);
            cmbNacionalidad.setItemCaptionGenerator(Pais::getDescripcion);
            binder.bind(cmbNacionalidad, 
                    (SolicitudAdmision source) -> source.getIdnacionalidad(), 
                    (SolicitudAdmision bean, Pais fieldvalue) -> { 
                        bean.setIdnacionalidad(fieldvalue);});
            
            List<Estadocivil> listaEstadocivils = estadocivilDao.listaEstadocivil();
            cmbEstadoCivil.setItems(listaEstadocivils);
            cmbEstadoCivil.setItemCaptionGenerator(Estadocivil::getDescripcion);
            binder.bind(cmbEstadoCivil, 
                    (SolicitudAdmision source) -> source.getIdestadocivil(), 
                    (SolicitudAdmision bean, Estadocivil fieldvalue) -> { 
                        bean.setIdestadocivil(fieldvalue);});
            
            List<Barrio> listaBarrios = barrioDao.listaBarrio();
            cmbBarrio.setItems(listaBarrios);
            cmbBarrio.setItemCaptionGenerator(Barrio::getDescripcion);
            binder.bind(cmbBarrio, 
                    (SolicitudAdmision source) -> source.getIdbarrio(), 
                    (SolicitudAdmision bean, Barrio fieldvalue) -> { 
                        bean.setIdbarrio(fieldvalue);});
            
            List<Tipocasa> listaTipocasas = tipocasaDao.listaTipocasa();
            cmbTipoCasa.setItems(listaTipocasas);
            cmbTipoCasa.setItemCaptionGenerator(Tipocasa::getDescripcion);
            binder.bind(cmbTipoCasa, 
                    (SolicitudAdmision source) -> source.getIdtipocasa(), 
                    (SolicitudAdmision bean, Tipocasa fieldvalue) -> { 
                        bean.setIdtipocasa(fieldvalue);});
            
            List<FormacionAcademica> listaFormacionAcademicas = formacionAcademicaDao.listaFormacionAcademica();
            cmbFormacionAcademica.setItems(listaFormacionAcademicas);
            cmbFormacionAcademica.setItemCaptionGenerator(FormacionAcademica::getDescripcion);
            binder.bind(cmbFormacionAcademica, 
                    (SolicitudAdmision source) -> source.getIdformacion(), 
                    (SolicitudAdmision bean, FormacionAcademica fieldvalue) -> { 
                        bean.setIdformacion(fieldvalue);});
            
            List<Institucion> listaInstitucions = institucionDao.listaInstitucion();
            cmbInstitucion.setItems(listaInstitucions);
            cmbInstitucion.setItemCaptionGenerator(Institucion::getDescripcion);
            binder.bind(cmbInstitucion, 
                    (SolicitudAdmision source) -> source.getIdinstitucion(), 
                    (SolicitudAdmision bean, Institucion fieldvalue) -> { 
                        bean.setIdinstitucion(fieldvalue);});
            
            List<Cargo> listaCargos = cargoDao.listaCargo();
            cmbCargo.setItems(listaCargos);
            cmbCargo.setItemCaptionGenerator(Cargo::getDescripcion);
            binder.bind(cmbCargo, 
                    (SolicitudAdmision source) -> source.getIdcargo(), 
                    (SolicitudAdmision bean, Cargo fieldvalue) -> { 
                        bean.setIdcargo(fieldvalue);});
            
            List<Giraduria> listaGiradurias = giraduriaDao.listaGiraduria();
            cmbGiraduria.setItems(listaGiradurias);
            cmbGiraduria.setItemCaptionGenerator(Giraduria::getDescripcion);
            binder.bind(cmbGiraduria, 
                    (SolicitudAdmision source) -> source.getIdgiraduria(), 
                    (SolicitudAdmision bean, Giraduria fieldvalue) -> { 
                        bean.setIdgiraduria(fieldvalue);});
            
            List<Promotor> listaPromotors = promotorDao.listaPromotor();
            cmbPromotor.setItems(listaPromotors);
            cmbPromotor.setItemCaptionGenerator(Promotor::getNombreFuncionario);
            binder.bind(cmbPromotor, 
                    (SolicitudAdmision source) -> source.getIdpromotor(), 
                    (SolicitudAdmision bean, Promotor fieldvalue) -> { 
                        bean.setIdpromotor(fieldvalue);});
            
            List<Funcionario> listaFuncionarios = funcionarioDao.listaFuncionario();
            cmbFuncionario.setItems(listaFuncionarios);
            cmbFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);
            binder.bind(cmbFuncionario, 
                    (SolicitudAdmision source) -> source.getIdfuncionario(), 
                    (SolicitudAdmision bean, Funcionario fieldvalue) -> { 
                        bean.setIdfuncionario(fieldvalue);});
            
            List<Sexo> listaSexos = sexoDao.listaSexos();
            radSexo.setItems(listaSexos);
            radSexo.setItemCaptionGenerator(Sexo::getDescripcion);
            binder.bind(radSexo, 
                    (SolicitudAdmision source) -> source.getIdsexo(), 
                    (SolicitudAdmision bean, Sexo fieldvalue) -> { 
                        bean.setIdsexo(fieldvalue);});
            
            List<Rubro> listaRubros = rubroDao.listaRubros();
            radRubro.setItems(listaRubros);
            radRubro.setItemCaptionGenerator(Rubro::getDescripcion);
            binder.bind(radRubro, 
                    (SolicitudAdmision source) -> source.getIdrubro(), 
                    (SolicitudAdmision bean, Rubro fieldvalue) -> { 
                        bean.setIdrubro(fieldvalue);});
            
            binder.bind(dtfFechaNacimiento, No.getter(), No.setter());
            binder.bind(dtfFechaIngreso, No.getter(), No.setter());
            binder.bind(dtfFechaActa, No.getter(), No.setter());
            binder.bind(dtfFechaDesvinculacion, No.getter(), No.setter());
            binder.bind(dtfFechaResolucion, No.getter(), No.setter());
            
            binder.bind(chkExSocio, SolicitudAdmision::getExsocio, SolicitudAdmision::setExsocio);
            binder.bind(chkReuneRequisitos, SolicitudAdmision::getReunerequisitos, SolicitudAdmision::setReunerequisitos);
            binder.bind(chkAprobado, SolicitudAdmision::getAprobado, SolicitudAdmision::setAprobado);
            binder.bind(chkApruebaComision, SolicitudAdmision::getApruebacomision, SolicitudAdmision::setApruebacomision);
          
            binder.bindInstanceFields(this);
            
            txtfNombre.addValueChangeListener(n -> {txtfNombre.setValue(txtfNombre.getValue().toUpperCase());});
            txtfApellido.addValueChangeListener(a -> {txtfApellido.setValue(txtfApellido.getValue().toUpperCase());});
            txtfDireccion.addValueChangeListener(d -> {txtfDireccion.setValue(txtfDireccion.getValue().toUpperCase());});
            txtfObservaciones.addValueChangeListener(o -> {txtfObservaciones.setValue(txtfObservaciones.getValue().toUpperCase());});
            txtfObservacionComite.addValueChangeListener(oc -> {txtfObservacionComite.setValue(txtfObservacionComite.getValue().toUpperCase());});
            txtfSeccion.addValueChangeListener(s -> {txtfSeccion.setValue(txtfSeccion.getValue().toUpperCase());});
            txtfEmail.addValueChangeListener(em -> {txtfEmail.setValue(txtfEmail.getValue().toLowerCase());});
            
            
            grillaBeneficiarios.addItemClickListener(e ->{
                if(e.getMouseEventDetails().isDoubleClick()){
                    Window subVentana = new Window("Datos de Beneficiario");
                    FormLayout subContenido = new FormLayout();
                    VerticalLayout vertical = new VerticalLayout();
                    HorizontalLayout horizontal = new HorizontalLayout();
                    Binder<BeneficiarioAdmision> binder = new Binder<>(BeneficiarioAdmision.class);
                    ComboBox<Parentesco> cmbParentesco = new ComboBox<>("Parentesco");
                    ParentescoDao parentescoDao = ResourceLocator.locate(ParentescoDao.class);
                    List<Parentesco> listaParentescos = parentescoDao.listaParentesco();
                    cmbParentesco.setItems(listaParentescos);
                    cmbParentesco.setItemCaptionGenerator(Parentesco::getDescripcion);
                    binder.bind(cmbParentesco, 
                            (BeneficiarioAdmision source) -> source.getIdparentesco(), 
                            (BeneficiarioAdmision bean, Parentesco fieldvalue) -> { 
                                bean.setIdparentesco(fieldvalue);});
                    ComboBox<Ciudad> cmbCiudadResidencia = new ComboBox<>("Ciudad Residencia");
                    CiudadDao ciudadDao = ResourceLocator.locate(CiudadDao.class);
                    List<Ciudad> listaCiudads = ciudadDao.listaCiudad();
                    cmbCiudadResidencia.setItems(listaCiudads);
                    cmbCiudadResidencia.setItemCaptionGenerator(Ciudad::getDescripcion);
                    binder.bind(cmbCiudadResidencia, 
                            (BeneficiarioAdmision source) -> source.getIdciudadresidencia(),
                            (BeneficiarioAdmision bean, Ciudad fieldvalue) -> {
                                bean.setIdciudadresidencia(fieldvalue);});
                    ComboBox<Profesion> cmbProfesion = new ComboBox<>("Ocupación");
                    ProfesionDao profesionDao = ResourceLocator.locate(ProfesionDao.class);
                    List<Profesion> listaProfesions1 = profesionDao.listaProfesion();
                    cmbProfesion.setItems(listaProfesions1);
                    cmbProfesion.setItemCaptionGenerator(Profesion::getDescripcion);
                    binder.bind(cmbProfesion, 
                            (BeneficiarioAdmision source) -> source.getIdprofesion(),
                            (BeneficiarioAdmision bean, Profesion fieldvalue) -> {
                                bean.setIdprofesion(fieldvalue);});
                    TextField txtfCedula = new TextField("Cédula");
                    TextField txtfNombre = new TextField("Nombre");
                    TextField txtfApellido = new TextField("Apellido");
                    TextField txtfDireccion = new TextField("Dirección");
                    TextField txtfTelefono = new TextField("Telefono");
                    DateField dtfFechaNacimiento = new DateField("Fecha Nacimiento");
                    binder.bind(dtfFechaNacimiento, No.getter(), No.setter());
                    Button btnGuardar = new Button("Guardar");
                    Button btnBorrar = new Button("Borrar");
                    Button btnCancelar = new Button("Cancelar");
                    
                    btnGuardar.addStyleName(ValoTheme.BUTTON_PRIMARY);
                    btnBorrar.addStyleName(ValoTheme.BUTTON_DANGER);
                    subVentana.setWidth("50%");
                    subVentana.setHeight("65%");
                    
                    
                    txtfCedula.setValue(e.getItem().getCedula());
                    subContenido.addComponent(txtfCedula);
                    txtfNombre.setValue(e.getItem().getNombre());
                    subContenido.addComponent(txtfNombre);
                    txtfApellido.setValue(e.getItem().getApellido());
                    subContenido.addComponent(txtfApellido);
                    subContenido.addComponent(dtfFechaNacimiento);
                    subContenido.addComponent(cmbParentesco);
                    txtfDireccion.setValue(e.getItem().getDireccion());
                    subContenido.addComponent(txtfDireccion);
                    txtfTelefono.setValue(e.getItem().getTelefono());
                    subContenido.addComponent(cmbCiudadResidencia);
                    subContenido.addComponent(cmbProfesion);
                    subContenido.addComponent(txtfTelefono);
                    // Agregar componetes a la subVentana
                    vertical.addComponents(subContenido, horizontal);
                    horizontal.addComponents(btnGuardar, btnBorrar, btnCancelar);
                    subVentana.setContent(vertical);
                    
                    btnCancelar.addClickListener(c -> {subVentana.close();});
                    
                    btnGuardar.addClickListener(g -> {
                        try {
                            BeneficiarioAdmision beneficiarioAdmision = e.getItem();
                            if(dtfFechaNacimiento.getValue()==null)  beneficiarioAdmision.setFechanacimiento(null);
                            else {java.util.Date dfechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaNacimiento.getValue().toString());
                            beneficiarioAdmision.setFechanacimiento(dfechaNacimiento);}
                            beneficiarioAdmision.setCedula(txtfCedula.getValue());
                            beneficiarioAdmision.setNombre(txtfNombre.getValue());
                            beneficiarioAdmision.setApellido(txtfApellido.getValue());
                            beneficiarioAdmision.setDireccion(txtfDireccion.getValue());
                            beneficiarioAdmision.setTelefono(txtfTelefono.getValue());
                            beneficiarioAdmision.setIdparentesco(cmbParentesco.getValue());
                            beneficiarioAdmision.setIdciudadresidencia(cmbCiudadResidencia.getValue());
                            beneficiarioAdmision.setIdprofesion(cmbProfesion.getValue());
                            cache.cargarDetalle(beneficiarioAdmision);
                            subVentana.close();
                            listaBeneficiarios = cache.getLista();
                            mostrarBeneficiario(listaBeneficiarios);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    });
                    
                    btnBorrar.addClickListener(b ->{
                        BeneficiarioAdmision beneficiarioAdmision = e.getItem();
                        beneficiarioAdmisionDao.borrar(beneficiarioAdmision);
                        subVentana.close();
                        listaBeneficiarios = beneficiarioAdmisionDao.getListBeneficiarioAdmision(solicitudAdmision.getId());
                        mostrarBeneficiario(listaBeneficiarios);
                    });
                    // Centrar en la subVentana del browser
                    subVentana.center();
                    // Abrir la subVentana en la UI
                    this.getUI().addWindow(subVentana);
                } 
            
            });
            
            btnGuardar.addClickListener(e ->{
                guardar();
            });
            
            btnBorrar.addClickListener(e ->{
               borrar(); 
            });
            
            btnCancelar.addClickListener(e ->{
                setVisible(false);
                cancelarListener.accept(solicitudAdmision);
            });
            
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        
    }
    
    
    // convenience empty getter and setter implementation for better readability
    public static class No {
       public static <SOURCE, TARGET> ValueProvider<SOURCE, TARGET> getter() {
           return source -> null;
       }
       public static <BEAN, FIELDVALUE> Setter<BEAN, FIELDVALUE> setter() {
           return (bean, fieldValue) -> {
               //no op
           };
       }
    }
    
    private void mostrarBeneficiario(List<BeneficiarioAdmision> beneficiarioAdmisions) {
        try {
            if (beneficiarioAdmisions==null || beneficiarioAdmisions.isEmpty()) {
                grillaBeneficiarios.clearSortOrder();
            } else {
                grillaBeneficiarios.clearSortOrder();
                grillaBeneficiarios.setItems(beneficiarioAdmisions);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void guardar() {
        try {
            if(dtfFechaNacimiento.getValue()==null)  solicitudAdmision.setFechanacimiento(null);
            else {java.util.Date dfechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaNacimiento.getValue().toString());
                solicitudAdmision.setFechanacimiento(dfechaNacimiento);}
            if(dtfFechaIngreso.getValue()==null) solicitudAdmision.setFechaingreso(null);
            else {java.util.Date dfechaIngreso = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaIngreso.getValue().toString());
                solicitudAdmision.setFechaingreso(dfechaIngreso);}
            if(dtfFechaDesvinculacion.getValue()==null) solicitudAdmision.setFechadesvinculacion(null);
            else {java.util.Date dfechaDesvinculacion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaDesvinculacion.getValue().toString());
                solicitudAdmision.setFechadesvinculacion(dfechaDesvinculacion);}
            if(dtfFechaActa.getValue()==null) solicitudAdmision.setFechaacta(null);
            else {java.util.Date dfechaActa = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaActa.getValue().toString());
                solicitudAdmision.setFechaacta(dfechaActa);}
            if(dtfFechaResolucion.getValue()==null) solicitudAdmision.setFecharesolucion(null);
            else {java.util.Date dfechaResolucion = new SimpleDateFormat("yyyy-MM-dd").parse(dtfFechaResolucion.getValue().toString());
                solicitudAdmision.setFecharesolucion(dfechaResolucion);}
            solicitudAdmisionDao.guardar(solicitudAdmision);
            setVisible(false);
            guardarListener.accept(solicitudAdmision);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
            if(viejo.equals("")) operacion="I";
            else operacion="M";
            this.setNuevo();
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(solicitudAdmision);
        }
    }
    
    private void borrar() {
        try {
            solicitudAdmisionDao.borrar(solicitudAdmision);
            setVisible(false);
            borrarListener.accept(solicitudAdmision);
            Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            operacion="E";
            nuevo="";
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
            borrarListener.accept(solicitudAdmision);
        }
    }
    
    public void setGuardarListener(Consumer<SolicitudAdmision> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<SolicitudAdmision> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<SolicitudAdmision> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }
    
    public void setViejo(){
        if(solicitudAdmision.getId()!=null) this.viejo = "("+solicitudAdmision.getId()+","+solicitudAdmision.getNumerosolicitud()+","+
                solicitudAdmision.getIdestadosolicitud().getId()+","+solicitudAdmision.getCedula()+","+solicitudAdmision.getNombre()+","+
                solicitudAdmision.getApellido()+","+solicitudAdmision.getIdprofesion().getId()+","+solicitudAdmision.getIdlugarnacimiento().getId()+","+
                dtfFechaNacimiento.getValue()+","+solicitudAdmision.getIdnacionalidad().getId()+","+solicitudAdmision.getTelefonocelular()+","+
                solicitudAdmision.getTelefonolineabaja()+","+solicitudAdmision.getEmail()+","+solicitudAdmision.getIdsexo().getId()+","+
                solicitudAdmision.getIdestadocivil().getId()+","+solicitudAdmision.getDireccion()+","+solicitudAdmision.getNumerocasa()+","+
                solicitudAdmision.getIdbarrio().getId()+","+solicitudAdmision.getIdciudadresidencia().getId()+","+solicitudAdmision.getIdtipocasa().getId()+","+
                solicitudAdmision.getIdformacion().getId()+","+solicitudAdmision.getExsocio()+","+dtfFechaDesvinculacion.getValue()+","+
                solicitudAdmision.getIdinstitucion().getId()+","+dtfFechaIngreso.getValue()+","+solicitudAdmision.getIdcargo().getId()+","+
                solicitudAdmision.getSeccion()+","+solicitudAdmision.getIdrubro().getId()+","+solicitudAdmision.getIdgiraduria().getId()+","+
                solicitudAdmision.getIdpromotor().getId()+","+solicitudAdmision.getIdfuncionario().getId()+","+
                solicitudAdmision.getReunerequisitos()+","+solicitudAdmision.getObservaciones()+","+solicitudAdmision.getNumeroacta()+","+
                dtfFechaActa.getValue()+","+solicitudAdmision.getAprobado()+","+solicitudAdmision.getObservacioncomite()+","+
                solicitudAdmision.getNumeroresolucion()+","+dtfFechaResolucion.getValue()+","+solicitudAdmision.getNumerosesion()+","+solicitudAdmision.getApruebacomision()+")";
    }
    
    public void setNuevo() {
        this.nuevo = "("+solicitudAdmision.getId()+","+solicitudAdmision.getNumerosolicitud()+","+
                solicitudAdmision.getIdestadosolicitud().getId()+","+solicitudAdmision.getCedula()+","+solicitudAdmision.getNombre()+","+
                solicitudAdmision.getApellido()+","+solicitudAdmision.getIdprofesion().getId()+","+solicitudAdmision.getIdlugarnacimiento().getId()+","+
                dtfFechaNacimiento.getValue()+","+solicitudAdmision.getIdnacionalidad().getId()+","+solicitudAdmision.getTelefonocelular()+","+
                solicitudAdmision.getTelefonolineabaja()+","+solicitudAdmision.getEmail()+","+solicitudAdmision.getIdsexo().getId()+","+
                solicitudAdmision.getIdestadocivil().getId()+","+solicitudAdmision.getDireccion()+","+solicitudAdmision.getNumerocasa()+","+
                solicitudAdmision.getIdbarrio().getId()+","+solicitudAdmision.getIdciudadresidencia().getId()+","+solicitudAdmision.getIdtipocasa().getId()+","+
                solicitudAdmision.getIdformacion().getId()+","+solicitudAdmision.getExsocio()+","+dtfFechaDesvinculacion.getValue()+","+
                solicitudAdmision.getIdinstitucion().getId()+","+dtfFechaIngreso.getValue()+","+solicitudAdmision.getIdcargo().getId()+","+
                solicitudAdmision.getSeccion()+","+solicitudAdmision.getIdrubro().getId()+","+solicitudAdmision.getIdgiraduria().getId()+","+
                solicitudAdmision.getIdpromotor().getId()+","+solicitudAdmision.getIdfuncionario().getId()+","+
                solicitudAdmision.getReunerequisitos()+","+solicitudAdmision.getObservaciones()+","+solicitudAdmision.getNumeroacta()+","+
                dtfFechaActa.getValue()+","+solicitudAdmision.getAprobado()+","+solicitudAdmision.getObservacioncomite()+","+
                solicitudAdmision.getNumeroresolucion()+","+dtfFechaResolucion.getValue()+","+solicitudAdmision.getNumerosesion()+","+solicitudAdmision.getApruebacomision()+")";
    }

    public void setSolicitudAdmision(SolicitudAdmision s) {
        try {
            this.solicitudAdmision = s;
            binder.setBean(s);
            btnBorrar.setVisible((solicitudAdmision.getId()!=null));
            setVisible(true);
            txtfCedula.selectAll();
            this.viejo= "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
