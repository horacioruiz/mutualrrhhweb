/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.DependenciaDao;
import py.mutualmsp.mutualweb.dao.FeriadoDao;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.dao.MarcacionesDao;
import py.mutualmsp.mutualweb.dao.ParametroDao;
import py.mutualmsp.mutualweb.dao.RotacionesDao;
import py.mutualmsp.mutualweb.dto.MarcacionesDTO;
import py.mutualmsp.mutualweb.dto.RotacionesDTO;
import py.mutualmsp.mutualweb.entities.Dependencia;
import py.mutualmsp.mutualweb.entities.Feriado;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Licencias;
import py.mutualmsp.mutualweb.entities.Marcaciones;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.Constants;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.InputModal;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;
import static py.mutualmsp.mutualweb.vista.MarcacionesView.isWeekend;

/**
 *
 * @author Dbarreto
 */
public class RotacionForm extends FormLayout {

    private DateField fecha = new DateField("Fecha");
    ComboBox<Funcionario> filterFuncionario = new ComboBox<>("Funcionario");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");
    Button btnBorrar = new Button("Borrar");

    List<Marcaciones> listMarcation = new ArrayList<>();
    ArrayList<RotacionesDTO> listaRotacionesDTO = new ArrayList<>();

    ImageReceiver receiver = new ImageReceiver();
    Upload upload = new Upload("", receiver);
    String destinarariosString = "";
    String filename;
    byte[] content;
    public File file;
    FileOutputStream fos = null;
    String url = "";
    Button btnCargarDatos = new Button("");
    Button btnImportar = new Button("");
    TextField txtfFiltro = new TextField("Archivo");

    RotacionesDao rotacionDao = ResourceLocator.locate(RotacionesDao.class);
    ParametroDao parametroDao = ResourceLocator.locate(ParametroDao.class);
    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    Rotaciones rotacion;

    FeriadoDao feriadoDao = ResourceLocator.locate(FeriadoDao.class);
    MarcacionesDao marcacionDao = ResourceLocator.locate(MarcacionesDao.class);
    HorarioFuncionarioDao hfDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioLaboralDao hlDao = ResourceLocator.locate(HorarioLaboralDao.class);

    private Consumer<Rotaciones> guardarListener;
    private Consumer<Rotaciones> borrarListener;
    private Consumer<Rotaciones> cancelarListener;

    private Date viejo = new Date();
    private Funcionario func = new Funcionario();

    private String nuevo = "";
    private String operacion = "";
    public static final String NOMBRE_TABLA = "rotacion";
    DependenciaDao dptoDao = ResourceLocator.locate(DependenciaDao.class);
    Map<Long, Funcionario> mapeoFuncionarios = new HashMap();

    public RotacionForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();

            List<Dependencia> depen = dptoDao.getDependenciaByDescripcion("departamento de recursos humanos", "seccion de salarios y beneficios");
            List<Dependencia> listDependencia = new ArrayList<>();
            List<Funcionario> listFunc = new ArrayList<>();
            for (Dependencia dependencia : depen) {
                listDependencia = dptoDao.listarSubDependencia(dependencia.getId());
                    if (listFunc.isEmpty()) {
                        listFunc = funcionarioDao.ListarPorDependencia(dependencia.getId());
                    }else{
                        listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia.getId()));
                    } 
            }
            for (Dependencia dependencia1 : listDependencia) {
                listFunc.addAll(funcionarioDao.ListarPorDependencia(dependencia1.getId()));
            }
            for (Funcionario funcio : listFunc) {
                mapeoFuncionarios.put(funcio.getId(), funcio);
            }

            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);

            Label htmlRegister = new Label("<b>Registrar por funcionario</b>",
                    ContentMode.HTML);
            fecha.setValue(DateUtils.asLocalDate(new Date()));
            filterFuncionario.setPlaceholder("Seleccione funcionario");
            if (UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("JEFE") != -1 || UserHolder.get().getIdfuncionario().getCargo().getDescripcion().toUpperCase().indexOf("ENCARGADO") != -1 || mapeoFuncionarios.containsKey(UserHolder.get().getIdfuncionario().getId())) {
                filterFuncionario.setItems(funcionarioDao.listaFuncionario());
            } else {
                filterFuncionario.setItems(funcionarioDao.listarFuncionarioPorCI(UserHolder.get().getIdfuncionario().getCedula()));
            }
            filterFuncionario.setItemCaptionGenerator(Funcionario::getNombreCompleto);

            txtfFiltro.setEnabled(false);

            Label divider = new Label("<hr></hr>", ContentMode.HTML);
            Label htmlLabel = new Label("<b>Importar Rotaciones</b>",
                    ContentMode.HTML);

            addComponents(htmlRegister, fecha, filterFuncionario, botones, divider, htmlLabel);
            divider.setWidth(100f, Sizeable.Unit.PERCENTAGE);
            txtfFiltro.setPlaceholder("Archivo Seleccionado");
            addComponent(txtfFiltro);
            HorizontalLayout botonesImportacion = new HorizontalLayout();

            btnCargarDatos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnCargarDatos.setIcon(VaadinIcons.UPLOAD);
            btnCargarDatos.setEnabled(false);

            btnCargarDatos.addClickListener(c -> {
                if (!txtfFiltro.getValue().equalsIgnoreCase("")) {
                    ConfirmButton confirmMessage = new ConfirmButton("");
                    confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea registrar las rotaciones seleccionada?", "25%");
                    confirmMessage.getOkButton().addClickListener(e -> {
                        UI ui = UI.getCurrent();

                        // Instruct client side to poll for changes and show spinner
                        ui.setPollInterval(500);

                        // Start background task
                        CompletableFuture.runAsync(() -> {

                            // Do some long running task
                            //Thread.sleep(2000);
                            try {
                                leerCsv(file.getAbsolutePath());
                                //btnImportar.setVisible(true);
                            } catch (FileNotFoundException ex) {
                                System.out.println("-->> " + ex.getLocalizedMessage());
                                System.out.println("-->> " + ex.fillInStackTrace());
                            } catch (IOException ex) {
                                System.out.println("-->> " + ex.getLocalizedMessage());
                                System.out.println("-->> " + ex.fillInStackTrace());
                            } catch (ParseException ex) {
                                System.out.println("-->> " + ex.getLocalizedMessage());
                                System.out.println("-->> " + ex.fillInStackTrace());
                            } finally {
                            }

                            // Need to use access() when running from background thread
                            ui.access(() -> {
                                // Stop polling and hide spinner
                                ui.setPollInterval(-1);
                                Notification.show("Mensaje del Sistema", "Excelente, los datos estan siendo procesados...", Notification.Type.HUMANIZED_MESSAGE);
                                setVisible(false);
                                cancelarListener.accept(rotacion);
                            });
                        });
                        confirmMessage.closePopup();
                    });
                    confirmMessage.getCancelButton().addClickListener(e -> {
                        confirmMessage.closePopup();
                    });
                }
            });

            upload.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnImportar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnImportar.setIcon(VaadinIcons.UPLOAD_ALT);
            btnImportar.setVisible(false);

            botonesImportacion.addComponents(upload, btnCargarDatos, btnImportar);
            botonesImportacion.setComponentAlignment(btnCargarDatos, Alignment.BOTTOM_CENTER);
            botonesImportacion.setComponentAlignment(btnImportar, Alignment.BOTTOM_CENTER);
            addComponent(botonesImportacion);
            //            binder.forField(txtfDescripcion).withNullRepresentation("")
            //                    .bind(Rotaciones::getDescripcion, Rotaciones::setDescripcion);
            //            binder.bindInstanceFields(this);
            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    List<Rotaciones> listRotacion = rotacionDao.listarRotacionesPorPeriodoFuncionario(DateUtils.asDate(fecha.getValue()), DateUtils.asDate(fecha.getValue()), filterFuncionario.getValue().getId());
                    if (listRotacion.size() > 0) {
                        Notification.show("Mensaje del Sistema", "No se crean rotacions en días de rotaciones.", Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        guardar();
                    }
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(rotacion);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Los datos que se leen son ci, nombreApellido y fecha
    private void leerCsv(String absolutePath) throws IOException, ParseException {
        File f1 = new File(absolutePath);// OUTFILE
        FileReader fR1 = new FileReader(f1);
        BufferedReader reader1 = new BufferedReader(fR1);
        HashMap<String, MarcacionesDTO> mapeo = new HashMap();
        listaRotacionesDTO = new ArrayList<>();
        while (reader1.ready()) {
            try {
                String linea = reader1.readLine();

                StringTokenizer st = new StringTokenizer(linea, ",");
                RotacionesDTO marcDTO = new RotacionesDTO();
                marcDTO.setCi(st.nextElement().toString());
                marcDTO.setNombreApellido(st.nextElement().toString());
                String[] parts = st.nextElement().toString().split(" ");
                SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyy-MM-dd");
                String dateInString = parts[0];
                marcDTO.setFecha(sdfFecha.parse(dateInString));

                listaRotacionesDTO.add(marcDTO);
            } catch (Exception e) {
            } finally {
            }
        }
        int i = 0;
        Comparator<RotacionesDTO> compareById = (RotacionesDTO o1, RotacionesDTO o2)
                -> o1.getCi().compareTo(o2.getCi());
        Collections.sort(listaRotacionesDTO, compareById);
        for (RotacionesDTO row : listaRotacionesDTO) {
            Funcionario func = funcionarioDao.listarFuncionarioPorCI(row.getCi());
            List<Rotaciones> listrotacion = rotacionDao.listarPorFechas(row.getFecha(), row.getFecha(), func.getId());
            i++;
            Rotaciones marcacion = new Rotaciones();
            if (listrotacion.size() > 0) {
                marcacion = listrotacion.get(0);
            }
            marcacion.setNombrefuncionario(func.getNombreCompleto());
            marcacion.setFuncionario(func);
            marcacion.setFecha(row.getFecha());
            marcacion.setCargofunc(func.getCargo().getDescripcion());
            marcacion.setAreafunc(func.getDependencia().getDescripcion());

            rotacionDao.guardarRotaciones(marcacion);
        }
    }

    private void guardar() {
        try {
            try {
                rotacion.setFecha(DateUtils.asDate(fecha.getValue()));
            } catch (Exception ex) {
                rotacion.setFecha(null);
            } finally {
            }
            try {
                rotacion.setFuncionario(filterFuncionario.getValue());
            } catch (Exception ex) {
                rotacion.setFuncionario(null);
            } finally {
            }
            rotacion.setFuncionario(filterFuncionario.getValue());
            rotacion.setNombrefuncionario(filterFuncionario.getValue().getNombreCompleto());
            rotacion.setAreafunc(filterFuncionario.getValue().getCargo().getDescripcion());
            rotacion.setCargofunc(filterFuncionario.getValue().getDependencia().getDescripcion());

            listMarcation = marcacionDao.getByFilter(rotacion.getFuncionario(), rotacion.getFecha(), rotacion.getFecha(), null, 0);
            List<Feriado> listFeriado = feriadoDao.listarFeriadoPorPeriodo(rotacion.getFecha(), rotacion.getFecha());
            if (listFeriado.size() > 0) {
                Notification.show("Mensaje del Sistema", "No se asignan rotacions a dias feriados.", Notification.Type.HUMANIZED_MESSAGE);
            } else {
                if (isWeekendSaturday(fecha.getValue())) {
                    rotacionDao.guardarRotaciones(rotacion);
                    setVisible(false);
                    guardarListener.accept(rotacion);
                    Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                } else {
                    Notification.show("Información", "Se registran solo las rotaciones de los días sabados.", Notification.Type.ERROR_MESSAGE);
                }

//            if (viejo.equals("")) {
//                operacion = "I";
//            } else {
//                operacion = "M";
//            }
                this.setNuevo();
            }
            //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(rotacion);
        }
    }

    public static boolean isWeekendSaturday(LocalDate date) {
        DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
        switch (dayOfWeek) {
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    private void borrar() {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar la rotación?", "20%");
        confirmMessage.getOkButton().addClickListener(e -> {
            try {
                rotacionDao.borrar(rotacion);
                setVisible(false);
                borrarListener.accept(rotacion);
                Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
                operacion = "E";
                nuevo = "";
                //AuditoriaRegistrar.guardarAuditoria(NOMBRE_TABLA, operacion, viejo, nuevo);
            } catch (Exception ec) {
                ec.printStackTrace();
                Notification.show("Advertencia", "No se pudo borrar.", Notification.Type.ERROR_MESSAGE);
                borrarListener.accept(rotacion);
            }
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });

    }

    public void setGuardarListener(Consumer<Rotaciones> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Rotaciones> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Rotaciones> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setViejo(Rotaciones value) {
        try {
            if (value.getId() != null) {
                this.viejo = value.getFecha();
                this.func = value.getFuncionario();
            } else {
                fecha.setValue(DateUtils.asLocalDate(new Date()));
                filterFuncionario.setValue(null);
            }
        } catch (Exception e) {
            fecha.setValue(DateUtils.asLocalDate(new Date()));
            filterFuncionario.setValue(null);
        } finally {
        }
    }

    public void setNuevo() {
        this.nuevo = "(" + rotacion.getId() + "," + rotacion.getFecha() + ")";
    }

    public void setRotaciones(Rotaciones c) {
        try {
            this.rotacion = c;
            if (c.getId() != null) {
                fecha.setValue(DateUtils.asLocalDate(c.getFecha()));
                filterFuncionario.setValue(c.getFuncionario());
            }
            btnBorrar.setVisible((rotacion.getId() != null));
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (filterFuncionario == null || filterFuncionario.isEmpty()) {
            savedEnabled = false;
        }
        if (fecha == null || fecha.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {

        private static final long serialVersionUID = -1276759102490466761L;

        public OutputStream receiveUpload(String filename,
                String mimeType) {
            try {
//                // Open the file for writing.
                file = new File(Constants.UPLOAD_DIR + "/mutual-archivos/" + filename);
//                url = Constants.PUBLIC_SERVER_URL + "/helpdesk/" + filename;
//                fileName = filename;
//                ubicacion = Constants.PUBLIC_SERVER_URL + "/mutual-archivos/";
                fos = new FileOutputStream(file);
                btnCargarDatos.setEnabled(true);
                txtfFiltro.setValue(file.getName());
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            } catch (IOException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
            return fos; // Return the output stream to write to
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent event) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
