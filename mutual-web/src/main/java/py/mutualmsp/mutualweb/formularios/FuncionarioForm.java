/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.formularios;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import py.mutualmsp.mutualweb.dao.FuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioExporadicoDao;
import py.mutualmsp.mutualweb.dao.HorarioFuncionarioDao;
import py.mutualmsp.mutualweb.dao.HorarioLaboralDao;
import py.mutualmsp.mutualweb.entities.Funcionario;
import py.mutualmsp.mutualweb.entities.HorarioExporadico;
import py.mutualmsp.mutualweb.entities.HorarioFuncionario;
import py.mutualmsp.mutualweb.entities.HorarioLaboral;
import py.mutualmsp.mutualweb.entities.Rotaciones;
import py.mutualmsp.mutualweb.util.ConfirmButton;
import py.mutualmsp.mutualweb.util.DateUtils;
import py.mutualmsp.mutualweb.util.InputModal;
import py.mutualmsp.mutualweb.util.ResourceLocator;
import py.mutualmsp.mutualweb.util.UserHolder;

/**
 *
 * @author Dbarreto
 */
public class FuncionarioForm extends FormLayout {

    TextField txtfCedula = new TextField("Cédula", "Ingrese cédula");
    Label txtfCedulaIdentidad = new Label("Cédula");
    Label txtfNombreApellido = new Label("Nombre y Apellido");
    TextField txtfNombre = new TextField("Nombre", "Ingrese Nombre");
    TextField txtfApellido = new TextField("Apellido", "Ingrese Apellido");
    Button btnGuardar = new Button("Guardar");
    Button btnCancelar = new Button("Cancelar");

    Button btnGuardarExporadicos = new Button("Guardar");
    Button btnVerExporadico = new Button("");
    Button btnCancelarExporadicos = new Button("Cancelar");

    Button btnBorrar = new Button("Borrar");
    Button btnHorarioExporadico = new Button();
    CheckBox habilitado = new CheckBox("Habilitado");

    VerticalLayout formularioPrimero = new VerticalLayout();
    VerticalLayout formularioSegundo = new VerticalLayout();

    CheckBox chkLunes = new CheckBox("Lun");
    CheckBox chkMartes = new CheckBox("Mar");
    CheckBox chkMiercoles = new CheckBox("Miér");
    CheckBox chkJueves = new CheckBox("Jue");
    CheckBox chkViernes = new CheckBox("Vier");
    CheckBox chkSabado = new CheckBox("Sáb");
    CheckBox chkDomingo = new CheckBox("Domin.");;

    private DateField fechaDesde = new DateField("Fecha Desde");
    private DateField fechaHasta = new DateField("Fecha Hasta");

    TextField txtIdReloj = new TextField("ID reloj", "");
    ComboBox<String> cbHorarioLaboral = new ComboBox<>("Horario");
    ComboBox<String> cbHorarioLaboralExporadico = new ComboBox<>("Horario");
    HashMap<String, HorarioLaboral> mapHL = new HashMap<>();

    List<Funcionario> listaFuncionarioList = new ArrayList<>();

    FuncionarioDao funcionarioDao = ResourceLocator.locate(FuncionarioDao.class);
    HorarioLaboralDao horarioLaboralDao = ResourceLocator.locate(HorarioLaboralDao.class);
    HorarioFuncionarioDao horarioFuncionarioDao = ResourceLocator.locate(HorarioFuncionarioDao.class);
    HorarioExporadicoDao horarioExporadicoDao = ResourceLocator.locate(HorarioExporadicoDao.class);
    Binder<Funcionario> binder = new Binder<>(Funcionario.class);

    HorarioExporadicoForm horarioExporadicoForm = new HorarioExporadicoForm();

    Funcionario funcionario;

    private Consumer<Funcionario> guardarListener;
    private Consumer<Funcionario> borrarListener;
    private Consumer<Funcionario> cancelarListener;

    public FuncionarioForm() {
        try {
            addStyleName("product-form-wrapper");
            addStyleName("product-form");
            setSizeUndefined();
            HorizontalLayout botones = new HorizontalLayout();
            botones.addComponents(btnGuardar, btnBorrar, btnCancelar);

            HorizontalLayout botonesExporadicos = new HorizontalLayout();
            botonesExporadicos.addComponents(btnVerExporadico, btnGuardarExporadicos, btnCancelarExporadicos);

            HorizontalLayout checkSemanas = new HorizontalLayout();
            checkSemanas.addComponents(chkLunes, chkMartes, chkMiercoles);

            HorizontalLayout checkSemanas2 = new HorizontalLayout();
            checkSemanas2.addComponents(chkJueves, chkViernes, chkSabado);
            HorizontalLayout checkSemanas3 = new HorizontalLayout();
            checkSemanas2.addComponents(chkDomingo);
            btnGuardar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
            btnBorrar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnHorarioExporadico.addStyleName(MaterialTheme.BUTTON_ROUND);
            btnHorarioExporadico.setIcon(VaadinIcons.PLUS);
            btnCancelar.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
            btnVerExporadico.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_DANGER);
            btnVerExporadico.setIcon(VaadinIcons.LIST);

            btnGuardarExporadicos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_PRIMARY);
//            btnGuardarExporadicos.setIcon(VaadinIcons.ADD_DOCK);

            btnCancelarExporadicos.addStyleName(MaterialTheme.BUTTON_ROUND + " " + MaterialTheme.BUTTON_FRIENDLY);
//            btnCancelarExporadicos.setIcon(VaadinIcons.);

            Label divider = new Label("<hr></hr>", ContentMode.HTML);
            formularioPrimero.addComponent(txtfCedula);
            formularioPrimero.addComponent(txtfNombre);
            formularioPrimero.addComponent(txtfApellido);
            formularioPrimero.addComponent(txtIdReloj);
            formularioPrimero.addComponent(cbHorarioLaboral);
            formularioPrimero.addComponent(botones);
            formularioSegundo.addComponent(txtfCedulaIdentidad);
            formularioSegundo.addComponent(txtfNombreApellido);
            formularioSegundo.addComponent(checkSemanas);
            formularioSegundo.addComponent(checkSemanas2);
            formularioSegundo.addComponent(checkSemanas3);
            formularioSegundo.addComponent(fechaDesde);
            formularioSegundo.addComponent(fechaHasta);
            formularioSegundo.addComponent(cbHorarioLaboralExporadico);
            formularioSegundo.addComponent(botonesExporadicos);

            addComponents(formularioPrimero, formularioSegundo);

            binder.bind(txtfCedula, Funcionario::getCedula, Funcionario::setCedula);
            binder.forField(txtfCedula).withNullRepresentation("")
                    .bind(Funcionario::getCedula, Funcionario::setCedula);

            binder.bind(txtfNombre, Funcionario::getNombre, Funcionario::setNombre);
            binder.forField(txtfNombre).withNullRepresentation("")
                    .bind(Funcionario::getNombre, Funcionario::setNombre);

            binder.bind(txtfApellido, Funcionario::getApellido, Funcionario::setApellido);
            binder.forField(txtfApellido).withNullRepresentation("")
                    .bind(Funcionario::getApellido, Funcionario::setApellido);
            binder.bindInstanceFields(this);

            btnGuardar.addClickListener(e -> {
                if (validate()) {
                    guardar();
                } else {
                    Notification.show("Atención", "Todos los campos son obligatorios", Notification.Type.ERROR_MESSAGE);
                }
            });
            btnVerExporadico.addClickListener(e -> {
                verExporadicos();
            });

            btnBorrar.addClickListener(e -> {
                borrar();
            });

            btnHorarioExporadico.addClickListener(e -> {
                borrar();
            });
            btnGuardarExporadicos.addClickListener(e -> {
                saveExporadicos();
            });

            btnCancelar.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(funcionario);
            });
            btnCancelarExporadicos.addClickListener(e -> {
                setVisible(false);
                cancelarListener.accept(funcionario);
            });
            List<HorarioLaboral> listHorarioLaboral = horarioLaboralDao.getListHorarioLaboralByEstado(true);
            List<String> listString = new ArrayList<>();
            SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
            for (HorarioLaboral listHorario : listHorarioLaboral) {
                mapHL.put(formatHor.format(listHorario.getEntrada()) + " a " + formatHor.format(listHorario.getSalida()), listHorario);
                listString.add(formatHor.format(listHorario.getEntrada()) + " a " + formatHor.format(listHorario.getSalida()));
            }
            cbHorarioLaboral.setItems(listString);
            cbHorarioLaboralExporadico.setItems(listString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar() {
        try {
            funcionario.setIdestado(habilitado.getValue() == null || habilitado.getValue() == false ? 262L : 13L);
            funcionario = funcionarioDao.guardar(funcionario);
            setVisible(false);
            guardarHorarioFuncionario(funcionario);
            guardarListener.accept(funcionario);
            Notification.show("Información", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            Notification.show("Advertencia", "No se pudo guardar", Notification.Type.ERROR_MESSAGE);
            guardarListener.accept(funcionario);
        }
    }

    private void guardarHorarioFuncionario(Funcionario funcionario) {
        try {
            HorarioFuncionario hf = horarioFuncionarioDao.listarPorIdFuncionario(funcionario.getId());
            if (hf == null) {
                hf = new HorarioFuncionario();
                hf.setFuncionario(funcionario);
                hf.setHorarioLaboral(mapHL.get(cbHorarioLaboral.getValue()));
                hf.setIdreloj(Long.parseLong(txtIdReloj.getValue()));
                hf.setEstado(true);
            } else {
                hf.setFuncionario(funcionario);
                hf.setHorarioLaboral(mapHL.get(cbHorarioLaboral.getValue()));
                hf.setIdreloj(Long.parseLong(txtIdReloj.getValue()));
                hf.setEstado(hf.getEstado());
            }
            horarioFuncionarioDao.guardarHorarioFuncionario(hf);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    private void borrar() {
        ConfirmButton confirmMessage = new ConfirmButton("");
        confirmMessage.openInModalPopup("Mensaje de confirmación", "¿Desea eliminar al funcionario Seleccionado?", "20%");
        confirmMessage.getOkButton().addClickListener(e -> {
            try {
                funcionario.setIdestado(262L);
                funcionarioDao.guardar(funcionario);
                setVisible(false);
                borrarListener.accept(funcionario);
                Notification.show("Advertencia", "Se borraron los datos.", Notification.Type.HUMANIZED_MESSAGE);
            } catch (Exception ex) {
                ex.printStackTrace();
                Notification.show("Advertencia", "No se pudieron borrar los datos.", Notification.Type.ERROR_MESSAGE);
            }
            confirmMessage.closePopup();
        });
        confirmMessage.getCancelButton().addClickListener(e -> {
            confirmMessage.closePopup();
        });
    }

    public void setGuardarListener(Consumer<Funcionario> guardarListener) {
        this.guardarListener = guardarListener;
    }

    public void setBorrarListener(Consumer<Funcionario> borrarListener) {
        this.borrarListener = borrarListener;
    }

    public void setCancelarListener(Consumer<Funcionario> cancelarListener) {
        this.cancelarListener = cancelarListener;
    }

    public void setFuncionario(Funcionario f, boolean valor) {
        if (valor) {
            formularioSegundo.setVisible(true);
            formularioPrimero.setVisible(false);

            fechaDesde.setValue(null);
            fechaHasta.setValue(null);
            cbHorarioLaboralExporadico.setValue(null);

            chkLunes.setValue(false);
            chkMartes.setValue(false);
            chkMiercoles.setValue(false);
            chkJueves.setValue(false);
            chkViernes.setValue(false);
            chkSabado.setValue(false);
            chkDomingo.setValue(false);
        } else {
            formularioSegundo.setVisible(false);
            formularioPrimero.setVisible(true);
        }
        SimpleDateFormat formatHor = new SimpleDateFormat("HH:mm");
        try {
            this.funcionario = f;
            if (f.getId() != null) {
                if (f.getIdestado() == 13) {
                    habilitado.setValue(true);
                } else {
                    habilitado.setValue(false);
                }
                HorarioFuncionario hf = new HorarioFuncionario();
                try {
                    hf = horarioFuncionarioDao.listarPorIdFuncionario(funcionario.getId());
                    txtIdReloj.setValue(String.valueOf(hf.getIdreloj()));
                } catch (Exception e) {
                    txtIdReloj.setValue("");
                } finally {
                }
                try {
                    cbHorarioLaboral.setValue(formatHor.format(hf.getHorarioLaboral().getEntrada()) + " a " + formatHor.format(hf.getHorarioLaboral().getSalida()));
                } catch (Exception e) {
                    cbHorarioLaboral.setValue("");
                } finally {
                }
            } else {
                txtIdReloj.setValue("");
                cbHorarioLaboral.setValue("");
            }

            txtfCedula.setVisible(true);
            txtfCedulaIdentidad.setVisible(true);
            txtfNombreApellido.setVisible(true);

            binder.setBean(f);
            btnBorrar.setVisible((funcionario.getId() != null));
            setVisible(true);
            txtfCedula.selectAll();
            txtfCedulaIdentidad.setValue(f.getCedula());
            txtfNombreApellido.setValue(f.getNombreCompleto());
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnVerExporadico.setVisible(true);
    }

    private boolean validate() {
        boolean savedEnabled = true;
        if (txtfCedula == null || txtfCedula.isEmpty()) {
            savedEnabled = false;
        }
        if (txtfNombre == null || txtfNombre.isEmpty()) {
            savedEnabled = false;
        }
        if (txtfApellido == null || txtfApellido.isEmpty()) {
            savedEnabled = false;
        }
        if (cbHorarioLaboral == null || cbHorarioLaboral.isEmpty()) {
            savedEnabled = false;
        }
        return savedEnabled;
    }

    private void saveExporadicos() {
        boolean lunes = chkLunes.getValue();
        boolean martes = chkMartes.getValue();
        boolean miercoles = chkMiercoles.getValue();
        boolean jueves = chkJueves.getValue();
        boolean viernes = chkViernes.getValue();
        boolean sabado = chkSabado.getValue();
        boolean domingo = chkDomingo.getValue();

        Date desde = null;
        try {
            desde = DateUtils.asDate(fechaDesde.getValue());
        } catch (Exception e) {
        } finally {
        }
        Date hasta = null;
        try {
            hasta = DateUtils.asDate(fechaHasta.getValue());
        } catch (Exception e) {
        } finally {
        }

        boolean val = false;
        if (!(lunes || martes || miercoles || jueves || viernes || sabado || domingo || desde != null || hasta != null)) {
            Notification.show((String)"Advertencia", (String)"Debes seleccionar dias y rango de fecha para registrar un nuevo horario exporadico.", (Notification.Type)Notification.Type.ERROR_MESSAGE);
        } else if (this.cbHorarioLaboralExporadico.getValue() == null || ((String)this.cbHorarioLaboralExporadico.getValue()).equalsIgnoreCase("")) {
            Notification.show((String)"Advertencia", (String)"Debes seleccionar el horario.", (Notification.Type)Notification.Type.ERROR_MESSAGE);
        } else if ((lunes || martes || miercoles || jueves || viernes || sabado || domingo) && !((String)this.cbHorarioLaboralExporadico.getValue()).equalsIgnoreCase("")) {
            if (desde != null && hasta != null && !((String)this.cbHorarioLaboralExporadico.getValue()).equalsIgnoreCase("")) {
                val = true;
            } else {
                Notification.show((String)"Advertencia", (String)"Debes seleccionar fecha desde y fecha hasta para registrar los datos.", (Notification.Type)Notification.Type.ERROR_MESSAGE);
            }
        } else {
            Notification.show((String)"Advertencia", (String)"Debes seleccionar al menos un dia de la semana para registrar los datos.", (Notification.Type)Notification.Type.ERROR_MESSAGE);
        }

        if (val) {

            Funcionario funcionarioF = funcionarioDao.listarFuncionarioPorCI(txtfCedulaIdentidad.getValue());

            boolean valorExistencia = false;
            if (lunes && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("LUNES", desde, hasta, funcionarioF.getId());
            }
            if (martes && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("MARTES", desde, hasta, funcionarioF.getId());
            }
            if (miercoles && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("MIERCOLES", desde, hasta, funcionarioF.getId());
            }
            if (jueves && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("JUEVES", desde, hasta, funcionarioF.getId());
            }
            if (viernes && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("VIERNES", desde, hasta, funcionarioF.getId());
            }
            if (sabado && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("SABADO", desde, hasta, funcionarioF.getId());
            }
            if (domingo && !valorExistencia) {
                valorExistencia = horarioExporadicoDao.consultarExistenciasPorDias("DOMINGO", desde, hasta, funcionarioF.getId());
            }
            
            if (valorExistencia) {
                Notification.show("Advertencia", "Existe/n algún/os día/s que ya se han asignado como horario exporádico, verifíquelos.", Notification.Type.ERROR_MESSAGE);
            } else {
                if (listaFuncionarioList.isEmpty()) {
                    try {

                        HorarioExporadico he = new HorarioExporadico();
                        he.setFechadesde(desde);
                        he.setFechahasta(hasta);
                        he.setFuncionario(funcionarioF);
                        String horarioLaboralHere = cbHorarioLaboralExporadico.getValue();
                        he.setHorarioLaboral(mapHL.get(horarioLaboralHere));
                        he.setLunes(lunes);
                        he.setMartes(martes);
                        he.setMiercoles(miercoles);
                        he.setJueves(jueves);
                        he.setViernes(viernes);
                        he.setSabado(sabado);
                        he.setDomingo(domingo);

                        List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.listarExistenciaDatos(he);
                        if (listHorarioExporadico.isEmpty()) {
                            horarioExporadicoDao.guardarHorarioExporadico(he);
                            setVisible(false);
                            guardarListener.accept(funcionario);
                            Notification.show("Mensaje del Sistema", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                        } else {
                            Notification.show("Información", "Verificar el detalle existen datos que generan solapamiento.", Notification.Type.ERROR_MESSAGE);
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else {
                    boolean value = false;
                    for (Funcionario funcionario1 : listaFuncionarioList) {
                        try {
                            funcionarioF = funcionario1;
                            HorarioExporadico he = new HorarioExporadico();
                            he.setFechadesde(desde);
                            he.setFechahasta(hasta);
                            he.setFuncionario(funcionarioF);
                            String horarioLaboralHere = cbHorarioLaboralExporadico.getValue();
                            he.setHorarioLaboral(mapHL.get(horarioLaboralHere));
                            he.setLunes(lunes);
                            he.setMartes(martes);
                            he.setMiercoles(miercoles);
                            he.setJueves(jueves);
                            he.setViernes(viernes);
                            he.setSabado(sabado);
                            he.setDomingo(domingo);

                            List<HorarioExporadico> listHorarioExporadico = horarioExporadicoDao.listarExistenciaDatos(he);
                            if (listHorarioExporadico.isEmpty()) {
                                horarioExporadicoDao.guardarHorarioExporadico(he);
                                value = true;
                            } else {
                                System.out.println("Verificar el detalle existen datos que generan solapamiento.");
                                //Notification.show("Información", "Verificar el detalle existen datos que generan solapamiento.", Notification.Type.ERROR_MESSAGE);
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                    }
                    if (value) {
                        guardarListener.accept(funcionario);
                        Notification.show("Mensaje del Sistema", "Se ha guardado correctamente.", Notification.Type.HUMANIZED_MESSAGE);
                        setVisible(false);
                    } else {
                        Notification.show("Información", "Verificar el detalle existen datos que generan solapamiento.", Notification.Type.ERROR_MESSAGE);
                    }
                }
            }
        }
    }

    private void verExporadicos() {
        Funcionario funcionarioF = funcionarioDao.listarFuncionarioPorCI(txtfCedulaIdentidad.getValue());
        List<HorarioExporadico> listHE = horarioExporadicoDao.listarPorIdFuncionarioF(funcionarioF.getId());

        if (listHE.isEmpty()) {
            Notification.show("Mensaje del Sistema", "No existen detalles asociados.", Notification.Type.HUMANIZED_MESSAGE);
        } else {
            HorarioExporadicoForm horarioExporadicoForm = new HorarioExporadicoForm();
            UI.getCurrent().addWindow(horarioExporadicoForm);

            horarioExporadicoForm.nuevoRegistro(funcionarioF, listHE);
            horarioExporadicoForm.setVisible(true);
        }
    }

    public void setFuncionario(List<Funcionario> listaFuncionario) {
        listaFuncionarioList = listaFuncionario;

        formularioSegundo.setVisible(true);
        formularioPrimero.setVisible(false);

        fechaDesde.setValue(null);
        fechaHasta.setValue(null);
        cbHorarioLaboralExporadico.setValue(null);

        chkLunes.setValue(false);
        chkMartes.setValue(false);
        chkMiercoles.setValue(false);
        chkJueves.setValue(false);
        chkViernes.setValue(false);
        chkSabado.setValue(false);
        chkDomingo.setValue(false);

        setVisible(true);
        txtfCedula.setVisible(false);
        txtfCedulaIdentidad.setVisible(false);
        txtfNombreApellido.setVisible(false);

        btnVerExporadico.setVisible(false);
    }

}
