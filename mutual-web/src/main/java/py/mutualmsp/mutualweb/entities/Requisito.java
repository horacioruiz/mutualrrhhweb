/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "requisito")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requisito.findAll", query = "SELECT r FROM Requisito r")
    , @NamedQuery(name = "Requisito.findByIdrequisito", query = "SELECT r FROM Requisito r WHERE r.idrequisito = :idrequisito")
    , @NamedQuery(name = "Requisito.findByDescripcion", query = "SELECT r FROM Requisito r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Requisito.findByIdtiporequisito", query = "SELECT r FROM Requisito r WHERE r.idtiporequisito = :idtiporequisito")})
public class Requisito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idrequisito")
    private Long idrequisito;
    @Size(max = 200)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrequisito", fetch = FetchType.LAZY)
    @JoinColumn(name = "idtiporequisito", referencedColumnName = "idtiporequisito")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tiporequisito idtiporequisito;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrequisito", fetch = FetchType.LAZY)
    private List<Requisitooperacion> requisitooperacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrequisito", fetch = FetchType.LAZY)
    private List<Verificaciondetalle> verificaciondetalleList;

    public Requisito() {
    }

    public Requisito(Long idrequisito) {
        this.idrequisito = idrequisito;
    }

    public Long getIdrequisito() {
        return idrequisito;
    }

    public void setIdrequisito(Long idrequisito) {
        this.idrequisito = idrequisito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setIdtiporequisito(Tiporequisito idtiporequisito) {
        this.idtiporequisito = idtiporequisito;
    }

    public Tiporequisito getIdtiporequisito() {
        return idtiporequisito;
    }
    
    @XmlTransient
    public List<Requisitooperacion> getRequisitooperacionList() {
        return requisitooperacionList;
    }

    public void setRequisitooperacionList(List<Requisitooperacion> requisitooperacionList) {
        this.requisitooperacionList = requisitooperacionList;
    }

    @XmlTransient
    public List<Verificaciondetalle> getVerificaciondetalleList() {
        return verificaciondetalleList;
    }

    public void setVerificaciondetalleList(List<Verificaciondetalle> verificaciondetalleList) {
        this.verificaciondetalleList = verificaciondetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrequisito != null ? idrequisito.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requisito)) {
            return false;
        }
        Requisito other = (Requisito) object;
        if ((this.idrequisito == null && other.idrequisito != null) || (this.idrequisito != null && !this.idrequisito.equals(other.idrequisito))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Requisito[ idrequisito=" + idrequisito + " ]";
    }
    
}
