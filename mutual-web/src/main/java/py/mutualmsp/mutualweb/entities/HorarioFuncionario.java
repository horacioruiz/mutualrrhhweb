/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "horario_funcionario", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HorarioFuncionario.findAll", query = "SELECT d FROM HorarioFuncionario d WHERE d.estado=TRUE")
    , @NamedQuery(name = "HorarioFuncionario.findById", query = "SELECT d FROM HorarioFuncionario d WHERE d.id = :id AND d.estado=TRUE")})
public class HorarioFuncionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idreloj")
    private Long idreloj;

    @JoinColumn(name = "idfuncionario", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcionario;

    @JoinColumn(name = "idhorariolaboral", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private HorarioLaboral horarioLaboral;

    @Column(name = "estado")
    private Boolean estado;

    public HorarioFuncionario() {
    }

    public Long getIdreloj() {
        return idreloj;
    }

    public void setIdreloj(Long idreloj) {
        this.idreloj = idreloj;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public HorarioLaboral getHorarioLaboral() {
        return horarioLaboral;
    }

    public void setHorarioLaboral(HorarioLaboral horarioLaboral) {
        this.horarioLaboral = horarioLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idreloj != null ? idreloj.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioFuncionario)) {
            return false;
        }
        HorarioFuncionario other = (HorarioFuncionario) object;
        if ((this.idreloj == null && other.idreloj != null) || (this.idreloj != null && !this.idreloj.equals(other.idreloj))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.HorarioFuncionario[ id=" + idreloj + " ]";
    }

}
