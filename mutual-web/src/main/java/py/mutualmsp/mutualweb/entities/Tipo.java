/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "tipo", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipo.findAll", query = "SELECT c FROM Tipo c")
    , @NamedQuery(name = "Tipo.findById", query = "SELECT c FROM Tipo c WHERE c.id = :id")
    , @NamedQuery(name = "Tipo.findByDescripcion", query = "SELECT c FROM Tipo c WHERE c.descripcion = :descripcion")})
public class Tipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "codigo")
    private String codigo;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "idcargo", fetch = FetchType.LAZY)
    //private List<SolicitudAdmision> solicitudAdmisionList;

    public Tipo() {
    }

    public Tipo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    //@XmlTransient
    //public List<SolicitudAdmision> getSolicitudAdmisionList() {
      //  return solicitudAdmisionList;
    //}

    //public void setSolicitudAdmisionList(List<SolicitudAdmision> solicitudAdmisionList) {
      //  this.solicitudAdmisionList = solicitudAdmisionList;
    //}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipo)) {
            return false;
        }
        Tipo other = (Tipo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Tipo[ id=" + id + " ]";
    }
    
}
