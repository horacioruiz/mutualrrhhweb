/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "licencias", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Licencias.findAll", query = "SELECT d FROM Licencias d")
    , @NamedQuery(name = "Licencias.findById", query = "SELECT d FROM Licencias d WHERE d.id = :id")})
public class Licencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idparametro", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Parametro parametro;

    @Column(name = "descripcion")
    private String descripcion;

    @JoinColumn(name = "idfuncionario", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcionario;

    @Column(name = "nombrefuncionario")
    private String nombrefuncionario;

    @Column(name = "cargofunc")
    private String cargofunc;

    @Column(name = "areafunc")
    private String areafunc;

    @Column(name = "fechaini")
    private Date fechaini;

    @Column(name = "fechafin")
    private Date fechafin;

    @Column(name = "horaini")
    private Timestamp horaini;

    @Column(name = "horafin")
    private Timestamp horafin;

    @Column(name = "cantmin")
    private Long cantmin;

    @Column(name = "motivo")
    private String motivo;

    @Column(name = "fechacreacion")
    private Date fechacreacion;

    @Column(name = "aprobado")
    private Long aprobado;

    @Column(name = "rechazo")
    private String rechazo;

    @Column(name = "cantidad")
    private Long cantidad;

    @JoinColumn(name = "idencargado", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario encargado;

    @JoinColumn(name = "idencargado2", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario encargado2;

    @JoinColumn(name = "idfuncrrhh", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Funcionario funcrrhh;

    public Licencias() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Parametro getParametro() {
        return parametro;
    }

    public Long getCantmin() {
        return cantmin;
    }

    public void setCantmin(Long cantmin) {
        this.cantmin = cantmin;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Long getAprobado() {
        return aprobado;
    }

    public void setAprobado(Long aprobado) {
        this.aprobado = aprobado;
    }

    public void setParametro(Parametro parametro) {
        this.parametro = parametro;
    }

    public Funcionario getEncargado2() {
        return encargado2;
    }

    public void setEncargado2(Funcionario encargado2) {
        this.encargado2 = encargado2;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Funcionario getEncargado() {
        return encargado;
    }

    public void setEncargado(Funcionario encargado) {
        this.encargado = encargado;
    }

    public Funcionario getFuncrrhh() {
        return funcrrhh;
    }

    public void setFuncrrhh(Funcionario funcrrhh) {
        this.funcrrhh = funcrrhh;
    }

    public String getRechazo() {
        return rechazo;
    }

    public void setRechazo(String rechazo) {
        this.rechazo = rechazo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombrefuncionario() {
        return nombrefuncionario;
    }

    public void setNombrefuncionario(String nombrefuncionario) {
        this.nombrefuncionario = nombrefuncionario;
    }

    public String getCargofunc() {
        return cargofunc;
    }

    public void setCargofunc(String cargofunc) {
        this.cargofunc = cargofunc;
    }

    public String getAreafunc() {
        return areafunc;
    }

    public void setAreafunc(String areafunc) {
        this.areafunc = areafunc;
    }

    public Date getFechaini() {
        return fechaini;
    }

    public void setFechaini(Date fechaini) {
        this.fechaini = fechaini;
    }

    public Date getFechafin() {
        return fechafin;
    }

    public void setFechafin(Date fechafin) {
        this.fechafin = fechafin;
    }

    public Timestamp getHoraini() {
        return horaini;
    }

    public void setHoraini(Timestamp horaini) {
        this.horaini = horaini;
    }

    public Timestamp getHorafin() {
        return horafin;
    }

    public void setHorafin(Timestamp horafin) {
        this.horafin = horafin;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Image getEstadoIcon() {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();
        if (this.aprobado == 1) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/good.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else if (this.aprobado == 2) {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/wrong.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        } else {
            FileResource resource = new FileResource(new File(basepath
                    + "/WEB-INF/images/pending.png"));
            Image imagen = new Image("Image from file", resource);
            imagen.setWidth(2, Sizeable.Unit.EM);
            return imagen;
        }
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licencias)) {
            return false;
        }
        Licencias other = (Licencias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Licencias[ id=" + id + " ]";
    }

}
