/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "requisitooperacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requisitooperacion.findAll", query = "SELECT r FROM Requisitooperacion r")
    , @NamedQuery(name = "Requisitooperacion.findByIdrequisitooperacion", query = "SELECT r FROM Requisitooperacion r WHERE r.idrequisitooperacion = :idrequisitooperacion")
    , @NamedQuery(name = "Requisitooperacion.findByIdtipooperacion", query = "SELECT r FROM Requisitooperacion r WHERE r.idtipooperacion = :idtipooperacion")
    , @NamedQuery(name = "Requisitooperacion.findByActivo", query = "SELECT r FROM Requisitooperacion r WHERE r.activo = :activo")
    , @NamedQuery(name = "Requisitooperacion.findByFechaactivacion", query = "SELECT r FROM Requisitooperacion r WHERE r.fechaactivacion = :fechaactivacion")
    , @NamedQuery(name = "Requisitooperacion.findByFechadesactivacion", query = "SELECT r FROM Requisitooperacion r WHERE r.fechadesactivacion = :fechadesactivacion")})
public class Requisitooperacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idrequisitooperacion")
    private Long idrequisitooperacion;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "idtipooperacion", referencedColumnName = "idtipooperacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipooperacion idtipooperacion;
    @Column(name = "activo")
    private Boolean activo;
    @JoinColumn(name = "idrequisito", referencedColumnName = "idrequisito")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Requisito idrequisito;
    @Column(name = "fechaactivacion")
    @Temporal(TemporalType.DATE)
    private Date fechaactivacion;
    @Column(name = "fechadesactivacion")
    @Temporal(TemporalType.DATE)
    private Date fechadesactivacion;
    
    public Requisitooperacion() {
    }

    public Requisitooperacion(Long idrequisitooperacion) {
        this.idrequisitooperacion = idrequisitooperacion;
    }

    public Requisitooperacion(Long idrequisitooperacion, Tipooperacion idtipooperacion) {
        this.idrequisitooperacion = idrequisitooperacion;
        this.idtipooperacion = idtipooperacion;
    }

    public Long getIdrequisitooperacion() {
        return idrequisitooperacion;
    }

    public void setIdrequisitooperacion(Long idrequisitooperacion) {
        this.idrequisitooperacion = idrequisitooperacion;
    }

    public Tipooperacion getIdtipooperacion() {
        return idtipooperacion;
    }

    public void setIdtipooperacion(Tipooperacion idtipooperacion) {
        this.idtipooperacion = idtipooperacion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Requisito getIdrequisito() {
        return idrequisito;
    }

    public void setIdrequisito(Requisito idrequisito) {
        this.idrequisito = idrequisito;
    }

    public Date getFechaactivacion() {
        return fechaactivacion;
    }

    public Date getFechadesactivacion() {
        return fechadesactivacion;
    }

    public void setFechaactivacion(Date fechaactivacion) {
        this.fechaactivacion = fechaactivacion;
    }

    public void setFechadesactivacion(Date fechadesactivacion) {
        this.fechadesactivacion = fechadesactivacion;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrequisitooperacion != null ? idrequisitooperacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requisitooperacion)) {
            return false;
        }
        Requisitooperacion other = (Requisitooperacion) object;
        if ((this.idrequisitooperacion == null && other.idrequisitooperacion != null) || (this.idrequisitooperacion != null && !this.idrequisitooperacion.equals(other.idrequisitooperacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Requisitooperacion[ idrequisitooperacion=" + idrequisitooperacion + " ]";
    }
    
}
