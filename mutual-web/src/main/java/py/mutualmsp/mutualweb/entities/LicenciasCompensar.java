/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "licencia_compensacion", schema = "rrhh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LicenciasCompensar.findAll", query = "SELECT d FROM LicenciasCompensar d")
    , @NamedQuery(name = "LicenciasCompensar.findById", query = "SELECT d FROM LicenciasCompensar d WHERE d.id = :id")})
public class LicenciasCompensar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "idlicencia", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Licencias licencia;

    @Column(name = "fechacompensar")
    private Date fechacompensar;

    @Column(name = "horaini")
    private Timestamp horaini;

    @Column(name = "horafin")
    private Timestamp horafin;

    @Column(name = "cantidad")
    private Long cantidad;

    @Column(name = "observacion")
    private String observacion;

    public LicenciasCompensar() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getHoraini() {
        return horaini;
    }

    public void setHoraini(Timestamp horaini) {
        this.horaini = horaini;
    }

    public Timestamp getHorafin() {
        return horafin;
    }

    public void setHorafin(Timestamp horafin) {
        this.horafin = horafin;
    }

    public Licencias getLicencia() {
        return licencia;
    }

    public void setLicencia(Licencias licencia) {
        this.licencia = licencia;
    }

    public Date getFechacompensar() {
        return fechacompensar;
    }

    public String getCantHsmin() {
        SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        SimpleDateFormat formatFecs = new SimpleDateFormat("yyyy-MM-dd");
        int minFin = ((getHorafin().toLocalDateTime().getHour() * 60) + getHorafin().toLocalDateTime().getMinute());
        int minInicio = ((getHoraini().toLocalDateTime().getHour() * 60) + getHoraini().toLocalDateTime().getMinute());
        long millis = (minFin - minInicio);
        try {
            return sdfHM.format(sdf.parse(millis + ""));
        } catch (ParseException ex) {
            return "00.00";
        }
    }

    public void setFechacompensar(Date fechacompensar) {
        this.fechacompensar = fechacompensar;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LicenciasCompensar)) {
            return false;
        }
        LicenciasCompensar other = (LicenciasCompensar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.LicenciasCompensar[ id=" + id + " ]";
    }

}
