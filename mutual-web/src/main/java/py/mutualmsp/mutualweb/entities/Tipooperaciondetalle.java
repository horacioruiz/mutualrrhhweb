/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dbarreto
 */
@Entity
@Table(name = "tipooperaciondetalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipooperaciondetalle.findAll", query = "SELECT t FROM Tipooperaciondetalle t")
    , @NamedQuery(name = "Tipooperaciondetalle.findByIdtipooperaciondetalle", query = "SELECT t FROM Tipooperaciondetalle t WHERE t.idtipooperaciondetalle = :idtipooperaciondetalle")
    , @NamedQuery(name = "Tipooperaciondetalle.findByIdtipooperacion", query = "SELECT t FROM Tipooperaciondetalle t WHERE t.idtipooperacion = :idtipooperacion")
    , @NamedQuery(name = "Tipooperaciondetalle.findByIdmodalidad", query = "SELECT t FROM Tipooperaciondetalle t WHERE t.idmodalidad = :idmodalidad")})
public class Tipooperaciondetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipooperaciondetalle")
    private Long idtipooperaciondetalle;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "idtipooperacion", referencedColumnName = "idtipooperacion")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipooperacion idtipooperacion;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "idmodalidad", referencedColumnName = "idmodalidad")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Modalidad idmodalidad;
    @NotNull
    
    public Tipooperaciondetalle() {
    }

    public Tipooperaciondetalle(Long idtipooperaciondetalle) {
        this.idtipooperaciondetalle = idtipooperaciondetalle;
    }

    public Tipooperaciondetalle(Long idtipooperaciondetalle, Tipooperacion idtipooperacion, Modalidad idmodalidad) {
        this.idtipooperaciondetalle = idtipooperaciondetalle;
        this.idtipooperacion = idtipooperacion;
        this.idmodalidad = idmodalidad;
    }

    public Long getIdtipooperaciondetalle() {
        return idtipooperaciondetalle;
    }

    public void setIdtipooperaciondetalle(Long idtipooperaciondetalle) {
        this.idtipooperaciondetalle = idtipooperaciondetalle;
    }

    public Tipooperacion getIdtipooperacion() {
        return idtipooperacion;
    }

    public void setIdtipooperacion(Tipooperacion idtipooperacion) {
        this.idtipooperacion = idtipooperacion;
    }

    public Modalidad getIdmodalidad() {
        return idmodalidad;
    }

    public void setIdmodalidad(Modalidad idmodalidad) {
        this.idmodalidad = idmodalidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipooperaciondetalle != null ? idtipooperaciondetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipooperaciondetalle)) {
            return false;
        }
        Tipooperaciondetalle other = (Tipooperaciondetalle) object;
        if ((this.idtipooperaciondetalle == null && other.idtipooperaciondetalle != null) || (this.idtipooperaciondetalle != null && !this.idtipooperaciondetalle.equals(other.idtipooperaciondetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Tipooperaciondetalle[ idtipooperaciondetalle=" + idtipooperaciondetalle + " ]";
    }
    
}
