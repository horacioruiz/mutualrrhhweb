/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "parentesco")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parentesco.findAll", query = "SELECT p FROM Parentesco p")
    , @NamedQuery(name = "Parentesco.findById", query = "SELECT p FROM Parentesco p WHERE p.id = :id")
    , @NamedQuery(name = "Parentesco.findByDescripcion", query = "SELECT p FROM Parentesco p WHERE p.descripcion = :descripcion")})
public class Parentesco implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idparentesco", fetch = FetchType.LAZY)
    private List<ReferenciaPersonalAdmision> referenciaPersonalAdmisionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idparentesco", fetch = FetchType.LAZY)
    private List<BeneficiarioAdmision> beneficiarioAdmisionList;

    public Parentesco() {
    }

    public Parentesco(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<ReferenciaPersonalAdmision> getReferenciaPersonalAdmisionList() {
        return referenciaPersonalAdmisionList;
    }

    public void setReferenciaPersonalAdmisionList(List<ReferenciaPersonalAdmision> referenciaPersonalAdmisionList) {
        this.referenciaPersonalAdmisionList = referenciaPersonalAdmisionList;
    }

    @XmlTransient
    public List<BeneficiarioAdmision> getBeneficiarioAdmisionList() {
        return beneficiarioAdmisionList;
    }

    public void setBeneficiarioAdmisionList(List<BeneficiarioAdmision> beneficiarioAdmisionList) {
        this.beneficiarioAdmisionList = beneficiarioAdmisionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parentesco)) {
            return false;
        }
        Parentesco other = (Parentesco) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.Parentesco[ id=" + id + " ]";
    }
    
}
