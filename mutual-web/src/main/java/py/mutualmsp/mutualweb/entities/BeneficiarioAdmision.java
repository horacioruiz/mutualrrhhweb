/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutualweb.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DBarreto
 */
@Entity
@Table(name = "beneficiario_admision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BeneficiarioAdmision.findAll", query = "SELECT b FROM BeneficiarioAdmision b")
    , @NamedQuery(name = "BeneficiarioAdmision.findById", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.id = :id")
    , @NamedQuery(name = "BeneficiarioAdmision.findByCedula", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.cedula = :cedula")
    , @NamedQuery(name = "BeneficiarioAdmision.findByNombre", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.nombre = :nombre")
    , @NamedQuery(name = "BeneficiarioAdmision.findByApellido", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.apellido = :apellido")
    , @NamedQuery(name = "BeneficiarioAdmision.findByFechanacimiento", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "BeneficiarioAdmision.findByDireccion", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.direccion = :direccion")
    , @NamedQuery(name = "BeneficiarioAdmision.findByTelefono", query = "SELECT b FROM BeneficiarioAdmision b WHERE b.telefono = :telefono")})
public class BeneficiarioAdmision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "cedula")
    private String cedula;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "telefono")
    private String telefono;
    @JoinColumn(name = "idciudadresidencia", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad idciudadresidencia;
    @JoinColumn(name = "idparentesco", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Parentesco idparentesco;
    @JoinColumn(name = "idprofesion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profesion idprofesion;
    @JoinColumn(name = "idsolicitud_admision", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudAdmision idsolicitudAdmision;

    public BeneficiarioAdmision() {
    }

    public BeneficiarioAdmision(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Ciudad getIdciudadresidencia() {
        return idciudadresidencia;
    }

    public void setIdciudadresidencia(Ciudad idciudadresidencia) {
        this.idciudadresidencia = idciudadresidencia;
    }

    public Parentesco getIdparentesco() {
        return idparentesco;
    }

    public void setIdparentesco(Parentesco idparentesco) {
        this.idparentesco = idparentesco;
    }

    public Profesion getIdprofesion() {
        return idprofesion;
    }

    public void setIdprofesion(Profesion idprofesion) {
        this.idprofesion = idprofesion;
    }

    public SolicitudAdmision getIdsolicitudAdmision() {
        return idsolicitudAdmision;
    }

    public void setIdsolicitudAdmision(SolicitudAdmision idsolicitudAdmision) {
        this.idsolicitudAdmision = idsolicitudAdmision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BeneficiarioAdmision)) {
            return false;
        }
        BeneficiarioAdmision other = (BeneficiarioAdmision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutualweb.entities.BeneficiarioAdmision[ id=" + id + " ]";
    }
    
}
